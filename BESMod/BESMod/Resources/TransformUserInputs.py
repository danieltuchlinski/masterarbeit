import openpyxl

def excel_to_text(input_file, output_file):
    wb = openpyxl.load_workbook(input_file)
    sheet = wb.active

    with open(output_file, 'w') as txt_file:
        for row in sheet.iter_rows():
            row_data = []
            for cell in row:
                if cell.value is not None:
                    row_data.append(str(cell.value))
                else:
                    row_data.append('')
            txt_file.write('\t'.join(row_data) + '\n')
            txt_file.write('\n')

    print("Die Excel-Datei wurde erfolgreich in eine Textdatei konvertiert!")

# Beispielaufruf
excel_to_text('SupplyTemperature_ddMPC.xlsx', 'SupplyTemperature_ddMPC.txt')
