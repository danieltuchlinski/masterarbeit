import openpyxl
from openpyxl.utils import get_column_letter
from datetime import datetime, timedelta

# Definiere den Zeitraum
start_date = datetime(2014, 9, 10)
end_date = datetime(2015, 5, 22)

# Definiere die Temperaturen
weekday_day_temperature = 20 + 273.15  # °C zu Kelvin
weekday_work_hours_temperature = 17 + 273.15  # °C zu Kelvin
weekend_temperature = 20 + 273.15  # °C zu Kelvin

# Erstelle eine neue Arbeitsmappe
workbook = openpyxl.Workbook()
sheet = workbook.active
sheet.title = "Temperature Data"

# Überschriften für die Spalten setzen
sheet["A1"] = "Time (seconds)"
sheet["B1"] = "Temperature (K)"

# Iteriere über den Zeitraum
current_date = start_date
time_delta = timedelta(hours=1)
time_in_seconds = 0
row = 2  # Startzeile für die Daten

while current_date <= end_date:
    # Überprüfe, ob es sich um ein Wochenende handelt
    if current_date.weekday() < 5:  # 0-4: Montag-Freitag
        # Überprüfe, ob es sich um die Arbeitszeit handelt (8 bis 17 Uhr)
            if current_date.hour >= 8 and current_date.hour < 17: # 8 17
                temperature = weekday_work_hours_temperature
            else:
                temperature = weekday_day_temperature
    else:
        temperature = weekend_temperature

    # Schreibe die Zeit in Sekunden und die Temperatur in die entsprechenden Zellen
    sheet["A" + str(row)] = time_in_seconds
    sheet["B" + str(row)] = temperature

    # Erhöhe das Datum um 1 Stunde und die Zeit in Sekunden um 1 Stunde
    current_date += time_delta
    time_in_seconds += 3600
    row += 1

# Speichere die Arbeitsmappe als XLSX-Datei
filename = "VariableSetTemperature_StandardTime.xlsx"
workbook.save(filename)

print("Die Datei", filename, "wurde erfolgreich erstellt.")