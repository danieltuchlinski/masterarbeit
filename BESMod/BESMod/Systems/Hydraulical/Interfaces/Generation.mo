within BESMod.Systems.Hydraulical.Interfaces;
package Generation "Models for heat generation in Heat Pump Systems"
  extends BESMod.Utilities.Icons.SystemIcon;

  model ElectricalHeater "Only heat using a heating rod"
    extends BaseClasses.PartialGeneration(
      final dTLoss_nominal=fill(0, nParallelDem),
      dp_nominal={hea.dp_nominal}, final nParallelDem=1);

    Modelica.Blocks.Logical.Switch switch1 annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=270,
          origin={46,-16})));
    Modelica.Blocks.Sources.Constant       dummyMassFlow(final k=1)
      annotation (Placement(transformation(extent={{84,-6},{64,14}})));
    Modelica.Blocks.Sources.Constant       dummyZero(k=0)
                                                     annotation (Placement(
          transformation(
          extent={{-10,-10},{10,10}},
          rotation=0,
          origin={18,4})));

    AixLib.Fluid.HeatExchangers.HeatingRod hea(
      redeclare package Medium = Medium,
      final allowFlowReversal=allowFlowReversal,
      final m_flow_nominal=m_flow_nominal[1],
      final m_flow_small=1E-4*abs(m_flow_nominal[1]),
      final show_T=show_T,
      final dp_nominal=heatingRodParameters.dp_nominal,
      final tau=30,
      final energyDynamics=energyDynamics,
      final p_start=p_start,
      final T_start=T_start,
      final Q_flow_nominal=Q_flow_nominal[1],
      final V=heatingRodParameters.V_hr,
      final eta=heatingRodParameters.eta_hr)
      annotation (Placement(transformation(extent={{-16,-16},{16,16}},
          rotation=90,
          origin={-32,10})));
    replaceable parameter
      BESMod.Systems.Hydraulical.Interfaces.Generation.RecordsCollection.HeatingRodBaseDataDefinition
      heatingRodParameters annotation (choicesAllMatching=true, Placement(
          transformation(extent={{-62,-42},{-50,-30}})));

    Modelica.Blocks.Logical.GreaterThreshold isOnHR(threshold=Modelica.Constants.eps)
      annotation (Placement(transformation(
          extent={{-6,-6},{6,6}},
          rotation=270,
          origin={46,14})));

    IBPSA.Fluid.Movers.SpeedControlled_y pump(
      redeclare final package Medium = Medium,
      final energyDynamics=energyDynamics,
      final p_start=p_start,
      final T_start=T_start,
      final allowFlowReversal=allowFlowReversal,
      final show_T=show_T,
      redeclare
        BESMod.Systems.RecordsCollection.Movers.AutomaticConfigurationData
        per(
        final speed_rpm_nominal=pumpData.speed_rpm_nominal,
        final m_flow_nominal=m_flow_nominal[1],
        final dp_nominal=dpDem_nominal[1] + dp_nominal[1],
        final rho=rho,
        final V_flowCurve=pumpData.V_flowCurve,
        final dpCurve=pumpData.dpCurve),
      final inputType=IBPSA.Fluid.Types.InputType.Continuous,
      final addPowerToMedium=pumpData.addPowerToMedium,
      final tau=pumpData.tau,
      final use_inputFilter=pumpData.use_inputFilter,
      final riseTime=pumpData.riseTimeInpFilter,
      final init=Modelica.Blocks.Types.Init.InitialOutput,
      final y_start=1) annotation (Placement(transformation(
          extent={{-10,10},{10,-10}},
          rotation=180,
          origin={48,-48})));
    IBPSA.Fluid.Sources.Boundary_pT bou1(
      redeclare package Medium = Medium,
      p=p_start,
      T=T_start,
      nPorts=1)                                    annotation (Placement(
          transformation(
          extent={{-10,-10},{10,10}},
          rotation=90,
          origin={62,-74})));

    Utilities.KPIs.EnergyKPICalculator KPIQHR(use_inpCon=false, y=hea.vol.heatPort.Q_flow)
      annotation (Placement(transformation(extent={{-40,-80},{-20,-60}})));
    replaceable parameter
      BESMod.Systems.RecordsCollection.Movers.MoverBaseDataDefinition
      pumpData annotation (choicesAllMatching=true, Placement(transformation(extent={{14,-64},
              {28,-52}})));
    BESMod.Utilities.Electrical.RealToElecCon realToElecCon(use_souGen=false)
      annotation (Placement(transformation(extent={{32,-108},{52,-88}})));
    Utilities.KPIs.DeviceKPICalculator KPIHeaRod1(
      use_reaInp=true,
      calc_singleOnTime=true,
      calc_totalOnTime=true,
      calc_numSwi=true)
      annotation (Placement(transformation(extent={{-60,-100},{-40,-80}})));
  equation
    connect(dummyZero.y,switch1. u3)
      annotation (Line(points={{29,4},{38,4},{38,-4}},    color={0,0,127}));
    connect(dummyMassFlow.y,switch1. u1)
      annotation (Line(points={{63,4},{54,4},{54,-4}}, color={0,0,127}));

    connect(hea.port_b, portGen_out[1]) annotation (Line(points={{-32,26},{-32,80},
            {100,80}},        color={0,127,255}));
    connect(hea.Pel, outBusGen.PelHR) annotation (Line(points={{-41.6,27.6},{-41.6,
            49.6},{-72,49.6},{-72,-100},{0,-100}},
                                         color={0,0,127}), Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}},
        horizontalAlignment=TextAlignment.Left));
    connect(portGen_in[1], pump.port_a) annotation (Line(points={{100,-2},{80,-2},
            {80,-48},{58,-48}}, color={0,127,255}));
    connect(pump.port_a, bou1.ports[1])
      annotation (Line(points={{58,-48},{62,-48},{62,-64}}, color={0,127,255}));
    connect(switch1.y, pump.y) annotation (Line(points={{46,-27},{46,-31.5},{48,-31.5},
            {48,-36}}, color={0,0,127}));
    connect(isOnHR.y, switch1.u2)
      annotation (Line(points={{46,7.4},{46,-4}}, color={255,0,255}));
    connect(hea.port_a, pump.port_b) annotation (Line(points={{-32,-6},{-34,-6},{
            -34,-48},{38,-48}}, color={0,127,255}));
    connect(isOnHR.u, sigBusGen.uHR) annotation (Line(points={{46,21.2},{48,21.2},
            {48,46},{2,46},{2,98}}, color={0,0,127}), Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}},
        horizontalAlignment=TextAlignment.Left));
    connect(realToElecCon.internalElectricalPin, internalElectricalPin)
      annotation (Line(
        points={{52.2,-97.8},{52.2,-96},{58,-96},{58,-86},{72,-86},{72,-100}},
        color={0,0,0},
        thickness=1));
    connect(realToElecCon.PEleLoa, hea.Pel) annotation (Line(points={{30,-94},{
            -80,-94},{-80,28},{-60,28},{-60,27.6},{-41.6,27.6}},
                                               color={0,0,127}));
    connect(KPIQHR.KPI, outBusGen.QHR_flow) annotation (Line(points={{-17.8,-70},
            {0,-70},{0,-100}}, color={135,135,135}), Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}},
        horizontalAlignment=TextAlignment.Left));
    connect(KPIHeaRod1.KPI, outBusGen.heaRod) annotation (Line(points={{-37.8,-90},
            {-14,-90},{-14,-86},{0,-86},{0,-100}}, color={135,135,135}), Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}},
        horizontalAlignment=TextAlignment.Left));
    connect(KPIHeaRod1.uRea, sigBusGen.uHR) annotation (Line(points={{-62.2,-90},
            {-76,-90},{-76,98},{2,98}}, color={0,0,127}), Text(
        string="%second",
        index=1,
        extent={{-6,3},{-6,3}},
        horizontalAlignment=TextAlignment.Right));
    connect(hea.u, sigBusGen.uHeaRod) annotation (Line(points={{-41.6,-9.2},{
            -41.6,-14},{-54,-14},{-54,98},{2,98}}, color={0,0,127}), Text(
        string="%second",
        index=1,
        extent={{-3,-6},{-3,-6}},
        horizontalAlignment=TextAlignment.Right));
  end ElectricalHeater;

  model GasBoiler "Just a gas boiler"
    extends BaseClasses.PartialGeneration(
      dTTra_nominal=fill(20, nParallelDem),
      dp_nominal={boilerNoControl.dp_nominal},
      final nParallelDem=1);
    replaceable parameter AixLib.DataBase.Boiler.General.BoilerTwoPointBaseDataDefinition
      paramBoiler "Parameters for Boiler" annotation(Dialog(group="Component data"),
      choicesAllMatching=true);
    parameter Real etaTempBased[:,2]=[293.15,1.09; 303.15,1.08; 313.15,1.05;
        323.15,1.; 373.15,0.99] "Table matrix for temperature based efficiency"
          annotation(Dialog(group="Component data"));
    replaceable parameter
      BESMod.Systems.RecordsCollection.TemperatureSensors.TemperatureSensorBaseDefinition
      temperatureSensorData
      annotation (Dialog(group="Component data"), choicesAllMatching=true,
      Placement(transformation(extent={{-98,-16},{-78,4}})));
    replaceable parameter
      BESMod.Systems.RecordsCollection.Movers.MoverBaseDataDefinition
      pumpData annotation (Dialog(group="Component data"),
        choicesAllMatching=true, Placement(transformation(extent={{54,-80},
              {68,-68}})));
    AixLib.Fluid.BoilerCHP.BoilerNoControl boilerNoControl(
      redeclare package Medium = Medium,
      final allowFlowReversal=allowFlowReversal,
      final m_flow_nominal=m_flow_nominal[1],
      final m_flow_small=1E-4*abs(m_flow_nominal[1]),
      final show_T=show_T,
      final tau=temperatureSensorData.tau,
      final initType=temperatureSensorData.initType,
      final transferHeat=temperatureSensorData.transferHeat,
      final TAmb=temperatureSensorData.TAmb,
      final tauHeaTra=temperatureSensorData.tauHeaTra,
      final rho_default=rho,
      final p_start=p_start,
      final T_start=T_start,
      paramBoiler=paramBoiler,
      etaTempBased=etaTempBased)
      annotation (Placement(transformation(extent={{-66,-6},{-34,26}})));

    Utilities.KPIs.EnergyKPICalculator KPIQHR(use_inpCon=false, y=boilerNoControl.QflowCalculation.y)
      annotation (Placement(transformation(extent={{-40,-100},{-20,-80}})));

    IBPSA.Fluid.Movers.SpeedControlled_y pump(
      redeclare final package Medium = Medium,
      final energyDynamics=energyDynamics,
      final p_start=p_start,
      final T_start=T_start,
      final allowFlowReversal=allowFlowReversal,
      final show_T=show_T,
      redeclare
        BESMod.Systems.RecordsCollection.Movers.AutomaticConfigurationData
        per(
        final speed_rpm_nominal=pumpData.speed_rpm_nominal,
        final m_flow_nominal=m_flow_nominal[1],
        final dp_nominal=dpDem_nominal[1] + dp_nominal[1],
        final rho=rho,
        final V_flowCurve=pumpData.V_flowCurve,
        final dpCurve=pumpData.dpCurve),
      final inputType=IBPSA.Fluid.Types.InputType.Continuous,
      final addPowerToMedium=pumpData.addPowerToMedium,
      final tau=pumpData.tau,
      final use_inputFilter=pumpData.use_inputFilter,
      final riseTime=pumpData.riseTimeInpFilter,
      final init=Modelica.Blocks.Types.Init.InitialOutput,
      final y_start=1) annotation (Placement(transformation(
          extent={{-10,10},{10,-10}},
          rotation=180,
          origin={46,-50})));

    IBPSA.Fluid.Sources.Boundary_pT bou1(
      redeclare package Medium = Medium,
      p=p_start,
      T=T_start,
      nPorts=1)                                    annotation (Placement(
          transformation(
          extent={{-10,-10},{10,10}},
          rotation=270,
          origin={66,-26})));
    BESMod.Utilities.Electrical.ZeroLoad zeroLoad
      annotation (Placement(transformation(extent={{26,-108},{46,-88}})));
    Utilities.KPIs.DeviceKPICalculator KPIHeaRod1(
      use_reaInp=true,
      calc_singleOnTime=true,
      calc_totalOnTime=true,
      calc_numSwi=true)
      annotation (Placement(transformation(extent={{-60,-80},{-40,-60}})));

  initial equation
    assert(paramBoiler.Q_nom >= Q_flow_nominal[1], "Nominal heat flow rate
  of boiler is smaller than nominal heat demand",   AssertionLevel.warning);

  equation

    connect(boilerNoControl.port_b, portGen_out[1]) annotation (Line(points={{-34,
            10},{-20,10},{-20,80},{100,80}}, color={0,127,255}));
    connect(boilerNoControl.T_in, sigBusGen.TBoiIn) annotation (Line(points={{-38.48,
            19.28},{2,19.28},{2,98}}, color={0,0,127}), Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}},
        horizontalAlignment=TextAlignment.Left));
    connect(boilerNoControl.T_out, sigBusGen.TBoiOut) annotation (Line(points={{-38.48,
            15.12},{2,15.12},{2,98}}, color={0,0,127}), Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}},
        horizontalAlignment=TextAlignment.Left));
    connect(boilerNoControl.u_rel, sigBusGen.uBoiSet) annotation (Line(points={{-61.2,
            21.2},{-68,21.2},{-68,98},{2,98}}, color={0,0,127}), Text(
        string="%second",
        index=1,
        extent={{-6,3},{-6,3}},
        horizontalAlignment=TextAlignment.Right));
    connect(portGen_in[1], pump.port_a) annotation (Line(points={{100,-2},{94,-2},
            {94,-6},{86,-6},{86,-50},{56,-50}}, color={0,127,255}));
    connect(boilerNoControl.port_a, pump.port_b) annotation (Line(points={{-66,10},
            {-68,10},{-68,-50},{36,-50},{36,-50}}, color={0,127,255}));
    connect(bou1.ports[1], pump.port_a)
      annotation (Line(points={{66,-36},{66,-50},{56,-50}}, color={0,127,255}));
    connect(zeroLoad.internalElectricalPin, internalElectricalPin) annotation (
        Line(
        points={{46,-98},{60,-98},{60,-100},{72,-100}},
        color={0,0,0},
        thickness=1));
    connect(pump.y, sigBusGen.uPump) annotation (Line(points={{46,-38},{48,-38},{
            48,14},{2,14},{2,98}}, color={0,0,127}), Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}},
        horizontalAlignment=TextAlignment.Left));
    connect(KPIQHR.KPI, outBusGen.QBoi_flow) annotation (Line(points={{-17.8,-90},
            {-10,-90},{-10,-88},{0,-88},{0,-100}}, color={135,135,135}), Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}},
        horizontalAlignment=TextAlignment.Left));
    connect(KPIHeaRod1.KPI, outBusGen.boi) annotation (Line(points={{-37.8,-70},{
            0,-70},{0,-100}}, color={135,135,135}), Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}},
        horizontalAlignment=TextAlignment.Left));
    connect(KPIHeaRod1.uRea, sigBusGen.uBoiSet) annotation (Line(points={{-62.2,
            -70},{-74,-70},{-74,98},{2,98}}, color={0,0,127}), Text(
        string="%second",
        index=1,
        extent={{-6,3},{-6,3}},
        horizontalAlignment=TextAlignment.Right));
    annotation (Documentation(info="<html>
<h4>Bottom-up parameters</h4>
<p><br>The value for dTTra_nominal is based on EN 303-1:2017, which requires a temperature spread between 10 and 20 K. </p>
<p>20 K is used for most applications, as for example stated in the datasheet of the Vitocrossal Typ CU3A.</p>
</html>"));
  end GasBoiler;

  model HeatPumpAndHeatingRod "Bivalent monoenergetic heat pump"
    extends
      BESMod.Systems.Hydraulical.Interfaces.Generation.BaseClasses.PartialGeneration(
      final QLoss_flow_nominal=f_design .* Q_flow_nominal .- Q_flow_nominal,
      final dTLoss_nominal=fill(0, nParallelDem),
      dTTra_nominal={if TDem_nominal[i] > 273.15 + 55 then 10 elseif
          TDem_nominal[i] > 44.9 + 273.15 then 8 else 5 for i in 1:nParallelDem},
      dp_nominal={heatPump.dpCon_nominal + dpHeaRod_nominal},
      nParallelDem=1);

    parameter Boolean use_airSource=true
      "Turn false to use water as temperature source."
       annotation(Dialog(group="Component choices"));

     parameter Boolean use_heaRod=true "=false to disable the heating rod"
       annotation(Dialog(group="Component choices"));
      replaceable model PerDataMainHP =
        AixLib.DataBase.HeatPump.PerformanceData.LookUpTable2D
      constrainedby
      AixLib.DataBase.HeatPump.PerformanceData.BaseClasses.PartialPerformanceData
      annotation (Dialog(group="Component data"), __Dymola_choicesAllMatching=true);
    parameter Modelica.Media.Interfaces.Types.Temperature TSoilConst=273.15 + 10
      "Constant soil temperature for ground source heat pumps"
      annotation(Dialog(group="Component choices", enable=use_airSource));

    replaceable package Medium_eva = Modelica.Media.Interfaces.PartialMedium constrainedby
      Modelica.Media.Interfaces.PartialMedium annotation (Dialog(group="Component choices"),
        choices(
          choice(redeclare package Medium = IBPSA.Media.Air "Moist air"),
          choice(redeclare package Medium = IBPSA.Media.Water "Water"),
          choice(redeclare package Medium =
              IBPSA.Media.Antifreeze.PropyleneGlycolWater (
                property_T=293.15,
                X_a=0.40)
                "Propylene glycol water, 40% mass fraction")));
    replaceable parameter
      BESMod.Systems.Hydraulical.Interfaces.Generation.RecordsCollection.HeatPumpBaseDataDefinition
      heatPumpParameters constrainedby
      BESMod.Systems.Hydraulical.Interfaces.Generation.RecordsCollection.HeatPumpBaseDataDefinition(
        final QGen_flow_nominal=Q_flow_nominal[1], final TOda_nominal=
          TOda_nominal) annotation (
      Dialog(group="Component data"),
      choicesAllMatching=true,
      Placement(transformation(extent={{-98,14},{-82,28}})));
    replaceable parameter
      BESMod.Systems.Hydraulical.Interfaces.Generation.RecordsCollection.HeatingRodBaseDataDefinition
      heatingRodParameters annotation (
      Dialog(group="Component data"),
      choicesAllMatching=true,
      Placement(transformation(extent={{64,44},{76,56}})));

    replaceable parameter
      BESMod.Systems.RecordsCollection.Movers.MoverBaseDataDefinition
      pumpData annotation (Dialog(group="Component data"),
      choicesAllMatching=true, Placement(transformation(extent={{42,-56},
              {56,-44}})));
    replaceable parameter
      BESMod.Systems.RecordsCollection.TemperatureSensors.TemperatureSensorBaseDefinition
      temperatureSensorData
      annotation (Dialog(group="Component data"), choicesAllMatching=true,
      Placement(transformation(extent={{60,10},{80,30}})));

    AixLib.Fluid.Interfaces.PassThroughMedium pasThrMedHeaRod(redeclare package
        Medium = Medium, allowFlowReversal=allowFlowReversal) if not use_heaRod
      "Pass through if heating rod is not used"
      annotation (Placement(transformation(extent={{20,20},{40,40}})));

    AixLib.Fluid.HeatPumps.HeatPump heatPump(
      redeclare package Medium_con = Medium,
      redeclare package Medium_eva = Medium_eva,
      final use_rev=true,
      final use_autoCalc=false,
      final Q_useNominal=0,
      final scalingFactor=heatPumpParameters.scalingFactor,
      final use_refIne=heatPumpParameters.use_refIne,
      final refIneFre_constant=heatPumpParameters.refIneFre_constant,
      final nthOrder=1,
      final useBusConnectorOnly=false,
      final mFlow_conNominal=m_flow_nominal[1],
      final VCon=heatPumpParameters.VCon,
      final dpCon_nominal=heatPumpParameters.dpCon_nominal,
      final use_conCap=false,
      final CCon=0,
      final GConOut=0,
      final GConIns=0,
      final mFlow_evaNominal=heatPumpParameters.mEva_flow_nominal,
      final VEva=heatPumpParameters.VEva,
      final dpEva_nominal=heatPumpParameters.dpEva_nominal,
      final use_evaCap=false,
      final CEva=0,
      final GEvaOut=0,
      final GEvaIns=0,
      final tauSenT=temperatureSensorData.tau,
      final transferHeat=true,
      final allowFlowReversalEva=allowFlowReversal,
      final allowFlowReversalCon=allowFlowReversal,
      final tauHeaTraEva=temperatureSensorData.tauHeaTra,
      final TAmbEva_nominal=temperatureSensorData.TAmb,
      final tauHeaTraCon=temperatureSensorData.tauHeaTra,
      final TAmbCon_nominal=temperatureSensorData.TAmb,
      final pCon_start=p_start,
      final TCon_start=T_start,
      final pEva_start=Medium_eva.p_default,
      final TEva_start=Medium_eva.T_default,
      final energyDynamics=energyDynamics,
      final show_TPort=show_T,
      redeclare model PerDataMainHP = PerDataMainHP,
      redeclare model PerDataRevHP =
          AixLib.DataBase.Chiller.PerformanceData.LookUpTable2D (dataTable=
              AixLib.DataBase.Chiller.EN14511.Vitocal200AWO201()))
                                                   annotation (Placement(
          transformation(
          extent={{22,-27},{-22,27}},
          rotation=270,
          origin={-44,15})));

    IBPSA.Fluid.Sources.Boundary_ph bou_sinkAir(final nPorts=1, redeclare
        package Medium = Medium_eva)                       annotation (Placement(
          transformation(
          extent={{10,-10},{-10,10}},
          rotation=180,
          origin={-90,-10})));
    IBPSA.Fluid.Sources.MassFlowSource_T bou_air(
      final m_flow=heatPumpParameters.mEva_flow_nominal,
      final use_T_in=true,
      redeclare package Medium = Medium_eva,
      final use_m_flow_in=false,
      final nPorts=1)
      annotation (Placement(transformation(extent={{-100,40},{-80,60}})));

    Modelica.Blocks.Logical.Switch switch2 annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=0,
          origin={-130,50})));
    Modelica.Blocks.Sources.BooleanConstant AirOrSoil(final k=use_airSource)
      annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=0,
          origin={-170,90})));

    Utilities.KPIs.EnergyKPICalculator KPIWel(use_inpCon=true)
      annotation (Placement(transformation(extent={{-140,-80},{-120,-60}})));
    Utilities.KPIs.EnergyKPICalculator KPIWHRel(use_inpCon=true) if use_heaRod
      annotation (Placement(transformation(extent={{-140,-48},{-120,-28}})));

    IBPSA.Fluid.Movers.SpeedControlled_y pump(
      redeclare final package Medium = Medium,
      final energyDynamics=energyDynamics,
      final p_start=p_start,
      final T_start=T_start,
      final allowFlowReversal=allowFlowReversal,
      final show_T=show_T,
      redeclare
        BESMod.Systems.RecordsCollection.Movers.AutomaticConfigurationData
        per(
        final speed_rpm_nominal=pumpData.speed_rpm_nominal,
        final m_flow_nominal=m_flow_nominal[1],
        final dp_nominal=dpDem_nominal[1] + dp_nominal[1],
        final rho=rho,
        final V_flowCurve=pumpData.V_flowCurve,
        final dpCurve=pumpData.dpCurve),
      final inputType=IBPSA.Fluid.Types.InputType.Continuous,
      final addPowerToMedium=pumpData.addPowerToMedium,
      final tau=pumpData.tau,
      final use_inputFilter=pumpData.use_inputFilter,
      final riseTime=pumpData.riseTimeInpFilter,
      final init=Modelica.Blocks.Types.Init.InitialOutput,
      final y_start=1)                 annotation (Placement(transformation(
          extent={{-10,10},{10,-10}},
          rotation=180,
          origin={10,-70})));
    AixLib.Fluid.HeatExchangers.HeatingRod hea(
      redeclare package Medium = Medium,
      final allowFlowReversal=allowFlowReversal,
      final m_flow_nominal=m_flow_nominal[1],
      final m_flow_small=1E-4*abs(m_flow_nominal[1]),
      final show_T=show_T,
      final dp_nominal=heatingRodParameters.dp_nominal,
      final tau=30,
      final energyDynamics=energyDynamics,
      final p_start=p_start,
      final T_start=T_start,
      final Q_flow_nominal=heatPumpParameters.QSec_flow_nominal,
      final V=heatingRodParameters.V_hr,
      final eta=heatingRodParameters.eta_hr,
      use_countNumSwi=false) if use_heaRod
      annotation (Placement(transformation(extent={{20,40},{40,60}})));

    Modelica.Blocks.Sources.Constant TSoil(k=TSoilConst)
      annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=0,
          origin={-170,50})));

    Utilities.KPIs.EnergyKPICalculator KPIQHP(use_inpCon=false, y=heatPump.con.QFlow_in)
      annotation (Placement(transformation(extent={{-140,-112},{-120,-92}})));
    Utilities.KPIs.EnergyKPICalculator KPIQHR(use_inpCon=false, y=hea.vol.heatPort.Q_flow)
      if use_heaRod
      annotation (Placement(transformation(extent={{-140,-140},{-120,-120}})));

    IBPSA.Fluid.Sources.Boundary_pT bouPum(
      redeclare package Medium = Medium,
      final p=p_start,
      final T=T_start,
      final nPorts=1) "Pressure boundary for pump" annotation (Placement(
          transformation(
          extent={{-10,-10},{10,10}},
          rotation=90,
          origin={50,-86})));

    IBPSA.Fluid.Sensors.TemperatureTwoPort senTGenOut(
      redeclare final package Medium = Medium,
      final allowFlowReversal=allowFlowReversal,
      m_flow_nominal=m_flow_nominal[1],
      tau=temperatureSensorData.tau,
      initType=temperatureSensorData.initType,
      T_start=T_start,
      final transferHeat=temperatureSensorData.transferHeat,
      TAmb=temperatureSensorData.TAmb,
      tauHeaTra=temperatureSensorData.tauHeaTra)
      "Temperature at supply (generation outlet)"
                                           annotation (Placement(transformation(
          extent={{10,10},{-10,-10}},
          rotation=180,
          origin={70,80})));
    BESMod.Utilities.Electrical.RealToElecCon realToElecCon(use_souGen=false)
      annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=180,
          origin={100,-78})));
    Modelica.Blocks.Math.MultiSum multiSum(nu=if use_heaRod then 3 else 2) annotation (Placement(
          transformation(
          extent={{-6,-6},{6,6}},
          rotation=180,
          origin={130,-82})));
    Utilities.KPIs.DeviceKPICalculator KPIHeaRod(
      use_reaInp=false,
      calc_singleOnTime=true,
      calc_totalOnTime=true,
      calc_numSwi=true)
      annotation (Placement(transformation(extent={{-140,-20},{-120,0}})));
    Utilities.KPIs.DeviceKPICalculator KPIHeaRod1(
      use_reaInp=true,
      calc_singleOnTime=true,
      calc_totalOnTime=true,
      calc_numSwi=true)
      annotation (Placement(transformation(extent={{-100,-120},{-80,-100}})));
    Modelica.Blocks.Sources.BooleanExpression booExpHeaPumIsOn(y=heatPump.greaterThreshold.y)
      annotation (Placement(transformation(extent={{-180,-20},{-160,0}})));
    Modelica.Blocks.Sources.RealExpression reaExpPEleHeaPum(y=heatPump.innerCycle.Pel)
      annotation (Placement(transformation(extent={{-180,-80},{-160,-60}})));
    Modelica.Blocks.Sources.Constant conIceFac(final k=1) annotation (Placement(
          transformation(
          extent={{-11,-11},{11,11}},
          rotation=0,
          origin={-169,11})));
    Modelica.Blocks.Sources.RealExpression reaExpTHeaPumOut(y=heatPump.senT_b1.T)
      annotation (Placement(transformation(extent={{-60,80},{-40,100}})));
    Modelica.Blocks.Sources.RealExpression reaExpTHeaPumIn(y=heatPump.senT_a1.T)
      annotation (Placement(transformation(extent={{-60,60},{-40,80}})));
    Modelica.Blocks.Sources.BooleanConstant conNotRev(final k=true) annotation (
        Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=0,
          origin={-170,-38})));
    Modelica.Blocks.Sources.RealExpression reaExpTEvaIn(y=heatPump.senT_a2.T)
      annotation (Placement(transformation(extent={{-60,40},{-40,60}})));
  protected
    parameter Modelica.Units.SI.PressureDifference dpHeaRod_nominal=if use_heaRod
         then heatingRodParameters.dp_nominal else 0;

  equation

    connect(bou_air.ports[1], heatPump.port_a2) annotation (Line(
        points={{-80,50},{-74,50},{-74,42},{-57.5,42},{-57.5,37}},
        color={0,127,255}));
    connect(heatPump.port_b2, bou_sinkAir.ports[1]) annotation (Line(
        points={{-57.5,-7},{-56,-7},{-56,-10},{-80,-10}},
        color={0,127,255}));
    connect(bou_air.T_in, switch2.y)
      annotation (Line(points={{-102,54},{-108,54},{-108,50},{-119,50}},
                                                     color={0,0,127}));
    connect(switch2.u2, AirOrSoil.y)
      annotation (Line(points={{-142,50},{-152,50},{-152,90},{-159,90}},
                                                       color={255,0,255}));
    connect(hea.Pel, KPIWHRel.u) annotation (Line(points={{41,56},{40,56},{40,68},
            {0,68},{0,-30},{-74,-30},{-74,-36},{-110,-36},{-110,-24},{-141.8,-24},
            {-141.8,-38}},            color={0,0,127}));
    connect(pump.port_a, portGen_in[1]) annotation (Line(
        points={{20,-70},{100,-70},{100,-2}},
        color={0,127,255}));

    connect(pasThrMedHeaRod.port_a, heatPump.port_b1) annotation (Line(points={{20,
            30},{10,30},{10,50},{-30,50},{-30,38},{-30.5,38},{-30.5,37}}, color={0,
            127,255}));
    connect(pump.port_b, heatPump.port_a1) annotation (Line(
        points={{1.77636e-15,-70},{-30.5,-70},{-30.5,-7}},
        color={0,127,255}));
    connect(TSoil.y, switch2.u3) annotation (Line(points={{-159,50},{-156,50},{-156,
            42},{-142,42}},       color={0,0,127}));
    connect(heatPump.port_b1, hea.port_a) annotation (Line(points={{-30.5,37},{-30.5,
            36},{-30,36},{-30,50},{20,50}},
                                color={0,127,255}));
    connect(bouPum.ports[1], pump.port_a)
      annotation (Line(points={{50,-76},{50,-70},{20,-70}}, color={0,127,255}));
    connect(senTGenOut.T, sigBusGen.THeaRodMea) annotation (Line(points={{70,91},{
            70,96},{26,96},{26,74},{2,74},{2,98}},
                             color={0,0,127}), Text(
        string="%second",
        index=1,
        extent={{-3,6},{-3,6}},
        horizontalAlignment=TextAlignment.Right));
    connect(senTGenOut.port_b, portGen_out[1])
      annotation (Line(points={{80,80},{100,80}}, color={0,127,255}));
    connect(pasThrMedHeaRod.port_b, senTGenOut.port_a) annotation (Line(points={{40,
            30},{54,30},{54,80},{60,80}}, color={0,127,255}));
    connect(hea.port_b,senTGenOut. port_a)
      annotation (Line(points={{40,50},{54,50},{54,80},{60,80}},
                                                 color={0,127,255}));

    connect(realToElecCon.internalElectricalPin, internalElectricalPin)
      annotation (Line(
        points={{89.8,-78.2},{72,-78.2},{72,-100}},
        color={0,0,0},
        thickness=1));
    connect(multiSum.y, realToElecCon.PEleLoa)
      annotation (Line(points={{122.98,-82},{112,-82}}, color={0,0,127}));
    if use_heaRod then
      connect(multiSum.u[3], hea.Pel) annotation (Line(points={{136,-82},{94,-82},
              {94,56},{41,56}},     color={0,0,127}));
      connect(multiSum.u[1], reaExpPEleHeaPum.y) annotation (Line(points={{136,-82},
              {144,-82},{144,-150},{-154,-150},{-154,-70},{-159,-70}},
                                                             color={0,0,127}),
        Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}},
        horizontalAlignment=TextAlignment.Left));
      connect(multiSum.u[2], pump.P) annotation (Line(points={{136,-82},{144,-82},
              {144,-114},{-14,-114},{-14,-58},{0,-58},{0,-61},{-1,-61}},
                                               color={0,0,127}));
    else
      connect(multiSum.u[2], pump.P) annotation (Line(points={{136,-82},{144,-82},
              {144,-114},{-14,-114},{-14,-58},{0,-58},{0,-61},{-1,-61}},
                                               color={0,0,127}));
      connect(multiSum.u[1], reaExpPEleHeaPum.y) annotation (Line(points={{136,-82},
              {144,-82},{144,-150},{-154,-150},{-154,-70},{-159,-70}},
                                                             color={0,0,127}),
        Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}},
        horizontalAlignment=TextAlignment.Left));
    end if;
    connect(switch2.u1, weaBus.TDryBul) annotation (Line(points={{-142,58},{-144,58},
            {-144,80},{-101,80}}, color={0,0,127}), Text(
        string="%second",
        index=1,
        extent={{-6,3},{-6,3}},
        horizontalAlignment=TextAlignment.Right));
    connect(pump.y, sigBusGen.uPump) annotation (Line(points={{10,-58},{24,-58},{24,
            -22},{2,-22},{2,98}}, color={0,0,127}), Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}},
        horizontalAlignment=TextAlignment.Left));
    connect(KPIQHR.KPI, outBusGen.QHR_flow) annotation (Line(points={{-117.8,-130},
            {-106,-130},{-106,-102},{0,-102},{0,-100}}, color={135,135,135}),
        Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}},
        horizontalAlignment=TextAlignment.Left));
    connect(KPIQHP.KPI, outBusGen.QHP_flow) annotation (Line(points={{-117.8,-102},
            {0,-102},{0,-100}},              color={135,135,135}), Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}},
        horizontalAlignment=TextAlignment.Left));
    connect(KPIWel.KPI, outBusGen.PEleHP) annotation (Line(points={{-117.8,-70},{-104,
            -70},{-104,-102},{0,-102},{0,-100}},                   color={135,135,
            135}), Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}},
        horizontalAlignment=TextAlignment.Left));
    connect(KPIWHRel.KPI, outBusGen.PEleHR) annotation (Line(points={{-117.8,-38},
            {-106,-38},{-106,-102},{0,-102},{0,-100}}, color={135,135,135}), Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}},
        horizontalAlignment=TextAlignment.Left));
    connect(KPIHeaRod.KPI, outBusGen.heaPum) annotation (Line(points={{-117.8,-10},
            {-106,-10},{-106,-12},{-98,-12},{-98,-100},{0,-100}},   color={135,
            135,135}), Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}},
        horizontalAlignment=TextAlignment.Left));
    connect(KPIHeaRod1.KPI, outBusGen.heaRod) annotation (Line(points={{-77.8,-110},
            {0,-110},{0,-100}},       color={135,135,135}), Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}},
        horizontalAlignment=TextAlignment.Left));
    connect(KPIHeaRod1.uRea, sigBusGen.uHeaRod) annotation (Line(points={{-102.2,-110},
            {-128,-110},{-128,-112},{-150,-112},{-150,98},{2,98}},       color={0,
            0,127}), Text(
        string="%second",
        index=1,
        extent={{-6,3},{-6,3}},
        horizontalAlignment=TextAlignment.Right));
    connect(booExpHeaPumIsOn.y, KPIHeaRod.u)
      annotation (Line(points={{-159,-10},{-142.2,-10}}, color={255,0,255}));
    connect(reaExpPEleHeaPum.y, KPIWel.u)
      annotation (Line(points={{-159,-70},{-141.8,-70}}, color={0,0,127}));
    connect(conIceFac.y, heatPump.iceFac_in) annotation (Line(points={{-156.9,11},
            {-106,11},{-106,-1.72},{-74.6,-1.72}}, color={0,0,127}));
    connect(heatPump.nSet, sigBusGen.yHeaPumSet) annotation (Line(points={{-39.5,-10.52},
            {-39.5,-26},{2,-26},{2,98}}, color={0,0,127}), Text(
        string="%second",
        index=1,
        extent={{-3,-6},{-3,-6}},
        horizontalAlignment=TextAlignment.Right));
    connect(booExpHeaPumIsOn.y, sigBusGen.heaPumIsOn) annotation (Line(points={{-159,
            -10},{-142,-10},{-142,98},{2,98}}, color={255,0,255}), Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}},
        horizontalAlignment=TextAlignment.Left));
    connect(reaExpTHeaPumIn.y, sigBusGen.THeaPumIn) annotation (Line(points={{-39,
            70},{2,70},{2,98}}, color={0,0,127}), Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}},
        horizontalAlignment=TextAlignment.Left));
    connect(reaExpTHeaPumOut.y, sigBusGen.THeaPumOut) annotation (Line(points={{-39,
            90},{-28,90},{-28,74},{2,74},{2,98}}, color={0,0,127}), Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}},
        horizontalAlignment=TextAlignment.Left));
    connect(heatPump.modeSet, conNotRev.y) annotation (Line(points={{-48.5,-10.52},
            {-48.5,-38},{-159,-38}}, color={255,0,255}));
    connect(reaExpTEvaIn.y, sigBusGen.THeaPumEvaIn) annotation (Line(points={{-39,
            50},{2,50},{2,98}}, color={0,0,127}), Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}},
        horizontalAlignment=TextAlignment.Left));
    connect(hea.u, sigBusGen.uHeaRod) annotation (Line(points={{18,56},{2,56},{2,98}},
          color={0,0,127}), Text(
        string="%second",
        index=1,
        extent={{-6,3},{-6,3}},
        horizontalAlignment=TextAlignment.Right));
    annotation (Line(
        points={{-52.775,-6.78},{-52.775,33.61},{-56,33.61},{-56,74}},
        color={255,204,51},
        thickness=0.5),
                Diagram(coordinateSystem(extent={{-180,-140},{100,100}})));
  end HeatPumpAndHeatingRod;

  model NoGeneration "No heat generation at all"
    extends BaseClasses.PartialGeneration(
      dp_nominal={0},
      dTTra_nominal={10},
      final nParallelDem=1);
    BESMod.Utilities.Electrical.ZeroLoad zeroLoad
      annotation (Placement(transformation(extent={{32,-108},{52,-88}})));
  equation
    connect(portGen_out, portGen_in) annotation (Line(points={{100,80},{78,80},{
            78,-2},{100,-2}}, color={0,127,255}));
    connect(zeroLoad.internalElectricalPin, internalElectricalPin) annotation (
        Line(
        points={{52,-98},{62,-98},{62,-100},{72,-100}},
        color={0,0,0},
        thickness=1));
  end NoGeneration;

  model SolarThermalBivHPAixLib
    "Solar thermal assistet monoenergetic heat pump with heating rod using AixLibs ST model"
    extends HeatPumpAndHeatingRod(
      m_flow_nominal={Q_flow_nominal[1]*f_design[1]/dTTra_nominal[1]/4184,
          solarThermalParas.m_flow_nominal},
                                  dTTra_nominal={if TDem_nominal[1] > 273.15 + 55
           then 10 elseif TDem_nominal[1] > 44.9 then 8 else 5,solarThermalParas.dTMax},
           final nParallelDem=2,
           final dp_nominal={heatPump.dpCon_nominal + dpHeaRod_nominal, dpST_nominal});
    replaceable parameter
      BESMod.Systems.Hydraulical.Interfaces.Generation.RecordsCollection.SolarThermalBaseDataDefinition
      solarThermalParas constrainedby
      BESMod.Systems.Hydraulical.Interfaces.Generation.RecordsCollection.SolarThermalBaseDataDefinition(
        final c_p=cp) annotation (
      Dialog(group="Component data"),
      choicesAllMatching=true,
      Placement(transformation(extent={{-86,-62},{-66,-42}})));
    replaceable parameter
      BESMod.Systems.RecordsCollection.Movers.MoverBaseDataDefinition
      pumpSTData
      annotation (Dialog(group="Component data"), choicesAllMatching=true, Placement(transformation(extent={{-98,
              -176},{-84,-162}})));
    AixLib.Fluid.Solar.Thermal.SolarThermal solarThermal(
      redeclare final package Medium = Medium,
      final allowFlowReversal=true,
      final m_flow_nominal=solarThermalParas.m_flow_nominal,
      final m_flow_small=1E-4*abs(solarThermalParas.m_flow_nominal),
      final show_T=false,
      final tau=1,
      final initType=Modelica.Blocks.Types.Init.InitialState,
      final T_start=T_start,
      final transferHeat=false,
      final TAmb=Medium.T_default,
      final tauHeaTra=1200,
      final p_start=p_start,
      final dp_nominal=dpST_nominal,
      final rho_default=rho,
      final a=solarThermal.pressureDropCoeff,
      final A=solarThermalParas.A,
      final volPip=solarThermalParas.volPip,
      final pressureDropCoeff=solarThermalParas.pressureDropCoeff,
      final Collector=AixLib.DataBase.SolarThermal.SimpleAbsorber(
            eta_zero=solarThermalParas.eta_zero,
            c1=solarThermalParas.c1,
            c2=solarThermalParas.c2),
      vol(final energyDynamics=energyDynamics))                annotation (Placement(
          transformation(
          extent={{-10,-10},{10,10}},
          rotation=180,
          origin={-30,-148})));

    IBPSA.Fluid.Movers.SpeedControlled_y pumpST(
      redeclare final package Medium = Medium,
      final energyDynamics=energyDynamics,
      final p_start=p_start,
      final T_start=T_start,
      final allowFlowReversal=allowFlowReversal,
      final show_T=show_T,
      redeclare BESMod.Systems.RecordsCollection.Movers.AutomaticConfigurationData per(
        final speed_rpm_nominal=pumpSTData.speed_rpm_nominal,
        final m_flow_nominal=solarThermalParas.m_flow_nominal,
        final dp_nominal=dpST_nominal+ dpDem_nominal[2],
        final rho=rho,
        final V_flowCurve=pumpSTData.V_flowCurve,
        final dpCurve=pumpSTData.dpCurve),
      final inputType=IBPSA.Fluid.Types.InputType.Continuous,
      final addPowerToMedium=pumpSTData.addPowerToMedium,
      final tau=pumpSTData.tau,
      final use_inputFilter=pumpSTData.use_inputFilter,
      final riseTime=pumpSTData.riseTimeInpFilter,
      final init=Modelica.Blocks.Types.Init.InitialOutput,
      final y_start=1) annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=180,
          origin={10,-150})));

    IBPSA.Fluid.Sources.Boundary_pT bou(
      redeclare package Medium = Medium,
      final p=p_start,
      final T=T_start,              nPorts=1) annotation (Placement(
          transformation(
          extent={{-10,-10},{10,10}},
          rotation=180,
          origin={70,-150})));
    Modelica.Blocks.Sources.Constant uPumSolTheAlwOn(k=1) annotation (Placement(
          transformation(
          extent={{-10,-10},{10,10}},
          rotation=180,
          origin={50,-170})));

    Utilities.KPIs.EnergyKPICalculator KPIWel1(use_inpCon=false, y=-solarThermal.heater.port.Q_flow)
      annotation (Placement(transformation(extent={{-60,-100},{-40,-80}})));

      Modelica.Blocks.Sources.RealExpression reaExpSolTheTCol(y=solarThermal.senTCold.T)
      annotation (Placement(transformation(extent={{-100,-120},{-80,-100}})));
      Modelica.Blocks.Sources.RealExpression reaExpSolTheTHot(y=solarThermal.senTHot.T)
      annotation (Placement(transformation(extent={{-100,-100},{-80,-80}})));

  protected
    parameter Modelica.Units.SI.PressureDifference dpST_nominal=solarThermalParas.m_flow_nominal
        ^2*solarThermalParas.pressureDropCoeff/(rho^2)
      "Pressure drop at nominal mass flow rate";
  equation

    connect(pumpST.port_a, bou.ports[1]) annotation (Line(points={{20,-150},{20,
            -136},{60,-136},{60,-150}},
                        color={0,127,255}));
    connect(pumpST.port_b, solarThermal.port_a) annotation (Line(points={{
            -1.77636e-15,-150},{-1.77636e-15,-148},{-20,-148}},
                                   color={0,127,255}));
    connect(solarThermal.port_b, portGen_out[2]) annotation (Line(points={{-40,
            -148},{-202,-148},{-202,124},{106,124},{106,82},{108,82},{108,82.5},{
            100,82.5}},                             color={0,127,255}));
    connect(portGen_in[2], pumpST.port_a) annotation (Line(points={{100,0.5},{102,
            0.5},{102,-6},{100,-6},{100,-48},{162,-48},{162,-136},{20,-136},{20,
            -150}},                color={0,127,255}));
    connect(pumpST.P, outBusGen.PelPumpST) annotation (Line(points={{-1,-159},{-6,
            -159},{-6,-132},{0,-132},{0,-100}},
                             color={0,0,127}), Text(
        string="%second",
        index=1,
        extent={{-6,3},{-6,3}},
        horizontalAlignment=TextAlignment.Right));
    connect(reaExpSolTheTCol.y, outBusGen.TSolCol_in) annotation (Line(points={{
            -79,-110},{-62,-110},{-62,-126},{0,-126},{0,-100}}, color={0,0,127}),
        Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}},
        horizontalAlignment=TextAlignment.Left));
    connect(reaExpSolTheTHot.y, outBusGen.TSolCol_out) annotation (Line(points={{
            -79,-90},{-66,-90},{-66,-108},{-16,-108},{-16,-116},{0,-116},{0,-100}},
          color={0,0,127}), Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}},
        horizontalAlignment=TextAlignment.Left));

    connect(solarThermal.T_air, weaBus.TDryBul) annotation (Line(points={{-24,
            -158},{-24,-178},{-194,-178},{-194,80},{-101,80}},
                                   color={0,0,127}), Text(
        string="%second",
        index=1,
        extent={{-6,3},{-6,3}},
        horizontalAlignment=TextAlignment.Right));
    connect(solarThermal.Irradiation, weaBus.HGloHor) annotation (Line(points={{-30,
            -158},{-30,-182},{-198,-182},{-198,80},{-101,80}},  color={0,0,127}),
        Text(
        string="%second",
        index=1,
        extent={{-6,3},{-6,3}},
        horizontalAlignment=TextAlignment.Right));
    connect(KPIWel1.KPI, outBusGen.QSolThe_flow) annotation (Line(points={{-37.8,
            -90},{-24,-90},{-24,-88},{0,-88},{0,-100}}, color={135,135,135}),
        Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}},
        horizontalAlignment=TextAlignment.Left));
    connect(uPumSolTheAlwOn.y, pumpST.y)
      annotation (Line(points={{39,-170},{10,-170},{10,-162}}, color={0,0,127}));
    annotation (Diagram(coordinateSystem(extent={{-200,-180},{100,100}}),
          graphics={Rectangle(
            extent={{100,-180},{-200,-118}},
            lineColor={0,0,0},
            lineThickness=1), Text(
            extent={{-188,-122},{-124,-140}},
            textColor={0,0,0},
            textString="Solar Thermal")}));
  end SolarThermalBivHPAixLib;

  model SolarThermalBivHPBuiLib
    "Solar thermal assistet monoenergetic heat pump with heating rod using Buildings ST model"
    extends HeatPumpAndHeatingRod(
      m_flow_nominal={Q_flow_nominal[1]*f_design[1]/dTTra_nominal[1]/4184,
          solarThermalParas.m_flow_nominal},
      redeclare package Medium = IBPSA.Media.Water,
                                  dTTra_nominal={if TDem_nominal[1] > 273.15 + 55
           then 10 elseif TDem_nominal[1] > 44.9 then 8 else 5,solarThermalParas.dTMax},
           final nParallelDem=2,
           final dp_nominal={heatPump.dpCon_nominal + dpHeaRod_nominal, dpST_nominal});
    replaceable parameter
      BESMod.Systems.Hydraulical.Interfaces.Generation.RecordsCollection.SolarThermalBaseDataDefinition
      solarThermalParas constrainedby
      BESMod.Systems.Hydraulical.Interfaces.Generation.RecordsCollection.SolarThermalBaseDataDefinition(
        final c_p=cp) annotation (
      Dialog(group="Component data"),
      choicesAllMatching=true,
      Placement(transformation(extent={{-86,-62},{-66,-42}})));
    replaceable parameter
      BESMod.Systems.RecordsCollection.Movers.MoverBaseDataDefinition
      pumpSTData
      annotation (Dialog(group="Component data"), choicesAllMatching=true, Placement(transformation(extent={{-80,
              -158},{-66,-144}})));
    Buildings.Fluid.SolarCollectors.EN12975 solCol(
      redeclare final package Medium = Medium,
      final allowFlowReversal=true,
      final m_flow_small=1E-4*abs(solarThermalParas.m_flow_nominal),
      final show_T=false,
      final T_start=T_start,
      final p_start=p_start,
      nSeg=5,
      azi=0,
      til=0.5235987755983,
      rho=0.2,
      use_shaCoe_in=false,
      shaCoe=0,
      nColType=Buildings.Fluid.SolarCollectors.Types.NumberSelection.Area,
      totalArea=solarThermalParas.A,
      sysConfig=Buildings.Fluid.SolarCollectors.Types.SystemConfiguration.Series,
      per=Buildings.Fluid.SolarCollectors.Data.GenericSolarCollector(
          ATyp=Buildings.Fluid.SolarCollectors.Types.Area.Aperture,
          A=4.302,
          mDry=484,
          V=4.4/1000,
          dp_nominal=100,
          mperA_flow_nominal=solarThermalParas.m_flow_nominal/solarThermalParas.A,
          B0=0,
          B1=0,
          y_intercept=solarThermalParas.eta_zero,
          slope=0,
          IAMDiff=0.133,
          C1=solarThermalParas.c1,
          C2=solarThermalParas.c2,
          G_nominal=solarThermalParas.GMax,
          dT_nominal=solarThermalParas.dTMax))                 annotation (Placement(
          transformation(
          extent={{-10,-10},{10,10}},
          rotation=180,
          origin={-30,-170})));

    IBPSA.Fluid.Movers.SpeedControlled_y pumpST(
      redeclare final package Medium = Medium,
      final energyDynamics=energyDynamics,
      final p_start=p_start,
      final T_start=T_start,
      final allowFlowReversal=allowFlowReversal,
      final show_T=show_T,
      redeclare BESMod.Systems.RecordsCollection.Movers.AutomaticConfigurationData per(
        final speed_rpm_nominal=pumpSTData.speed_rpm_nominal,
        final m_flow_nominal=solarThermalParas.m_flow_nominal,
        final dp_nominal=dpST_nominal+ dpDem_nominal[2],
        final rho=rho,
        final V_flowCurve=pumpSTData.V_flowCurve,
        final dpCurve=pumpSTData.dpCurve),
      final inputType=IBPSA.Fluid.Types.InputType.Continuous,
      final addPowerToMedium=pumpSTData.addPowerToMedium,
      final tau=pumpSTData.tau,
      final use_inputFilter=pumpSTData.use_inputFilter,
      final riseTime=pumpSTData.riseTimeInpFilter,
      final init=Modelica.Blocks.Types.Init.InitialOutput,
      final y_start=1) annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=180,
          origin={30,-170})));

    IBPSA.Fluid.Sources.Boundary_pT bou(
      redeclare package Medium = Medium,
      final p=p_start,
      final T=T_start,              nPorts=1) annotation (Placement(
          transformation(
          extent={{-10,-10},{10,10}},
          rotation=180,
          origin={70,-170})));
    Modelica.Blocks.Sources.Constant AirOrSoil1(k=1)
      annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=0,
          origin={-190,-150})));

    Utilities.KPIs.EnergyKPICalculator KPIWel1(use_inpCon=false, y=sum(solCol.vol.heatPort.Q_flow))
      annotation (Placement(transformation(extent={{-60,-120},{-40,-100}})));

    Modelica.Blocks.Logical.Switch switch3 annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=0,
          origin={-158,-170})));
    Modelica.Blocks.Sources.Constant AirOrSoil2(k=0)
      annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=0,
          origin={-190,-190})));
    Modelica.Blocks.Logical.Hysteresis       isOnHR1(uLow=10, uHigh=100)
      annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=0,
          origin={-210,-170})));
  protected
    parameter Modelica.Units.SI.PressureDifference dpST_nominal=solarThermalParas.m_flow_nominal
        ^2*solarThermalParas.pressureDropCoeff/(rho^2)
      "Pressure drop at nominal mass flow rate";
  equation

    connect(pumpST.port_a, bou.ports[1]) annotation (Line(points={{40,-170},{60,
            -170}},     color={0,127,255}));
    connect(pumpST.port_b, solCol.port_a) annotation (Line(points={{20,-170},{-20,
            -170}},           color={0,127,255}));
    connect(solCol.port_b, portGen_out[2]) annotation (Line(points={{-40,-170},{
            -40,-124},{-232,-124},{-232,126},{116,126},{116,78},{106,78},{106,
            82.5},{100,82.5}},                     color={0,127,255}));
    connect(portGen_in[2], pumpST.port_a) annotation (Line(points={{100,0.5},{102,
            0.5},{102,-156},{44,-156},{44,-170},{40,-170}},
                                   color={0,127,255}));
    connect(pumpST.P, outBusGen.PelPumpST) annotation (Line(points={{19,-179},{0,
            -179},{0,-100}}, color={0,0,127}), Text(
        string="%second",
        index=1,
        extent={{-6,3},{-6,3}},
        horizontalAlignment=TextAlignment.Right));

    connect(weaBus, solCol.weaBus) annotation (Line(
        points={{-101,80},{-101,-6},{-104,-6},{-104,-108},{-108,-108},{-108,-184},
            {-20,-184},{-20,-179.6}},
        color={255,204,51},
        thickness=0.5), Text(
        string="%first",
        index=-1,
        extent={{-6,3},{-6,3}},
        horizontalAlignment=TextAlignment.Right));
    connect(switch3.y, pumpST.y) annotation (Line(points={{-147,-170},{-130,-170},
            {-130,-186},{30,-186},{30,-182}},
                                        color={0,0,127}));
    connect(AirOrSoil2.y, switch3.u3) annotation (Line(points={{-179,-190},{-179,
            -186},{-170,-186},{-170,-178}},
                                          color={0,0,127}));
    connect(switch3.u2, isOnHR1.y) annotation (Line(points={{-170,-170},{-199,
            -170}},                                             color={255,0,255}));
    connect(isOnHR1.u, weaBus.HDirNor) annotation (Line(points={{-222,-170},{-228,
            -170},{-228,28},{-101,28},{-101,80}},
          color={0,0,127}), Text(
        string="%second",
        index=1,
        extent={{-6,3},{-6,3}},
        horizontalAlignment=TextAlignment.Right));
    connect(AirOrSoil1.y, switch3.u1) annotation (Line(points={{-179,-150},{-170,
            -150},{-170,-162}}, color={0,0,127}));
    annotation (Diagram(coordinateSystem(extent={{-220,-200},{100,100}}),
          graphics={Text(
            extent={{-216,-122},{-152,-140}},
            textColor={0,0,0},
            textString="Solar Thermal"), Rectangle(
            extent={{94,-198},{-218,-136}},
            lineColor={0,0,0},
            lineThickness=1)}));
  end SolarThermalBivHPBuiLib;

  package Tests
  extends Modelica.Icons.ExamplesPackage;

    model ElectricalHeater
      extends PartialTest(redeclare
          BESMod.Systems.Hydraulical.Interfaces.Generation.ElectricalHeater generation(
            redeclare
            BESMod.Systems.Hydraulical.Interfaces.Generation.RecordsCollection.DefaultHR
            heatingRodParameters, redeclare
            BESMod.Systems.RecordsCollection.Movers.DefaultMover pumpData));
      extends Modelica.Icons.Example;

      Modelica.Blocks.Sources.Constant     const1(k=1)
        annotation (Placement(transformation(extent={{-52,64},{-32,84}})));
    equation
      connect(const1.y, genControlBus.uHeaRod) annotation (Line(points={{-31,74},{
              10,74}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
    end ElectricalHeater;

    model GasBoiler
      extends Generation.Tests.PartialTest(redeclare
          BESMod.Systems.Hydraulical.Interfaces.Generation.GasBoiler generation(
          dTTra_nominal={10},
          redeclare AixLib.DataBase.Boiler.General.Boiler_Vitogas200F_11kW
            paramBoiler,
          redeclare
            BESMod.Systems.RecordsCollection.TemperatureSensors.DefaultSensor
            temperatureSensorData,
          redeclare BESMod.Systems.RecordsCollection.Movers.DefaultMover
            pumpData));
      extends Modelica.Icons.Example;

      Modelica.Blocks.Sources.Pulse        pulse(period=1800)
        annotation (Placement(transformation(extent={{-42,68},{-22,88}})));
    equation
      connect(pulse.y, genControlBus.uBoiSet) annotation (Line(points={{-21,78},{
              -18,78},{-18,74},{10,74}},
                    color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
      connect(pulse.y, genControlBus.uPump) annotation (Line(points={{-21,78},{-18,
              78},{-18,74},{10,74}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
    end GasBoiler;

    model HeatPumpAndHeatingRod
      extends PartialTest(   redeclare
          BESMod.Systems.Hydraulical.Interfaces.Generation.HeatPumpAndHeatingRod
          generation(
          use_heaRod=true,
          redeclare model PerDataMainHP =
              AixLib.DataBase.HeatPump.PerformanceData.LookUpTable2D (dataTable=
                 AixLib.DataBase.HeatPump.EN255.Vitocal350AWI114()),
          redeclare
            BESMod.Systems.Hydraulical.Interfaces.Generation.RecordsCollection.DefaultHP
            heatPumpParameters,
          redeclare
            BESMod.Systems.Hydraulical.Interfaces.Generation.RecordsCollection.DefaultHR
            heatingRodParameters,
          redeclare BESMod.Systems.RecordsCollection.Movers.DefaultMover
            pumpData,
          redeclare package Medium_eva = IBPSA.Media.Air,
          redeclare
            BESMod.Systems.RecordsCollection.TemperatureSensors.DefaultSensor
            temperatureSensorData));
       extends Modelica.Icons.Example;

      Modelica.Blocks.Sources.Constant     const1(k=0)
        annotation (Placement(transformation(extent={{-80,40},{-60,60}})));
      Modelica.Blocks.Sources.Pulse        pulse(period=1800)
        annotation (Placement(transformation(extent={{-40,80},{-20,100}})));
    equation
      connect(pulse.y, genControlBus.yHeaPumSet) annotation (Line(points={{-19,90},
              {-14,90},{-14,98},{10,98},{10,74}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
      connect(pulse.y, genControlBus.uPump) annotation (Line(points={{-19,90},{-14,
              90},{-14,98},{10,98},{10,74}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
      connect(const1.y, genControlBus.uHeaRod) annotation (Line(points={{-59,50},{
              -24,50},{-24,48},{10,48},{10,74}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
    end HeatPumpAndHeatingRod;

    model SolarThermalAndHeatPumpAndHeatingRod
      extends PartialTest(redeclare
          BESMod.Systems.Hydraulical.Interfaces.Generation.SolarThermalBivHPAixLib
          generation(
          redeclare model PerDataMainHP =
              AixLib.DataBase.HeatPump.PerformanceData.VCLibMap,
          redeclare
            BESMod.Systems.Hydraulical.Interfaces.Generation.RecordsCollection.DefaultHP
            heatPumpParameters,
          redeclare
            BESMod.Systems.Hydraulical.Interfaces.Generation.RecordsCollection.DefaultHR
            heatingRodParameters,
          redeclare BESMod.Systems.RecordsCollection.Movers.DefaultMover
            pumpData,
          redeclare package Medium_eva = IBPSA.Media.Air,
          redeclare
            BESMod.Systems.RecordsCollection.TemperatureSensors.DefaultSensor
            temperatureSensorData,
          redeclare
            BESMod.Systems.Hydraulical.Interfaces.Generation.RecordsCollection.DefaultSolarThermal
            solarThermalParas,
          redeclare BESMod.Systems.RecordsCollection.Movers.DefaultMover
            pumpSTData));
      extends Modelica.Icons.Example;

      Modelica.Blocks.Sources.Constant     const1(k=0)
        annotation (Placement(transformation(extent={{-100,40},{-80,60}})));
      Modelica.Blocks.Sources.Pulse        pulse(period=1800)
        annotation (Placement(transformation(extent={{-40,80},{-20,100}})));
    equation
      connect(pulse.y, genControlBus.yHeaPumSet) annotation (Line(points={{-19,90},
              {-14,90},{-14,98},{10,98},{10,74}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
      connect(pulse.y, genControlBus.uPump) annotation (Line(points={{-19,90},{-14,
              90},{-14,98},{10,98},{10,74}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
      connect(const1.y, genControlBus.uHeaRod) annotation (Line(points={{-79,50},{
              -32,50},{-32,48},{10,48},{10,74}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
    end SolarThermalAndHeatPumpAndHeatingRod;

    partial model PartialTest
      "Model for a partial test of hydraulic generation systems"
      extends BESMod.Systems.BaseClasses.PartialBESExample;

      BESMod.Systems.Hydraulical.Interfaces.GenerationControlBus
        genControlBus
        annotation (Placement(transformation(extent={{-10,54},{30,94}})));
      replaceable
        BESMod.Systems.Hydraulical.Interfaces.Generation.BaseClasses.PartialGeneration
        generation constrainedby
        BESMod.Systems.Hydraulical.Interfaces.Generation.BaseClasses.PartialGeneration(
        redeclare package Medium = IBPSA.Media.Water,
        energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
        dTTra_nominal=fill(10, generation.nParallelDem),
        m_flow_nominal=fill(0.317, generation.nParallelDem),
        Q_flow_nominal=fill(sum(systemParameters.QBui_flow_nominal), generation.nParallelDem),
        TOda_nominal=systemParameters.TOda_nominal,
        dpDem_nominal=fill(0, generation.nParallelDem),
        TDem_nominal=fill(systemParameters.THydSup_nominal[1], generation.nParallelDem),
        TAmb=systemParameters.TAmbHyd) annotation (choicesAllMatching=true,
          Placement(transformation(extent={{-50,-44},{24,28}})));

      IBPSA.Fluid.MixingVolumes.MixingVolume vol[generation.nParallelDem](
        redeclare final package Medium = IBPSA.Media.Water,
        each final energyDynamics=generation.energyDynamics,
        each final massDynamics=generation.massDynamics,
        each final p_start=generation.p_start,
        final T_start=fixedTemperature.T,
        each final X_start=generation.X_start,
        each final C_start=generation.C_start,
        each final C_nominal=generation.C_nominal,
        each final mSenFac=generation.mSenFac,
        final m_flow_nominal=generation.m_flow_nominal,
        final m_flow_small=1E-4*abs(generation.m_flow_nominal),
        each final allowFlowReversal=generation.allowFlowReversal,
        each V=23.5e-6*sum(systemParameters.QBui_flow_nominal),
        each final use_C_flow=false,
        each nPorts=2) annotation (Placement(transformation(
            extent={{-10,-10},{10,10}},
            rotation=270,
            origin={54,12})));
      Modelica.Thermal.HeatTransfer.Sources.FixedTemperature fixedTemperature[
        generation.nParallelDem](each final T(displayUnit="K") = systemParameters.THydSup_nominal[
          1])                                                      annotation (Placement(
            transformation(
            extent={{-10,-10},{10,10}},
            rotation=180,
            origin={90,8})));
      IBPSA.BoundaryConditions.WeatherData.ReaderTMY3 weaDat(final filNam=
            systemParameters.filNamWea)
        "Weather data reader"
        annotation (Placement(transformation(extent={{-102,66},{-66,100}})));
    equation
      connect(generation.sigBusGen, genControlBus) annotation (Line(
          points={{-12.26,27.28},{-12.26,49.64},{10,49.64},{10,74}},
          color={255,204,51},
          thickness=0.5), Text(
          string="%second",
          index=1,
          extent={{-3,6},{-3,6}},
          horizontalAlignment=TextAlignment.Right));
      for i in 1:generation.nParallelDem loop
      connect(generation.portGen_out[i], vol[i].ports[1]) annotation (Line(points={{24,20.8},
                {36,20.8},{36,6},{44,6},{44,13}},     color={0,127,255}));
      connect(generation.portGen_in[i], vol[i].ports[2])
        annotation (Line(points={{24,6.4},{44,6.4},{44,11}}, color={0,127,255}));
      end for;

      connect(vol.heatPort, fixedTemperature.port) annotation (Line(points={{54,22},
              {54,26},{76,26},{76,8},{80,8}}, color={191,0,0}));
      connect(weaDat.weaBus, generation.weaBus) annotation (Line(
          points={{-66,83},{-66,82},{-58,82},{-58,13.6},{-49.26,13.6}},
          color={255,204,51},
          thickness=0.5));

      annotation (experiment(
          StopTime=864000,
          Interval=600,
          __Dymola_Algorithm="Dassl"));
    end PartialTest;
  end Tests;

  package RecordsCollection "Record data for the options in this subsystem"
    extends Modelica.Icons.RecordsPackage;

    partial record HeatingRodBaseDataDefinition
      extends Modelica.Icons.Record;
      // Generation: Heating Rod
      parameter Real eta_hr "Heating rod efficiency";
      parameter Modelica.Units.SI.Volume V_hr
        "Volume to model thermal inertia of water";
      parameter Modelica.Units.SI.PressureDifference dp_nominal
        "Pressure difference";

      parameter Integer discretizationSteps(min=0) "Number of steps to dicretize. =0 modulating, =1 resembels an on-off controller. =2 would sample 0, 0.5 and 1";

      annotation (Icon(graphics,
                       coordinateSystem(preserveAspectRatio=false)), Diagram(graphics,
            coordinateSystem(preserveAspectRatio=false)));
    end HeatingRodBaseDataDefinition;

    record DefaultHR
      extends HeatingRodBaseDataDefinition(
        discretizationSteps=0,
        V_hr=0.001,
        eta_hr=0.97,
        dp_nominal(displayUnit="Pa") = 1000);
    end DefaultHR;

    partial record SolarThermalBaseDataDefinition
      extends Modelica.Icons.Record;
      parameter Modelica.Units.SI.Efficiency eta_zero=0.75
        "Conversion factor/Efficiency at Q = 0";
      parameter Real c1=2                   "Loss coefficient c1";
      parameter Real c2=0.005                 "Loss coefficient c2";
      parameter Modelica.Units.SI.Area A=2 "Area of solar thermal collector";

      parameter Modelica.Units.SI.Diameter dPipe "Diameter of the pip";
      parameter Modelica.Units.SI.Length spacing
        "Spacing of pipes / distance betwenn two pipes";
      parameter Modelica.Units.SI.SpecificHeatCapacityAtConstantPressure c_p=4184
        "Heat capacity of water";
      parameter Modelica.Units.SI.TemperatureDifference dTMax
        "Maximal temperature difference";
      parameter Modelica.Units.SI.Irradiance GMax
        "Maximal heat flow rate per area due to radation";

      parameter Real pressureDropCoeff=2500/(A*2.5e-5)^2
        "Pressure drop coefficient, delta_p[Pa] = PD * Q_flow[m^3/s]^2";
      parameter Modelica.Units.SI.MassFlowRate m_flow_nominal=GMax*A/(c_p*dTMax)
        "Nominal mass flow rate";

      parameter Modelica.Units.SI.Volume volPip=dPipe^2*Modelica.Constants.pi/4*A/
          spacing "Water volume of piping";

      annotation (Icon(graphics,
                       coordinateSystem(preserveAspectRatio=false)), Diagram(graphics,
            coordinateSystem(preserveAspectRatio=false)));
    end SolarThermalBaseDataDefinition;

    record DefaultSolarThermal "DummyParameters"
      extends
        BESMod.Systems.Hydraulical.Interfaces.Generation.RecordsCollection.SolarThermalBaseDataDefinition(
        GMax=1000,
        dTMax=35,
        spacing=0.1,
        dPipe=0.001,
        volPip=5e-3,
        m_flow_nominal=0.1);

    end DefaultSolarThermal;

    partial record HeatPumpBaseDataDefinition
      extends Modelica.Icons.Record;

      parameter Modelica.Units.SI.HeatFlowRate QGen_flow_nominal
        "Nominal heating load at outdoor air temperature"
        annotation (Dialog(group="Design"));

      // Temperature Levels
      parameter Modelica.Units.SI.Temperature TOda_nominal
        "Nominal outdoor air temperature" annotation (Dialog(group="Design"));
      parameter Modelica.Units.SI.Temperature THeaTresh "Heating treshhold"
        annotation (Dialog(group="Design"));
      parameter
        BESMod.Systems.Hydraulical.Interfaces.Generation.Types.GenerationDesign
        genDesTyp "Type of generation system design"
        annotation (Dialog(group="Design"));
      parameter Modelica.Units.SI.HeatFlowRate QPri_flow_nominal=if genDesTyp ==Types.GenerationDesign.Monovalent
                                                                           then
          QGen_flow_nominal else QGenBiv_flow_nominal
        "Nominal heat flow rate of primary generation component (e.g. heat pump)"
        annotation (Dialog(group="Design"));
      parameter Modelica.Units.SI.HeatFlowRate QSec_flow_nominal=if genDesTyp ==Types.GenerationDesign.Monovalent
                                                                           then 0
           elseif genDesTyp ==Types.GenerationDesign.BivalentAlternativ
           then QGen_flow_nominal elseif genDesTyp ==Types.GenerationDesign.BivalentParallel
           then QGen_flow_nominal - QGenBiv_flow_nominal else QGen_flow_nominal
        "Nominal heat flow rate of secondary generation component (e.g. auxilliar heater)"
        annotation (Dialog(group="Design"));
      parameter Modelica.Units.SI.Temperature TBiv=TOda_nominal
        "Nominal bivalence temperature. = TOda_nominal for monovalent systems."
        annotation (Dialog(enable=genDesTyp <> Types.GenerationDesign.Monovalent,
            group="Design"));

      parameter Modelica.Units.SI.HeatFlowRate QGenBiv_flow_nominal=
          QGen_flow_nominal*(TBiv - THeaTresh)/(TOda_nominal - THeaTresh)
        "Nominal heat flow rate at bivalence temperature"
        annotation (Dialog(group="Design"));

      // Generation: Heat Pump
      parameter Real scalingFactor=1 "Scaling-factor of vapour compression machine";
      parameter Modelica.Units.SI.MassFlowRate mEva_flow_nominal=1
        "Mass flow rate through evaporator";
      parameter Modelica.Units.SI.Volume VEva=0.004
        "Manual input of the evaporator volume (if not automatically calculated)";
      parameter Modelica.Units.SI.Volume VCon=0.001
        "Manual input of the condenser volume";
      parameter Modelica.Units.SI.PressureDifference dpCon_nominal=1000
        "Pressure difference";
      parameter Modelica.Units.SI.PressureDifference dpEva_nominal=1000
        "Pressure difference";
       parameter Boolean use_refIne=false
        "Consider the inertia of the refrigerant cycle";
      parameter Modelica.Units.SI.Frequency refIneFre_constant=0
        "Cut off frequency for inertia of refrigerant cycle";
      annotation (Icon(graphics,
                       coordinateSystem(preserveAspectRatio=false)), Diagram(graphics,
            coordinateSystem(preserveAspectRatio=false)));
    end HeatPumpBaseDataDefinition;

    record DefaultHP
      extends HeatPumpBaseDataDefinition(genDesTyp=BESMod.Systems.Hydraulical.Interfaces.Generation.Types.GenerationDesign.BivalentAlternativ,
          THeaTresh=293.15);

    end DefaultHP;
  end RecordsCollection;

  package Types "Package with types like enumartions or similar"

    type GenerationDesign = enumeration(
        Monovalent "Monovalent",
        BivalentAlternativ "Bivalent alternativ",
        BivalentParallel "Bivalent parallel",
        BivalentPartParallel "Bivalent partly parallel")
      "Choose between different design options for primary generation";
  end Types;

  package BaseClasses "Base class with partial generation models"
    partial model PartialGeneration "Partial generation model for HPS"
      extends BESMod.Utilities.Icons.GenerationIcon;
      extends BESMod.Systems.BaseClasses.PartialFluidSubsystemWithParameters(
        final TSup_nominal=TDem_nominal .+ dTLoss_nominal,
        m_flow_nominal=Q_flow_nominal .* f_design ./ dTTra_nominal ./ 4184,                final
          nParallelSup=nParallelDem);

      parameter Modelica.Units.SI.PressureDifference dpDem_nominal[nParallelDem]
        "Nominal pressure loss of resistances in the demand system of the generation"
        annotation (Dialog(group=
              "Design - Top Down: Parameters are given by the parent system"));

      BESMod.Systems.Hydraulical.Interfaces.GenerationControlBus
        sigBusGen
        annotation (Placement(transformation(extent={{-18,78},{22,118}})));
      Modelica.Fluid.Interfaces.FluidPort_b portGen_out[nParallelDem](redeclare
          final package Medium = Medium) "Outlet of the generation" annotation (
          Placement(transformation(extent={{90,70},{110,90}}), iconTransformation(
              extent={{90,70},{110,90}})));
      Modelica.Fluid.Interfaces.FluidPort_a portGen_in[nParallelDem](redeclare
          final package Medium = Medium) "Inlet to the generation" annotation (
          Placement(transformation(extent={{90,-12},{110,8}}), iconTransformation(
              extent={{90,30},{110,50}})));
      BESMod.Systems.Hydraulical.Interfaces.GenerationOutputs
        outBusGen if not use_openModelica
        annotation (Placement(transformation(extent={{-10,-110},{10,-90}})));

      IBPSA.BoundaryConditions.WeatherData.Bus
          weaBus "Weather data bus" annotation (Placement(transformation(extent={{-122,58},
                {-80,102}}),         iconTransformation(extent={{-108,50},{-88,
                70}})));
      BESMod.Systems.Electrical.Interfaces.InternalElectricalPinOut
        internalElectricalPin
        annotation (Placement(transformation(extent={{62,-110},{82,-90}})));
      annotation (Icon(graphics,
                       coordinateSystem(preserveAspectRatio=false)), Diagram(graphics,
            coordinateSystem(preserveAspectRatio=false)));
    end PartialGeneration;
  annotation (Icon(graphics={
          Rectangle(
            lineColor={200,200,200},
            fillColor={248,248,248},
            fillPattern=FillPattern.HorizontalCylinder,
            extent={{-100.0,-100.0},{100.0,100.0}},
            radius=25.0),
          Rectangle(
            lineColor={128,128,128},
            extent={{-100.0,-100.0},{100.0,100.0}},
            radius=25.0),
          Ellipse(
            extent={{-30.0,-30.0},{30.0,30.0}},
            lineColor={128,128,128},
            fillColor={255,255,255},
            fillPattern=FillPattern.Solid)}));
  end BaseClasses;
end Generation;
