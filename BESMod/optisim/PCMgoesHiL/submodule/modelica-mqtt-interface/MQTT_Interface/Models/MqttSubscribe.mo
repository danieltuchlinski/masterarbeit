within MQTT_Interface.Models;
block MqttSubscribe

  extends Modelica_DeviceDrivers.Utilities.Icons.BaseIcon;
  extends Modelica_DeviceDrivers.Utilities.Icons.MQTTconnection;

  import Modelica_DeviceDrivers.Communication.MQTT;
  import Modelica_DeviceDrivers.Utilities.Functions.getMACAddress;

  parameter Integer numberOfValues = 2
    "Number of values that are serialized in payload string";

  parameter String delimiter = "," "Delimiter that separates values in payload";

  parameter Modelica.Units.SI.Time startTime=0.0 "Time to start sampling";
  parameter Modelica.Units.SI.Period sampleTime=0.1 "Sample time";

  parameter String IPAddress = "localhost" "IP address";
  parameter Integer port = 1883 "Port";
  parameter String topic = "someTopic" "Channel name";
  parameter Integer deliveryTimeout = 10 "Delivery timeout (in seconds)";
  parameter Boolean retained = false "Retained flag" annotation(Dialog(choices(checkBox=true)));
  parameter String clientID = "read-" + getMACAddress() "Unique client identifier";
  parameter Integer QoS(min=0, max=2) = 1 "Quality of service";
  parameter Boolean reliable = true "=true, if a published message must be completed (acknowledgements received) before another message can be sent";
  parameter String userName = "" "User name for authentication and authorisation";
  parameter String password = "" "Password for authentication and authorisation";
  parameter String valueEmptyTopic = "75.0,-3.0"
    "Value that is used if there is no retained message in target topic";

  Modelica.Blocks.Interfaces.RealOutput y[numberOfValues]
    annotation (Placement(transformation(extent={{100,-10},{120,10}})));

protected
  parameter Boolean receiver = true "Set to be a receiver port";

  MQTT mqtt = MQTT(  provider = "tcp://",
               address = IPAddress,
               port = port,
               receiver = receiver,
               channel = topic,
               QoS = QoS,
               reliable = reliable,
               userName = userName,
               password = password,
               clientID = clientID);

initial equation

  y = fill(0.0, numberOfValues);

equation

  when sample(startTime,sampleTime) then
    y =MQTT_Interface.Functions.callMqttSubscribe(
      mqtt=mqtt,
      valueEmptyTopic=valueEmptyTopic,
      numberOfValues=numberOfValues,
      delimiter=delimiter);
  end when;
  annotation(defaultComponentName="mqttSubscribe",
          Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics={Text(extent={{-150,136},{150,96}},
            textString="%name")}), Documentation(info="<html>
</html>"));
end MqttSubscribe;
