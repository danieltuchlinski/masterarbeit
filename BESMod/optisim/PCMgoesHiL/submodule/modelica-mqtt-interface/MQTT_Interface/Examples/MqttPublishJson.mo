﻿within MQTT_Interface.Examples;
model MqttPublishJson
  "An example where the message payload is a json string of subtopic keys and their values"

  extends Modelica.Icons.Example;

  MQTT_Interface.Models.MqttPublish mqttSend(
    sampleTime=1,
    IPAddress="137.226.248.130",
    topic={"HiL2/Klimakammer","HiL2/Tür"},
    useJSON=true,
    nKeys={2,2},
    keys={"RealVal","BoolVal","RealVal","BoolVal"})    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={68,8})));
        //(or use SPS IP) 137.226.248.130

  Modelica.Blocks.Sources.RealExpression DummyReal(y=56.91)
    annotation (Placement(transformation(extent={{-66,30},{-46,50}})));
  Modelica.Blocks.Sources.BooleanExpression DummyBool
    annotation (Placement(transformation(extent={{-66,-34},{-46,-14}})));
  Modelica.Blocks.Math.BooleanToReal booleanToReal
    annotation (Placement(transformation(extent={{-26,-34},{-6,-14}})));
  Modelica.Blocks.Sources.RealExpression DummyReal1(y=2.91)
    annotation (Placement(transformation(extent={{-64,14},{-44,34}})));
  Modelica.Blocks.Sources.BooleanExpression DummyBool1(y=true)
    annotation (Placement(transformation(extent={{-2,-86},{18,-66}})));
  Modelica.Blocks.Math.BooleanToReal booleanToReal1
    annotation (Placement(transformation(extent={{38,-86},{58,-66}})));
equation

  connect(DummyReal.y, mqttSend.u[1]) annotation (Line(points={{-45,40},{-16,40},
          {-16,8},{56,8}},  color={0,0,127}));
  connect(DummyBool.y, booleanToReal.u)
    annotation (Line(points={{-45,-24},{-28,-24}}, color={255,0,255}));
  connect(booleanToReal.y, mqttSend.u[2])
    annotation (Line(points={{-5,-24},{2,-24},{2,8},{56,8}},color={0,0,127}));
  connect(DummyReal1.y, mqttSend.u[3]) annotation (Line(points={{-43,24},{12,24},
          {12,8},{56,8}}, color={0,0,127}));
  connect(DummyBool1.y, booleanToReal1.u)
    annotation (Line(points={{19,-76},{36,-76}}, color={255,0,255}));
  connect(booleanToReal1.y, mqttSend.u[4]) annotation (Line(points={{59,-76},{68,
          -76},{68,-72},{50,-72},{50,8},{56,8}},             color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    experiment(StopTime=30, __Dymola_Algorithm="Dassl"));
end MqttPublishJson;
