within MQTT_Interface.Examples;
model MqttSubscribeJson
  "(WIP) An example where the message payload is a json string of subtopic keys and their values"

  extends Modelica.Icons.Example;

  parameter String topics[:] = {"hil1/HYD.Bank[1].TempOutA.RealVar", "hil1/HYD.Bank[1].TempOutB.RealVar"};

  MQTT_Interface.Models.MqttSubscribe mqttSubscribe[size(topics, 1)](
    each numberOfValues=1,
    each IPAddress="137.226.248.130",
    topic=topics,
    clientID=topics,
    each valueEmptyTopic="0")
    annotation (Placement(transformation(extent={{-28,6},{-8,26}})));

end MqttSubscribeJson;
