within MQTT_Interface.Examples;
model MqttSubscribe

  extends Modelica.Icons.Example;

  parameter String topics[:] = {"hil2/KK.Turauf","hil2/KK.Soll_Temp_KK","hil2/KK.Temp_KK",
                                "hil2/Leistung.Messung_5.Gesamtleistung", "hil2/Leistung.Messung_1.L2_Spannung", "hil2/Leistung.Messung_1.L1_Spannung",
                                "hil2/KK.xNachWarmeubertrager","hil2/KK.xKimakammerSoll","hil2/KK.xKimakammerIst",
                                "hil2/KK.Vdot_VS_Warmwasser.REAL_VAR", "hil2/KK.Vdot_VS_Kaltwasser.REAL_VAR","hil2/KK.Vdot_KK_Kalte.REAL_VAR",
                                "hil2/KK.Vdot_KK_Auslass_UR.REAL_VAR","hil2/KK.Temp_KK_Kalte_VL.REAL_VAR", "hil2/KK.Temp_KK_Kalte_RL.REAL_VAR",
                                "hil2/KK.starkeEntfeuchtung","hil2/KK.Soll_Vdot_KK","hil2/KK.Soll_Temp_KK_Kalte_VL",
                                "hil2/KK.Soll_rlFeuchte_KK","hil2/KK.Soll_KK_Ventilator.REAL_VAR","hil2/KK.rlFeuchte_KK"};

  MQTT_Interface.Models.MqttSubscribe mqttSubscribe[size(topics, 1)](
    each numberOfValues=1,
    each IPAddress="broker.mqttdashboard.com",
    topic=topics,
    clientID=topics,
    each valueEmptyTopic="0")
    annotation (Placement(transformation(extent={{-28,6},{-8,26}})));

end MqttSubscribe;
