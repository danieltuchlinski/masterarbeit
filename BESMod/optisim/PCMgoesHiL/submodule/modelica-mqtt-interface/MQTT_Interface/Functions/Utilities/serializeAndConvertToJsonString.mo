within MQTT_Interface.Functions.Utilities;
function serializeAndConvertToJsonString "Takes an array of keys and an array of variable then converts them into a JSON dictionary string"

  input String[:] keys "Array of variable keys for the JSON dict";
  input Integer[:] nKeys "Number of keys of each topoc";
  input Real[:] u "Array of variable values for the JSON dict";
  output String [size(nKeys,1)] jsonOut "Converted String";


protected
  Integer numberOfValues = size(u, 1) "Number of entries";
  Integer i "Index";
  String prefix = "{";
  String col_delimiter = ":";
  String row_delimiter = ",";
  String postfix = "}";
  Integer keyCounter = 0;

algorithm


  for n in 1:size(nKeys,1) loop
    jsonOut[n] := prefix;
    for i in 1:(nKeys[n]-1) loop
      jsonOut[n] := jsonOut[n] + "\"" + keys[i+keyCounter] + "\"" + col_delimiter + String(u[i+keyCounter]) + row_delimiter;
    end for;
  jsonOut[n] := jsonOut[n] + "\"" + keys[nKeys[n]+keyCounter] + "\"" + col_delimiter + String(u[nKeys[n]+keyCounter]) + postfix;
  keyCounter := keyCounter + nKeys[n];
  end for;

  annotation (Documentation(info="<html>
<p>The function converts a 2D String-array into a String fromatted as a JSON dictionary. One purpose is to send the values from a simulation via MQTT summarized in one string and one publish process.</p>
<p><b>Example</b></p>
<p><b>Input: </b></p>
<p style=\"margin-left: 30px;\">{{&quot;Real_Result_1&quot;, &quot;9.0&quot;},</p>
<p style=\"margin-left: 30px;\">{&quot;Bool_Result&quot;, &quot;true&quot;}</p>
<p style=\"margin-left: 30px;\">{&quot;Real_Result_2&quot;, &quot;-0.7&quot;}}</p>
<p><b>Output:</b></p>
<p style=\"margin-left: 30px;\"> &quot;{&quot;Real_Result_1&quot;: &quot;9.0&quot;,</p>
<p style=\"margin-left: 30px;\">&quot;Bool_Result&quot;: &quot;true&quot;,</p>
<p style=\"margin-left: 30px;\">&quot;Real_Result_2&quot;: &quot;-0.7&quot;}&quot;</p>
</html>"));
end serializeAndConvertToJsonString;
