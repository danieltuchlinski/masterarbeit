within MQTT_Interface.Functions.Utilities;
model BooleanPassThroughConditional "Pass a Boolean signal through if existent"
  extends Modelica.Blocks.Icons.BooleanBlock;
  parameter Boolean passThrough = true "If true output y is input u, else Boolean constant" annotation(Evaluate=true);
  parameter Boolean constU = true "Constant output, if not passed through"  annotation(Dialog(enable=not passThrough));

  Modelica.Blocks.Interfaces.BooleanInput u if passThrough "Connector of Boolean input signal" annotation (Placement(
        transformation(extent={{-140,-20},{-100,20}})));
  Modelica.Blocks.Interfaces.BooleanOutput y "Connector of Boolean output signal" annotation (Placement(
        transformation(extent={{100,-10},{120,10}})));
protected
  Modelica.Blocks.Interfaces.BooleanInput u_internal
    "Needed to connect to conditional connector";
equation
  connect(u, u_internal);
  if passThrough then
    y = u_internal;
  else
    u_internal = false; // Dummy
    y = constU;
  end if;

  annotation (Documentation(info="<html>
<p>Passes a Boolean signal through without modification.  Enables signals to be read out of one bus, have their name changed and be sent back to a bus.</p>
</html>"),
    Icon(
      coordinateSystem(preserveAspectRatio=true,
          extent={{-100.0,-100.0},{100.0,100.0}}),
          graphics={
      Line(
        points={{-100.0,0.0},{100.0,0.0}},
        color={255,0,255})}));
end BooleanPassThroughConditional;
