within MQTT_Interface.Functions.BaseClasses;
function mqttPublishWrapper

  extends Modelica.Icons.Function;

  input Modelica_DeviceDrivers.Communication.MQTT mqtt;
  input String topic "Topic name";
  input String message "Payload";
  input Boolean retained "Retained flag";
  input Integer deliveryTimeout "Delivery timeout";
  input Integer dataSize "Number of bytes";

  external "C" MDD_mqttSend(mqtt, topic, message, retained, deliveryTimeout, dataSize)
    annotation (
      Include = "#include \"MDDMQTT.h\"",
      Library = {"paho-mqtt3cs", "pthread", "ssl", "crypto"},
      LibraryDirectory = "modelica://Modelica_DeviceDrivers/Resources/Library",
      __iti_dll = "ITI_MDDMQTT.dll",
      __iti_dllNoExport = true);

end mqttPublishWrapper;
