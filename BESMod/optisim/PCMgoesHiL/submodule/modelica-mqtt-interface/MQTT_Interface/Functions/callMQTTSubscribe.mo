within MQTT_Interface.Functions;
function callMqttSubscribe

  extends Modelica.Icons.Function;

  input Modelica_DeviceDrivers.Communication.MQTT mqtt;
  input String valueEmptyTopic;
  input Integer numberOfValues;
  input String delimiter;
  output Real valuesFromTopic[numberOfValues];

protected
  String message;
  String messageReturn;

algorithm

  message :=MQTT_Interface.Functions.BaseClasses.mqttSubscribeWrapper(mqtt);

  if Modelica.Utilities.Strings.isEmpty(message) then
    messageReturn := valueEmptyTopic;
  else
    messageReturn := message;
  end if;

  valuesFromTopic :=Utilities.splitAndConvertToReal(
    serializedData=messageReturn,
    delimiter=delimiter,
    numberOfValues=numberOfValues);

end callMqttSubscribe;
