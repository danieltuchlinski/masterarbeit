within MQTT_Interface.Functions;
function callMqttPublish

  extends Modelica.Icons.Function;

  input Modelica_DeviceDrivers.Communication.MQTT mqtt;
  input String topic[:] "Topic name";
  input Boolean useJSON "If true, message is formatted as JSON dict";
  input String keys[:] "Keys (if JSON format message is chosen)";
  input Integer nKeys[:] "Number of keys for each topic";
  input Real message[:] "Payload";
  input Boolean retained "Retained flag";
  input Integer deliveryTimeout "Delivery timeout";
protected
  Integer numberOfValues = size(nKeys, 1);
  String stringMessage[size(nKeys, 1)];

algorithm
  if useJSON then
    stringMessage :=
      MQTT_Interface.Functions.Utilities.serializeAndConvertToJsonString(
      keys,
      nKeys,
      message);
  else
    for i in 1:(numberOfValues) loop
    stringMessage[i] := String(message[i]);
     end for;
  end if;

     for i in 1:(numberOfValues) loop
    MQTT_Interface.Functions.BaseClasses.mqttPublishWrapper(
      mqtt,
      topic[i],
      stringMessage[i],
      retained,
      deliveryTimeout,
      Modelica.Utilities.Strings.length(stringMessage[i]));
     end for;


end callMqttPublish;
