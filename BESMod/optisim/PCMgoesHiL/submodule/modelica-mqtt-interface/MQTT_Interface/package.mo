within ;
package MQTT_Interface
  annotation (version="0.0.2", uses(
      Modelica_DeviceDrivers(version="2.0.0"), Modelica(version="4.0.0")));
end MQTT_Interface;
