within MQTT_Interface.MQTTConnectionPara;
record TickStackOpenstack
  extends BasisForMQTTConnection(server=
        "hil.ebc-team-kap.osc.eonerc.rwth-aachen.de");
end TickStackOpenstack;
