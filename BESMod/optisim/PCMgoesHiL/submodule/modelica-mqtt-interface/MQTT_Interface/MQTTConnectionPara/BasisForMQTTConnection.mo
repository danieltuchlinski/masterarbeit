within MQTT_Interface.MQTTConnectionPara;
record BasisForMQTTConnection
  extends Modelica.Icons.Record;
  parameter String server="broker.mqttdashboard.com";
  parameter Integer port = 1883;
  parameter String user = "UserName";
  parameter String password = "PasswordForBroker";
  parameter Integer QoS(min=0, max=2)=0 "Quality of service";
end BasisForMQTTConnection;
