within PCMgoesHIL.Test;
model TestDHWNC
  extends Modelica.Icons.Example;
  Utilities.DHW.NC_obsolet.DHWCalcNC dHWCalcNC(fileName_scheduleDHW=
        "D:/04_Git/ebc0888_mitsubishi_pcmgoeshil_kap/Modelica/Resources/dhw_day.nc")
    annotation (Placement(transformation(extent={{-18,6},{2,26}})));
  BESMod.Systems.Demand.DHW.RecordsCollection.ProfileL profileL
    annotation (Placement(transformation(extent={{66,-98},{86,-76}})));
  Modelica.Blocks.Sources.Constant mFlowMes(k=200/3600)
    annotation (Placement(transformation(extent={{-90,18},{-70,38}})));
  Modelica.Blocks.Sources.Constant TDHWMes(k=273.15 + 50)
    annotation (Placement(transformation(extent={{-88,-36},{-68,-16}})));
  Modelica.Blocks.Sources.Constant TDHWColdMes(k=273.15 + 10)
    annotation (Placement(transformation(extent={{-82,-80},{-62,-60}})));
  Modelica.Blocks.Interfaces.RealOutput m_flow_setNC
    annotation (Placement(transformation(extent={{90,8},{110,28}})));
equation
  connect(mFlowMes.y, dHWCalcNC.mFlowMes) annotation (Line(points={{-69,28},{
          -26,28},{-26,22.4},{-18,22.4}}, color={0,0,127}));
  connect(TDHWMes.y, dHWCalcNC.TDHWMes) annotation (Line(points={{-67,-26},{-60,
          -26},{-60,-20},{-50,-20},{-50,16},{-18,16}}, color={0,0,127}));
  connect(TDHWColdMes.y, dHWCalcNC.TColdMes) annotation (Line(points={{-61,-70},
          {-50,-70},{-50,-72},{-28,-72},{-28,9.4},{-18,9.4}}, color={0,0,127}));
  connect(dHWCalcNC.mFlowDHWTank, m_flow_setNC) annotation (Line(points={{3,20},
          {86,20},{86,18},{100,18}}, color={0,0,127}));
  annotation (experiment(StopTime=86400, __Dymola_Algorithm="Dassl"));
end TestDHWNC;
