within PCMgoesHIL.UseCaseTypDays;
model BES_HIL_ReducedOrder
  extends BaseClasses.PartialHiL_BES(
    redeclare PCMgoesHIL.Systems.RecordCollection.HOMSystem systemParameters(
      nZones=1,
      QBui_flow_nominal={5942.07},
      TSetZone_nominal={293.15}),
    redeclare BESMod.Systems.Demand.Building.TEASERThermalZone building(
        redeclare BESMod.Examples.BAUSimStudy.Buildings.Case_1_retrofit
        oneZoneParam),
    redeclare BESMod.Systems.UserProfiles.TEASERProfiles userProfiles);

  annotation (experiment(
      StopTime=31536000,
      Interval=600,
      __Dymola_Algorithm="Dassl"));
end BES_HIL_ReducedOrder;
