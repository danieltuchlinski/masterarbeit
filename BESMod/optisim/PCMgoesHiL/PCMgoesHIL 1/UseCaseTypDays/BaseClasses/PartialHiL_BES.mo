within PCMgoesHIL.UseCaseTypDays.BaseClasses;
model PartialHiL_BES
  extends BESMod.Systems.BaseClasses.PartialBuildingEnergySystem(
    redeclare PCMgoesHIL.Systems.RecordCollection.HOMSystem systemParameters(filNamWea
        =Modelica.Utilities.Files.loadResource(
          "modelica://PCMgoesHIL/Resources/WeatherData/TRY2015_523938130651_Jahr_City_Potsdam.mos")),
    redeclare BESMod.Systems.Electrical.DirectGridConnectionSystem electrical,
    redeclare BESMod.Systems.Control.NoControl control,
    redeclare BESMod.Systems.Hydraulical.HydraulicSystem hydraulic(
      redeclare BESMod.Systems.Hydraulical.Generation.HeatPumpAndHeatingRod
        generation(
        m_flow_nominal=hydraulic.transfer.mSup_flow_nominal,
        redeclare PCMgoesHIL.Systems.RecordCollection.Movers.EcodanWaterPump pumpData(dp_nominal=
              hydraulic.generation.dpDem_nominal[1] + hydraulic.generation.dp_nominal[
              1], m_flow_nominal=hydraulic.generation.m_flow_nominal[1]),
        redeclare package Medium_eva = AixLib.Media.Air,
        use_pressure=true,
        redeclare
          PCMgoesHIL.Systems.Hydraulic.Generation.RecordCollection.EcodanParameters
          heatPumpParameters,
        redeclare
          BESMod.Systems.Hydraulical.Generation.RecordsCollection.DefaultHR
          heatingRodParameters,
        redeclare model PerDataMainHP =
            AixLib.DataBase.HeatPump.PerformanceData.LookUpTable2D (dataTable=
                PCMgoesHIL.Systems.Hydraulic.Generation.RecordCollection.Ecodan_PUZ_WM60VAA(),
              extrapolation=false)),
      redeclare PCMgoesHIL.Systems.Hydraulic.Control.MitsubishiControl control(
        redeclare
          PCMgoesHIL.Systems.Hydraulic.Control.RecordsCollection.ThermostaticValveHILDataDefinition
          thermostaticValveHILDataDefinition,
        redeclare
          BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultBivHPControl
          bivalentControlData(dTHysBui=2, k=0.01),
        redeclare
          BESMod.Systems.Hydraulical.Control.Components.DHWSetControl.ConstTSet_DHW
          TSet_DHW,
        redeclare
          BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultSafetyControl
          safetyControl,
        redeclare
          BESMod.Systems.Hydraulical.Control.Components.HeatPumpNSetController.PI_InverterHeatPumpController
          HP_nSet_Controller),
      redeclare
        PCMgoesHIL.Systems.Hydraulic.Distribution.MitsubishiDistributionSystem
        distribution(
        dpDem_nominal={hydraulic.transfer.transferDataBaseDefinition.dpPumpHeaCir_nominal},
        redeclare BESMod.Systems.RecordsCollection.Movers.DefaultMover pumpData,
        redeclare
          BESMod.Systems.RecordsCollection.TemperatureSensors.DefaultSensor
          temperatureSensorData,
        redeclare BESMod.Systems.RecordsCollection.Valves.DefaultThreeWayValve
          threeWayValveParameters,
        redeclare
          BESMod.Systems.Hydraulical.Distribution.RecordsCollection.BufferStorage.DefaultDetailedStorage
          dhwParameters,
        V=0.2),
      redeclare PCMgoesHIL.Systems.Hydraulic.Transfer.RadiatorTransferSystemHIL
        transfer(
        redeclare
          BESMod.Systems.Hydraulical.Transfer.RecordsCollection.RadiatorTransferData
          radParameters,
        redeclare
          BESMod.Systems.RecordsCollection.TemperatureSensors.DefaultSensor
          sensorData,
        redeclare
          BESMod.Systems.Hydraulical.Transfer.RecordsCollection.SteelRadiatorStandardPressureLossData
          transferDataBaseDefinition,
        dpFullOpen_nominal(displayUnit="Pa") = 73853.65852*0.8)),
    redeclare BESMod.Systems.Demand.DHW.DHW DHW(
      redeclare BESMod.Systems.Demand.DHW.RecordsCollection.ProfileM DHWProfile,
      redeclare BESMod.Systems.RecordsCollection.Movers.DefaultMover pumpData,
      redeclare BESMod.Systems.Demand.DHW.TappingProfiles.calcmFlowEquStatic
        calcmFlow),
    redeclare
      PCMgoesHIL.Systems.RecordCollection.ParameterStudy.ParametersToChange
      parameterStudy,
    redeclare final package MediumDHW = AixLib.Media.Water,
    redeclare final package MediumZone = AixLib.Media.Air,
    redeclare final package MediumHyd = AixLib.Media.Water,
    redeclare BESMod.Systems.Ventilation.NoVentilation ventilation);

    extends Modelica.Icons.Example;
  annotation (experiment(
      StopTime=31536000,
      Interval=600,
      __Dymola_Algorithm="Dassl"));
end PartialHiL_BES;
