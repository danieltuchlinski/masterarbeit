within PCMgoesHIL.Utilities.DHW;
model CalcDHWmFlow
  Modelica.Blocks.Math.Add diffTempNum(k2=-1)
    annotation (Placement(transformation(extent={{-22,-24},{-14,-16}})));
  Modelica.Blocks.Math.Add diffTempDen(k2=-1)
    annotation (Placement(transformation(extent={{-22,-36},{-14,-28}})));
  Modelica.Blocks.Nonlinear.Limiter limiter(uMax=100, uMin=Modelica.Constants.eps)
    annotation (Placement(transformation(extent={{-10,-32},{-4,-26}})));
  Modelica.Blocks.Math.Division division
    annotation (Placement(transformation(extent={{0,-28},{8,-20}})));
  Modelica.Blocks.Math.Product product
    annotation (Placement(transformation(extent={{14,-24},{22,-16}})));
  VariableLimiterWarningOnly               variableLimiter
    annotation (Placement(transformation(extent={{42,-6},{54,6}})));
  Modelica.Blocks.Sources.Constant mFlowZero(k=0, y(unit="kg/s"))
    annotation (Placement(transformation(extent={{28,-28},{32,-24}})));
  Modelica.Blocks.Interfaces.RealInput TTankDHWHotMes
    "hot temperature of water that is withdrawn from the DHW tank (measured value)"
    annotation (Placement(transformation(rotation=0, extent={{-112,-30},{-92,
            -10}})));
  Modelica.Blocks.Interfaces.RealInput Tp
    "top temperature that has to be reached acc. to table of EN 16147"
    annotation (Placement(transformation(rotation=0, extent={{-110,10},{-90,30}})));
  Modelica.Blocks.Interfaces.RealInput TCold
    "temperature of district water (around 10 degC)" annotation (Placement(
        transformation(rotation=0, extent={{-110,-94},{-90,-74}})));
  Modelica.Blocks.Interfaces.RealInput mFlowFromTable
    "mFlowSet at the tapping outlet" annotation (Placement(transformation(
          rotation=0, extent={{-110,70},{-90,90}})));
  Modelica.Blocks.Interfaces.RealOutput mFlowCalcedDHWSet
    "mFlow that should be set for DHW when there is a demand" annotation (
      Placement(transformation(rotation=0, extent={{90,-10},{110,10}})));
  TriggerQ trigger_m_flow(x=0)
    annotation (Placement(transformation(extent={{-60,52},{-40,72}})));
  TriggerQ trigger_temp(x=tempThr)
    annotation (Placement(transformation(extent={{-74,10},{-54,30}})));
  parameter Real tempThr=273.15 + 10 "Threshold defines if to change values";
equation
  connect(division.y, product.u2) annotation (Line(points={{8.4,-24},{10,-24},{10,
          -22.4},{13.2,-22.4}}, color={0,0,127}));
  connect(diffTempNum.y, division.u1) annotation (Line(points={{-13.6,-20},{-10,
          -20},{-10,-21.6},{-0.8,-21.6}}, color={0,0,127}));
  connect(diffTempDen.y, limiter.u) annotation (Line(points={{-13.6,-32},{-10.6,
          -32},{-10.6,-29}}, color={0,0,127}));
  connect(limiter.y, division.u2) annotation (Line(points={{-3.7,-29},{-3.7,-27.5},
          {-0.8,-27.5},{-0.8,-26.4}}, color={0,0,127}));
  connect(mFlowZero.y, variableLimiter.limit2) annotation (Line(points={{32.2,-26},{36,-26},{36,-4.8},{40.8,-4.8}}, color={0,0,127}));
  connect(product.y, variableLimiter.u) annotation (Line(points={{22.4,-20},{30,-20},{30,0},{40.8,0}}, color={0,0,127}));
  connect(TTankDHWHotMes, diffTempDen.u1) annotation (Line(points={{-102,-20},{
          -62,-20},{-62,-29.6},{-22.8,-29.6}}, color={0,0,127}));
  connect(TCold, diffTempDen.u2) annotation (Line(points={{-100,-84},{-62,-84},
          {-62,-34.4},{-22.8,-34.4}}, color={0,0,127}));
  connect(TCold, diffTempNum.u2) annotation (Line(points={{-100,-84},{-62,-84},
          {-62,-22.4},{-22.8,-22.4}}, color={0,0,127}));
  connect(mFlowCalcedDHWSet, mFlowCalcedDHWSet)
    annotation (Line(points={{100,0},{100,0}}, color={0,0,127}));
  connect(variableLimiter.y, mFlowCalcedDHWSet) annotation (Line(points={{54.6,0},{72.25,0},{100,0}}, color={0,0,127}));
  connect(trigger_m_flow.u, mFlowFromTable) annotation (Line(points={{-62,62},{
          -78,62},{-78,80},{-100,80}}, color={0,0,127}));
  connect(trigger_m_flow.y, variableLimiter.limit1) annotation (Line(points={{
          -39,62},{-4,62},{-4,4.8},{40.8,4.8}}, color={0,0,127}));
  connect(product.u1, trigger_m_flow.y) annotation (Line(points={{13.2,-17.6},{
          -4,-17.6},{-4,62},{-39,62}}, color={0,0,127}));
  connect(Tp, trigger_temp.u)
    annotation (Line(points={{-100,20},{-76,20}}, color={0,0,127}));
  connect(trigger_temp.y, diffTempNum.u1) annotation (Line(points={{-53,20},{
          -34,20},{-34,-17.6},{-22.8,-17.6}}, color={0,0,127}));
end CalcDHWmFlow;
