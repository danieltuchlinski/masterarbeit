within PCMgoesHIL.Utilities.DHW.NC_obsolet;
model IntegerCounterAndBooleanTrigger

  parameter Integer pre_int_in_start=0 "Start value of pre(int_in) at initial time";
  parameter Integer pre_counter_start=0 "Start value of pre(counter) at initial time";

  Modelica.Blocks.Interfaces.IntegerInput int_in
    annotation (Placement(transformation(extent={{-126,40},{-86,80}})));

  Modelica.Blocks.Interfaces.BooleanOutput trigger
    annotation (Placement(transformation(extent={{96,-10},{116,10}})));
  Modelica.Blocks.Interfaces.IntegerOutput counter
    annotation (Placement(transformation(extent={{96,50},{116,70}})));
initial equation
  pre(int_in)=pre_int_in_start;
  pre(counter)=pre_counter_start;
equation
  when pre(int_in)<>int_in then
    counter =  pre(counter)+1;
  end when;
  if counter == pre(counter) then
    trigger=false;
  else
    trigger = true;
  end if;
  annotation ();
end IntegerCounterAndBooleanTrigger;
