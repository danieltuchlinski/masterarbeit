within PCMgoesHIL.Systems.RecordCollection;
record HOMSystem "Parameters for HOM system PCM Mitsubishi project"
  extends BESMod.Systems.RecordsCollection.SystemParametersBaseDataDefinition(
    use_elecHeating=false,
    nZones=10,
    filNamWea=Modelica.Utilities.Files.loadResource(
        "modelica://PCMgoesHIL/Resources/WeatherData/TRY2015_523938130651_Jahr_City_Potsdam.mos"),
    QBui_flow_nominal={1119.5809,566.4584,51.050556,676.7798,805.4208,797.43317,559.32,1,672.6929,643.7064},
    TSetZone_nominal={293.15,293.15,288.15,293.15,293.15,293.15,293.15,288.15,
        297.15,293.15},
    use_ventilation=false,
    THydSup_nominal=fill(328.15,nZones),
    TOda_nominal=261.15);

end HOMSystem;
