within PCMgoesHIL.Systems.Hydraulic.Control.Components;
model ThermostaticValveControlHIL
 extends
    BESMod.Systems.Hydraulical.Control.Components.ThermostaticValveController.BaseClasses.PartialThermostaticValveController;

  parameter Modelica.Units.SI.TemperatureDifference hysteresis=0.5
    "setTemp + hysteresis --> y = 1; 
    setTemp - hysteresis --> y = 0 (without time delay)";
  parameter Real k(min=0, unit="1") = 1 "Gain of controller";
  parameter Modelica.Units.SI.Time Ti(min=Modelica.Constants.small)=0.5
    "Time constant of Integrator block" annotation (Dialog(enable=
          controllerType == .Modelica.Blocks.Types.SimpleController.PI or
          controllerType == .Modelica.Blocks.Types.SimpleController.PID));
  parameter Modelica.Units.SI.Time Td(min=0)=0.1
    "Time constant of Derivative block" annotation (Dialog(enable=
          controllerType == .Modelica.Blocks.Types.SimpleController.PD or
          controllerType == .Modelica.Blocks.Types.SimpleController.PID));
  parameter .Modelica.Blocks.Types.SimpleController controllerType=
         .Modelica.Blocks.Types.SimpleController.PID "Type of controller";
  parameter Modelica.Units.SI.Time timeDelayActTemp=60
    "Time delay until actual room temperature reaches actual controller";
  parameter Modelica.Blocks.Types.Init initType=Modelica.Blocks.Types.Init.NoInit
    "Type of initialization (1: no init, 2: steady state, 3: initial state, 4: initial output)"
    annotation (Dialog(group="Initialization"));
  parameter Real y_start(
    min=0.0,
    max=1.0) = 1.0 "Set start position of valve opening"
   annotation (Dialog(
        enable=initType == Modelica.Blocks.Types.InitPID.InitialOutput,
        group="Initialization"));
  Modelica.Blocks.Math.Feedback tempDiffActual[nZones]
    "Positiv temp diff means room warmer than set temp" annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-62,8})));
  Modelica.Blocks.Nonlinear.FixedDelay fixedDelay[nZones](each final delayTime=
        timeDelayActTemp)
    "Time delay until actual room temperature reaches fluid in the thermostatic head"
    annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={-22,60})));
  Modelica.Blocks.Math.Feedback tempDiffWithDelay[nZones]
    "Positiv temp diff means room warmer than set temp (be careful with time delay)"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-22,10})));
  Modelica.Blocks.Sources.Constant minusHalfHysteresis[nZones](each final k=
        hysteresis) "Half of the hysteresis bandwidth"
    annotation (Placement(transformation(extent={{-40,-44},{-26,-30}})));

  Modelica.Blocks.Continuous.LimPID PContr[nZones](
    each u_s(unit="K"),
    each u_m(unit="K"),
    each final controllerType=controllerType,
    each final k=k/hysteresis,
    each final Ti=Ti,
    each final Td=Td,
    each final initType=initType,
    each final y_start=y_start,
    each final yMax=1,
    each final yMin=0) "Actual controller"
    annotation (Placement(transformation(extent={{42,-26},{62,-46}})));

protected
  Modelica.Blocks.Sources.Constant zeroDiff[nZones](each final k(unit="K") = 0)
    "Target deviation"
    annotation (Placement(transformation(extent={{8,-78},{22,-64}})));
  Modelica.Blocks.Math.Add minus[nZones](each final k2=-1)
    "Minus half the hysteresis bandwidth"
    annotation (Placement(transformation(extent={{6,-26},{20,-12}})));

equation

  connect(PContr.u_s, zeroDiff.y) annotation (Line(points={{40,-36},{28,-36},{28,
          -71},{22.7,-71}}, color={0,0,127}));
  connect(minusHalfHysteresis.y, minus.u2) annotation (Line(points={{-25.3,-37},
          {-4,-37},{-4,-23.2},{4.6,-23.2}},           color={0,0,127}));
  connect(tempDiffWithDelay.y, minus.u1) annotation (Line(points={{-22,1},{-22,
          -16},{4.6,-16},{4.6,-14.8}},                    color={0,0,127}));
  connect(fixedDelay.y, tempDiffWithDelay.u1)
    annotation (Line(points={{-22,49},{-22,18}},        color={0,0,127}));
  connect(minus.y, PContr.u_m) annotation (Line(points={{20.7,-19},{52,-19},{52,
          -24}},                                                                      color={0,0,127}));

  connect(fixedDelay.u, TZoneMea) annotation (Line(points={{-22,72},{-22,80},{-62,
          80},{-62,60},{-120,60}}, color={0,0,127}));
  connect(tempDiffActual.u1, TZoneMea)
    annotation (Line(points={{-62,16},{-62,60},{-120,60}}, color={0,0,127}));
  connect(tempDiffActual.u2, TZoneSet) annotation (Line(points={{-70,8},{-82,8},
          {-82,-60},{-120,-60}}, color={0,0,127}));
  connect(tempDiffWithDelay.u2, TZoneSet) annotation (Line(points={{-30,10},{-46,
          10},{-46,-60},{-120,-60}}, color={0,0,127}));
  connect(PContr.y, opening) annotation (Line(points={{63,-36},{74,-36},{74,0},{
          120,0}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Line(points={{-82,-82},{-82,-22},{-82,0},{74,0}},  color={0,0,127}),
        Polygon(
          points={{88,-82},{66,-74},{66,-90},{88,-82}},
          lineColor={192,192,192},
          fillColor={192,192,192},
          fillPattern=FillPattern.Solid),
        Line(points={{-92,-82},{80,-82}}, color={192,192,192}),
        Line(points={{-82,76},{-82,-92}}, color={192,192,192}),
        Polygon(
          points={{-82,88},{-90,66},{-74,66},{-82,88}},
          lineColor={192,192,192},
          fillColor={192,192,192},
          fillPattern=FillPattern.Solid)}),                      Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end ThermostaticValveControlHIL;
