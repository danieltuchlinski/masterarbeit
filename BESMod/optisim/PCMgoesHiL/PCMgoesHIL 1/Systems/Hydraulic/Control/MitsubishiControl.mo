within PCMgoesHIL.Systems.Hydraulic.Control;
model MitsubishiControl
  extends BESMod.Systems.Hydraulical.Control.BaseClasses.PartialControl;
  Components.ThermostaticValveControlHIL thermostaticValveControlHIL(
    final nZones=transferParameters.nParallelDem,
    final leakageOpening=0.0001,
    final hysteresis=thermostaticValveHILDataDefinition.hysteresis,
    final k=thermostaticValveHILDataDefinition.k,
    final Ti=thermostaticValveHILDataDefinition.Ti,
    final Td=thermostaticValveHILDataDefinition.Td,
    final controllerType=thermostaticValveHILDataDefinition.controllerType,
    final timeDelayActTemp=thermostaticValveHILDataDefinition.timeDelayActTemp,
    final initType=thermostaticValveHILDataDefinition.initType,
    final y_start=thermostaticValveHILDataDefinition.y_start)
    annotation (Placement(transformation(extent={{140,-80},{160,-60}})));

  replaceable RecordsCollection.ThermostaticValveHILDataDefinition
    thermostaticValveHILDataDefinition constrainedby
    RecordsCollection.ThermostaticValveHILDataDefinition annotation (Placement(
        transformation(extent={{180,-78},{200,-58}})), choicesAllMatching=true);
    BESMod.Systems.Hydraulical.Control.Components.OnOffController.ConstantHysteresisTimeBasedHR
    DHWOnOffContoller(Hysteresis=bivalentControlData.dTHysDHW, dt_hr=
        bivalentControlData.dtHeaRodDHW)
                      annotation (choicesAllMatching=true, Placement(
        transformation(extent={{-124,74},{-108,90}})));
    BESMod.Systems.Hydraulical.Control.Components.OnOffController.ConstantHysteresisTimeBasedHR
    BufferOnOffController(
    Hysteresis=bivalentControlData.dTHysBui,
    dt_hr=bivalentControlData.dtHeaRodBui,
    addSet_dt_hr=1)       annotation (choicesAllMatching=true, Placement(
        transformation(extent={{-124,30},{-108,44}})));
  replaceable parameter BESMod.Systems.Hydraulical.Control.RecordsCollection.BivalentHeatPumpControlDataDefinition
    bivalentControlData constrainedby
    BESMod.Systems.Hydraulical.Control.RecordsCollection.BivalentHeatPumpControlDataDefinition(
      final TOda_nominal=generationParameters.TOda_nominal,
      TSup_nominal=generationParameters.TSup_nominal[1],
      TSetRoomConst=sum(transferParameters.TDem_nominal)/transferParameters.nParallelDem)
    annotation (choicesAllMatching=true, Placement(transformation(extent={{-88,-36},
            {-66,-14}})));
  replaceable
    BESMod.Systems.Hydraulical.Control.Components.DHWSetControl.BaseClasses.PartialTSet_DHW_Control
    TSet_DHW constrainedby
    BESMod.Systems.Hydraulical.Control.Components.DHWSetControl.BaseClasses.PartialTSet_DHW_Control(
      final T_DHW=distributionParameters.TDHW_nominal) annotation (
      choicesAllMatching=true, Placement(transformation(extent={{-212,62},{-188,
            86}})));
  BESMod.Systems.Hydraulical.Control.Components.HeatingCurve
    heatingCurve(
    GraHeaCurve=bivalentControlData.gradientHeatCurve,
    THeaThres=bivalentControlData.TSetRoomConst,
    dTOffSet_HC=bivalentControlData.dTOffSetHeatCurve)
    annotation (Placement(transformation(extent={{-160,12},{-140,32}})));
  Modelica.Blocks.MathBoolean.Or
                             DHWHysOrLegionella(nu=4)
    "Use the HR if the HP reached its limit" annotation (Placement(
        transformation(
        extent={{-5,-5},{5,5}},
        rotation=0,
        origin={-73,65})));
  Modelica.Blocks.Math.BooleanToReal booleanToReal annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=270,
        origin={-34,-40})));
  Modelica.Blocks.Logical.Not bufOn "buffer is charged" annotation (Placement(
        transformation(
        extent={{-5,-5},{5,5}},
        rotation=270,
        origin={-33,-21})));
  Modelica.Blocks.MathBoolean.Or
                             HRactive(nu=3)
                                      annotation (Placement(transformation(
        extent={{-5,-5},{5,5}},
        rotation=0,
        origin={21,21})));
  Modelica.Blocks.Math.Max max1
                               annotation (Placement(transformation(
        extent={{4,-4},{-4,4}},
        rotation=180,
        origin={30,34})));
  Modelica.Blocks.Logical.Switch switchHR annotation (Placement(transformation(
        extent={{-5,-5},{5,5}},
        rotation=0,
        origin={49,21})));
  Modelica.Blocks.Sources.Constant constZero(final k=0) annotation (Placement(
        transformation(
        extent={{2,-2},{-2,2}},
        rotation=180,
        origin={30,6})));
  replaceable BESMod.Systems.Hydraulical.Control.RecordsCollection.HeatPumpSafetyControl
    safetyControl
    annotation (choicesAllMatching=true,Placement(transformation(extent={{206,24},
            {226,44}})));
  Modelica.Blocks.Logical.Or HP_active
                                      annotation (Placement(transformation(
        extent={{-5,-5},{5,5}},
        rotation=0,
        origin={33,85})));
    BESMod.Systems.Hydraulical.Control.Components.HeatPumpNSetController.PI_InverterHeatPumpController
    HP_nSet_Controller(
    P=bivalentControlData.k,
    nMin=bivalentControlData.nMin,
    T_I=bivalentControlData.T_I)
                       annotation (choicesAllMatching=true, Placement(
        transformation(extent={{88,58},{118,86}})));
  Modelica.Blocks.Logical.Switch switch1
    annotation (Placement(transformation(extent={{-5,-5},{5,5}},
        rotation=0,
        origin={69,67})));
  Modelica.Blocks.Sources.Constant const_dT_loading1(k=distributionParameters.dTTra_nominal[
        1])                                               annotation (Placement(
        transformation(
        extent={{4,-4},{-4,4}},
        rotation=180,
        origin={20,52})));
  AixLib.Controls.HeatPump.SafetyControls.SafetyControl securityControl(
    final minRunTime=safetyControl.minRunTime,
    final minLocTime=safetyControl.minLocTime,
    final maxRunPerHou=safetyControl.maxRunPerHou,
    final use_opeEnv=safetyControl.use_opeEnv,
    final use_opeEnvFroRec=false,
    final dataTable=AixLib.DataBase.HeatPump.EN14511.Vitocal200AWO201(
        tableUppBou=[-20,50; -10,60; 30,60; 35,55]),
    final tableUpp=safetyControl.tableUpp,
    final use_minRunTime=safetyControl.use_minRunTime,
    final use_minLocTime=safetyControl.use_minLocTime,
    final use_runPerHou=safetyControl.use_runPerHou,
    final dTHystOperEnv=safetyControl.dT_opeEnv,
    final use_deFro=false,
    final minIceFac=0,
    final use_chiller=false,
    final calcPel_deFro=0,
    final pre_n_start=safetyControl.pre_n_start_hp,
    use_antFre=false) annotation (Placement(transformation(
        extent={{-16,-17},{16,17}},
        rotation=0,
        origin={216,75})));
  Modelica.Blocks.Sources.BooleanConstant hp_mode(final k=true) annotation (
      Placement(transformation(
        extent={{-7,-7},{7,7}},
        rotation=0,
        origin={161,63})));
  Modelica.Blocks.Math.Add add_dT_LoadingBuf
    annotation (Placement(transformation(extent={{44,48},{54,58}})));
  Modelica.Blocks.Sources.Constant const_dT_loading2(k=distributionParameters.dTTraDHW_nominal
         + bivalentControlData.dTHysDHW/2) annotation (Placement(transformation(
        extent={{4,-4},{-4,4}},
        rotation=180,
        origin={20,68})));
  Modelica.Blocks.Math.Add add_dT_LoadingDHW
    annotation (Placement(transformation(extent={{42,72},{52,82}})));
  Modelica.Blocks.Sources.Constant hp_iceFac(final k=1) annotation (Placement(
        transformation(
        extent={{-7,-7},{7,7}},
        rotation=0,
        origin={-195,-51})));
  Modelica.Blocks.Math.MinMax minMax(nu=transferParameters.nParallelDem)
    annotation (Placement(transformation(extent={{-200,20},{-180,40}})));
  Modelica.Blocks.Math.BooleanToReal booleanToReal1
                                                   annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=270,
        origin={-50,-40})));
  AixLib.Utilities.Tables.CombiTable2DExtra nMinTable(
    final u1(unit="degC"),
    final u2(unit="degC"),
    final y(unit="W", displayUnit="kW"),
    final extrapolation=false,
    table=tableMinPartLoad) annotation (extent=[-60,40; -40,60], Placement(
        transformation(
        extent={{-14,-14},{14,14}},
        rotation=-90,
        origin={162,2})));
  Modelica.Blocks.Math.UnitConversions.To_degC t_Ev_in
    annotation (extent=[-88,38; -76,50], Placement(transformation(extent={{-6,-6},
            {6,6}},
        rotation=270,
        origin={170,32})));
  Modelica.Blocks.Math.UnitConversions.To_degC t_Co_ou annotation (extent=[-88,38;
        -76,50], Placement(transformation(extent={{-6,-6},{6,6}},
        rotation=270,
        origin={154,32})));
  parameter Real tableMinPartLoad[:,:]=[0,-20,-15,-10,-7,2,7,12,15,20; 25,0.000,
      0.000,0.511,0.540,0.455,0.322,0.281,0.314,0.58490566; 35,0.543,0.615,
      0.468,0.491,0.463,0.321,0.290,0.299,0.568627451; 40,0.633,0.615,0.457,
      0.434,0.481,0.273,0.300,0.288,0.56; 45,0.000,0.590,0.444,0.490,0.490,
      0.241,0.305,0.297,0.551020408; 50,0.000,0.000,0.452,0.468,0.480,0.250,
      0.298,0.290,0.541666667; 55,0.000,0.000,0.450,0.455,0.460,0.260,0.315,
      0.305,0.543478261; 60,0.000,0.000,0.000,0.000,0.458,0.265,0.302,0.293,
      0.533333333]
    "Table matrix (grid u1 = first column, grid u2 = first row; e.g., table=[0,0;0,1])";
  Modelica.Blocks.Nonlinear.VariableLimiter variableLimiter annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={190,-34})));
  Modelica.Blocks.Sources.Constant const(final k=1) annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={218,-6})));
  Modelica.Blocks.Logical.Switch onOffSwitch
    annotation (Placement(transformation(extent={{60,-100},{42,-82}})));
  Modelica.Blocks.Sources.Constant const1(final k=0)   "HP turned off"
    annotation (Placement(transformation(extent={{-8,-8},{8,8}},
        rotation=180,
        origin={90,-98})));
  Modelica.Blocks.Sources.Constant constPumpOn(final k=1) "Pump is always on"
    annotation (Placement(transformation(
        extent={{-7,-7},{7,7}},
        rotation=0,
        origin={-195,-27})));
  Modelica.Blocks.Nonlinear.Limiter         limiter(uMax=1, uMin=0)
                                                            annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={30,-30})));
equation
  connect(thermostaticValveControlHIL.opening, sigBusTra.opening) annotation (
      Line(points={{162,-70},{174,-70},{174,-100}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(thermostaticValveControlHIL.TZoneMea, buiMeaBus.TZoneMea) annotation (
     Line(points={{138,-64},{122,-64},{122,-54},{244,-54},{244,103},{65,103}},
        color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(thermostaticValveControlHIL.TZoneSet, useProBus.TZoneSet) annotation (
     Line(points={{138,-76},{116,-76},{116,-50},{248,-50},{248,74},{-119,74},{
          -119,103}},
                 color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(DHWOnOffContoller.T_bot, sigBusDistr.TStoDHWTopMea) annotation (Line(
        points={{-124.8,78},{-266,78},{-266,-114},{1,-114},{1,-100}},
        color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(DHWOnOffContoller.Auxilliar_Heater_On, HRactive.u[1]) annotation (
      Line(points={{-106.88,78},{-88,78},{-88,64},{-92,64},{-92,19.8333},{16,
          19.8333}},                                                  color={
          255,0,255}));
  connect(BufferOnOffController.Auxilliar_Heater_On, HRactive.u[2]) annotation (
     Line(points={{-106.88,33.5},{-106.88,32},{-92,32},{-92,21},{16,21}},color=
          {255,0,255}));
  connect(TSet_DHW.y, HRactive.u[3]) annotation (Line(points={{-186.8,67.04},{
          -186.8,66},{-92,66},{-92,22.1667},{16,22.1667}},                color=
         {255,0,255}));
  connect(BufferOnOffController.HP_On, HP_active.u2) annotation (Line(points={{-106.88,
          41.9},{-106.88,40},{-2,40},{-2,78},{22,78},{22,76},{27,76},{27,81}},
                                                 color={255,0,255}));
  connect(DHWHysOrLegionella.y, sigBusDistr.dhw_on) annotation (Line(
        points={{-67.25,65},{-22,65},{-22,-100},{1,-100}},
        color={255,0,255}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(DHWHysOrLegionella.y, switch1.u2) annotation (Line(points={{-67.25,65},
          {-32,65},{-32,64},{-22,64},{-22,66},{12,66},{12,60},{40,60},{40,67},{63,
          67}},                                   color={255,0,255}));
  connect(TSet_DHW.y,DHWHysOrLegionella. u[1]) annotation (Line(points={{-186.8,
          67.04},{-92,67.04},{-92,63.6875},{-78,63.6875}},    color={255,0,255}));
  connect(DHWOnOffContoller.Auxilliar_Heater_On,DHWHysOrLegionella. u[2])
    annotation (Line(points={{-106.88,78},{-88,78},{-88,64.5625},{-78,64.5625}},
                                                                        color={
          255,0,255}));
  connect(DHWOnOffContoller.HP_On,DHWHysOrLegionella. u[3]) annotation (Line(
        points={{-106.88,87.6},{-86,87.6},{-86,65.4375},{-78,65.4375}},
        color={255,0,255}));
  connect(TSet_DHW.y,DHWHysOrLegionella. u[4]) annotation (Line(points={{-186.8,
          67.04},{-132.4,67.04},{-132.4,66.3125},{-78,66.3125}},
                                                               color={255,0,255}));
  connect(heatingCurve.TSet, add_dT_LoadingBuf.u1) annotation (Line(points={{-139,22},
          {-90,22},{-90,44},{34,44},{34,52},{38,52},{38,56},{43,56}},
                                             color={0,0,127}));
  connect(booleanToReal.u,bufOn. y) annotation (Line(points={{-34,-32.8},{-34,-30},
          {-33,-30},{-33,-26.5}},      color={255,0,255}));
  connect(bufOn.u,DHWHysOrLegionella. y) annotation (Line(points={{-33,-15},{-33,
          66},{-67.25,66},{-67.25,65}},     color={255,0,255}));
  connect(constZero.y,switchHR. u3) annotation (Line(points={{32.2,6},{34,6},{34,
          17},{43,17}},    color={0,0,127}));
  connect(HRactive.y,switchHR. u2)
    annotation (Line(points={{26.75,21},{43,21}}, color={255,0,255}));
  connect(max1.y, switchHR.u1)
    annotation (Line(points={{34.4,34},{43,34},{43,25}}, color={0,0,127}));
  connect(HP_active.y,HP_nSet_Controller. HP_On) annotation (Line(points={{38.5,85},
          {76,85},{76,72},{85,72}},     color={255,0,255}));
  connect(securityControl.sigBusHP, sigBusGen.hp_bus) annotation (Line(
      points={{198,63.27},{186,63.27},{186,64},{190,64},{190,-60},{-152,-60},{-152,
          -99}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(securityControl.modeOut, sigBusGen.hp_bus.modeSet)
    annotation (Line(points={{233.333,71.6},{274,71.6},{274,-142},{-152,-142},{
          -152,-99}},                        color={255,0,255}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(securityControl.modeSet,hp_mode. y) annotation (Line(points={{197.867,
          71.6},{174,71.6},{174,63},{168.7,63}}, color={255,0,255}));
  connect(BufferOnOffController.HP_On,HP_active. u2) annotation (Line(points={{-106.88,
          41.9},{-72,41.9},{-72,48},{-32,48},{-32,81},{27,81}},
                                                 color={255,0,255}));
  connect(DHWOnOffContoller.HP_On,HP_active. u1) annotation (Line(points={{-106.88,
          87.6},{-26,87.6},{-26,85},{27,85}},    color={255,0,255}));
  connect(DHWHysOrLegionella.y,switch1. u2) annotation (Line(points={{-67.25,65},
          {-14,65},{-14,67},{63,67}},             color={255,0,255}));
  connect(HP_nSet_Controller.IsOn, sigBusGen.hp_bus.onOffMea) annotation (Line(
        points={{94,55.2},{94,-64},{-152,-64},{-152,-99}}, color={255,0,255}),
      Text(
      string="%second",
      index=1,
      extent={{-3,-6},{-3,-6}},
      horizontalAlignment=TextAlignment.Right));
  connect(const_dT_loading2.y,add_dT_LoadingDHW. u2) annotation (Line(points={{24.4,68},
          {30,68},{30,74},{41,74}},                  color={0,0,127}));
  connect(switch1.y,HP_nSet_Controller. T_Set) annotation (Line(points={{74.5,67},
          {76,67},{76,80.4},{85,80.4}},     color={0,0,127}));
  connect(heatingCurve.TSet,add_dT_LoadingBuf. u1) annotation (Line(points={{-139,22},
          {-90,22},{-90,44},{34,44},{34,52},{38,52},{38,56},{43,56}},
                                             color={0,0,127}));
  connect(add_dT_LoadingBuf.y,switch1. u3) annotation (Line(points={{54.5,53},{60,
          53},{60,63},{63,63}},    color={0,0,127}));
  connect(add_dT_LoadingDHW.y,switch1. u1) annotation (Line(points={{52.5,77},{57.25,
          77},{57.25,71},{63,71}},       color={0,0,127}));
  connect(const_dT_loading1.y,add_dT_LoadingBuf. u2) annotation (Line(points={{24.4,52},
          {32,52},{32,50},{43,50}},          color={0,0,127}));
  connect(DHWOnOffContoller.Auxilliar_Heater_set, max1.u2) annotation (Line(
        points={{-106.88,75.12},{-106.88,74},{28,74},{28,42},{20,42},{20,36.4},{
          25.2,36.4}}, color={0,0,127}));
  connect(BufferOnOffController.Auxilliar_Heater_set, max1.u1) annotation (Line(
        points={{-106.88,30.98},{-104,30.98},{-104,31.6},{25.2,31.6}}, color={0,
          0,127}));
  connect(switchHR.y, sigBusGen.hr_on) annotation (Line(points={{54.5,21},{70,21},
          {70,-64},{-152,-64},{-152,-99}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(heatingCurve.TOda, weaBus.TDryBul) annotation (Line(points={{-162,22},
          {-210,22},{-210,2},{-237,2}},
                               color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(TSet_DHW.sigBusDistr, sigBusDistr) annotation (Line(
      points={{-212,73.88},{-224,73.88},{-224,74},{-248,74},{-248,-110},{-6,-110},
          {-6,-100},{1,-100}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(add_dT_LoadingDHW.u1, TSet_DHW.TSet_DHW) annotation (Line(points={{41,
          80},{-90,80},{-90,66},{-94,66},{-94,56},{-186.8,56},{-186.8,74}},
        color={0,0,127}));
  connect(TSet_DHW.TSet_DHW, DHWOnOffContoller.T_Set) annotation (Line(points={{
          -186.8,74},{-186.8,76},{-182,76},{-182,64},{-116,64},{-116,73.2}},
        color={0,0,127}));
  connect(BufferOnOffController.T_oda, weaBus.TDryBul) annotation (Line(points={
          {-116,44.84},{-116,46},{-237,46},{-237,2}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-3,6},{-3,6}},
      horizontalAlignment=TextAlignment.Right));
  connect(DHWOnOffContoller.T_Top, sigBusDistr.TStoDHWTopMea) annotation (Line(
        points={{-124.8,87.6},{-256,87.6},{-256,-114},{1,-114},{1,-100}}, color=
         {0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(hp_iceFac.y, sigBusGen.hp_bus.iceFacMea) annotation (Line(points={{
          -187.3,-51},{-152,-51},{-152,-99}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-3,-6},{-3,-6}},
      horizontalAlignment=TextAlignment.Right));
  connect(DHWOnOffContoller.T_oda, weaBus.TDryBul) annotation (Line(points={{
          -116,90.96},{-182,90.96},{-182,100},{-237,100},{-237,2}}, color={0,0,
          127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(HP_nSet_Controller.T_Meas, sigBusGen.hp_bus.TConOutMea) annotation (
      Line(points={{103,55.2},{103,-66},{-152,-66},{-152,-99}}, color={0,0,127}),
      Text(
      string="%second",
      index=1,
      extent={{-3,-6},{-3,-6}},
      horizontalAlignment=TextAlignment.Right));
  connect(minMax.yMax, heatingCurve.TSetRoom) annotation (Line(points={{-179,36},
          {-166,36},{-166,38},{-150,38},{-150,34}}, color={0,0,127}));
  connect(minMax.u, useProBus.TZoneSet) annotation (Line(points={{-200,30},{-206,
          30},{-206,60},{-174,60},{-174,68},{-119,68},{-119,103}}, color={0,0,127}),
      Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(DHWHysOrLegionella.y, booleanToReal1.u) annotation (Line(points={{
          -67.25,65},{-60,65},{-60,64},{-32,64},{-32,-8},{-50,-8},{-50,-32.8}},
        color={255,0,255}));
  connect(booleanToReal1.y, sigBusDistr.uDHWPump) annotation (Line(points={{-50,
          -46.6},{-50,-68},{1,-68},{1,-100}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(t_Ev_in.y, nMinTable.u1) annotation (Line(points={{170,25.4},{170,24},
          {170.4,24},{170.4,18.8}}, color={0,0,127}));
  connect(nMinTable.u2, t_Co_ou.y) annotation (Line(points={{153.6,18.8},{153.6,
          22.1},{154,22.1},{154,25.4}}, color={0,0,127}));
  connect(const.y, variableLimiter.limit1) annotation (Line(points={{218,-17},{
          216,-17},{216,-22},{198,-22}}, color={0,0,127}));
  connect(nMinTable.y, variableLimiter.limit2) annotation (Line(points={{162,
          -13.4},{168,-13.4},{168,-22},{182,-22}}, color={0,0,127}));
  connect(t_Ev_in.u, sigBusGen.hp_bus.TEvaInMea) annotation (Line(points={{170,
          39.2},{256,39.2},{256,38},{258,38},{258,-128},{-152,-128},{-152,-99}},
        color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(t_Co_ou.u, sigBusGen.hp_bus.TConInMea) annotation (Line(points={{154,
          39.2},{256,39.2},{256,36},{258,36},{258,-142},{-152,-142},{-152,-99}},
        color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(const1.y, onOffSwitch.u3) annotation (Line(points={{81.2,-98},{71.5,
          -98},{71.5,-98.2},{61.8,-98.2}}, color={0,0,127}));
  connect(onOffSwitch.u1, variableLimiter.y) annotation (Line(points={{61.8,
          -83.8},{108,-83.8},{108,-45},{190,-45}}, color={0,0,127}));
  connect(onOffSwitch.u2, HP_active.y) annotation (Line(points={{61.8,-91},{78,
          -91},{78,85},{38.5,85}}, color={255,0,255}));
  connect(constPumpOn.y, sigBusGen.uPump) annotation (Line(points={{-187.3,-27},
          {-152,-27},{-152,-99}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(booleanToReal.y, limiter.u) annotation (Line(points={{-34,-46.6},{-34,
          -54},{0,-54},{0,-18},{30,-18}}, color={0,0,127}));
  connect(limiter.y, sigBusDistr.uThrWayVal) annotation (Line(points={{30,-41},{
          20,-41},{20,-100},{1,-100}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(BufferOnOffController.T_bot, sigBusDistr.TBuiSupMea) annotation (Line(
        points={{-124.8,33.5},{-124.8,32},{-130,32},{-130,24},{0,24},{0,-16},{-2,
          -16},{-2,-62},{1,-62},{1,-100}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-3,-6},{-3,-6}},
      horizontalAlignment=TextAlignment.Right));
  connect(BufferOnOffController.T_Top, sigBusDistr.TBuiSupMea) annotation (Line(
        points={{-124.8,41.9},{-124.8,40},{-130,40},{-130,24},{0,24},{0,-16},{-2,
          -16},{-2,-62},{1,-62},{1,-100}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-3,-6},{-3,-6}},
      horizontalAlignment=TextAlignment.Right));
  connect(heatingCurve.TSet, BufferOnOffController.T_Set) annotation (Line(
        points={{-139,22},{-124,22},{-124,18},{-116,18},{-116,29.3}}, color={0,0,
          127}));
  connect(onOffSwitch.y, securityControl.nSet) annotation (Line(points={{41.1,
          -91},{36,-91},{36,-58},{106,-58},{106,48},{144,48},{144,72},{150,72},
          {150,78.4},{197.867,78.4}}, color={0,0,127}));
  connect(HP_nSet_Controller.n_Set, variableLimiter.u) annotation (Line(points=
          {{119.5,72},{146,72},{146,44},{190,44},{190,-22}}, color={0,0,127}));
  connect(securityControl.nOut, sigBusGen.hp_bus.nSet) annotation (Line(points={{233.333,
          78.4},{248,78.4},{248,-99},{-152,-99}},           color={0,0,127}),
      Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  annotation (Diagram(graphics={
        Rectangle(
          extent={{-236,96},{-46,56}},
          lineColor={238,46,47},
          lineThickness=1),
        Rectangle(
          extent={{-236,54},{-46,10}},
          lineColor={0,140,72},
          lineThickness=1),
        Text(
          extent={{-210,-20},{-116,14}},
          lineColor={0,140,72},
          lineThickness=1,
          textString="Building Control"),
        Rectangle(
          extent={{6,42},{138,0}},
          lineColor={162,29,33},
          lineThickness=1),
        Text(
          extent={{8,0},{112,-20}},
          lineColor={162,29,33},
          lineThickness=1,
          textString="Heating Rod Control"),
        Rectangle(
          extent={{6,94},{138,46}},
          lineColor={28,108,200},
          lineThickness=1),
        Rectangle(
          extent={{144,94},{246,46}},
          lineColor={28,108,200},
          lineThickness=1),
        Text(
          extent={{10,116},{114,96}},
          lineColor={28,108,200},
          lineThickness=1,
          textString="Heat Pump Control")}));
end MitsubishiControl;
