within PCMgoesHIL.Systems.Hydraulic.Generation.RecordCollection;
record EcodanParameters
  extends
    BESMod.Systems.Hydraulical.Generation.RecordsCollection.HeatPumpBaseDataDefinition(
    mEva_flow_nominal=44/60*1.2,
    QSec_flow_nominal(displayUnit="kW") = 2000,
    genDesTyp=BESMod.Systems.Hydraulical.Generation.Types.GenerationDesign.BivalentAlternativ,
    THeaTresh=293.15,
    redeclare BESMod.Systems.RecordsCollection.TemperatureSensors.DefaultSensor
      TempSensorData);

end EcodanParameters;
