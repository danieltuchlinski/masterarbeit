within PCMgoesHIL.Systems.Hydraulic.Distribution;
partial record BaseHiLPar
  extends Modelica.Icons.Record;
  parameter Integer n_reaVal "Number of values to subscribe" annotation (Dialog(tab="MQTT"));
  parameter String topicsToRea[n_reaVal] "Topics to subscribe" annotation (Dialog(tab="MQTT"));
  parameter Integer n_wriVal "Number of values to publish" annotation (Dialog(tab="MQTT"));
  parameter String topicsToPub[n_wriVal] "Topics to publish" annotation (Dialog(tab="MQTT"));
  parameter Real staReaVal[n_reaVal] "Used before any message has been received" annotation (Dialog(tab="MQTT"));
  parameter Real timConDamReaVal[n_reaVal] "Time constants for critical dumper for read values" annotation (Dialog(tab="MQTT"));
  parameter Real timConDamWriVal[n_wriVal] "Time constants for critical dumper for write values" annotation (Dialog(tab="MQTT"));
  parameter Boolean useJSON = false "True, if JSON format should be used" annotation (Dialog(tab="MQTT"));
  parameter Integer nKey[size(topicsToPub,1)] = fill(1,size(topicsToPub,1)) "number of keys of each topic" annotation (Dialog(tab="MQTT"));
  parameter String key[sum(nKey)] = fill("x", sum(nKey)) "Keys if JSON is chosen" annotation (Dialog(tab="MQTT"));
  replaceable parameter
    MQTT_Interface.MQTTConnectionPara.BasisForMQTTConnection mqttConParam
    constrainedby MQTT_Interface.MQTTConnectionPara.BasisForMQTTConnection
    "MQTT server information" annotation (choicesAllMatching=true, Dialog(tab="MQTT"));

  parameter Modelica.Units.SI.Time startTimeSQL=0
    "Start time, when data exchange with the set frequency (rate) begins"
    annotation (Dialog(enable=use_SQL, group="Data exchange", tab="MQTT"));
  parameter Modelica.Units.SI.Time samRate=1
    "Rate / frequency of data exchange" annotation (Dialog(enable=use_SQL, group="Data exchange", tab="MQTT"));
  parameter Real kReadValues[n_reaVal]=fill(1, n_reaVal) "Mulitplicator for input read values (e.g. unit conversion)" annotation (Dialog(tab="MQTT", enable=use_SQL, group="Unit conversion - Read values"));
  parameter Real addReadValues[n_reaVal]=fill(0, n_reaVal) "Add constants for input read values (e.g. unit conversion)" annotation (Dialog(tab="MQTT",enable=use_SQL, group="Unit conversion - Read values"));
  parameter Real maxReadValues[n_reaVal]=fill(Modelica.Constants.inf, n_reaVal) "Upper limits of read signals" annotation (Dialog(tab="MQTT",enable=use_SQL, group="Unit conversion - Read values"));
  parameter Real minReadValues[n_reaVal]=fill(-1*Modelica.Constants.inf, n_reaVal) "Lower limits of read signals" annotation (Dialog(tab="MQTT",enable=use_SQL, group="Unit conversion - Read values"));

  parameter Real kWriteValues[n_wriVal]=fill(1, n_wriVal) "Mulitplicator for input write values (e.g. unit conversion)" annotation (Dialog(tab="MQTT",enable=use_SQL, group="Unit conversion - Write values"));
  parameter Real addWriteValues[n_wriVal]=fill(0, n_wriVal) "Add constants for input write values (e.g. unit conversion)" annotation (Dialog(tab="MQTT",enable=use_SQL, group="Unit conversion - Write values"));
  parameter Real maxWriteValues[n_wriVal]=fill(Modelica.Constants.inf, n_wriVal) "Upper limits of write signals" annotation (Dialog(tab="MQTT",enable=use_SQL, group="Unit conversion - Write values"));
  parameter Real minWriteValues[n_wriVal]=fill(-1*Modelica.Constants.inf, n_wriVal) "Lower limits of write signals" annotation (Dialog(tab="MQTT",enable=use_SQL, group="Unit conversion - Write values"));
end BaseHiLPar;
