within PCMgoesHIL.Systems.Hydraulic.Distribution;
record HiL1PCMTopics
  extends BaseHiLPar(
    addWriteValues={-273.15,0,0,-273.15},
    redeclare MQTT_Interface.MQTTConnectionPara.TickStackOpenstack mqttConParam,
    kWriteValues={1,3600,3600,1},
    addReadValues={0,273.15,0,273.15,273.15},
    kReadValues={1/3600,1,1/3600,1,1},
    timConDamWriVal={5,5,1,5},
    timConDamReaVal={10,1,10,1,1},
    staReaVal={1206,30,500,50,10},
    topicsToPub={"hil1/Sim/T_RL","hil1/Sim/VFlowSetDHW","hil1/Sim/VFlowPreCirc",
        "hil1/Sim/T_setColDHW"},
    topicsToRea={"hil1/HYD.Bank[1].Vdot.RealVar",
        "hil1/HYD.Bank[1].TempInA.RealVar","hil1/HYD.Bank[3].Vdot.RealVar",
        "hil1/HYD.Bank[3].TempInA.RealVar","hil1/HYD.Bank[3].TempInB.RealVar"},
    n_reaVal=5,
    n_wriVal=4);

end HiL1PCMTopics;
