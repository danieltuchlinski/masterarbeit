within PCMgoesHIL.Systems.Hydraulic.Distribution;
model MitsubishiDistributionSystem
  extends
    BESMod.Systems.Hydraulical.Distribution.BaseClasses.PartialDistribution(
    final dpSup_nominal={2*(threeWayValveParameters.dpValve_nominal + max(
        threeWayValveParameters.dp_nominal))},
    final dTTraDHW_nominal=dynamicHX.dT_nom,
    final dTTra_nominal={0},
    final m_flow_nominal=mDem_flow_nominal,
    final VStoDHW=dhwParameters.V,
    final QDHWStoLoss_flow=dhwParameters.QLoss_flow,
    designType=BESMod.Systems.Hydraulical.Distribution.Types.DHWDesignType.NoStorage,
    final TSup_nominal=TDem_nominal .+ dTLoss_nominal .+ dTTra_nominal,
    final nParallelSup=1,
    final nParallelDem=1);
  parameter Modelica.Units.SI.PressureDifference dpHPToHX_nominal(displayUnit="Pa")=
     20000
    "Pressure difference";
  parameter Modelica.Units.SI.PressureDifference dpHXToDHWSto_nominal(
      displayUnit="Pa")=20000
    "Pressure difference";
  parameter Modelica.Units.SI.TemperatureDifference dTDHWHX_nominal=5
    "Temperature difference at nominal conditions (used to calculate Gc)";
  parameter Modelica.Units.SI.HeatFlowRate QHXDHW_flow_nominal=
      QDHW_flow_nominal
    "Temperature difference at nominal conditions (used to calculate Gc)";
  parameter Modelica.Units.SI.MassFlowRate mHXDHWCircuit_flow_nominal=
      mSup_flow_nominal[1]
    "Nominal mass flow rate";
  parameter Integer nNodes=2 "Spatial segmentation of HX";
  replaceable parameter
    BESMod.Systems.RecordsCollection.Movers.MoverBaseDataDefinition
    pumpData annotation (choicesAllMatching=true, Placement(transformation(extent={{-98,-58},
            {-82,-42}})));
  replaceable parameter
    BESMod.Systems.RecordsCollection.TemperatureSensors.TemperatureSensorBaseDefinition
    temperatureSensorData
    annotation (choicesAllMatching=true, Placement(transformation(extent={{42,82},
            {58,96}})));
  replaceable parameter
    BESMod.Systems.RecordsCollection.Valves.ThreeWayValve
    threeWayValveParameters constrainedby
    BESMod.Systems.RecordsCollection.Valves.ThreeWayValve(
    final dp_nominal={dpDem_nominal[1],dpHPToHX_nominal},
    final m_flow_nominal=mSup_flow_nominal[1],
    final fraK=1,
    use_inputFilter=false) annotation (choicesAllMatching=
       true, Placement(transformation(extent={{-78,82},{-62,98}})));

  replaceable parameter
    BESMod.Systems.Hydraulical.Distribution.RecordsCollection.BufferStorage.BufferStorageBaseDataDefinition
    dhwParameters constrainedby
    BESMod.Systems.Hydraulical.Distribution.RecordsCollection.BufferStorage.BufferStorageBaseDataDefinition(
    final Q_flow_nominal=0,
    final VPerQ_flow=0,
    final rho=rho,
    final c_p=cp,
    final V=V,
    final TAmb=TAmb,
    T_m=TDHW_nominal,
    final QHC1_flow_nominal=QDHW_flow_nominal,
    final mHC1_flow_nominal=mSup_flow_nominal[1],
    final use_HC2=storageDHW.useHeatingCoil2,
    final use_HC1=storageDHW.useHeatingCoil1,
    final dTLoadingHC2=9999999,
    final fHeiHC2=1,
    final fDiaHC2=1,
    final QHC2_flow_nominal=9999999,
    final mHC2_flow_nominal=9999999)
    annotation (choicesAllMatching=true, Placement(transformation(extent={{64,-18},
            {78,-4}})));

  Modelica.Blocks.Sources.RealExpression T_stoDHWTop(final y(unit="K", displayUnit="degC")=storageDHW.layer[end].T) annotation (Placement(transformation(
        extent={{-10,-5},{10,5}},
        rotation=0,
        origin={-30,83})));
  Modelica.Blocks.Sources.RealExpression T_stoDHWBot(final y(unit="K", displayUnit="degC")=storageDHW.layer[1].T)
    annotation (Placement(transformation(
        extent={{-10,-5},{10,5}},
        rotation=0,
        origin={-30,95})));

  Modelica.Thermal.HeatTransfer.Sources.FixedTemperature fixedTemperatureDHW(final T=dhwParameters.TAmb)           annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={70,-50})));

  AixLib.Fluid.Storage.BufferStorage storageDHW(
    redeclare final package Medium = MediumDHW,
    final energyDynamics=energyDynamics,
    final p_start=p_start,
    final mSenFac=1,
    redeclare final package MediumHC1 = MediumGen,
    redeclare final package MediumHC2 = MediumGen,
    final m1_flow_nominal=mSup_flow_nominal[1],
    final m2_flow_nominal=mDHW_flow_nominal,
    final mHC1_flow_nominal=dhwParameters.mHC1_flow_nominal,
    final mHC2_flow_nominal=dhwParameters.mHC2_flow_nominal,
    final useHeatingCoil1=false,
    final useHeatingCoil2=false,
    final useHeatingRod=dhwParameters.use_hr,
    final TStart=T_start,
    redeclare final BESMod.Systems.Hydraulical.Distribution.RecordsCollection.BufferStorage.bufferData
                                                               data(
      final hTank=dhwParameters.h,
      hHC1Low=0,
      hHR=dhwParameters.nLayerHR/dhwParameters.nLayer*dhwParameters.h,
      final dTank=dhwParameters.d,
      final sWall=dhwParameters.sIns/2,
      final sIns=dhwParameters.sIns/2,
      final lambdaWall=dhwParameters.lambda_ins,
      final lambdaIns=dhwParameters.lambda_ins,
      final rhoIns=373,
      final cIns=1000,
      pipeHC1=dhwParameters.pipeHC1,
      pipeHC2=dhwParameters.pipeHC2,
      lengthHC1=dhwParameters.lengthHC1,
      lengthHC2=dhwParameters.lengthHC2),
    final n=dhwParameters.nLayer,
    final hConIn=dhwParameters.hConIn,
    final hConOut=dhwParameters.hConOut,
    final hConHC1=dhwParameters.hConHC1,
    final hConHC2=dhwParameters.hConHC2,
    final upToDownHC1=true,
    final upToDownHC2=true,
    final TStartWall=T_start,
    final TStartIns=T_start,
    redeclare model HeatTransfer =
        AixLib.Fluid.Storage.BaseClasses.HeatTransferBuoyancyWetter,
    final allowFlowReversal_layers=allowFlowReversal,
    final allowFlowReversal_HC1=allowFlowReversal,
    final allowFlowReversal_HC2=allowFlowReversal)
    annotation (Placement(transformation(extent={{20,-26},{40,0}})));

  BESMod.Systems.Hydraulical.Distribution.Components.Valves.ThreeWayValveWithFlowReturn
    threeWayValveWithFlowReturn(
    redeclare package Medium = MediumGen,
    final energyDynamics=energyDynamics,
    final p_start=p_start,
    final T_start=T_start,
    final X_start=X_start,
    final C_start=C_start,
    final C_nominal=C_nominal,
    final mSenFac=mSenFac,
    redeclare BESMod.Systems.RecordsCollection.Valves.DefaultThreeWayValve
      parameters=threeWayValveParameters)
    annotation (Placement(transformation(extent={{-80,40},{-60,60}})));

  BESMod.Utilities.KPIs.InternalKPICalculator internalKPICalculatorDHWLoss(
    unit="W",
    integralUnit="J",
    calc_singleOnTime=false,
    calc_integral=true,
    calc_totalOnTime=false,
    calc_numSwi=false,
    calc_movAve=false,
    calc_intBelThres=false,
    y=fixedTemperatureDHW.port.Q_flow)
    annotation (Placement(transformation(extent={{-40,-108},{-20,-70}})));
  IBPSA.Fluid.Sensors.TemperatureTwoPort senTBuiSup(
    redeclare final package Medium = Medium,
    final allowFlowReversal=allowFlowReversal,
    m_flow_nominal=m_flow_nominal[1],
    tau=temperatureSensorData.tau,
    initType=temperatureSensorData.initType,
    T_start=T_start,
    final transferHeat=true,
    TAmb=TAmb,
    tauHeaTra=temperatureSensorData.tauHeaTra)
    "Temperature at supply for building" annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=180,
        origin={-18,60})));

  BESMod.Utilities.Electrical.ZeroLoad zeroLoad
    annotation (Placement(transformation(extent={{20,-100},{40,-80}})));
  IBPSA.Fluid.Sensors.TemperatureTwoPort senTBuiRet(
    redeclare final package Medium = Medium,
    final allowFlowReversal=allowFlowReversal,
    m_flow_nominal=m_flow_nominal[1],
    tau=temperatureSensorData.tau,
    initType=temperatureSensorData.initType,
    T_start=T_start,
    final transferHeat=temperatureSensorData.transferHeat,
    TAmb=temperatureSensorData.TAmb,
    tauHeaTra=temperatureSensorData.tauHeaTra)
    "Temperature at supply for building" annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=180,
        origin={-10,30})));
  parameter Modelica.Units.SI.Volume V=0.2
                                       "Volume of storage";
  AixLib.Fluid.HeatExchangers.DynamicHX dynamicHX(
    redeclare final package Medium1 = MediumGen,
    redeclare final package Medium2 = MediumDHW,
    final m1_flow_nominal=mSup_flow_nominal[1],
    final m2_flow_nominal=mHXDHWCircuit_flow_nominal,
    final dp1_nominal=dpHPToHX_nominal,
    final dp2_nominal=dpHXToDHWSto_nominal,
    final nNodes=nNodes,
    redeclare final AixLib.Fluid.MixingVolumes.MixingVolume vol1,
    redeclare final AixLib.Fluid.MixingVolumes.MixingVolume vol2,
    final dT_nom=dTDHWHX_nominal,
    final Q_nom=QHXDHW_flow_nominal)
    annotation (Placement(transformation(extent={{10,-10},{-10,10}},
        rotation=180,
        origin={-50,12})));

  IBPSA.Fluid.Movers.SpeedControlled_y pump(
    redeclare final package Medium = MediumDHW,
    final energyDynamics=energyDynamics,
    final p_start=p_start,
    final T_start=T_start,
    final allowFlowReversal=allowFlowReversal,
    final show_T=show_T,
    redeclare
      BESMod.Systems.RecordsCollection.Movers.AutomaticConfigurationData per(
      final speed_rpm_nominal=pumpData.speed_rpm_nominal,
      final m_flow_nominal=mHXDHWCircuit_flow_nominal,
      final dp_nominal=dpHXToDHWSto_nominal,
      final rho=rho,
      final V_flowCurve=pumpData.V_flowCurve,
      final dpCurve=pumpData.dpCurve),
    final inputType=IBPSA.Fluid.Types.InputType.Continuous,
    final addPowerToMedium=pumpData.addPowerToMedium,
    final tau=pumpData.tau,
    final use_inputFilter=pumpData.use_inputFilter,
    final riseTime=pumpData.riseTimeInpFilter,
    final init=Modelica.Blocks.Types.Init.InitialOutput,
    final y_start=1)                 annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={-50,-30})));

initial equation
  assert(mDem_flow_nominal[1] == mSup_flow_nominal[1], "Nominal mass flow rates are not equal!", AssertionLevel.error);
equation
  connect(T_stoDHWBot.y, sigBusDistr.TStoDHWBotMea) annotation (Line(points={{-19,95},
          {-10,95},{-10,101},{0,101}},                  color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(T_stoDHWTop.y, sigBusDistr.TStoDHWTopMea) annotation (Line(points={{-19,83},
          {0,83},{0,101}},                            color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(fixedTemperatureDHW.port, storageDHW.heatportOutside) annotation (
      Line(points={{60,-50},{52,-50},{52,-12.22},{39.75,-12.22}}, color={191,0,0}));
  connect(portDHW_in, storageDHW.fluidportBottom2) annotation (Line(points={{100,-82},
          {100,-34},{32,-34},{32,-26.13},{32.875,-26.13}},
                                                         color={0,127,255}));
  connect(portGen_in[1], threeWayValveWithFlowReturn.portGen_a) annotation (
      Line(points={{-100,80},{-86,80},{-86,54.4},{-80,54.4}}, color={0,127,255}));
  connect(portGen_out[1], threeWayValveWithFlowReturn.portGen_b) annotation (
      Line(points={{-100,40},{-100,46.4},{-80,46.4}},
        color={0,127,255}));
  connect(threeWayValveWithFlowReturn.uBuf, sigBusDistr.uThrWayVal) annotation (
     Line(points={{-70,62},{-70,72},{0,72},{0,101}},
                                               color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-3,6},{-3,6}},
      horizontalAlignment=TextAlignment.Right));
  connect(internalKPICalculatorDHWLoss.KPIBus, outBusDist.QDHWLoss) annotation (
     Line(
      points={{-19.8,-89},{-14,-89},{-14,-86},{0,-86},{0,-100}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(senTBuiSup.T, sigBusDistr.TBuiSupMea) annotation (Line(points={{-18,71},
          {-18,70},{-10,70},{-10,76},{0,76},{0,101}},                color={0,0,
          127}), Text(
      string="%second",
      index=1,
      extent={{-3,6},{-3,6}},
      horizontalAlignment=TextAlignment.Right));
  connect(zeroLoad.internalElectricalPin, internalElectricalPin) annotation (
      Line(
      points={{40,-90},{56,-90},{56,-84},{70,-84},{70,-98}},
      color={0,0,0},
      thickness=1));
  connect(storageDHW.fluidportTop2, portDHW_out) annotation (Line(points={{33.125,
          0.13},{32,0.13},{32,4},{86,4},{86,-22},{100,-22}},
                                                      color={0,127,255}));
  connect(threeWayValveWithFlowReturn.portBui_b, senTBuiSup.port_a) annotation (
     Line(points={{-60,58},{-58,58},{-58,60},{-28,60}},            color={0,127,
          255}));
  connect(senTBuiRet.port_a, threeWayValveWithFlowReturn.portBui_a) annotation (
     Line(points={{-20,30},{-46,30},{-46,54},{-60,54}},color={0,127,255}));
  connect(senTBuiRet.T, sigBusDistr.TBuiRetMea) annotation (Line(points={{-10,41},
          {-8,41},{-8,60},{0,60},{0,101}},
                                   color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-3,6},{-3,6}},
      horizontalAlignment=TextAlignment.Right));
  connect(threeWayValveWithFlowReturn.portDHW_b, dynamicHX.port_a2) annotation (
     Line(points={{-60,46.4},{-48,46.4},{-48,24},{-38,24},{-38,18},{-40,18}},
        color={0,127,255}));
  connect(storageDHW.fluidportTop1, dynamicHX.port_b1) annotation (Line(points={
          {26.5,0.13},{26.5,4},{26,4},{26,6},{-40,6}}, color={0,127,255}));
  connect(dynamicHX.port_b2, threeWayValveWithFlowReturn.portDHW_a) annotation (
     Line(points={{-60,18},{-64,18},{-64,34},{-60,34},{-60,42.4}},
        color={0,127,255}));
  connect(pump.port_a, storageDHW.fluidportBottom1) annotation (Line(points={{-40,
          -30},{26.625,-30},{26.625,-26.26}}, color={0,127,255}));
  connect(pump.port_b, dynamicHX.port_a1) annotation (Line(points={{-60,-30},{-70,
          -30},{-70,6},{-60,6}}, color={0,127,255}));
  connect(pump.y, sigBusDistr.uDHWPump) annotation (Line(points={{-50,-42},{-50,
          -46},{-10,-46},{-10,22},{0,22},{0,101}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-3,-6},{-3,-6}},
      horizontalAlignment=TextAlignment.Right));
  connect(senTBuiSup.port_b, portBui_out[1]) annotation (Line(points={{-8,60},{
          54,60},{54,78},{86,78},{86,80},{100,80}}, color={0,127,255}));
  connect(senTBuiRet.port_b, portBui_in[1]) annotation (Line(points={{
          1.77636e-15,30},{84,30},{84,40},{100,40}}, color={0,127,255}));
end MitsubishiDistributionSystem;
