within ;
package PCMgoesHIL
annotation (uses(
    Modelica(version="4.0.0"),
    BESMod(version="0.2.2"),
    AixLib(version="1.3.1"),
    IBPSA(version="3.0.0")),
  version="1",
  conversion(from(version="", script=
          "modelica://PCMgoesHIL/ConvertFromPCMgoesHIL_.mos")));
end PCMgoesHIL;
