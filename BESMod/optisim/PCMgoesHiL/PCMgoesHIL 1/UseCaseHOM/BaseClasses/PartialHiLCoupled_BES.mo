within PCMgoesHIL.UseCaseHOM.BaseClasses;
model PartialHiLCoupled_BES
  extends BESMod.Systems.BaseClasses.PartialBuildingEnergySystem(
    redeclare PCMgoesHIL.Systems.RecordCollection.HOMSystem systemParameters,
    redeclare PCMgoesHIL.Systems.UserProfiles.AixLibHighOrderProfiles_HIL
      userProfiles,
    redeclare BESMod.Systems.Demand.Building.AixLibHighOrder building(
      useConstVentRate=false,
      Latitude=Modelica.Units.Conversions.to_deg(weaDat.lat),
      Longitude=Modelica.Units.Conversions.to_deg(weaDat.lon),
      TimeCorrection=0,
      DiffWeatherDataTime=Modelica.Units.Conversions.to_hour(weaDat.timZon),
      redeclare AixLib.DataBase.Walls.Collections.OFD.EnEV2009Heavy wallTypes,
      redeclare model WindowModel =
          AixLib.ThermalZones.HighOrder.Components.WindowsDoors.WindowSimple,
      redeclare AixLib.DataBase.WindowsDoors.Simple.WindowSimple_EnEV2009
        Type_Win,
      redeclare model CorrSolarGainWin =
          AixLib.ThermalZones.HighOrder.Components.WindowsDoors.BaseClasses.CorrectionSolarGain.CorGSimple,
      redeclare BESMod.Systems.Demand.Building.Components.AixLibHighOrderOFD
        aixLiBHighOrderOFD),
    redeclare BESMod.Systems.Electrical.DirectGridConnectionSystem electrical,
    redeclare BESMod.Systems.Control.NoControl control,
    redeclare BESMod.Systems.Hydraulical.HydraulicSystem hydraulic(
      redeclare BESMod.Systems.Hydraulical.Generation.NoGeneration generation,
      redeclare PCMgoesHIL.Systems.Hydraulic.Control.HiLControl control(
          redeclare
          PCMgoesHIL.Systems.Hydraulic.Control.RecordsCollection.ThermostaticValveHILDataDefinition
          thermostaticValveHILDataDefinition),
      redeclare PCMgoesHIL.Systems.Hydraulic.Distribution.HiLMqttConnection
        distribution(mQTTAndConst(startTimeSQL=startTimeSQL)),
      redeclare PCMgoesHIL.Systems.Hydraulic.Transfer.RadiatorTransferSystemHIL
        transfer(
        use_preRelVal=true,
        redeclare
          BESMod.Systems.Hydraulical.Transfer.RecordsCollection.RadiatorTransferData
          radParameters,
        redeclare
          BESMod.Systems.RecordsCollection.TemperatureSensors.DefaultSensor
          sensorData,
        redeclare
          BESMod.Systems.Hydraulical.Transfer.RecordsCollection.SteelRadiatorStandardPressureLossData
          transferDataBaseDefinition,
        dpFullOpen_nominal(displayUnit="Pa") = 73853.65852*0.8)),
    redeclare BESMod.Systems.Demand.DHW.DHW DHW(
      redeclare BESMod.Systems.Demand.DHW.RecordsCollection.ProfileM DHWProfile,
      redeclare BESMod.Systems.RecordsCollection.Movers.DefaultMover pumpData,
      redeclare BESMod.Systems.Demand.DHW.TappingProfiles.calcmFlowEquStatic
        calcmFlow),
    redeclare Systems.RecordCollection.ParameterStudy.ParametersToChange
      parameterStudy,
    redeclare final package MediumDHW = AixLib.Media.Water,
    redeclare final package MediumZone = AixLib.Media.Air,
    redeclare final package MediumHyd = AixLib.Media.Water,
    redeclare BESMod.Systems.Ventilation.NoVentilation ventilation);

    extends Modelica.Icons.Example;
  parameter Modelica.Units.SI.Time startTimeSQL=31536000
    "Start time, when data exchange with the set frequency (rate) begins";
  annotation (experiment(
      StopTime=31536000,
      Interval=600,
      __Dymola_Algorithm="Dassl"));
end PartialHiLCoupled_BES;
