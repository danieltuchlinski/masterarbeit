within PCMgoesHIL.Utilities.DHW;
model BooleanTrigger

  parameter Boolean pre_int_in_start=false "Start value of pre(int_in) at initial time";
  parameter Integer pre_counter_start=0 "Start value of pre(counter) at initial time";
  Boolean int_in;

  Modelica.Blocks.Interfaces.BooleanOutput trigger
    annotation (Placement(transformation(extent={{96,-10},{116,10}})));
  Modelica.Blocks.Interfaces.IntegerOutput counter
    annotation (Placement(transformation(extent={{96,50},{116,70}})));
  Modelica.Blocks.Interfaces.RealInput m_flowSet
    annotation (Placement(transformation(extent={{-122,-16},{-82,24}})));
initial equation
  pre(int_in)=pre_int_in_start;
  pre(counter)=pre_counter_start;
equation
  int_in =  m_flowSet>0; //Check if m_flow is greater 0
  when edge(int_in) then
    counter =  pre(counter)+1;
  end when;
  trigger = edge(int_in);
  annotation ();
end BooleanTrigger;
