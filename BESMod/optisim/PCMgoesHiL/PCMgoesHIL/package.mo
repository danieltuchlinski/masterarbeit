within ;
package PCMgoesHIL
annotation (uses(
    Modelica(version="4.0.0"),
    AixLib(version="1.3.1"),
    BESMod(version="0.3.2"),
    IBPSA(version="4.0.0")),
  version="2",
  conversion(from(version="", script=
          "modelica://PCMgoesHIL/ConvertFromPCMgoesHIL_.mos"), noneFromVersion=
        "1"));
end PCMgoesHIL;
