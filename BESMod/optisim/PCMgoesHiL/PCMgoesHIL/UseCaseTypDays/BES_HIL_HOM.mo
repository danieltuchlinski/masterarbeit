within PCMgoesHIL.UseCaseTypDays;
model BES_HIL_HOM
  extends BaseClasses.PartialHiL_BES(
    redeclare PCMgoesHIL.Systems.RecordCollection.HOMSystem systemParameters,
    redeclare BESMod.Systems.Demand.Building.AixLibHighOrder building(
      useConstVentRate=false,
      Latitude=Modelica.Units.Conversions.to_deg(weaDat.lat),
      Longitude=Modelica.Units.Conversions.to_deg(weaDat.lon),
      TimeCorrection=0,
      DiffWeatherDataTime=Modelica.Units.Conversions.to_hour(weaDat.timZon),
      redeclare AixLib.DataBase.Walls.Collections.OFD.EnEV2009Heavy wallTypes,
      redeclare model WindowModel =
          AixLib.ThermalZones.HighOrder.Components.WindowsDoors.WindowSimple,
      redeclare AixLib.DataBase.WindowsDoors.Simple.WindowSimple_EnEV2009
        Type_Win,
      redeclare model CorrSolarGainWin =
          AixLib.ThermalZones.HighOrder.Components.WindowsDoors.BaseClasses.CorrectionSolarGain.CorGSimple,
      redeclare BESMod.Systems.Demand.Building.Components.AixLibHighOrderOFD
        aixLiBHighOrderOFD),
    redeclare BESMod.Systems.UserProfiles.AixLibHighOrderProfiles userProfiles);

  annotation (experiment(
      StopTime=31536000,
      Interval=600,
      __Dymola_Algorithm="Dassl"));
end BES_HIL_HOM;
