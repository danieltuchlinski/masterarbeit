within PCMgoesHIL.DataBase;
package WallsAndWindows

  package Design_5kW
    record EnEV2009Heavy_AddIsulation
      "Heavy building mass, increased insulation (5kW) based on regulation EnEV 2009"
      extends AixLib.DataBase.Walls.Collections.OFD.BaseDataMultiInnerWalls(
        OW=AixLib.DataBase.Walls.EnEV2009.OW.OW_EnEV2009_S(d={0.05,0.23,0.24,0.015}),
        IW_vert_half_a=AixLib.DataBase.Walls.EnEV2009.IW.IWsimple_EnEV2009_S_half(),
        IW_vert_half_b=AixLib.DataBase.Walls.EnEV2009.IW.IWsimple_EnEV2009_S_half(),
        IW_hori_upp_half=
            AixLib.DataBase.Walls.EnEV2009.Floor.FLpartition_EnEV2009_SM_upHalf(),
        IW_hori_low_half=
            AixLib.DataBase.Walls.EnEV2009.Ceiling.CEpartition_EnEV2009_SM_loHalf(),
        IW_hori_att_upp_half=
            AixLib.DataBase.Walls.EnEV2009.Floor.FLattic_EnEV2009_SML_upHalf(),
        IW_hori_att_low_half=
            AixLib.DataBase.Walls.EnEV2009.Ceiling.CEattic_EnEV2009_SML_loHalf(),
        groundPlate_upp_half=
            AixLib.DataBase.Walls.EnEV2009.Floor.FLground_EnEV2009_SML_upHalf(d={0.1,0.11}),
        groundPlate_low_half=
            AixLib.DataBase.Walls.EnEV2009.Floor.FLground_EnEV2009_SML_loHalf(d={0.15,0.1,0.06}),
        roof=AixLib.DataBase.Walls.EnEV2009.Ceiling.ROsaddleAttic_EnEV2009_SML(d={0.27}),
        IW2_vert_half_a=AixLib.DataBase.Walls.EnEV2009.IW.IWload_EnEV2009_S_half(),
        IW2_vert_half_b=AixLib.DataBase.Walls.EnEV2009.IW.IWload_EnEV2009_S_half(),
        roofRoomUpFloor=
            AixLib.DataBase.Walls.EnEV2009.Ceiling.ROsaddleRoom_EnEV2009_SML(d={0.27,0.0125,0.015}));

    end EnEV2009Heavy_AddIsulation;

    record WindowSimple_EnEV2009_AddInsulation
      "Window according to EnEV 2009 with increased isulation"
      extends AixLib.DataBase.WindowsDoors.Simple.OWBaseDataDefinition_Simple(
        Uw=0.8,
        frameFraction=0.2,
        g=0.6);
      annotation(Documentation(revisions = "<html><ul>
  <li>
    <i>September 11, 2013&#160;</i> by Ole Odendahl:<br/>
    Added reference
  </li>
  <li>
    <i>July 5, 2011</i> by Ana Constantin:<br/>
    implemented
  </li>
</ul>
</html>",     info="<html>
<p>
  <b><span style=\"color: #008000\">Overview</span></b>
</p>
<p>
  Window definition according to EnEV 2009 for a simple window.
</p>
<p>
  <b><span style=\"color: #008000\">References</span></b>
</p>
<p>
  <b><span style=\"color: #008000\">References</span></b>
</p>
<p>
  Record is used in model <a href=
  \"Building.Components.WindowsDoors.WindowSimple\">Building.Components.WindowsDoors.WindowSimple</a>
</p>
<p>
  Source:
</p>
<ul>
  <li>For EnEV see Bundesregierung (Veranst.): Verordnung ueber
  energiesparenden Waermeschutz und energiesparende Anlagentechnik bei
  Gebaeuden. Berlin, 2009
  </li>
</ul>
</html>"));
    end WindowSimple_EnEV2009_AddInsulation;
  end Design_5kW;

  package Design_6kW

    record EnEV2009Heavy_AddIsulation
      "Heavy building mass, increased insulation (6kW) based on regulation EnEV 2009"
      extends AixLib.DataBase.Walls.Collections.OFD.BaseDataMultiInnerWalls(
        OW=AixLib.DataBase.Walls.EnEV2009.OW.OW_EnEV2009_S(d={0.05,0.15,0.20,0.015}),
        IW_vert_half_a=AixLib.DataBase.Walls.EnEV2009.IW.IWsimple_EnEV2009_S_half(),
        IW_vert_half_b=AixLib.DataBase.Walls.EnEV2009.IW.IWsimple_EnEV2009_S_half(),
        IW_hori_upp_half=
            AixLib.DataBase.Walls.EnEV2009.Floor.FLpartition_EnEV2009_SM_upHalf(),
        IW_hori_low_half=
            AixLib.DataBase.Walls.EnEV2009.Ceiling.CEpartition_EnEV2009_SM_loHalf(),
        IW_hori_att_upp_half=
            AixLib.DataBase.Walls.EnEV2009.Floor.FLattic_EnEV2009_SML_upHalf(),
        IW_hori_att_low_half=
            AixLib.DataBase.Walls.EnEV2009.Ceiling.CEattic_EnEV2009_SML_loHalf(),
        groundPlate_upp_half=
            AixLib.DataBase.Walls.EnEV2009.Floor.FLground_EnEV2009_SML_upHalf(),
        groundPlate_low_half=
            AixLib.DataBase.Walls.EnEV2009.Floor.FLground_EnEV2009_SML_loHalf(),
        roof=AixLib.DataBase.Walls.EnEV2009.Ceiling.ROsaddleAttic_EnEV2009_SML(),
        IW2_vert_half_a=AixLib.DataBase.Walls.EnEV2009.IW.IWload_EnEV2009_S_half(),
        IW2_vert_half_b=AixLib.DataBase.Walls.EnEV2009.IW.IWload_EnEV2009_S_half(),
        roofRoomUpFloor=
            AixLib.DataBase.Walls.EnEV2009.Ceiling.ROsaddleRoom_EnEV2009_SML());

    end EnEV2009Heavy_AddIsulation;
  end Design_6kW;
end WallsAndWindows;
