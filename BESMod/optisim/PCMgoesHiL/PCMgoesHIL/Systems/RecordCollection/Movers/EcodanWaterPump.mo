within PCMgoesHIL.Systems.RecordCollection.Movers;
record EcodanWaterPump
  extends BESMod.Systems.RecordsCollection.Movers.MoverBaseDataDefinition(
    tau=1,
    riseTimeInpFilter=30,
    use_inputFilter=false,
    addPowerToMedium=false,
    speed_rpm_nominal=1500,
    V_flowCurve=V_flowCurve_unscaled ./ m_flow_nominal,
   dpCurve=dpCurve_unscaled ./ dp_nominal);
  parameter Modelica.Units.SI.PressureDifference dp_nominal;
  parameter Modelica.Units.SI.MassFlowRate m_flow_nominal;
  parameter Real V_flowCurve_unscaled[:]={0.000121124,0.082606589,0.166545543,0.249031008,
      0.272286822,0.311531008,0.34132752,0.400193799,0.429990311}
                                    "Relative V_flow curve to be used";
  parameter Real dpCurve_unscaled[:]={73853.65852,71024.39026,64000.00003,54048.78051,
      50243.90243,39902.43903,30048.78049,10439.02438,0}
                                 "Relative dp curve to be used";
end EcodanWaterPump;
