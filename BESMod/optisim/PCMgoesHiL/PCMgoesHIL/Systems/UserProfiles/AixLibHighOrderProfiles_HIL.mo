within PCMgoesHIL.Systems.UserProfiles;
model AixLibHighOrderProfiles_HIL "Profiles for HIL"
  extends BESMod.Systems.UserProfiles.BaseClasses.PartialUserProfiles;
  parameter String fileNameIntGains=Modelica.Utilities.Files.loadResource("modelica://BESMod/Resources/InternalGainsHOM.txt")
    "File where matrix is stored"
    annotation (Dialog(tab="Inputs", group="Internal Gains"));

  parameter AixLib.DataBase.Profiles.ProfileBaseDataDefinition VentilationProfile = AixLib.DataBase.Profiles.Ventilation2perDayMean05perH();
  parameter AixLib.DataBase.Profiles.ProfileBaseDataDefinition TSetProfile = AixLib.DataBase.Profiles.SetTemperaturesVentilation2perDay();
  parameter Real gain=1 "Gain value multiplied with internal gains. Used to e.g. disable single gains."          annotation (Dialog(group=
          "Internal Gains",                                                                                                 tab="Inputs"));

  Modelica.Blocks.Sources.CombiTimeTable tableInternalGains(
    final tableOnFile=true,
    final extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    final tableName="Internals",
    final fileName=fileNameIntGains,
    columns=2:nZones + 1)
                 "Profiles for internal gains"
    annotation (Placement(transformation(extent={{23,23},{-23,-23}},
        rotation=180,
        origin={-27,1})));

  Modelica.Blocks.Math.Gain gainIntGains[nZones](each k=gain)
    "Profiles for internal gains" annotation (Placement(transformation(
        extent={{23,23},{-23,-23}},
        rotation=180,
        origin={45,1})));

  Modelica.Blocks.Sources.CombiTimeTable NaturalVentilation(
    tableName="NaturalVentilation",
    fileName=ModelicaServices.ExternalReferences.loadResource(
        "modelica://PCMgoesHIL/Resources/NaturalVentilationHOM_HiL.txt"),
    columns=2:11,
    smoothness=Modelica.Blocks.Types.Smoothness.ConstantSegments,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    tableOnFile=true,
    table=VentilationProfile.Profile)                                                                                                                                                                         annotation(Placement(transformation(extent={{-93,69},
            {-73,89}})));

  Modelica.Blocks.Sources.CombiTimeTable TZoneSet(
    tableName="TZoneSet",
    fileName=ModelicaServices.ExternalReferences.loadResource(
        "modelica://PCMgoesHIL/Resources/TZoneSet_HIL.txt"),
    columns=2:11,
    smoothness=Modelica.Blocks.Types.Smoothness.ConstantSegments,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    tableOnFile=true,
    table=VentilationProfile.Profile)
    annotation (Placement(transformation(extent={{-93,-89},{-73,-69}})));

equation
  //for i in nZones loop
  //end for;
  connect(tableInternalGains.y, gainIntGains.u) annotation (Line(points={{-1.7,1},
          {-1.7,0.5},{17.4,0.5},{17.4,1}},    color={0,0,127}));
  connect(gainIntGains.y, useProBus.intGains) annotation (Line(points={{70.3,1},
          {79.15,1},{79.15,-1},{115,-1}},             color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));

  connect(NaturalVentilation.y, useProBus.NaturalVentilation) annotation (Line(
        points={{-72,79},{-18,79},{-18,50},{115,50},{115,-1}}, color={0,0,127}),
      Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(TZoneSet.y, useProBus.TZoneSet) annotation (Line(points={{-72,-79},{116,
          -79},{116,-78},{115,-78},{115,-1}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
end AixLibHighOrderProfiles_HIL;
