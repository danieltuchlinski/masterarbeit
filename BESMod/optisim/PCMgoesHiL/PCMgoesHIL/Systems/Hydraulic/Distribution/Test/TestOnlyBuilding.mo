within PCMgoesHIL.Systems.Hydraulic.Distribution.Test;
model TestOnlyBuilding
  extends BESMod.Systems.Hydraulical.Distribution.Tests.PartialTest(redeclare
      PCMgoesHIL.Systems.Hydraulic.Distribution.HiLMqttConnection distribution(
      T_start(displayUnit="K") = 293.15,
      nParallelDem=1,
      nParallelSup=1,
      dTTra_nominal={30},
      m_flow_nominal={1000/3600},
      dTTraDHW_nominal=5,
      designType=BESMod.Systems.Hydraulical.Distribution.Types.DHWDesignType.NoStorage,
      QDHWStoLoss_flow=0,
      VStoDHW=0,
      dpSup_nominal={10000},
      dpDem_nominal={10000},
      redeclare PCMgoesHIL.Systems.Hydraulic.Distribution.HiL1PCMTopics
        topicsMQTT,
      mQTTAndConst(startTimeSQL=10)));

  extends Modelica.Icons.Example;

  Modelica.Blocks.Sources.Constant conTflo(each final k=273.15 + 35)
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={-66,60})));
equation
  connect(conTflo.y, sigBusDistr.TFlowHeaCur) annotation (Line(points={{-55,60},
          {-42,60},{-42,81},{-14,81}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  annotation (experiment(StopTime=86400, __Dymola_Algorithm="Dassl"));
end TestOnlyBuilding;
