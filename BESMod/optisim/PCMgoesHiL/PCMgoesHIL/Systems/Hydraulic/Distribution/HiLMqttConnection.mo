within PCMgoesHIL.Systems.Hydraulic.Distribution;
model HiLMqttConnection
  extends
    BESMod.Systems.Hydraulical.Distribution.BaseClasses.PartialDistribution(
    dpDem_nominal={0},
    dpSup_nominal={0},
    VStoDHW=0,
    QDHWStoLoss_flow=0,
    designType=BESMod.Systems.Hydraulical.Distribution.Types.DHWDesignType.NoStorage,
    dTTraDHW_nominal=5,
    m_flow_nominal=mDem_flow_nominal,
    dTTra_nominal={0},
    nParallelSup=1,
    nParallelDem=1);

  MQTT_Interface.Models.MQTTAndConst mQTTAndConst(
    final nWriteValues=topicsMQTT.n_wriVal,
    final nReadValues=topicsMQTT.n_reaVal,
    final mqttConParam=topicsMQTT.mqttConParam,
    startTimeSQL=31536000,
    final namesToRead=topicsMQTT.topicsToRea,
    final standardReadValues=topicsMQTT.staReaVal,
    final namesToWrite=topicsMQTT.topicsToPub,
    final timeConstsDampReadValues=topicsMQTT.timConDamReaVal,
    final timeConstsDampWriteValues=topicsMQTT.timConDamWriVal,
    final kReadValues=topicsMQTT.kReadValues,
    final addReadValues=topicsMQTT.addReadValues,
    final maxReadValues=topicsMQTT.maxReadValues,
    final minReadValues=topicsMQTT.minReadValues,
    final kWriteValues=topicsMQTT.kWriteValues,
    final addWriteValues=topicsMQTT.addWriteValues,
    final maxWriteValues=topicsMQTT.maxWriteValues,
    final minWriteValues=topicsMQTT.minWriteValues,
    final useJSON=topicsMQTT.useJSON,
    final nKeys=topicsMQTT.nKey,
    final keys=topicsMQTT.key)
    annotation (Placement(transformation(extent={{-23,-23},{23,23}},
        rotation=180,
        origin={-57,-23})));

  Utilities.IdealSourcePumpTemperaturewoPump
                                       idealSourcePumpTemperaturewoPump(
    redeclare final package Medium = Medium,
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
    energyDynamicsDelVol=Modelica.Fluid.Types.Dynamics.FixedInitial,
    TFlow_nominal=323.15,
    TRetDesign=313.15,
    m_flow_nominal=m_flow_nominal[1],
    dpExtra_nominal=0,
    redeclare AixLib.Fluid.Movers.Data.Pumps.Wilo.Stratos25slash1to4 per,
    initTypeDampTFlow=Modelica.Blocks.Types.Init.InitialOutput,
    y_start_TFlow=T_start,
    tauVolHeatUp=10,
    final p_start=p_start,
    final T_start=T_start)
    annotation (Placement(transformation(extent={{-42,10},{20,72}})));
  replaceable parameter PCMgoesHIL.Systems.Hydraulic.Distribution.HiL1PCMTopics
    topicsMQTT constrainedby BaseHiLPar annotation (Placement(transformation(
          extent={{-98,-98},{-78,-78}})), choicesAllMatching=true);

  BESMod.Systems.Demand.DHW.RecordsCollection.ProfileL DHWProfile
    annotation (Placement(transformation(extent={{-66,-98},{-46,-78}})));
  Modelica.Blocks.Sources.Constant m_flow_nom(k=m_flow_nominal[1])
    annotation (Placement(transformation(extent={{-44,84},{-28,100}})));
  BESMod.Utilities.Electrical.ZeroLoad zeroLoad
    annotation (Placement(transformation(extent={{28,-124},{48,-104}})));
  Modelica.Fluid.Sources.Boundary_pT BouGenSin(redeclare package Medium =
        Medium, nPorts=1) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-80,102})));
  Modelica.Fluid.Sources.MassFlowSource_T bouSouGen(redeclare package Medium =
        Medium, nPorts=nParallelSup)
    annotation (Placement(transformation(extent={{-92,46},{-72,66}})));
  IBPSA.Fluid.Sensors.TemperatureTwoPort senTBuiRet(
    redeclare final package Medium = Medium,
    final allowFlowReversal=allowFlowReversal,
    m_flow_nominal=m_flow_nominal[1],
    tau=temperatureSensorData.tau,
    initType=temperatureSensorData.initType,
    T_start=T_start,
    final transferHeat=temperatureSensorData.transferHeat,
    TAmb=temperatureSensorData.TAmb,
    tauHeaTra=temperatureSensorData.tauHeaTra)
    "Temperature at supply for building" annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=180,
        origin={58,48})));
  IBPSA.Fluid.Sensors.TemperatureTwoPort senTBuiSup(
    redeclare final package Medium = Medium,
    final allowFlowReversal=allowFlowReversal,
    m_flow_nominal=m_flow_nominal[1],
    tau=temperatureSensorData.tau,
    initType=temperatureSensorData.initType,
    T_start=T_start,
    final transferHeat=true,
    TAmb=TAmb,
    tauHeaTra=temperatureSensorData.tauHeaTra)
    "Temperature at supply for building" annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=180,
        origin={58,78})));
  BESMod.Systems.RecordsCollection.TemperatureSensors.DefaultSensor
    temperatureSensorData
    annotation (Placement(transformation(extent={{74,6},{94,26}})));
equation
  connect(mQTTAndConst.activeSQL, idealSourcePumpTemperaturewoPump.switchToSQL)
    annotation (Line(points={{-82.3,-16.1},{-88,-16.1},{-88,41},{-45.1,41}},
        color={255,0,255}));
  connect(idealSourcePumpTemperaturewoPump.TReturnMix, mQTTAndConst.writeValues[
    1]) annotation (Line(points={{23.1,19.3},{23.1,-14},{-8,-14},{-8,-23},{-31.7,
          -23}}, color={0,0,127}));
  connect(portDHW_in, portDHW_in)
    annotation (Line(points={{100,-82},{100,-82}}, color={0,127,255}));
  connect(idealSourcePumpTemperaturewoPump.TFlowHeaCur, sigBusDistr.TFlowHeaCur)
    annotation (Line(points={{-45.1,19.3},{-66,19.3},{-66,80},{0,80},{0,101}},
        color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(m_flow_nom.y, idealSourcePumpTemperaturewoPump.m_flow_in_withoutSQL)
    annotation (Line(points={{-27.2,92},{-26,92},{-26,76},{-54,76},{-54,62.7},{
          -45.1,62.7}}, color={0,0,127}));
  connect(zeroLoad.internalElectricalPin, internalElectricalPin) annotation (
      Line(
      points={{48,-114},{56,-114},{56,-84},{70,-84},{70,-98}},
      color={0,0,0},
      thickness=1));
  connect(portDHW_out, portDHW_in) annotation (Line(points={{100,-22},{74,-22},
          {74,-82},{100,-82}}, color={0,127,255}));
  connect(bouSouGen.ports, portGen_out) annotation (Line(points={{-72,56},{-64,
          56},{-64,40},{-100,40}}, color={0,127,255}));
  connect(BouGenSin.ports[1], portGen_in[1])
    annotation (Line(points={{-80,92},{-80,80},{-100,80}}, color={0,127,255}));
  connect(idealSourcePumpTemperaturewoPump.port_b, senTBuiSup.port_a)
    annotation (Line(points={{20,65.8},{26,65.8},{26,78},{48,78}}, color={0,127,
          255}));
  connect(senTBuiSup.port_b, portBui_out[1]) annotation (Line(points={{68,78},{
          84,78},{84,80},{100,80}}, color={0,127,255}));
  connect(idealSourcePumpTemperaturewoPump.port_a, senTBuiRet.port_a)
    annotation (Line(points={{20,56.5},{32,56.5},{32,48},{48,48}}, color={0,127,
          255}));
  connect(senTBuiRet.port_b, portBui_in[1]) annotation (Line(points={{68,48},{
          74,48},{74,50},{84,50},{84,40},{100,40}}, color={0,127,255}));
  connect(senTBuiSup.T, sigBusDistr.TBuiSupMea) annotation (Line(points={{58,89},
          {60,89},{60,101},{0,101}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(senTBuiRet.T, sigBusDistr.TBuiRetMea) annotation (Line(points={{58,59},
          {60,59},{60,66},{32,66},{32,101},{0,101}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(mQTTAndConst.activeSQL, sigBusDistr.bSQL) annotation (Line(points={{
          -82.3,-16.1},{-88,-16.1},{-88,40},{-52,40},{-52,74},{-24,74},{-24,76},
          {0,76},{0,101}}, color={255,0,255}), Text(
      string="%second",
      index=1,
      extent={{-3,6},{-3,6}},
      horizontalAlignment=TextAlignment.Right));
  connect(mQTTAndConst.readValues[2], idealSourcePumpTemperaturewoPump.TFlow)
    annotation (Line(points={{-82.3,-23},{-92,-23},{-92,2},{-84,2},{-84,20},{
          -54,20},{-54,28.6},{-45.1,28.6}}, color={0,0,127}));
  connect(mQTTAndConst.readValues[1], idealSourcePumpTemperaturewoPump.m_flow_in_withSQL)
    annotation (Line(points={{-82.3,-23},{-92,-23},{-92,2},{-84,2},{-84,20},{
          -54,20},{-54,53.4},{-45.1,53.4}}, color={0,0,127}));
end HiLMqttConnection;
