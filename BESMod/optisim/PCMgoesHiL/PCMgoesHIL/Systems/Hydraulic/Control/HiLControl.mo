within PCMgoesHIL.Systems.Hydraulic.Control;
model HiLControl
  extends BESMod.Systems.Hydraulical.Control.BaseClasses.PartialControl(use_dhw=
       false);
  Components.ThermostaticValveControlHIL thermostaticValveControlHIL(
    final nZones=transferParameters.nParallelDem,
    final leakageOpening=0.0001,
    final hysteresis=thermostaticValveHILDataDefinition.hysteresis,
    final k=thermostaticValveHILDataDefinition.k,
    final Ti=thermostaticValveHILDataDefinition.Ti,
    final Td=thermostaticValveHILDataDefinition.Td,
    final controllerType=thermostaticValveHILDataDefinition.controllerType,
    final timeDelayActTemp=thermostaticValveHILDataDefinition.timeDelayActTemp,
    final initType=thermostaticValveHILDataDefinition.initType,
    final y_start=thermostaticValveHILDataDefinition.y_start)
    annotation (Placement(transformation(extent={{112,10},{132,30}})));
  BESMod.Systems.Hydraulical.Control.Components.HeatingCurve
    heatingCurve(
    GraHeaCurve=bivalentControlData.gradientHeatCurve,
    THeaThres=bivalentControlData.TSetRoomConst,
    dTOffSet_HC=bivalentControlData.dTOffSetHeatCurve)
    annotation (Placement(transformation(extent={{-144,18},{-124,38}})));
  Modelica.Blocks.Math.MinMax minMax(nu=transferParameters.nParallelDem)
    annotation (Placement(transformation(extent={{-186,24},{-166,44}})));
  replaceable RecordsCollection.ThermostaticValveHILDataDefinition
    thermostaticValveHILDataDefinition constrainedby
    RecordsCollection.ThermostaticValveHILDataDefinition annotation (Placement(
        transformation(extent={{140,38},{160,58}})),   choicesAllMatching=true);
  replaceable parameter BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultBivHPControl
    bivalentControlData(TSetRoomConst=max(transferParameters.TDem_nominal))
                        constrainedby
    BESMod.Systems.Hydraulical.Control.RecordsCollection.BivalentHeatPumpControlDataDefinition(
      final TOda_nominal=generationParameters.TOda_nominal,
      TSup_nominal=generationParameters.TSup_nominal[1],
      TSetRoomConst=sum(transferParameters.TDem_nominal)/transferParameters.nParallelDem)
    annotation (choicesAllMatching=true, Placement(transformation(extent={{-230,70},
            {-208,92}})));

  Modelica.Blocks.Sources.Constant T_setZone_nominal[transferParameters.nParallelDem](
     k=transferParameters.TDem_nominal)
    annotation (Placement(transformation(extent={{-230,26},{-210,46}})));
equation
  connect(thermostaticValveControlHIL.opening, sigBusTra.opening) annotation (
      Line(points={{134,20},{146,20},{146,18},{174,18},{174,-100}}, color={0,0,
          127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(buiMeaBus.TZoneMea, thermostaticValveControlHIL.TZoneMea) annotation (
     Line(
      points={{65,103},{65,26},{110,26}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(useProBus.TZoneSet, thermostaticValveControlHIL.TZoneSet) annotation (
     Line(
      points={{-119,103},{-119,38},{52,38},{52,14},{110,14}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(heatingCurve.TSet, sigBusDistr.TFlowHeaCur) annotation (Line(points={
          {-123,28},{-62,28},{-62,20},{1,20},{1,-100}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(minMax.yMax, heatingCurve.TSetRoom) annotation (Line(points={{-165,40},
          {-152,40},{-152,46},{-134,46},{-134,40}}, color={0,0,127}));
  connect(weaBus.TDryBul, heatingCurve.TOda) annotation (Line(
      points={{-237,2},{-208,2},{-208,-2},{-156,-2},{-156,28},{-146,28}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(T_setZone_nominal.y, minMax.u) annotation (Line(points={{-209,36},{
          -206,36},{-206,34},{-186,34}}, color={0,0,127}));
end HiLControl;
