within PCMgoesHIL.Systems.Hydraulic.Control.RecordsCollection;
record ThermostaticValveHILDataDefinition
  extends Modelica.Icons.Record;

  parameter Modelica.Units.SI.TemperatureDifference hysteresis=0.5
    "setTemp + hysteresis --> y = 1; 
    setTemp - hysteresis --> y = 0 (without time delay)";
  parameter Real k(unit="1")=0.2        "Gain of controller";
  parameter Modelica.Units.SI.Time Ti=1800
    "Time constant of Integrator block";
  parameter Modelica.Units.SI.Time Td=0
    "Time constant of Derivative block";
  parameter Modelica.Blocks.Types.SimpleController controllerType=
         Modelica.Blocks.Types.SimpleController.PID "Type of controller";
  parameter Modelica.Units.SI.Time timeDelayActTemp=60
    "Time delay until actual room temperature reaches actual controller";
  parameter Modelica.Blocks.Types.Init initType=Modelica.Blocks.Types.Init.NoInit
    "Type of initialization (1: no init, 2: steady state, 3: initial state, 4: initial output)";
  parameter Real y_start(
    min=0.0,
    max=1.0) = 1.0 "Set start position of valve opening";

  annotation (Icon(graphics,
                   coordinateSystem(preserveAspectRatio=false)), Diagram(graphics,
        coordinateSystem(preserveAspectRatio=false)));
end ThermostaticValveHILDataDefinition;
