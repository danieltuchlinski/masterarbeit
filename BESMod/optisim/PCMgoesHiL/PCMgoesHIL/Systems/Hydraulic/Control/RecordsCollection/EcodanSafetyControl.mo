within PCMgoesHIL.Systems.Hydraulic.Control.RecordsCollection;
record EcodanSafetyControl
  extends
    BESMod.Systems.Hydraulical.Control.RecordsCollection.HeatPumpSafetyControl(
    dT_opeEnv=5,
    use_opeEnv=true,
    tableUpp=[-20,45; -10,55; 0,60; 25,60; 35,55]);
end EcodanSafetyControl;
