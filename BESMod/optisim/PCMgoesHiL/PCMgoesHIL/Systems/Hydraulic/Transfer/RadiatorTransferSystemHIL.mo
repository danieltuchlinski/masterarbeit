within PCMgoesHIL.Systems.Hydraulic.Transfer;
model RadiatorTransferSystemHIL
  "Transfer system HIL system using a radiator and ideal thermostatic valves"
  extends BESMod.Systems.Hydraulical.Transfer.BaseClasses.PartialTransfer(
    final nParallelSup=1,
    final QLoss_flow_nominal=f_design .* Q_flow_nominal .- Q_flow_nominal,
    final dTLoss_nominal=fill(0, nParallelDem),
    final dp_nominal=transferDataBaseDefinition.dp_nominal);
  parameter Boolean use_preRelVal=true "=false to disable pressure relief valve";
  parameter Real perPreRelValOpens=0.95 "Percentage of nominal pressure difference at which the pressure relief valve starts to open" annotation(Dialog(enable=use_preRelVal));

    parameter Real deltaMPressDropMainBranch=0.3 "Fraction of nominal mass flow rate where transition to turbulent occurs"
    annotation (Dialog(tab="Advanced", group="Transition to laminar"));
    parameter Boolean from_dpMainBranch=false
    "= true, use m_flow = f(dp) else dp = f(m_flow)"  annotation (Dialog(tab="Advanced"));

   parameter Modelica.Units.SI.MassFlowRate m_flow_small(min=0) = 1E-4*abs(
    sum(m_flow_nominal))
     "Small mass flow rate for regularization of zero flow" annotation(Dialog(tab="Advanced"));

   parameter Boolean linearized=false
    "= true, use linear relation between m_flow and dp for any flow rate"  annotation (Dialog(tab="Advanced"));
   parameter Real deltaMValve=0.02
    "Fraction of nominal flow rate where linearization starts, if y=1" annotation (Dialog(tab="Advanced", group="Transition to laminar"));
   parameter Boolean from_dpBranches=false
    "= true, use m_flow = f(dp) else dp = f(m_flow)" annotation (Dialog(tab="Advanced"));
   parameter Modelica.Units.SI.Time riseTime=120
    "Rise time of the filter (time to reach 99.6 % of an opening step)";
   parameter Integer order=2 "Order of filter";
   parameter Boolean use_inputFilter=true
    "= true, if opening is filtered with a 2nd order CriticalDamping filter";
   parameter Modelica.Blocks.Types.Init init=Modelica.Blocks.Types.Init.InitialOutput
    "Type of initialization (no init/steady state/initial state/initial output)";
   parameter Real y_start=1 "Initial value of output";
   parameter Real R=50 "Rangeability, R=50...100 typically";
   parameter Real delta0=0.01
    "Range of significant deviation from equal percentage law";

  IBPSA.Fluid.HeatExchangers.Radiators.RadiatorEN442_2 rad[nParallelDem](
    each final allowFlowReversal=allowFlowReversal,
    final m_flow_nominal=m_flow_nominal,
    each final show_T=show_T,
    each final energyDynamics=energyDynamics,
    each final p_start=p_start,
    each final nEle=radParameters.nEle,
    each final fraRad=radParameters.fraRad,
    final Q_flow_nominal=Q_flow_nominal .* f_design,
    final T_a_nominal=TTra_nominal,
    final T_b_nominal=TTra_nominal .- dTTra_nominal,
    final TAir_nominal=TDem_nominal,
    final TRad_nominal=TDem_nominal,
    each final n=radParameters.n,
    each final deltaM=0.3,
    final dp_nominal=transferDataBaseDefinition.dpRad_nominal,
    redeclare package Medium = Medium,
    each final T_start=T_start) "Radiator" annotation (
      Placement(transformation(
        extent={{11,11},{-11,-11}},
        rotation=90,
        origin={31,-31})));

  replaceable parameter BESMod.Systems.Hydraulical.Transfer.RecordsCollection.RadiatorTransferData radParameters
    annotation (choicesAllMatching=true, Placement(transformation(extent={{-100,-98},{-80,-78}})));
  BESMod.Utilities.Electrical.ZeroLoad zeroLoad
    annotation (Placement(transformation(extent={{30,-106},{50,-86}})));
  AixLib.Fluid.MixingVolumes.MixingVolume volFlow(
    redeclare final package Medium = Medium,
    final m_flow_nominal=sum(m_flow_nominal),
    final m_flow_small=m_flow_small,
    final allowFlowReversal=allowFlowReversal,
    final nPorts=2,
    final energyDynamics=energyDynamics,
    final massDynamics=massDynamics,
    final p_start=p_start,
    final T_start=T_start,
    final X_start=X_start,
    final C_start=C_start,
    final C_nominal=C_nominal,
    final mSenFac=mSenFac,
    final V=transferDataBaseDefinition.vol/2,
    final use_C_flow=false) "Fluid volume"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}},
        rotation=180,
        origin={-32,30})));
  AixLib.Fluid.FixedResistances.PressureDrop res(
    redeclare final package Medium = Medium,
    final show_T=show_T,
    final allowFlowReversal=allowFlowReversal,
    final m_flow_nominal=sum(m_flow_nominal),
    from_dp=from_dpMainBranch,
    final dp_nominal=transferDataBaseDefinition.dpHeaDistr_nominal,
    final linearized=linearized,
    final deltaM=deltaMPressDropMainBranch) "Pressure drop"
    annotation (Placement(transformation(extent={{0,40},{20,60}})));
  AixLib.Fluid.Actuators.Valves.TwoWayEqualPercentage val[nParallelDem](
    redeclare each final package Medium = Medium,
    final m_flow_nominal=m_flow_nominal,
    each final CvData=AixLib.Fluid.Types.CvTypes.OpPoint,
    each final deltaM=deltaMValve,
    each final from_dp=from_dpBranches,
    final dpValve_nominal=transferDataBaseDefinition.dpHeaSysValve_nominal,
    each final allowFlowReversal=allowFlowReversal,
    each final show_T=show_T,
    each final linearized=linearized,
    final dpFixed_nominal=transferDataBaseDefinition.dpHeaSysPreValve_nominal,
    each final l=transferDataBaseDefinition.leakageOpening,
    each final R=R,
    each final delta0=delta0,
    each final rhoStd=rho,
    each final use_inputFilter=use_inputFilter,
    each final riseTime=riseTime,
    each final init=init,
    each final y_start=y_start)
                         annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={30,30})));
  AixLib.Fluid.Sensors.TemperatureTwoPort senTFlowEach[nParallelDem](
    redeclare each final package Medium = Medium,
    each final tau=sensorData.tau,
    each final allowFlowReversal=allowFlowReversal,
    final m_flow_nominal=m_flow_nominal,
    each final initType=sensorData.initType,
    each final T_start=T_start,
    each final transferHeat=sensorData.transferHeat,
    each final TAmb=sensorData.TAmb,
    each final tauHeaTra=sensorData.tauHeaTra,
    final m_flow_small=1E-4*abs(m_flow_nominal))
    "Temperature sensor before each radiator" annotation (Placement(
        transformation(
        extent={{-9,10},{9,-10}},
        rotation=90,
        origin={30,-1})));
  AixLib.Fluid.Sensors.TemperatureTwoPort senTFlow(
    redeclare final package Medium = Medium,
    final tau=sensorData.tau,
    final m_flow_nominal=sum(m_flow_nominal),
    final initType=sensorData.initType,
    final T_start=T_start,
    final transferHeat=sensorData.transferHeat,
    final TAmb=sensorData.TAmb,
    final tauHeaTra=sensorData.tauHeaTra,
    final allowFlowReversal=allowFlowReversal,
    final m_flow_small=m_flow_small) "Temperature sensor in flow/supply"
    annotation (Placement(transformation(extent={{-80,40},{-60,60}})));
  AixLib.Fluid.Sensors.TemperatureTwoPort senTRetEach[nParallelDem](
    redeclare each final package Medium = Medium,
    each final tau=sensorData.tau,
    each final allowFlowReversal=allowFlowReversal,
    final m_flow_nominal=m_flow_nominal,
    each final initType=sensorData.initType,
    each final T_start=T_start,
    each final transferHeat=sensorData.transferHeat,
    each final TAmb=sensorData.TAmb,
    each final tauHeaTra=sensorData.tauHeaTra,
    final m_flow_small=1E-4*abs(m_flow_nominal))
    "Temperature sensor after each radiator"
    annotation (Placement(transformation(extent={{20,-60},{0,-42}})));
  AixLib.Fluid.MixingVolumes.MixingVolume volRet(
    redeclare final package Medium = Medium,
    final m_flow_nominal=sum(m_flow_nominal),
    final m_flow_small=m_flow_small,
    final allowFlowReversal=allowFlowReversal,
    final nPorts=nParallelDem + 1,
    final energyDynamics=energyDynamics,
    final massDynamics=massDynamics,
    final p_start=p_start,
    final T_start=T_start,
    final X_start=X_start,
    final C_start=C_start,
    final C_nominal=C_nominal,
    final mSenFac=mSenFac,
    final V=transferDataBaseDefinition.vol/2,
    final use_C_flow=false) "Fluid volume"
    annotation (Placement(transformation(extent={{-20,-42},{0,-22}})));
  AixLib.Fluid.Sensors.TemperatureTwoPort senTRet(
    redeclare final package Medium = Medium,
    final tau=sensorData.tau,
    final m_flow_nominal=sum(m_flow_nominal),
    final initType=sensorData.initType,
    final T_start=T_start,
    final transferHeat=sensorData.transferHeat,
    final TAmb=sensorData.TAmb,
    final tauHeaTra=sensorData.tauHeaTra,
    final allowFlowReversal=allowFlowReversal,
    final m_flow_small=m_flow_small) "Temperature sensor in return"
    annotation (Placement(transformation(extent={{-18,-60},{-38,-40}})));
  AixLib.Fluid.Sensors.MassFlowRate senMasFlo(redeclare final package Medium =
        Medium, final allowFlowReversal=allowFlowReversal)
    "Sensor for mass flwo rate"
    annotation (Placement(transformation(extent={{-60,-60},{-80,-40}})));
  replaceable
    BESMod.Systems.RecordsCollection.TemperatureSensors.TemperatureSensorBaseDefinition
    sensorData
    annotation (choicesAllMatching=true,Placement(transformation(extent={{-98,78},{-78,98}})));
  BESMod.Utilities.KPIs.InternalKPICalculator internalKPICalculator(
    unit="W",
    integralUnit="J",
    calc_singleOnTime=false,
    calc_totalOnTime=false,
    calc_numSwi=false,
    calc_movAve=false,
    y=sum(-heatPortRad.Q_flow) + sum(-heatPortCon.Q_flow))
    annotation (Placement(transformation(extent={{-60,-108},{-40,-74}})));
  BESMod.Utilities.KPIs.InputKPICalculator inputKPICalculator[nParallelDem](
    unit=fill("", nParallelDem),
    integralUnit=fill("s", nParallelDem),
    each calc_singleOnTime=false,
    each calc_integral=false,
    each calc_totalOnTime=false,
    each calc_numSwi=false,
    each calc_movAve=false)
    annotation (Placement(transformation(extent={{-26,-108},{-6,-74}})));
  replaceable parameter BESMod.Systems.Hydraulical.Transfer.RecordsCollection.TransferDataBaseDefinition
    transferDataBaseDefinition constrainedby
    BESMod.Systems.Hydraulical.Transfer.RecordsCollection.TransferDataBaseDefinition(
    final Q_flow_nominal=Q_flow_nominal .* f_design,
    final nZones=nParallelDem,
    final AFloor=ABui,
    final heiBui=hBui,
    final mRad_flow_nominal=m_flow_nominal,
    final mHeaCir_flow_nominal=mSup_flow_nominal[1])
    annotation (choicesAllMatching=true, Placement(transformation(extent={{76,78},
            {96,98}})));
  BESMod.Systems.Hydraulical.Distribution.Components.Valves.PressureReliefValve
    pressureReliefValveOnOff(
    redeclare final package Medium = Medium,
    m_flow_nominal=mSup_flow_nominal[1],
    final dpFullOpen_nominal=dpFullOpen_nominal,
    final dpThreshold_nominal=perPreRelValOpens*dpFullOpen_nominal,
    final l=transferDataBaseDefinition.leakageOpening,
    use_inputFilter=false,
    dpValve_nominal=transferDataBaseDefinition.dpPumpHeaCir_nominal)
                                                       if use_preRelVal
                                                 annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-78,0})));
  parameter Modelica.Units.SI.PressureDifference dpFullOpen_nominal
    "Pressure difference at which valve is fully open";
equation
  connect(rad.heatPortRad, heatPortRad) annotation (Line(points={{38.92,-33.2},{
          38.92,-32},{84,-32},{84,-40},{100,-40}},
                                          color={191,0,0}));
  connect(rad.heatPortCon, heatPortCon) annotation (Line(points={{38.92,-28.8},{
          86,-28.8},{86,40},{100,40}},             color={191,0,0}));

  for i in 1:nParallelDem loop
    connect(val[i].port_a, res.port_b) annotation (Line(points={{30,40},{30,50},
            {20,50}},                   color={0,127,255}));
    connect(senTRetEach[i].port_b, volRet.ports[i]) annotation (Line(points={{0,-51},
            {-10,-51},{-10,-42}},              color={0,127,255}));
  end for;
  connect(volRet.ports[nParallelDem + 1], senTRet.port_a) annotation (Line(
        points={{-10,-42},{-10,-50},{-18,-50}},           color={0,127,255}));

  connect(zeroLoad.internalElectricalPin, internalElectricalPin) annotation (
      Line(
      points={{50,-96},{72,-96},{72,-98}},
      color={0,0,0},
      thickness=1));
  connect(rad.port_a, senTFlowEach.port_a) annotation (Line(points={{31,-20},{31,
          -10},{30,-10}},         color={0,127,255}));
  connect(senTFlowEach.port_b, val.port_b) annotation (Line(points={{30,8},{30,20}},
                                 color={0,127,255}));
  connect(portTra_in[1], senTFlow.port_a)
    annotation (Line(points={{-100,38},{-92,38},{-92,50},{-80,50}},
                                                  color={0,127,255}));
  connect(senTFlow.port_b, volFlow.ports[1]) annotation (Line(points={{-60,50},{
          -31,50},{-31,40}},     color={0,127,255}));
  connect(res.port_a, volFlow.ports[2]) annotation (Line(points={{0,50},{-33,50},
          {-33,40}},       color={0,127,255}));

  connect(senTRetEach.port_a, rad.port_b)
    annotation (Line(points={{20,-51},{24,-51},{24,-42},{31,-42}},
                                                         color={0,127,255}));
  connect(senMasFlo.port_b, portTra_out[1]) annotation (Line(points={{-80,-50},{
          -86,-50},{-86,-42},{-100,-42}}, color={0,127,255}));
  connect(senMasFlo.port_a, senTRet.port_b)
    annotation (Line(points={{-60,-50},{-38,-50}}, color={0,127,255}));
  connect(val.y, traControlBus.opening) annotation (Line(points={{42,30},{48,30},
          {48,86},{0,86},{0,100}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));

  connect(inputKPICalculator.u, traControlBus.opening) annotation (Line(points={{-28.2,
          -91},{-60,-91},{-60,-74},{48,-74},{48,86},{0,86},{0,100}},
        color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(outBusTra.opening, inputKPICalculator.KPIBus) annotation (Line(
      points={{0,-104},{0,-91},{-5.8,-91}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(internalKPICalculator.KPIBus, outBusTra.Q_flow) annotation (Line(
      points={{-39.8,-91},{-30,-91},{-30,-70},{0,-70},{0,-104}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(pressureReliefValveOnOff.port_a, portTra_in[1])
    annotation (Line(points={{-78,10},{-78,38},{-100,38}}, color={0,127,255}));
  connect(pressureReliefValveOnOff.port_b, portTra_out[1]) annotation (Line(
        points={{-78,-10},{-78,-42},{-100,-42}}, color={0,127,255}));
end RadiatorTransferSystemHIL;
