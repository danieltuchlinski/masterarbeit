within PCMgoesHIL.Systems.Demand;
model DHWHil
  extends BESMod.Systems.Demand.DHW.BaseClasses.PartialDHW;
  Modelica.Fluid.Sources.MassFlowSource_T bouDHW(redeclare package Medium =
        Medium, nPorts=1) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={-44,-58})));
  Modelica.Fluid.Sources.Boundary_pT BouDHW(redeclare package Medium = Medium,
      nPorts=1) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={-68,60})));
  BESMod.Utilities.Electrical.ZeroLoad zeroLoad
    annotation (Placement(transformation(extent={{12,-90},{32,-70}})));
equation
  connect(port_b, bouDHW.ports[1]) annotation (Line(points={{-100,-60},{-98,-60},
          {-98,-58},{-54,-58}}, color={0,127,255}));
  connect(port_a, BouDHW.ports[1])
    annotation (Line(points={{-100,60},{-78,60}}, color={0,127,255}));
  connect(zeroLoad.internalElectricalPin, internalElectricalPin) annotation (
      Line(
      points={{32,-80},{70,-80},{70,-98}},
      color={0,0,0},
      thickness=1));
end DHWHil;
