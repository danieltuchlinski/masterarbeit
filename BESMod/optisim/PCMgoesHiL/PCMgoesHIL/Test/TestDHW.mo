within PCMgoesHIL.Test;
model TestDHW
  extends Modelica.Icons.Example;
  Utilities.DHW.DHWCalcTab dHWCalcTab(DHWProfile=profileL.table)
    annotation (Placement(transformation(extent={{-18,-60},{2,-40}})));
  BESMod.Systems.Demand.DHW.RecordsCollection.ProfileL profileL
    annotation (Placement(transformation(extent={{66,-98},{86,-76}})));
  Modelica.Blocks.Sources.Constant TDHWMes(k=273.15 + 50)
    annotation (Placement(transformation(extent={{-88,-36},{-68,-16}})));
  Modelica.Blocks.Sources.Constant TDHWColdMes(k=273.15 + 10)
    annotation (Placement(transformation(extent={{-82,-80},{-62,-60}})));
  Modelica.Blocks.Interfaces.RealOutput m_flow_setTable
    annotation (Placement(transformation(extent={{94,-60},{114,-40}})));
  Modelica.Blocks.Sources.Constant mFlowInput(k=200/3600)
    annotation (Placement(transformation(extent={{-90,28},{-70,48}})));
equation
  connect(TDHWMes.y, dHWCalcTab.TDHWMes) annotation (Line(points={{-67,-26},{
          -62,-26},{-62,-28},{-36,-28},{-36,-50},{-18,-50}}, color={0,0,127}));
  connect(TDHWColdMes.y, dHWCalcTab.TColdMes) annotation (Line(points={{-61,-70},
          {-50,-70},{-50,-68},{-24,-68},{-24,-56.6},{-18,-56.6}}, color={0,0,
          127}));
  connect(dHWCalcTab.mFlowDHWTank, m_flow_setTable) annotation (Line(points={{3,
          -46},{88,-46},{88,-50},{104,-50}}, color={0,0,127}));
  connect(dHWCalcTab.mFlowMes, mFlowInput.y) annotation (Line(points={{-18,
          -43.6},{-36,-43.6},{-36,38},{-69,38}}, color={0,0,127}));
  annotation (experiment(StopTime=86400, __Dymola_Algorithm="Dassl"));
end TestDHW;
