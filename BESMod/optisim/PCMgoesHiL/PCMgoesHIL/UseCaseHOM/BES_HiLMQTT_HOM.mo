within PCMgoesHIL.UseCaseHOM;
model BES_HiLMQTT_HOM
    extends BaseClasses.PartialHiLCoupled_BES(hydraulic(distribution(topicsMQTT(
            topicsToRea={"hil1/HYD.Bank[1].Vdot.RealVar",
              "hil1/HYD.Bank[1].TempInA.RealVar",
              "hil2/HYD.Bank[4].Vdot.REAL_VAR",
              "hil2/HYD.Bank[4].Temp_extRL.REAL_VAR",
              "hil2/HYD.Bank[4].Temp_extVL.REAL_VAR"}, topicsToPub={
              "hil1/Sim/T_RL","VFlowDHW_toHiL_lperh","hil1/Sim/VFlowPreCirc",
              "hil1/Sim/T_setColDHW"}))));
  MQTT_Interface.Models.MQTTAndConst mQTTAndConst(
    nWriteValues=22,
    redeclare MQTT_Interface.MQTTConnectionPara.TickStackOpenstack mqttConParam,
    startTimeSQL=startTimeSQL,
    samRate=1,
    namesToWrite={"hil1/Sim/T_Room1","hil1/Sim/T_Room2","hil1/Sim/T_Room3",
        "hil1/Sim/T_Room4","hil1/Sim/T_Room5","hil1/Sim/T_Room6",
        "hil1/Sim/T_Room7","hil1/Sim/T_Room8","hil1/Sim/T_Room9",
        "hil1/Sim/T_Room10","hil1/Sim/T_amb","hil1/Sim/relHum",
        "hil1/Sim/TSet_Room1","hil1/Sim/TSet_Room2","hil1/Sim/TSet_Room3",
        "hil1/Sim/TSet_Room4","hil1/Sim/TSet_Room5","hil1/Sim/TSet_Room6",
        "hil1/Sim/TSet_Room7","hil1/Sim/TSet_Room8","hil1/Sim/TSet_Room9",
        "hil1/Sim/TSet_Room10"},
    kWriteValues={1,1,1,1,1,1,1,1,1,1,1,100,1,1,1,1,1,1,1,1,1,1},
    addWriteValues={-273.15,-273.15,-273.15,-273.15,-273.15,-273.15,-273.15,-273.15,
        -273.15,-273.15,-273.15,0,-273.15,-273.15,-273.15,-273.15,-273.15,-273.15,
        -273.15,-273.15,-273.15,-273.15})
    annotation (Placement(transformation(extent={{336,-104},{398,-42}})));

  BESMod.Systems.Interfaces.BuiMeaBus buiMeaBus annotation (Placement(
        transformation(extent={{252,-98},{302,-46}}), iconTransformation(extent=
           {{252,-98},{302,-46}})));
  AixLib.BoundaryConditions.WeatherData.Bus weaBus annotation (Placement(
        transformation(extent={{260,-126},{300,-86}}), iconTransformation(
          extent={{-502,42},{-482,62}})));
  BESMod.Systems.Interfaces.UseProBus useProBus1
    annotation (Placement(transformation(extent={{270,-38},{290,-18}})));
equation
  connect(building.buiMeaBus, buiMeaBus) annotation (Line(
      points={{39,77.62},{142,77.62},{142,-72},{277,-72}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(weaDat.weaBus, weaBus) annotation (Line(
      points={{-220,70},{-218,70},{-218,36},{-208,36},{-208,-140},{280,-140},{
          280,-106}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(mQTTAndConst.writeValues[11], weaBus.TDryBul) annotation (Line(points={{332.9,
          -73.0705},{332.9,-106},{280,-106}},      color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(mQTTAndConst.writeValues[12], weaBus.relHum) annotation (Line(points={{332.9,
          -72.9295},{316,-72.9295},{316,-106},{280,-106}},         color={0,0,
          127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(mQTTAndConst.writeValues[1:10], buiMeaBus.TZoneMea) annotation (Line(
        points={{332.9,-73.2114},{308,-73.2114},{308,-40},{246,-40},{246,-72},{
          277,-72}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(electrical.useProBus, useProBus1) annotation (Line(
      points={{-159.918,135.314},{60.041,135.314},{60.041,-28},{280,-28}},
      color={255,204,51},
      thickness=0.5));
  connect(useProBus1.TZoneSet, mQTTAndConst.writeValues[13:22]) annotation (
      Line(
      points={{280,-28},{324,-28},{324,-71.5205},{332.9,-71.5205}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  annotation (experiment(
      StopTime=31536000,
      Interval=600,
      __Dymola_Algorithm="Dassl"));
end BES_HiLMQTT_HOM;
