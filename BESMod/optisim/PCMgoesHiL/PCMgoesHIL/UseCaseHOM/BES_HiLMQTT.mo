within PCMgoesHIL.UseCaseHOM;
model BES_HiLMQTT
  extends BESMod.Systems.BaseClasses.PartialBuildingEnergySystem(
    redeclare BESMod.Systems.Control.NoControl control,
    redeclare BESMod.Systems.Ventilation.NoVentilation ventilation,
    redeclare BESMod.Systems.Electrical.DirectGridConnectionSystem electrical,
    redeclare PCMgoesHIL.Systems.Demand.DHWHil DHW(
      mDHW_flow_nominal=0,
      VDHWDay=0,
      tCrit=0,
      QCrit=0),
    redeclare BESMod.Systems.UserProfiles.TEASERProfiles userProfiles,
    redeclare BESMod.Systems.Demand.Building.TEASERThermalZone building(
        redeclare BESMod.Examples.BAUSimStudy.Buildings.Case_1_standard
        oneZoneParam),
    redeclare BESMod.Systems.RecordsCollection.ParameterStudy.NoStudy
      parameterStudy,
    redeclare BESMod.Examples.UseCaseAachen.AachenSystem systemParameters(
        use_dhw=false),
    redeclare package MediumDHW = IBPSA.Media.Water,
    redeclare BESMod.Systems.Hydraulical.HydraulicSystem hydraulic(
      redeclare BESMod.Systems.Hydraulical.Generation.NoGeneration generation,
      redeclare PCMgoesHIL.Systems.Hydraulic.Control.HiLControl control,
      redeclare PCMgoesHIL.Systems.Hydraulic.Distribution.HiLMqttConnection
        distribution(
        nParallelDem=1,
        nParallelSup=1,
        dTTra_nominal={5},
        m_flow_nominal={800/3600},
        dTTraDHW_nominal=5,
        designType=BESMod.Systems.Hydraulical.Distribution.Types.DHWDesignType.NoStorage,
        QDHWStoLoss_flow=0,
        VStoDHW=0,
        dpSup_nominal={0},
        dpDem_nominal={0}),
      redeclare BESMod.Systems.Hydraulical.Transfer.NoHeatTransfer transfer(
          nParallelSup=1, dTTra_nominal={5})));

  annotation (experiment(StopTime=20, __Dymola_Algorithm="Dassl"));
end BES_HiLMQTT;
