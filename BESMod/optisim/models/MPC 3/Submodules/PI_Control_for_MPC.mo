within MPC.Submodules;
model PI_Control_for_MPC

  BESMod.Systems.Hydraulical.Control.Components.HeatPumpNSetController.LimPID
    PID(
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    final k=0.1,
    Ti=100,
    final yMax=0.8889,
    final yMin=0.2,
    final wd=0,
    final initType=Modelica.Blocks.Types.Init.InitialState,
    homotopyType=Modelica.Blocks.Types.LimiterHomotopy.NoHomotopy,
    final strict=false,
    final xd_start=0,
    final y_start=0,
    final limitsAtInit=true,
    I(use_set=false))
    annotation (Placement(transformation(extent={{-36,14},{0,50}})));
  Modelica.Blocks.Interfaces.RealOutput n_Set "Relative compressor set value"
annotation (Placement(transformation(extent={{172,22},{192,42}})));
  Modelica.Blocks.Interfaces.RealInput T_Set "Current set temperature"
annotation (Placement(transformation(extent={{-146,32},{-106,72}})));
  Modelica.Blocks.Interfaces.RealInput T_Meas "Current measured temperature"
annotation (Placement(transformation(
    extent={{-20,-20},{20,20}},
    rotation=90,
    origin={-6,-128})));
  Modelica.Blocks.Interfaces.BooleanInput HP_on
    annotation (Placement(transformation(extent={{-148,-94},{-108,-54}})));
equation
  connect(T_Set,PID. u_s) annotation (Line(points={{-126,52},{-76,52},{-76,32},
          {-39.6,32}}, color={0,0,127}));
  connect(T_Meas,PID. u_m) annotation (Line(points={{-6,-128},{-6,-62},{-18,
          -62},{-18,10.4}},
                       color={0,0,127}));
  connect(n_Set, n_Set)
    annotation (Line(points={{182,32},{182,32}}, color={0,0,127}));
  connect(PID.y, n_Set) annotation (Line(points={{1.8,32},{182,32}},
                    color={0,0,127}));
  connect(PID.IsOn, HP_on) annotation (Line(points={{-28.8,10.4},{-28.8,-72},{
          -90,-72},{-90,-74},{-128,-74}}, color={255,0,255}));
  annotation (Diagram(coordinateSystem(extent={{-100,-100},{140,100}})), Icon(
        coordinateSystem(extent={{-100,-100},{140,100}})));
end PI_Control_for_MPC;
