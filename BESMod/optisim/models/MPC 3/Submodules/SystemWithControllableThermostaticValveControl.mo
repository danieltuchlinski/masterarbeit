within MPC.Submodules;
model SystemWithControllableThermostaticValveControl
  extends BESMod.Systems.Hydraulical.Control.BaseClasses.PartialControl;
  Modelica.Blocks.Logical.Switch Valve_Switch
    annotation (Placement(transformation(extent={{176,-170},{196,-150}})));
  replaceable
    BESMod.Systems.Hydraulical.Control.Components.ThermostaticValveController.BaseClasses.PartialThermostaticValveController
    thermostaticValveController constrainedby
    BESMod.Systems.Hydraulical.Control.Components.ThermostaticValveController.BaseClasses.PartialThermostaticValveController(
      final nZones=transferParameters.nParallelDem, final leakageOpening=
        thermostaticValveParameters.leakageOpening) annotation (
      choicesAllMatching=true, Placement(transformation(extent={{108,-104},{
            134,-74}})));
  replaceable parameter
    BESMod.Systems.Hydraulical.Control.RecordsCollection.ThermostaticValveDataDefinition
    thermostaticValveParameters annotation (choicesAllMatching=true, Placement(
        transformation(extent={{174,-90},{194,-70}})));
  outer Modelica.Blocks.Interfaces.RealInput u_Valve_outer
    annotation (Placement(transformation(extent={{-4,-178},{36,-138}})));
  Modelica.Blocks.Logical.Switch Valve_Switch_fmu
    annotation (Placement(transformation(extent={{108,-158},{128,-138}})));
  Modelica.Blocks.Logical.Switch Valve_Switch_fmu1
    annotation (Placement(transformation(extent={{34,-130},{54,-110}})));
  outer Modelica.Blocks.Interfaces.RealInput T_Zone_Set_outer
    annotation (Placement(transformation(extent={{-102,-178},{-62,-138}})));
equation
  connect(thermostaticValveController.TZoneMea, buiMeaBus.TZoneMea) annotation (
     Line(points={{105.4,-80},{100,-80},{100,-52},{234,-52},{234,103},{65,103}},
        color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(Valve_Switch.y, sigBusTra.opening[1]) annotation (Line(points={{197,
          -160},{197,-120},{202,-120},{202,-64},{174,-64},{174,-100}},
                       color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(thermostaticValveController.opening[1], Valve_Switch.u1)
    annotation (Line(points={{136.6,-89},{136.6,-121.5},{174,-121.5},{174,
          -152}}, color={0,0,127}));
  connect(Valve_Switch_fmu.y, Valve_Switch.u3) annotation (Line(points={{129,
          -148},{154,-148},{154,-168},{174,-168}}, color={0,0,127}));
  connect(Valve_Switch_fmu.u1, u_Valve_outer) annotation (Line(points={{106,
          -140},{70,-140},{70,-158},{16,-158}}, color={0,0,127}));
  connect(Valve_Switch_fmu1.y, thermostaticValveController.TZoneSet[1])
    annotation (Line(points={{55,-120},{80,-120},{80,-98},{105.4,-98}}, color={
          0,0,127}));
  connect(Valve_Switch_fmu1.u3, useProBus.TZoneSet[1]) annotation (Line(points=
          {{32,-128},{-256,-128},{-256,103},{-119,103}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-3,6},{-3,6}},
      horizontalAlignment=TextAlignment.Right));
  connect(Valve_Switch_fmu1.u1, T_Zone_Set_outer) annotation (Line(points={{32,
          -112},{-82,-112},{-82,-158}}, color={0,0,127}));
  annotation (Diagram(graphics={
        Rectangle(
          extent={{70,-68},{202,-110}},
          lineColor={162,29,33},
          lineThickness=1),
        Text(
          extent={{72,-110},{176,-130}},
          lineColor={162,29,33},
          lineThickness=1,
          textString="Thermostatic Valve")}));
end SystemWithControllableThermostaticValveControl;
