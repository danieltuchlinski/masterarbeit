within MPC.Submodules;
model Controller_Presim "Controller for Presim"
  Modelica.Blocks.Interfaces.RealOutput T_Zone_Set
    annotation (Placement(transformation(extent={{136,-74},{156,-54}})));
  Buildings.Utilities.Time.CalendarTime calTim(zerTim=Buildings.Utilities.Time.Types.ZeroTime.NY2015,
      yearRef=2015)
    annotation (Placement(transformation(extent={{-98,42},{-44,96}})));
  Modelica.Blocks.Sources.Constant T_Zone_Set_day(k=19 + 273.15)
    annotation (Placement(transformation(extent={{-94,-30},{-68,-4}})));
  Modelica.Blocks.Sources.Constant T_Zone_Set_night(k=16 + 273.15)
    annotation (Placement(transformation(extent={{-94,-72},{-68,-46}})));
  Modelica.Blocks.Logical.GreaterThreshold greaterThreshold(threshold=3)
    annotation (Placement(transformation(extent={{18,14},{38,34}})));
  Modelica.Blocks.Math.IntegerToReal integerToReal
    annotation (Placement(transformation(extent={{-20,14},{0,34}})));
  Modelica.Blocks.Logical.Switch switch1
    annotation (Placement(transformation(extent={{98,-74},{118,-54}})));
  Modelica.Blocks.Logical.LessThreshold lessThreshold(threshold=16)
    annotation (Placement(transformation(extent={{18,-26},{38,-6}})));
  Modelica.Blocks.Logical.And and1
    annotation (Placement(transformation(extent={{56,-4},{76,16}})));
equation
  connect(integerToReal.y, greaterThreshold.u)
    annotation (Line(points={{1,24},{16,24}}, color={0,0,127}));
  connect(switch1.u1, T_Zone_Set_day.y) annotation (Line(points={{96,-56},{-58,
          -56},{-58,-17},{-66.7,-17}}, color={0,0,127}));
  connect(switch1.u3, T_Zone_Set_night.y) annotation (Line(points={{96,-72},{
          -58,-72},{-58,-59},{-66.7,-59}}, color={0,0,127}));
  connect(switch1.y, T_Zone_Set) annotation (Line(points={{119,-64},{140,-64},{
          140,-64},{146,-64}}, color={0,0,127}));
  connect(T_Zone_Set, T_Zone_Set)
    annotation (Line(points={{146,-64},{146,-64}}, color={0,0,127}));
  connect(lessThreshold.u, integerToReal.y) annotation (Line(points={{16,-16},{
          6,-16},{6,24},{1,24}}, color={0,0,127}));
  connect(greaterThreshold.y, and1.u1) annotation (Line(points={{39,24},{52,24},
          {52,6},{54,6}}, color={255,0,255}));
  connect(lessThreshold.y, and1.u2) annotation (Line(points={{39,-16},{52,-16},
          {52,-2},{54,-2}}, color={255,0,255}));
  connect(calTim.hour, integerToReal.u) annotation (Line(points={{-41.3,86.28},
          {-28,86.28},{-28,24},{-22,24}}, color={255,127,0}));
  connect(and1.y, switch1.u2) annotation (Line(points={{77,6},{82,6},{82,-64},{
          96,-64}}, color={255,0,255}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{120,100}})), Diagram(coordinateSystem(preserveAspectRatio=
            false, extent={{-100,-100},{120,100}})));
end Controller_Presim;
