within MPC.Submodules.Generation;
model HPC "Model with calibrated heat pump"
  extends BESMod.Systems.Hydraulical.Generation.BaseClasses.PartialGeneration(
    p_start(displayUnit="Pa"),
    T_start(displayUnit="K"),
    redeclare package Medium = IBPSA.Media.Water,
    final QLoss_flow_nominal=f_design .* Q_flow_nominal .- Q_flow_nominal,
    final dTLoss_nominal=fill(0, nParallelDem),
    dTTra_nominal={if TDem_nominal[i] > 273.15 + 55 then 10 elseif TDem_nominal[
        i] > 44.9 + 273.15 then 8 else 5 for i in 1:nParallelDem},
    dp_nominal={heatPump.dpCon_nominal + dpHeaRod_nominal},
      nParallelDem=1);
     parameter Boolean use_pressure=false "=true to use a pump which works on pressure difference";
   parameter Boolean use_heaRod=true "=false to disable the heating rod";
    replaceable model PerDataMainHP =
      AixLib.DataBase.HeatPump.PerformanceData.LookUpTableND (
      nConv=90,
      filename_Pel=ModelicaServices.ExternalReferences.loadResource(
          "modelica://MPC/Resources/Optihorst/Optihorst_New.sdf"),
      filename_QCon=ModelicaServices.ExternalReferences.loadResource(
          "modelica://MPC/Resources/Optihorst/Optihorst_New.sdf"))
    constrainedby
    AixLib.DataBase.HeatPump.PerformanceData.BaseClasses.PartialPerformanceData
    annotation (__Dymola_choicesAllMatching=true);

  parameter Modelica.Media.Interfaces.Types.Temperature TSoilConst=273.15 + 10
    "Constant soil temperature for ground source heat pumps";
  replaceable package Medium_eva = AixLib.Media.Air                        constrainedby
    Modelica.Media.Interfaces.PartialMedium annotation (
      __Dymola_choicesAllMatching=true);
  replaceable parameter
    MPC.DataBase.HPCalibration
    heatPumpParameters constrainedby
    BESMod.Systems.Hydraulical.Generation.RecordsCollection.HeatPumpBaseDataDefinition
                       annotation (choicesAllMatching=true, Placement(
        transformation(extent={{-90,20},{-72,38}})));
  replaceable parameter
    BESMod.Systems.Hydraulical.Generation.RecordsCollection.DefaultHR
    heatingRodParameters annotation (choicesAllMatching=true, Placement(
        transformation(extent={{58,42},{70,54}})));

  replaceable parameter
    BESMod.Systems.RecordsCollection.Movers.DefaultMover
    pumpData annotation (choicesAllMatching=true, Placement(transformation(extent={{4,-46},{18,-34}})));

  AixLib.Fluid.Interfaces.PassThroughMedium passThroughMediumHRBuf(redeclare
      package Medium = Medium, allowFlowReversal=allowFlowReversal)
    if not use_heaRod
    annotation (Placement(transformation(extent={{32,74},{44,86}})));

  Modelica.Blocks.Logical.Switch switch1 annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={46,-16})));
  Modelica.Blocks.Sources.RealExpression dummyMassFlow(final y=if use_pressure
         then 1 else m_flow_nominal[1])
    annotation (Placement(transformation(extent={{84,-6},{64,14}})));
  Modelica.Blocks.Sources.RealExpression dummyZero annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={18,4})));
  Modelica.Blocks.MathBoolean.Or
                             or1(nu=3)
                                 annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=270,
        origin={46,14})));

  AixLib.Fluid.HeatPumps.HeatPump heatPump(
    redeclare package Medium_con = Medium,
    redeclare package Medium_eva = Medium_eva,
    final use_rev=true,
    final use_autoCalc=false,
    final Q_useNominal=0,
    final scalingFactor=heatPumpParameters.scalingFactor,
    final use_refIne=true,
    final refIneFre_constant=heatPumpParameters.refIneFre_constant,
    final nthOrder=3,
    final useBusConnectorOnly=false,
    final mFlow_conNominal=m_flow_nominal[1],
    final VCon=heatPumpParameters.VCon,
    final dpCon_nominal=heatPumpParameters.dpCon_nominal,
    final use_conCap=true,
    final CCon=13997,
    final GConOut=5.23,
    final GConIns=60.63,
    final mFlow_evaNominal=heatPumpParameters.mEva_flow_nominal,
    final VEva=heatPumpParameters.VEva,
    final dpEva_nominal=heatPumpParameters.dpEva_nominal,
    final use_evaCap=false,
    final CEva=3997,
    final GEvaOut=0,
    final GEvaIns=0,
    final tauSenT=heatPumpParameters.TempSensorData.tau,
    final transferHeat=true,
    final allowFlowReversalEva=allowFlowReversal,
    final allowFlowReversalCon=allowFlowReversal,
    final tauHeaTraEva=heatPumpParameters.TempSensorData.tauHeaTra,
    final TAmbEva_nominal=heatPumpParameters.TempSensorData.TAmb,
    final tauHeaTraCon=heatPumpParameters.TempSensorData.tauHeaTra,
    final TAmbCon_nominal=heatPumpParameters.TempSensorData.TAmb,
    final pCon_start=p_start,
    final TCon_start=T_start,
    final pEva_start=Medium_eva.p_default,
    final TEva_start=Medium_eva.T_default,
    final energyDynamics=energyDynamics,
    final show_TPort=show_T,
    redeclare model PerDataMainHP = PerDataMainHP (interpMethod=SDF.Types.InterpolationMethod.Linear,
          extrapMethod=SDF.Types.ExtrapolationMethod.Linear),
    redeclare model PerDataRevHP =
        AixLib.DataBase.Chiller.PerformanceData.LookUpTable2D (final dataTable=
            AixLib.DataBase.Chiller.EN14511.Vitocal200AWO201()))
                                                 annotation (Placement(
        transformation(
        extent={{22,-27},{-22,27}},
        rotation=270,
        origin={-44,15})));

  IBPSA.Fluid.Sources.Boundary_ph bou_sinkAir(final nPorts=1, redeclare package
      Medium =         Medium_eva)                       annotation (Placement(
        transformation(
        extent={{10,-10},{-10,10}},
        rotation=180,
        origin={-88,-18})));
  IBPSA.Fluid.Sources.MassFlowSource_T bou_air(
    final m_flow=heatPumpParameters.mEva_flow_nominal,
    final use_T_in=true,
    redeclare package Medium = Medium_eva,
    final use_m_flow_in=false,
    final nPorts=1)
    annotation (Placement(transformation(extent={{-100,42},{-80,62}})));

  Modelica.Blocks.Logical.GreaterThreshold isOnHP(threshold=Modelica.Constants.eps)
    annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=270,
        origin={16,38})));

  Modelica.Blocks.Logical.Switch switch2 annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={-124,56})));
  Modelica.Blocks.Sources.BooleanConstant
                                   AirOrSoil(k=heatPumpParameters.useAirSource)
    annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=0,
        origin={-164,56})));
  Modelica.Blocks.Logical.GreaterThreshold isOnHR(threshold=Modelica.Constants.eps)
    annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=270,
        origin={40,36})));

  BESMod.Utilities.KPIs.InputKPICalculator KPIWel(
    integralUnit="J",
    calc_singleOnTime=true,
    calc_integral=true,
    calc_movAve=false,
    unit="W")
    annotation (Placement(transformation(extent={{-78,-106},{-66,-84}})));
  BESMod.Utilities.KPIs.InputKPICalculator KPIWHRel(
    unit="W",
    integralUnit="J",
    calc_singleOnTime=true,
    calc_integral=true,
    calc_movAve=false) if use_heaRod
    annotation (Placement(transformation(extent={{86,86},{98,108}})));

  Modelica.Blocks.Sources.BooleanConstant constRunPump247(final k=
        heatPumpParameters.use_refIne)
    annotation (Placement(transformation(
        extent={{-5,5},{5,-5}},
        rotation=180,
        origin={79,27})));
  IBPSA.Fluid.Movers.FlowControlled_m_flow
                                       pump(
    redeclare final package Medium = Medium,
    final energyDynamics=energyDynamics,
    final p_start=p_start,
    final T_start=T_start,
    final allowFlowReversal=allowFlowReversal,
    m_flow_nominal=m_flow_nominal[1],
    final show_T=show_T,
    redeclare
      BESMod.Systems.RecordsCollection.Movers.AutomaticConfigurationData
      per(
      final speed_rpm_nominal=pumpData.speed_rpm_nominal,
      final m_flow_nominal=m_flow_nominal[1],
      final dp_nominal=dpDem_nominal[1] + dp_nominal[1],
      final rho=rho,
      final V_flowCurve=pumpData.V_flowCurve,
      final dpCurve=pumpData.dpCurve),
    final inputType=IBPSA.Fluid.Types.InputType.Continuous,
    final addPowerToMedium=false,
    final tau=pumpData.tau,
    final use_inputFilter=pumpData.use_inputFilter,
    final riseTime=pumpData.riseTimeInpFilter,
    final init=Modelica.Blocks.Types.Init.InitialOutput)
                                     annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=180,
        origin={16,-68})));
  AixLib.Fluid.HeatExchangers.HeatingRod hea(
    redeclare package Medium = Medium,
    final allowFlowReversal=allowFlowReversal,
    final m_flow_nominal=m_flow_nominal[1],
    final m_flow_small=1E-4*abs(m_flow_nominal[1]),
    final show_T=show_T,
    final dp_nominal=heatingRodParameters.dp_nominal,
    final tau=30,
    final energyDynamics=energyDynamics,
    final p_start=p_start,
    final T_start=T_start,
    final Q_flow_nominal=heatPumpParameters.QSec_flow_nominal,
    final V=heatingRodParameters.V_hr,
    final eta=heatingRodParameters.eta_hr,
    use_countNumSwi=false) if use_heaRod
    annotation (Placement(transformation(extent={{22,64},{54,96}})));

  Modelica.Blocks.Sources.Constant TSoil(k=TSoilConst)
    annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=0,
        origin={-166,30})));

BESMod.Utilities.KPIs.InternalKPICalculator KPIQHP(
    unit="W",
    integralUnit="J",
    calc_singleOnTime=false,
    calc_integral=true,
    calc_totalOnTime=false,
    calc_numSwi=false,
    calc_movAve=false,
    calc_intBelThres=false,
    y=heatPump.con.QFlow_in)
    annotation (Placement(transformation(extent={{-76,-124},{-64,-102}})));
BESMod.Utilities.KPIs.InternalKPICalculator KPIQHR(
    unit="W",
    integralUnit="J",
    calc_singleOnTime=false,
    calc_integral=true,
    calc_totalOnTime=false,
    calc_numSwi=false,
    calc_movAve=false,
    calc_intBelThres=false,
    y=hea.vol.heatPort.Q_flow) if use_heaRod
    annotation (Placement(transformation(extent={{-76,-142},{-64,-120}})));

  IBPSA.Fluid.Sources.Boundary_pT bouPumpHP(
    redeclare package Medium = Medium,
    final p=p_start,
    final T=T_start,
    final nPorts=1)                 annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={52,-86})));

  IBPSA.Fluid.Sensors.TemperatureTwoPort senTBuiSup(
    redeclare final package Medium = Medium,
    final allowFlowReversal=allowFlowReversal,
    m_flow_nominal=m_flow_nominal[1],
    tau=heatPumpParameters.TempSensorData.tau,
    initType=heatPumpParameters.TempSensorData.initType,
    T_start=T_start,
    final transferHeat=heatPumpParameters.TempSensorData.transferHeat,
    TAmb=heatPumpParameters.TempSensorData.TAmb,
    tauHeaTra=heatPumpParameters.TempSensorData.tauHeaTra)
    "Temperature at supply for building" annotation (Placement(transformation(
        extent={{5,6},{-5,-6}},
        rotation=180,
        origin={71,80})));
  BESMod.Utilities.Electrical.RealToElecCon realToElecCon(use_souGen=false)
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={100,-78})));
  Modelica.Blocks.Math.MultiSum multiSum(nu=if (use_pressure and use_heaRod) then 3 elseif use_pressure and not use_heaRod then 2 elseif use_heaRod and not use_pressure then 2 else 1) annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=180,
        origin={130,-82})));
  Modelica.Blocks.Sources.Constant T_con_amb_mea(k=273.15 + 15) annotation (
      Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=0,
        origin={-50,66})));
  Modelica.Blocks.Sources.Constant icing_factor(k=1) annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=0,
        origin={-92,8})));
  Modelica.Blocks.Sources.RealExpression Pel(y=heatPump.innerCycle.Pel)
    annotation (Placement(transformation(extent={{-94,-70},{-74,-50}})));
  Modelica.Blocks.Sources.RealExpression TConOut(y=heatPump.senT_b1.T)
    annotation (Placement(transformation(extent={{-92,-54},{-72,-34}})));
  Modelica.Blocks.Sources.BooleanExpression OnOff(y=heatPump.greaterThreshold.y)
    annotation (Placement(transformation(extent={{-64,-86},{-44,-66}})));
  Modelica.Blocks.Sources.RealExpression TEvaInMeaRe(y=heatPump.senT_a2.T)
    annotation (Placement(transformation(extent={{-78,78},{-58,98}})));
protected
  parameter Modelica.Units.SI.PressureDifference dpHeaRod_nominal=if use_heaRod
       then heatingRodParameters.dp_nominal else 0;

equation
  connect(KPIQHP.KPIBus, outBusGen.QHP_flow) annotation (Line(points={{-63.88,-113},{0,-113},{0,-100}}, color={255,204,51}, thickness=0.5), Text(string="%second", index=1, extent={{6,3},{6,3}}, horizontalAlignment=TextAlignment.Left));
connect(KPIQHR.KPIBus, outBusGen.QHR_flow) annotation (Line(points={{-63.88,-131},{0,-131},{0,-100}}, color={255,204,51}, thickness=0.5), Text(string="%second", index=1, extent={{6,3},{6,3}}, horizontalAlignment=TextAlignment.Left));

  connect(dummyZero.y,switch1. u3)
    annotation (Line(points={{29,4},{38,4},{38,-4}},    color={0,0,127}));
  connect(dummyMassFlow.y,switch1. u1)
    annotation (Line(points={{63,4},{54,4},{54,-4}}, color={0,0,127}));
  connect(or1.y,switch1. u2)
    annotation (Line(points={{46,7.1},{46,-4}},
                                             color={255,0,255}));

  connect(bou_air.ports[1], heatPump.port_a2) annotation (Line(
      points={{-80,52},{-57.5,52},{-57.5,37}},
      color={0,127,255},
      pattern=LinePattern.Dash));
  connect(heatPump.port_b2, bou_sinkAir.ports[1]) annotation (Line(
      points={{-57.5,-7},{-58,-7},{-58,-18},{-78,-18}},
      color={0,127,255},
      pattern=LinePattern.Dash));

  connect(sigBusGen.hp_bus.nSet, isOnHP.u) annotation (Line(
      points={{2,98},{2,58},{18,58},{18,52},{16,52},{16,45.2}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-3,6},{-3,6}},
      horizontalAlignment=TextAlignment.Right));
  connect(bou_air.T_in, switch2.y)
    annotation (Line(points={{-102,56},{-113,56}}, color={0,0,127}));
  connect(switch2.u2, AirOrSoil.y)
    annotation (Line(points={{-136,56},{-157.4,56}}, color={255,0,255}));
  connect(hea.u, sigBusGen.hr_on) annotation (Line(points={{18.8,89.6},{18.8,74},
          {2,74},{2,98}},        color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(sigBusGen.hr_on, isOnHR.u) annotation (Line(
      points={{2,98},{2,60},{46,60},{46,43.2},{40,43.2}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(KPIWel.u, sigBusGen.hp_bus.PelMea) annotation (Line(points={{-79.32,
          -95},{-110,-95},{-110,98},{2,98}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(KPIWel.KPIBus, outBusGen.WHPel) annotation (Line(
      points={{-65.88,-95},{0,-95},{0,-100}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(hea.Pel, KPIWHRel.u) annotation (Line(points={{55.6,89.6},{54,89.6},{
          54,100},{76,100},{76,97},{84.68,97}},
                                    color={0,0,127}));
  connect(KPIWHRel.KPIBus, outBusGen.WHRel) annotation (Line(
      points={{98.12,97},{116,97},{116,-108},{0,-108},{0,-100}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(isOnHR.y, or1.u[1]) annotation (Line(points={{40,29.4},{40,26},{48.8,
          26},{48.8,20}},
                      color={255,0,255}));
  connect(isOnHP.y, or1.u[2]) annotation (Line(points={{16,31.4},{16,24},{46,24},
          {46,20}}, color={255,0,255}));
  connect(constRunPump247.y, or1.u[3]) annotation (Line(points={{73.5,27},{48.1,
          27},{48.1,20},{43.2,20}}, color={255,0,255}));
  connect(pump.port_a, portGen_in[1]) annotation (Line(
      points={{26,-68},{100,-68},{100,-2}},
      color={0,127,255},
      pattern=LinePattern.Dash));

  connect(passThroughMediumHRBuf.port_a, heatPump.port_b1) annotation (Line(
        points={{32,80},{-30,80},{-30,78},{-30.5,78},{-30.5,37}},
                                                color={0,127,255}));
  connect(TSoil.y, switch2.u3) annotation (Line(points={{-159.4,30},{-146,30},{
          -146,48},{-136,48}},  color={0,0,127}));
  connect(heatPump.port_b1, hea.port_a) annotation (Line(points={{-30.5,37},{
          -30.5,80},{22,80}}, color={0,127,255}));
  connect(bouPumpHP.ports[1], pump.port_a) annotation (Line(
      points={{52,-76},{52,-68},{26,-68}},
      color={0,127,255},
      pattern=LinePattern.Dash));
  connect(hea.port_b, passThroughMediumHRBuf.port_a)
    annotation (Line(points={{54,80},{32,80}}, color={0,127,255}));
  connect(senTBuiSup.T, sigBusGen.THeaRodMea) annotation (Line(points={{71,86.6},
          {71,98},{2,98}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-3,6},{-3,6}},
      horizontalAlignment=TextAlignment.Right));
  connect(senTBuiSup.port_b, portGen_out[1])
    annotation (Line(points={{76,80},{100,80}}, color={0,127,255}));
  connect(passThroughMediumHRBuf.port_a, hea.port_a)
    annotation (Line(points={{32,80},{22,80}}, color={0,127,255}));
  connect(passThroughMediumHRBuf.port_b, senTBuiSup.port_a)
    annotation (Line(points={{44,80},{66,80}}, color={0,127,255}));
  connect(hea.port_b, senTBuiSup.port_a)
    annotation (Line(points={{54,80},{66,80}}, color={0,127,255}));

  connect(realToElecCon.internalElectricalPin, internalElectricalPin)
    annotation (Line(
      points={{89.8,-78.2},{72,-78.2},{72,-100}},
      color={0,0,0},
      thickness=1));
  connect(multiSum.y, realToElecCon.PEleLoa)
    annotation (Line(points={{122.98,-82},{112,-82}}, color={0,0,127}));
  if use_heaRod and use_pressure then
    connect(multiSum.u[2], hea.Pel) annotation (Line(points={{136,-82},{142,-82},
          {142,89.6},{55.6,89.6}},color={0,0,127}));
    connect(multiSum.u[1], sigBusGen.hp_bus.PelMea) annotation (Line(points={{136,
          -82},{140,-82},{140,96},{72,96},{72,98},{2,98}}, color={0,0,127}),
      Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
    connect(multiSum.u[3], pump.P) annotation (Line(points={{136,-82},{140,-82},{
          140,-86},{144,-86},{144,-59},{5,-59}},
                                             color={0,0,127}));
  elseif use_heaRod then
    connect(multiSum.u[2], hea.Pel) annotation (Line(points={{136,-82},{142,-82},
          {142,89.6},{55.6,89.6}},color={0,0,127}));
    connect(multiSum.u[1], sigBusGen.hp_bus.PelMea) annotation (Line(points={{136,
          -82},{140,-82},{140,96},{72,96},{72,98},{2,98}}, color={0,0,127}),
      Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  elseif use_pressure then
    connect(multiSum.u[2], pump.P) annotation (Line(points={{136,-82},{140,-82},{
          140,-86},{144,-86},{144,-59},{5,-59}},
                                             color={0,0,127}));
    connect(multiSum.u[1], sigBusGen.hp_bus.PelMea) annotation (Line(points={{136,
          -82},{140,-82},{140,96},{72,96},{72,98},{2,98}}, color={0,0,127}),
      Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  else
    connect(multiSum.u[1], sigBusGen.hp_bus.PelMea) annotation (Line(points={{136,
          -82},{140,-82},{140,96},{72,96},{72,98},{2,98}}, color={0,0,127}),
      Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  end if;
  connect(icing_factor.y, heatPump.iceFac_in) annotation (Line(points={{-85.4,8},
          {-76,8},{-76,4},{-74.6,4},{-74.6,-1.72}}, color={0,0,127}));
  connect(T_con_amb_mea.y, heatPump.T_amb_con) annotation (Line(points={{-43.4,
          66},{-20,66},{-20,44},{-21.5,44},{-21.5,39.2}}, color={0,0,127}));
  connect(sigBusGen.hp_bus.nSet, heatPump.nSet) annotation (Line(
      points={{2,98},{-10,98},{-10,-28},{-39.5,-28},{-39.5,-10.52}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-3,6},{-3,6}},
      horizontalAlignment=TextAlignment.Right));
  connect(sigBusGen.hp_bus.modeSet, heatPump.modeSet) annotation (Line(
      points={{2,98},{2,-10.52},{-48.5,-10.52}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(Pel.y, sigBusGen.hp_bus.PelMea) annotation (Line(points={{-73,-60},{
          -2,-60},{-2,-24},{2,-24},{2,98}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(OnOff.y, sigBusGen.hp_bus.onOffMea) annotation (Line(points={{-43,-76},
          {-26,-76},{-26,6},{2,6},{2,98}}, color={255,0,255}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(pump.port_b, heatPump.port_a1) annotation (Line(points={{6,-68},{-14,
          -68},{-14,-66},{-30.5,-66},{-30.5,-7}}, color={0,127,255}));
  connect(switch1.y, pump.m_flow_in) annotation (Line(points={{46,-27},{44,-27},
          {44,-44},{16,-44},{16,-56}}, color={0,0,127}));
  connect(weaBus.TDryBul, switch2.u1) annotation (Line(
      points={{-101,80},{-126,80},{-126,82},{-150,82},{-150,64},{-136,64}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(TConOut.y, sigBusGen.TBoiOut) annotation (Line(points={{-71,-44},{-38,
          -44},{-38,-48},{2,-48},{2,98}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(TConOut.y, sigBusGen.hp_bus.TConOutMea) annotation (Line(points={{-71,
          -44},{-40,-44},{-40,4},{2,4},{2,98}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(TEvaInMeaRe.y, sigBusGen.hp_bus.TEvaInMea) annotation (Line(points={{
          -57,88},{-28,88},{-28,74},{2,74},{2,98}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(heatPump.sigBus, sigBusGen) annotation (Line(
      points={{-52.775,-6.78},{2,-6.78},{2,98}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
end HPC;
