within MPC.Submodules;
model Model_inputs
  outer Modelica.Blocks.Interfaces.RealInput u_T_VL_outer
    annotation (Placement(transformation(extent={{-146,-10},{-106,30}})));
  outer Modelica.Blocks.Interfaces.BooleanInput b_u_comp_ctrl
    annotation (Placement(transformation(extent={{-146,22},{-106,62}})));
  outer Modelica.Blocks.Interfaces.BooleanInput b_MPC_input
    annotation (Placement(transformation(extent={{-146,56},{-106,96}})));
  outer Modelica.Blocks.Interfaces.RealInput u_comp_outer
    annotation (Placement(transformation(extent={{-146,-44},{-106,-4}})));
  Modelica.Blocks.Logical.And and_u_comp
    annotation (Placement(transformation(extent={{-68,54},{-48,74}})));
  Modelica.Blocks.Logical.Switch MPC_Switch
    annotation (Placement(transformation(extent={{24,24},{44,44}})));
  Modelica.Blocks.Interfaces.RealInput u_comp_PID
    annotation (Placement(transformation(extent={{-146,-108},{-106,-68}})));
  Modelica.Blocks.Logical.Switch T_set_Switch
    annotation (Placement(transformation(extent={{50,-84},{70,-64}})));
  Modelica.Blocks.Logical.Or or_b_MPC__b_u_comp
    annotation (Placement(transformation(extent={{2,-60},{22,-40}})));
  Modelica.Blocks.Logical.Not not_b_MPC
    annotation (Placement(transformation(extent={{-38,-4},{-18,16}})));
  Modelica.Blocks.Interfaces.RealInput T_set_HC
    annotation (Placement(transformation(extent={{-146,-164},{-106,-124}})));
  Modelica.Blocks.Interfaces.RealOutput n_Set
    annotation (Placement(transformation(extent={{212,-4},{232,16}})));
  Modelica.Blocks.Interfaces.RealOutput T_VL_set
    annotation (Placement(transformation(extent={{210,-86},{230,-66}})));
  outer Modelica.Blocks.Interfaces.BooleanInput b_Valve_PI
    annotation (Placement(transformation(extent={{-146,96},{-106,136}})));
  Modelica.Blocks.Interfaces.BooleanOutput b_valve_pi_out
    annotation (Placement(transformation(extent={{208,106},{228,126}})));
  Modelica.Blocks.Logical.Switch n_Set_switch_fmu
    annotation (Placement(transformation(extent={{152,-4},{172,16}})));
  Modelica.Blocks.Logical.Switch T_set_switch_fmu
    annotation (Placement(transformation(extent={{142,-86},{162,-66}})));
  Modelica.Blocks.Logical.LogicalSwitch b_valve_switch_fmu
    annotation (Placement(transformation(extent={{140,106},{160,126}})));
  Modelica.Blocks.Sources.BooleanExpression valve_pi_inner(y=false)
    annotation (Placement(transformation(extent={{38,88},{58,108}})));
        Modelica.Blocks.Interfaces.BooleanInput b_FMU_input
    annotation (Placement(transformation(extent={{-144,142},{-104,182}})));
equation
  connect(and_u_comp.u1, b_MPC_input) annotation (Line(points={{-70,64},{-80,
          64},{-80,76},{-126,76}}, color={255,0,255}));
  connect(and_u_comp.u2, b_u_comp_ctrl) annotation (Line(points={{-70,56},{
          -88,56},{-88,42},{-126,42}}, color={255,0,255}));
  connect(and_u_comp.y, MPC_Switch.u2) annotation (Line(points={{-47,64},{16,
          64},{16,34},{22,34}}, color={255,0,255}));
  connect(u_comp_outer, MPC_Switch.u1) annotation (Line(points={{-126,-24},{
          -52,-24},{-52,42},{22,42}}, color={0,0,127}));
  connect(MPC_Switch.u3, u_comp_PID) annotation (Line(points={{22,26},{-42,26},
          {-42,-88},{-126,-88}}, color={0,0,127}));
  connect(or_b_MPC__b_u_comp.y, T_set_Switch.u2) annotation (Line(points={{23,
          -50},{30,-50},{30,-74},{48,-74}}, color={255,0,255}));
  connect(not_b_MPC.u, b_MPC_input) annotation (Line(points={{-40,6},{-92,6},
          {-92,76},{-126,76}}, color={255,0,255}));
  connect(not_b_MPC.y, or_b_MPC__b_u_comp.u1) annotation (Line(points={{-17,6},
          {-10,6},{-10,-50},{0,-50}}, color={255,0,255}));
  connect(or_b_MPC__b_u_comp.u2, b_u_comp_ctrl) annotation (Line(points={{0,
          -58},{-44,-58},{-44,42},{-126,42}}, color={255,0,255}));
  connect(T_set_Switch.u3, u_T_VL_outer) annotation (Line(points={{48,-82},{
          28,-82},{28,-32},{-26,-32},{-26,-30},{-96,-30},{-96,10},{-126,10}},
        color={0,0,127}));
  connect(T_set_Switch.u1, T_set_HC) annotation (Line(points={{48,-66},{-30,
          -66},{-30,-144},{-126,-144}}, color={0,0,127}));
  connect(T_VL_set, T_VL_set)
    annotation (Line(points={{220,-76},{220,-76}}, color={0,0,127}));
  connect(MPC_Switch.y, n_Set_switch_fmu.u1) annotation (Line(points={{45,34},
          {142,34},{142,14},{150,14}}, color={0,0,127}));
  connect(n_Set_switch_fmu.y, n_Set)
    annotation (Line(points={{173,6},{222,6}}, color={0,0,127}));
  connect(n_Set_switch_fmu.u3, u_comp_PID) annotation (Line(points={{150,-2},
          {54,-2},{54,-44},{-42,-44},{-42,-88},{-126,-88}}, color={0,0,127}));
  connect(T_set_switch_fmu.u2, n_Set_switch_fmu.u2) annotation (Line(points={
          {140,-76},{134,-76},{134,6},{150,6}}, color={255,0,255}));
  connect(T_set_switch_fmu.y, T_VL_set)
    annotation (Line(points={{163,-76},{220,-76}}, color={0,0,127}));
  connect(T_set_Switch.y, T_set_switch_fmu.u1) annotation (Line(points={{71,
          -74},{136,-74},{136,-68},{140,-68}}, color={0,0,127}));
  connect(T_set_switch_fmu.u3, T_set_HC) annotation (Line(points={{140,-84},{
          110,-84},{110,-128},{-30,-128},{-30,-144},{-126,-144}}, color={0,0,
          127}));
  connect(b_Valve_PI, b_valve_switch_fmu.u1) annotation (Line(points={{-126,
          116},{122,116},{122,124},{138,124}}, color={255,0,255}));
  connect(b_valve_switch_fmu.y, b_valve_pi_out)
    annotation (Line(points={{161,116},{218,116}}, color={255,0,255}));
  connect(valve_pi_inner.y, b_valve_switch_fmu.u3)
    annotation (Line(points={{59,98},{130,98},{130,108},{138,108}},
                                                  color={255,0,255}));
  connect(b_valve_switch_fmu.u2, b_FMU_input) annotation (Line(points={{138,
          116},{132,116},{132,162},{-124,162}}, color={255,0,255}));
  connect(n_Set_switch_fmu.u2, b_FMU_input) annotation (Line(points={{150,6},
          {132,6},{132,162},{-124,162}}, color={255,0,255}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -160},{180,180}})), Diagram(coordinateSystem(preserveAspectRatio=
            false, extent={{-100,-160},{180,180}})));
end Model_inputs;
