within MPC.DataBase;
record MPC_Zone_SingleDwelling_2_Floors_110
  "MPC_Zone_SingleDwelling_2_Floors_110"
  extends AixLib.DataBase.ThermalZones.ZoneBaseRecord(
    T_start = 293.15,
    withAirCap = true,
    VAir = 275.0,
    AZone = 110.0,
    hRad = 5.0,
    lat = 0.88645272708792,
    nOrientations = 4,
    AWin = {5.445, 5.445, 5.445, 5.445},
    ATransparent = {5.445, 5.445, 5.445, 5.445},
    hConWin = 2.7,
    RWin = 0.006542699724517906,
    gWin = 0.75,
    UWin= 3.2013304230329482,
    ratioWinConRad = 0.02,
    AExt = {38.7475, 38.7475, 38.7475, 38.7475},
    hConExt = 2.7,
    nExt = 1,
    RExt = {0.002041725133293758},
    RExtRem = 0.010217130276732695,
    CExt = {6199746.512962887},
    AInt = 339.16666666666663,
    hConInt = 2.375675675675676,
    nInt = 1,
    RInt = {0.00018222820069856784},
    CInt = {37546864.86288327},
    AFloor = 55.22,
    hConFloor = 1.7,
    nFloor = 1,
    RFloor = {0.002184380091645702},
    RFloorRem =  0.028717289031331736,
    CFloor = {6521979.525629826},
    ARoof = 90.343,
    hConRoof = 1.7,
    nRoof = 1,
    RRoof = {0.0006006777486754225},
    RRoofRem = 0.028457136005873473,
    CRoof = {1728601.6009581988},
    nOrientationsRoof = 2,
    tiltRoof = {0.6108652381980153, 0.6108652381980153},
    aziRoof = {3.141592653589793, -1.5707963267948966},
    wfRoof = {0.5, 0.5},
    aRoof = 0.5,
    aExt = 0.5,
    TSoil = 286.15,
    hConWallOut = 20.0,
    hRadWall = 5.0,
    hConWinOut = 20.0,
    hConRoofOut = 19.999999999999996,
    hRadRoof = 4.999999999999999,
    tiltExtWalls = {1.5707963267948966, 1.5707963267948966, 1.5707963267948966, 1.5707963267948966},
    aziExtWalls = {0.0, 1.5707963267948966, -1.5707963267948966, 3.141592653589793},
    wfWall = {0.25, 0.25, 0.25, 0.25},
    wfWin = {0.25, 0.25, 0.25, 0.25},
    wfGro = 0.0,
    specificPeople = 0.02,
    internalGainsMoistureNoPeople = 0.5,
    fixedHeatFlowRatePersons = 70,
    activityDegree = 1.2,
    ratioConvectiveHeatPeople = 0.5,
    internalGainsMachinesSpecific = 2.0,
    ratioConvectiveHeatMachines = 0.75,
    lightingPowerSpecific = 7.0,
    ratioConvectiveHeatLighting = 0.5,
    useConstantACHrate = false,
    baseACH = 0.2,
    maxUserACH = 1.0,
    maxOverheatingACH = {3.0, 2.0},
    maxSummerACH = {1.0, 283.15, 290.15},
    winterReduction = {0.2, 273.15, 283.15},
    maxIrr = {100.0, 100.0, 100.0, 100.0},
    shadingFactor = {1.0, 1.0, 1.0, 1.0},
    withAHU = false,
    minAHU = 0.3,
    maxAHU = 0.6,
    hHeat = 6475.16860006111,
    lHeat = 0,
    KRHeat = 10000,
    TNHeat = 1,
    HeaterOn = false,
    hCool = 0,
    lCool = -6475.16860006111,
    KRCool = 10000,
    TNCool = 1,
    CoolerOn = false,
    withIdealThresholds = false,
    TThresholdHeater = 288.15,
    TThresholdCooler = 295.15,
    heaLoadFacGrd=0,
    heaLoadFacOut=0);
end MPC_Zone_SingleDwelling_2_Floors_110;
