within MPC.Utilities.DHW;
model DHWCalcTab
  "Calculates DHW mass flow and temperature due to tabulated input data"
  parameter Modelica.Units.SI.Temperature TColdWater=283.15
    "Constant temperature assumed for city water"
    annotation (Dialog(group="Water properties"));
  parameter Modelica.Units.SI.SpecificHeatCapacity cp=4180
    "Specific heat capacity of liquid water"
    annotation (Dialog(group="Water properties"));
  parameter String tableOutName[5] = {"QTap","mFlow","Tm","Tp","tapNo"} "Names for the variables in the dhw *.nc file";
  parameter Modelica.Units.SI.Time preAnnTime_m_flowIdeal=240
    "Time period prior current simulation time"
    annotation (Dialog(group="Pre Announced mFlowSet"));
  Modelica.Blocks.Interfaces.RealInput mFlowMes(
                                               unit="kg/s")
    "Mass flow coming from the test rig as a measured input"
    annotation (Placement(transformation(extent={{-120,54},{-100,74}}), iconTransformation(extent={{-120,44},{-80,84}})));
  Modelica.Blocks.Interfaces.RealInput TDHWMes(unit="K", displayUnit="degC")
    "Temperature (hot water) coming from the DHW tank"
    annotation (Placement(transformation(extent={{-120,-10},{-100,10}}), iconTransformation(extent={{-120,-20},{-80,20}})));
  Modelica.Blocks.Interfaces.RealInput TColdMes(
                                               unit="K", displayUnit="degC")
    "Temperature of cold water coming from the test rig"
    annotation (Placement(transformation(extent={{-120,-70},{-100,-50}}), iconTransformation(extent={{-120,-86},{-80,-46}})));
  Modelica.Blocks.Logical.GreaterEqual greaterEqualIntegratedEnergy
    "if true then tapping was successfull in terms of reaching demanded energy value."
    annotation (Placement(transformation(extent={{62,34},{74,48}})));
  Modelica.Blocks.Interfaces.RealOutput mFlowDHWTank(unit="kg/s")
    "Mass flow rate that has to be provided by the DHW tank"
    annotation (Placement(transformation(extent={{100,30},{120,50}}), iconTransformation(extent={{100,30},{120,50}})));
  Modelica.Blocks.Interfaces.RealOutput TColdFromParam(unit="K", displayUnit="degC")
    "Cold water temperature coming from district water"
    annotation (Placement(transformation(extent={{100,-50},{120,-30}}), iconTransformation(extent={{100,-50},{120,-30}})));
  discrete Modelica.Blocks.Interfaces.IntegerOutput counterTaps(fixed=true,start = 0)
    "Number of started taps"
    annotation (Placement(transformation(extent={{100,-70},{120,-50}}), iconTransformation(extent={{100,-70},{120,-50}})));
  discrete Modelica.Blocks.Interfaces.IntegerOutput counterEnergy(fixed=true,start = 0)
    "Number of successful taps with reached desired energy value"
    annotation (Placement(transformation(extent={{100,-86},{120,-66}}), iconTransformation(extent={{100,-86},{120,-66}})));
  discrete Modelica.Blocks.Interfaces.IntegerOutput counterTMax(fixed=true, start = 0)
    "Number of successful taps with reached desired maximal temperature"
    annotation (Placement(transformation(extent={{100,-102},{120,-82}}), iconTransformation(extent={{100,-102},{120,-82}})));
  Modelica.Blocks.Sources.Constant TColdDHW(          y(unit="K"), k=TColdWater)
    annotation (Placement(transformation(extent={{-70,-40},{-62,-32}})));
  Modelica.Blocks.Math.Gain specHeatCap(k=4180, y(unit="J/(s.K)"))
    "assumed as constant"
    annotation (Placement(transformation(extent={{-46,60},{-38,68}})));
  Modelica.Blocks.Math.Add diffTempEn(k2=-1)
    annotation (Placement(transformation(extent={{-42,30},{-34,38}})));
  Modelica.Blocks.Math.Product productEn
    annotation (Placement(transformation(extent={{-16,36},{-8,44}})));
  IntegratorWithReset                                integrator(
    k=1,
    initType=Modelica.Blocks.Types.Init.InitialOutput,
    withResetIntegrator=true,
    y_start=Modelica.Constants.inf,
    yReset=0) annotation (Placement(transformation(extent={{46,22},{54,30}})));
  Modelica.Blocks.Logical.Switch switchQInt
    annotation (Placement(transformation(extent={{68,-20},{78,-10}})));
  CalcDHWmFlow                                calcDHWmFlow(tempThr=273.15 + min(
        DHWTable.table[:, 4]))                             annotation (Placement(transformation(rotation=0, extent={{-2,-32},{18,-12}})));
  Boolean reachedTMax(start=false);
  Modelica.Blocks.Sources.BooleanExpression conditionToIntegrateBoolExpr(y=((
        greaterEqualIntegratedEnergy.u2 >= greaterEqualIntegratedEnergy.u1)
         and (TDHWMes >= delayTableOutputs[3].y)))
    annotation (Placement(transformation(extent={{-4,-6},{16,14}})));
  Modelica.Blocks.Sources.Constant mFlowZero(k=0, y(unit="kg/s"))
    annotation (Placement(transformation(extent={{46,-30},{52,-24}})));
  Modelica.Blocks.Logical.Not not1
    annotation (Placement(transformation(extent={{-5,-5},{5,5}},
        rotation=-90,
        origin={63,7})));
  Modelica.Blocks.Interfaces.RealOutput DHWTableOutputs_preDelteTime[size(
    DHWTable.y, 1)] "Acc. to EN 16147
1: QTap [J]
2: mFlow [kg/s]
3: Tm [K]
4: Tp [K]
5: drawOffNum [-]" annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={0,-112}), iconTransformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={40,-110})));
  Modelica.Blocks.Routing.RealPassThrough
                                       delayTableOutputs[size(
    DHWTable.y, 1)]
    annotation (Placement(transformation(extent={{-38,4},{-30,12}})));

  Modelica.Blocks.Interfaces.BooleanOutput THotMeaAboveTm annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={80,-110}), iconTransformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={80,-110})));
  Modelica.Blocks.Sources.CombiTimeTable DHWTable(
    tableOnFile=false,
    final table=DHWProfile,
    smoothness=Modelica.Blocks.Types.Smoothness.ConstantSegments,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    offset={0,0,273.15,273.15})
               annotation (Placement(transformation(extent={{-78,-6},{-58,14}})));
  parameter Real DHWProfile[:,:]=fill(
      0.0,
      0,
      5) "Table matrix (time = first column; e.g., table=[0, 0; 1, 1; 2, 4])";
  BooleanTrigger booleanTrigger
    annotation (Placement(transformation(extent={{8,10},{28,30}})));
  Modelica.Blocks.Math.Gain kWhToJ(k=3600*1000)
    annotation (Placement(transformation(extent={{-50,14},{-44,20}})));
  TriggerQ triggerQ(x=0)
    annotation (Placement(transformation(extent={{24,48},{42,64}})));
  Modelica.Blocks.Sources.CombiTimeTable DHWTableShift(
    tableOnFile=false,
    final table=DHWProfile,
    smoothness=Modelica.Blocks.Types.Smoothness.ConstantSegments,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    offset={0,0,273.15,273.15},
    shiftTime=-preAnnTime_m_flowIdeal)
    annotation (Placement(transformation(extent={{-70,-72},{-50,-52}})));
  Modelica.Blocks.Math.Gain kWhToJ1(k=3600*1000)
    annotation (Placement(transformation(extent={{-36,-66},{-30,-60}})));
protected
  Modelica.Blocks.Logical.Greater greater annotation (Placement(transformation(extent={{44,-90},{50,-84}})));
  Modelica.Blocks.Logical.And and1 annotation (Placement(transformation(extent={{52,-74},{58,-68}})));
  Modelica.Blocks.Logical.GreaterThreshold greaterThresholdMFlowMea(final threshold=0) annotation (Placement(transformation(extent={{44,-80},{50,-74}})));
  Modelica.Blocks.Logical.GreaterThreshold greaterThresholdMFlowSet(final threshold=0) annotation (Placement(transformation(extent={{44,-72},{50,-66}})));
  Modelica.Blocks.Logical.And and2 annotation (Placement(transformation(extent={{60,-84},{66,-78}})));
algorithm
//   when edge1.y then
//     counterTaps := pre(counterTaps)+1;
//     reachedTMax := false;
//   end when;
  when mFlowMes > 0 and TDHWMes >= DHWTable.y[4] and not greaterEqualIntegratedEnergy.y then
    counterTMax := if not reachedTMax then pre(counterTMax) + 1 else pre(
    counterTMax);
    reachedTMax := true;
  end when;
  when greaterEqualIntegratedEnergy.y then
    counterEnergy :=pre(counterEnergy) + 1;
  end when;
equation

  connect(TColdDHW.y, calcDHWmFlow.TCold) annotation (Line(points={{-61.6,-36},{
          -44,-36},{-44,-30.4},{-2,-30.4}}, color={0,0,127}));
  connect(TDHWMes, calcDHWmFlow.TTankDHWHotMes) annotation (Line(points={{-110,0},{-80,0},{-80,-26},{-30,-26},{-30,-24},{-2.2,-24}},
                                                             color={0,0,127}));
  connect(switchQInt.y, mFlowDHWTank) annotation (Line(points={{78.5,-15},{84,-15},{84,40},{110,40}},
                             color={0,0,127}));
  connect(TColdDHW.y, TColdFromParam) annotation (Line(points={{-61.6,-36},{-44,-36},{-44,-40},{110,-40}},
                                     color={0,0,127}));
  connect(productEn.y, integrator.u)
    annotation (Line(points={{-7.6,40},{42,40},{42,26},{45.2,26}},
                                                             color={0,0,127}));
  connect(TDHWMes, diffTempEn.u1) annotation (Line(points={{-110,0},{-80,0},{-80,36},{-50,36},{-50,36.4},{-42.8,36.4}},
                                           color={0,0,127}));
  connect(mFlowMes, specHeatCap.u)
    annotation (Line(points={{-110,64},{-46.8,64}},
                                                  color={0,0,127}));
  connect(diffTempEn.y, productEn.u2) annotation (Line(points={{-33.6,34},{-24,34},
          {-24,37.6},{-16.8,37.6}},
                                  color={0,0,127}));
  connect(specHeatCap.y, productEn.u1) annotation (Line(points={{-37.6,64},{-26,
          64},{-26,42.4},{-16.8,42.4}},
                                      color={0,0,127}));
  connect(TColdMes, diffTempEn.u2) annotation (Line(points={{-110,-60},{-76,-60},{-76,31.6},{-42.8,31.6}},
                                            color={0,0,127}));
  connect(integrator.y, greaterEqualIntegratedEnergy.u1) annotation (Line(
        points={{54.4,26},{58,26},{58,41},{60.8,41}}, color={0,0,127}));
  connect(mFlowZero.y, switchQInt.u3) annotation (Line(points={{52.3,-27},{59.15,
          -27},{59.15,-19},{67,-19}}, color={0,0,127}));
  connect(calcDHWmFlow.mFlowCalcedDHWSet, switchQInt.u1) annotation (Line(
        points={{18,-22},{42,-22},{42,-11},{67,-11}}, color={0,0,127}));
  connect(greaterEqualIntegratedEnergy.y, not1.u) annotation (Line(points={{74.6,41},
          {78,41},{78,16},{64,16},{64,13},{63,13}},     color={255,0,255}));
  connect(not1.y, switchQInt.u2)
    annotation (Line(points={{63,1.5},{63,-15},{67,-15}}, color={255,0,255}));
  connect(conditionToIntegrateBoolExpr.y, integrator.condToIntegrate)
    annotation (Line(points={{17,4},{48,4},{48,22}}, color={255,0,255}));
  connect(delayTableOutputs[2].y, calcDHWmFlow.mFlowFromTable) annotation (Line(
        points={{-29.6,8},{-20,8},{-20,-14},{-2,-14}}, color={0,0,127}));
  connect(delayTableOutputs[4].y, calcDHWmFlow.Tp) annotation (Line(points={{-29.6,8},
          {-10,8},{-10,-20},{-2,-20}},    color={0,0,127}));
  connect(mFlowMes, greaterThresholdMFlowMea.u) annotation (Line(points={{-110,64},{-94,64},{-94,-77},{43.4,-77}}, color={0,0,127}));
  connect(and1.u2, greaterThresholdMFlowMea.y) annotation (Line(points={{51.4,-73.4},{50,-73.4},{50,-77},{50.3,-77}}, color={255,0,255}));
  connect(greaterThresholdMFlowSet.y, and1.u1) annotation (Line(points={{50.3,-69},{50.3,-70.5},{51.4,-70.5},{51.4,-71}}, color={255,0,255}));
  connect(switchQInt.y, greaterThresholdMFlowSet.u) annotation (Line(points={{78.5,-15},{80,-15},{80,-56},{42,-56},{42,-69},{43.4,-69}}, color={0,0,127}));
  connect(and1.y, and2.u1) annotation (Line(points={{58.3,-71},{58.3,-76.5},{59.4,-76.5},{59.4,-81}}, color={255,0,255}));
  connect(greater.y, and2.u2) annotation (Line(points={{50.3,-87},{55.15,-87},{55.15,-83.4},{59.4,-83.4}}, color={255,0,255}));
  connect(and2.y, THotMeaAboveTm) annotation (Line(points={{66.3,-81},{80,-81},{80,-110}}, color={255,0,255}));
  connect(TDHWMes, greater.u1) annotation (Line(points={{-110,0},{-96,0},{-96,-87},{43.4,-87}}, color={0,0,127}));
  connect(delayTableOutputs[3].y, greater.u2) annotation (Line(points={{-29.6,8},
          {-24,8},{-24,-89.4},{43.4,-89.4}},                                                                        color={0,0,127}));
  connect(booleanTrigger.counter, counterTaps) annotation (Line(points={{28.6,
          26},{32,26},{32,-62},{110,-62},{110,-60}}, color={255,127,0}));
  connect(booleanTrigger.trigger, integrator.resetI) annotation (Line(points={{
          28.6,20},{28.6,23.36},{45.2,23.36}}, color={255,0,255}));
  connect(booleanTrigger.m_flowSet, delayTableOutputs[2].y) annotation (Line(
        points={{7.8,20.4},{-29.6,20.4},{-29.6,8}}, color={0,0,127}));
  connect(DHWTable.y[1], kWhToJ.u)
    annotation (Line(points={{-57,4},{-57,17},{-50.6,17}}, color={0,0,127}));
  connect(kWhToJ.y, delayTableOutputs[1].u) annotation (Line(points={{-43.7,17},
          {-38.8,17},{-38.8,8}}, color={0,0,127}));
  connect(DHWTable.y[2:4], delayTableOutputs[2:4].u)
    annotation (Line(points={{-57,4},{-57,8},{-38.8,8}}, color={0,0,127}));
  connect(delayTableOutputs[1].y, triggerQ.u) annotation (Line(points={{-29.6,8},
          {-18,8},{-18,18},{0,18},{0,56},{22.2,56}}, color={0,0,127}));
  connect(triggerQ.y, greaterEqualIntegratedEnergy.u2) annotation (Line(points=
          {{42.9,56},{52,56},{52,35.4},{60.8,35.4}}, color={0,0,127}));
  connect(DHWTableShift.y[1], kWhToJ1.u) annotation (Line(points={{-49,-62},{
          -42.8,-62},{-42.8,-63},{-36.6,-63}}, color={0,0,127}));
  connect(kWhToJ1.y, DHWTableOutputs_preDelteTime[1])
    annotation (Line(points={{-29.7,-63},{0,-63},{0,-112}}, color={0,0,127}));
  connect(DHWTableShift.y[2:4], DHWTableOutputs_preDelteTime[2:4]) annotation (
      Line(points={{-49,-62},{-46,-62},{-46,-66},{0,-66},{0,-112}}, color={0,0,
          127}));
                                                                                                            annotation(Dialog(group="Pre Announced mFlowSet"),
    Icon(coordinateSystem(preserveAspectRatio=false), graphics={
      Polygon(
        points={{48,14},{39,-10},{58,-10},{48,14}},
        lineColor={0,0,255},
        smooth=Smooth.None,
        fillColor={0,0,255},
        fillPattern=FillPattern.Solid),
      Ellipse(
        extent={{39,-2},{58,-22}},
        lineColor={0,0,255},
        fillColor={0,0,255},
        fillPattern=FillPattern.Solid),
      Polygon(
        points={{36,-22},{27,-46},{46,-46},{36,-22}},
        lineColor={0,0,255},
        smooth=Smooth.None,
        fillColor={0,0,255},
        fillPattern=FillPattern.Solid),
      Ellipse(
        extent={{27,-38},{46,-58}},
        lineColor={0,0,255},
        fillColor={0,0,255},
        fillPattern=FillPattern.Solid),
      Polygon(
        points={{68,-12},{59,-36},{78,-36},{68,-12}},
        lineColor={0,0,255},
        smooth=Smooth.None,
        fillColor={0,0,255},
        fillPattern=FillPattern.Solid),
      Ellipse(
        extent={{59,-28},{78,-48}},
        lineColor={0,0,255},
        fillColor={0,0,255},
        fillPattern=FillPattern.Solid),
      Polygon(
        points={{58,-50},{49,-74},{68,-74},{58,-50}},
        lineColor={0,0,255},
        smooth=Smooth.None,
        fillColor={0,0,255},
        fillPattern=FillPattern.Solid),
      Ellipse(
        extent={{49,-66},{68,-86}},
        lineColor={0,0,255},
        fillColor={0,0,255},
        fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-88,80},{26,40}},
          lineColor={95,95,95},
          fillPattern=FillPattern.HorizontalCylinder,
          fillColor={175,175,175}),
        Rectangle(
          extent={{-39,27},{39,-27}},
          lineColor={95,95,95},
          fillPattern=FillPattern.HorizontalCylinder,
          fillColor={175,175,175},
          origin={53,41},
          rotation=-90)}),
    Diagram(coordinateSystem(preserveAspectRatio=false), graphics={Text(
          extent={{-186,64},{-172,38}},
          lineColor={95,95,95},
          fillPattern=FillPattern.HorizontalCylinder,
          fillColor={175,175,175},
          horizontalAlignment=TextAlignment.Left,
          textString="Acc. to EN 16147
1: QTap [J]
2: mFlow [kg/s]
3: Tm [K]
4: Tp [K]
5: drawOffNum [-]")}),
    Documentation(info="<html>
<p>Acc. to EN 16147</p>
<p>Table input data already with converted unit (densitiy of water is assumed to 1 kg/l)</p>
</html>"),
    experiment(
      StopTime=86400),
    __Dymola_experimentSetupOutput);
end DHWCalcTab;
