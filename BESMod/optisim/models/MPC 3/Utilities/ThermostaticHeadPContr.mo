within MPC.Utilities;
model ThermostaticHeadPContr "Thermostatic Head without offset error."
  parameter Modelica.Units.SI.TemperatureDifference hysteresis=0.5 "setTemp + hysteresis --> y = 1; 
    setTemp - hysteresis --> y = 0 (without time delay)";
    parameter Real k(min=0, unit="1") = 1 "Gain of controller";
  parameter Modelica.Units.SI.Time Ti(min=Modelica.Constants.small) = 0.5
    "Time constant of Integrator block" annotation (Dialog(enable=
          controllerType == .Modelica.Blocks.Types.SimpleController.PI or
          controllerType == .Modelica.Blocks.Types.SimpleController.PID));
  parameter Modelica.Units.SI.Time Td(min=0) = 0.1
    "Time constant of Derivative block" annotation (Dialog(enable=
          controllerType == .Modelica.Blocks.Types.SimpleController.PD or
          controllerType == .Modelica.Blocks.Types.SimpleController.PID));
  parameter .Modelica.Blocks.Types.SimpleController controllerType=
         .Modelica.Blocks.Types.SimpleController.PID "Type of controller";
  parameter Modelica.Units.SI.Time timeDelayActTemp=60
    "Time delay until actual room temperature reaches actual controller";
  parameter Modelica.Blocks.Types.Init initType=Modelica.Blocks.Types.Init.NoInit
    "Type of initialization (1: no init, 2: steady state, 3: initial state, 4: initial output)"
    annotation (Dialog(group="Initialization"));
  parameter Real y_start(
    min=0.0,
    max=1.0) = 1.0 "Set start position of valve opening"
   annotation (Dialog(
        enable=initType == Modelica.Blocks.Types.Init.InitialOutput,
        group="Initialization"));
  Modelica.Blocks.Interfaces.RealInput actualTemp(unit="K", displayUnit="degC")
    "Actual (room) temperature"
    annotation (Placement(transformation(
        origin={-110,0},
        extent={{-10,-10},{10,10}},
        rotation=0), iconTransformation(extent={{-120,-10},{-100,10}})));
  Modelica.Blocks.Interfaces.RealInput setTemp(unit="K", displayUnit="degC")
    "Target temperature"
    annotation (Placement(transformation(
        origin={20,110},
        extent={{-10,-10},{10,10}},
        rotation=270), iconTransformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={0,110})));
  Modelica.Blocks.Interfaces.RealOutput y(min=0, max=1) "spool position H=0..1"
    annotation (Placement(transformation(
        origin={0,-104},
        extent={{-10,-10},{10,10}},
        rotation=270), iconTransformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={0,-110})));
  Modelica.Blocks.Math.Feedback tempDiffActual
    "Positiv temp diff means room warmer than set temp"
    annotation (Placement(
        transformation(
        extent={{-10,10},{10,-10}},
        rotation=270,
        origin={8,60})));
  Modelica.Blocks.Nonlinear.FixedDelay fixedDelay(
    final delayTime=timeDelayActTemp)
    "Time delay until actual room temperature reaches fluid in the thermostatic head"
    annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={-22,60})));
  Modelica.Blocks.Math.Feedback tempDiffWithDelay
    "Positiv temp diff means room warmer than set temp (be careful with time delay)"
    annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=270,
        origin={8,16})));
  Modelica.Blocks.Sources.Constant minusHalfHysteresis(
    final k=hysteresis)
    "Half of the hysteresis bandwidth"
    annotation (Placement(transformation(extent={{-40,-44},{-26,-30}})));

  Modelica.Blocks.Continuous.LimPID PContr(
    u_s(unit="K"),
    u_m(unit="K"),
    final controllerType=controllerType,
    final k=k/hysteresis,
    final Ti=Ti,
    final Td=Td,
    final initType=initType,
    final y_start=y_start,
    final yMax=1,
    final yMin=0)
    "Actual controller"
    annotation (Placement(transformation(extent={{28,-46},{8,-66}})));

protected
  Modelica.Blocks.Sources.Constant zeroDiff(
    final k(unit="K") = 0)
    "Target deviation"
    annotation (Placement(transformation(extent={{60,-54},{46,-40}})));
  Modelica.Blocks.Math.Add minus(
    final k2=-1)
    "Minus half the hysteresis bandwidth"
    annotation (Placement(transformation(extent={{-8,-30},{6,-16}})));

equation

  connect(PContr.u_s, zeroDiff.y) annotation (Line(points={{30,-56},{40,-56},{
          40,-47},{45.3,-47}},
                            color={0,0,127}));
  connect(minusHalfHysteresis.y, minus.u2) annotation (Line(points={{-25.3,-37},
          {-21.65,-37},{-21.65,-27.2},{-9.4,-27.2}},  color={0,0,127}));
  connect(tempDiffWithDelay.y, minus.u1) annotation (Line(points={{8,7},{8,0},{
          -22,0},{-22,-18.8},{-9.4,-18.8}},               color={0,0,127}));
  connect(fixedDelay.y, tempDiffWithDelay.u1)
    annotation (Line(points={{-22,49},{-22,24},{8,24}}, color={0,0,127}));
  connect(actualTemp, fixedDelay.u)
    annotation (Line(points={{-110,0},{-86,0},{-86,86},{-22,86},{-22,72}},
                                                           color={0,0,127}));
  connect(setTemp, tempDiffWithDelay.u2) annotation (Line(points={{20,110},{20,98},{22,98},{22,16},{16,16}},
                            color={0,0,127}));
  connect(PContr.y, y)
    annotation (Line(points={{7,-56},{0,-56},{0,-104}}, color={0,0,127}));
  connect(actualTemp, tempDiffActual.u1)
    annotation (Line(points={{-110,0},{-88,0},{-88,88},{8,88},{8,68}},
                                                         color={0,0,127}));
  connect(setTemp, tempDiffActual.u2)
    annotation (Line(points={{20,110},{20,60},{16,60}}, color={0,0,127}));
  connect(minus.y, PContr.u_m) annotation (Line(points={{6.7,-23},{18,-23},{18,-44}}, color={0,0,127}));
  annotation (
    Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{100,
            100}})),
    Documentation(info="<html>
</html>", revisions="<html>
<p><ul>
<li><i>November 22, 2013&nbsp;</i> by Ole Odendahl:<br/>Formatted documentation appropriately</li>
<li><i>June 30, 2011 </i>by Ana Constantin:<br/>Wrote documentation in english</li>
</ul></p>
</html>"),
    Icon(graphics={
        Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={0,0,0},
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid),
        Polygon(points={{-32,76},{32,76},{38,64},{38,-14},{22,-56},{-24,-56},{-38,-12},{-38,64},{-32,76}},
                                           lineColor={0,0,0}),
        Line(points={{-26,60},{-26,-10}}, color={0,0,0}),
        Line(points={{-12,60},{-12,-10}}, color={0,0,0}),
        Line(points={{4,60},{4,-10}}, color={0,0,0}),
        Line(points={{18,60},{18,-10}}, color={0,0,0}),
        Line(points={{30,60},{30,-10}}, color={0,0,0})}));
end ThermostaticHeadPContr;
