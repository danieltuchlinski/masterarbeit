within MPC;
model MA_Kevin_Model_Sim "based on MPC Model"
  extends Partial_MPC_Model(
    hydraulic(
      redeclare BESMod.Systems.Hydraulical.Distribution.BuildingOnly
        distribution(nParallelDem=1),
      redeclare MPC.Submodules.Generation.HPC generation,
      redeclare MPC.Submodules.MonovalentOptihorst control(
        redeclare
          BESMod.Systems.Hydraulical.Control.Components.ThermostaticValveController.ThermostaticValvePIControlled
          thermostaticValveController,
        redeclare
          BESMod.Systems.Hydraulical.Control.RecordsCollection.ThermostaticValveDataDefinition
          thermostaticValveParameters,
        redeclare
          BESMod.Systems.Hydraulical.Control.Components.HeatPumpNSetController.PI_InverterHeatPumpController
          HP_nSet_Controller,
        redeclare
          BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultBivHPControl
          monovalentControlParas(
          dTHysBui=8,
          k=0.5,
          T_I=1000,
          nMin=1/3),
        safetyControl(
          minRunTime=180,
          minLocTime=180,
          maxRunPerHou=10)),
      transfer(redeclare MPC.DataBase.UFH_Optihorst_Case UFHParameters,
          redeclare
          BESMod.Systems.Hydraulical.Transfer.RecordsCollection.SteelRadiatorStandardPressureLossData
          transferDataBaseDefinition(traType=BESMod.Systems.Hydraulical.Transfer.Types.HeatTransferSystemType.FloorHeating))),
    systemParameters(
      QBui_flow_nominal={6596.211},
      TOda_nominal=261.15,
      THydSup_nominal={313.15},
      use_ventilation=false,                   use_dhw=false,
      use_elecHeating=false),
    DHW(
      mDHW_flow_nominal=0,
      VDHWDay=0,
      tCrit=0,
      QCrit=0));

  Modelica.Blocks.Sources.RealExpression UpperDB(y=hydraulic.control.HP_nSet_Controller.T_Set
         + hydraulic.control.BoilerOnOffBuf.bandwidth/2)
    annotation (Placement(transformation(extent={{170,176},{190,196}})));
  Modelica.Blocks.Sources.RealExpression LowerDB(y=hydraulic.control.HP_nSet_Controller.T_Set
         - hydraulic.control.BoilerOnOffBuf.bandwidth/2)
    annotation (Placement(transformation(extent={{170,156},{190,176}})));
  Modelica.Blocks.Sources.RealExpression COP(y=(4184*0.315*(hydraulic.generation.heatPump.senT_b1.T
         - hydraulic.generation.heatPump.senT_a1.T))/(hydraulic.generation.heatPump.innerCycle.Pel
         + 0.000000001))
    annotation (Placement(transformation(extent={{160,124},{180,144}})));
  annotation (experiment(
      StartTime=86400,
      StopTime=950400,
      Interval=10,
      __Dymola_Algorithm="Dassl"));
end MA_Kevin_Model_Sim;
