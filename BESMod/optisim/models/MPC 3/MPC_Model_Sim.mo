within MPC;
model MPC_Model_Sim
  extends Partial_MPC_Model(
    hydraulic(
      redeclare BESMod.Systems.Hydraulical.Distribution.BuildingOnly
        distribution(nParallelDem=1),
      redeclare MPC.HeatPumpAndHeatingRodMeasPPump generation(
        use_heaRod=false,
        redeclare model PerDataMainHP =
            AixLib.DataBase.HeatPump.PerformanceData.LookUpTableND (
            nConv=90,
            interpMethod=SDF.Types.InterpolationMethod.Linear,
            extrapMethod=SDF.Types.ExtrapolationMethod.Linear,
            filename_Pel=ModelicaServices.ExternalReferences.loadResource(
                "modelica://MPC/Resources/Optihorst/Optihorst_New.sdf"),
            filename_QCon=ModelicaServices.ExternalReferences.loadResource(
                "modelica://MPC/Resources/Optihorst/Optihorst_New.sdf")),
        redeclare
          BESMod.Systems.Hydraulical.Generation.RecordsCollection.DefaultHP
          heatPumpParameters(
          genDesTyp=BESMod.Systems.Hydraulical.Generation.Types.GenerationDesign.Monovalent,
          TBiv=266.15,
          VEva=0.004,
          VCon=0.0047,
          use_refIne=false,
          refIneFre_constant=0),
        redeclare
          BESMod.Systems.Hydraulical.Generation.RecordsCollection.DefaultHR
          heatingRodParameters(eta_hr=parameterStudy.efficiceny_heating_rod),
        redeclare BESMod.Systems.RecordsCollection.Movers.DefaultMover pumpData,
        redeclare package Medium_eva = AixLib.Media.Air),
      redeclare MPC.Submodules.Controller_for_MPC control(
        HeatingRod(y=false),
        redeclare
          BESMod.Systems.Hydraulical.Control.Components.ThermostaticValveController.ThermostaticValvePIControlled
          thermostaticValveController(Ti={200}),
        redeclare
          BESMod.Systems.Hydraulical.Control.RecordsCollection.ThermostaticValveDataDefinition
          thermostaticValveParameters,
        redeclare
          BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultSafetyControl
          safetyControl,
        redeclare
          BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultBivHPControl
          bivalentControlData,
        model_inputs(n_Set(start=0), valve_pi_inner(y=true)),
        pI_Control_for_MPC(PID(controllerType=Modelica.Blocks.Types.SimpleController.PI)),
        const_dT_loading1(k=0),
        FMU_Input(y=true),
        const_dT_loading2(k=273.15 + 20)),
      transfer(
        redeclare MPC.DataBase.UFH_Optihorst_Case UFHParameters,
        redeclare
          BESMod.Systems.Hydraulical.Transfer.RecordsCollection.SteelRadiatorStandardPressureLossData
          transferDataBaseDefinition(traType=BESMod.Systems.Hydraulical.Transfer.Types.HeatTransferSystemType.FloorHeating),
        pressureReliefValveOnOff(val(R=50)))),
    systemParameters(
      use_ventilation=false,
      use_dhw=false,
      use_elecHeating=false),
    DHW(
      mDHW_flow_nominal=0,
      VDHWDay=0,
      tCrit=0,
      QCrit=0));

  annotation (experiment(
      StopTime=2678400,
      Interval=10,
      __Dymola_Algorithm="Dassl"));
end MPC_Model_Sim;
