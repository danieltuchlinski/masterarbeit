within MPC.Utilities;
model IdealSourcePumpTemperature

  replaceable package Medium = Modelica.Media.Interfaces.PartialMedium
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Medium model"
   annotation (choicesAllMatching=true);

  parameter Modelica.Fluid.Types.Dynamics energyDynamics=Modelica.Fluid.Types.Dynamics.DynamicFreeInitial
    "Type of energy balance: dynamic (3 initialization options) or steady state"
    annotation(Evaluate=true, Dialog(tab = "Dynamics", group="Equations"));

  parameter Modelica.Fluid.Types.Dynamics energyDynamicsDelVol=energyDynamics
    "Volume ideal heat input: Type of energy balance: dynamic (3 initialization options) or steady state"
    annotation(Evaluate=true, Dialog(tab = "Dynamics", group="Equations"));

  parameter Modelica.Media.Interfaces.Types.AbsolutePressure p_sinkBou=180000
    "Boundary pressure";
  parameter Modelica.Media.Interfaces.Types.Temperature TFlow_nominal
    "Boundary temperature";
  parameter Modelica.Media.Interfaces.Types.Temperature TRetDesign=Medium.T_default
    "Boundary temperature";

  parameter Modelica.Units.SI.MassFlowRate m_flow_nominal
    "Nominal mass flow rate, used for regularization near zero flow";
  parameter Modelica.Units.SI.PressureDifference dpExtra_nominal
    "Pressure drop at nominal mass flow rate in extra resistance";
    replaceable parameter AixLib.Fluid.Movers.Data.Generic per
    constrainedby AixLib.Fluid.Movers.Data.Generic
    "Record with performance data" annotation(Dialog(group="Pump"), choicesAllMatching=true);
  parameter Real k=(PIPump.yMax - PIPump.yMin)/m_flow_nominal "Gain of controller" annotation(Dialog(group="Pump"));
  parameter Modelica.Units.SI.Time Ti=2 "Time constant of Integrator block"
    annotation (Dialog(group="Pump"));

  parameter Modelica.Units.SI.Time TDampTFlow(min=Modelica.Constants.eps) = 10
    "1/Cut-off frequency" annotation (Dialog(tab="Time constants and initialization",
        group="Damper flow temperature"));
  parameter Modelica.Blocks.Types.Init initTypeDampTFlow=Modelica.Blocks.Types.Init.NoInit   "Type of initialization (1: no init, 2: steady state, 3: initial state, 4: initial output)" annotation (Dialog(tab="Time constants and initialization", group="Damper flow temperature"));
  parameter Real y_start_TFlow=TFlow_nominal
    "Initial value of output (remaining states are in steady state)" annotation(Dialog(tab="Time constants and initialization", group="Damper flow temperature"));
  parameter Modelica.Units.SI.Time TDampMFlow(min=Modelica.Constants.eps) = 10
    "1/Cut-off frequency" annotation (Dialog(tab="Time constants and initialization",
        group="Damper mass flow"));
  parameter Real y_start_mass_flow=m_flow_nominal
    "Initial value of output (remaining states are in steady state)" annotation(Dialog(tab="Time constants and initialization", group="Damper mass flow"));
  parameter Modelica.Units.SI.Time riseTimePump=10
    "Rise time of the filter (time to reach 99.6 % of the speed)"
    annotation (Dialog(tab="Time constants and initialization", group="Pump"));
  parameter Modelica.Units.SI.Time tauVolHeatUp=5
    "Time constant at nominal flow to define volume size" annotation (Dialog(
        tab="Time constants and initialization", group="Volume ideal thermal conditioning"));

  parameter Modelica.Media.Interfaces.Types.AbsolutePressure p_start=Medium.p_default "Start value of pressure" annotation (Dialog(tab="Initialization"));
  parameter Modelica.Media.Interfaces.Types.Temperature T_start=TFlow_nominal    "Start value of temperature" annotation (Dialog(tab="Initialization"));

  Modelica.Blocks.Interfaces.BooleanInput switchToSQL
    "Connector of Boolean input signal"
    annotation (Placement(transformation(extent={{-140,-20},{-100,20}}),
        iconTransformation(extent={{-120,-10},{-100,10}})));
  Modelica.Blocks.Interfaces.RealInput TFlowHeaCur(unit="K", displayUnit="degC")
    "Ambient outdoor air temperature" annotation (Placement(transformation(
          extent={{-140,-90},{-100,-50}}), iconTransformation(extent={{-120,-80},
            {-100,-60}})));
  Modelica.Blocks.Interfaces.RealOutput m_flow_actual(unit="kg/s") annotation (
      Placement(transformation(extent={{100,-70},{120,-50}}),iconTransformation(
          extent={{100,-40},{120,-20}})));
  Modelica.Fluid.Interfaces.FluidPort_b port_b(redeclare package Medium =
        Medium)
    "Second port, typically outlet"
    annotation (Placement(transformation(extent={{88,40},{108,60}}), iconTransformation(extent={{90,70},{110,90}})));
  Modelica.Fluid.Interfaces.FluidPort_a port_a(redeclare package Medium =
        Medium) "First port, typically inlet"
    annotation (Placement(transformation(extent={{88,-10},{108,10}}), iconTransformation(extent={{88,-10},
            {108,10}})));
  Modelica.Blocks.Interfaces.RealOutput TReturnMix
    annotation (Placement(transformation(extent={{100,-100},{120,-80}}), iconTransformation(extent={{100,-80},{120,-60}})));
  Modelica.Blocks.Interfaces.RealInput m_flow_in_withSQL(unit="kg/s")
    "Prescribed mass flow rate" annotation (Placement(transformation(extent={{-140,20},{-100,60}}),
                            iconTransformation(extent={{-120,30},{-100,50}})));
  Modelica.Blocks.Interfaces.RealInput TFlow(unit="K", displayUnit="degC")
                                             "Prescribed boundary temperature"
    annotation (Placement(transformation(extent={{-140,-60},{-100,-20}}),
        iconTransformation(extent={{-120,-50},{-100,-30}})));

  Modelica.Blocks.Logical.Switch switch_T_flow(
    y(unit="K"),
    u3(unit="K", displayUnit="degC"),
    u1(unit="K", displayUnit="degC"))
    annotation (Placement(transformation(extent={{-68,-62},{-54,-48}})));
  AixLib.Fluid.Sources.Boundary_pT bou(
    redeclare package Medium = Medium,
    final p=p_sinkBou,
    final T=TRetDesign,
    nPorts=1) annotation (Placement(transformation(extent={{-2,32},{12,46}})));

  Modelica.Blocks.Logical.Switch switch_m_flow(
    u1(unit="kg/s"),
    u3(unit="kg/s"),
    y(unit="kg/s"))
    annotation (Placement(transformation(extent={{-70,76},{-58,64}})));
  AixLib.Fluid.Sensors.TemperatureTwoPort TReturnMixSen(
    allowFlowReversal=allowFlowReversal,                final m_flow_nominal=
        m_flow_nominal, redeclare final package Medium = Medium)
    annotation (Placement(transformation(extent={{24,-32},{10,-46}})));
  AixLib.Fluid.Movers.SpeedControlled_y pump(
    redeclare package Medium = Medium,
    allowFlowReversal=allowFlowReversal,
    p_start=p_start,
    T_start=T_start,
    addPowerToMedium=false,
    redeclare final
      AixLib.Fluid.Movers.Data.Pumps.Wilo.CronolineIL80slash220dash4slash4 per,
    final energyDynamics=energyDynamics,
    inputType=AixLib.Fluid.Types.InputType.Continuous,
    use_inputFilter=true,
    final riseTime=riseTimePump)
    annotation (Placement(transformation(extent={{16,36},{44,64}})));

  Modelica.Blocks.Continuous.LimPID PIPump(
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    yMax=1,
    yMin=0,
    initType=Modelica.Blocks.Types.Init.InitialOutput,
    k=k,
    Ti=Ti,
    y_start=0) annotation (Placement(transformation(extent={{6,80},{22,96}})));
  AixLib.Fluid.Delays.DelayFirstOrder     del(
    allowFlowReversal=allowFlowReversal,
    p_start=p_start,
    T_start=T_start,
    tau=tauVolHeatUp,
    redeclare final package Medium = Medium,
    energyDynamics=energyDynamicsDelVol,
    final m_flow_nominal=m_flow_nominal,
    nPorts=2)                            annotation (Placement(
        transformation(
        extent={{-9,-9},{9,9}},
        rotation=90,
        origin={-17,-37})));

  AixLib.Fluid.Sensors.MassFlowRate senMasFlo(redeclare final package Medium =
        Medium, allowFlowReversal=allowFlowReversal)
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={64,50})));
  Modelica.Thermal.HeatTransfer.Sources.PrescribedTemperature
    prescribedTemperature
    annotation (Placement(transformation(extent={{-7,-7},{7,7}},
        rotation=90,
        origin={-17,-63})));
  Modelica.Blocks.Continuous.CriticalDamping dampMFlow(
    normalized=true,
    n=3,
    initType=Modelica.Blocks.Types.Init.InitialOutput,
    y(unit="kg/s"),
    final f=1/TDampMFlow,
    final y_start=y_start_mass_flow)
         annotation (Placement(transformation(extent={{-50,64},{-38,76}})));

  Modelica.Blocks.Continuous.CriticalDamping dampTFlow(
    normalized=true,
    n=3,
    initType=initTypeDampTFlow,
    y(unit="K", displayUnit="degC"),
    final f=1/TDampTFlow,
    final y_start=y_start_TFlow)
    annotation (Placement(transformation(extent={{-44,-90},{-32,-78}})));

  Modelica.Blocks.Interfaces.RealInput m_flow_in_withoutSQL(unit="kg/s")
    "Prescribed mass flow rate"
    annotation (Placement(transformation(extent={{-140,50},{-100,90}}), iconTransformation(extent={{-120,60},{-100,80}})));
  AixLib.Fluid.FixedResistances.PressureDrop resExtra(
    redeclare final package Medium = Medium,
    allowFlowReversal=allowFlowReversal,
    final m_flow_nominal=m_flow_nominal,
    from_dp=from_dp,
    final dp_nominal=dpExtra_nominal)
                         if dpExtra_nominal > 0
    annotation (Placement(transformation(extent={{70,-50},{48,-28}})));

  AixLib.Fluid.Interfaces.PassThroughMedium passThroughMediumNoDp(redeclare
      final package Medium =                                                                       Medium, allowFlowReversal=allowFlowReversal)
                                                                                                           if dpExtra_nominal <= 0 annotation (Placement(transformation(extent={{64,-6},{52,6}})));

  parameter Boolean allowFlowReversal=true "= false to simplify equations, assuming, but not enforcing, no flow reversal" annotation (Dialog(tab="Assumptions"));
  parameter Boolean from_dp=false "= true, use m_flow = f(dp) else dp = f(m_flow)" annotation (Dialog(tab="Advanced"));
equation
  connect(TReturnMixSen.T, TReturnMix) annotation (Line(points={{17,-46.7},{17,-90},{110,-90}},
                           color={0,0,127}));
  connect(TFlow, switch_T_flow.u1) annotation (Line(points={{-120,-40},{-100,-40},{-100,-49.4},{-69.4,-49.4}},
                                   color={0,0,127}));
  connect(switch_T_flow.u2, switchToSQL) annotation (Line(points={{-69.4,-55},{-80,-55},{-80,0},{-120,0}},
                                     color={255,0,255}));
  connect(m_flow_in_withSQL, switch_m_flow.u1) annotation (Line(points={{-120,40},{-88,40},{-88,65.2},{-71.2,65.2}},
                                             color={0,0,127}));
  connect(switch_m_flow.u2, switchToSQL) annotation (Line(points={{-71.2,70},{-80,70},{-80,0},{-120,0}},
                                     color={255,0,255}));
  connect(bou.ports[1], pump.port_a)
    annotation (Line(points={{12,39},{12,50},{16,50}}, color={0,127,255}));
  connect(pump.port_b, senMasFlo.port_a)
    annotation (Line(points={{44,50},{54,50}},         color={0,127,255}));
  connect(prescribedTemperature.port,del. heatPort)
    annotation (Line(points={{-17,-56},{-17,-46}},           color={191,0,0}));
  connect(switch_m_flow.y,dampMFlow. u) annotation (Line(points={{-57.4,70},{-51.2,70}},
                                    color={0,0,127}));
  connect(switch_m_flow.u3, m_flow_in_withoutSQL) annotation (Line(points={{-71.2,74.8},{-100,74.8},{-100,70},{-120,70}},
                                                  color={0,0,127}));
  connect(resExtra.port_b, TReturnMixSen.port_a) annotation (Line(
      points={{48,-39},{24,-39}},
      color={0,127,255},
      pattern=LinePattern.Dash));
  connect(passThroughMediumNoDp.port_b, TReturnMixSen.port_a) annotation (Line(
      points={{52,0},{34,0},{34,-39},{24,-39}},
      color={0,127,255},
      pattern=LinePattern.Dash));
  connect(switch_T_flow.y, dampTFlow.u) annotation (Line(points={{-53.3,-55},{
          -50,-55},{-50,-84},{-45.2,-84}},                                                                     color={0,0,127}));
  connect(dampTFlow.y, prescribedTemperature.T) annotation (Line(points={{-31.4,
          -84},{-17,-84},{-17,-71.4}},                                                                       color={0,0,127}));
  connect(senMasFlo.m_flow, m_flow_actual) annotation (Line(points={{64,61},{64,66},{88,66},{88,-60},{110,-60}}, color={0,0,127}));
  connect(dampMFlow.y, PIPump.u_s) annotation (Line(points={{-37.4,70},{-18,70},{-18,88},{4.4,88}}, color={0,0,127}));
  connect(senMasFlo.m_flow, PIPump.u_m) annotation (Line(points={{64,61},{64,74},{14,74},{14,78.4}}, color={0,0,127}));
  connect(PIPump.y, pump.y) annotation (Line(points={{22.8,88},{30,88},{30,66.8}}, color={0,0,127}));
  connect(switch_T_flow.u3, TFlowHeaCur) annotation (Line(points={{-69.4,-60.6},
          {-80,-60.6},{-80,-70},{-120,-70}}, color={0,0,127}));
  connect(passThroughMediumNoDp.port_a, port_a)
    annotation (Line(points={{64,0},{98,0}}, color={0,127,255}));
  connect(resExtra.port_a, port_a) annotation (Line(
      points={{70,-39},{78,-39},{78,0},{98,0}},
      color={0,127,255},
      pattern=LinePattern.Dash));
  connect(senMasFlo.port_b, port_b)
    annotation (Line(points={{74,50},{98,50}}, color={0,127,255}));
  connect(del.ports[1], pump.port_a) annotation (Line(points={{-8,-37.9},{-2,
          -37.9},{-2,28},{-6,28},{-6,50},{16,50}}, color={0,127,255}));
  connect(TReturnMixSen.port_b, del.ports[2]) annotation (Line(points={{10,-39},
          {10,-36.1},{-8,-36.1}}, color={0,127,255}));
  annotation (Icon(graphics={
        Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={0,0,0},
          fillColor={234,234,234},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-60,-4},{-20,-44}},
          lineColor={28,108,200},
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Line(
          points={{60,50},{88,50}},
          color={28,108,200},
          thickness=1),
        Line(
          points={{-40,-60},{60,-60}},
          color={28,108,200},
          thickness=1),
        Line(
          points={{60,20},{60,50}},
          color={28,108,200},
          thickness=1),
        Line(
          points={{60,-60},{60,-20}},
          color={28,108,200},
          thickness=1),
        Ellipse(
          extent={{-18,96},{14,64}},
          lineColor={28,108,200},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Line(
          points={{-10,94},{14,80}},
          color={28,108,200}),
        Line(
          points={{14,80},{-10,66}},
          color={28,108,200}),
        Line(
          points={{-40,-60},{-40,80}},
          color={28,108,200},
          thickness=1),
        Line(
          points={{-40,80},{-18,80}},
          color={28,108,200},
          thickness=1),
        Line(
          points={{14,80},{88,80}},
          color={28,108,200},
          thickness=1),
        Rectangle(
          extent={{-64,-22},{-58,-28}},
          pattern=LinePattern.None,
          lineColor={0,0,0},
          fillColor={238,46,47},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{64,0},{56,0},{46,20},{74,20},{64,0}},
          lineColor={28,108,200},
          fillColor={28,108,200},
          fillPattern=FillPattern.None,
          lineThickness=1),
        Polygon(
          points={{64,0},{56,0},{46,-20},{74,-20},{64,0}},
          lineColor={28,108,200},
          fillColor={28,108,200},
          fillPattern=FillPattern.None,
          lineThickness=1)}));
end IdealSourcePumpTemperature;
