within MPC.Utilities.DHW;
block TriggerQ
  extends Modelica.Blocks.Interfaces.SISO;
  parameter Real x "Threshold defines if to change values";
  Real u_pre(start=1);

algorithm
  when (u_pre > u or u_pre < u) and u > x then
    y :=u;
  end when;
   u_pre :=u;
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end TriggerQ;
