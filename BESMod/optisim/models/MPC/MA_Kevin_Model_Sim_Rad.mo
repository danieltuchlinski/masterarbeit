within MPC;
model MA_Kevin_Model_Sim_Rad
  extends Partial_MPC_Model(
    hydraulic(
      redeclare BESMod.Systems.Hydraulical.Distribution.BuildingOnly
        distribution(nParallelDem=1),
      redeclare MPC.Submodules.Generation.HPC generation,
      redeclare MPC.Submodules.MonovalentOptihorst control(
        redeclare
          BESMod.Systems.Hydraulical.Control.Components.ThermostaticValveController.ThermostaticValvePIControlled
          thermostaticValveController,
        redeclare
          BESMod.Systems.Hydraulical.Control.RecordsCollection.ThermostaticValveDataDefinition
          thermostaticValveParameters,
        redeclare
          BESMod.Systems.Hydraulical.Control.Components.HeatPumpNSetController.PI_InverterHeatPumpController
          HP_nSet_Controller,
        redeclare
          BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultBivHPControl
          monovalentControlParas(
          dTHysBui=8,
          k=0.5,
          T_I=1000,
          TSup_nominal=328.15,
          nMin=1/3),
        safetyControl(maxRunPerHou=10)),
      redeclare PCMgoesHIL.Systems.Hydraulic.Transfer.RadiatorTransferSystemHIL
        transfer(
        redeclare
          BESMod.Systems.Hydraulical.Transfer.RecordsCollection.RadiatorTransferData
          radParameters,
        redeclare
          BESMod.Systems.RecordsCollection.TemperatureSensors.DefaultSensor
          sensorData,
        redeclare
          BESMod.Systems.Hydraulical.Transfer.RecordsCollection.SteelRadiatorStandardPressureLossData
          transferDataBaseDefinition,
        dpFullOpen_nominal=99999.99999999999*(73853.65852*0.8))),
    systemParameters(
      QBui_flow_nominal={6596.211},
      TOda_nominal=261.15,
      THydSup_nominal={328.15},
      use_ventilation=false,                   use_dhw=false,
      use_elecHeating=false),
    DHW(
      mDHW_flow_nominal=0,
      VDHWDay=0,
      tCrit=0,
      QCrit=0));

  annotation (experiment(
      StartTime=864000,
      StopTime=950400,
      Interval=10,
      __Dymola_Algorithm="Dassl"));
end MA_Kevin_Model_Sim_Rad;
