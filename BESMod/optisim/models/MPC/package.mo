within ;
package MPC



  annotation (uses(
      Modelica(version="4.0.0"),
    ModelicaServices(version="4.0.0"),
    AixLib(version="1.3.1"),
 SDF(version="0.4.2"),
    MQTT_Interface(version="0.0.2"),
    PCMgoesHIL(version="2"),
    BESMod(version="0.3.2"),
    IBPSA(version="4.0.0")),
  version="5",
  conversion(noneFromVersion="3", noneFromVersion="4"));
end MPC;
