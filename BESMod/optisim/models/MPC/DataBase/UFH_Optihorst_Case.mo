within MPC.DataBase;
record UFH_Optihorst_Case "UFH Optihorst Case"
  extends BESMod.Systems.Hydraulical.Transfer.RecordsCollection.UFHData(
    T_floor=281.65,
    diameter=18e-3,
    c_top_ratio=fill(0.3053, nZones),
    C_ActivatedElement=fill(404835.58, nZones),
    k_down=fill(2.351, nZones),
    k_top=fill(20.24, nZones),
    is_groundFloor=fill(true, nZones),
    nZones=1);
end UFH_Optihorst_Case;
