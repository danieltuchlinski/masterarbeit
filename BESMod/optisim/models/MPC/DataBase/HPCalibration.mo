within MPC.DataBase;
record HPCalibration
  extends
    BESMod.Systems.Hydraulical.Interfaces.Generation.RecordsCollection.HeatPumpBaseDataDefinition(
    refIneFre_constant=0.75,
    VCon=0.0047,
    genDesTyp=BESMod.Systems.Hydraulical.Interfaces.Generation.Types.GenerationDesign.Monovalent,
    THeaTresh=318.15,
    TOda_nominal=273.15,
    QGen_flow_nominal=10670,
    redeclare BESMod.Systems.RecordsCollection.TemperatureSensors.DefaultSensor
      TempSensorData,
    use_refIne=true);

end HPCalibration;
