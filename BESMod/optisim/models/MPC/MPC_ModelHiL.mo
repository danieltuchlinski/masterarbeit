within MPC;
model MPC_ModelHiL
  extends Partial_MPC_Model(hydraulic(
      redeclare MPC.Submodules.UFHTransferSystemwoPump transfer(redeclare
          MPC.DataBase.UFH_Optihorst_Case UFHParameters, redeclare
          BESMod.Systems.Hydraulical.Transfer.RecordsCollection.SteelRadiatorStandardPressureLossData
          transferDataBaseDefinition(traType=BESMod.Systems.Hydraulical.Transfer.Types.HeatTransferSystemType.FloorHeating)),
      redeclare BESMod.Systems.Hydraulical.Interfaces.Generation.NoGeneration
        generation,
      redeclare PCMgoesHIL.Systems.Hydraulic.Distribution.HiLMqttConnection
        distribution(redeclare MPC.Submodules.Distribution.HiL2MPCTopics
          topicsMQTT, mQTTAndConst(startTimeSQL=startTimeSQL)),
      redeclare MPC.Submodules.Controller_for_HiL control(redeclare
          BESMod.Systems.Hydraulical.Control.Components.ThermostaticValveController.ThermostaticValvePIControlled
          thermostaticValveController, redeclare
          BESMod.Systems.Hydraulical.Control.RecordsCollection.ThermostaticValveDataDefinition
          thermostaticValveParameters)),                               DHW(
      mDHW_flow_nominal=800,
      VDHWDay=2,
      tCrit=216000,
      QCrit=100));

    MQTT_Interface.Models.MQTTAndConst mQTTAndConst(
    nWriteValues=4,
    redeclare MQTT_Interface.MQTTConnectionPara.TickStackOpenstack mqttConParam,
    startTimeSQL=startTimeSQL,
    samRate=1,
    namesToWrite={"hil2/Sim/Sim_Time","hil2/Sim/T_Zone",
        "hil2/Sim/building.weaBus.TDryBul","hil2/Sim/building.weaBus.HDirNor"},
    addWriteValues={0,-273.15,-273.15,0})
    annotation (Placement(transformation(extent={{350,-88},{412,-26}})));

  Modelica.Blocks.Sources.ContinuousClock Clock(offset=0, startTime=0)
    annotation (Placement(transformation(extent={{314,48},{334,68}})));
  parameter Modelica.Units.SI.Time startTimeSQL=9E7
    "Start time, when data exchange with the set frequency (rate) begins";
  AixLib.BoundaryConditions.WeatherData.Bus weaBus annotation (Placement(
        transformation(extent={{246,-124},{286,-84}}), iconTransformation(
          extent={{182,-72},{202,-52}})));
equation
  connect(Clock.y, mQTTAndConst.writeValues[1]) annotation (Line(points={{335,58},
          {366,58},{366,-58.1625},{346.9,-58.1625}},        color={0,0,127}));
  connect(electrical.weaBus, weaBus) annotation (Line(
      points={{-198,103.429},{-198,102},{-214,102},{-214,70},{-212,70},{-212,
          -142},{266,-142},{266,-104}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(weaBus.TDryBul, mQTTAndConst.writeValues[3]) annotation (Line(
      points={{266,-104},{134,-104},{134,-56},{240,-56},{240,-56.6125},{346.9,
          -56.6125}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(building.buiMeaBus.TZoneMea[1], mQTTAndConst.writeValues[2])
  annotation (experiment(
      StopTime=31536000,
      Interval=600,
      __Dymola_Algorithm="Dassl"));
  connect(weaBus.HDirNor, mQTTAndConst.writeValues[4]) annotation (Line(
      points={{266,-104},{378,-104},{378,-55.8375},{346.9,-55.8375}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  annotation (experiment(
      StopTime=3153600,
      Interval=600,
      __Dymola_Algorithm="Dassl"));
end MPC_ModelHiL;
