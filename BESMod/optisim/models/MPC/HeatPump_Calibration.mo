﻿within MPC;
model HeatPump_Calibration "Example for the reversible heat pump model."
 extends Modelica.Icons.Example;

  replaceable package Medium_sin = IBPSA.Media.Water
    constrainedby Modelica.Media.Interfaces.PartialMedium annotation (choicesAllMatching=true);
  replaceable package Medium_sou = AixLib.Media.Air
    constrainedby Modelica.Media.Interfaces.PartialMedium annotation (choicesAllMatching=true);
  AixLib.Fluid.Sources.MassFlowSource_T                sourceSideMassFlowSource(
    use_T_in=true,
    m_flow=1,
    nPorts=1,
    redeclare package Medium = Medium_sou,
    T=275.15) "Ideal mass flow source at the inlet of the source side"
              annotation (Placement(transformation(extent={{-44,-64},{-24,-44}})));

  AixLib.Fluid.Sources.Boundary_pT                  sourceSideFixedBoundary(
                                                                         nPorts=
       1, redeclare package Medium = Medium_sou)
          "Fixed boundary at the outlet of the source side"
          annotation (Placement(transformation(extent={{-11,11},{11,-11}},
        rotation=0,
        origin={-65,-23})));
  AixLib.Fluid.HeatPumps.HeatPump heatPump(
    refIneFre_constant=0.01,
    nthOrder=3,
    useBusConnectorOnly=true,
    GConIns=65.26,
    CEva=3997,
    GEvaOut=5,
    CCon=3997,
    GConOut=3,
    dpEva_nominal=1000,
    dpCon_nominal=1000,
    VCon=0.0047,
    use_conCap=true,
    GEvaIns=50,
    tauSenT=1,
    tauHeaTraEva=1200,
    tauHeaTraCon=1200,
    redeclare package Medium_con = Medium_sin,
    redeclare package Medium_eva = Medium_sou,
    use_refIne=true,
    use_rev=true,
    redeclare model PerDataMainHP =
        AixLib.DataBase.HeatPump.PerformanceData.LookUpTableND (
        nConv=90,
        interpMethod=SDF.Types.InterpolationMethod.Linear,
        extrapMethod=SDF.Types.ExtrapolationMethod.Linear,
        filename_Pel=
            "D:/sgo-kwa/Repos/optisim/calibration/sdf/Optihorst_New.sdf",
        filename_QCon=
            "D:/sgo-kwa/Repos/optisim/calibration/sdf/Optihorst_New.sdf"),
    redeclare model PerDataRevHP =
        AixLib.DataBase.Chiller.PerformanceData.LookUpTable2D (
          smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments, dataTable=
           AixLib.DataBase.Chiller.EN14511.Vitocal200AWO201()),
    VEva=0.001,
    use_evaCap=false,
    scalingFactor=1,
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
    mFlow_conNominal=0.35,
    mFlow_evaNominal=1,
    use_autoCalc=false,
    TAmbEva_nominal=293.15,
    TAmbCon_nominal=293.15,
    TCon_start=328.15) annotation (Placement(transformation(
        extent={{-24,-29},{24,29}},
        rotation=270,
        origin={-2,-21})));

  Modelica.Blocks.Sources.BooleanStep     booleanStep(startTime=0, startValue=false)
    annotation (Placement(transformation(extent={{-6,-6},{6,6}},
        rotation=0,
        origin={-68,6})));

  AixLib.Fluid.Sensors.TemperatureTwoPort senTAct(
    final m_flow_nominal=heatPump.m1_flow_nominal,
    final tau=1,
    final initType=Modelica.Blocks.Types.Init.InitialState,
    final tauHeaTra=1200,
    final allowFlowReversal=heatPump.allowFlowReversalCon,
    final transferHeat=true,
    redeclare final package Medium = Medium_sin,
    final T_start=328.15,
    final TAmb=293.15) "Temperature at sink inlet" annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={54,-24})));

  AixLib.Fluid.MixingVolumes.MixingVolume Room(
    nPorts=1,
    final use_C_flow=false,
    final m_flow_nominal=heatPump.m1_flow_nominal,
    final V=5,
    final allowFlowReversal=true,
    redeclare package Medium = Medium_sin,
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial)
    "Volume of Condenser" annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={76,2})));

  AixLib.Fluid.Sources.Boundary_pT   sinkSideFixedBoundary(      nPorts=1,
      redeclare package Medium = Medium_sin)
    "Fixed boundary at the outlet of the sink side" annotation (Placement(
        transformation(
        extent={{10,-10},{-10,10}},
        rotation=0,
        origin={84,-44})));
  Modelica.Blocks.Sources.Constant iceFac(final k=1)
    "Fixed value for icing factor. 1 means no icing/frosting (full heat transfer in heat exchanger)" annotation (Placement(
        transformation(
        extent={{8,8},{-8,-8}},
        rotation=180,
        origin={-74,34})));
  AixLib.Controls.Interfaces.VapourCompressionMachineControlBus sigBus1
    annotation (Placement(transformation(extent={{-20,20},{10,54}}),
        iconTransformation(extent={{-8,28},{10,54}})));
  AixLib.Fluid.Sources.MassFlowSource_T MassFlowSink(
    use_m_flow_in=true,
    use_T_in=true,
    m_flow=0.318,
    nPorts=1,
    redeclare package Medium = Medium_sin,
    T=325.15) "Ideal mass flow source at the inlet of the sink side"
    annotation (Placement(transformation(extent={{40,8},{20,28}})));
  Modelica.Blocks.Math.Gain nFreq(k=1/90)
    "Changing absolute frequency of the compressor to relative between 0 and 1"
    annotation (Placement(transformation(extent={{-50,54},{-36,68}})));

  Modelica.Blocks.Sources.CombiTimeTable TimTab(
    tableOnFile=true,
    table=fill(
        0.0,
        0,
        5),
    tableName="Comp_30to55",
    fileName=
        "D:/sgo-kwa/Repos/optisim/calibration/heatpumpcalibration/data/CHP.txt")
    annotation (Placement(transformation(extent={{-96,68},{-76,88}})));
  Modelica.Blocks.Math.Gain gain2(k=1/3600)
    "Changing absolute frequency of the compressor to relative between 0 and 1"
    annotation (Placement(transformation(extent={{34,42},{48,56}})));
  Modelica.Blocks.Math.UnitConversions.From_degC from_degC_source
    annotation (Placement(transformation(extent={{-84,-62},{-68,-46}})));
  Modelica.Blocks.Math.UnitConversions.From_degC from_degC_sink
    annotation (Placement(transformation(extent={{40,78},{56,94}})));
  Modelica.Blocks.Sources.RealExpression Pel_Inv(y=heatPump.innerCycle.Pel)
    annotation (Placement(transformation(
        extent={{-39,-8},{39,8}},
        rotation=0,
        origin={-59,-72})));
  Modelica.Blocks.Sources.RealExpression T_Cond_Out(y=heatPump.senT_b1.T)
    annotation (Placement(transformation(
        extent={{-39,-8},{39,8}},
        rotation=0,
        origin={-59,-88})));
  Modelica.Blocks.Interfaces.RealOutput Pel_comp annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={82,-68}),  iconTransformation(extent={{120,-76},{140,-56}})));
  Modelica.Blocks.Interfaces.RealOutput T_VL
    annotation (Placement(transformation(extent={{-10,-10},{10,10}},
        rotation=0,
        origin={82,-88}),
        iconTransformation(extent={{120,-76},{140,-56}})));
  Modelica.Blocks.Math.Gain W_in_kW(k=1/1000)
    "Changing absolute frequency of the compressor to relative between 0 and 1"
    annotation (Placement(transformation(extent={{20,-70},{34,-56}})));
  Modelica.Thermal.HeatTransfer.Celsius.FromKelvin KelvinToCelsius
    annotation (Placement(transformation(extent={{24,-98},{44,-78}})));
equation

  connect(sourceSideMassFlowSource.ports[1], heatPump.port_a2) annotation (Line(
        points={{-24,-54},{-16.5,-54},{-16.5,-45}},         color={0,127,255}));
  connect(heatPump.port_b2, sourceSideFixedBoundary.ports[1]) annotation (Line(
        points={{-16.5,3},{-16.5,10},{-38,10},{-38,-23},{-54,-23}},
                                                      color={0,127,255}));
  connect(heatPump.port_b1, senTAct.port_a) annotation (Line(points={{12.5,-45},
          {12.5,-50},{36,-50},{36,-24},{44,-24}},
                                       color={0,127,255}));
  connect(senTAct.port_b, sinkSideFixedBoundary.ports[1]) annotation (Line(
        points={{64,-24},{68,-24},{68,-44},{74,-44}}, color={0,127,255}));
  connect(senTAct.port_b, Room.ports[1]) annotation (Line(points={{64,-24},{76,
          -24},{76,-8}},           color={0,127,255}));
  connect(booleanStep.y, sigBus1.modeSet) annotation (Line(points={{-61.4,6},{
          -50,6},{-50,14},{-24,14},{-24,37.085},{-4.925,37.085}},
                                                color={255,0,255}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(iceFac.y, sigBus1.iceFacMea) annotation (Line(points={{-65.2,34},{-32,
          34},{-32,37.085},{-4.925,37.085}},  color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(sigBus1, heatPump.sigBus) annotation (Line(
      points={{-5,37},{-5,2.76},{-11.425,2.76}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(MassFlowSink.ports[1], heatPump.port_a1) annotation (Line(points={{20,
          18},{14,18},{14,8},{12.5,8},{12.5,3}}, color={0,127,255}));
  connect(nFreq.y, sigBus1.nSet) annotation (Line(points={{-35.3,61},{-4.925,61},
          {-4.925,37.085}},  color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-3,-6},{-3,-6}},
      horizontalAlignment=TextAlignment.Right));
  connect(TimTab.y[4],nFreq. u) annotation (Line(points={{-75,78},{-66,78},{-66,
          61},{-51.4,61}}, color={0,0,127}));
  connect(gain2.y, MassFlowSink.m_flow_in) annotation (Line(points={{48.7,49},{
          52,49},{52,26},{42,26}}, color={0,0,127}));
  connect(TimTab.y[2], gain2.u) annotation (Line(points={{-75,78},{22,78},{22,
          49},{32.6,49}},                 color={0,0,127}));
  connect(TimTab.y[3], from_degC_source.u) annotation (Line(points={{-75,78},{
          -66,78},{-66,50},{-90,50},{-90,-54},{-85.6,-54}}, color={0,0,127}));
  connect(from_degC_source.y, sourceSideMassFlowSource.T_in) annotation (Line(
        points={{-67.2,-54},{-54,-54},{-54,-50},{-46,-50}}, color={0,0,127}));
  connect(TimTab.y[1], from_degC_sink.u) annotation (Line(points={{-75,78},{-48,
          78},{-48,86},{38.4,86}}, color={0,0,127}));
  connect(from_degC_sink.y, MassFlowSink.T_in) annotation (Line(points={{56.8,
          86},{80,86},{80,22},{42,22}}, color={0,0,127}));
  connect(Pel_Inv.y, W_in_kW.u) annotation (Line(points={{-16.1,-72},{12,-72},{
          12,-63},{18.6,-63}}, color={0,0,127}));
  connect(W_in_kW.y, Pel_comp) annotation (Line(points={{34.7,-63},{62,-63},{62,
          -68},{82,-68}}, color={0,0,127}));
  connect(T_Cond_Out.y, KelvinToCelsius.Kelvin) annotation (Line(points={{-16.1,
          -88},{22,-88}},                      color={0,0,127}));
  connect(KelvinToCelsius.Celsius, T_VL)
    annotation (Line(points={{45,-88},{82,-88}},           color={0,0,127}));
  connect(Pel_comp, Pel_comp)
    annotation (Line(points={{82,-68},{82,-68}}, color={0,0,127}));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}})),
    experiment(Tolerance=1e-6, StopTime=3600),
__Dymola_Commands(file="modelica://AixLib/Resources/Scripts/Dymola/Fluid/HeatPumps/Examples/HeatPump.mos"
        "Simulate and plot"),
    Documentation(info="<html><h4>
  <span style=\"color: #008000\">Overview</span>
</h4>
<p>
  Simple test set-up for the HeatPumpDetailed model. The heat pump is
  turned on and off while the source temperature increases linearly.
  Outputs are the electric power consumption of the heat pump and the
  supply temperature.
</p>
<p>
  Besides using the default simple table data, the user should also
  test tabulated data from <a href=
  \"modelica://AixLib.DataBase.HeatPump\">AixLib.DataBase.HeatPump</a> or
  polynomial functions.
</p>
</html>",
      revisions="<html><ul>
  <li>
    <i>May 22, 2019</i> by Julian Matthes:<br/>
    Rebuild due to the introducion of the thermal machine partial model
    (see issue <a href=
    \"https://github.com/RWTH-EBC/AixLib/issues/715\">#715</a>)
  </li>
  <li>
    <i>November 26, 2018&#160;</i> by Fabian Wüllhorst:<br/>
    First implementation (see issue <a href=
    \"https://github.com/RWTH-EBC/AixLib/issues/577\">#577</a>)
  </li>
</ul>
</html>"),
    __Dymola_Commands(file="Modelica://AixLib/Resources/Scripts/Dymola/Fluid/HeatPumps/Examples/HeatPump.mos" "Simulate and plot"),
    Icon(coordinateSystem(extent={{-100,-100},{100,80}})));
end HeatPump_Calibration;
