within MPC.Submodules;
model MonovalentOptihorst "PI Control of Optihorst"
  extends
    BESMod.Systems.Hydraulical.Control.BaseClasses.SystemWithThermostaticValveControl;
  BESMod.Systems.Hydraulical.Control.Components.HeatingCurve
    heatingCurve(
    GraHeaCurve=monovalentControlParas.gradientHeatCurve,
    THeaThres=monovalentControlParas.TSetRoomConst,
    dTOffSet_HC=monovalentControlParas.dTOffSetHeatCurve)
    annotation (Placement(transformation(extent={{-220,20},{-200,40}})));
  BESMod.Systems.Hydraulical.Control.Components.DHWSetControl.ConstTSet_DHW
    TSet_DHW(T_DHW=distributionParameters.TDHW_nominal) if use_dhw
    annotation (Placement(transformation(extent={{-220,80},{-200,100}})));
  replaceable
    BESMod.Systems.Hydraulical.Control.Components.HeatPumpNSetController.PI_InverterHeatPumpController
    HP_nSet_Controller(
    P=monovalentControlParas.k,
    nMin=monovalentControlParas.nMin,
    T_I=monovalentControlParas.T_I,
    Ni=monovalentControlParas.Ni,
    PID(P(k=0.1)))                annotation (choicesAllMatching=true,
      Placement(transformation(extent={{102,42},{138,78}})));
  Modelica.Blocks.Logical.OnOffController BoilerOnOffBuf(bandwidth=
        monovalentControlParas.dTHysBui, pre_y_start=true)
    "Generates the on/off signal depending on the temperature inputs"
    annotation (Placement(transformation(extent={{-160,0},{-140,20}})));
  Modelica.Blocks.Logical.OnOffController boilerOnOffDHW(bandwidth=
        monovalentControlParas.dTHysDHW, pre_y_start=true) if use_dhw
    "Generates the on/off signal depending on the temperature inputs"
    annotation (Placement(transformation(extent={{-160,40},{-140,60}})));
  Modelica.Blocks.Sources.Constant const_dT_loading(k=distributionParameters.dTTra_nominal[1])
                                     annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=180,
        origin={-110,-10})));
  Modelica.Blocks.Math.Add add_dT_LoadingBuf
    annotation (Placement(transformation(extent={{-80,0},{-60,20}})));
  Modelica.Blocks.Logical.Or BoiOn annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={-10,30})));
  Modelica.Blocks.Logical.Switch switch1 if use_dhw
    annotation (Placement(transformation(extent={{-10,-10},{10,10}},
        rotation=0,
        origin={10,70})));
  Modelica.Blocks.Sources.Constant const_dT_loading1(k=distributionParameters.dTTraDHW_nominal
         + monovalentControlParas.dTHysDHW /2) if use_dhw
                                               annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=180,
        origin={-90,90})));
  Modelica.Blocks.Math.Add add_dT_LoadingDHW if use_dhw
    annotation (Placement(transformation(extent={{-60,80},{-40,60}})));
  replaceable parameter
    BESMod.Systems.Hydraulical.Control.RecordsCollection.BivalentHeatPumpControlDataDefinition
    monovalentControlParas constrainedby
    BESMod.Systems.Hydraulical.Control.RecordsCollection.BivalentHeatPumpControlDataDefinition(
    TOda_nominal=generationParameters.TOda_nominal,
    TSup_nominal=generationParameters.TSup_nominal[1],
    TSetRoomConst=transferParameters.TDem_nominal[1],
    final TBiv=monovalentControlParas.TOda_nominal) annotation (
      choicesAllMatching=true, Placement(transformation(extent={{-218,-36},{
            -204,-22}})));
  Modelica.Blocks.Sources.BooleanConstant booleanConstant(k=false)
    if not use_dhw annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=180,
        origin={-44,-8})));
  Modelica.Blocks.Routing.RealPassThrough realPassThrough if not use_dhw
    annotation (Placement(transformation(extent={{46,48},{66,68}})));
  Modelica.Blocks.Math.MinMax minMax(nu=transferParameters.nParallelDem)
    annotation (Placement(transformation(extent={{-240,50},{-220,70}})));
  Modelica.Blocks.Sources.Constant hr_on(k=0) annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=0,
        origin={-196,-62})));
  AixLib.Controls.HeatPump.SafetyControls.SafetyControl safetyControl(
    minRunTime=180,
    minLocTime=180,
    maxRunPerHou=20,
    use_opeEnv=true,
    use_opeEnvFroRec=false,
    dataTable=MPC.DataBase.Optihorst_Kevin_81rps(),
    tableUpp=[-40,70; 40,70],
    use_deFro=false,
    minIceFac=1,
    use_chiller=false,
    calcPel_deFro=1000,
    use_antFre=false)
    annotation (Placement(transformation(extent={{178,42},{230,86}})));
  Modelica.Blocks.Sources.BooleanConstant hp_mode_sc(k=true) if not use_dhw
    annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=180,
        origin={80,-4})));
equation
  connect(sigBusDistr,TSet_DHW. sigBusDistr) annotation (Line(
      points={{1,-100},{10,-100},{10,-146},{-280,-146},{-280,89.9},{-220,89.9}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(boilerOnOffDHW.u, sigBusDistr.TStoDHWTopMea) annotation (Line(points={{-162,44},
          {-162,22},{-128,22},{-128,-68},{-28,-68},{-28,-66},{1,-66},{1,-100}},
                              color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(TSet_DHW.TSet_DHW, boilerOnOffDHW.reference) annotation (Line(
        points={{-199,90},{-194,90},{-194,72},{-168,72},{-168,56},{-162,56}},
                                                                color={0,0,
          127}));
  connect(heatingCurve.TSet, BoilerOnOffBuf.reference) annotation (Line(
        points={{-199,30},{-168,30},{-168,16},{-162,16}},         color={0,0,
          127}));
  connect(const_dT_loading.y, add_dT_LoadingBuf.u2) annotation (Line(points={{-99,-10},
          {-88,-10},{-88,4},{-82,4}},            color={0,0,127}));
  connect(boilerOnOffDHW.y, BoiOn.u1) annotation (Line(points={{-139,50},{-134,
          50},{-134,46},{-30,46},{-30,36},{-28,36},{-28,30},{-22,30}},
                                      color={255,0,255}));
  connect(BoilerOnOffBuf.y, BoiOn.u2) annotation (Line(points={{-139,10},{-90,
          10},{-90,20},{-82,20},{-82,22},{-22,22}},
                                      color={255,0,255}));
  connect(boilerOnOffDHW.y, switch1.u2) annotation (Line(points={{-139,50},{-134,
          50},{-134,46},{-28,46},{-28,66},{-8,66},{-8,70},{-2,70}},
                                       color={255,0,255}));
  connect(boilerOnOffDHW.y, sigBusDistr.dhw_on) annotation (Line(points={{-139,50},
          {-134,50},{-134,46},{-30,46},{-30,36},{-28,36},{-28,28},{-26,28},{-26,
          2},{-12,2},{-12,-62},{1,-62},{1,-100}},
        color={255,0,255}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(heatingCurve.TOda, weaBus.TDryBul) annotation (Line(points={{-222,30},
          {-238,30},{-238,18},{-237,18},{-237,2}},
                                   color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(const_dT_loading1.y, add_dT_LoadingDHW.u2) annotation (Line(points={{-79,90},
          {-72,90},{-72,76},{-62,76}},               color={0,0,127}));
  connect(TSet_DHW.TSet_DHW, add_dT_LoadingDHW.u1) annotation (Line(points={{-199,90},
          {-194,90},{-194,72},{-70,72},{-70,64},{-62,64}},
                                                    color={0,0,127}));
  connect(switch1.y, HP_nSet_Controller.T_Set) annotation (Line(points={{21,70},
          {40,70},{40,74},{84,74},{84,70.8},{98.4,70.8}},
                                      color={0,0,127}));
  connect(add_dT_LoadingBuf.y, switch1.u3) annotation (Line(points={{-59,10},{-44,
          10},{-44,54},{-8,54},{-8,62},{-2,62}},
                                             color={0,0,127}));
  connect(add_dT_LoadingDHW.y, switch1.u1) annotation (Line(points={{-39,70},{-39,
          68},{-14,68},{-14,78},{-2,78}},
                                color={0,0,127}));
  connect(heatingCurve.TSet, add_dT_LoadingBuf.u1) annotation (Line(points={{-199,30},
          {-88,30},{-88,16},{-82,16}},                                color={0,
          0,127}));
  connect(booleanConstant.y, BoiOn.u1) annotation (Line(points={{-33,-8},{-28,
          -8},{-28,26},{-30,26},{-30,30},{-22,30}},
                                                color={255,0,255}));
  connect(realPassThrough.y, HP_nSet_Controller.T_Set) annotation (Line(points={
          {67,58},{84,58},{84,70.8},{98.4,70.8}}, color={0,0,127}));

  connect(realPassThrough.u, add_dT_LoadingBuf.y) annotation (Line(points={{44,
          58},{26,58},{26,52},{-26,52},{-26,54},{-44,54},{-44,10},{-59,10}},
        color={0,0,127}));
  connect(minMax.yMax, heatingCurve.TSetRoom)
    annotation (Line(points={{-219,66},{-210,66},{-210,42}}, color={0,0,127}));
  connect(minMax.u, useProBus.TZoneSet) annotation (Line(points={{-240,60},{-244,
          60},{-244,103},{-119,103}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(hr_on.y, sigBusGen.hr_on) annotation (Line(points={{-189.4,-62},{-152,
          -62},{-152,-99}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(HP_nSet_Controller.n_Set, safetyControl.nSet) annotation (Line(points={{139.8,
          60},{146,60},{146,62},{174.533,62},{174.533,68.4}},        color={0,0,
          127}));
  connect(safetyControl.modeOut, sigBusGen.hp_bus.modeSet) annotation (Line(
        points={{232.167,59.6},{236,59.6},{236,60},{238,60},{238,-20},{-152,-20},
          {-152,-99}}, color={255,0,255}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(safetyControl.nOut, sigBusGen.hp_bus.nSet) annotation (Line(points={{232.167,
          68.4},{232.167,-99},{-152,-99}},         color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-3,-6},{-3,-6}},
      horizontalAlignment=TextAlignment.Right));
  connect(sigBusGen.hp_bus, safetyControl.sigBusHP) annotation (Line(
      points={{-152,-99},{174.75,-99},{174.75,48.82}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-3,-6},{-3,-6}},
      horizontalAlignment=TextAlignment.Right));
  connect(hp_mode_sc.y, safetyControl.modeSet) annotation (Line(points={{91,-4},
          {120,-4},{120,12},{174.533,12},{174.533,59.6}}, color={255,0,255}));
  connect(BoiOn.y, HP_nSet_Controller.HP_On) annotation (Line(points={{1,30},{
          60,30},{60,22},{74,22},{74,30},{90,30},{90,60},{98.4,60}},
                                               color={255,0,255}));
  connect(sigBusGen.hp_bus.TConOutMea, BoilerOnOffBuf.u) annotation (Line(
      points={{-152,-99},{-164,-99},{-164,4},{-162,4}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(sigBusGen.hp_bus.TConOutMea, HP_nSet_Controller.T_Meas) annotation (
      Line(
      points={{-152,-99},{-138,-99},{-138,-64},{120,-64},{120,38.4}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-3,-6},{-3,-6}},
      horizontalAlignment=TextAlignment.Right));
  connect(BoiOn.y, HP_nSet_Controller.IsOn) annotation (Line(points={{1,30},{1,
          32},{109.2,32},{109.2,38.4}},         color={255,0,255}));
  connect(heatingCurve.TSet, sigBusDistr.TFlowHeaCur) annotation (Line(points={
          {-199,30},{-194,30},{-194,22},{6,22},{6,-100},{1,-100}}, color={0,0,
          127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  annotation (experiment(
      StopTime=864000,
      Interval=10,
      __Dymola_Algorithm="Dassl"));
end MonovalentOptihorst;
