within MPC.Submodules;
model Controller_for_HiL
  "Controller_for_HiL"
  extends BESMod.Systems.Hydraulical.Control.BaseClasses.PartialControl;

  Modelica.Blocks.Routing.RealPassThrough realPassThrough_T_Amb1
    "Only used to make warning disappear, has no effect on model veloccity"
    annotation (Placement(transformation(extent={{-228,-58},{-206,-36}})));

  parameter
    BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultBivHPControl bivalentControlData(
    final TOda_nominal=generationParameters.TOda_nominal,
    TSup_nominal=generationParameters.TSup_nominal[1],
    TSetRoomConst=sum(transferParameters.TDem_nominal)/transferParameters.nParallelDem,
    nMin=0)
    annotation (choicesAllMatching=true, Placement(transformation(extent={{
            -106,-26},{-84,-4}})));
  replaceable
    BESMod.Systems.Hydraulical.Control.Components.ThermostaticValveController.ThermostaticValvePIControlled
    thermostaticValveController constrainedby
    BESMod.Systems.Hydraulical.Control.Components.ThermostaticValveController.BaseClasses.PartialThermostaticValveController(
      final nZones=transferParameters.nParallelDem, final leakageOpening=
        thermostaticValveParameters.leakageOpening) annotation (
      choicesAllMatching=true, Placement(transformation(extent={{66,-32},{92,-2}})));

  replaceable parameter
    BESMod.Systems.Hydraulical.Control.RecordsCollection.ThermostaticValveDataDefinition
    thermostaticValveParameters annotation (choicesAllMatching=true, Placement(
        transformation(extent={{198,-80},{218,-60}})));
  Modelica.Blocks.Logical.Switch SQLorNot
    annotation (Placement(transformation(extent={{128,-66},{148,-46}})));
  Modelica.Blocks.Sources.Constant Valve_open(k=1)
    annotation (Placement(transformation(extent={{-38,-62},{-12,-36}})));
  MA_Daniel.AdaptedModels.HeatingCurveDaniel heatingCurveDaniel
    annotation (Placement(transformation(extent={{-118,12},{-98,32}})));
  Modelica.Blocks.Logical.OnOffController onOffController(bandwidth=6)
    annotation (Placement(transformation(extent={{22,50},{42,70}})));
  BESMod.Systems.Hydraulical.Control.Components.HeatPumpNSetController.PI_InverterHeatPumpController
    pI_InverterHeatPumpController(
    P=1,
    nMin=1/3,
    T_I=100)
    annotation (Placement(transformation(extent={{72,50},{92,70}})));
equation

  connect(realPassThrough_T_Amb1.y, sigBusGen.hp_bus.TOdaMea) annotation (Line(
        points={{-204.9,-47},{-200,-47},{-200,-68},{-152,-68},{-152,-99}},
        color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(realPassThrough_T_Amb1.u, weaBus.TDryBul) annotation (
      Line(points={{-230.2,-47},{-236,-47},{-236,-26},{-210,-26},{-210,2},{
          -237,2}},                                                       color=
         {0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(thermostaticValveController.TZoneMea, buiMeaBus.TZoneMea) annotation (
     Line(points={{63.4,-8},{56,-8},{56,78},{65,78},{65,103}},
                                                  color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-3,6},{-3,6}},
      horizontalAlignment=TextAlignment.Right));
  connect(SQLorNot.y, sigBusTra.opening[1]) annotation (Line(points={{149,-56},{
          174,-56},{174,-100}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(SQLorNot.u2, sigBusDistr.bSQL) annotation (Line(points={{126,-56},{30,
          -56},{30,-100},{1,-100}}, color={255,0,255}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(SQLorNot.u1, Valve_open.y) annotation (Line(points={{126,-48},{-4,-48},
          {-4,-49},{-10.7,-49}}, color={0,0,127}));
  connect(thermostaticValveController.opening[1], SQLorNot.u3) annotation (Line(
        points={{94.6,-17},{100,-17},{100,-64},{126,-64}}, color={0,0,127}));
  connect(thermostaticValveController.TZoneSet[1], useProBus.TZoneSet[1])
    annotation (Line(points={{63.4,-26},{-6,-26},{-6,70},{-76,70},{-76,103},{
          -119,103}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(heatingCurveDaniel.TSet, sigBusDistr.TFlowHeaCur) annotation (Line(
        points={{-97,22},{-80,22},{-80,18},{-60,18},{-60,-100},{1,-100}}, color
        ={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(heatingCurveDaniel.TRaumnom, useProBus.TZoneSet[1]) annotation (Line(
        points={{-108,34},{-108,74},{-119,74},{-119,103}}, color={0,0,127}),
      Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(heatingCurveDaniel.TOda, weaBus.TDryBul) annotation (Line(points={{
          -120,22},{-210,22},{-210,2},{-237,2}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(onOffController.y, pI_InverterHeatPumpController.HP_On)
    annotation (Line(points={{43,60},{70,60}}, color={255,0,255}));
  annotation (Diagram(coordinateSystem(extent={{-240,-140},{240,100}})), Icon(
        coordinateSystem(extent={{-240,-140},{240,100}})));
end Controller_for_HiL;
