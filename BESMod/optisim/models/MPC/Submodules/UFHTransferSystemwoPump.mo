within MPC.Submodules;
model UFHTransferSystemwoPump
  extends BESMod.Systems.Hydraulical.Transfer.BaseClasses.PartialTransfer(
      final nParallelSup=1, final dp_nominal=fill(0, nParallelDem));

  IBPSA.Fluid.FixedResistances.PressureDrop res1[nParallelDem](
    redeclare package Medium = Medium,
    each final dp_nominal=1,
    final m_flow_nominal=m_flow_nominal)     "Hydraulic resistance of supply"
    annotation (Placement(transformation(
        extent={{-10.5,-12},{10.5,12}},
        rotation=0,
        origin={-24.5,88})));

  BESMod.Systems.Hydraulical.Components.UFH.PanelHeating panelHeating[
    nParallelDem](
    redeclare package Medium = Medium,
    final floorHeatingType=floorHeatingType,
    each final dis=5,
    final A=UFHParameters.area,
    final T0=TDem_nominal,
    each calcMethod=1) annotation (Placement(transformation(
        extent={{-23,-10},{23,10}},
        rotation=270,
        origin={5,-4})));

  Modelica.Thermal.HeatTransfer.Sources.FixedTemperature
                                                      fixedTemperature
                                                                   [nParallelDem](each final
            T=UFHParameters.T_floor)
               annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={-90,10})));
  Modelica.Thermal.HeatTransfer.Sensors.HeatFlowSensor heatFlowSensor
                                                                   [nParallelDem]
               annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={-46,-6})));
  Modelica.Thermal.HeatTransfer.Sources.FixedHeatFlow fixedHeatFlow[nParallelDem](each final
            Q_flow=0)
               annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={-92,-20})));
  Modelica.Thermal.HeatTransfer.Components.HeatCapacitor
                                                      heatCapacitor[nParallelDem](each final
            C=100)
               annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={-114,2})));

  replaceable parameter
    BESMod.Systems.Hydraulical.Transfer.RecordsCollection.UFHData UFHParameters
    constrainedby BESMod.Systems.Hydraulical.Transfer.RecordsCollection.UFHData(
      nZones=nParallelDem, area=AZone) annotation (choicesAllMatching=true,
      Placement(transformation(extent={{26,40},{46,60}})));

  BESMod.Utilities.KPIs.InputKPICalculator inputKPICalculatorOpening[
    nParallelDem](
    unit=fill("", nParallelDem),
    integralUnit=fill("s", nParallelDem),
    each calc_singleOnTime=false,
    each calc_integral=false,
    each calc_totalOnTime=false,
    each calc_numSwi=false,
    each calc_movAve=false)
    annotation (Placement(transformation(extent={{-46,-86},{-26,-50}})));
  BESMod.Utilities.KPIs.InputKPICalculator inputKPICalculatorLossUFH[
    nParallelDem](
    unit=fill("W", nParallelDem),
    integralUnit=fill("J", nParallelDem),
    each calc_singleOnTime=false,
    each calc_integral=false,
    each calc_totalOnTime=false,
    each calc_numSwi=false,
    each calc_movAve=false)
    annotation (Placement(transformation(extent={{-62,-120},{-42,-84}})));
  BESMod.Utilities.Electrical.ZeroLoad zeroLoad
    annotation (Placement(transformation(extent={{32,-108},{52,-88}})));
  BESMod.Systems.Hydraulical.Distribution.Components.Valves.PressureReliefValve
    pressureReliefValveOnOff(
    redeclare final package Medium = Medium,
    m_flow_nominal=mSup_flow_nominal[1],
    dpFullOpen_nominal=dpFullOpen_nominal,
    dpThreshold_nominal=perPreRelValOpens*dpFullOpen_nominal,
    l=transferDataBaseDefinition.leakageOpening,
    use_inputFilter=false,
    dpValve_nominal=transferDataBaseDefinition.dpPumpHeaCir_nominal,
    val(R=50))
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-142,4})));
  AixLib.Fluid.Actuators.Valves.TwoWayEqualPercentage val[nParallelDem](
    redeclare each final package Medium = Medium,
    final m_flow_nominal=m_flow_nominal,
    each final CvData=AixLib.Fluid.Types.CvTypes.OpPoint,
    each final deltaM=deltaMValve,
    each final from_dp=from_dpBranches,
    final dpValve_nominal=transferDataBaseDefinition.dpHeaSysValve_nominal,
    each final allowFlowReversal=allowFlowReversal,
    each final show_T=show_T,
    each final linearized=linearized,
    final dpFixed_nominal=transferDataBaseDefinition.dpHeaSysPreValve_nominal,
    each final l=transferDataBaseDefinition.leakageOpening,
    each final R=R,
    each final delta0=delta0,
    each final rhoStd=rho,
    each final use_inputFilter=use_inputFilter,
    each final riseTime=riseTime,
    each final init=init,
    each final y_start=y_start)
                         annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={0,42})));
  parameter Modelica.Units.SI.PressureDifference dpFullOpen_nominal=
      99999.99999999999*(73835.65852*0.8)
    "Pressure difference at which valve is fully open";
  parameter Real perPreRelValOpens=0.95 "Percentage of nominal pressure difference at which the pressure relief valve starts to open" annotation(Dialog(enable=use_preRelVal));
  parameter Boolean from_dpBranches=false
    "= true, use m_flow = f(dp) else dp = f(m_flow)" annotation (Dialog(tab="Advanced"));
  parameter Boolean linearized=false
  "= true, use linear relation between m_flow and dp for any flow rate"  annotation (Dialog(tab="Advanced"));
  parameter Real deltaMValve=0.02
  "Fraction of nominal flow rate where linearization starts, if y=1" annotation (Dialog(tab="Advanced", group="Transition to laminar"));
  parameter Modelica.Units.SI.Time riseTime=120
    "Rise time of the filter (time to reach 99.6 % of an opening step)";
  parameter Modelica.Blocks.Types.Init init=Modelica.Blocks.Types.Init.InitialOutput
    "Type of initialization (no init/steady state/initial state/initial output)";
  parameter Real y_start=1 "Initial value of output";
  parameter Real R=50 "Rangeability, R=50...100 typically";
  parameter Real delta0=0.01
    "Range of significant deviation from equal percentage law";
  parameter Boolean use_inputFilter=true
    "= true, if opening is filtered with a 2nd order CriticalDamping filter";
  parameter Modelica.Units.SI.MassFlowRate m_flow_small(min=0) = 1E-4*abs(
    sum(m_flow_nominal))
     "Small mass flow rate for regularization of zero flow" annotation(Dialog(tab="Advanced"));

  replaceable parameter BESMod.Systems.Hydraulical.Transfer.RecordsCollection.TransferDataBaseDefinition
    transferDataBaseDefinition constrainedby
    BESMod.Systems.Hydraulical.Transfer.RecordsCollection.TransferDataBaseDefinition(
    final Q_flow_nominal=Q_flow_nominal .* f_design,
    final nZones=nParallelDem,
    final AFloor=ABui,
    final heiBui=hBui,
    final mRad_flow_nominal=m_flow_nominal,
    final mHeaCir_flow_nominal=mSup_flow_nominal[1])
    annotation (choicesAllMatching=true, Placement(transformation(extent={{58,72},
            {78,92}})));
protected
  parameter
    BESMod.Systems.Hydraulical.Components.UFH.ActiveWallBaseDataDefinition
    floorHeatingType[nParallelDem]={
      BESMod.Systems.Hydraulical.Components.UFH.ActiveWallBaseDataDefinition(
      Temp_nom=Modelica.Units.Conversions.from_degC({TTra_nominal[i],
        TTra_nominal[i] - dTTra_nominal[i],TDem_nominal[i]}),
      q_dot_nom=Q_flow_nominal[i]/UFHParameters.area[i],
      k_isolation=UFHParameters.k_top[i] + UFHParameters.k_down[i],
      k_top=UFHParameters.k_top[i],
      k_down=UFHParameters.k_down[i],
      VolumeWaterPerMeter=0,
      eps=0.9,
      C_ActivatedElement=UFHParameters.C_ActivatedElement[i],
      c_top_ratio=UFHParameters.c_top_ratio[i],
      PressureDropExponent=0,
      PressureDropCoefficient=0,
      diameter=UFHParameters.diameter) for i in 1:nParallelDem};

equation

  for i in 1:nParallelDem loop
  if UFHParameters.is_groundFloor[i] then
   connect(fixedHeatFlow[i].port, heatCapacitor[i].port) annotation (Line(points=
         {{-82,-20},{-76,-20},{-76,-8},{-114,-8}}, color={191,0,0}));
   connect(fixedTemperature[i].port, heatFlowSensor[i].port_a) annotation (Line(
      points={{-80,10},{-64,10},{-64,-6},{-56,-6}},
      color={191,0,0},
      pattern=LinePattern.Dash));
  else
   connect(fixedHeatFlow[i].port, heatFlowSensor[i].port_a) annotation (Line(
      points={{-82,-20},{-66,-20},{-66,-6},{-56,-6}},
      color={191,0,0},
      pattern=LinePattern.Dash));
   connect(fixedTemperature[i].port, heatCapacitor[i].port) annotation (Line(
      points={{-80,10},{-80,2},{-70,2},{-70,-8},{-114,-8}},
      color={191,0,0},
      pattern=LinePattern.Dash));
  end if;
  end for;

  connect(panelHeating.thermConv, heatPortCon) annotation (Line(points={{16.6667,
          -7.22},{52,-7.22},{52,42},{100,42},{100,40}}, color={191,0,0}));
  connect(panelHeating.starRad, heatPortRad) annotation (Line(points={{16,-1.24},
          {40,-1.24},{40,-40},{100,-40}}, color={0,0,0}));

  connect(heatFlowSensor.port_b, panelHeating.ThermDown) annotation (Line(
        points={{-36,-6},{-22,-6},{-22,-5.84},{-6,-5.84}}, color={191,0,0}));

  connect(inputKPICalculatorOpening.KPIBus, outBusTra.opening) annotation (Line(
      points={{-25.8,-76},{0,-76},{0,-104}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(heatFlowSensor.Q_flow, inputKPICalculatorLossUFH.u) annotation (Line(
        points={{-46,-17},{-46,-22},{-62,-22},{-62,-102},{-48.2,-102}}, color={
          0,0,127}));
  connect(inputKPICalculatorLossUFH.KPIBus, outBusTra.QLossUFH) annotation (
      Line(
      points={{-25.8,-102},{-14,-102},{-14,-100},{0,-100},{0,-104}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(zeroLoad.internalElectricalPin, internalElectricalPin) annotation (
      Line(
      points={{52,-98},{72,-98}},
      color={0,0,0},
      thickness=1));
  connect(portTra_in[1], pressureReliefValveOnOff.port_a) annotation (Line(
        points={{-100,38},{-86,38},{-86,30},{-120,30},{-120,14},{-142,14}},
        color={0,127,255}));
  connect(pressureReliefValveOnOff.port_b, portTra_out[1]) annotation (Line(
        points={{-142,-6},{-124,-6},{-124,-42},{-100,-42}}, color={0,127,255}));
  connect(panelHeating.port_a, val.port_b) annotation (Line(points={{3.33333,19},
          {3.33333,24},{-1.83187e-15,24},{-1.83187e-15,32}}, color={0,127,255}));
  connect(val.port_a, res1.port_b) annotation (Line(points={{1.77636e-15,52},{1.77636e-15,
          70},{-14,70},{-14,88}},                            color={0,127,255}));
  connect(inputKPICalculatorOpening.u, traControlBus.opening) annotation (Line(
        points={{-46.2,-76},{-160,-76},{-160,100},{0,100}},
                                                          color={0,0,127}),
      Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(portTra_out, panelHeating.port_b) annotation (Line(points={{-100,
          -42},{3.33333,-42},{3.33333,-27}}, color={0,127,255}));
  connect(portTra_in, res1.port_a) annotation (Line(points={{-100,38},{-64,38},
          {-64,82},{-58,82},{-58,88},{-35,88}}, color={0,127,255}));
  connect(val.y, traControlBus.opening) annotation (Line(points={{12,42},{18,42},
          {18,84},{0,84},{0,100}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
end UFHTransferSystemwoPump;
