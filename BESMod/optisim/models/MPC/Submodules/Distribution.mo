within MPC.Submodules;
package Distribution
    extends BESMod.Utilities.Icons.SystemIcon;

  partial record BaseHiLPar
    extends Modelica.Icons.Record;
    parameter Integer n_reaVal "Number of values to subscribe" annotation (Dialog(tab="MQTT"));
    parameter String topicsToRea[n_reaVal] "Topics to subscribe" annotation (Dialog(tab="MQTT"));
    parameter Integer n_wriVal "Number of values to publish" annotation (Dialog(tab="MQTT"));
    parameter String topicsToPub[n_wriVal] "Topics to publish" annotation (Dialog(tab="MQTT"));
    parameter Real staReaVal[n_reaVal] "Used before any message has been received" annotation (Dialog(tab="MQTT"));
    parameter Real timConDamReaVal[n_reaVal] "Time constants for critical dumper for read values" annotation (Dialog(tab="MQTT"));
    parameter Real timConDamWriVal[n_wriVal] "Time constants for critical dumper for write values" annotation (Dialog(tab="MQTT"));
    parameter Boolean useJSON = false "True, if JSON format should be used" annotation (Dialog(tab="MQTT"));
    parameter Integer nKey[size(topicsToPub,1)] = fill(1,size(topicsToPub,1)) "number of keys of each topic" annotation (Dialog(tab="MQTT"));
    parameter String key[sum(nKey)] = fill("x", sum(nKey)) "Keys if JSON is chosen" annotation (Dialog(tab="MQTT"));
    replaceable parameter
      MQTT_Interface.MQTTConnectionPara.BasisForMQTTConnection mqttConParam
      constrainedby MQTT_Interface.MQTTConnectionPara.BasisForMQTTConnection
      "MQTT server information" annotation (choicesAllMatching=true, Dialog(tab="MQTT"));

    parameter Modelica.Units.SI.Time startTimeSQL=0
      "Start time, when data exchange with the set frequency (rate) begins"
      annotation (Dialog(enable=use_SQL, group="Data exchange", tab="MQTT"));
    parameter Modelica.Units.SI.Time samRate=1
      "Rate / frequency of data exchange" annotation (Dialog(enable=use_SQL, group="Data exchange", tab="MQTT"));
    parameter Real kReadValues[n_reaVal]=fill(1, n_reaVal) "Mulitplicator for input read values (e.g. unit conversion)" annotation (Dialog(tab="MQTT", enable=use_SQL, group="Unit conversion - Read values"));
    parameter Real addReadValues[n_reaVal]=fill(0, n_reaVal) "Add constants for input read values (e.g. unit conversion)" annotation (Dialog(tab="MQTT",enable=use_SQL, group="Unit conversion - Read values"));
    parameter Real maxReadValues[n_reaVal]=fill(Modelica.Constants.inf, n_reaVal) "Upper limits of read signals" annotation (Dialog(tab="MQTT",enable=use_SQL, group="Unit conversion - Read values"));
    parameter Real minReadValues[n_reaVal]=fill(-1*Modelica.Constants.inf, n_reaVal) "Lower limits of read signals" annotation (Dialog(tab="MQTT",enable=use_SQL, group="Unit conversion - Read values"));

    parameter Real kWriteValues[n_wriVal]=fill(1, n_wriVal) "Mulitplicator for input write values (e.g. unit conversion)" annotation (Dialog(tab="MQTT",enable=use_SQL, group="Unit conversion - Write values"));
    parameter Real addWriteValues[n_wriVal]=fill(0, n_wriVal) "Add constants for input write values (e.g. unit conversion)" annotation (Dialog(tab="MQTT",enable=use_SQL, group="Unit conversion - Write values"));
    parameter Real maxWriteValues[n_wriVal]=fill(Modelica.Constants.inf, n_wriVal) "Upper limits of write signals" annotation (Dialog(tab="MQTT",enable=use_SQL, group="Unit conversion - Write values"));
    parameter Real minWriteValues[n_wriVal]=fill(-1*Modelica.Constants.inf, n_wriVal) "Lower limits of write signals" annotation (Dialog(tab="MQTT",enable=use_SQL, group="Unit conversion - Write values"));
  end BaseHiLPar;

  record HiL1PCMTopics
    extends BaseHiLPar(
      addWriteValues={-273.15,0,0,-273.15},
      redeclare MQTT_Interface.MQTTConnectionPara.TickStackOpenstack mqttConParam,
      kWriteValues={1,3600,3600,1},
      addReadValues={0,273.15,0,273.15,273.15},
      kReadValues={1/3600,1,1/3600,1,1},
      timConDamWriVal={5,5,1,5},
      timConDamReaVal={10,1,10,1,1},
      staReaVal={1206,30,0,50,10},
      topicsToPub={"hil1/Sim/T_RL","hil1/Sim/VFlowSetDHW","hil1/Sim/VFlowPreCirc",
          "hil1/Sim/T_setColDHW"},
      topicsToRea={"hil1/HYD.Bank[1].Vdot.RealVar",
          "hil1/HYD.Bank[1].TempInA.RealVar","hil1/HYD.Bank[3].Vdot.RealVar",
          "hil1/HYD.Bank[3].TempInA.RealVar","hil1/HYD.Bank[3].TempInB.RealVar"},
      n_reaVal=5,
      n_wriVal=4);

  end HiL1PCMTopics;

  package Test
  extends Modelica.Icons.ExamplesPackage;
    model TestOnlyBuilding
      extends BESMod.Systems.Hydraulical.Distribution.Tests.PartialTest(
          redeclare MPC.Submodules.Distribution.HiLMqttConnection distribution(
          T_start(displayUnit="K") = 293.15,
          nParallelDem=1,
          nParallelSup=1,
          dTTra_nominal={30},
          m_flow_nominal={1000/3600},
          dTTraDHW_nominal=5,
          designType=BESMod.Systems.Hydraulical.Distribution.Types.DHWDesignType.NoStorage,
          QDHWStoLoss_flow=0,
          VStoDHW=0,
          dpSup_nominal={10000},
          dpDem_nominal={10000},
          redeclare MPC.Submodules.Distribution.HiL1PCMTopics topicsMQTT,
          mQTTAndConst(startTimeSQL=10)));

      extends Modelica.Icons.Example;

      Modelica.Blocks.Sources.Constant conTflo(each final k=273.15 + 35)
        annotation (Placement(transformation(
            extent={{-10,-10},{10,10}},
            rotation=0,
            origin={-66,60})));
    equation
      connect(conTflo.y, sigBusDistr.TFlowHeaCur) annotation (Line(points={{-55,60},
              {-42,60},{-42,81},{-14,81}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
      annotation (experiment(StopTime=86400, __Dymola_Algorithm="Dassl"));
    end TestOnlyBuilding;
  end Test;

  model MitsubishiDistributionSystem
    extends
      BESMod.Systems.Hydraulical.Distribution.BaseClasses.PartialDistribution(
      final dpSup_nominal={2*(threeWayValveParameters.dpValve_nominal + max(
          threeWayValveParameters.dp_nominal))},
      final dTTraDHW_nominal=dynamicHX.dT_nom,
      final dTTra_nominal={0},
      final m_flow_nominal=mDem_flow_nominal,
      final VStoDHW=dhwParameters.V,
      final QDHWStoLoss_flow=dhwParameters.QLoss_flow,
      designType=BESMod.Systems.Hydraulical.Distribution.Types.DHWDesignType.NoStorage,
      final TSup_nominal=TDem_nominal .+ dTLoss_nominal .+ dTTra_nominal,
      final nParallelSup=1,
      final nParallelDem=1);
    parameter Modelica.Units.SI.PressureDifference dpHPToHX_nominal(displayUnit="Pa")=
       20000
      "Pressure difference";
    parameter Modelica.Units.SI.PressureDifference dpHXToDHWSto_nominal(
        displayUnit="Pa")=20000
      "Pressure difference";
    parameter Modelica.Units.SI.TemperatureDifference dTDHWHX_nominal=5
      "Temperature difference at nominal conditions (used to calculate Gc)";
    parameter Modelica.Units.SI.HeatFlowRate QHXDHW_flow_nominal=
        QDHW_flow_nominal
      "Temperature difference at nominal conditions (used to calculate Gc)";
    parameter Modelica.Units.SI.MassFlowRate mHXDHWCircuit_flow_nominal=
        mSup_flow_nominal[1]
      "Nominal mass flow rate";
    parameter Integer nNodes=2 "Spatial segmentation of HX";
    replaceable parameter
      BESMod.Systems.RecordsCollection.Movers.MoverBaseDataDefinition
      pumpData annotation (choicesAllMatching=true, Placement(transformation(extent={{-98,-58},
              {-82,-42}})));
    replaceable parameter
      BESMod.Systems.RecordsCollection.TemperatureSensors.TemperatureSensorBaseDefinition
      temperatureSensorData
      annotation (choicesAllMatching=true, Placement(transformation(extent={{42,82},
              {58,96}})));
    replaceable parameter
      BESMod.Systems.RecordsCollection.Valves.ThreeWayValve
      threeWayValveParameters constrainedby
      BESMod.Systems.RecordsCollection.Valves.ThreeWayValve(
      final dp_nominal={dpDem_nominal[1],dpHPToHX_nominal},
      final m_flow_nominal=mSup_flow_nominal[1],
      final fraK=1,
      use_inputFilter=false) annotation (choicesAllMatching=
         true, Placement(transformation(extent={{-78,82},{-62,98}})));

    replaceable parameter
      BESMod.Systems.Hydraulical.Distribution.RecordsCollection.BufferStorage.BufferStorageBaseDataDefinition
      dhwParameters constrainedby
      BESMod.Systems.Hydraulical.Distribution.RecordsCollection.BufferStorage.BufferStorageBaseDataDefinition(
      final Q_flow_nominal=0,
      final VPerQ_flow=0,
      final rho=rho,
      final c_p=cp,
      final V=V,
      final TAmb=TAmb,
      T_m=TDHW_nominal,
      final QHC1_flow_nominal=QDHW_flow_nominal,
      final mHC1_flow_nominal=mSup_flow_nominal[1],
      final use_HC2=storageDHW.useHeatingCoil2,
      final use_HC1=storageDHW.useHeatingCoil1,
      final dTLoadingHC2=9999999,
      final fHeiHC2=1,
      final fDiaHC2=1,
      final QHC2_flow_nominal=9999999,
      final mHC2_flow_nominal=9999999)
      annotation (choicesAllMatching=true, Placement(transformation(extent={{64,-18},
              {78,-4}})));

    Modelica.Blocks.Sources.RealExpression T_stoDHWTop(final y(unit="K", displayUnit="degC")=storageDHW.layer[end].T) annotation (Placement(transformation(
          extent={{-10,-5},{10,5}},
          rotation=0,
          origin={-30,83})));
    Modelica.Blocks.Sources.RealExpression T_stoDHWBot(final y(unit="K", displayUnit="degC")=storageDHW.layer[1].T)
      annotation (Placement(transformation(
          extent={{-10,-5},{10,5}},
          rotation=0,
          origin={-30,95})));

    Modelica.Thermal.HeatTransfer.Sources.FixedTemperature fixedTemperatureDHW(final T=dhwParameters.TAmb)           annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=180,
          origin={70,-50})));

    AixLib.Fluid.Storage.BufferStorage storageDHW(
      redeclare final package Medium = MediumDHW,
      final energyDynamics=energyDynamics,
      final p_start=p_start,
      final mSenFac=1,
      redeclare final package MediumHC1 = MediumGen,
      redeclare final package MediumHC2 = MediumGen,
      final m1_flow_nominal=mSup_flow_nominal[1],
      final m2_flow_nominal=mDHW_flow_nominal,
      final mHC1_flow_nominal=dhwParameters.mHC1_flow_nominal,
      final mHC2_flow_nominal=dhwParameters.mHC2_flow_nominal,
      final useHeatingCoil1=false,
      final useHeatingCoil2=false,
      final useHeatingRod=dhwParameters.use_hr,
      final TStart=T_start,
      redeclare final BESMod.Systems.Hydraulical.Distribution.RecordsCollection.BufferStorage.bufferData
                                                                 data(
        final hTank=dhwParameters.h,
        hHC1Low=0,
        hHR=dhwParameters.nLayerHR/dhwParameters.nLayer*dhwParameters.h,
        final dTank=dhwParameters.d,
        final sWall=dhwParameters.sIns/2,
        final sIns=dhwParameters.sIns/2,
        final lambdaWall=dhwParameters.lambda_ins,
        final lambdaIns=dhwParameters.lambda_ins,
        final rhoIns=373,
        final cIns=1000,
        pipeHC1=dhwParameters.pipeHC1,
        pipeHC2=dhwParameters.pipeHC2,
        lengthHC1=dhwParameters.lengthHC1,
        lengthHC2=dhwParameters.lengthHC2),
      final n=dhwParameters.nLayer,
      final hConIn=dhwParameters.hConIn,
      final hConOut=dhwParameters.hConOut,
      final hConHC1=dhwParameters.hConHC1,
      final hConHC2=dhwParameters.hConHC2,
      final upToDownHC1=true,
      final upToDownHC2=true,
      final TStartWall=T_start,
      final TStartIns=T_start,
      redeclare model HeatTransfer =
          AixLib.Fluid.Storage.BaseClasses.HeatTransferBuoyancyWetter,
      final allowFlowReversal_layers=allowFlowReversal,
      final allowFlowReversal_HC1=allowFlowReversal,
      final allowFlowReversal_HC2=allowFlowReversal)
      annotation (Placement(transformation(extent={{20,-26},{40,0}})));

    BESMod.Systems.Hydraulical.Distribution.Components.Valves.ThreeWayValveWithFlowReturn
      threeWayValveWithFlowReturn(
      redeclare package Medium = MediumGen,
      final energyDynamics=energyDynamics,
      final p_start=p_start,
      final T_start=T_start,
      final X_start=X_start,
      final C_start=C_start,
      final C_nominal=C_nominal,
      final mSenFac=mSenFac,
      redeclare BESMod.Systems.RecordsCollection.Valves.DefaultThreeWayValve
        parameters=threeWayValveParameters)
      annotation (Placement(transformation(extent={{-80,40},{-60,60}})));

    BESMod.Utilities.KPIs.InternalKPICalculator internalKPICalculatorDHWLoss(
      unit="W",
      integralUnit="J",
      calc_singleOnTime=false,
      calc_integral=true,
      calc_totalOnTime=false,
      calc_numSwi=false,
      calc_movAve=false,
      calc_intBelThres=false,
      y=fixedTemperatureDHW.port.Q_flow)
      annotation (Placement(transformation(extent={{-40,-108},{-20,-70}})));
    IBPSA.Fluid.Sensors.TemperatureTwoPort senTBuiSup(
      redeclare final package Medium = Medium,
      final allowFlowReversal=allowFlowReversal,
      m_flow_nominal=m_flow_nominal[1],
      tau=temperatureSensorData.tau,
      initType=temperatureSensorData.initType,
      T_start=T_start,
      final transferHeat=true,
      TAmb=TAmb,
      tauHeaTra=temperatureSensorData.tauHeaTra)
      "Temperature at supply for building" annotation (Placement(transformation(
          extent={{10,10},{-10,-10}},
          rotation=180,
          origin={70,70})));

    BESMod.Utilities.Electrical.ZeroLoad zeroLoad
      annotation (Placement(transformation(extent={{20,-100},{40,-80}})));
    IBPSA.Fluid.Sensors.TemperatureTwoPort senTBuiRet(
      redeclare final package Medium = Medium,
      final allowFlowReversal=allowFlowReversal,
      m_flow_nominal=m_flow_nominal[1],
      tau=temperatureSensorData.tau,
      initType=temperatureSensorData.initType,
      T_start=T_start,
      final transferHeat=temperatureSensorData.transferHeat,
      TAmb=temperatureSensorData.TAmb,
      tauHeaTra=temperatureSensorData.tauHeaTra)
      "Temperature at supply for building" annotation (Placement(transformation(
          extent={{10,10},{-10,-10}},
          rotation=180,
          origin={32,40})));
    parameter Modelica.Units.SI.Volume V=0.2
                                         "Volume of storage";
    AixLib.Fluid.HeatExchangers.DynamicHX dynamicHX(
      redeclare final package Medium1 = MediumGen,
      redeclare final package Medium2 = MediumDHW,
      final m1_flow_nominal=mSup_flow_nominal[1],
      final m2_flow_nominal=mHXDHWCircuit_flow_nominal,
      final dp1_nominal=dpHPToHX_nominal,
      final dp2_nominal=dpHXToDHWSto_nominal,
      final nNodes=nNodes,
      redeclare final AixLib.Fluid.MixingVolumes.MixingVolume vol1,
      redeclare final AixLib.Fluid.MixingVolumes.MixingVolume vol2,
      final dT_nom=dTDHWHX_nominal,
      final Q_nom=QHXDHW_flow_nominal)
      annotation (Placement(transformation(extent={{10,-10},{-10,10}},
          rotation=180,
          origin={-50,12})));

    IBPSA.Fluid.Movers.SpeedControlled_y pump(
      redeclare final package Medium = MediumDHW,
      final energyDynamics=energyDynamics,
      final p_start=p_start,
      final T_start=T_start,
      final allowFlowReversal=allowFlowReversal,
      final show_T=show_T,
      redeclare
        BESMod.Systems.RecordsCollection.Movers.AutomaticConfigurationData per(
        final speed_rpm_nominal=pumpData.speed_rpm_nominal,
        final m_flow_nominal=mHXDHWCircuit_flow_nominal,
        final dp_nominal=dpHXToDHWSto_nominal,
        final rho=rho,
        final V_flowCurve=pumpData.V_flowCurve,
        final dpCurve=pumpData.dpCurve),
      final inputType=IBPSA.Fluid.Types.InputType.Continuous,
      final addPowerToMedium=pumpData.addPowerToMedium,
      final tau=pumpData.tau,
      final use_inputFilter=pumpData.use_inputFilter,
      final riseTime=pumpData.riseTimeInpFilter,
      final init=Modelica.Blocks.Types.Init.InitialOutput,
      final y_start=1)                 annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=180,
          origin={-50,-30})));

  initial equation
    assert(mDem_flow_nominal[1] == mSup_flow_nominal[1], "Nominal mass flow rates are not equal!", AssertionLevel.error);
  equation
    connect(T_stoDHWBot.y, sigBusDistr.TStoDHWBotMea) annotation (Line(points={{-19,95},
            {-10,95},{-10,101},{0,101}},                  color={0,0,127}), Text(
        string="%second",
        index=1,
        extent={{-6,3},{-6,3}},
        horizontalAlignment=TextAlignment.Right));
    connect(T_stoDHWTop.y, sigBusDistr.TStoDHWTopMea) annotation (Line(points={{-19,83},
            {0,83},{0,101}},                            color={0,0,127}), Text(
        string="%second",
        index=1,
        extent={{-6,3},{-6,3}},
        horizontalAlignment=TextAlignment.Right));
    connect(fixedTemperatureDHW.port, storageDHW.heatportOutside) annotation (
        Line(points={{60,-50},{52,-50},{52,-12.22},{39.75,-12.22}}, color={191,0,0}));
    connect(portDHW_in, storageDHW.fluidportBottom2) annotation (Line(points={{100,-82},
            {100,-34},{32,-34},{32,-26.13},{32.875,-26.13}},
                                                           color={0,127,255}));
    connect(portGen_in[1], threeWayValveWithFlowReturn.portGen_a) annotation (
        Line(points={{-100,80},{-86,80},{-86,54.4},{-80,54.4}}, color={0,127,255}));
    connect(portGen_out[1], threeWayValveWithFlowReturn.portGen_b) annotation (
        Line(points={{-100,40},{-100,46.4},{-80,46.4}},
          color={0,127,255}));
    connect(threeWayValveWithFlowReturn.uBuf, sigBusDistr.uThrWayVal) annotation (
       Line(points={{-70,62},{-70,72},{0,72},{0,101}},
                                                 color={0,0,127}), Text(
        string="%second",
        index=1,
        extent={{-3,6},{-3,6}},
        horizontalAlignment=TextAlignment.Right));
    connect(internalKPICalculatorDHWLoss.KPIBus, outBusDist.QDHWLoss) annotation (
       Line(
        points={{-19.8,-89},{-14,-89},{-14,-86},{0,-86},{0,-100}},
        color={255,204,51},
        thickness=0.5), Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}},
        horizontalAlignment=TextAlignment.Left));
    connect(senTBuiSup.port_b, portBui_out[1]) annotation (Line(points={{80,70},{86,
            70},{86,80},{100,80}},  color={0,127,255}));
    connect(senTBuiSup.T, sigBusDistr.TBuiSupMea) annotation (Line(points={{70,81},
            {70,100},{28,100},{28,76},{0,76},{0,101}},                 color={0,0,
            127}), Text(
        string="%second",
        index=1,
        extent={{-3,6},{-3,6}},
        horizontalAlignment=TextAlignment.Right));
    connect(zeroLoad.internalElectricalPin, internalElectricalPin) annotation (
        Line(
        points={{40,-90},{56,-90},{56,-84},{70,-84},{70,-98}},
        color={0,0,0},
        thickness=1));
    connect(storageDHW.fluidportTop2, portDHW_out) annotation (Line(points={{33.125,
            0.13},{32,0.13},{32,4},{86,4},{86,-22},{100,-22}},
                                                        color={0,127,255}));
    connect(threeWayValveWithFlowReturn.portBui_b, senTBuiSup.port_a) annotation (
       Line(points={{-60,58},{-42,58},{-42,62},{54,62},{54,70},{60,70}},
                                                                     color={0,127,
            255}));
    connect(senTBuiRet.port_b, portBui_in[1])
      annotation (Line(points={{42,40},{100,40}}, color={0,127,255}));
    connect(senTBuiRet.port_a, threeWayValveWithFlowReturn.portBui_a) annotation (
       Line(points={{22,40},{-40,40},{-40,54},{-60,54}}, color={0,127,255}));
    connect(senTBuiRet.T, sigBusDistr.TBuiRetMea) annotation (Line(points={{32,51},
            {32,76},{0,76},{0,101}}, color={0,0,127}), Text(
        string="%second",
        index=1,
        extent={{-3,6},{-3,6}},
        horizontalAlignment=TextAlignment.Right));
    connect(threeWayValveWithFlowReturn.portDHW_b, dynamicHX.port_a2) annotation (
       Line(points={{-60,46.4},{-48,46.4},{-48,24},{-38,24},{-38,18},{-40,18}},
          color={0,127,255}));
    connect(storageDHW.fluidportTop1, dynamicHX.port_b1) annotation (Line(points={
            {26.5,0.13},{26.5,4},{26,4},{26,6},{-40,6}}, color={0,127,255}));
    connect(dynamicHX.port_b2, threeWayValveWithFlowReturn.portDHW_a) annotation (
       Line(points={{-60,18},{-64,18},{-64,34},{-60,34},{-60,42.4}},
          color={0,127,255}));
    connect(pump.port_a, storageDHW.fluidportBottom1) annotation (Line(points={{-40,
            -30},{26.625,-30},{26.625,-26.26}}, color={0,127,255}));
    connect(pump.port_b, dynamicHX.port_a1) annotation (Line(points={{-60,-30},{-70,
            -30},{-70,6},{-60,6}}, color={0,127,255}));
    connect(pump.y, sigBusDistr.uDHWPump) annotation (Line(points={{-50,-42},{-50,
            -46},{-10,-46},{-10,22},{0,22},{0,101}}, color={0,0,127}), Text(
        string="%second",
        index=1,
        extent={{-3,-6},{-3,-6}},
        horizontalAlignment=TextAlignment.Right));
  end MitsubishiDistributionSystem;

  record HiL2MPCTopics
    extends BaseHiLPar(
    redeclare MQTT_Interface.MQTTConnectionPara.TickStackOpenstack mqttConParam,
    n_reaVal=5,
    n_wriVal=4,
    topicsToRea={"hil2/HYD.Bank[6].Vdot.REAL_VAR","detHP/IndoorUnit.T_FlowOutlet_IDU",
        "hil1/HYD.Bank[3].Vdot.RealVar","hil1/HYD.Bank[3].TempInA.RealVar","hil1/HYD.Bank[3].TempInB.RealVar"},
    topicsToPub={"hil2/Sim/T_RL","dummy1","dummy2","dummy3"},
    staReaVal={1206,30,0,50,10},
    timConDamReaVal={10,1,10,1,1},
    timConDamWriVal={5,5,1,5},
    kReadValues={1/3600,1,1/3600,1,1},
    addReadValues={0,273.15,0,273.15,273.15},
    kWriteValues={1,3600,3600,1},
    addWriteValues={-273.15,0,0,-273.15});
  end HiL2MPCTopics;

  model HiLMqttConnection
    extends
      BESMod.Systems.Hydraulical.Distribution.BaseClasses.PartialDistribution(
      VStoDHW=0,
      QDHWStoLoss_flow=0,
      designType=BESMod.Systems.Hydraulical.Distribution.Types.DHWDesignType.NoStorage,
      dpDem_nominal={0},
      dpSup_nominal={0},
      dTTraDHW_nominal=5,
      m_flow_nominal=mDem_flow_nominal,
      dTTra_nominal={0},
      nParallelSup=1,
      nParallelDem=1);

    MQTT_Interface.Models.MQTTAndConst mQTTAndConst(
      final nWriteValues=topicsMQTT.n_wriVal,
      final nReadValues=topicsMQTT.n_reaVal,
      final mqttConParam=topicsMQTT.mqttConParam,
      startTimeSQL=31536000,
      final namesToRead=topicsMQTT.topicsToRea,
      final standardReadValues=topicsMQTT.staReaVal,
      final namesToWrite=topicsMQTT.topicsToPub,
      final timeConstsDampReadValues=topicsMQTT.timConDamReaVal,
      final timeConstsDampWriteValues=topicsMQTT.timConDamWriVal,
      final kReadValues=topicsMQTT.kReadValues,
      final addReadValues=topicsMQTT.addReadValues,
      final maxReadValues=topicsMQTT.maxReadValues,
      final minReadValues=topicsMQTT.minReadValues,
      final kWriteValues=topicsMQTT.kWriteValues,
      final addWriteValues=topicsMQTT.addWriteValues,
      final maxWriteValues=topicsMQTT.maxWriteValues,
      final minWriteValues=topicsMQTT.minWriteValues,
      final useJSON=topicsMQTT.useJSON,
      final nKeys=topicsMQTT.nKey,
      final keys=topicsMQTT.key)
      annotation (Placement(transformation(extent={{-23,-23},{23,23}},
          rotation=180,
          origin={-57,-25})));

    Utilities.IdealSourcePumpTemperature            idealSourcePumpTemperature(
      redeclare final package Medium = Medium,
      energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
      energyDynamicsDelVol=Modelica.Fluid.Types.Dynamics.FixedInitial,
      TFlow_nominal=323.15,
      TRetDesign=313.15,
      m_flow_nominal=m_flow_nominal[1],
      dpExtra_nominal=0,
      redeclare AixLib.Fluid.Movers.Data.Pumps.Wilo.Stratos25slash1to4 per,
      initTypeDampTFlow=Modelica.Blocks.Types.Init.InitialOutput,
      y_start_TFlow=T_start,
      tauVolHeatUp=10,
      final p_start=p_start,
      final T_start=T_start)
      annotation (Placement(transformation(extent={{-40,10},{22,72}})));
    replaceable parameter MPC.Submodules.Distribution.HiL1PCMTopics topicsMQTT
      constrainedby Submodules.Distribution.BaseHiLPar
                               annotation (Placement(transformation(extent={{-98,
              -100},{-78,-80}})),    choicesAllMatching=true);
    Submodules.Demand.HiLDHWTab hiLDHWTab(DHWProfile=DHWProfile.table)
      annotation (Placement(transformation(extent={{-6,-74},{32,-36}})));

    BESMod.Systems.Demand.DHW.RecordsCollection.ProfileL DHWProfile
      annotation (Placement(transformation(extent={{-66,-98},{-46,-78}})));
    Modelica.Blocks.Sources.Constant m_flow_nom(k=m_flow_nominal[1])
      annotation (Placement(transformation(extent={{-44,84},{-28,100}})));
    BESMod.Utilities.Electrical.ZeroLoad zeroLoad
      annotation (Placement(transformation(extent={{28,-124},{48,-104}})));
    Modelica.Fluid.Sources.Boundary_pT BouGenSin(redeclare package Medium =
          Medium, nPorts=1) annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=270,
          origin={-80,102})));
    Modelica.Fluid.Sources.MassFlowSource_T bouSouGen(redeclare package Medium =
          Medium, nPorts=nParallelSup)
      annotation (Placement(transformation(extent={{-92,46},{-72,66}})));
  equation
    connect(mQTTAndConst.activeSQL, idealSourcePumpTemperature.switchToSQL)
      annotation (Line(points={{-82.3,-18.1},{-88,-18.1},{-88,41},{-43.1,41}},
                                                  color={255,0,255}));
    connect(idealSourcePumpTemperature.TReturnMix, mQTTAndConst.writeValues[1])
      annotation (Line(points={{25.1,19.3},{25.1,-14},{-8,-14},{-8,-25},{-31.7,
            -25}},
          color={0,0,127}));
    connect(mQTTAndConst.readValues[1], idealSourcePumpTemperature.m_flow_in_withSQL)
      annotation (Line(points={{-82.3,-25},{-94,-25},{-94,4},{-62,4},{-62,53.4},
            {-43.1,53.4}},
                    color={0,0,127}));
    connect(mQTTAndConst.readValues[2], idealSourcePumpTemperature.TFlow)
      annotation (Line(points={{-82.3,-25},{-94,-25},{-94,4},{-60,4},{-60,28.6},
            {-43.1,28.6}},
                    color={0,0,127}));
    connect(mQTTAndConst.readValues[3], hiLDHWTab.V_flow_mesDHW) annotation (Line(
          points={{-82.3,-25},{-88,-25},{-88,-54},{-26,-54},{-26,-40.94},{-6.38,
            -40.94}}, color={0,0,127}));
    connect(mQTTAndConst.readValues[4], hiLDHWTab.T_hotMesDHW) annotation (Line(
          points={{-82.3,-25},{-88,-25},{-88,-52.34},{-6.38,-52.34}}, color={0,0,
            127}));
    connect(mQTTAndConst.readValues[5], hiLDHWTab.T_coldMesDHW) annotation (Line(
          points={{-82.3,-25},{-88,-25},{-88,-64.12},{-6.38,-64.12}}, color={0,0,
            127}));
    connect(hiLDHWTab.v_flow_setDHW, mQTTAndConst.writeValues[2]) annotation (
        Line(points={{33.9,-48.54},{42,-48.54},{42,-25},{-31.7,-25}}, color={0,0,
            127}));
    connect(hiLDHWTab.V_flow_preCirDHW, mQTTAndConst.writeValues[3]) annotation (
        Line(points={{33.9,-60.7},{46,-60.7},{46,-25},{-31.7,-25}}, color={0,0,
            127}));
    connect(hiLDHWTab.T_setDHW, mQTTAndConst.writeValues[4]) annotation (Line(
          points={{33.9,-54.24},{50,-54.24},{50,-25},{-31.7,-25}}, color={0,0,127}));
    connect(portDHW_in, portDHW_in)
      annotation (Line(points={{100,-82},{100,-82}}, color={0,127,255}));
    connect(hiLDHWTab.V_flow_preCirDHW, outBusDist.V_flow_preCircDHW) annotation (
       Line(points={{33.9,-60.7},{40,-60.7},{40,-84},{0,-84},{0,-100}}, color={0,
            0,127}), Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}},
        horizontalAlignment=TextAlignment.Left));
    connect(hiLDHWTab.v_flow_setDHW, outBusDist.V_flow_setDHW) annotation (Line(
          points={{33.9,-48.54},{38,-48.54},{38,-100},{0,-100}}, color={0,0,127}),
        Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}},
        horizontalAlignment=TextAlignment.Left));
    connect(idealSourcePumpTemperature.port_b, portBui_out[1]) annotation (Line(
          points={{22,65.8},{50,65.8},{50,80},{100,80}}, color={0,127,255}));
    connect(idealSourcePumpTemperature.port_a, portBui_in[1]) annotation (Line(
          points={{21.38,41},{46,41},{46,56},{56,56},{56,40},{100,40}},  color={0,
            127,255}));
    connect(idealSourcePumpTemperature.TFlowHeaCur, sigBusDistr.TFlowHeaCur)
      annotation (Line(points={{-43.1,19.3},{-66,19.3},{-66,80},{0,80},{0,101}},
          color={0,0,127}), Text(
        string="%second",
        index=1,
        extent={{-6,3},{-6,3}},
        horizontalAlignment=TextAlignment.Right));
    connect(m_flow_nom.y, idealSourcePumpTemperature.m_flow_in_withoutSQL)
      annotation (Line(points={{-27.2,92},{-26,92},{-26,76},{-54,76},{-54,62.7},{-43.1,
            62.7}},
          color={0,0,127}));
    connect(zeroLoad.internalElectricalPin, internalElectricalPin) annotation (
        Line(
        points={{48,-114},{56,-114},{56,-84},{70,-84},{70,-98}},
        color={0,0,0},
        thickness=1));
    connect(portDHW_out, portDHW_in) annotation (Line(points={{100,-22},{74,-22},
            {74,-82},{100,-82}}, color={0,127,255}));
    connect(bouSouGen.ports, portGen_out) annotation (Line(points={{-72,56},{-64,
            56},{-64,40},{-100,40}}, color={0,127,255}));
    connect(BouGenSin.ports[1], portGen_in[1])
      annotation (Line(points={{-80,92},{-80,80},{-100,80}}, color={0,127,255}));
  end HiLMqttConnection;
end Distribution;
