within MPC.Submodules;
block comp_input_transformer "smoothes comp input"
  Modelica.Blocks.Logical.Timer timer_on
    annotation (Placement(transformation(extent={{56,-234},{76,-214}})));
  Modelica.Blocks.Nonlinear.Limiter limiter2(uMax=0.8889,
                                                     uMin=0.33334)
    annotation (Placement(transformation(extent={{70,-26},{86,-10}})));
  Modelica.Blocks.Sources.Constant const3(k=0)
    annotation (Placement(transformation(extent={{596,-228},{616,-208}})));
  Modelica.Blocks.Logical.Switch switch1
    annotation (Placement(transformation(extent={{670,-138},{690,-118}})));
  Modelica.Blocks.Logical.Timer timer_off
    annotation (Placement(transformation(extent={{74,-296},{94,-276}})));
  Modelica.Blocks.Interfaces.RealInput u
    annotation (Placement(transformation(extent={{-240,-110},{-200,-70}})));
  Modelica.Blocks.Interfaces.RealOutput y(start=0)
    annotation (Placement(transformation(extent={{844,-148},{864,-128}})));
  Modelica.Blocks.Logical.GreaterEqualThreshold greaterEqualThreshold3(threshold=
       0.1)
    annotation (Placement(transformation(extent={{738,-238},{758,-218}})));
  Modelica.Blocks.Logical.Not not1
    annotation (Placement(transformation(extent={{36,-296},{56,-276}})));
  Modelica.Blocks.Logical.And and1
    annotation (Placement(transformation(extent={{378,-136},{398,-116}})));
  Modelica.Blocks.Logical.GreaterEqualThreshold greaterEqualThreshold1(
      threshold=599)
    annotation (Placement(transformation(extent={{128,-296},{148,-276}})));
  Modelica.Blocks.Logical.GreaterEqualThreshold greaterEqualThreshold2(
      threshold=599)
    annotation (Placement(transformation(extent={{98,-232},{118,-212}})));
  Modelica.Blocks.MathBoolean.Or or1(nu=4)
    annotation (Placement(transformation(extent={{504,-190},{538,-156}})));
  Modelica.Blocks.Logical.And and2
    annotation (Placement(transformation(extent={{396,-222},{416,-202}})));
  Modelica.Blocks.Logical.Not not2
    annotation (Placement(transformation(extent={{146,-196},{166,-176}})));
  Modelica.Blocks.Logical.And and3
    annotation (Placement(transformation(extent={{300,-188},{320,-168}})));
  Modelica.Blocks.Logical.And and4
    annotation (Placement(transformation(extent={{360,-194},{380,-174}})));
  Modelica.Blocks.Logical.GreaterEqualThreshold greaterEqualThreshold(threshold=
       0.33334)
    annotation (Placement(transformation(extent={{56,-100},{76,-80}})));
  Modelica.Blocks.Logical.Pre pre1
    annotation (Placement(transformation(extent={{-20,-262},{0,-242}})));
  Modelica.Blocks.Logical.And and5
    annotation (Placement(transformation(extent={{460,-272},{480,-252}})));
  Modelica.Blocks.Logical.GreaterEqualThreshold greaterEqualThreshold4(
      threshold=0.1)
    annotation (Placement(transformation(extent={{226,-270},{246,-250}})));
  Modelica.Blocks.Interfaces.BooleanOutput HP_on
    annotation (Placement(transformation(extent={{834,-80},{854,-60}})));
  Modelica.Blocks.MathInteger.TriggeredAdd triggeredAdd
    annotation (Placement(transformation(extent={{474,-64},{486,-52}})));
  Modelica.Blocks.Sources.IntegerConstant integerConstant(final k=1)
    annotation (Placement(transformation(extent={{400,-66},{416,-50}})));
  Modelica.Blocks.Interfaces.IntegerOutput Starts
    annotation (Placement(transformation(extent={{834,-40},{854,-20}})));
equation
  connect(y,y)
    annotation (Line(points={{854,-138},{854,-138}},
                                                   color={0,0,127}));
  connect(limiter2.u,u)  annotation (Line(points={{68.4,-18},{-176,-18},{-176,
          -90},{-220,-90}},                   color={0,0,127}));
  connect(switch1.u1, limiter2.y) annotation (Line(points={{668,-120},{668,
          -18},{86.8,-18}}, color={0,0,127}));
  connect(switch1.u3, const3.y) annotation (Line(points={{668,-136},{668,-218},
          {617,-218}}, color={0,0,127}));
  connect(switch1.y, y) annotation (Line(points={{691,-128},{832,-128},{832,
          -138},{854,-138}}, color={0,0,127}));
  connect(greaterEqualThreshold3.u, y) annotation (Line(points={{736,-228},{
          726,-228},{726,-130},{722,-130},{722,-128},{832,-128},{832,-138},{
          854,-138}}, color={0,0,127}));
  connect(not1.y, timer_off.u)
    annotation (Line(points={{57,-286},{72,-286}}, color={255,0,255}));
  connect(timer_on.y, greaterEqualThreshold2.u)
    annotation (Line(points={{77,-224},{86,-224},{86,-222},{96,-222}},
                                                   color={0,0,127}));
  connect(greaterEqualThreshold2.y, and1.u2) annotation (Line(points={{119,-222},
          {212,-222},{212,-152},{376,-152},{376,-134}},       color={255,0,
          255}));
  connect(or1.y, switch1.u2) annotation (Line(points={{540.55,-173},{650,-173},
          {650,-128},{668,-128}}, color={255,0,255}));
  connect(and1.y, or1.u[1]) annotation (Line(points={{399,-126},{504,-126},{504,
          -177.462}},     color={255,0,255}));
  connect(greaterEqualThreshold1.y, and2.u2) annotation (Line(points={{149,
          -286},{392,-286},{392,-228},{394,-228},{394,-220}}, color={255,0,
          255}));
  connect(and2.y, or1.u[2]) annotation (Line(points={{417,-212},{432,-212},{432,
          -168},{436,-168},{436,-140},{504,-140},{504,-174.488}},     color={
          255,0,255}));
  connect(not2.u, greaterEqualThreshold2.y) annotation (Line(points={{144,-186},
          {128,-186},{128,-208},{132,-208},{132,-222},{119,-222}},
        color={255,0,255}));
  connect(not2.y, and3.u2)
    annotation (Line(points={{167,-186},{298,-186}}, color={255,0,255}));
  connect(and4.u1, and3.y) annotation (Line(points={{358,-184},{336,-184},{
          336,-178},{321,-178}}, color={255,0,255}));
  connect(and4.y, or1.u[3]) annotation (Line(points={{381,-184},{432,-184},{432,
          -168},{436,-168},{436,-140},{504,-140},{504,-171.512}},     color={
          255,0,255}));
  connect(greaterEqualThreshold.u, u)
    annotation (Line(points={{54,-90},{-220,-90}}, color={0,0,127}));
  connect(greaterEqualThreshold.y, and1.u1) annotation (Line(points={{77,-90},
          {356,-90},{356,-126},{376,-126}}, color={255,0,255}));
  connect(and3.u1, greaterEqualThreshold.y) annotation (Line(points={{298,
          -178},{180,-178},{180,-90},{77,-90}}, color={255,0,255}));
  connect(and2.u1, greaterEqualThreshold.y) annotation (Line(points={{394,
          -212},{288,-212},{288,-200},{180,-200},{180,-90},{77,-90}}, color={
          255,0,255}));
  connect(not1.u, pre1.y) annotation (Line(points={{34,-286},{12,-286},{12,
          -252},{1,-252}}, color={255,0,255}));
  connect(timer_on.u, pre1.y) annotation (Line(points={{54,-224},{32,-224},{
          32,-252},{1,-252}}, color={255,0,255}));
  connect(pre1.u, greaterEqualThreshold3.y) annotation (Line(points={{-22,
          -252},{-20,-252},{-20,-348},{776,-348},{776,-228},{759,-228}},
        color={255,0,255}));
  connect(timer_off.y, greaterEqualThreshold1.u)
    annotation (Line(points={{95,-286},{126,-286}}, color={0,0,127}));
  connect(and4.u2, greaterEqualThreshold3.y) annotation (Line(points={{358,
          -192},{342,-192},{342,-348},{776,-348},{776,-228},{759,-228}},
        color={255,0,255}));
  connect(and5.u1, not2.y) annotation (Line(points={{458,-262},{456,-262},{
          456,-240},{284,-240},{284,-186},{167,-186}}, color={255,0,255}));
  connect(and5.y, or1.u[4]) annotation (Line(points={{481,-262},{496,-262},{496,
          -200},{488,-200},{488,-140},{504,-140},{504,-168.538}},     color={
          255,0,255}));
  connect(greaterEqualThreshold4.u, timer_on.y) annotation (Line(points={{224,
          -260},{88,-260},{88,-224},{77,-224}}, color={0,0,127}));
  connect(greaterEqualThreshold4.y, and5.u2) annotation (Line(points={{247,-260},
          {444,-260},{444,-270},{458,-270}},       color={255,0,255}));
  connect(triggeredAdd.trigger, or1.y) annotation (Line(points={{476.4,-65.2},
          {476.4,-119.6},{540.55,-119.6},{540.55,-173}}, color={255,0,255}));
  connect(integerConstant.y, triggeredAdd.u)
    annotation (Line(points={{416.8,-58},{471.6,-58}}, color={255,127,0}));
  connect(triggeredAdd.y, Starts) annotation (Line(points={{487.2,-58},{572,
          -58},{572,-44},{820,-44},{820,-30},{844,-30}}, color={255,127,0}));
  connect(HP_on, or1.y) annotation (Line(points={{844,-70},{560,-70},{560,
          -173},{540.55,-173}}, color={255,0,255}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-160,
            -400},{820,60}})),                                   Diagram(
        coordinateSystem(preserveAspectRatio=false, extent={{-160,-400},{820,
            60}})));
end comp_input_transformer;
