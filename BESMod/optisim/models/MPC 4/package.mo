within ;
package MPC



  annotation (uses(
      Modelica(version="4.0.0"),
    PCMgoesHIL(version="1"),
    ModelicaServices(version="4.0.0"),
    AixLib(version="1.3.1"),
 SDF(version="0.4.2"),
    MQTT_Interface(version="0.0.2"),
    IBPSA(version="4.0.0"),
    BESMod(version="0.3.1")),
  version="4",
  conversion(noneFromVersion="3"));
end MPC;
