within MPC;
model Partial_MPC_Model
  extends BESMod.Systems.BaseClasses.PartialBuildingEnergySystem(
    redeclare BESMod.Systems.Electrical.DirectGridConnectionSystem electrical,
    redeclare BESMod.Systems.Demand.Building.TEASERThermalZone building(
        redeclare MPC.DataBase.MPC_Zone_SingleDwelling_130 oneZoneParam),
    redeclare BESMod.Systems.Hydraulical.HydraulicSystem hydraulic(redeclare
        MPC.Submodules.UFHTransferSystemwoPump transfer),
    redeclare BESMod.Systems.Control.NoControl control,
    redeclare BESMod.Systems.Ventilation.NoVentilation ventilation,
    redeclare MPC.Submodules.Demand.DHWHil DHW,
    redeclare BESMod.Systems.UserProfiles.TEASERProfiles userProfiles,
    redeclare
      BESMod.Examples.MyOwnHeatingRodEfficiencyStudy.SimpleStudyOfHeatingRodEfficiency
      parameterStudy,
    redeclare MPC.DataBase.SystemParametersMPC systemParameters);

  annotation (experiment(
      StopTime=1000000,
      Interval=600,
      __Dymola_Algorithm="Dassl"));
end Partial_MPC_Model;
