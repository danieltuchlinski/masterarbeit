within MPC;
model HeatPumpAndHeatingRodMeasPPump
  "HeatPumpAndHeatingRodMeasPPump"
  extends BESMod.Systems.Hydraulical.Interfaces.Generation.HeatPumpAndHeatingRod;
  BESMod.Utilities.KPIs.InputKPICalculator KPIWPRel(
    unit="W",
    integralUnit="J",
    calc_singleOnTime=true,
    calc_integral=true,
    calc_movAve=false)
    annotation (Placement(transformation(extent={{-52,-120},{-40,-98}})));
equation
  connect(pump.P, KPIWPRel.u) annotation (Line(points={{5,-59},{-14,-59},{-14,-124},
          {-60,-124},{-60,-109},{-53.32,-109}}, color={0,0,127}));
end HeatPumpAndHeatingRodMeasPPump;
