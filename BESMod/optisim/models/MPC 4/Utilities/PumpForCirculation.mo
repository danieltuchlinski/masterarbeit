within MPC.Utilities;
model PumpForCirculation

  replaceable package Medium =
    Modelica.Media.Interfaces.PartialMedium "Medium in model"  annotation (
      choicesAllMatching = true);

  parameter Modelica.Fluid.Types.Dynamics energyDynamics=Modelica.Fluid.Types.Dynamics.DynamicFreeInitial
    "Type of energy balance: dynamic (3 initialization options) or steady state"
    annotation(Evaluate=true, Dialog(tab = "Dynamics", group="Equations"));

  parameter Modelica.Units.SI.MassFlowRate m_flow_nominal
    "Nominal mass flow rate" annotation (Dialog(group="Nominal condition"));

  parameter Modelica.Units.SI.PressureDifference dpExtra_nominal
    "Pressure drop at nominal mass flow rate in extra resistance";
  parameter Boolean from_dp=false
    "= true, use m_flow = f(dp) else dp = f(m_flow)";

  replaceable parameter AixLib.Fluid.Movers.Data.Generic per
    constrainedby AixLib.Fluid.Movers.Data.Generic
    "Record with performance data" annotation(Dialog(group="Pump"), choicesAllMatching=true);
  parameter Modelica.Units.SI.Time riseTimePump=10
    "Rise time of the filter (time to reach 99.6 % of the speed)"
    annotation (Dialog(group="Pump"));

  parameter Modelica.Media.Interfaces.Types.AbsolutePressure p_bou=150000
    "Boundary pressure";

  AixLib.Fluid.Movers.SpeedControlled_y pump(
    redeclare package Medium = Medium,
    final p_start=p_start,
    final T_start=T_start,
    inputType=AixLib.Fluid.Types.InputType.Constant,
    addPowerToMedium=false,
    energyDynamics=energyDynamics,
    final per=per,
    use_inputFilter=true)
    annotation (Placement(transformation(extent={{2,64},{22,84}})));
  AixLib.Fluid.Sensors.RelativePressure senRelPre(redeclare package Medium =
        Medium)
    annotation (Placement(transformation(extent={{10,-10},{-10,10}},
        rotation=90,
        origin={40,56})));
  Modelica.Blocks.Interfaces.RealOutput m_flow_toSQL(unit="kg/s")
    annotation (Placement(transformation(extent={{100,-10},{120,10}}), iconTransformation(extent={{100,-10},{120,10}})));
  AixLib.Fluid.Sensors.MassFlowRate senMasFlo(redeclare package Medium = Medium)
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={40,0})));
  AixLib.Fluid.Sources.Boundary_pT bou(
    nPorts=1,
    redeclare package Medium = Medium,
    p=p_bou) annotation (Placement(transformation(extent={{-36,68},{-24,80}})));

  AixLib.Fluid.FixedResistances.PressureDrop resExtra(
    redeclare final package Medium = Medium,
    final m_flow_nominal=m_flow_nominal,
    final from_dp=from_dp,
    final dp_nominal=dpExtra_nominal) if dpExtra_nominal > 0
    annotation (Placement(transformation(extent={{28,-58},{8,-38}})));

  AixLib.Fluid.Interfaces.PassThroughMedium passThroughMediumNoDp(redeclare
      final package Medium =                                                                       Medium) if dpExtra_nominal <= 0 annotation (Placement(transformation(extent={{24,-38},{12,-26}})));
  Modelica.Fluid.Interfaces.FluidPort_a port_a(redeclare package Medium =
        Medium)
    annotation (Placement(transformation(extent={{90,30},{110,50}}), iconTransformation(extent={{90,40},{110,60}})));
  Modelica.Fluid.Interfaces.FluidPort_b port_b(redeclare package Medium =
        Medium)
    annotation (Placement(transformation(extent={{90,70},{110,90}}), iconTransformation(extent={{90,70},{110,90}})));
  parameter Modelica.Media.Interfaces.Types.AbsolutePressure p_start=Medium.p_default "Start value of pressure" annotation (Dialog(tab="Initialization"));
  parameter Modelica.Media.Interfaces.Types.Temperature T_start=Medium.T_default "Start value of temperature" annotation (Dialog(tab="Initialization"));
equation
  connect(senMasFlo.m_flow, m_flow_toSQL)
    annotation (Line(points={{51,0},{110,0}}, color={0,0,127}));
  connect(senMasFlo.port_b, resExtra.port_a)
    annotation (Line(
      points={{40,-10},{40,-48},{28,-48}},
      color={0,127,255},
      pattern=LinePattern.Dash));
  connect(resExtra.port_b, pump.port_a)
    annotation (Line(
      points={{8,-48},{-20,-48},{-20,74},{2,74}},
      color={0,127,255},
      pattern=LinePattern.Dash));
  connect(senMasFlo.port_b, passThroughMediumNoDp.port_a) annotation (Line(
      points={{40,-10},{40,-32},{24,-32}},
      color={0,127,255},
      pattern=LinePattern.Dash));
  connect(passThroughMediumNoDp.port_b, pump.port_a) annotation (Line(
      points={{12,-32},{-16,-32},{-16,74},{2,74}},
      color={0,127,255},
      pattern=LinePattern.Dash));
  connect(senRelPre.port_a, pump.port_b) annotation (Line(points={{40,66},{40,74},{22,74}},
                                   color={0,127,255}));
  connect(pump.port_b, port_b) annotation (Line(points={{22,74},{62,74},{62,80},{100,80}},
                              color={0,127,255}));
  connect(port_a, senMasFlo.port_a)
    annotation (Line(points={{100,40},{40,40},{40,10}}, color={0,127,255}));
  connect(senRelPre.port_b, senMasFlo.port_a) annotation (Line(points={{40,46},{40,10}},
                                      color={0,127,255}));
  connect(pump.port_a, bou.ports[1]) annotation (Line(points={{2,74},{-24,74}},
                                     color={0,127,255}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={0,0,0},
          fillColor={234,234,234},
          fillPattern=FillPattern.Solid),
        Line(
          points={{60,50},{88,50}},
          color={28,108,200},
          thickness=1),
        Line(
          points={{-40,-60},{60,-60}},
          color={28,108,200},
          thickness=1),
        Line(
          points={{60,20},{60,50}},
          color={28,108,200},
          thickness=1),
        Line(
          points={{60,-60},{60,-20}},
          color={28,108,200},
          thickness=1),
        Ellipse(
          extent={{-18,96},{14,64}},
          lineColor={28,108,200},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Line(
          points={{-10,94},{14,80}},
          color={28,108,200}),
        Line(
          points={{14,80},{-10,66}},
          color={28,108,200}),
        Line(
          points={{-40,-60},{-40,80}},
          color={28,108,200},
          thickness=1),
        Line(
          points={{-40,80},{-18,80}},
          color={28,108,200},
          thickness=1),
        Line(
          points={{14,80},{88,80}},
          color={28,108,200},
          thickness=1),
        Polygon(
          points={{64,0},{56,0},{46,20},{74,20},{64,0}},
          lineColor={28,108,200},
          fillColor={28,108,200},
          fillPattern=FillPattern.None,
          lineThickness=1),
        Polygon(
          points={{64,0},{56,0},{46,-20},{74,-20},{64,0}},
          lineColor={28,108,200},
          fillColor={28,108,200},
          fillPattern=FillPattern.None,
          lineThickness=1)}),                                    Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end PumpForCirculation;
