within MPC;
model MA_Kevin_Model_Rad_HiL "HiL model for radiator"
  extends Partial_MPC_Model(
    hydraulic(
      redeclare MPC.Submodules.Distribution.HiLMqttConnection distribution(
          redeclare MPC.Submodules.Distribution.HiL2MPCTopics topicsMQTT,
          mQTTAndConst(startTimeSQL=startTimeSQL)),
      redeclare MPC.Submodules.Generation.HiLControlled generation(dp_nominal={
            0}, mQTTAndConst(startTimeSQL=startTimeSQL)),
      redeclare MPC.Submodules.MonovalentOptihorst control(
        redeclare
          BESMod.Systems.Hydraulical.Control.Components.ThermostaticValveController.ThermostaticValvePIControlled
          thermostaticValveController,
        redeclare
          BESMod.Systems.Hydraulical.Control.RecordsCollection.ThermostaticValveDataDefinition
          thermostaticValveParameters,
        redeclare
          BESMod.Systems.Hydraulical.Control.Components.HeatPumpNSetController.PI_InverterHeatPumpController
          HP_nSet_Controller,
        redeclare
          BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultBivHPControl
          monovalentControlParas(
          dTHysBui=8,
          k=1,
          T_I=1000,
          nMin=1/3),
        safetyControl(
          minRunTime=180,
          minLocTime=180,
          maxRunPerHou=10)),
      redeclare PCMgoesHIL.Systems.Hydraulic.Transfer.RadiatorTransferSystemHIL
        transfer(
        redeclare
          BESMod.Systems.Hydraulical.Transfer.RecordsCollection.RadiatorTransferData
          radParameters,
        redeclare
          BESMod.Systems.RecordsCollection.TemperatureSensors.DefaultSensor
          sensorData,
        redeclare
          BESMod.Systems.Hydraulical.Transfer.RecordsCollection.SteelRadiatorStandardPressureLossData
          transferDataBaseDefinition,
        dpFullOpen_nominal=99999.99999999999*(73853.65852*0.8))),
    systemParameters(
      QBui_flow_nominal={6596.211},
      TOda_nominal=261.15,
      THydSup_nominal={328.15},
      use_ventilation=false,                   use_dhw=false,
      use_elecHeating=false),
    DHW(
      mDHW_flow_nominal=0,
      VDHWDay=0,
      tCrit=0,
      QCrit=0));
      parameter Modelica.Units.SI.Time startTimeSQL(displayUnit="h")=31536000;

  Modelica.Blocks.Sources.RealExpression UpperDB(y=hydraulic.control.HP_nSet_Controller.T_Set
         + hydraulic.control.BoilerOnOffBuf.bandwidth/2)
    annotation (Placement(transformation(extent={{-188,172},{-168,192}})));
  Modelica.Blocks.Sources.RealExpression LowerDB(y=hydraulic.control.HP_nSet_Controller.T_Set
         - hydraulic.control.BoilerOnOffBuf.bandwidth/2)
    annotation (Placement(transformation(extent={{-158,154},{-138,174}})));
  Modelica.Blocks.Sources.RealExpression Temp(y=1.15)
    annotation (Placement(transformation(extent={{-94,172},{-74,192}})));
    MQTT_Interface.Models.MQTTAndConst mQTTAndConst(
    nWriteValues=4,
    redeclare MQTT_Interface.MQTTConnectionPara.TickStackOpenstack mqttConParam,
    startTimeSQL=startTimeSQL,
    samRate=1,
    namesToWrite={"hil2/Sim/Sim_Time","hil2/Sim/T_Zone","hil2/Sim/building.weaBus.TDryBul",
        "hil2/Sim/building.weaBus.HDirNor"},
    addWriteValues={0,-273.15,-273.15,0})
    annotation (Placement(transformation(extent={{324,-170},{386,-108}})));

  Modelica.Blocks.Sources.ContinuousClock Clock(offset=0, startTime=0)
    annotation (Placement(transformation(extent={{250,-110},{270,-90}})));
  AixLib.BoundaryConditions.WeatherData.Bus weaBus annotation (Placement(
        transformation(extent={{262,-172},{302,-132}}),iconTransformation(
          extent={{182,-72},{202,-52}})));
equation
  connect(weaDat.weaBus, weaBus) annotation (Line(
      points={{-220,70},{-208,70},{-208,-132},{282,-132},{282,-152}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
        connect(weaBus.TDryBul, mQTTAndConst.writeValues[3]) annotation (Line(
      points={{282,-152},{316,-152},{316,-138.225},{320.9,-138.225}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(building.buiMeaBus.TZoneMea[1], mQTTAndConst.writeValues[2])
  annotation (experiment(
      StopTime=31536000,
      Interval=600,
      __Dymola_Algorithm="Dassl"));
  connect(weaBus.HDirNor, mQTTAndConst.writeValues[4]) annotation (Line(
      points={{282,-152},{316,-152},{316,-136.675},{320.9,-136.675}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));

  connect(Clock.y, mQTTAndConst.writeValues[1]) annotation (Line(points={{271,
          -100},{320.9,-100},{320.9,-141.325}}, color={0,0,127}));
  annotation (experiment(
      StopTime=30,
      Interval=1,
      __Dymola_Algorithm="Dassl"));
end MA_Kevin_Model_Rad_HiL;
