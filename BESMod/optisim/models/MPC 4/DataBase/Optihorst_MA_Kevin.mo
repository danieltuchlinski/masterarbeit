within MPC.DataBase;
record Optihorst_MA_Kevin "Calibration Data"
  extends AixLib.DataBase.HeatPump.HeatPumpBaseDataDefinition(
    tableQdot_con=[0,-7,0,12; 35,9420,11100,13600; 45,8920,10650,13250; 55,8805,
        10490,13050],
    tableP_ele=[0,-7,0,12; 35,4225,4305,4485; 44.5,5032,5110,5315; 54,5948,6200,
        6280],
   mFlow_conNom=0.67,
   mFlow_evaNom=1,
   tableUppBou=[-10,55; 25,55]);
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end Optihorst_MA_Kevin;
