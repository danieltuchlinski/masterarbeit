within MPC.DataBase;
record Optihorst_Kevin_81rps "Optihorst Kevin 81rps"
  extends AixLib.DataBase.HeatPump.HeatPumpBaseDataDefinition(
   tableQdot_con=[0,-7, 0, 12; 35,9390, 11040, 13600; 45,8785, 10670, 13250; 55, 9027, 10490, 13000],
   tableP_ele=[0,-7, 0, 12; 35,4215, 4295, 4490; 45,5025, 5115, 5350; 55, 5938, 6246, 6260],
   mFlow_conNom=0.28,
   mFlow_evaNom=1,
   tableUppBou=[-10,55; 25,55]);

end Optihorst_Kevin_81rps;
