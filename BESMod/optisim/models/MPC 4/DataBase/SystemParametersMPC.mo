within MPC.DataBase;
record SystemParametersMPC
  extends BESMod.Systems.RecordsCollection.SystemParametersBaseDataDefinition(
    filNamWea=Modelica.Utilities.Files.loadResource(
        "modelica://BESMod/Resources/TRY2015_522361130393_Jahr_City_Potsdam.mos"),
    THydSup_nominal={313.15},
    QBui_flow_nominal={6596.211},
    use_ventilation=true,
    TOda_nominal=261.15);

end SystemParametersMPC;
