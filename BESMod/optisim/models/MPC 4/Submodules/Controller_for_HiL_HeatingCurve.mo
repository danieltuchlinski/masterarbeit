within MPC.Submodules;
model Controller_for_HiL_HeatingCurve
  "Controller_for_HiL_HeatingCurve"
  extends BESMod.Systems.Hydraulical.Control.BaseClasses.PartialControl;

  Modelica.Blocks.Routing.RealPassThrough realPassThrough_T_Amb1
    "Only used to make warning disappear, has no effect on model veloccity"
    annotation (Placement(transformation(extent={{-228,-58},{-206,-36}})));

  parameter
    BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultBivHPControl bivalentControlData(
    final TOda_nominal=generationParameters.TOda_nominal,
    TSup_nominal=generationParameters.TSup_nominal[1],
    TSetRoomConst=sum(transferParameters.TDem_nominal)/transferParameters.nParallelDem,
    nMin=0)
    annotation (choicesAllMatching=true, Placement(transformation(extent={{
            -106,-26},{-84,-4}})));
  replaceable
    BESMod.Systems.Hydraulical.Control.Components.ThermostaticValveController.BaseClasses.PartialThermostaticValveController
    thermostaticValveController constrainedby
    BESMod.Systems.Hydraulical.Control.Components.ThermostaticValveController.BaseClasses.PartialThermostaticValveController(
      final nZones=transferParameters.nParallelDem, final leakageOpening=
        thermostaticValveParameters.leakageOpening) annotation (
      choicesAllMatching=true, Placement(transformation(extent={{122,-58},{148,
            -28}})));
    MQTT_Interface.Models.MQTTAndConst mQTTAndConst(
    nWriteValues=1,
    nReadValues=1,
    redeclare MQTT_Interface.MQTTConnectionPara.TickStackOpenstack mqttConParam,
    startTimeSQL=startTimeSQL,
    samRate=1,
    namesToRead={"hil2/Sim/T_Zone_Set"},
    namesToWrite={"dummy"})
    annotation (Placement(transformation(extent={{-20,26},{42,88}})));

  replaceable parameter
    BESMod.Systems.Hydraulical.Control.RecordsCollection.ThermostaticValveDataDefinition
    thermostaticValveParameters annotation (choicesAllMatching=true, Placement(
        transformation(extent={{198,-80},{218,-60}})));
  Controller_Presim controller_Presim
    annotation (Placement(transformation(extent={{-224,62},{-172,94}})));
  Modelica.Blocks.Logical.Switch SQLorNot
    annotation (Placement(transformation(extent={{76,-62},{96,-42}})));
  Modelica.Blocks.Sources.Constant dummy(k=1)
    annotation (Placement(transformation(extent={{-82,46},{-56,72}})));
  HeatingCurve heatingCurve
    annotation (Placement(transformation(extent={{-134,0},{-114,20}})));
equation

  connect(realPassThrough_T_Amb1.y, sigBusGen.hp_bus.TOdaMea) annotation (Line(
        points={{-204.9,-47},{-200,-47},{-200,-68},{-152,-68},{-152,-99}},
        color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(realPassThrough_T_Amb1.u, weaBus.TDryBul) annotation (
      Line(points={{-230.2,-47},{-236,-47},{-236,-26},{-210,-26},{-210,2},{
          -237,2}},                                                       color=
         {0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(thermostaticValveController.TZoneMea, buiMeaBus.TZoneMea) annotation (
     Line(points={{119.4,-34},{65,-34},{65,103}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-3,6},{-3,6}},
      horizontalAlignment=TextAlignment.Right));
  connect(thermostaticValveController.opening, sigBusTra.opening) annotation (
      Line(points={{150.6,-43},{174,-43},{174,-100}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-3,-6},{-3,-6}},
      horizontalAlignment=TextAlignment.Right));
  connect(mQTTAndConst.activeSQL, SQLorNot.u2) annotation (Line(points={{45.1,
          47.7},{45.1,46},{54,46},{54,-52},{74,-52}}, color={255,0,255}));
  connect(SQLorNot.u1, mQTTAndConst.readValues[1]) annotation (Line(points={{74,
          -44},{56,-44},{56,57},{45.1,57}}, color={0,0,127}));
  connect(SQLorNot.y, thermostaticValveController.TZoneSet[1])
    annotation (Line(points={{97,-52},{119.4,-52}}, color={0,0,127}));
  connect(SQLorNot.u3, controller_Presim.T_Zone_Set) annotation (Line(points={{74,-60},
          {4,-60},{4,18},{-26,18},{-26,42},{-120,42},{-120,66},{-165.855,66},{
          -165.855,67.76}},               color={0,0,127}));
  connect(dummy.y, mQTTAndConst.writeValues[1]) annotation (Line(points={{-54.7,
          59},{-54.7,60},{-32,60},{-32,57},{-23.1,57}}, color={0,0,127}));
  connect(heatingCurve.TSetRoom, controller_Presim.T_Zone_Set) annotation (Line(
        points={{-124,22},{-124,67.76},{-165.855,67.76}}, color={0,0,127}));
  connect(heatingCurve.TOda, weaBus.TDryBul) annotation (Line(points={{-136,10},
          {-210,10},{-210,2},{-237,2}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(heatingCurve.TSet, sigBusDistr.TFlowHeaCur) annotation (Line(points={
          {-113,10},{-6,10},{-6,-72},{-2,-72},{-2,-100},{1,-100}}, color={0,0,
          127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  annotation (Diagram(coordinateSystem(extent={{-240,-140},{240,100}})), Icon(
        coordinateSystem(extent={{-240,-140},{240,100}})));
end Controller_for_HiL_HeatingCurve;
