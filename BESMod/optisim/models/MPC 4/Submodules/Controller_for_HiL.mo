within MPC.Submodules;
model Controller_for_HiL
  "Controller_for_HiL"
  extends BESMod.Systems.Hydraulical.Control.BaseClasses.PartialControl;

  Modelica.Blocks.Routing.RealPassThrough realPassThrough_T_Amb1
    "Only used to make warning disappear, has no effect on model veloccity"
    annotation (Placement(transformation(extent={{-228,-58},{-206,-36}})));

  parameter
    BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultBivHPControl bivalentControlData(
    final TOda_nominal=generationParameters.TOda_nominal,
    TSup_nominal=generationParameters.TSup_nominal[1],
    TSetRoomConst=sum(transferParameters.TDem_nominal)/transferParameters.nParallelDem,
    nMin=0)
    annotation (choicesAllMatching=true, Placement(transformation(extent={{
            -106,-26},{-84,-4}})));
  replaceable
    BESMod.Systems.Hydraulical.Control.Components.ThermostaticValveController.BaseClasses.PartialThermostaticValveController
    thermostaticValveController constrainedby
    BESMod.Systems.Hydraulical.Control.Components.ThermostaticValveController.BaseClasses.PartialThermostaticValveController(
      final nZones=transferParameters.nParallelDem, final leakageOpening=
        thermostaticValveParameters.leakageOpening) annotation (
      choicesAllMatching=true, Placement(transformation(extent={{66,-32},{92,-2}})));

  replaceable parameter
    BESMod.Systems.Hydraulical.Control.RecordsCollection.ThermostaticValveDataDefinition
    thermostaticValveParameters annotation (choicesAllMatching=true, Placement(
        transformation(extent={{198,-80},{218,-60}})));
  Controller_Presim controller_Presim
    annotation (Placement(transformation(extent={{-224,62},{-172,94}})));
  Modelica.Blocks.Logical.Switch SQLorNot
    annotation (Placement(transformation(extent={{128,-66},{148,-46}})));
  Modelica.Blocks.Sources.Constant Valve_open(k=1)
    annotation (Placement(transformation(extent={{-38,-62},{-12,-36}})));
  HeatingCurve heatingCurve
    annotation (Placement(transformation(extent={{-138,2},{-118,22}})));
equation

  connect(realPassThrough_T_Amb1.y, sigBusGen.hp_bus.TOdaMea) annotation (Line(
        points={{-204.9,-47},{-200,-47},{-200,-68},{-152,-68},{-152,-99}},
        color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(realPassThrough_T_Amb1.u, weaBus.TDryBul) annotation (
      Line(points={{-230.2,-47},{-236,-47},{-236,-26},{-210,-26},{-210,2},{
          -237,2}},                                                       color=
         {0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(thermostaticValveController.TZoneMea, buiMeaBus.TZoneMea) annotation (
     Line(points={{63.4,-8},{56,-8},{56,78},{65,78},{65,103}},
                                                  color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-3,6},{-3,6}},
      horizontalAlignment=TextAlignment.Right));
  connect(thermostaticValveController.TZoneSet[1], controller_Presim.T_Zone_Set)
    annotation (Line(points={{63.4,-26},{2,-26},{2,62},{-165.855,62},{-165.855,
          67.76}},
        color={0,0,127}));
  connect(SQLorNot.y, sigBusTra.opening[1]) annotation (Line(points={{149,-56},{
          174,-56},{174,-100}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(SQLorNot.u2, sigBusDistr.bSQL) annotation (Line(points={{126,-56},{30,
          -56},{30,-100},{1,-100}}, color={255,0,255}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(SQLorNot.u1, Valve_open.y) annotation (Line(points={{126,-48},{-4,-48},
          {-4,-49},{-10.7,-49}}, color={0,0,127}));
  connect(thermostaticValveController.opening[1], SQLorNot.u3) annotation (Line(
        points={{94.6,-17},{100,-17},{100,-64},{126,-64}}, color={0,0,127}));
  connect(heatingCurve.TSetRoom, controller_Presim.T_Zone_Set) annotation (Line(
        points={{-128,24},{-128,62},{-165.855,62},{-165.855,67.76}}, color={0,0,
          127}));
  connect(heatingCurve.TOda, weaBus.TDryBul) annotation (Line(points={{-140,12},
          {-210,12},{-210,2},{-237,2}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(heatingCurve.TSet, sigBusDistr.TFlowHeaCur) annotation (Line(points={
          {-117,12},{-44,12},{-44,-100},{1,-100}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  annotation (Diagram(coordinateSystem(extent={{-240,-140},{240,100}})), Icon(
        coordinateSystem(extent={{-240,-140},{240,100}})));
end Controller_for_HiL;
