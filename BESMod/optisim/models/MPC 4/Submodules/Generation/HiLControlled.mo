within MPC.Submodules.Generation;
model HiLControlled
  extends BESMod.Systems.Hydraulical.Interfaces.Generation.BaseClasses.PartialGeneration(
      dTTra_nominal={5}, nParallelDem=1);
  MQTT_Interface.Models.MQTTAndConst mQTTAndConst(
    nWriteValues=1,
    nReadValues=2,
    redeclare MQTT_Interface.MQTTConnectionPara.TickStackOpenstack mqttConParam,
    namesToRead={"detHP/IndoorUnit.T_FlowOutlet_IDU",
        "detHP/StateMachine.rps_Comp"},
    standardReadValues={20,0},
    namesToWrite={"optihorst/n_set"},
    timeConstsDampReadValues={5,5},
    timeConstsDampWriteValues={5},
    kReadValues={1,1},
    addReadValues={273.15,0},
    maxReadValues={Modelica.Constants.inf,Modelica.Constants.inf},
    minReadValues={-1*Modelica.Constants.inf,-1*Modelica.Constants.inf},
    kWriteValues={90})
    annotation (Placement(transformation(extent={{-28,-32},{34,30}})));

  BESMod.Utilities.Electrical.ZeroLoad zeroLoad
    annotation (Placement(transformation(extent={{32,-112},{52,-92}})));
  Modelica.Blocks.Logical.Greater checkHpOn
    annotation (Placement(transformation(extent={{60,-32},{80,-12}})));
  Modelica.Blocks.Sources.Constant const(k=15)
    annotation (Placement(transformation(extent={{24,-60},{44,-40}})));
  Modelica.Blocks.Routing.RealPassThrough realPassThrough
    annotation (Placement(transformation(extent={{-68,76},{-48,96}})));
equation
  connect(portGen_out[1], portGen_in[1]) annotation (Line(points={{100,80},{86,
          80},{86,82},{72,82},{72,0},{100,0},{100,-2}}, color={0,127,255}));
  connect(sigBusGen.hp_bus.nSet, mQTTAndConst.writeValues[1]) annotation (Line(
      points={{2,98},{4,98},{4,70},{-60,70},{-60,-1},{-31.1,-1}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(zeroLoad.internalElectricalPin, internalElectricalPin) annotation (
      Line(
      points={{52,-102},{58,-102},{58,-86},{72,-86},{72,-100}},
      color={0,0,0},
      thickness=1));
  connect(mQTTAndConst.readValues[1], sigBusGen.hp_bus.TConOutMea) annotation (
      Line(points={{37.1,-2.55},{58,-2.55},{58,98},{2,98}},       color={0,0,
          127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(mQTTAndConst.readValues[2], checkHpOn.u1) annotation (Line(points={{
          37.1,0.55},{52,0.55},{52,-22},{58,-22}}, color={0,0,127}));
  connect(const.y, checkHpOn.u2) annotation (Line(points={{45,-50},{56,-50},{56,
          -30},{58,-30}}, color={0,0,127}));
  connect(checkHpOn.y, sigBusGen.hp_bus.onOffMea) annotation (Line(points={{81,
          -22},{62,-22},{62,76},{2,76},{2,98}}, color={255,0,255}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(weaBus.TDryBul, realPassThrough.u) annotation (Line(
      points={{-101,80},{-76,80},{-76,86},{-70,86}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(realPassThrough.y, sigBusGen.hp_bus.TEvaInMea) annotation (Line(
        points={{-47,86},{-22,86},{-22,74},{2,74},{2,98}}, color={0,0,127}),
      Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
end HiLControlled;
