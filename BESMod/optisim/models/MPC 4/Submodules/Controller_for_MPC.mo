within MPC.Submodules;
model Controller_for_MPC
  extends MPC.Submodules.SystemWithControllableThermostaticValveControl;

  replaceable BESMod.Systems.Hydraulical.Control.RecordsCollection.HeatPumpSafetyControl
    safetyControl
    annotation (choicesAllMatching=true,Placement(transformation(extent={{206,32},
            {226,52}})));
  BESMod.Systems.Hydraulical.Control.Components.HeatingCurve
    heatingCurve(
    GraHeaCurve=bivalentControlData.gradientHeatCurve,
    THeaThres=bivalentControlData.TSetRoomConst,
    dTOffSet_HC=bivalentControlData.dTOffSetHeatCurve)
    annotation (Placement(transformation(extent={{-152,36},{-130,58}})));
  Modelica.Blocks.Sources.Constant const_dT_loading1(k=distributionParameters.dTTra_nominal[1])
                                                          annotation (Placement(
        transformation(
        extent={{4,-4},{-4,4}},
        rotation=180,
        origin={14,58})));

  Modelica.Blocks.Sources.BooleanConstant hp_mode(final k=true) annotation (
      Placement(transformation(
        extent={{-7,-7},{7,7}},
        rotation=0,
        origin={177,83})));
  Modelica.Blocks.Sources.Constant hp_iceFac(final k=1) annotation (Placement(
        transformation(
        extent={{-7,-7},{7,7}},
        rotation=0,
        origin={-225,-175})));

  Modelica.Blocks.Routing.RealPassThrough realPassThrough_T_Amb1
    "Only used to make warning disappear, has no effect on model veloccity"
    annotation (Placement(transformation(extent={{-202,38},{-180,60}})));

  Modelica.Blocks.Math.Add add_dT_LoadingBuf
    annotation (Placement(transformation(extent={{38,54},{48,64}})));

  parameter
    BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultBivHPControl bivalentControlData(
    final TOda_nominal=generationParameters.TOda_nominal,
    TSup_nominal=generationParameters.TSup_nominal[1],
    TSetRoomConst=sum(transferParameters.TDem_nominal)/transferParameters.nParallelDem,
    nMin=0)
    annotation (choicesAllMatching=true, Placement(transformation(extent={{-230,
            -66},{-208,-44}})));
  Modelica.Blocks.Sources.BooleanExpression HeatingRod(y=true)
    annotation (Placement(transformation(extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-152,-220})));
  Modelica.Blocks.Math.BooleanToReal booleanToReal_HR annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=90,
        origin={-152,-196})));
  Model_inputs model_inputs
    annotation (Placement(transformation(extent={{-40,26},{-16,54}})));
  PI_Control_for_MPC pI_Control_for_MPC
    annotation (Placement(transformation(extent={{80,64},{112,92}})));
  Modelica.Blocks.Sources.Constant const_dT_loading2(k=273.15 + 25)
                                                          annotation (Placement(
        transformation(
        extent={{4,-4},{-4,4}},
        rotation=180,
        origin={-164,74})));
  Modelica.Blocks.Math.BooleanToReal booleanToReal_u_Pump annotation (
      Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=90,
        origin={-134,-196})));
  Modelica.Blocks.Sources.BooleanExpression FMU_Input(y=true)
    annotation (Placement(transformation(extent={{-88,42},{-68,62}})));
  Modelica.Blocks.Sources.RealExpression valve_set(y=1)
    annotation (Placement(transformation(extent={{52,-186},{72,-166}})));
  Modelica.Blocks.Sources.BooleanExpression Pump(y=true)
    annotation (Placement(transformation(extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-134,-220})));
  comp_input_transformer comp_input_transformer1
    annotation (Placement(transformation(extent={{92,-30},{146,2}})));
  Modelica.Blocks.Logical.Switch Valve_Switch_fmu2
    annotation (Placement(transformation(extent={{-64,-100},{-44,-80}})));
  Modelica.Blocks.Sources.RealExpression HP_off(y=0)
    annotation (Placement(transformation(extent={{-114,-92},{-94,-72}})));
  Modelica.Blocks.Logical.Hysteresis hysteresis(uLow=59.9 + 273.15, uHigh=60.1
         + 273.15)
    annotation (Placement(transformation(extent={{-112,-102},{-100,-90}})));
equation

  connect(hp_iceFac.y, sigBusGen.hp_bus.iceFacMea) annotation (Line(
        points={{-217.3,-175},{-217.3,-176},{-152,-176},{-152,-99}},
                      color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));

  connect(realPassThrough_T_Amb1.y, sigBusGen.hp_bus.TOdaMea) annotation (Line(
        points={{-178.9,49},{-178.9,48},{-160,48},{-160,-68},{-152,-68},{-152,
          -99}},
        color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(realPassThrough_T_Amb1.u, weaBus.TDryBul) annotation (
      Line(points={{-204.2,49},{-204.2,48},{-218,48},{-218,2},{-237,2}},  color=
         {0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(realPassThrough_T_Amb1.y, heatingCurve.TOda) annotation (Line(
        points={{-178.9,49},{-178.9,48},{-160,48},{-160,47},{-154.2,47}},
                       color={0,0,127}));
  connect(const_dT_loading1.y, add_dT_LoadingBuf.u2) annotation (Line(points={{
          18.4,58},{26,58},{26,56},{37,56}}, color={0,0,127}));
  connect(HeatingRod.y, booleanToReal_HR.u) annotation (Line(points={{-152,-209},
          {-152,-203.2}},                   color={255,0,255}));
  connect(booleanToReal_HR.y, sigBusGen.hr_on) annotation (Line(points={{-152,
          -189.4},{-152,-99}},                     color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(hp_mode.y, sigBusGen.hp_bus.modeSet) annotation (Line(points={{184.7,
          83},{202,83},{202,-44},{110,-44},{110,-66},{42,-66},{42,-64},{-152,
          -64},{-152,-99}},
                 color={255,0,255}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(heatingCurve.TSet, model_inputs.T_set_HC) annotation (Line(points={{-128.9,
          47},{-128.9,14},{-42.2286,14},{-42.2286,27.3176}},    color={0,0,
          127}));
  connect(model_inputs.T_VL_set, add_dT_LoadingBuf.u1) annotation (Line(
        points={{-12.5714,32.9176},{-4,32.9176},{-4,66},{28,66},{28,62},{37,62}},
        color={0,0,127}));
  connect(add_dT_LoadingBuf.y, pI_Control_for_MPC.T_Set) annotation (Line(
        points={{48.5,59},{54,59},{54,60},{76.5333,60},{76.5333,85.28}},
        color={0,0,127}));
  connect(pI_Control_for_MPC.n_Set, model_inputs.u_comp_PID) annotation (Line(
        points={{117.6,82.48},{116,82.48},{116,72},{114,72},{114,66},{116,66},{
          116,52},{74,52},{74,38},{0,38},{0,20},{-34,20},{-34,18},{-46,18},{-46,
          30},{-42.2286,30},{-42.2286,31.9294}},      color={0,0,127}));
  connect(pI_Control_for_MPC.T_Meas, sigBusGen.hp_bus.TConOutMea) annotation (
     Line(points={{92.5333,60.08},{92.5333,20},{2,20},{2,-62},{-152,-62},{-152,
          -99}},      color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(model_inputs.b_valve_pi_out, Valve_Switch.u2) annotation (Line(
        points={{-12.7429,48.7294},{-12.7429,-50},{44,-50},{44,-128},{82,-128},
          {82,-160},{174,-160}}, color={255,0,255}));
  connect(heatingCurve.TSet, sigBusDistr.TFlowHeaCur) annotation (Line(points={{-128.9,
          47},{-128.9,14},{-130,14},{-130,-66},{1,-66},{1,-100}},
                     color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(const_dT_loading2.y, heatingCurve.TSetRoom) annotation (Line(points={{-159.6,
          74},{-141,74},{-141,60.2}},         color={0,0,127}));
  connect(booleanToReal_u_Pump.y, sigBusGen.uPump) annotation (Line(points={{-134,
          -189.4},{-134,-100},{-152,-100},{-152,-99}},
        color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(FMU_Input.y, model_inputs.b_FMU_input) annotation (Line(points={{-67,52},
          {-54.5286,52},{-54.5286,52.5176},{-42.0571,52.5176}}, color={255,0,
          255}));
  connect(Valve_Switch_fmu.u2, model_inputs.b_FMU_input) annotation (Line(
        points={{106,-148},{30,-148},{30,-136},{-48,-136},{-48,52.5176},{
          -42.0571,52.5176}}, color={255,0,255}));
  connect(valve_set.y, Valve_Switch_fmu.u3) annotation (Line(points={{73,-176},
          {84,-176},{84,-156},{106,-156}}, color={0,0,127}));
  connect(booleanToReal_u_Pump.u, Pump.y) annotation (Line(points={{-134,-203.2},
          {-134,-209}},                 color={255,0,255}));
  connect(Valve_Switch_fmu1.u2, FMU_Input.y) annotation (Line(points={{80,-220},
          {-8,-220},{-8,-38},{-67,-38},{-67,52}},  color={255,0,255}));
  connect(comp_input_transformer1.HP_on, sigBusTra.HP_on) annotation (Line(
        points={{147.322,-7.04348},{147.322,-58},{174,-58},{174,-100},{174,-100}},
        color={255,0,255}), Text(
      string="%second",
      index=1,
      extent={{-3,-6},{-3,-6}},
      horizontalAlignment=TextAlignment.Right));
  connect(Valve_Switch_fmu2.u3, comp_input_transformer1.y) annotation (Line(
        points={{-66,-98},{-72,-98},{-72,-58},{98,-58},{98,-42},{152,-42},{152,
          -11.7739},{147.873,-11.7739}}, color={0,0,127}));
  connect(Valve_Switch_fmu2.u1, HP_off.y) annotation (Line(points={{-66,-82},{
          -93,-82}},                                    color={0,0,127}));
  connect(hysteresis.y, Valve_Switch_fmu2.u2) annotation (Line(points={{-99.4,
          -96},{-94,-96},{-94,-90},{-66,-90}},                  color={255,0,
          255}));
  connect(hysteresis.u, sigBusGen.hp_bus.TConOutMea) annotation (Line(points={{-113.2,
          -96},{-152,-96},{-152,-99}},                             color={0,0,
          127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(comp_input_transformer1.u, model_inputs.n_Set) annotation (Line(
        points={{88.6939,-8.43478},{82,-8.43478},{82,22},{-6,22},{-6,39.6706},{
          -12.4,39.6706}}, color={0,0,127}));
  connect(pI_Control_for_MPC.HP_on, comp_input_transformer1.HP_on) annotation (
      Line(points={{76.2667,67.64},{76.2667,-36},{147.322,-36},{147.322,
          -7.04348}}, color={255,0,255}));
  connect(Valve_Switch_fmu2.y, sigBusGen.hp_bus.nSet) annotation (Line(points={{-43,-90},
          {-43,-102},{-78,-102},{-78,-68},{-152,-68},{-152,-99}},
                                                       color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  annotation (Diagram(graphics={
        Text(
          extent={{-234,94},{-140,128}},
          lineColor={238,46,47},
          lineThickness=1,
          textString="DHW Control"),
        Text(
          extent={{4,122},{108,102}},
          lineColor={28,108,200},
          lineThickness=1,
          textString="Heat Pump Control"),
        Text(
          extent={{138,122},{242,102}},
          lineColor={28,108,200},
          lineThickness=1,
          textString="Heat Pump Safety")}));
end Controller_for_MPC;
