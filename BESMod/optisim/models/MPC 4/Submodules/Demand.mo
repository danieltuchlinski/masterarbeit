within MPC.Submodules;
package Demand
extends BESMod.Utilities.Icons.SystemIcon;
  model HiLDHWNC

    PCMgoesHIL.Utilities.DHW.NC_obsolet.DHWCalcNC dHWCalc(
      final TColdWater=TColdWater,
      final cp=cp,
      final fileName_scheduleDHW=fileName_scheduleDHW,
      final tableOutName=tableOutName,
      final preAnnTime_m_flowIdeal=preAnnTime_m_flowIdeal)
      annotation (Placement(transformation(extent={{-28,-18},{32,56}})));
    PCMgoesHIL.Utilities.DHW.CalcDHWmFlow calcDHWmFlow annotation (Placement(
          transformation(
          extent={{-17,17},{17,-17}},
          rotation=180,
          origin={-27,-49})));
    Modelica.Blocks.Sources.Constant conDHWHotAssumed(k=273.15 + 55) annotation (
        Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=180,
          origin={48,-52})));
    Modelica.Blocks.Interfaces.RealOutput V_flow_preCirDHW
      "mFlow that should be set for DHW when there is a demand" annotation (
        Placement(transformation(extent={{100,-40},{120,-20}}),
          iconTransformation(extent={{100,-40},{120,-20}})));
    Modelica.Blocks.Interfaces.RealOutput v_flow_setDHW
      "Mass flow rate that has to be provided by the DHW tank" annotation (
        Placement(transformation(extent={{100,24},{120,44}}), iconTransformation(
            extent={{100,24},{120,44}})));
    Modelica.Blocks.Interfaces.RealInput V_flow_mesDHW
      "Mass flow coming from the test rig as a measured input" annotation (
        Placement(transformation(extent={{-122,54},{-82,94}}), iconTransformation(
            extent={{-122,54},{-82,94}})));
    Modelica.Blocks.Interfaces.RealInput T_hotMesDHW
      "Temperature (hot water) coming from the DHW tank" annotation (Placement(
          transformation(extent={{-122,-6},{-82,34}}), iconTransformation(extent=
              {{-122,-6},{-82,34}})));
    Modelica.Blocks.Interfaces.RealInput T_coldMesDHW
      "Temperature of cold water coming from the test rig" annotation (Placement(
          transformation(extent={{-122,-68},{-82,-28}}), iconTransformation(
            extent={{-122,-68},{-82,-28}})));
    Modelica.Blocks.Interfaces.RealOutput T_setDHW
      "Cold water temperature coming from district water"
      annotation (Placement(transformation(extent={{100,-6},{120,14}})));
    parameter String fileName_scheduleDHW=Modelica.Utilities.Files.loadResource(
        "modelica://DynEvaluationHP/SimulationInputData/Tables/Profiles/dhw_day.nc");
    parameter String tableOutName[5]={"QTap","mFlow","Tm","Tp","tapNo"}
      "Names for the variables in the dhw *.nc file";
    parameter Modelica.Units.SI.Temperature TColdWater=283.15
      "Constant temperature assumed for city water";
    parameter Modelica.Units.SI.SpecificHeatCapacity cp=4180
      "Specific heat capacity of liquid water";
    parameter Modelica.Units.SI.Time preAnnTime_m_flowIdeal=240
      "Time period prior current simulation time";
  equation
    connect(calcDHWmFlow.mFlowFromTable, dHWCalc.DHWTableOutputs_preDelteTime[2])
      annotation (Line(points={{-10,-35.4},{14,-35.4},{14,-21.7}}, color={0,0,127}));
    connect(calcDHWmFlow.Tp, dHWCalc.DHWTableOutputs_preDelteTime[4]) annotation (
       Line(points={{-10,-45.6},{14,-45.6},{14,-21.7}}, color={0,0,127}));
    connect(conDHWHotAssumed.y, calcDHWmFlow.TTankDHWHotMes) annotation (Line(
          points={{37,-52},{0,-52},{0,-52.4},{-9.66,-52.4}}, color={0,0,127}));
    connect(dHWCalc.TColdFromParam, calcDHWmFlow.TCold) annotation (Line(points={{
            35,4.2},{54,4.2},{54,-26},{26,-26},{26,-63.28},{-10,-63.28}}, color={0,
            0,127}));
    connect(calcDHWmFlow.mFlowCalcedDHWSet, V_flow_preCirDHW) annotation (Line(
          points={{-44,-49},{-48,-49},{-48,-50},{-56,-50},{-56,-30},{110,-30}},
          color={0,0,127}));
    connect(dHWCalc.mFlowDHWTank, v_flow_setDHW)
      annotation (Line(points={{35,33.8},{110,33.8},{110,34}}, color={0,0,127}));
    connect(dHWCalc.mFlowMes, V_flow_mesDHW) annotation (Line(points={{-28,42.68},
            {-102,42.68},{-102,74}}, color={0,0,127}));
    connect(dHWCalc.TDHWMes, T_hotMesDHW)
      annotation (Line(points={{-28,19},{-102,19},{-102,14}}, color={0,0,127}));
    connect(dHWCalc.TColdMes, T_coldMesDHW) annotation (Line(points={{-28,-5.42},
            {-72,-5.42},{-72,-48},{-102,-48}}, color={0,0,127}));
    connect(dHWCalc.TColdFromParam, T_setDHW) annotation (Line(points={{35,4.2},{
            67.5,4.2},{67.5,4},{110,4}}, color={0,0,127}));
    annotation (Icon(graphics={
        Ellipse(
          extent={{45,2},{64,-18}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{33,-34},{52,-54}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{65,-24},{84,-44}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{55,-62},{74,-82}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-39,27},{39,-27}},
            lineColor={95,95,95},
            fillPattern=FillPattern.HorizontalCylinder,
            fillColor={175,175,175},
            origin={59,45},
            rotation=-90),
          Rectangle(
            extent={{-82,84},{32,44}},
            lineColor={95,95,95},
            fillPattern=FillPattern.HorizontalCylinder,
            fillColor={175,175,175})}));
  end HiLDHWNC;

  model HiLDHWTab

    Utilities.DHW.DHWCalcTab            dHWCalcTab(
      final TColdWater=TColdWater,
      final cp=cp,
      final tableOutName=tableOutName,
      final preAnnTime_m_flowIdeal=preAnnTime_m_flowIdeal,
      DHWProfile=DHWProfile)
      annotation (Placement(transformation(extent={{-28,-18},{32,56}})));
    Utilities.DHW.CalcDHWmFlow            calcDHWmFlow annotation (Placement(
          transformation(
          extent={{-17,17},{17,-17}},
          rotation=180,
          origin={-27,-49})));
    Modelica.Blocks.Sources.Constant conDHWHotAssumed(k=273.15 + 55) annotation (
        Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=180,
          origin={48,-52})));
    Modelica.Blocks.Interfaces.RealOutput V_flow_preCirDHW
      "mFlow that should be set for DHW when there is a demand" annotation (
        Placement(transformation(extent={{100,-40},{120,-20}}),
          iconTransformation(extent={{100,-40},{120,-20}})));
    Modelica.Blocks.Interfaces.RealOutput v_flow_setDHW
      "Mass flow rate that has to be provided by the DHW tank" annotation (
        Placement(transformation(extent={{100,24},{120,44}}), iconTransformation(
            extent={{100,24},{120,44}})));
    Modelica.Blocks.Interfaces.RealInput V_flow_mesDHW
      "Mass flow coming from the test rig as a measured input" annotation (
        Placement(transformation(extent={{-122,54},{-82,94}}), iconTransformation(
            extent={{-122,54},{-82,94}})));
    Modelica.Blocks.Interfaces.RealInput T_hotMesDHW
      "Temperature (hot water) coming from the DHW tank" annotation (Placement(
          transformation(extent={{-122,-6},{-82,34}}), iconTransformation(extent=
              {{-122,-6},{-82,34}})));
    Modelica.Blocks.Interfaces.RealInput T_coldMesDHW
      "Temperature of cold water coming from the test rig" annotation (Placement(
          transformation(extent={{-122,-68},{-82,-28}}), iconTransformation(
            extent={{-122,-68},{-82,-28}})));
    Modelica.Blocks.Interfaces.RealOutput T_setDHW
      "Cold water temperature coming from district water"
      annotation (Placement(transformation(extent={{100,-6},{120,14}})));
    parameter String tableOutName[5]={"QTap","mFlow","Tm","Tp","tapNo"}
      "Names for the variables in the dhw *.nc file";
    parameter Modelica.Units.SI.Temperature TColdWater=283.15
      "Constant temperature assumed for city water";
    parameter Modelica.Units.SI.SpecificHeatCapacity cp=4180
      "Specific heat capacity of liquid water";
    parameter Modelica.Units.SI.Time preAnnTime_m_flowIdeal=240
      "Time period prior current simulation time";
    parameter Real DHWProfile[:,:]=fill(
        0.0,
        0,
        6) "Table matrix (time = first column; e.g., table=[0, 0; 1, 1; 2, 4])";
  equation
    connect(calcDHWmFlow.mFlowFromTable, dHWCalcTab.DHWTableOutputs_preDelteTime[2])
      annotation (Line(points={{-10,-35.4},{14,-35.4},{14,-21.7}}, color={0,0,127}));
    connect(calcDHWmFlow.Tp, dHWCalcTab.DHWTableOutputs_preDelteTime[4])
      annotation (Line(points={{-10,-45.6},{14,-45.6},{14,-21.7}}, color={0,0,127}));
    connect(conDHWHotAssumed.y, calcDHWmFlow.TTankDHWHotMes) annotation (Line(
          points={{37,-52},{0,-52},{0,-52.4},{-9.66,-52.4}}, color={0,0,127}));
    connect(dHWCalcTab.TColdFromParam, calcDHWmFlow.TCold) annotation (Line(
          points={{35,4.2},{54,4.2},{54,-26},{26,-26},{26,-63.28},{-10,-63.28}},
          color={0,0,127}));
    connect(calcDHWmFlow.mFlowCalcedDHWSet, V_flow_preCirDHW) annotation (Line(
          points={{-44,-49},{-48,-49},{-48,-50},{-56,-50},{-56,-30},{110,-30}},
          color={0,0,127}));
    connect(dHWCalcTab.mFlowDHWTank, v_flow_setDHW)
      annotation (Line(points={{35,33.8},{110,33.8},{110,34}}, color={0,0,127}));
    connect(dHWCalcTab.mFlowMes, V_flow_mesDHW) annotation (Line(points={{-28,42.68},
            {-102,42.68},{-102,74}}, color={0,0,127}));
    connect(dHWCalcTab.TDHWMes, T_hotMesDHW)
      annotation (Line(points={{-28,19},{-102,19},{-102,14}}, color={0,0,127}));
    connect(dHWCalcTab.TColdMes, T_coldMesDHW) annotation (Line(points={{-28,-5.42},
            {-72,-5.42},{-72,-48},{-102,-48}}, color={0,0,127}));
    connect(dHWCalcTab.TColdFromParam, T_setDHW) annotation (Line(points={{35,4.2},
            {67.5,4.2},{67.5,4},{110,4}}, color={0,0,127}));
    annotation (Icon(graphics={
        Ellipse(
          extent={{45,2},{64,-18}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{33,-34},{52,-54}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{65,-24},{84,-44}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{55,-62},{74,-82}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-39,27},{39,-27}},
            lineColor={95,95,95},
            fillPattern=FillPattern.HorizontalCylinder,
            fillColor={175,175,175},
            origin={59,45},
            rotation=-90),
          Rectangle(
            extent={{-82,84},{32,44}},
            lineColor={95,95,95},
            fillPattern=FillPattern.HorizontalCylinder,
            fillColor={175,175,175})}));
  end HiLDHWTab;

  model DHWHil
    extends BESMod.Systems.Demand.DHW.BaseClasses.PartialDHW;
    Modelica.Fluid.Sources.MassFlowSource_T bouDHW(redeclare package Medium =
          Medium, nPorts=1) annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=180,
          origin={-44,-58})));
    Modelica.Fluid.Sources.Boundary_pT BouDHW(redeclare package Medium = Medium,
        nPorts=1) annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=180,
          origin={-68,60})));
    BESMod.Utilities.Electrical.ZeroLoad zeroLoad
      annotation (Placement(transformation(extent={{12,-90},{32,-70}})));
  equation
    connect(port_b, bouDHW.ports[1]) annotation (Line(points={{-100,-60},{-98,-60},
            {-98,-58},{-54,-58}}, color={0,127,255}));
    connect(port_a, BouDHW.ports[1])
      annotation (Line(points={{-100,60},{-78,60}}, color={0,127,255}));
    connect(zeroLoad.internalElectricalPin, internalElectricalPin) annotation (
        Line(
        points={{32,-80},{70,-80},{70,-98}},
        color={0,0,0},
        thickness=1));
  end DHWHil;
end Demand;
