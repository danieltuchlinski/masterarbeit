within MPC;
model MA_Kevin_Model
  extends BESMod.Systems.BaseClasses.PartialBuildingEnergySystem(
    redeclare BESMod.Systems.Demand.DHW.DHW DHW(
      use_pressure=false,
      redeclare BESMod.Systems.RecordsCollection.Movers.DefaultMover pumpData,
      redeclare BESMod.Systems.Demand.DHW.TappingProfiles.calcmFlowEquStatic
        calcmFlow),
    redeclare BESMod.Systems.Electrical.DirectGridConnectionSystem electrical,
    redeclare BESMod.Systems.Demand.Building.TEASERThermalZone building(
        redeclare BESMod.Examples.BAUSimStudy.Buildings.Case_1_standard
        oneZoneParam(
        VAir=120,
        AZone=40,
        AWin={2,2,2,2},
        ATransparent={2,2,2,2},
        AExt={15,15,15,15},
        AInt=140,
        AFloor=35,
        ARoof=37)),
    redeclare BESMod.Systems.Control.NoControl control,
    redeclare BESMod.Systems.Ventilation.NoVentilation ventilation,
    redeclare BESMod.Systems.Hydraulical.HydraulicSystem hydraulic(
      redeclare
        BESMod.Systems.Hydraulical.Interfaces.Generation.HeatPumpAndHeatingRod
        generation(
        redeclare package Medium_eva = AixLib.Media.Air,
        redeclare
          BESMod.Systems.Hydraulical.Interfaces.Generation.RecordsCollection.HPCalibration
          heatPumpParameters,
        redeclare
          BESMod.Systems.Hydraulical.Interfaces.Generation.RecordsCollection.DefaultHR
          heatingRodParameters,
        redeclare BESMod.Systems.RecordsCollection.Movers.DefaultMover pumpData),
      redeclare MPC.Controller_for_MPC control(
        HeatingRod(y=false),
        redeclare
          BESMod.Systems.Hydraulical.Control.Components.ThermostaticValveController.ThermostaticValvePIControlled
          thermostaticValveController,
        redeclare
          BESMod.Systems.Hydraulical.Control.RecordsCollection.ThermostaticValveDataDefinition
          thermostaticValveParameters,
        redeclare
          BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultSafetyControl
          safetyControl,
        redeclare
          BESMod.Systems.Hydraulical.Control.Components.HeatPumpNSetController.PI_InverterHeatPumpController
          HP_nSet_Controller(nMin=0.33),
        redeclare
          BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultBivHPControl
          bivalentControlData(k=0.3, T_I=1200),
        booleanExpression1(y=true)),
      redeclare BESMod.Systems.Hydraulical.Distribution.BuildingOnly
        distribution(nParallelDem=1),
      redeclare BESMod.Systems.Hydraulical.Transfer.IdealValveRadiator transfer(
          redeclare BESMod.Systems.RecordsCollection.Movers.DefaultMover
          pumpData)),
    redeclare BESMod.Systems.UserProfiles.TEASERProfiles userProfiles(use_dhw=
          false,
               redeclare BESMod.Systems.Demand.DHW.RecordsCollection.NoDHW
        DHWProfile),
    redeclare
      BESMod.Examples.MyOwnHeatingRodEfficiencyStudy.SimpleStudyOfHeatingRodEfficiency
      parameterStudy,
    redeclare MPC.DataBase.MAKevinSystemParameters systemParameters);

  Modelica.Blocks.Sources.ContinuousClock Clock(offset=0, startTime=0)
    annotation (Placement(transformation(extent={{172,-60},{192,-40}})));
  Modelica.Blocks.Interfaces.RealOutput SimuTime
    annotation (Placement(transformation(extent={{222,-66},{242,-46}})));
  Modelica.Blocks.Math.ContinuousMean continuousMean
    annotation (Placement(transformation(extent={{216,140},{256,172}})));
  Modelica.Blocks.Sources.RealExpression T_VL(y=hydraulic.generation.heatPump.senT_b1.T)
    annotation (Placement(transformation(extent={{118,158},{162,176}})));
  Modelica.Blocks.Interfaces.RealOutput T_VL_mean
    annotation (Placement(transformation(extent={{280,140},{300,160}})));
equation
  connect(Clock.y, SimuTime) annotation (Line(points={{193,-50},{216,-50},{
          216,-56},{232,-56}}, color={0,0,127}));
  connect(T_VL.y, continuousMean.u) annotation (Line(points={{164.2,167},{204,
          167},{204,156},{212,156}}, color={0,0,127}));
  connect(continuousMean.y, T_VL_mean) annotation (Line(points={{258,156},{282,
          156},{282,150},{290,150}}, color={0,0,127}));
  annotation (experiment(
      StartTime=25142400,
      StopTime=25228800,
      Interval=1,
      __Dymola_Algorithm="Dassl"));
end MA_Kevin_Model;
