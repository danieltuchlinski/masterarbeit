import teaser.examples.e1_generate_archetype as e1
import teaser.logic.utilities as utilities
import os
from teaser.project import Project

prj = Project(load_data=False)
prj.name = "ArchetypeExample"

prj.add_residential(
        method='iwu',
        usage='single_family_dwelling',
        name="MPC_Zone",
        year_of_construction=1997,
        number_of_floors=1,
        height_of_floors=2.5,
        net_leased_area=120.0,
        construction_type='light')


prj.used_library_calc = 'AixLib'
prj.number_of_elements_calc = 4
prj.weather_file_path = utilities.get_full_path(
        os.path.join(
            "data",
            "input",
            "inputdata",
            "weatherdata",
            "DEU_BW_Mannheim_107290_TRY2010_12_Jahr_BBSR.mos"))

prj.calc_all_buildings()

prj.export_aixlib(
        internal_id=None,
        path=r'D:\cve-fwi\optisim\submodules')