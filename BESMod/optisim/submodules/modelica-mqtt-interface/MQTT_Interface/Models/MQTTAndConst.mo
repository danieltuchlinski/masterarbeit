within MQTT_Interface.Models;
model MQTTAndConst "Switch between MQTT and Constants"

  parameter Boolean use_SQL=true;

  parameter Integer nWriteValues=5;
  parameter Integer nReadValues = 1;

  Modelica.Blocks.Interfaces.RealOutput readValues[nReadValues] annotation (Placement(transformation(extent={{100,-10},{120,10}}), iconTransformation(extent={{100,-10},{120,10}})));
  Modelica.Blocks.Interfaces.RealInput writeValues[nWriteValues] if use_SQL
                                                                          annotation (Placement(transformation(extent={{-120,-20},{-80,20}}), iconTransformation(extent={{-120,-10},{-100,10}})));
  replaceable parameter
    MQTT_Interface.MQTTConnectionPara.BasisForMQTTConnection mqttConParam
    constrainedby MQTT_Interface.MQTTConnectionPara.BasisForMQTTConnection
    "SQL server, database, table information" annotation (choicesAllMatching=true,
      Dialog(enable=use_SQL, group="Connection infos"));
  parameter String startName="simStartModelName" annotation (Dialog(enable=use_SQL, group="Connection infos"));
  parameter Modelica.Units.SI.Time startTimeSQL=0
    "Start time, when data exchange with the set frequency (rate) begins"
    annotation (Dialog(enable=use_SQL, group="Data exchange"));
  parameter Modelica.Units.SI.Time samRate=1
    "Rate / frequency of data exchange"
    annotation (Dialog(enable=use_SQL, group="Data exchange"));

  parameter String namesToRead[:]={"nameToRead1","nameToRead2"} "Names of values to read from SQL table";
  parameter Real standardReadValues[nReadValues] = fill(0, nReadValues) "Used before any message received";
  parameter String namesToWrite[:]={"nameToWrite1","nameToWrite2"} "Names of values to write to SQL table" annotation(Dialog(enable=use_SQL));

  parameter Modelica.Units.SI.Time timeConstsDampReadValues[nReadValues](each
      min=Modelica.Constants.eps) = fill(Modelica.Constants.eps, nReadValues)
    "Time constants for critical damper of read values"
    annotation (Dialog(enable=use_SQL));
  parameter Modelica.Units.SI.Time timeConstsDampWriteValues[nWriteValues](
      each min=Modelica.Constants.eps) = fill(Modelica.Constants.eps,
    nWriteValues) "Time constants for critical damper of write values"
    annotation (Dialog(enable=use_SQL));

  parameter Real kReadValues[nReadValues]=fill(1, nReadValues) "Mulitplicator for input read values (e.g. unit conversion)" annotation (Dialog(enable=use_SQL, group="Unit conversion - Read values"));
  parameter Real addReadValues[nReadValues]=fill(0, nReadValues) "Add constants for input read values (e.g. unit conversion)" annotation (Dialog(enable=use_SQL, group="Unit conversion - Read values"));
  parameter Real maxReadValues[nReadValues]=fill(Modelica.Constants.inf, nReadValues) "Upper limits of read signals" annotation (Dialog(enable=use_SQL, group="Unit conversion - Read values"));
  parameter Real minReadValues[nReadValues]=fill(-1*Modelica.Constants.inf, nReadValues) "Lower limits of read signals" annotation (Dialog(enable=use_SQL, group="Unit conversion - Read values"));

  parameter Real kWriteValues[nWriteValues]=fill(1, nWriteValues) "Mulitplicator for input write values (e.g. unit conversion)" annotation (Dialog(enable=use_SQL, group="Unit conversion - Write values"));
  parameter Real addWriteValues[nWriteValues]=fill(0, nWriteValues) "Add constants for input write values (e.g. unit conversion)" annotation (Dialog(enable=use_SQL, group="Unit conversion - Write values"));
  parameter Real maxWriteValues[nWriteValues]=fill(Modelica.Constants.inf, nWriteValues) "Upper limits of write signals" annotation (Dialog(enable=use_SQL, group="Unit conversion - Write values"));
  parameter Real minWriteValues[nWriteValues]=fill(-1*Modelica.Constants.inf, nWriteValues) "Lower limits of write signals" annotation (Dialog(enable=use_SQL, group="Unit conversion - Write values"));

  parameter String terminateString="terminate" "A name / string to abort simulation. When value of 'terminateString' becomes > 0.5, simulation aborts." annotation (Dialog(enable=use_SQL, group="Termination of simulation via SQL"));

  Modelica.Blocks.Sources.Constant const[nReadValues](final k=standardReadValues) if not use_SQL annotation (Placement(transformation(extent={{-10,-50},{10,-30}})));

  Modelica.Blocks.Continuous.CriticalDamping dampWriteValues[nWriteValues](
    each n=3,
    final f=fill(1, nWriteValues) ./ timeConstsDampWriteValues,
    each final normalized=true,
    each initType=Modelica.Blocks.Types.Init.SteadyState) if use_SQL
                                                                annotation (Placement(transformation(extent={{-38,40},{-22,56}})));
  Modelica.Blocks.Interfaces.BooleanOutput activeSQL "Is SQL active (time > start time SQL)" annotation (Placement(transformation(extent={{100,-40},{120,-20}}), iconTransformation(extent={{100,-40},{120,-20}})));

  MQTTBounds mQTTBounds(
    final nWriteValues=nWriteValues,
    final nReadValues=nReadValues,
    final rate=samRate,
    final namesToRead=namesToRead,
    final namesToWrite=namesToWrite,
    kWriteValues=kWriteValues,
    addWriteValues=addWriteValues,
    maxWriteValues=maxWriteValues,
    minWriteValues=minWriteValues,
    final staTimMQTT=startTimeSQL,
    final kReadValues=kReadValues,
    final maxReadValues=maxReadValues,
    final minReadValues=minReadValues,
    final addReadValues=addReadValues,
    final standardReadValues=standardReadValues,
    final useJSON=useJSON,
    final nKeys=nKeys,
    final keys=keys,
    final timeConstsDampReadValues=timeConstsDampReadValues,
    final mqttConParam=mqttConParam) if use_SQL
    annotation (Placement(transformation(extent={{-10,38},{10,58}})));
  parameter Boolean useJSON=false "If true, message is formatted as JSON dict";
  parameter Integer nKeys[size(namesToWrite,1)]= fill(1,size(namesToWrite,1))  "number of keys of each topic";
  parameter String keys[sum(nKeys)]=fill("x",sum(nKeys)) "Keys (if JSON format payload is chosen)";
protected
  Functions.Utilities.BooleanPassThroughConditional
                                              booleanPassThroughConditional(final
      passThrough=use_SQL, final constU=false)
    annotation (Placement(transformation(extent={{26,-18},{36,-8}})));
  Modelica.Blocks.Math.Gain gainReaVal[nReadValues](final k=kReadValues) if not use_SQL annotation (Placement(transformation(extent={{24,-46},{36,-34}})));
  Modelica.Blocks.Math.Add addReaVal[nReadValues](each final k1=+1, each final k2=+1) if not use_SQL annotation (Placement(transformation(extent={{54,-48},{64,-38}})));
  Modelica.Blocks.Sources.Constant constAddReaVal[nReadValues](final k=addReadValues) if not use_SQL annotation (Placement(transformation(extent={{24,-66},{36,-54}})));
equation
  connect(writeValues, dampWriteValues.u) annotation (Line(points={{-100,0},{-72,0},{-72,48},{-39.6,48}}, color={0,0,127}));
  connect(booleanPassThroughConditional.y, activeSQL) annotation (Line(points={{36.5,
          -13},{80,-13},{80,-30},{110,-30}},                                                                            color={255,0,255}));
  connect(gainReaVal.y, addReaVal.u1) annotation (Line(points={{36.6,-40},{53,-40}}, color={0,0,127}));
  connect(constAddReaVal.y, addReaVal.u2) annotation (Line(points={{36.6,-60},{44,-60},{44,-46},{53,-46}}, color={0,0,127}));
  connect(const.y, gainReaVal.u) annotation (Line(points={{11,-40},{22.8,-40}}, color={0,0,127}));
  connect(addReaVal.y, readValues) annotation (Line(points={{64.5,-43},{66,-43},{66,0},{110,0}}, color={0,0,127}));
  connect(dampWriteValues.y, mQTTBounds.writeValues)
    annotation (Line(points={{-21.2,48},{-11,48}}, color={0,0,127}));
  connect(mQTTBounds.activeMQTT, booleanPassThroughConditional.u) annotation (
      Line(points={{11,41},{16,41},{16,-13},{25,-13}}, color={255,0,255}));
  connect(mQTTBounds.readValues, readValues) annotation (Line(points={{10.6,48},
          {38,48},{38,0},{110,0}}, color={0,0,127}));
  annotation (Icon(graphics={
        Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={175,175,175},
          fillColor={170,213,255},
          fillPattern=FillPattern.Solid,
          visible=not use_SQL),    Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={170,255,255},
          fillColor={255,85,85},
          fillPattern=FillPattern.CrossDiag,
          visible=use_SQL),                   Text(
          extent={{-100,40},{100,-40}},
          lineColor={0,0,0},
          fillColor={255,85,85},
          fillPattern=FillPattern.CrossDiag,
          textString="MQTT",
          visible=use_SQL),                 Text(
          extent={{-60,40},{60,-40}},
          lineColor={0,0,0},
          fillColor={255,85,85},
          fillPattern=FillPattern.CrossDiag,
          textString="Const",
          visible=not use_SQL)}));
end MQTTAndConst;
