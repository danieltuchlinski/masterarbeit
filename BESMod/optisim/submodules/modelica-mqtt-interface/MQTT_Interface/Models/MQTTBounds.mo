within MQTT_Interface.Models;
model MQTTBounds
  parameter Integer nWriteValues=1;
  parameter Integer nReadValues=1;
  parameter Modelica.Units.SI.Time rate=1 "Rate / frequency of data exchange"
    annotation (Dialog(group="Data exchange"));
  parameter String namesToRead[:]={"nameToRead1","nameToRead2"} "Names of values to read from MQTT broker" annotation (Dialog(group="Reading"));
  parameter String namesToWrite[:]={"nameToWrite1","nameToWrite2"} "Names of values to write to MQTT broker" annotation (Dialog(group="Writing"));
  parameter Real kWriteValues[nWriteValues]=fill(1, nWriteValues) "Mulitplicator for input write values (e.g. unit conversion)" annotation (Dialog(group="Unit conversion - Write values"));
  parameter Real addWriteValues[nWriteValues]=fill(0, nWriteValues) "Add constants for input write values (e.g. unit conversion)" annotation (Dialog(group="Unit conversion - Write values"));
  parameter Real maxWriteValues[nWriteValues]=fill(Modelica.Constants.inf, nWriteValues) "Upper limits of write signals" annotation (Dialog(group="Unit conversion - Write values"));
  parameter Real minWriteValues[nWriteValues]=fill(-1*Modelica.Constants.inf, nWriteValues) "Lower limits of write signals" annotation (Dialog(group="Unit conversion - Write values"));
  parameter Modelica.Units.SI.Time staTimMQTT=0
    "Start time, when data exchange with the set frequency (rate) begins"
    annotation (Dialog(group="Data exchange"));
  parameter Real kReadValues[nReadValues]=fill(1, nReadValues) "Mulitplicator for input read values (e.g. unit conversion)" annotation (Dialog(group="Unit conversion - Read values"));
  parameter Real maxReadValues[nReadValues]=fill(Modelica.Constants.inf, nReadValues) "Upper limits of read signals" annotation (Dialog(group="Unit conversion - Read values"));
  parameter Real minReadValues[nReadValues]=fill(-1*Modelica.Constants.inf, nReadValues) "Lower limits of read signals" annotation (Dialog(group="Unit conversion - Read values"));
  parameter Real addReadValues[nReadValues]=fill(0, nReadValues) "Add constants for input read values (e.g. unit conversion)" annotation (Dialog(group="Unit conversion - Read values"));
  parameter Real standardReadValues[nReadValues] = fill(0, nReadValues) "Used before any message received" annotation (Dialog(group="Reading"));
  parameter Boolean useJSON=false "If true, message is formatted as JSON dict";
  parameter Integer nKeys[size(namesToWrite, 1)] "number of keys of each topic";
  parameter String keys[sum(nKeys)] "Keys (if JSON format payload is chosen)";
  parameter Modelica.Units.SI.Time timeConstsDampReadValues[nReadValues](each
      min=Modelica.Constants.eps) = fill(Modelica.Constants.eps, nWriteValues)
    "Time constants for critical damper of write values"
    annotation (Dialog(enable=use_SQL));

  Modelica.Blocks.Interfaces.RealInput writeValues[nWriteValues] annotation (
      Placement(transformation(extent={{-140,-18},{-100,22}}),
        iconTransformation(extent={{-120,-10},{-100,10}})));
  MqttPublish mqttPublish(final topic=namesToWrite,
    final useJSON=useJSON,
    final startTime=staTimMQTT,
    final sampleTime=rate,
    final IPAddress=mqttConParam.server,
    final port=mqttConParam.port,
    final QoS=mqttConParam.QoS,
    final clientID="send" + String(
        Modelica.Math.Random.Utilities.automaticGlobalSeed()),
    final nInputs=nWriteValues,
    final nKeys=nKeys,
    final keys=keys)
    annotation (Placement(transformation(extent={{-26,-10},{-6,10}})));
  replaceable MQTTConnectionPara.BasisForMQTTConnection mqttConParam
    annotation (Placement(transformation(extent={{-10,44},{10,64}})));
  Modelica.Blocks.Sources.BooleanExpression afterStartTime(y=time > staTimMQTT)
                                                                               annotation (Placement(transformation(extent={{-92,-80},
            {-72,-60}})));
  Modelica.Blocks.Interfaces.BooleanOutput activeMQTT
    "Is MQTT active (time > start time MQTT)" annotation (Placement(
        transformation(extent={{100,-80},{120,-60}}), iconTransformation(extent=
           {{100,-80},{120,-60}})));
  Modelica.Blocks.Interfaces.RealOutput readValues[nReadValues] annotation (Placement(transformation(extent={{96,-10},
            {116,10}}),                                                                                                            iconTransformation(extent={{96,-10},
            {116,10}})));
  MqttSubscribe mqttSubscribe[nReadValues](
    each final numberOfValues=1,
    each final startTime=staTimMQTT,
    each final sampleTime=rate,
    each final IPAddress=mqttConParam.server,
    each final port=mqttConParam.port,
    final topic=namesToRead,
    clientID=String(Modelica.Math.Random.Utilities.automaticGlobalSeed()),
    each final QoS=mqttConParam.QoS,
    each final userName=mqttConParam.user,
    each final password=mqttConParam.password,
    valueEmptyTopic=String(standardReadValues))
    annotation (Placement(transformation(extent={{-4,-10},{16,10}})));

  Modelica.Blocks.Continuous.CriticalDamping dampReadValues[nReadValues](
    each n=3,
    final f=fill(1, nReadValues) ./ timeConstsDampReadValues,
    each final normalized=true,
    each initType=Modelica.Blocks.Types.Init.InitialOutput,
    final y_start=standardReadValues)
    annotation (Placement(transformation(extent={{30,-44},{46,-28}})));
  Modelica.Blocks.Sources.Constant const[nReadValues](final k=
        standardReadValues)                                                                      annotation (Placement(transformation(extent={{-56,-50},
            {-36,-30}})));
  Modelica.Blocks.Logical.Switch switch_ConstantValue[nReadValues]
    annotation (Placement(transformation(extent={{-2,-46},{18,-26}})));

protected
  Modelica.Blocks.Math.Gain gainWriVal[nWriteValues](final k=kWriteValues) annotation (Placement(transformation(extent={{-94,-4},
            {-82,8}})));
  Modelica.Blocks.Sources.Constant constAddWriVal[nWriteValues](final k=
        addWriteValues)                                                  annotation (Placement(transformation(extent={{-94,-24},
            {-82,-12}})));
  Modelica.Blocks.Math.Add addWriVal[nWriteValues](each final k1=+1, each final
            k2=+1)                                                                     annotation (Placement(transformation(extent={{-70,-4},
            {-60,6}})));
  Modelica.Blocks.Nonlinear.Limiter limiterWriteValues[nWriteValues](final uMax=
       maxWriteValues, final uMin=minWriteValues)                                                                          annotation (Placement(transformation(extent={{-50,-6},
            {-38,6}})));
  Modelica.Blocks.Math.Gain gainReaVal[nReadValues](final k=kReadValues) annotation (Placement(transformation(extent={{56,-6},
            {68,6}})));
  Modelica.Blocks.Math.Add addReaVal[nReadValues](each final k1=+1, each final
      k2=+1)                                                                          annotation (Placement(transformation(extent={{78,-8},
            {88,2}})));
  Modelica.Blocks.Sources.Constant constAddReaVal[nReadValues](final k=
        addReadValues)                                                                annotation (Placement(transformation(extent={{56,-26},
            {68,-14}})));
  Modelica.Blocks.Nonlinear.Limiter limiterReadValues[nReadValues](final uMax=
        maxReadValues, final uMin=minReadValues)                                                                       annotation (Placement(transformation(extent={{36,-6},
            {48,6}})));
equation
  connect(writeValues, gainWriVal.u)
    annotation (Line(points={{-120,2},{-95.2,2}}, color={0,0,127}));
  connect(constAddWriVal.y, addWriVal.u2) annotation (Line(points={{-81.4,-18},{
          -76,-18},{-76,-2},{-71,-2}}, color={0,0,127}));
  connect(gainWriVal.y, addWriVal.u1)
    annotation (Line(points={{-81.4,2},{-71,2},{-71,4}}, color={0,0,127}));
  connect(addWriVal.y, limiterWriteValues.u) annotation (Line(points={{-59.5,1},
          {-55.75,1},{-55.75,0},{-51.2,0}}, color={0,0,127}));
  connect(limiterWriteValues.y, mqttPublish.u)
    annotation (Line(points={{-37.4,0},{-28,0}}, color={0,0,127}));
  connect(afterStartTime.y, activeMQTT) annotation (Line(points={{-71,-70},{110,
          -70}},               color={255,0,255}));
  connect(gainReaVal.y,addReaVal. u1) annotation (Line(points={{68.6,0},{77,0}}, color={0,0,127}));
  connect(constAddReaVal.y,addReaVal. u2) annotation (Line(points={{68.6,-20},{74,
          -20},{74,-6},{77,-6}},                                                                         color={0,0,127}));
  connect(addReaVal.y,readValues)  annotation (Line(points={{88.5,-3},{93.25,-3},
          {93.25,0},{106,0}},                                                                        color={0,0,127}));
  connect(limiterReadValues.y,gainReaVal. u) annotation (Line(points={{48.6,0},{
          54.8,0}},                                                                       color={0,0,127}));
  for i in 1:nReadValues loop
       connect(afterStartTime.y, switch_ConstantValue[i].u2) annotation (Line(points=
         {{-71,-70},{-16,-70},{-16,-36},{-4,-36}}, color={255,0,255}));
  end for;
  connect(mqttSubscribe.y[1], switch_ConstantValue.u1) annotation (Line(points=
          {{17,0},{22,0},{22,-14},{-12,-14},{-12,-28},{-4,-28}}, color={0,0,127}));
  connect(const.y, switch_ConstantValue.u3) annotation (Line(points={{-35,-40},
          {-26,-40},{-26,-44},{-4,-44}}, color={0,0,127}));
  connect(switch_ConstantValue.y, dampReadValues.u)
    annotation (Line(points={{19,-36},{28.4,-36}}, color={0,0,127}));
  connect(dampReadValues.y, limiterReadValues.u) annotation (Line(points={{46.8,
          -36},{48,-36},{48,-16},{28,-16},{28,0},{34.8,0}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
                                   Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={170,255,255},
          fillColor={255,85,85},
          fillPattern=FillPattern.CrossDiag), Text(
          extent={{-76,56},{68,-40}},
          lineColor={0,0,0},
          textString="MQTT")}),                                  Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end MQTTBounds;
