within MQTT_Interface.Models;
block MqttPublish

  extends Modelica_DeviceDrivers.Utilities.Icons.BaseIcon;
  extends Modelica_DeviceDrivers.Utilities.Icons.MQTTconnection;

  import Modelica_DeviceDrivers.Communication.MQTT;
  import Modelica_DeviceDrivers.Utilities.Functions.getMACAddress;

  parameter String delimiter = "," "Delimiter that separates values in payload";
  parameter String topic[:] = {"someTopic"} "Channel name";
  parameter Boolean useJSON = false "If true, message is formatted as JSON dict";
  parameter Modelica.Units.SI.Time startTime=0 "Start time for sampling";
  parameter Modelica.Units.SI.Period sampleTime=0.1 "Sample time";

  parameter String IPAddress = "localhost" "IP address";
  parameter Integer port = 1883 "Port";
  parameter Integer QoS(min=0, max=2)=0 "Quality of service";
  parameter Integer deliveryTimeout = 10 "Delivery timeout (in seconds)";
  parameter Boolean retained = false "Retained flag" annotation(Dialog(choices(checkBox=true)));
  parameter Boolean reliable = true "=true, if a published message must be completed (acknowledgements received) before another message can be sent";
  parameter String clientID = "send-" + getMACAddress() "Unique client identifier";
  parameter String userName = "" "User name for authentication and authorisation";
  parameter Integer nInputs;
  parameter String password = "" "Password for authentication and authorisation";

  Modelica.Blocks.Interfaces.RealInput u[nInputs]
    annotation (Placement(transformation(extent={{-140,-20},{-100,20}})));
  parameter Integer nKeys[size(topic,1)] "number of keys of each topic" annotation(Dialog(tab="General"));
  parameter String keys[sum(nKeys)] "Keys (if JSON format payload is chosen)";
protected
  parameter Boolean receiver = false "Set to be a sender port";

  MQTT mqtt = MQTT(  provider = "tcp://",
               address = IPAddress,
               port = port,
               receiver = receiver,
               channel = "test",
               QoS = QoS,
               reliable = reliable,
               clientID = clientID,
               userName = userName,
               password = password);

equation

  when sample(startTime, sampleTime) then

    MQTT_Interface.Functions.callMqttPublish(
      mqtt,
      topic,
      useJSON,
      keys,
      nKeys,
      u,
      retained,
      deliveryTimeout);
  end when;

  annotation (defaultComponentName="mqttPublish",
    Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end MqttPublish;
