within MQTT_Interface.Examples;
model TestMQTTAndConst

  extends Modelica.Icons.Example;

        //(or use SPS IP) 137.226.248.130

  Modelica.Blocks.Sources.Sine sine[5](each amplitude=10, each f=0.5)
    annotation (Placement(transformation(extent={{-80,-16},{-60,4}})));
  Models.MQTTAndConst mQTTAndConst(use_SQL=true,
    nWriteValues=5,
    nReadValues=2,
    redeclare MQTTConnectionPara.TickStackOpenstack mqttConParam,
                                                 standardReadValues={10,30},
    useJSON=true,
    nKeys={2,3},
    keys={"a","b","a","b","c"})
    annotation (Placement(transformation(extent={{-18,-16},{2,4}})));
equation

  connect(sine.y, mQTTAndConst.writeValues)
    annotation (Line(points={{-59,-6},{-19,-6}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    experiment(StopTime=120, __Dymola_Algorithm="Dassl"));
end TestMQTTAndConst;
