within MQTT_Interface.Examples;
model TestMQTTBounds

  extends Modelica.Icons.Example;

        //(or use SPS IP) 137.226.248.130

  Modelica.Blocks.Sources.Sine sine[5](each amplitude=10, each f=0.5)
    annotation (Placement(transformation(extent={{-80,-16},{-60,4}})));
  Models.MQTTBounds mQTTBounds(
    nWriteValues=5,            namesToRead={"nameToRead1","nameToRead2",
        "nameToRead3"}, namesToWrite={"nameToWrite1","nameToWrite2"},
    useJSON=true,
    nKeys={3,2},
    keys={"a","b","c","a","b"})
    annotation (Placement(transformation(extent={{-14,-12},{6,8}})));
equation

  connect(sine.y, mQTTBounds.writeValues) annotation (Line(points={{-59,-6},{
          -38,-6},{-38,-2},{-15,-2}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    experiment(StopTime=30, __Dymola_Algorithm="Dassl"));
end TestMQTTBounds;
