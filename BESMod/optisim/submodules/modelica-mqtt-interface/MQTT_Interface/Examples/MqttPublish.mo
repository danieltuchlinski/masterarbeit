within MQTT_Interface.Examples;
model MqttPublish

  extends Modelica.Icons.Example;

  MQTT_Interface.Models.MqttPublish mqttSend(
    sampleTime=1,
    IPAddress="broker.mqttdashboard.com",
    topic={"someTopic","otherTopic"},
    nInputs=2)                        annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={20,0})));
        //(or use SPS IP) 137.226.248.130

  Modelica.Blocks.Sources.Sine sine[2](each amplitude=10, f={0.1,0.3})
    annotation (Placement(transformation(extent={{-78,-10},{-58,10}})));
equation

  connect(sine.y, mqttSend.u[1:2])
    annotation (Line(points={{-57,0},{-24,0},{-24,0},{8,0}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    experiment(StopTime=30, __Dymola_Algorithm="Dassl"));
end MqttPublish;
