within MQTT_Interface.Examples;
model MqttPublishMultipleClients

  extends Modelica.Icons.Example;

        //(or use SPS IP) 137.226.248.130

  Modelica.Blocks.Sources.Sine sine(each amplitude=10, each f=0.3)
    annotation (Placement(transformation(extent={{-62,34},{-42,54}})));
  Models.MqttPublish mqttPublish(IPAddress="broker.mqttdashboard.com", nInputs=
        1) annotation (Placement(transformation(extent={{28,60},{48,80}})));
  Models.MqttPublish mqttPublish1(
    IPAddress="broker.mqttdashboard.com",
    clientID="asdsad",
    nInputs=1) annotation (Placement(transformation(extent={{36,20},{56,40}})));
equation

  connect(sine.y, mqttPublish.u[1]) annotation (Line(points={{-41,44},{-6,44},{
          -6,70},{26,70}}, color={0,0,127}));
  connect(mqttPublish1.u[1], sine.y) annotation (Line(points={{34,30},{8,30},{8,
          44},{-41,44}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    experiment(StopTime=30, __Dymola_Algorithm="Dassl"));
end MqttPublishMultipleClients;
