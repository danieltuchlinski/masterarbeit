within MQTT_Interface.Functions.Utilities;
function splitAndConvertToReal

  extends Modelica.Icons.Function;

  input String serializedData "String containing serialized data";
  input String delimiter "Delimiter that separates values in payload string";
  input Integer numberOfValues
    "Number of values that are encoded in payload string";
  output Real values[numberOfValues]
    "Vector with separated and converted data";

protected
  Integer i "Running index loop";
  Integer startIndex "Start index for scanReal";
  String serializedDataLocal "Variable to internally handle modify payload";

algorithm

  // As Modelicas scanReal can only scan numbers that are separated by a
  // whitespace, we need to replace the delimiter accordingly. This slows down
  // but allows for different payload structures.
  serializedDataLocal := Modelica.Utilities.Strings.replace(string = serializedData,
                    searchString = delimiter,
                    replaceString = " ",
                    replaceAll = true);

  startIndex := 1;

  for i in 1:numberOfValues loop
    (values[i], startIndex) := Modelica.Utilities.Strings.scanReal(
                                  string = serializedDataLocal,
                                  startIndex = startIndex,
                                  unsigned = false,
                                  message = "Scan real unsusccessful");
  end for;

end splitAndConvertToReal;
