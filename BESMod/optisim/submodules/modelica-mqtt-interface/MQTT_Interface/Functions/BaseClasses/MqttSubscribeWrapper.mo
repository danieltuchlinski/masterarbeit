within MQTT_Interface.Functions.BaseClasses;
function mqttSubscribeWrapper

  extends Modelica.Icons.Function;

  input Modelica_DeviceDrivers.Communication.MQTT mqtt;
  output String y;
  external "C" y = MDD_mqttRead(mqtt)
    annotation (
      Include = "#include \"MDDMQTT.h\"",
      Library = {"paho-mqtt3cs", "pthread", "ssl", "crypto"},
      LibraryDirectory = "modelica://Modelica_DeviceDrivers/Resources/Library",
      __iti_dll = "ITI_MDDMQTT.dll",
      __iti_dllNoExport = true);
end mqttSubscribeWrapper;
