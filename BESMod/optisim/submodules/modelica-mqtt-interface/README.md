
# Modelica-MQTT Interface

This project uses and tests the "Send"- and "Receive"- Blocks of the Modelica DeviceDrivers Library
(https://github.com/modelica-3rdparty/Modelica_DeviceDrivers).
Example models for testing are used from Aixlib.

There is another Modelica-MQTT Repo, which is used to create the interface by using c functions 
(https://git.rwth-aachen.de/EBC/Team_BES/Communication/MQTTModelica).

## Notes about the String-Buffersize

Data is sent and received with datatype "String".
To send data as String the Utilities.StringConverter model is used. There is a constant buffersize=8  (Parameter).
To send Strings with a different length there is a StringConverter_var model.

On the receiving side the function Modelica.Utilities.Strings.Advanced.scanReal() is used.

## Notes about Realtime

For simulating in realtime there is a "Realtime-Block" in the DeviceDrivers library. It works, but it's not a robust solution.

The DDE Server does not work yet. Therefore the windows binaries (.dll) of the DeviceDrivers library have to be copied to the used working directory.
This can be done by dymola commands (work in progress). 

## Notes about Samplingtime

The sampling time of the "when - sample" - environment and of the send-/receive- blocks is set to 1 sec.