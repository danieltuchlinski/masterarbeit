package ArchetypeExample
  extends Modelica.Icons.Package;

  annotation (uses(Modelica(version="4.0.0"), AixLib(version="1.2.0")),
                                                               version="2",
  conversion(from(version="1", script=
          "modelica://ArchetypeExample/Resources/Scripts/ConvertFromArchetypeExample_1.mos")));
end ArchetypeExample;
