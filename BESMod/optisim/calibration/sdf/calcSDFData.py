"""This file was used to calculate the sdf-input data based on the danfoss datasheet, presented in the thesis.
The resulting .sdf was added to the AixLib."""

import sdf
import numpy as np


def get_sdfData(filename):
    data = {}
    f = open(filename, "r+")
    content = f.read().split("\n")
    colHea = 3
    T_con = [int(val) for val in content[0].split(";")[colHea:]]
    names = content[1].split(";")[colHea:]
    temp_name = {}
    blockName = ""
    velo = 0
    temp_velo = {}
    for i in range(2, len(content)):
        lineSplit = content[i].split(";")
        if i + 1 == len(content):
            return data, T_con
        else:
            nextLine = content[i + 1].split(";")
        points = {"P_el": [], "Q_o": []}
        for i in range(0, len(lineSplit[colHea:])):
            val = lineSplit[colHea:][i]
            if val == "-":
                val = 0
            else:
                val = float(val)
            points[names[i]].append(val)
        TEva = int(lineSplit[2])
        if lineSplit[0] != "":
            blockName = lineSplit[0]  # Read name
            temp_name = {}
            velo = int(lineSplit[1])  # Read velo
            temp_velo = {}
        elif lineSplit[0] == "" and lineSplit[1] != "":
            velo = int(lineSplit[1])  # Read velo
            temp_velo = {}
        temp_velo[TEva] = points
        if nextLine == [""]:
            temp_name[velo] = temp_velo
            data[blockName] = temp_name
        else:
            if nextLine[0] != "":
                data[blockName] = temp_name
            if nextLine[1] != "":
                temp_name[velo] = temp_velo

    return data, T_con


def create_sdf(data, T_con, savepath):
    for group, value in data.items():
        dTCon = np.array([T_con[i] for i in range(0, len(T_con), 2)])
        dN = np.array(list(value.keys()))
        dTEva = np.array(list(value[dN[0]].keys()))
        sdfTCon = sdf.Dataset("TCon", data=dTCon, unit="K", is_scale=True, display_name="TCon")
        sdfTEva = sdf.Dataset("TEva", data=dTEva, unit="K", is_scale=True, display_name="TEva")
        sdfN = sdf.Dataset("n", data=dN, is_scale=True, display_name="n")
        Pel = np.zeros((6, 9, 3))
        # QCon = Pel+Q_o
        QCon = np.zeros((6, 9, 3))
        for n in range(0, len(dN)):
            temp_data = value[dN[n]]
            for tEva in range(0, len(dTEva)):
                P_el_temp = np.array(temp_data[dTEva[tEva]]["P_el"])
                Q_o_temp = np.array(temp_data[dTEva[tEva]]["Q_o"])
                QCon_temp = P_el_temp * 1000 + Q_o_temp
                for tCon in range(0, len(dTCon)):
                    Pel[tEva][tCon][n] = P_el_temp[tCon] * 1000
                    QCon[tEva][tCon][n] = QCon_temp[tCon]
        sdfPel = sdf.Dataset("Pel", data=Pel, unit="W", scales=[sdfTEva, sdfTCon, sdfN])
        sdfQCon = sdf.Dataset("QCon", data=QCon, unit="W", scales=[sdfTEva, sdfTCon, sdfN])
        g = sdf.Group("/", comment="Generated with python script", datasets=[sdfTCon, sdfN, sdfTEva, sdfPel, sdfQCon])
        sdf.save(savepath + "\%s.sdf" % group, g)


if __name__ == "__main__":
    PATH = r"D:\sgo-kwa\Repos\optisim\calibration\sdf\test.csv"
    SAVEPATH = r"D:\sgo-kwa\Repos\optisim\calibration\sdf"
    data, T_con = get_sdfData(PATH)
    create_sdf(data, T_con, SAVEPATH)
    