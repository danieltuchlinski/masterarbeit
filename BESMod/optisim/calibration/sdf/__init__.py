"""
Functions to generate HP Maps automatically
"""
import time
import sdf
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from vclibpy import HeatPumpState, FlowsheetState


def calc_multiple_states(savepath, heat_pump, fluid, hp_states, **kwargs):
    rel_infos = []
    for i, hp_state in enumerate(hp_states):
        fs_state = None
        print(f"Running combination {i+1}/{len(hp_states)}.")
        try:
            fs_state = heat_pump.calc_steady_state(fluid=fluid,
                                                   hp_state=hp_state,
                                                   **kwargs)
        except Exception as e:
            # Avoid loss of data if unexcepted refprop errors occur.
            print(f"An error occurred: {e}")
        if fs_state is None:
            fs_state = FlowsheetState()
        hp_state_dic = hp_state.__dict__.copy()
        hp_state_dic.update(fs_state.__dict__.copy())
        hp_state_dic["N / Hz"] = heat_pump.compressor.get_n_absolute(hp_state_dic["n"])
        rel_infos.append(hp_state_dic)
    df = pd.DataFrame(rel_infos)
    df.index.name = "State Number"
    df.to_excel(savepath + f"\{heat_pump}_{fluid}.xlsx", sheet_name="HP_states", float_format="%.5f")


def generate_hp_map(heat_pump, fluid, T_eva_in_ar, T_con_in_ar, n_ar,
                    m_flow_con=0.2, m_flow_eva=1.0, q_flow_nom=None,
                    dT_eva_superheating=5, dT_con_subcooling=0, **kwargs):

    _size = len(n_ar) * len(T_con_in_ar) * len(T_eva_in_ar)
    _counter = 1
    result_shape = (len(n_ar), len(T_con_in_ar), len(T_eva_in_ar))
    _dummy = np.zeros(result_shape)  # Use a copy to avoid overwriting of values of any sort.
    t0 = time.time()

    # Assign the zero matrix to all nd-values.
    cop, Q_con, pi, T_max, carnot_quality, p_1, p_2, T_4, m_flow_ref, T_con_out = \
        _dummy.copy(), _dummy.copy(), _dummy.copy(), _dummy.copy(), _dummy.copy(),\
        _dummy.copy(), _dummy.copy(), _dummy.copy(), _dummy.copy(), _dummy.copy()
    T_3, T_1, A_eva_SH, A_eva_Lat, A_con_SC, A_con_Lat, A_con_SH, con_err_ntu, eva_err_ntu = \
        _dummy.copy(), _dummy.copy(), _dummy.copy(), _dummy.copy(), _dummy.copy(),\
        _dummy.copy(), _dummy.copy(), _dummy.copy(), _dummy.copy()

    for i_T_eva_in, T_eva_in in enumerate(T_eva_in_ar):
        for i_n, n in enumerate(n_ar):
            for i_T_con_in, T_con_in in enumerate(T_con_in_ar):
                fs_state = None
                print(f"Running combination {_counter}/{_size}.")
                try:
                    hp_state = HeatPumpState(n=n,
                                             T_eva_in=T_eva_in,
                                             T_con_in=T_con_in,
                                             m_flow_eva=m_flow_eva,
                                             m_flow_con=m_flow_con,
                                             dT_eva_superheating=dT_eva_superheating,
                                             dT_con_subcooling=dT_con_subcooling)

                    fs_state = heat_pump.calc_steady_state(fluid=fluid, hp_state=hp_state,
                                                           **kwargs)
                except Exception as e:
                    # Avoid loss of data if unexcepted refprop errors occur.
                    print(f"An error occurred: {e}")
                    raise e
                if fs_state is None:
                    fs_state = FlowsheetState()
                cop[i_n][i_T_con_in][i_T_eva_in] = fs_state.COP
                Q_con[i_n][i_T_con_in][i_T_eva_in] = fs_state.Q_con
                T_con_out[i_n][i_T_con_in][i_T_eva_in] = fs_state.T_con_out
                pi[i_n][i_T_con_in][i_T_eva_in] = fs_state.PI
                T_max[i_n][i_T_con_in][i_T_eva_in] = fs_state.T_max
                carnot_quality[i_n][i_T_con_in][i_T_eva_in] = fs_state.carnot_quality
                p_1[i_n][i_T_con_in][i_T_eva_in] = fs_state.p_1
                p_2[i_n][i_T_con_in][i_T_eva_in] = fs_state.p_2
                T_4[i_n][i_T_con_in][i_T_eva_in] = fs_state.T_4
                T_1[i_n][i_T_con_in][i_T_eva_in] = fs_state.T_1
                T_3[i_n][i_T_con_in][i_T_eva_in] = fs_state.T_3
                m_flow_ref[i_n][i_T_con_in][i_T_eva_in] = fs_state.m_flow_ref
                A_eva_SH[i_n][i_T_con_in][i_T_eva_in] = fs_state.A_eva_SH
                A_eva_Lat[i_n][i_T_con_in][i_T_eva_in] = fs_state.A_eva_Lat
                A_con_SC[i_n][i_T_con_in][i_T_eva_in] = fs_state.A_con_SC
                A_con_Lat[i_n][i_T_con_in][i_T_eva_in] = fs_state.A_con_Lat
                A_con_SH[i_n][i_T_con_in][i_T_eva_in] = fs_state.A_con_SH
                con_err_ntu[i_n][i_T_con_in][i_T_eva_in] = fs_state.con_err_ntu
                eva_err_ntu[i_n][i_T_con_in][i_T_eva_in] = fs_state.eva_err_ntu

                _counter += 1
                if _counter == 5:
                    t1 = (time.time() - t0)
                    print(f"First 5 evaluations took {round(t1, 2)} sec.\n"
                          f"The code will be done in ca. {round(_size * t1 / 5 / 60, 2)} min")
    if q_flow_nom is None:
        # Get nominal value:
        rel_info_nominal = nominal_hp_design(heat_pump,
                                             m_flow_con_start=m_flow_con,
                                             m_flow_eva_start=m_flow_eva,
                                             fluid=fluid)
        q_flow_nom = rel_info_nominal["Q_con"]

    data_dict = {"scales": {"n": {"data": n_ar,
                                  "unit": "-",
                                  "comment": "Relative compressor speed"},
                            "T_con_in": {"data": T_con_in_ar,
                                         "unit": "K"},
                            "T_eva_in": {"data": T_eva_in_ar,
                                         "unit": "K"},
                            },
                 "parameters": {"m_flow_con": {"data": m_flow_con,
                                               "unit": "kg/s"},
                                "m_flow_eva": {"data": m_flow_eva,
                                               "unit": "kg/s"},
                                "Q_flow_con_nominal": {"data": q_flow_nom,
                                                       "unit": "W",
                                                       "comment": "Heat flow rate at nominal conditions"}
                                },
                 "nd_data": {"COP": {"data": cop,
                                     "unit": "-"},
                             "Q_con": {"data": Q_con,
                                       "unit": "W",
                                       "comment": "Condenser heat flow"},
                             "PI": {"data": pi,
                                    "unit": "-",
                                    "comment": "Pressure ratio"},
                             "T_max": {"data": T_max,
                                       "unit": "K",
                                       "comment": "Compressor outlet temperature"},
                             "CarnotQuality": {"data": carnot_quality,
                                               "unit": "-"},
                             "p_1": {"data": p_1,
                                     "unit": "Pa",
                                     "comment": "Pressure before compressor"},
                             "p_2": {"data": p_2,
                                     "unit": "Pa",
                                     "comment": "Pressure after compressor"},
                             "T_4": {"data": T_4,
                                     "unit": "K",
                                     "comment": "Refrigerant temperature at evaporator inlet"},
                             "T_3": {"data": T_3,
                                     "unit": "K",
                                     "comment": "Refrigerant temperature at condenser outlet"},
                             "T_1": {"data": T_1,
                                     "unit": "K",
                                     "comment": "Refrigerant temperature at evaporator outlet"},
                             "m_flow_ref": {"data": m_flow_ref,
                                            "unit": "kg/s"},
                             "T_con_out": {"data": T_con_out,
                                           "unit": "K"},
                             "A_eva_SH": {"data": A_eva_SH,
                                          "unit": "m²",
                                          "comment": "Area for SH in evaporator"},
                             "A_eva_Lat": {"data": A_eva_Lat,
                                           "unit": "m²",
                                           "comment": "Area for Lat in evaporator"},
                             "A_con_SH": {"data": A_con_SH,
                                          "unit": "m²",
                                          "comment": "Area for SH in condenser"},
                             "A_con_Lat": {"data": A_con_Lat,
                                           "unit": "m²",
                                           "comment": "Area for Lat in condenser"},
                             "A_con_SC": {"data": A_con_SC,
                                          "unit": "m²",
                                          "comment": "Area for SC in condenser"},
                             "con_err_ntu": {"data": con_err_ntu,
                                             "unit": "%",
                                             "comment": "Error in condenser in percentage"},
                             "eva_err_ntu": {"data": eva_err_ntu,
                                             "unit": "%",
                                             "comment": "Error in evaporator in percentage"},
                             }
                 }
    return data_dict


def nominal_hp_design(heat_pump, **kwargs):
    t0 = time.time()
    # Define nominal values:
    n = kwargs.get("n", 0.8)
    T_eva_in = kwargs.get("T_eva_in", 273.15 - 2)
    T_con_in = kwargs.get("T_con_in", 273.15 + 47)
    dT_eva_superheating = kwargs.get("dT_eva_superheating", 5)
    dT_con_subcooling = kwargs.get("dT_con_subcooling", 0)
    delta_T_con = kwargs.get("delta_T_con", 8)
    delta_T_eva = kwargs.get("delta_T_eva", 5)
    m_flow_con_start = kwargs.get("m_flow_con_start", 0.2)
    m_flow_eva_start = kwargs.get("m_flow_eva_start", 1)
    fluid = kwargs.get("fluid", "Propane")
    cp_con = kwargs.get("cp_con", 4184)
    cp_eva = kwargs.get("cp_eva", 1000)
    accuracy = kwargs.get("accuracy", 0.001)

    # We have to iterate to match the m_flows to the Q_cons:
    m_flow_eva_next = m_flow_eva_start
    m_flow_con_next = m_flow_con_start
    while True:
        # Set values
        m_flow_con = m_flow_con_next
        m_flow_eva = m_flow_eva_next
        hp_state = HeatPumpState(n=n,
                                 T_eva_in=T_eva_in,
                                 T_con_in=T_con_in,
                                 m_flow_eva=m_flow_eva,
                                 m_flow_con=m_flow_con,
                                 dT_eva_superheating=dT_eva_superheating,
                                 dT_con_subcooling=dT_con_subcooling)
        # Get nominal value:
        fs_state = heat_pump.calc_steady_state(fluid=fluid, hp_state=hp_state)
        if fs_state is None:
            raise ValueError("Given configuration is not feasible at nominal point.")

        m_flow_con_next = fs_state.Q_con / (delta_T_con * cp_con)
        m_flow_eva_next = (fs_state.Q_con * (1 - 1 / fs_state.COP)) / (delta_T_eva * cp_eva)
        # Check convergence:
        if abs(m_flow_eva_next - m_flow_eva) < accuracy and abs(m_flow_con-m_flow_con_next) < accuracy:
            break

    rel_info_nominal = {"Q_con": fs_state.Q_con,
                        "COP": fs_state.COP,
                        "m_flow_con": m_flow_con,
                        "m_flow_eva": m_flow_eva,
                        "delta_T_con": delta_T_con,
                        "delta_T_eva": delta_T_eva,
                        "dT_eva_superheating": dT_eva_superheating,
                        "dT_con_subcooling": dT_con_subcooling}
    print(f"Autogeneration of nominal values took {time.time()-t0} seconds")
    print(f'Nominal values:')
    #print("\n".join([f"{key}: {value}" for key, value in rel_info_nominal.items()]))
    print(rel_info_nominal)

    return rel_info_nominal


def save_to_sdf(input_dict, savepath):
    """
    Save given input dictionary to a sdf file in the given savepath


    :param input_dict:
        A dictionary with the following structure:
        Keys Flowsheet_name
        Values: A dictionary with the following structure:
            Keys: Fluids
            Values: Data dictionaries with the following structure:
                Keys: scales, nd_data, parameters
                    scales: Of the given nd_data, e.g. T_Amb, n
                    nd_data: More-dimensional data, e.g. COP
                    parameters: Scalar values, like m_flow_con or similar
                Values:
                    Dict with two keys, "data" and "unit". "data" holds the np.ndarray
                    and "unit" the unit of the data as string, e.g. "K" for Kelvin
    :param str,os.path.normpath savepath:
        Where to store the data
    :param str flowsheet_name:
        Name of the flowsheet. This is the top level group
    :return:
    """
    _all_groups = []

    for flowsheet_name, fluid_dict in input_dict.items():
        _all_fluids = []
        for fluid, fluid_data in fluid_dict.items():
            # First write scales
            _scales = []
            for scale_name, scale_values in fluid_data["scales"].items():
                _scales.append(sdf.Dataset(scale_name,
                                           data=scale_values["data"],
                                           unit=scale_values["unit"],
                                           is_scale=True,
                                           display_name=scale_name,
                                           comment=scale_values.get("comment", "")))
            # Now the ND-Data:
            _nd_data = []
            for data_name, data_values in fluid_data["nd_data"].items():
                _nd_data.append(sdf.Dataset(data_name,
                                            data=data_values["data"],
                                            unit=data_values["unit"],
                                            scales=_scales,
                                            comment=data_values.get("comment", "")))
            # Now the constant parameters
            _paras = []
            for para_name, para_value in fluid_data["parameters"].items():
                _paras.append(sdf.Dataset(para_name,
                                          data=para_value["data"],
                                          unit=para_value["unit"],
                                          comment=para_value.get("comment", "")))

            # Save everything
            fluid_group = sdf.Group(fluid, comment="Values for fluid", datasets=_scales + _nd_data + _paras)
            _all_fluids.append(fluid_group)

        flowsheet_group = sdf.Group(flowsheet_name,
                                    comment="Multiple fluids for the flowsheet",
                                    groups=_all_fluids)
        _all_groups.append(flowsheet_group)

    parent_group = sdf.Group("/", comment="Generated with python script", groups=_all_groups)
    sdf.save(savepath+"\HeatPumpMaps.sdf", group=parent_group)
    return savepath+"\HeatPumpMaps.sdf"


def plot_hp_map(filepath_sdf,
                nd_str,
                first_dim_str,
                sec_dim_str,
                fluids=None,
                flowsheets=None,
                violin_str=None,
                third_dim_str=None
                ):
    """
    Plot the COP of the given HP map for all present fluids
    :param filepath_sdf:
    # TODO: Docstrings
    :return:
    """
    if fluids is None:
        fluids = []
    if flowsheets is None:
        flowsheets = []
    if "T_" in sec_dim_str:
        offset_sec = -273.15
    else:
        offset_sec = 0
    if "T_" in first_dim_str:
        offset_pri = -273.15
    else:
        offset_pri = 0
    offset_thi = 0
    plot_4D = False
    if third_dim_str is not None:
        plot_4D = True
        if "T_" in third_dim_str:
            offset_thi = -273.15

    dataset = sdf.load(filepath_sdf)
    plot_violin = True
    if violin_str is None:
        plot_violin = False
        violin_str = ""
    if plot_violin:
        if flowsheets:
            n_rows = len(flowsheets)
        else:
            n_rows = len(dataset.groups)
        fig_v, ax_v = plt.subplots(nrows=n_rows, ncols=1, sharex=True,
                                   squeeze=False)
        fig_v.suptitle(violin_str)
    i_fs = 0
    nd_str_plot = nd_str
    fac = 1
    if nd_str == "dT_eva_min":
        nd_str = "T_1"
        sub_str = "T_eva_in"
        fac = - 1
    elif nd_str == "dT_con":
        nd_str = "T_3"
        sub_str = "T_con_in"
    elif nd_str == "dT_sh":
        nd_str = "T_1"
        sub_str = "T_4"
    else:
        sub_str = ""

    if nd_str_plot.startswith("T_"):
        offset_nd = -273.15
    else:
        offset_nd = 0

    for flowsheet in dataset.groups:
        violin_data = {}
        if flowsheet.name not in flowsheets and len(flowsheets) > 0:
            continue
        for fluid in flowsheet.groups:
            if fluid.name not in fluids and len(fluids) > 0:
                continue
            nd, fd, sd, sub_data, td = None, None, None, None, None
            _other_scale = {}
            for ds in fluid.datasets:
                if ds.name == nd_str:
                    nd = ds
                elif ds.name == first_dim_str:
                    fd = ds.data
                elif ds.name == sec_dim_str:
                    sd = ds.data
                if ds.name == sub_str:
                    sub_data = ds.data
                if ds.name == violin_str:
                    data = ds.data.flatten()
                    violin_data[fluid.name] = data[~np.isnan(data)]
                if plot_4D and ds.name == third_dim_str:
                    td = ds.data

            if nd is None:
                raise KeyError("nd-String not found in dataset")

            if sub_data is None:
                sub_data = np.zeros(nd.data.shape)

            # Check other scales:
            for i, scale in enumerate(nd.scales):
                if scale.name not in [first_dim_str, sec_dim_str]:
                    _other_scale[i] = scale

            if fd is None or sd is None or not _other_scale or (plot_4D and td is None):
                raise KeyError("One of the given strings was not found in dataset")

            if plot_4D:
                fig = plt.figure()
                figtitle = f"{flowsheet.name}_{fluid.name}_{nd_str_plot}"
                fig.suptitle(figtitle)
                ax = fig.add_subplot(111, projection='3d')
                ax.set_xlabel(first_dim_str)
                ax.set_ylabel(sec_dim_str)
                ax.set_zlabel(third_dim_str)
                fourth_dim = (nd.data - sub_data) * fac + offset_nd
                # Scale values for better sizes of circles:
                bounds = [fourth_dim.min(), fourth_dim.max()]
                _max_circle_size = 30
                fourth_dim_scaled = (fourth_dim - bounds[0]) / (bounds[1] - bounds[0]) * _max_circle_size
                inp = [fd + offset_pri, sd + offset_sec, td + offset_thi]
                import itertools
                scattergrid = np.array([c for c in itertools.product(*inp)])
                ax.scatter(scattergrid[:, 0],
                           scattergrid[:, 1],
                           scattergrid[:, 2],
                           c=fourth_dim_scaled,
                           s=fourth_dim_scaled)
            else:
                for index, scale in _other_scale.items():
                    for idx_data, value in enumerate(scale.data):
                        if index==0:
                            Z = nd.data[idx_data, :, :]
                            if sub_str in ["T_4", ""]:
                                sub_data_use = sub_data[idx_data, :, :]
                            else:
                                sub_data_use = sub_data
                        elif index == 1:
                            Z = nd.data[:, idx_data, :]
                            if sub_str in ["T_4", ""]:
                                sub_data_use = sub_data[:, idx_data, :]
                            else:
                                sub_data_use = sub_data
                        else:
                            Z = nd.data[:, :, idx_data]
                            if sub_str in ["T_4", ""]:
                                sub_data_use = sub_data[:, :, idx_data]
                            else:
                                sub_data_use = sub_data
                        if not plot_violin:
                            fig = plt.figure()
                            figtitle = f"{flowsheet.name}_{fluid.name}_{nd_str_plot}_{scale.name}={round(value, 3)}"
                            fig.suptitle(figtitle)
                            ax = fig.add_subplot(111, projection='3d')
                            ax.set_xlabel(first_dim_str)
                            ax.set_ylabel(sec_dim_str)
                            X, Y = np.meshgrid(fd, sd)
                            ax.plot_surface(X + offset_pri, Y + offset_sec, (Z - sub_data_use)*fac + offset_nd)

        if plot_violin:
            for key, value in violin_data.items():
                print(f"{violin_str}: {flowsheet.name}_{key}")
                print(f"Min: {np.min(value)}")
                print(f"Max: {np.max(value)}")
                print(f"Mean: {np.mean(value)}")
                print(f"Median: {np.median(value)}\n")
            ax_v[i_fs][0].violinplot(
                                     list(violin_data.values()),
                                     showextrema=True,
                                     showmeans=True,
                                     showmedians=True
                                     )
            set_axis_style(ax_v[i_fs][0], list(violin_data.keys()))
            ax_v[i_fs][0].set_ylabel(flowsheet.name.replace("Flowsheet", ""))
        i_fs += 1
    plt.show()


def set_axis_style(ax, labels):
    """From: https://matplotlib.org/3.1.1/gallery/statistics/customized_violin.html#sphx-glr-gallery-statistics-customized-violin-py"""
    ax.get_xaxis().set_tick_params(direction='out')
    ax.xaxis.set_ticks_position('bottom')
    ax.set_xticks(np.arange(1, len(labels) + 1))
    ax.set_xticklabels(labels)
    ax.set_xlim(0.25, len(labels) + 0.75)


def merge_sdfs(filepaths, savepath):
    """
    Merge given files and return a merged file.
    Be carefule if both files contain the same combination.
    Then, the latter element of the list will overwrite the first one.

    :param list filepathss:
        List with paths to the files
    :param str filepath:
        Str to the new file
    """
    _all_flowsheets = {}
    # Merge to structure
    for fpath in filepaths:
        dataset = sdf.load(fpath)
        for flowsheet in dataset.groups:
            fluid_data = {fluid.name: fluid for fluid in flowsheet.groups}
            if flowsheet.name not in _all_flowsheets:
                _all_flowsheets.update({flowsheet.name: fluid_data})
            else:
                _all_flowsheets[flowsheet.name].update(fluid_data)

    # Write structure
    _all_groups = []
    for flowsheet_name, fluid_dict in _all_flowsheets.items():
        _all_fluids = []
        for fluid, data in fluid_dict.items():
            _all_fluids.append(data)
        flowsheet_group = sdf.Group(flowsheet_name,
                                    comment="Multiple fluids for the flowsheet",
                                    groups=_all_fluids)
        _all_groups.append(flowsheet_group)

    parent_group = sdf.Group("/", comment="Generated with python script", groups=_all_groups)
    sdf.save(savepath, group=parent_group)
    return savepath
