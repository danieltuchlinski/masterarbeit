# Importing all relevant packages
import pathlib
import matplotlib.pyplot as plt
# Imports from ebcpy
from ebcpy import DymolaAPI, TimeSeriesData


def main(
    file_dir= "D:\sgo-kwa\Repos\optisim\calibration\heatpumpcalibration",
        # Path to all packages used for this task
    AixLib_mo= "D:\sgo-kwa\Repos\optisim\submodules\AixLib\AixLib\package.mo",
    BESMod_mo= "D:\sgo-kwa\Repos\optisim\submodules\BESMod\BESMod\package.mo",
    Buildings_mo= "D:\sgo-kwa\Repos\optisim\submodules\Buildings\Buildings\package.mo",
    IBPSA_mo= "D:\sgo-kwa\Repos\optisim\submodules\IBPSA\IBPSA\package.mo",
    Teaser_mo= "D:\sgo-kwa\Repos\optisim\submodules\teaserweb_AixLib\package.mo",
    MPC_mo="D:\sgo-kwa\Repos\optisim\models\MPC\package.mo", 

          
        # Storage path (below)
        cd=None,
        with_plot=True
):
    """
    Arguments of this example:

    :param [pathlib.Path, str] examples_dir:
        Path to the examples folder of AixCaliBuHA
    :param str aixlib_mo:
        Path to the package.mo of the AixLib.
        This example was tested for AixLib version 1.0.0.
    :param str cd:
        Path in which to store the output.
        Default is the \results folder
    :param bool with_plot:
        Show the plot at the end of the script. Default is True.
    """
    # General settings, saves the results into the HeatPumpCalibration\results folder 
    if cd is None:
        cd = pathlib.Path(file_dir).joinpath("results")
    #For another storage location, enter the path below (for "cd") and also change the following lines
    else:
        cd = pathlib.Path(cd)
    file_dir = pathlib.Path(file_dir)

    # Loading the model including the needed packages 
    dym_api = DymolaAPI(
        model_name="MPC.HeatPump_Calibration",
        cd=cd,
        packages=[
            AixLib_mo,
            IBPSA_mo,
            Buildings_mo,
            BESMod_mo,
            Teaser_mo,
            MPC_mo,
            
        ],
        show_window=True,
        equidistant_output=False
    )
    print("Pausing for analysis. Set the break point here if you like!")

    # Simulation
    dym_api.set_sim_setup({
        "stop_time": 1800,  # equals 2 hours in sec
        "output_interval": 1
    })
    file_path = dym_api.simulate(
        return_option="savepath"
    )

    # ## Data analysis
    # Now let's analyze the data we've generated.
    # Open the file first and extract variables of interest.
    # As the model concerns a heat pump, we are most interested in
    # variables relevant for heat pumps. This could be the
    # electrical power consumption (Pel) and room comfort (TAir)
    # supplied by the heat pump.
    # When looking at the model, we have one input:
    # TDryBulSource.y, which represents the outdoor air temperature.
    # This input is important for heat pumps, as efficiency mainly depends
    # on source temperatures, and the model is of an air source heat pump.
    # We thus also extract this input for our data analysis.
    tsd = TimeSeriesData(file_path)
    # Pel="hydraulic.generation.heatPump.innerCycle.Pel",
    # TConOut="hydraulic.generation.heatPump.senT_b1.T",
    # TEvaIn="hydraulic.generation.heatPump.senT_a2.T"

    tsd = tsd[["heatPump.innerCycle.Pel","heatPump.senT_b1.T"]]

    # Let's check the frequency of the data to see if our 1 s sampling rate
    # was correctly applied:
    print("Simulation had index-frequency of %s with "
          "standard deviation of %s" % tsd.frequency)

    data_dir = pathlib.Path(file_dir).joinpath("data")
    mtd = TimeSeriesData(data_dir.joinpath("C_30to55_55_12.csv"))
    
    # Map the measured keys to the names inside your simulation
    variable_names = {
        # Name of goal: Name of measured variable, Name of simulated variable
        # Either use list
        "Pel": ["StateMachine_Pel_Inv", "heatPump.innerCycle.Pel"],
        "Outlet temperature condensator": ["IndoorUnit_T_FlowOutlet_IDU","heatPump.senT_b1.T"],
    }
    # Due to state events (see Modelica help for more info on that),
    # our data is not equally sampled.
    # To later match the simulation data with a fixed output interval (parameter output_interval),
    # we have to process the data further.
    
    # To do this, we have the function 'clean_and_space_equally' in ebcpy's TimeSeriesData.
    # It only works on datetime indexes, hence we convert the data first:
    # Note: Real measured data would already contain DateTimeIndex anyways.
    
    mtd.to_float_index()
    print(tsd)
    print(mtd)
    # Save a copy to check if our resampling induces data loss:
    tsd_reference = tsd.copy()
    # Apply the function
    # tsd.clean_and_space_equally(desired_freq="1s")
    print("Simulation now has index-frequency of %s with "
          "standard deviation of %s" % tsd.frequency)

    # Let's check if the sampling changed our measured data and
    # plot all relevant variable to analyze our system:
    fig, ax = plt.subplots(2, 1, sharex=True)
    # ax[0].plot(tsd_reference['heatPump.senT_b1.T'] - 273.15, color="blue", label="Reference")
    ax[0].plot(tsd['heatPump.senT_b1.T'] - 273.15, color="blue", label="Resampled")
    ax[0].plot(mtd['IndoorUnit_T_FlowOutlet_IDU'] - 273.15, color="red", label="Resampled")
    ax[0].set_ylabel("$T_\mathrm{Inlet,Air}$ in °C")
    # ax[1].plot(tsd_reference['heatPump.innerCycle.Pel'] / 1000, color="blue", label="Reference")
    ax[1].plot(tsd['heatPump.innerCycle.Pel'] / 1000, color="blue", label="Resampled")
    ax[1].plot(mtd['StateMachine_Pel_Inv'] / 1000, color="red", label="Resampled")
    ax[1].set_ylabel("$P_\mathrm{el}$ in kW")
    ax[1].set_xlabel("Time in s")
    plt.legend()
    if with_plot:
        plt.show()


    # ## Data saving
    # In order to use this data in the other examples for the calibration, we have to save it.

    tsd_measurements = tsd[["heatPump.innerCycle.Pel", "heatPump.senT_b1.T"]]
    tsd_measurements.save(file_dir.joinpath("data", "measured_target_data.hdf"), key="example")
    print("Saved data under", file_dir.joinpath("data"))


if __name__ == '__main__':
    # TODO-User: Change the AixLib path!
    main(
        AixLib_mo=r"D:\sgo-kwa\Repos\optisim\submodules\AixLib\AixLib\package.mo",
        BESMod_mo=r"D:\sgo-kwa\Repos\optisim\submodules\BESMod\BESMod\package.mo",
        Buildings_mo=r"D:\sgo-kwa\Repos\optisim\submodules\Buildings\Buildings\package.mo",
        IBPSA_mo=r"D:\sgo-kwa\Repos\optisim\submodules\IBPSA\IBPSA\package.mo",
        Teaser_mo= r"D:\sgo-kwa\Repos\optisim\submodules\teaserweb_AixLib\package.mo",
        MPC_mo=r"D:\sgo-kwa\Repos\optisim\models\MPC\package.mo", 

        file_dir=pathlib.Path(__file__).parent
    )
