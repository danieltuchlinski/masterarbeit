
# Start by importing all relevant packages
import pathlib
import pandas as pd
import numpy as np
import statistics
import matplotlib.pyplot as plt
# Imports from ebcpy
from ebcpy import FMU_API, TimeSeriesData, DymolaAPI
from ebcpy.utils.conversion import convert_tsd_to_modelica_txt

def main(

        file_dir= "D:\sgo-kwa\Repos\ParaStudy",
        aixlib_mo="D:\sgo-kwa\Repos\optisim\submodules\AixLib\AixLib\package.mo",
        BESMod_mo= "D:\sgo-kwa\Repos\optisim\submodules\BESMod\BESMod\package.mo",
        Buildings_mo= "D:\sgo-kwa\Repos\optisim\submodules\Buildings\Buildings\package.mo",
        IBPSA_mo= "D:\sgo-kwa\Repos\optisim\submodules\IBPSA\IBPSA\package.mo",
        #Teaser_mo= "D:\sgo-kwa\Repos\optisim\submodules\teaserweb_AixLib\package.mo",
        MPC_mo="D:\sgo-kwa\Repos\optisim\models\MPC\package.mo", 
        PCM_mo="D:\sgo-kwa\Repos\ebc0888_mitsubishi_pcmgoeshil_kap\Modelica\PCMgoesHIL\package.mo",
        cd=None,
        log_fmu=True,
        output_interval=10,
        with_plot=True
):
       
    # General settings
    if cd is None:
        cd = pathlib.Path(file_dir).joinpath("parameter_study_dym", "Typtag_12")

    # ######################### Simulation API Instantiation ##########################
    # %% Setup the API:

    dym_api = DymolaAPI(
        model_name= "MPC.MA_Kevin_Model_Sim_Rad",
        cd=cd,
        packages=[aixlib_mo, Buildings_mo, IBPSA_mo, BESMod_mo, MPC_mo, PCM_mo],
        show_window=True,
        n_restart=-1,
        equidistant_output=False,
        # get_structural_parameters=True
    )
      
    # ######################### Simulation Setup Part ##########################
    
    simstart=0

    # Jede Simulation braucht 9 Tage vorlauf
    # Sprich wird der 11.1 simuliert, muss vom 2.(1*86400) bis zum 11. (11*86400) simuliert werden.
    # Danach wird das zu betrachtende Intervall extrahiert (864000-86400*11)


    # Typtag 0 (11. Jan)
    # Simstart: 86400
    # Simende: 950400

    # Typtag 6 (15. Mar)
    # Simstart: 5616000
    # Simende:  6480000

    # Typtag 12 (15. Oct)
    # Simstart: 24019200 
    # Simende: 24883200

    # Typtag A (5. Sep)
    # Simstart:  20563200
    # Simende:   21427200

    # Typtag B (18. Oct)
    # Simstart: 24278400
    # Simende:  25142400



    # Or pass a dictionary. This makes using configs (toml, json) much easier
    simulation_setup = {"start_time": 24278400,
                        "stop_time":  25142400,
                        "output_interval": output_interval}
    dym_api.set_sim_setup(sim_setup=simulation_setup)

    # ######################### Parameters ##########################
   
    #('hydraulic.control.monovalentControlParas.k', 3, (0.01, 20)),
    #('hydraulic.control.monovalentControlParas.T_I', 500, (100, 10000)),
    #('hydraulic.control.monovalentControlParas.dTHysBui', 6, (2, 12)),

    # Wer#te der Parameter
    PI_k = dym_api.parameters['hydraulic.control.monovalentControlParas.k'].value
    PI_Ti = dym_api.parameters['hydraulic.control.monovalentControlParas.T_I'].value
    HYS = dym_api.parameters['hydraulic.control.monovalentControlParas.dTHysBui'].value

    parameter_names=["PI_k", "PI_Ti", "HYS"]
    parameter_names_plot=["$K_\mathrm{p}$", "$T_\mathrm{n}$", "$T_\mathrm{hys}$"]

    #parameter_names=["PI_Tn", "HYS", "PI_k"]
    #parameter_names_plot=["$T_\mathrm{n}$", "$K_\mathrm{p}$", "$T_\mathrm{hys}$"]

    #parameter_names=["HYS", "PI_k", "PI_Ti"]
    #parameter_names_plot=["$T_\mathrm{hys}$", "$K_\mathrm{p}$", "$T_\mathrm{n}$"]

    # Alteration
    # Startwerte festlegen!
   
   
    # values_PI_k=[0.01, 0.05, 0.1, 0.25, 0.5, 0.75, 1, 2.5, 5, 7.5, 10, 12.5, 15, 17.5, 20] # 1
    values_PI_k=[0.00001, 0.0001, 0.0005, 0.001, 0.005, 0.01, 0.05, 0.1, 0.25, 0.5, 0.75, 1, 2.5, 5, 7.5, 10, 12.5, 15, 17.5, 20]

    # values_PI_Ti=[10, 50, 100, 150, 250, 500, 750, 1000, 1250, 1500, 1750, 2000, 2500, 5000, 10000] # 1000
    values_PI_Ti=[10, 50, 100, 150, 250, 500, 750, 1000, 1250, 1500, 1750, 2000, 2500, 5000, 10000, 15000, 20000, 25000, 35000, 50000]

    values_HYS=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10 , 11, 12]  # 8

    #print(values_PI_k)
    #print(values_PI_Ti)   
    #print(values_HYS)  

    parameters_PI_k = []
    parameters_PI_Ti = []
    parameters_HYS = []
  
    sim_parameters=[]
    sim_parameters_values=[]
    
    for value_k  in values_PI_k:
        parameters_PI_k.append({"hydraulic.control.monovalentControlParas.k": value_k})
       

    for value_t in  values_PI_Ti:
        parameters_PI_Ti.append({"hydraulic.control.monovalentControlParas.T_I": value_t})
        

    for value_hys in values_HYS:
        parameters_HYS.append({"hydraulic.control.monovalentControlParas.dTHysBui": value_hys })
       
    # Parameterliste für die Simulation
    sim_parameters.append(parameters_PI_k)
    #sim_parameters.append(parameters_PI_Ti)
    #sim_parameters.append(parameters_HYS)
      
    sim_parameters_values.append(values_PI_k)
    #sim_parameters_values.append(values_PI_Ti)
    #sim_parameters_values.append(values_HYS)

    print(sim_parameters)

    # ######################### Results to store ##########################
       
    dym_api.result_names = [ 

                            "hydraulic.generation.heatPump.senT_b1.T",
                            "weaDat.weaBus.TDryBul", 
                            "building.thermalZone[1].TAir",
                            'hydraulic.generation.heatPump.senT_a1.T',
                            'hydraulic.generation.heatPump.senT_b2.T',

                            'hydraulic.generation.heatPump.innerCycle.Pel',
                            'hydraulic.generation.heatPump.innerCycle.QCon',
                            'hydraulic.generation.heatPump.innerCycle.QEva',

                            'hydraulic.control.HP_nSet_Controller.n_Set',
                            'hydraulic.control.safetyControl.nOut',
                            'hydraulic.control.HP_nSet_Controller.T_Set',
                            'hydraulic.control.HP_nSet_Controller.T_Meas',
                            'hydraulic.control.sigBusGen.hp_bus.nSet',
                            'hydraulic.control.sigBusGen.hp_bus.onOffMea',
                            'hydraulic.transfer.pressureReliefValveOnOff.m_flow',
                            'hydraulic.transfer.val[1].m_flow',
                            'hydraulic.transfer.panelHeating[1].TReturn.T'
                            #'hydraulic.transfer.senTFlowEach[1].T'
                            ]

    # print("Results that will be stored", dym_api.result_names)
    
    # ######################### Execute simulation ##########################
    # Pass the created list to the simulate function
    
    # Saving data for the averages 
    res_td=({'Parameter':[], 'Wert':[], 'Pel':[],'Q_con':[], "TRL":[], "TVL":[], "TEvaOut":[],"T_room":[], "TAir":[], "TVL_m":[], "TVL_s":[], "T_ra":[], "COP":[], "OnOFF":[]})
    res_avg = pd.DataFrame(res_td)

    index_numbers=np.arange(0,8641,1)  
    data_time={'Time': index_numbers}
    
    dataframe_cop = pd.DataFrame(data_time)
    dataframe_pel = pd.DataFrame(data_time)
    dataframe_tset = pd.DataFrame(data_time)
    dataframe_tvl = pd.DataFrame(data_time)
    dataframe_tra = pd.DataFrame(data_time)
    dataframe_traum = pd.DataFrame(data_time)
   
   
    
    for sim_paras, value, name, plotname in zip(sim_parameters, sim_parameters_values, parameter_names, parameter_names_plot):
        results = dym_api.simulate(parameters=sim_paras,
                              )

        

        # ######################### Visualization ##########################
        # Tables and plots

        for res, parameter, index in zip(results, sim_paras, value):
            
            """""
            tsd = TimeSeriesData(res)
            tsd = tsd[[
                            "hydraulic.generation.heatPump.senT_b1.T",
                            "weaDat.weaBus.TDryBul", 
                            "building.thermalZone[1].TAir",
                            'hydraulic.generation.heatPump.senT_a1.T',
                            'hydraulic.generation.heatPump.senT_b2.T',

                            'hydraulic.generation.heatPump.innerCycle.Pel',
                            'hydraulic.generation.heatPump.innerCycle.QCon',
                            'hydraulic.generation.heatPump.innerCycle.QEva',

                            'hydraulic.control.HP_nSet_Controller.n_Set',
                            'hydraulic.control.HP_nSet_Controller.T_Set',
                            'hydraulic.control.HP_nSet_Controller.T_Meas',
                            'hydraulic.control.sigBusGen.hp_bus.nSet',
                            'hydraulic.control.safetyControl.nOut',
                            'hydraulic.control.sigBusGen.hp_bus.onOffMea',
                            'hydraulic.transfer.pressureReliefValveOnOff.m_flow',
                            'hydraulic.transfer.val[1].m_flow',
                            'hydraulic.transfer.panelHeating[1].TReturn.T'
                            #'hydraulic.transfer.senTFlowEach[1].T'
                            ]]

            """""                
           
            tsd2 = TimeSeriesData(res)
            tsd2 = tsd2[[
                            "hydraulic.generation.heatPump.senT_b1.T",
                            "weaDat.weaBus.TDryBul", 
                            "building.thermalZone[1].TAir",
                            'hydraulic.generation.heatPump.senT_a1.T',
                            'hydraulic.generation.heatPump.senT_b2.T',

                            'hydraulic.generation.heatPump.innerCycle.Pel',
                            'hydraulic.generation.heatPump.innerCycle.QCon',
                            'hydraulic.generation.heatPump.innerCycle.QEva',

                            'hydraulic.control.HP_nSet_Controller.n_Set',
                            'hydraulic.control.safetyControl.nOut',
                            'hydraulic.control.HP_nSet_Controller.T_Set',
                            'hydraulic.control.HP_nSet_Controller.T_Meas',
                            'hydraulic.control.sigBusGen.hp_bus.nSet',
                            'hydraulic.control.sigBusGen.hp_bus.onOffMea',
                            'hydraulic.transfer.pressureReliefValveOnOff.m_flow',
                            'hydraulic.transfer.val[1].m_flow',
                            'hydraulic.transfer.panelHeating[1].TReturn.T'
                            #'hydraulic.transfer.senTFlowEach[1].T'
                            ]]

            tsd2.to_datetime_index()
            tsd2.clean_and_space_equally(desired_freq="10s")

            data = pd.DataFrame(tsd2)
            data = data.iloc[9*8640:10*8640+1]
            data.to_excel(pathlib.Path(file_dir).joinpath("parameter_study_dym", "Typtag_12", "Results","Study_"+str(name)+"_"+str(index)+'.xlsx'))

            
            on = pd.read_excel(r"D:\sgo-kwa\Repos\ParaStudy\parameter_study_dym\Typtag_12\Results\Study_"+str(name)+r"_"+str(index)+r".xlsx", 'Sheet1', skiprows=[1,2])
            tsd = pd.read_excel(r"D:\sgo-kwa\Repos\ParaStudy\parameter_study_dym\Typtag_12\Results\Study_"+str(name)+r"_"+str(index)+r".xlsx", 'Sheet1', skiprows=[1,2])
            counter=np.diff((on['hydraulic.control.sigBusGen.hp_bus.onOffMea'])!=0).sum()/2
            counter=int(counter)
            
            # average data
            pel_avg=tsd['hydraulic.generation.heatPump.innerCycle.Pel'].mean().item()
            q_con_avg=tsd['hydraulic.generation.heatPump.innerCycle.QCon'].mean().item()
            t_con_out_avg=tsd['hydraulic.generation.heatPump.senT_b1.T'].mean().item()
            t_outdoor_avg=tsd['weaDat.weaBus.TDryBul'].mean().item()
            t_con_in_avg=tsd['hydraulic.generation.heatPump.senT_a1.T'].mean().item()
            t_eva_out_avg=tsd['hydraulic.generation.heatPump.senT_b2.T'].mean().item()
            t_vl_set_avg=tsd['hydraulic.control.HP_nSet_Controller.T_Set'].mean().item()
            t_vl_meas_avg=tsd['hydraulic.control.HP_nSet_Controller.T_Meas'].mean().item()
            t_room=tsd['building.thermalZone[1].TAir'].mean().item()
            t_ra_avg=(tsd['hydraulic.control.HP_nSet_Controller.T_Set']-tsd['hydraulic.control.HP_nSet_Controller.T_Meas']).mean().item()
            cop_avg=(tsd['hydraulic.generation.heatPump.innerCycle.QCon']/tsd['hydraulic.generation.heatPump.innerCycle.Pel']).mean().item()

            #res_td = df_marks.append(new_row, ignore_index=True)
            
            res_new_avg= ({'Parameter':str(name), 'Wert':index, 'Pel':pel_avg,'Q_con':q_con_avg, "TRL":t_con_in_avg, "TVL":t_con_out_avg, "TEvaOut":t_eva_out_avg, "T_room": t_room, "TAir":t_outdoor_avg, "TVL_m":t_vl_meas_avg, "TVL_s":t_vl_set_avg, "T_ra":t_ra_avg, "COP":cop_avg, "OnOFF":counter})
            res_avg= res_avg.append(res_new_avg, ignore_index=True)

            # Generate Boxplot data          
           
            new_data_tvl=on['hydraulic.generation.heatPump.senT_b1.T'].tolist()  
            dataframe_tvl[str(name)+'_'+str(index)]=new_data_tvl
           
            new_data_cop=on['hydraulic.generation.heatPump.innerCycle.QCon']/on['hydraulic.generation.heatPump.innerCycle.Pel'].tolist()
            dataframe_cop[str(name)+'_'+str(index)]=new_data_cop

            new_data_tset=on['hydraulic.control.HP_nSet_Controller.T_Set'].tolist()  
            dataframe_tset[str(name)+'_'+str(index)]=new_data_tset

            new_data_traum=on['building.thermalZone[1].TAir'].tolist()  
            dataframe_traum[str(name)+'_'+str(index)]=new_data_traum

            new_data_tra=on['hydraulic.control.HP_nSet_Controller.T_Set']-on['hydraulic.control.HP_nSet_Controller.T_Meas'].tolist()  
            dataframe_tra[str(name)+'_'+str(index)]=new_data_tra

            new_data_pel=on['hydraulic.generation.heatPump.innerCycle.Pel'].tolist()  
            dataframe_pel[str(name)+'_'+str(index)]=new_data_pel
           

            ########## Plotting-TNR ##########
            plt.rcParams["font.family"] = "Times New Roman"

            # Plot 1 (groß)

            fig, ax = plt.subplots(2, 1, sharex=True, gridspec_kw={'height_ratios': [4, 1]})

            # first plot
            ax[0].plot(tsd['hydraulic.generation.heatPump.senT_b1.T'] -273.15, color="red", label="T$_\mathregular{VL}$", linewidth=1)
            ax[0].plot(tsd['hydraulic.control.HP_nSet_Controller.T_Set'] -273.15, color="blue", linestyle='--', label="T$_\mathregular{Set}$", linewidth=1)
            ax[0].plot(tsd['weaDat.weaBus.TDryBul'] -273.15, color="black", label="T$_\mathregular{U}$", linewidth=1,  linestyle='-.')
            ax[0].plot(tsd['building.thermalZone[1].TAir'] -273.15, color="green", label="T$_\mathregular{Luft,Raum}$", linewidth=1)
            
            ax[0].set_ylabel("Temperatur [°C]", fontsize=11, labelpad=5)
            ax[0].tick_params(axis="y",direction="in", length=0, pad=5)
            ax[1].set_xlabel("Uhrzeit", fontsize=11, labelpad=5)
            for axis in ['top','bottom','left','right']:
                ax[0].spines[axis].set_linewidth(1)
                ax[1].spines[axis].set_linewidth(1)
            y_1 = [-10,0,10,20,30,40,50,60,70]
            y_1_labels= [-10,0,10,20,30,40,50,60,70]
            ax[0].set_yticks(y_1)
            ax[0].set_yticklabels(y_1_labels, fontsize=11)
            # ax[0].yaxis.set_ticks(np.arange(min(y_1), max(y_1)+1, 10))
            # ax[0].set_yticklabels(weight='bold')
            ax[0].text(0.1, 0.91,str(plotname)+'='+str(index), ha='center', va='center', bbox= dict(facecolor = 'white', alpha = 1), transform=ax[0].transAxes, fontsize=8)
            ax[0].text(0.11, 0.33,'Zyklen='+str(counter), ha='center', va='center', bbox= dict(facecolor = 'white', alpha = 1), transform=ax[0].transAxes, fontsize=8)
            # ax[0].text(0.1, 0.93,str(plotname)+'='+str(index), ha='center', va='center', bbox= dict(facecolor = 'white', alpha = 0.75), transform=ax.transAxes)
                

            # twin object for two different y-axis on the sample plot

            ax2=ax[0].twinx()
            ax2.plot(tsd['hydraulic.generation.heatPump.innerCycle.Pel']/1000, color="orange", label="P$_\mathregular{el,Verd}$", linewidth=1)
            
            ax2.set_ylabel("Leistung [kW]", fontsize=11, labelpad=8)
            ax2.tick_params(axis="y",direction="in", length=0, pad=5)
            y_2 = [0,3,6,9,12,15,18,21,24]
            y_2_labels= [0,3,6,9,12,15,18,21,24]
            ax2.set_yticks(y_2)
            ax2.set_yticklabels(y_2_labels, fontsize=11)
            # ax2.yaxis.set_ticks(np.arange(min(y_2), max(y_2)+1, 3))
            # ax2.set_yticklabels(weight='bold')
            
            # legend and others
            leg=fig.legend(loc="upper center", bbox_to_anchor=(0.5, 1.3), ncol=5, bbox_transform=ax[0].transAxes, fontsize=11)
            leg.get_frame().set_linewidth(0.0)
            ax[0].grid(color='black',axis='both', linewidth=0.5)
            fig.set_size_inches(5.11, 2.75)

            # Second Plot
            ax[1].plot(tsd['hydraulic.transfer.val[1].m_flow'], color="purple", label="Transfermassenstrom", linewidth=1)
            y_3=[-0.1, 0, 0.5, 0.6]
            y_3_labels= ['','0','0.5','']
            ax[1].set_yticks(y_3)
            ax[1].set_yticklabels(y_3_labels, fontsize=11)
            ax[1].set_ylabel("[kg/s]", fontsize=11, labelpad=5)
            ax[1].tick_params(axis="y",direction="in", length=2, pad=5)
            leg2=ax[1].legend(loc="upper center", bbox_to_anchor=(0.5,1.65), bbox_transform=ax[1].transAxes, fontsize=9, framealpha=0)
            leg2.get_frame().set_linewidth(0.0)

            # Second y axis 
            #ax3=ax[1].twinx()
            #ax3.plot(tsd['hydraulic.control.safetyControl.nOut'], color="black", label="Relative Verdichterdrehzahl", linewidth=1, linestyle='-')
            #leg3=ax3.legend(loc="upper center", bbox_to_anchor=(0.62,1.65), bbox_transform=ax[1].transAxes, fontsize=9, framealpha=0)
            #leg3.get_frame().set_linewidth(0.0)
            #ax3.tick_params(axis="y",direction="in", length=2, pad=10)
            #y_4 = [-0.2, 0, 1, 1.2]
            #ax3.yaxis.set_ticks(y_4)
            #y_4_labels= ['','0','1','']
            #ax3.set_yticklabels(y_4_labels, fontsize=11)

            ax[0].tick_params(axis="x",direction="in", length=0, pad=5)
            ax[1].tick_params(axis="x",direction="in", length=2, pad=5)
            ax[0].set_xticks([0+simstart, 1080+simstart, 2160+simstart, 3240+simstart, 4320+simstart, 5400+simstart, 6480+simstart, 7560+simstart, 8640+simstart]) # simulation time
            ax[1].set_xticklabels(['0:00', '3:00', '6:00', '9:00', '12:00', '15:00', '18:00', '21:00', '24:00'], fontsize=11)
            ax[0].margins(x=0)
            ax[1].margins(x=0)

            ########## Saving ##########
            
            fig.savefig(r'D:\sgo-kwa\Repos\ParaStudy\parameter_study_dym\Typtag_12\Plots\Day\Big\Study_'+str(name)+r'_'+str(index)+r'.png',
                    format='png',
                    dpi=1200,
                    bbox_inches='tight'
                    )

            plt.close(fig)



            # Plot 2 (klein)
            fig, ax = plt.subplots(2, 1, sharex=True, gridspec_kw={'height_ratios': [4, 1]})

            # first plot
            ax[0].plot(tsd['hydraulic.generation.heatPump.senT_b1.T'] -273.15, color="red", label="T$_\mathregular{VL}$", linewidth=1)
            ax[0].plot(tsd['hydraulic.control.HP_nSet_Controller.T_Set'] -273.15, color="blue", linestyle='--', label="T$_\mathregular{Set}$", linewidth=0.75)

            ax[0].set_ylabel("Temperatur [°C]", fontsize=11, labelpad=5)
            ax[0].tick_params(axis="y",direction="in", length=0, pad=5)
            ax[1].set_xlabel("Uhrzeit", fontsize=11, labelpad=5)
            for axis in ['top','bottom','left','right']:
                ax[0].spines[axis].set_linewidth(1)
                ax[1].spines[axis].set_linewidth(1)
            y_1 = [15, 25, 35, 45, 55, 65,75]
            y_1_labels= [15, 25, 35, 45, 55, 65,75]
            ax[0].set_yticks(y_1)
            ax[0].set_yticklabels(y_1_labels, fontsize=11)
            # ax[0].yaxis.set_ticks(np.arange(min(y_1), max(y_1)+1, 10))
            # ax[0].set_yticklabels(weight='bold')
            ax[0].text(0.14, 0.895,str(plotname)+'='+str(index), ha='center', va='center', bbox= dict(facecolor = 'white', alpha = 1, linewidth=0.75), transform=ax[0].transAxes, fontsize=8)
            ax[0].text(0.85, 0.895,'Zyklen='+str(counter), ha='center', va='center', bbox= dict(facecolor = 'white', alpha = 1, linewidth=0.75), transform=ax[0].transAxes, fontsize=8)
            # ax[0].text(0.1, 0.93,str(plotname)+'='+str(index), ha='center', va='center', bbox= dict(facecolor = 'white', alpha = 0.75), transform=ax.transAxes)
                

            # twin object for two different y-axis on the sample plot

            ax2=ax[0].twinx()
            ax2.plot(tsd['weaDat.weaBus.TDryBul'] -273.15, color="black", label="T$_\mathregular{U}$", linewidth=1,  linestyle='-.')
            
            ax2.set_ylabel("Lufttemperatur [C°]", fontsize=11, labelpad=5)
            ax2.tick_params(axis="y",direction="in", length=0, pad=5)
            y_2 = [-5,0,5,10,15,20,25]
            y_2_labels= [-5,0,5,10,15,20,25]
            ax2.set_yticks(y_2)
            ax2.set_yticklabels(y_2_labels, fontsize=11)
            # ax2.yaxis.set_ticks(np.arange(min(y_2), max(y_2)+1, 3))
            # ax2.set_yticklabels(weight='bold')
            
            # legend and others
            #font = font_manager.FontProperties(family='Times New Roman',
            #                              
            #                             size=7)
            leg=fig.legend(loc="upper center", bbox_to_anchor=(0.5, 1.25), ncol=5, bbox_transform=ax[0].transAxes, fontsize=10)
            leg.get_frame().set_linewidth(0.0)
            ax[0].grid(color='black',axis='both', linewidth=0.3)
            fig.set_size_inches(2.95, 2.36)

            '''''
            # Second Plot
            ax[1].plot(tsd['hydraulic.control.sigBusGen.hp_bus.onOffMea'], color="black", label="An/Aus", linewidth=0.5)
            y_3=[-0.2, 0, 1, 1.2]
            y_3_labels= ['','Aus','An','']
            ax[1].set_yticks(y_3)
            ax[1].set_yticklabels(y_3_labels, fontsize=10)
            ax[1].tick_params(axis="y",direction="in", length=2, pad=5)
            leg2=ax[1].legend(loc="upper center", bbox_to_anchor=(0.15,1.66), bbox_transform=ax[1].transAxes, fontsize=8, framealpha=0)
            leg2.get_frame().set_linewidth(0.0)

            # Second y axis 
            ax3=ax[1].twinx()
            ax3.plot(tsd['hydraulic.control.sigBusGen.hp_bus.nSet'], color="black", label="Relative Verdichterdrehzahl", linewidth=0.5, linestyle='--')
            leg3=ax3.legend(loc="upper center", bbox_to_anchor=(0.66,1.66), bbox_transform=ax[1].transAxes, fontsize=8, framealpha=0)
            leg3.get_frame().set_linewidth(0.0)
            ax3.tick_params(axis="y",direction="in", length=2, pad=5)
            y_4 = [-0.2, 0, 1, 1.2]
            ax3.yaxis.set_ticks(y_4)
            y_4_labels= ['','0','1','']
            ax3.set_yticklabels(y_4_labels, fontsize=10)
            '''
            ax[1].plot(tsd['hydraulic.control.safetyControl.nOut'], color="black", label="Relative Verdichterdrehzahl", linewidth=0.75, linestyle='-')
            leg3=ax[1].legend(loc="upper center", bbox_to_anchor=(0.5,1.66), bbox_transform=ax[1].transAxes, fontsize=8, framealpha=0)
            leg3.get_frame().set_linewidth(0.0)
            ax[1].tick_params(axis="y",direction="in", length=2, pad=5)
            y_4 = [-0.2, 0, 1, 1.2]
            ax[1].yaxis.set_ticks(y_4)
            y_4_labels= ['',0,1,'']
            ax[1].set_yticklabels(y_4_labels, fontsize=11)

            ax[0].tick_params(axis="x",direction="in", length=0, pad=5)
            ax[1].tick_params(axis="x",direction="in", length=2, pad=5)
            ax[0].set_xticks([simstart+0, simstart+2160, 4320+simstart, 6480+simstart, 8640+simstart]) # simulation time
            ax[1].set_xticklabels(['0:00', '6:00', '12:00', '18:00', '24:00'], fontsize=11)
            ax[0].margins(x=0)
            ax[1].margins(x=0)

            
            ########## Saving ##########
            
            fig.savefig(r'D:\sgo-kwa\Repos\ParaStudy\parameter_study_dym\Typtag_12\Plots\Day\Small\Study_'+str(name)+r'_'+str(index)+r'.png',
                    format='png',
                    dpi=1200,
                    bbox_inches='tight'
                    )

            plt.close(fig)

            
    
            
            

            ########## Shorts ##########
            
            #### Plot QG small ####

            fig, ax = plt.subplots(2, 1, sharex=True, gridspec_kw={'height_ratios': [4, 1]})

            # first plot
            ax[0].plot(tsd['hydraulic.generation.heatPump.senT_b1.T'] -273.15, color="red", label="T$_\mathregular{VL}$", linewidth=1)
            ax[0].plot(tsd['hydraulic.control.HP_nSet_Controller.T_Set'] -273.15, color="blue", linestyle='--', label="T$_\mathregular{Set}$", linewidth=0.75)

            ax[0].set_ylabel("Temperatur [°C]", fontsize=11, labelpad=5)
            ax[0].tick_params(axis="y",direction="in", length=0, pad=5)
            ax[1].set_xlabel("Uhrzeit", fontsize=11, labelpad=5)
            for axis in ['top','bottom','left','right']:
                ax[0].spines[axis].set_linewidth(1)
                ax[1].spines[axis].set_linewidth(1)
            y_1 = [15, 25, 35, 45, 55, 65,75]
            y_1_labels= [15, 25, 35, 45, 55, 65,75]
            ax[0].set_yticks(y_1)
            ax[0].set_yticklabels(y_1_labels, fontsize=11)
            # ax[0].yaxis.set_ticks(np.arange(min(y_1), max(y_1)+1, 10))
            # ax[0].set_yticklabels(weight='bold')
            ax[0].text(0.14, 0.895,str(plotname)+'='+str(index), ha='center', va='center', bbox= dict(facecolor = 'white', alpha = 1, linewidth=0.75), transform=ax[0].transAxes, fontsize=8)
            ax[0].text(0.85, 0.895,'Zyklen='+str(counter), ha='center', va='center', bbox= dict(facecolor = 'white', alpha = 1, linewidth=0.75), transform=ax[0].transAxes, fontsize=8)
            # ax[0].text(0.1, 0.93,str(plotname)+'='+str(index), ha='center', va='center', bbox= dict(facecolor = 'white', alpha = 0.75), transform=ax.transAxes)
                

            # twin object for two different y-axis on the sample plot

            ax2=ax[0].twinx()
            ax2.plot(tsd['weaDat.weaBus.TDryBul'] -273.15, color="black", label="T$_\mathregular{U}$", linewidth=1,  linestyle='-.')
            
            ax2.set_ylabel("Lufttemperatur [C°]", fontsize=11, labelpad=5)
            ax2.tick_params(axis="y",direction="in", length=0, pad=5)
            y_2 = [-5,0,5,10,15,20,25]
            y_2_labels= [-5,0,5,10,15,20,25]
            ax2.set_yticks(y_2)
            ax2.set_yticklabels(y_2_labels, fontsize=11)
            # ax2.yaxis.set_ticks(np.arange(min(y_2), max(y_2)+1, 3))
            # ax2.set_yticklabels(weight='bold')
            
            # legend and others
            #font = font_manager.FontProperties(family='Times New Roman',
            #                              
            #                             size=7)
            leg=fig.legend(loc="upper center", bbox_to_anchor=(0.5, 1.25), ncol=5, bbox_transform=ax[0].transAxes, fontsize=10)
            leg.get_frame().set_linewidth(0.0)
            ax[0].grid(color='black',axis='both', linewidth=0.3)
            fig.set_size_inches(2.95, 2.36)

            '''''
            # Second Plot
            ax[1].plot(tsd['hydraulic.control.sigBusGen.hp_bus.onOffMea'], color="black", label="An/Aus", linewidth=0.5)
            y_3=[-0.2, 0, 1, 1.2]
            y_3_labels= ['','Aus','An','']
            ax[1].set_yticks(y_3)
            ax[1].set_yticklabels(y_3_labels, fontsize=10)
            ax[1].tick_params(axis="y",direction="in", length=2, pad=5)
            leg2=ax[1].legend(loc="upper center", bbox_to_anchor=(0.15,1.66), bbox_transform=ax[1].transAxes, fontsize=8, framealpha=0)
            leg2.get_frame().set_linewidth(0.0)

            # Second y axis 
            ax3=ax[1].twinx()
            ax3.plot(tsd['hydraulic.control.sigBusGen.hp_bus.nSet'], color="black", label="Relative Verdichterdrehzahl", linewidth=0.5, linestyle='--')
            leg3=ax3.legend(loc="upper center", bbox_to_anchor=(0.66,1.66), bbox_transform=ax[1].transAxes, fontsize=8, framealpha=0)
            leg3.get_frame().set_linewidth(0.0)
            ax3.tick_params(axis="y",direction="in", length=2, pad=5)
            y_4 = [-0.2, 0, 1, 1.2]
            ax3.yaxis.set_ticks(y_4)
            y_4_labels= ['','0','1','']
            ax3.set_yticklabels(y_4_labels, fontsize=10)
            '''
            ax[1].plot(tsd['hydraulic.control.safetyControl.nOut'], color="black", label="Relative Verdichterdrehzahl", linewidth=0.75, linestyle='-')
            leg3=ax[1].legend(loc="upper center", bbox_to_anchor=(0.5,1.66), bbox_transform=ax[1].transAxes, fontsize=8, framealpha=0)
            leg3.get_frame().set_linewidth(0.0)
            ax[1].tick_params(axis="y",direction="in", length=2, pad=5)
            y_4 = [-0.2, 0, 1, 1.2]
            ax[1].yaxis.set_ticks(y_4)
            y_4_labels= ['',0,1,'']
            ax[1].set_yticklabels(y_4_labels, fontsize=11)

            ax[0].tick_params(axis="x",direction="in", length=0, pad=5)
            ax[1].tick_params(axis="x",direction="in", length=2, pad=5)
            ax[0].set_xlim(2160+simstart, 7920+simstart)
            ax[1].set_xlim(2160+simstart, 7920+simstart)
            ax[0].set_xticks([simstart+2160, simstart+3600, 5040+simstart, 6480+simstart, 7920+simstart]) # simulation time
            ax[1].set_xticklabels(['6:00', '10:00', '14:00', '18:00', '22:00'], fontsize=11)
            ax[0].margins(x=0)
            ax[1].margins(x=0)
    
            fig.savefig(r'D:\sgo-kwa\Repos\ParaStudy\parameter_study_dym\Typtag_12\Plots\Short\Small\Study_'+str(name)+r'_'+str(index)+r'.png',
                    format='png',
                    dpi=1200,
                    bbox_inches='tight'
                    )
            
            plt.close(fig)





            # Plot 2 (klein)
            fig, ax = plt.subplots(2, 1, sharex=True, gridspec_kw={'height_ratios': [4, 1]})

            # first plot
            ax[0].plot(tsd['hydraulic.generation.heatPump.senT_b1.T'] -273.15, color="red", label="T$_\mathregular{VL}$", linewidth=1)
            ax[0].plot(tsd['hydraulic.control.HP_nSet_Controller.T_Set'] -273.15, color="blue", linestyle='--', label="T$_\mathregular{Set}$", linewidth=0.75)

            ax[0].set_ylabel("Temperatur [°C]", fontsize=11, labelpad=5)
            ax[0].tick_params(axis="y",direction="in", length=0, pad=5)
            ax[1].set_xlabel("Uhrzeit", fontsize=11, labelpad=5)
            for axis in ['top','bottom','left','right']:
                ax[0].spines[axis].set_linewidth(1)
                ax[1].spines[axis].set_linewidth(1)
            y_1 = [15, 25, 35, 45, 55, 65,75]
            y_1_labels= [15, 25, 35, 45, 55, 65,75]
            ax[0].set_yticks(y_1)
            ax[0].set_yticklabels(y_1_labels, fontsize=11)
            # ax[0].yaxis.set_ticks(np.arange(min(y_1), max(y_1)+1, 10))
            # ax[0].set_yticklabels(weight='bold')
            ax[0].text(0.14, 0.895,str(plotname)+'='+str(index), ha='center', va='center', bbox= dict(facecolor = 'white', alpha = 1, linewidth=0.75), transform=ax[0].transAxes, fontsize=8)
            ax[0].text(0.85, 0.895,'Zyklen='+str(counter), ha='center', va='center', bbox= dict(facecolor = 'white', alpha = 1, linewidth=0.75), transform=ax[0].transAxes, fontsize=8)
            # ax[0].text(0.1, 0.93,str(plotname)+'='+str(index), ha='center', va='center', bbox= dict(facecolor = 'white', alpha = 0.75), transform=ax.transAxes)
                

            # twin object for two different y-axis on the sample plot

            ax2=ax[0].twinx()
            ax2.plot(tsd['weaDat.weaBus.TDryBul'] -273.15, color="black", label="T$_\mathregular{U}$", linewidth=1,  linestyle='-.')
            
            ax2.set_ylabel("Lufttemperatur [C°]", fontsize=11, labelpad=5)
            ax2.tick_params(axis="y",direction="in", length=0, pad=5)
            y_2 = [-5,0,5,10,15,20,25]
            y_2_labels= [-5,0,5,10,15,20,25]
            ax2.set_yticks(y_2)
            ax2.set_yticklabels(y_2_labels, fontsize=11)
            # ax2.yaxis.set_ticks(np.arange(min(y_2), max(y_2)+1, 3))
            # ax2.set_yticklabels(weight='bold')
            
            # legend and others
            #font = font_manager.FontProperties(family='Times New Roman',
            #                              
            #                             size=7)
            leg=fig.legend(loc="upper center", bbox_to_anchor=(0.5, 1.25), ncol=5, bbox_transform=ax[0].transAxes, fontsize=10)
            leg.get_frame().set_linewidth(0.0)
            ax[0].grid(color='black',axis='both', linewidth=0.3)
            fig.set_size_inches(2.95, 2.36)

            '''''
            # Second Plot
            ax[1].plot(tsd['hydraulic.control.sigBusGen.hp_bus.onOffMea'], color="black", label="An/Aus", linewidth=0.5)
            y_3=[-0.2, 0, 1, 1.2]
            y_3_labels= ['','Aus','An','']
            ax[1].set_yticks(y_3)
            ax[1].set_yticklabels(y_3_labels, fontsize=10)
            ax[1].tick_params(axis="y",direction="in", length=2, pad=5)
            leg2=ax[1].legend(loc="upper center", bbox_to_anchor=(0.15,1.66), bbox_transform=ax[1].transAxes, fontsize=8, framealpha=0)
            leg2.get_frame().set_linewidth(0.0)

            # Second y axis 
            ax3=ax[1].twinx()
            ax3.plot(tsd['hydraulic.control.sigBusGen.hp_bus.nSet'], color="black", label="Relative Verdichterdrehzahl", linewidth=0.5, linestyle='--')
            leg3=ax3.legend(loc="upper center", bbox_to_anchor=(0.66,1.66), bbox_transform=ax[1].transAxes, fontsize=8, framealpha=0)
            leg3.get_frame().set_linewidth(0.0)
            ax3.tick_params(axis="y",direction="in", length=2, pad=5)
            y_4 = [-0.2, 0, 1, 1.2]
            ax3.yaxis.set_ticks(y_4)
            y_4_labels= ['','0','1','']
            ax3.set_yticklabels(y_4_labels, fontsize=10)
            '''
            ax[1].plot(tsd['hydraulic.transfer.val[1].m_flow'], color="black", label="Transfermassenstrom", linewidth=1.0, linestyle='-')
            leg3=ax[1].legend(loc="upper center", bbox_to_anchor=(0.5,1.66), bbox_transform=ax[1].transAxes, fontsize=8, framealpha=0)
            leg3.get_frame().set_linewidth(0.0)
            ax[1].tick_params(axis="y",direction="in", length=2, pad=5)
            ax[1].set_ylabel("[kg/s]", fontsize=11, labelpad=5)
            y_4 = [-0.1, 0, 0.5, 0.6]
            ax[1].yaxis.set_ticks(y_4)
            y_4_labels= ['',0,0.5,'']
            ax[1].set_yticklabels(y_4_labels, fontsize=11)

            ax[0].tick_params(axis="x",direction="in", length=0, pad=5)
            ax[1].tick_params(axis="x",direction="in", length=2, pad=5)
            ax[0].set_xticks([simstart+0, simstart+2160, 4320+simstart, 6480+simstart, 8640+simstart]) # simulation time
            ax[1].set_xticklabels(['0:00', '6:00', '12:00', '18:00', '24:00'], fontsize=11)
            ax[0].margins(x=0)
            ax[1].margins(x=0)

            
            ########## Saving ##########
            
            fig.savefig(r'D:\sgo-kwa\Repos\ParaStudy\parameter_study_dym\Typtag_12\Plots\Day\Small\Study_'+str(name)+r'_'+str(index)+r'V.png',
                    format='png',
                    dpi=1200,
                    bbox_inches='tight'
                    )

            plt.close(fig)

            
    
            
            

            ########## Shorts ##########
            
            #### Plot QG small ####

            fig, ax = plt.subplots(2, 1, sharex=True, gridspec_kw={'height_ratios': [4, 1]})

            # first plot
            ax[0].plot(tsd['hydraulic.generation.heatPump.senT_b1.T'] -273.15, color="red", label="T$_\mathregular{VL}$", linewidth=1)
            ax[0].plot(tsd['hydraulic.control.HP_nSet_Controller.T_Set'] -273.15, color="blue", linestyle='--', label="T$_\mathregular{Set}$", linewidth=0.75)

            ax[0].set_ylabel("Temperatur [°C]", fontsize=11, labelpad=5)
            ax[0].tick_params(axis="y",direction="in", length=0, pad=5)
            ax[1].set_xlabel("Uhrzeit", fontsize=11, labelpad=5)
            for axis in ['top','bottom','left','right']:
                ax[0].spines[axis].set_linewidth(1)
                ax[1].spines[axis].set_linewidth(1)
            y_1 = [15, 25, 35, 45, 55, 65,75]
            y_1_labels= [15, 25, 35, 45, 55, 65,75]
            ax[0].set_yticks(y_1)
            ax[0].set_yticklabels(y_1_labels, fontsize=11)
            # ax[0].yaxis.set_ticks(np.arange(min(y_1), max(y_1)+1, 10))
            # ax[0].set_yticklabels(weight='bold')
            ax[0].text(0.14, 0.895,str(plotname)+'='+str(index), ha='center', va='center', bbox= dict(facecolor = 'white', alpha = 1, linewidth=0.75), transform=ax[0].transAxes, fontsize=8)
            ax[0].text(0.85, 0.895,'Zyklen='+str(counter), ha='center', va='center', bbox= dict(facecolor = 'white', alpha = 1, linewidth=0.75), transform=ax[0].transAxes, fontsize=8)
            # ax[0].text(0.1, 0.93,str(plotname)+'='+str(index), ha='center', va='center', bbox= dict(facecolor = 'white', alpha = 0.75), transform=ax.transAxes)
                

            # twin object for two different y-axis on the sample plot

            ax2=ax[0].twinx()
            ax2.plot(tsd['weaDat.weaBus.TDryBul'] -273.15, color="black", label="T$_\mathregular{U}$", linewidth=1,  linestyle='-.')
            
            ax2.set_ylabel("Lufttemperatur [C°]", fontsize=11, labelpad=5)
            ax2.tick_params(axis="y",direction="in", length=0, pad=5)
            y_2 = [-5,0,5,10,15,20,25]
            y_2_labels= [-5,0,5,10,15,20,25]
            ax2.set_yticks(y_2)
            ax2.set_yticklabels(y_2_labels, fontsize=11)
            # ax2.yaxis.set_ticks(np.arange(min(y_2), max(y_2)+1, 3))
            # ax2.set_yticklabels(weight='bold')
            
            # legend and others
            #font = font_manager.FontProperties(family='Times New Roman',
            #                              
            #                             size=7)
            leg=fig.legend(loc="upper center", bbox_to_anchor=(0.5, 1.25), ncol=5, bbox_transform=ax[0].transAxes, fontsize=10)
            leg.get_frame().set_linewidth(0.0)
            ax[0].grid(color='black',axis='both', linewidth=0.3)
            fig.set_size_inches(2.95, 2.36)

            '''''
            # Second Plot
            ax[1].plot(tsd['hydraulic.control.sigBusGen.hp_bus.onOffMea'], color="black", label="An/Aus", linewidth=0.5)
            y_3=[-0.2, 0, 1, 1.2]
            y_3_labels= ['','Aus','An','']
            ax[1].set_yticks(y_3)
            ax[1].set_yticklabels(y_3_labels, fontsize=10)
            ax[1].tick_params(axis="y",direction="in", length=2, pad=5)
            leg2=ax[1].legend(loc="upper center", bbox_to_anchor=(0.15,1.66), bbox_transform=ax[1].transAxes, fontsize=8, framealpha=0)
            leg2.get_frame().set_linewidth(0.0)

            # Second y axis 
            ax3=ax[1].twinx()
            ax3.plot(tsd['hydraulic.control.sigBusGen.hp_bus.nSet'], color="black", label="Relative Verdichterdrehzahl", linewidth=0.5, linestyle='--')
            leg3=ax3.legend(loc="upper center", bbox_to_anchor=(0.66,1.66), bbox_transform=ax[1].transAxes, fontsize=8, framealpha=0)
            leg3.get_frame().set_linewidth(0.0)
            ax3.tick_params(axis="y",direction="in", length=2, pad=5)
            y_4 = [-0.2, 0, 1, 1.2]
            ax3.yaxis.set_ticks(y_4)
            y_4_labels= ['','0','1','']
            ax3.set_yticklabels(y_4_labels, fontsize=10)
            '''
            
            ax[1].plot(tsd['hydraulic.transfer.val[1].m_flow'], color="black", label="Transfermassenstrom", linewidth=1.0, linestyle='-')
            leg3=ax[1].legend(loc="upper center", bbox_to_anchor=(0.5,1.66), bbox_transform=ax[1].transAxes, fontsize=8, framealpha=0)
            leg3.get_frame().set_linewidth(0.0)
            ax[1].tick_params(axis="y",direction="in", length=2, pad=5)
            ax[1].set_ylabel("[kg/s]", fontsize=11, labelpad=5)
            y_4 = [-0.1, 0, 0.5, 0.6]
            ax[1].yaxis.set_ticks(y_4)
            y_4_labels= ['',0,0.5,'']
            ax[1].set_yticklabels(y_4_labels, fontsize=11)


            ax[0].tick_params(axis="x",direction="in", length=0, pad=5)
            ax[1].tick_params(axis="x",direction="in", length=2, pad=5)
            ax[0].set_xlim(2160+simstart, 7920+simstart)
            ax[1].set_xlim(2160+simstart, 7920+simstart)
            ax[0].set_xticks([simstart+2160, simstart+3600, 5040+simstart, 6480+simstart, 7920+simstart]) # simulation time
            ax[1].set_xticklabels(['6:00', '10:00', '14:00', '18:00', '22:00'], fontsize=11)
            ax[0].margins(x=0)
            ax[1].margins(x=0)
    
            fig.savefig(r'D:\sgo-kwa\Repos\ParaStudy\parameter_study_dym\Typtag_12\Plots\Short\Small\Study_'+str(name)+r'_'+str(index)+r'V.png',
                    format='png',
                    dpi=1200,
                    bbox_inches='tight'
                    )
            
            plt.close(fig)

            
            
            #### Plot QG big ####
            fig, ax = plt.subplots(2, 1, sharex=True, gridspec_kw={'height_ratios': [4, 1]})

            # first plot
            ax[0].plot(tsd['hydraulic.generation.heatPump.senT_b1.T'] -273.15, color="red", label="T$_\mathregular{VL}$", linewidth=1)
            ax[0].plot(tsd['hydraulic.control.HP_nSet_Controller.T_Set'] -273.15, color="blue", linestyle='--', label="T$_\mathregular{Set}$", linewidth=1)
            ax[0].plot(tsd['weaDat.weaBus.TDryBul'] -273.15, color="black", label="T$_\mathregular{U}$", linewidth=1,  linestyle='-.')
            ax[0].plot(tsd['building.thermalZone[1].TAir'] -273.15, color="green", label="T$_\mathregular{Luft,Raum}$", linewidth=1)
            
            ax[0].set_ylabel("Temperatur [°C]", fontsize=11, labelpad=5)
            ax[0].tick_params(axis="y",direction="in", length=0, pad=5)
            ax[1].set_xlabel("Uhrzeit", fontsize=11, labelpad=5)
            for axis in ['top','bottom','left','right']:
                ax[0].spines[axis].set_linewidth(1)
                ax[1].spines[axis].set_linewidth(1)
            y_1 = [-10,0,10,20,30,40,50,60,70]
            y_1_labels= [-10,0,10,20,30,40,50,60,70]
            ax[0].set_yticks(y_1)
            ax[0].set_yticklabels(y_1_labels, fontsize=11)
            # ax[0].yaxis.set_ticks(np.arange(min(y_1), max(y_1)+1, 10))
            # ax[0].set_yticklabels(weight='bold')
            ax[0].text(0.1, 0.91,str(plotname)+'='+str(index), ha='center', va='center', bbox= dict(facecolor = 'white', alpha = 1), transform=ax[0].transAxes, fontsize=8)
            ax[0].text(0.11, 0.33,'Zyklen='+str(counter), ha='center', va='center', bbox= dict(facecolor = 'white', alpha = 1), transform=ax[0].transAxes, fontsize=8)
            # ax[0].text(0.1, 0.93,str(plotname)+'='+str(index), ha='center', va='center', bbox= dict(facecolor = 'white', alpha = 0.75), transform=ax.transAxes)
                

            # twin object for two different y-axis on the sample plot

            ax2=ax[0].twinx()
            ax2.plot(tsd['hydraulic.generation.heatPump.innerCycle.Pel']/1000, color="orange", label="P$_\mathregular{el,Verd}$", linewidth=1)
            
            ax2.set_ylabel("Leistung [kW]", fontsize=11, labelpad=8)
            ax2.tick_params(axis="y",direction="in", length=0, pad=5)
            y_2 = [0,3,6,9,12,15,18,21,24]
            y_2_labels= [0,3,6,9,12,15,18,21,24]
            ax2.set_yticks(y_2)
            ax2.set_yticklabels(y_2_labels, fontsize=11)
            # ax2.yaxis.set_ticks(np.arange(min(y_2), max(y_2)+1, 3))
            # ax2.set_yticklabels(weight='bold')
            
            # legend and others
            leg=fig.legend(loc="upper center", bbox_to_anchor=(0.5, 1.3), ncol=5, bbox_transform=ax[0].transAxes, fontsize=11)
            leg.get_frame().set_linewidth(0.0)
            ax[0].grid(color='black',axis='both', linewidth=0.5)
            fig.set_size_inches(5.11, 2.75)

            # Second Plot
            ax[1].plot(tsd['hydraulic.transfer.val[1].m_flow'], color="purple", label="Transfermassenstrom", linewidth=1)
            y_3=[-0.1, 0, 0.5, 0.6]
            y_3_labels= ['','0','0.5','']
            ax[1].set_yticks(y_3)
            ax[1].set_yticklabels(y_3_labels, fontsize=11)
            ax[1].set_ylabel("[kg/s]", fontsize=11, labelpad=5)
            ax[1].tick_params(axis="y",direction="in", length=2, pad=5)
            leg2=ax[1].legend(loc="upper center", bbox_to_anchor=(0.5,1.65), bbox_transform=ax[1].transAxes, fontsize=9, framealpha=0)
            leg2.get_frame().set_linewidth(0.0)
                
            ax[0].tick_params(axis="x",direction="in", length=0, pad=5)
            ax[1].tick_params(axis="x",direction="in", length=2, pad=5)
            ax[0].set_xlim(2160+simstart, 7920+simstart)
            ax[1].set_xlim(2160+simstart, 7920+simstart)
            ax[0].set_xticks([2160+simstart, 2880+simstart, 3600+simstart, 4320+simstart, 5040+simstart, 5760+simstart, 6480+simstart, 7200+simstart, 7920+simstart]) # simulation time
            ax[1].set_xticklabels(['6:00', '8:00', '10:00', '12:00', '14:00', '16:00', '18:00', '20:00', '22:00'], fontsize=11)
            ax[0].margins(x=0)
            ax[1].margins(x=0)

            

            fig.savefig(r'D:\sgo-kwa\Repos\ParaStudy\parameter_study_dym\Typtag_12\Plots\Short\Big\Study_'+str(name)+r'_'+str(index)+r'.png',
                    format='png',
                    dpi=1200,
                    bbox_inches='tight'
                    )
            
            plt.close(fig)
            

            
            




           
    
    res_avg.to_excel(pathlib.Path(file_dir).joinpath("parameter_study_dym", "Typtag_12", "Results", 'ParaStudyAverages_'+str(name)+'.xlsx'))

    dataframe_cop.to_excel(pathlib.Path(file_dir).joinpath("parameter_study_dym", "Typtag_12", "Results", 'ParaStudyCOP_'+str(name)+'.xlsx'))
    dataframe_tvl.to_excel(pathlib.Path(file_dir).joinpath("parameter_study_dym", "Typtag_12", "Results", 'ParaStudyTvl_'+str(name)+'.xlsx'))
    dataframe_tra.to_excel(pathlib.Path(file_dir).joinpath("parameter_study_dym", "Typtag_12", "Results", 'ParaStudyTra_'+str(name)+'.xlsx'))
    dataframe_traum.to_excel(pathlib.Path(file_dir).joinpath("parameter_study_dym", "Typtag_12", "Results", 'ParaStudyTraum_'+str(name)+'.xlsx'))
    dataframe_tset.to_excel(pathlib.Path(file_dir).joinpath("parameter_study_dym", "Typtag_12", "Results", 'ParaStudyTset_'+str(name)+'.xlsx'))
    dataframe_pel.to_excel(pathlib.Path(file_dir).joinpath("parameter_study_dym", "Typtag_12", "Results", 'ParaStudyPel_'+str(name)+'.xlsx'))


    dym_api.close()
    
if __name__ == '__main__':
    main(
        
        aixlib_mo=r"D:\sgo-kwa\Repos\optisim\submodules\AixLib\AixLib\package.mo",
        BESMod_mo= r"D:\sgo-kwa\Repos\optisim\submodules\BESMod\BESMod\package.mo",
        Buildings_mo=r"D:\sgo-kwa\Repos\optisim\submodules\Buildings\Buildings\package.mo",
        IBPSA_mo=r"D:\sgo-kwa\Repos\optisim\submodules\IBPSA\IBPSA\package.mo",
        #Teaser_mo=r"D:\sgo-kwa\Repos\optisim\submodules\teaserweb_AixLib\package.mo",
        MPC_mo=r"D:\sgo-kwa\Repos\optisim\models\MPC\package.mo", 
        PCM_mo=r"D:\sgo-kwa\Repos\ebc0888_mitsubishi_pcmgoeshil_kap\Modelica\PCMgoesHIL\package.mo",
        file_dir=r"D:\sgo-kwa\Repos\ParaStudy",
        log_fmu=False,
        output_interval=10
    )