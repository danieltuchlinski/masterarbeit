"""
Run an example for a calibration using multiprocessing.
Multiprocessing only runs with pymoo as the used framework.
"""


if __name__ == "__main__":
    import pathlib
    from calibration import run_calibration
    # Number of logical Processors to run calibration on:
    N_CPU = 5
    

    # Sensitivity analysis:
    run_calibration(
        file_dir=pathlib.Path(__file__).parent,
        n_cpu=N_CPU,
        framework="pymoo",
        method="GA"
    )
