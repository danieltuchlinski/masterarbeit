# # Example 5 Automated process

# Goals of this part of the examples:
# 1. Learn how to run everything in one script
#
# Start by importing everything
from sensitivity_analysis import run_sensitivity_analysis
from calibration import run_calibration


def main(
        file_dir= "D:\sgo-kwa\Repos\optisim\calibration\heatpumpcalibration",
        n_cpu: int = 1
):
    """ 
    Arguments of this example:

    :param [pathlib.Path, str] examples_dir:
        Path to the examples folder of AixCaliBuHA
    :param str example:
        Whether to use example A (requires windows) or B.
        Default is "A"
    :param int n_cpu:
        Number of cores to use

    """
    # First we run the sensitivity analysis:
    calibration_classes, sim_api = run_sensitivity_analysis(
        file_dir=file_dir, n_cpu=n_cpu
    )
    # Then the calibration and validation
    run_calibration(
        file_dir=file_dir,
        sim_api=sim_api,
        cal_classes=calibration_classes
    )


if __name__ == '__main__':
    import pathlib
    
    main(file_dir=pathlib.Path(__file__).parent)
