
# Start by importing all relevant packages
import pathlib
from pickle import NONE
# Imports from ebcpy
from ebcpy import TimeSeriesData
# Imports from aixcalibuha
from aixcalibuha import TunerParas, Goals, \
    CalibrationClass
from aixcalibuha.data_types import merge_calibration_classes


def main(
        file_dir= "D:\sgo-kwa\Repos\optisim\calibration\heatpumpcalibration",
        statistical_measure="NRMSE",
        multiple_classes=True
):
    """
    Arguments of this example:

      :param str statistical_measure:
        Measure to calculate the scalar of the objective,
        One of the supported methods in
        ebcpy.utils.statistics_analyzer.StatisticsAnalyzer
        e.g. RMSE, MAE, NRMSE (quadratischer Schätzungsfehler)
    :param bool multiple_classes:
        If False, all CalibrationClasses will have the
        same name
    """

    # ## Tuner Parameters
    # Tuner parameters are the optimization variables we will be
    # changing to match the simulated onto the measured output.
   
    # To define tuner parameters, you have to specify
    # - the name of the parameter
    # - an initial guess
    # - boundaries as a (min, max) tuple.
    # Note that the initial guess is not always used by optimization routines.
    # We've chosen to make it a requirement to prevent blindly accepting
    # calibration results. If the result is very far away from your initial guess
    # and you though you understand the model, maybe the parameter is just not
    # sensitive or influenced by another parameter.
    #
    # How to load the data is up to you. To make the structure clear,
    # we use a 3 element tuple in this example:
    
    # Choose the Tuner paramters
    # plt.rcParams["font.family"] = "Times New Roman"
    data = [
        # (name, initial_value, boundaries)
         ('heatPump.refIneFre_constant', 0.75, (0.75, 0.7500001)),
         ('heatPump.VCon', 0.0047, (0.0047, 0.00470001)), # Glättet stark
        #('heatPump.tauSenT', 1, (0.1, 10)),
        #('heatPump.tauHeaTraEva', 1200, (20, 2500)),
        #('heatPump.TAmbEva_nominal', 5.2320, (1, 50)),
        #('heatPump.tauHeaTraCon', 1200, (20, 2500)),
        #('heatPump.TAmbCon_nominal', 20, (1, 50)),
        #('heatPump.CCon', 30, (0, 80)),
         ('heatPump.GConIns', 60.63, (60.63, 60.6300001)),        
         ('heatPump.GConOut', 5.23, (5.23, 5.230001)),
        #('heatPump.CEva', 3000, (1000, 5000)),
        #('heatPump.GEvaIns', 50, (0, 80)),        
        #('heatPump.GEvaOut', 50, (0, 80)),
    ]
    
    # Saves the data into the tuner parameters class as indexed above
    tuner_paras = TunerParas(
        names=[entry[0] for entry in data],
        initial_values=[entry[1] for entry in data],
        bounds=[entry[2] for entry in data]
    )
    print(tuner_paras)
    print("Names of parameters", tuner_paras.get_names())
    
    # Scaling (will be done internally)
    print("Scaled initial values:\n", tuner_paras.scale(tuner_paras.get_initial_values()))

    # ## Goals
    # The evaluation of your goals (or mathematically speaking 'objective function')
    # depends on the difference of measured to simulated data.
    # Thus, you need to specify both measured and simulated data.
    #
    # Start by loading the measured data generated in system_analysis.py:
    data_dir = pathlib.Path(file_dir).joinpath("data")
    meas_target_data = TimeSeriesData(data_dir.joinpath("CHP.csv"))
    
    # Map the measured keys to the names inside your simulation
    # sub_s = "ₐ₈CDₑբGₕᵢⱼₖₗₘₙₒₚQᵣₛₜᵤᵥwₓᵧZₐ♭꜀ᑯₑբ₉ₕᵢⱼₖₗₘₙₒₚ૧ᵣₛₜᵤᵥwₓᵧ₂₀₁₂₃₄₅₆₇₈₉₊₋₌₍₎"
    # "ₛᵢₘ"
    # "ₘₑₐₛ"
  
    variable_names = {
        # Name of goal: Name of measured variable, Name of simulated variable
        # Either use list
        #"P$_\mathregular{el,}$ ": ["StateMachine_Pel_Inv", "Pel_comp"],
        "T$_\mathregular{VL,}$ ": ["IndoorUnit_T_FlowOutlet_IDU","T_VL"],
        
        # u'Pₑₗ': ["StateMachine_Pel_Inv", "Pel_comp"],
        # u'T\u1D04\u2092\u2099\u208D\u2092\u1D64\u209C\u208E': ["IndoorUnit_T_FlowOutlet_IDU","T_VL"],
        # "Outlet temperature condensator": ["IndoorUnit_T_CondOut_IDU","heatPump.senT_b1.T"],

    }
    # To match the measured data to simulated data,
    # the index has to match with the simulation output
    # Thus, convert it:
    
    meas_target_data.to_float_index()
    # Lastly, setup the goals object. Note that the statistical_measure
    # is parameter of the python version of this example. It's a metric to
    # compare two set's of time series data. Which one to choose is up to
    # your expert knowledge. If you have no clue, raise an issue or read
    # basic literature on calibration.
    goals = Goals(
        meas_target_data=meas_target_data,
        variable_names=variable_names,
        statistical_measure=statistical_measure,
        #weightings=[0.5, 0.5]
        weightings=[1]
    )
    # Let's check if our evaluation is possible by creating some
    # dummy `sim_target_data` with the same index:
  
    
      
    # sim_target_data = TimeSeriesData({"heatPump.senT_b1.T": 313.15, "heatPump.innerCycle.Pel": 4000},
    #                                 index=meas_target_data.index)
   # print("Goals data before setting simulation data:\n", goals.get_goals_data())
   # goals.set_sim_target_data(sim_target_data)
   # print("Goals data after setting simulation data:\n", goals.get_goals_data())
   # print(statistical_measure, "of goals: ", goals.eval_difference())
   # print("Verbose information on calculation", goals.eval_difference(verbose=True))
    
    # ## Calibration Classes
    # We now are going to wrap everything up into a single object called
    # `CalibrationClass`.
    # Each class has a `name`, a `start_time`, `stop_time` and
    # `goals`, `tuner_paras` (tuner parameters) and `inputs`.
    # The latter three can be set for all
    # classes if a distinction is not required.
    # ### Why do we use a `CalibrationClass`?
    # Because this class contains all information necessary
    # to perform both sensitivity analysis and calibration automatically.
    # ### Can there be multiple classes?
    # Yes! Because we expect different tuner parameters
    # to influence the outputs based on the state of the system,
    # e.g. 'On' and 'Off' more or less. To reduce the complexity of the
    # optimization problem, separating tuner parameters into time intervals
    # can be handy. For example heat losses to the ambient may be most
    # sensitive if the device is just turned off, while efficiency is more
    # sensitive during runtime.
    # In the video at 1:40:00

    calibration_classes = [
        CalibrationClass(
            name="Intervall_CHP_NRMSE_vl",
            start_time=200,
            stop_time=9000
        )
        
    ]

    for cal_class in calibration_classes:
        cal_class.goals = goals
        cal_class.tuner_paras = tuner_paras
    
    # ## Merge multiple classes
    # If wanted, we can merge multiple classes and optimize them as one.
    # Example:
    print([c.name for c in calibration_classes])
    calibration_classes_merged = merge_calibration_classes(calibration_classes)
    print([c.name for c in calibration_classes_merged])
    # Don't worry, the relevant_time_interval object keeps track
    # of which time intervals are relevant for the objective calculation
    print("Relevant time interval for class",
          calibration_classes_merged[0].name,
          calibration_classes_merged[0].relevant_intervals)

    # Let's also create an object to later validate our calibration:
    validation_class = CalibrationClass(
        name="Validation",
        start_time=1800,
        stop_time=1800,
        goals=goals,
        tuner_paras=tuner_paras,
    )
    return calibration_classes, validation_class


if __name__ == '__main__':
    main(
        file_dir=pathlib.Path(__file__).parent,
        multiple_classes=True
    )

