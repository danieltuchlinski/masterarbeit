
# Start by importing all relevant packages
import pathlib
import pandas as pd
import numpy as np
import statistics
import matplotlib.pyplot as plt
# Imports from ebcpy
from ebcpy import FMU_API, TimeSeriesData, DymolaAPI
from ebcpy.utils.conversion import convert_tsd_to_modelica_txt

def main(

        file_dir= "D:\sgo-kwa\Repos\ParaStudy",
        aixlib_mo="D:\sgo-kwa\Repos\optisim\submodules\AixLib\AixLib\package.mo",
        BESMod_mo= "D:\sgo-kwa\Repos\optisim\submodules\BESMod\BESMod\package.mo",
        Buildings_mo= "D:\sgo-kwa\Repos\optisim\submodules\Buildings\Buildings\package.mo",
        IBPSA_mo= "D:\sgo-kwa\Repos\optisim\submodules\IBPSA\IBPSA\package.mo",
        #Teaser_mo= "D:\sgo-kwa\Repos\optisim\submodules\teaserweb_AixLib\package.mo",
        MPC_mo="D:\sgo-kwa\Repos\optisim\models\MPC\package.mo", 
        PCM_mo="D:\sgo-kwa\Repos\ebc0888_mitsubishi_pcmgoeshil_kap\Modelica\PCMgoesHIL\package.mo",
        cd=None,
        log_fmu=True,
        output_interval=100,
        with_plot=True
):
       
    # General settings
    if cd is None:
        cd = pathlib.Path(file_dir).joinpath("parameter_study_dym", "Jahressimulation")

    # ######################### Simulation API Instantiation ##########################
    # %% Setup the FMU-API:

    dym_api = DymolaAPI(
        model_name= "MPC.MA_Kevin_Model_Sim_Rad",
        cd=cd,
        packages=[aixlib_mo, Buildings_mo, IBPSA_mo, BESMod_mo, MPC_mo, PCM_mo],
        show_window=True,
        n_restart=-1,
        equidistant_output=False,
        #get_structural_parameters=True
    )
    
    # ######################### Simulation Setup Part ##########################
 
    # Or pass a dictionary. This makes using configs (toml, json) much easier
    simulation_setup = {"start_time": 0,
                        "stop_time": 31536000,
                        "output_interval": output_interval}
    dym_api.set_sim_setup(sim_setup=simulation_setup)

    # ######################### Parameters ##########################
    # Let's get some parameter to change:
    
    # Wer#te der Parameter
    PI_k = dym_api.parameters['hydraulic.control.monovalentControlParas.k'].value
    PI_Ti = dym_api.parameters['hydraulic.control.monovalentControlParas.T_I'].value
    HYS = dym_api.parameters['hydraulic.control.monovalentControlParas.dTHysBui'].value
   
    #parameter_names=["PI_k", "PI_Ti", "HYS"]
    #parameter_names_plot=["$K_\mathrm{p}$", "$T_\mathrm{n}$", "$T_\mathrm{hys}$"]

    #parameter_names=["PI_Tn", "HYS", "PI_k"]
    #parameter_names_plot=["$T_\mathrm{n}$", "$K_\mathrm{p}$", "$T_\mathrm{hys}$"]

    parameter_names=["HYS", "PI_k", "PI_Ti"]
    parameter_names_plot=["$T_\mathrm{hys}$", "$K_\mathrm{p}$", "$T_\mathrm{n}$"]

    # Alteration
    # Startwerte festlegen!
   
    # values_PI_k=[[0.00001, 0.0001, 0.0005, 0.001, 0.005, 0.01, 0.05, 0.1, 0.25, 0.5, 0.75, 1, 2.5, 5, 7.5, 10, 12.5, 15, 17.5, 20] # 1
    values_PI_k=[7.5, 10, 12.5, 15, 17.5, 20]

    # values_PI_Ti=[10, 50, 100, 150, 250, 500, 750, 1000, 1250, 1500, 1750, 2000, 2500, 5000, 10000, 15000, 20000, 25000, 35000, 50000] # 1000
    values_PI_Ti=[10, 50, 100, 150, 250, 500, 750]

    values_HYS=[1,2,3,4,5,6]  # 8

    parameters_PI_k = []
    parameters_PI_Ti = []
    parameters_HYS = []
  
    sim_parameters=[]
    sim_parameters_values=[]
    
    for value_k  in values_PI_k:
        parameters_PI_k.append({"hydraulic.control.monovalentControlParas.k": value_k})
       

    for value_t in  values_PI_Ti:
        parameters_PI_Ti.append({"hydraulic.control.monovalentControlParas.T_I": value_t})
        

    for value_hys in values_HYS:
        parameters_HYS.append({"hydraulic.control.monovalentControlParas.dTHysBui": value_hys })
       
    # Parameterliste für die Simulation
    #sim_parameters.append(parameters_PI_k)
    #sim_parameters.append(parameters_PI_Ti)
    sim_parameters.append(parameters_HYS)
    
    #sim_parameters_values.append(values_PI_k)
    #sim_parameters_values.append(values_PI_Ti)
    sim_parameters_values.append(values_HYS)

    # ######################### Results to store ##########################
       
    dym_api.result_names = [ 

                            "hydraulic.generation.heatPump.senT_b1.T",
                            "weaDat.weaBus.TDryBul", 
                            "building.thermalZone[1].TAir",
                            'hydraulic.generation.heatPump.senT_a1.T',
                            'hydraulic.generation.heatPump.senT_b2.T',

                            'hydraulic.generation.heatPump.innerCycle.Pel',
                            'hydraulic.generation.heatPump.innerCycle.QCon',
                            'hydraulic.generation.heatPump.innerCycle.QEva',

                            'hydraulic.control.HP_nSet_Controller.n_Set',
                            'hydraulic.control.safetyControl.nOut',
                            'hydraulic.control.HP_nSet_Controller.T_Set',
                            'hydraulic.control.HP_nSet_Controller.T_Meas',
                            'hydraulic.control.sigBusGen.hp_bus.nSet',
                            'hydraulic.control.sigBusGen.hp_bus.onOffMea',
                            'hydraulic.transfer.pressureReliefValveOnOff.m_flow',
                            'hydraulic.transfer.val[1].m_flow',
                            'hydraulic.transfer.panelHeating[1].TReturn.T'
                            #'hydraulic.transfer.senTFlowEach[1].T
                            ]

    print("Results that will be stored", dym_api.result_names)

    # ######################### Execute simulation ##########################
    # Pass the created list to the simulate function
    
    # Saving data for the averages 
    res_td=({'Parameter':[], 'Wert':[], 'Pel':[],'Q_con':[], "TRL":[], "TVL":[], "TEvaOut":[],"T_room":[], "TAir":[], "TVL_m":[], "TVL_s":[], "T_ra":[], "COP":[], "OnOFF":[]})
    res_avg = pd.DataFrame(res_td)

    index_numbers=np.arange(0,315360,1)  
    data_time={'Time': index_numbers}
    
    dataframe_cop = pd.DataFrame(data_time)
    dataframe_pel = pd.DataFrame(data_time)
    dataframe_tset = pd.DataFrame(data_time)
    dataframe_tvl = pd.DataFrame(data_time)
    dataframe_tra = pd.DataFrame(data_time)
    dataframe_traum = pd.DataFrame(data_time)
    
    for sim_paras, value, name, plotname in zip(sim_parameters, sim_parameters_values, parameter_names, parameter_names_plot):
        results = dym_api.simulate(parameters=sim_paras,
                              )

        

        # ######################### Visualization ##########################
        # Tables and plots0

        for res, parameter, index in zip(results, sim_paras, value):
           
            '''''
            tsd = TimeSeriesData(res)
            tsd = tsd[[
                            "hydraulic.generation.heatPump.senT_b1.T",
                            "weaDat.weaBus.TDryBul", 
                            "building.thermalZone[1].TAir",
                            'hydraulic.generation.heatPump.senT_a1.T',
                            'hydraulic.generation.heatPump.senT_b2.T',

                            'hydraulic.generation.heatPump.innerCycle.Pel',
                            'hydraulic.generation.heatPump.innerCycle.QCon',
                            'hydraulic.generation.heatPump.innerCycle.QEva',

                            'hydraulic.control.HP_nSet_Controller.n_Set',
                            'hydraulic.control.HP_nSet_Controller.T_Set',
                            'hydraulic.control.HP_nSet_Controller.T_Meas',
                            'hydraulic.control.safetyControl.nOut',
                            'hydraulic.control.sigBusGen.hp_bus.nSet',
                            'hydraulic.control.sigBusGen.hp_bus.onOffMea',
                            'hydraulic.transfer.pressureReliefValveOnOff.m_flow',
                            'hydraulic.transfer.val[1].m_flow',
                            'hydraulic.transfer.panelHeating[1].TReturn.T'
                            #'hydraulic.transfer.senTFlowEach[1].T
                            ]]
            '''
            
            tsd2 = TimeSeriesData(res)
            tsd2 = tsd2[[
                            "hydraulic.generation.heatPump.senT_b1.T",
                            "weaDat.weaBus.TDryBul", 
                            "building.thermalZone[1].TAir",
                            'hydraulic.generation.heatPump.senT_a1.T',
                            'hydraulic.generation.heatPump.senT_b2.T',

                            'hydraulic.generation.heatPump.innerCycle.Pel',
                            'hydraulic.generation.heatPump.innerCycle.QCon',
                            'hydraulic.generation.heatPump.innerCycle.QEva',

                            'hydraulic.control.HP_nSet_Controller.n_Set',
                            'hydraulic.control.safetyControl.nOut',
                            'hydraulic.control.HP_nSet_Controller.T_Set',
                            'hydraulic.control.HP_nSet_Controller.T_Meas',
                            'hydraulic.control.sigBusGen.hp_bus.nSet',
                            'hydraulic.control.sigBusGen.hp_bus.onOffMea',
                            'hydraulic.transfer.pressureReliefValveOnOff.m_flow',
                            'hydraulic.transfer.val[1].m_flow',
                            'hydraulic.transfer.panelHeating[1].TReturn.T'
                            #'hydraulic.transfer.senTFlowEach[1].T
                            ]]

            tsd2.to_datetime_index()
            tsd2.clean_and_space_equally(desired_freq="100s")

   
            data = pd.DataFrame(tsd2)
            data.to_excel(pathlib.Path(file_dir).joinpath("parameter_study_dym", "Jahressimulation", 'Results', 'Study_'+str(name)+"_"+str(index)+'.xlsx'))

            
            on = pd.read_excel(r"D:\sgo-kwa\Repos\ParaStudy\parameter_study_dym\Jahressimulation\Results\Study_"+str(name)+r"_"+str(index)+r".xlsx", 'Sheet1', skiprows=[1,2,3])
            tsd = pd.read_excel(r"D:\sgo-kwa\Repos\ParaStudy\parameter_study_dym\Jahressimulation\Results\Study_"+str(name)+r"_"+str(index)+r".xlsx", 'Sheet1', skiprows=[1,2,3])
            counter=np.diff((on['hydraulic.control.sigBusGen.hp_bus.onOffMea'])!=0).sum()/2
            counter=int(counter)

            # daily average temp outdoor/ daily average COP / daily average on/off cycles 

            data_save_daily = pd.DataFrame({})
            daily_onoff=[]
            days=np.linspace(0, 364, num=365)  # Anzahl der Intervalle
            days = days.astype(int)

            for i in days:
                
                rows = tsd.iloc[864*i:864*(i+1)]
                counter_it=np.diff((rows['hydraulic.control.sigBusGen.hp_bus.onOffMea'])!=0).sum()/2
                counter_it=int(counter_it)
                daily_onoff.append(counter_it)
            
            daily_onoff=np.array(daily_onoff)
            daily_t_out=np.mean(np.array(tsd["weaDat.weaBus.TDryBul"]- 273.15).reshape(-1, 864), axis=1)
            data_save_daily['T_Out_da']=daily_t_out.tolist()
            daily_pel=np.mean(np.array(tsd["hydraulic.generation.heatPump.innerCycle.Pel"]/1000).reshape(-1, 864), axis=1)
            data_save_daily['Pel_da']=daily_pel.tolist()
            daily_qcon=np.mean(np.array(tsd["hydraulic.generation.heatPump.innerCycle.QCon"]/1000).reshape(-1, 864), axis=1)
            data_save_daily['QCon_da']=daily_qcon.tolist()
            daily_cop=daily_qcon/daily_pel
            data_save_daily['COP_da']=daily_cop.tolist()
            data_save_daily['OnOff_da']=daily_onoff.tolist()
            
            data_save_daily.to_excel(pathlib.Path(file_dir).joinpath("parameter_study_dym", "Jahressimulation", 'ParaStudy_Daily'+str(name)+"_"+str(index)+'.xlsx'))

            
            ########## Plotting ##########

            plt.rcParams["font.family"] = "Times New Roman"

            # Plot 1 scatter pel
            fig, ax = plt.subplots()

            ax.scatter(x = daily_t_out, y = daily_pel, color = "lightskyblue", edgecolors = "navy", s=10,  linewidths = 0.5, alpha = 0.5, marker='s', label="P$_\mathregular{el,Verd}$")
            ax.set_xlabel("Tägliche durchschnittliche Außentemperatur [°C]", fontsize=10, labelpad=5)
            ax.set_ylabel("Verdichterleistung in kW", fontsize=10, labelpad=5)
            ax.text(0.12, 0.9,str(plotname)+'='+str(index), ha='center', va='center', bbox= dict(facecolor = 'white', alpha = 1, linewidth=0.75), transform=ax.transAxes, fontsize=7)
            ax.tick_params(axis="y",direction="in", length=0, pad=5)

            y_1 = [-0.2,0,1,2,3,4,5,6,7]
            y_1_labels= ["",0,1,2,3,4,5,6,7]
            ax.set_yticks(y_1)
            ax.set_yticklabels(y_1_labels, fontsize=10)

            ax2=ax.twinx()
            ax2.scatter(x = daily_t_out, y = daily_cop, color = "lightcoral", edgecolors = "darkred", s=10, linewidths = 0.5, alpha = 0.5, marker='^', label="COP")
            
            ax2.set_ylabel("Täglicher COP", fontsize=10, labelpad=5)
            ax2.tick_params(axis="y",direction="in", length=0, pad=5)
            y_2 = [-0.2, 0, 1, 2, 3, 4, 5, 6, 7]
            y_2_labels= ["", 0, 1, 2, 3, 4, 5, 6, 7]
            ax2.set_yticks(y_2)
            ax2.set_yticklabels(y_2_labels, fontsize=10)

            ax.tick_params(axis="x",direction="in", length=0, pad=5)
            x_1 = [-10,-5,0,5,10,15,20,25,30]
            x_1_labels= [-10,-5,0,5,10,15,20,25,30]
            ax.set_xticks(x_1)
            ax.set_xticklabels(x_1_labels, fontsize=10)
            
            ax.grid(color='black',axis='both', linewidth=0.3)
            for axis in ['top','bottom','left','right']:
                ax.spines[axis].set_linewidth(1)

            fig.set_size_inches(2.95, 1.97)
            legend=fig.legend(loc="upper right", bbox_to_anchor=(1.0, 1.01), bbox_transform=ax.transAxes, fontsize=8, framealpha=1.0, frameon=True)
            frame=legend.get_frame().set_linewidth(0.75)
            color=legend.get_frame().set_edgecolor("black")

            fig.savefig(r'D:\sgo-kwa\Repos\ParaStudy\parameter_study_dym\Jahressimulation\Plots\COP\ParaStudy_Pel_'+str(name)+r'_'+str(index)+r'.png',
                    format='png',
                    dpi=1200,
                    bbox_inches='tight'
                    )

            plt.close(fig)

            # Plot 2 OnOff scatter 

            fig, ax = plt.subplots()

            ax.scatter(x = daily_t_out, y = daily_onoff, color = "palegreen", edgecolors = "darkgreen",s=10, linewidths = 0.5, alpha = 0.5, marker='s')
            ax.set_xlabel("Tägliche durchschnittliche Außentemperatur [°C]", fontsize=10, labelpad=5)
            ax.set_ylabel("Anzahl täglicher An/Aus-Zyklen", fontsize=10, labelpad=5)
            ax.text(0.12, 0.9,str(plotname)+'='+str(index), ha='center', va='center', bbox= dict(facecolor = 'white', alpha = 1, linewidth=0.75), transform=ax.transAxes, fontsize=7)
            ax.tick_params(axis="y",direction="in", length=0,  pad=5)

            y_1 = [0,25,50,70,100,125,150]
            y_1_labels= [0,25,50,70,100,125,150]
            #y_1 = [0,2,4,6,8,10]
            #y_1_labels= [0,2,4,6,8,10]
            ax.set_yticks(y_1)
            ax.set_yticklabels(y_1_labels,  fontsize=10)

            ax.tick_params(axis="x",direction="in", length=0, pad=5)
            x_1 = [-10,-5,0,5,10,15,20,25,30]
            x_1_labels= [-10,-5,0,5,10,15,20,25,30]
            ax.set_xticks(x_1)
            ax.set_xticklabels(x_1_labels,  fontsize=10)
            
            ax.grid(color='black',axis='both', linewidth=0.3)
            for axis in ['top','bottom','left','right']:
                ax.spines[axis].set_linewidth(1)

            fig.set_size_inches(2.95, 1.97)  
            

            fig.savefig(r'D:\sgo-kwa\Repos\ParaStudy\parameter_study_dym\Jahressimulation\Plots\OnOff\ParaStudy_OnOff_'+str(name)+r'_'+str(index)+r'.png',
                    format='png',
                    dpi=1200,
                    bbox_inches='tight'
                    )

            plt.close(fig)
            
    
            
            # average data
            pel_avg=tsd['hydraulic.generation.heatPump.innerCycle.Pel'].mean().item()
            q_con_avg=tsd['hydraulic.generation.heatPump.innerCycle.QCon'].mean().item()
            t_con_out_avg=tsd['hydraulic.generation.heatPump.senT_b1.T'].mean().item()
            t_outdoor_avg=tsd['weaDat.weaBus.TDryBul'].mean().item()
            t_con_in_avg=tsd['hydraulic.generation.heatPump.senT_a1.T'].mean().item()
            t_eva_out_avg=tsd['hydraulic.generation.heatPump.senT_b2.T'].mean().item()
            t_vl_set_avg=tsd['hydraulic.control.HP_nSet_Controller.T_Set'].mean().item()
            t_vl_meas_avg=tsd['hydraulic.control.HP_nSet_Controller.T_Meas'].mean().item()
            t_room=tsd['building.thermalZone[1].TAir'].mean().item()
            t_ra_avg=(tsd['hydraulic.control.HP_nSet_Controller.T_Set']-tsd['hydraulic.control.HP_nSet_Controller.T_Meas']).mean().item()
            cop_avg=(tsd['hydraulic.generation.heatPump.innerCycle.QCon']/tsd['hydraulic.generation.heatPump.innerCycle.Pel']).mean().item()

            #res_td = df_marks.append(new_row, ignore_index=True)
            
            res_new_avg= ({'Parameter':str(name), 'Wert':index, 'Pel':pel_avg,'Q_con':q_con_avg, "TRL":t_con_in_avg, "TVL":t_con_out_avg, "TEvaOut":t_eva_out_avg, "T_room": t_room, "TAir":t_outdoor_avg, "TVL_m":t_vl_meas_avg, "TVL_s":t_vl_set_avg, "T_ra":t_ra_avg, "COP":cop_avg, "OnOFF":counter})
            res_avg= res_avg.append(res_new_avg, ignore_index=True) 
            
            # Generate Boxplot data  
            
            new_data_tvl=on['hydraulic.generation.heatPump.senT_b1.T'].tolist()  
            dataframe_tvl[str(name)+'_'+str(index)]=new_data_tvl
           
            new_data_cop=on['hydraulic.generation.heatPump.innerCycle.QCon']/on['hydraulic.generation.heatPump.innerCycle.Pel'].tolist()
            dataframe_cop[str(name)+'_'+str(index)]=new_data_cop

            new_data_tset=on['hydraulic.control.HP_nSet_Controller.T_Set'].tolist()  
            dataframe_tset[str(name)+'_'+str(index)]=new_data_tset

            new_data_traum=on['building.thermalZone[1].TAir'].tolist()  
            dataframe_traum[str(name)+'_'+str(index)]=new_data_traum

            new_data_tra=on['hydraulic.control.HP_nSet_Controller.T_Set']-on['hydraulic.control.HP_nSet_Controller.T_Meas'].tolist()  
            dataframe_tra[str(name)+'_'+str(index)]=new_data_tra

            new_data_pel=on['hydraulic.generation.heatPump.innerCycle.Pel'].tolist()  
            dataframe_pel[str(name)+'_'+str(index)]=new_data_pel        
            
           
            
    
    
    res_avg.to_excel(pathlib.Path(file_dir).joinpath("parameter_study_dym", "Jahressimulation", 'ParaStudyAveragesNew_'+str(name)+'.xlsx'))

    dataframe_cop.to_excel(pathlib.Path(file_dir).joinpath("parameter_study_dym", "Jahressimulation", "Results", 'ParaStudyCOPNew_'+str(name)+'.xlsx'))
    dataframe_tvl.to_excel(pathlib.Path(file_dir).joinpath("parameter_study_dym", "Jahressimulation", "Results", 'ParaStudyTvlNew_'+str(name)+'.xlsx'))
    dataframe_tra.to_excel(pathlib.Path(file_dir).joinpath("parameter_study_dym", "Jahressimulation", "Results", 'ParaStudyTraNew_'+str(name)+'.xlsx'))
    dataframe_traum.to_excel(pathlib.Path(file_dir).joinpath("parameter_study_dym", "Jahressimulation", "Results", 'ParaStudyTraumNew_'+str(name)+'.xlsx'))
    dataframe_tset.to_excel(pathlib.Path(file_dir).joinpath("parameter_study_dym", "Jahressimulation", "Results", 'ParaStudyTsetNew_'+str(name)+'.xlsx'))
    dataframe_pel.to_excel(pathlib.Path(file_dir).joinpath("parameter_study_dym", "Jahressimulation", "Results", 'ParaStudyPelNew_'+str(name)+'.xlsx'))

    dym_api.close()
    
if __name__ == '__main__':
    main(
        
        aixlib_mo=r"D:\sgo-kwa\Repos\optisim\submodules\AixLib\AixLib\package.mo",
        BESMod_mo= r"D:\sgo-kwa\Repos\optisim\submodules\BESMod\BESMod\package.mo",
        Buildings_mo=r"D:\sgo-kwa\Repos\optisim\submodules\Buildings\Buildings\package.mo",
        IBPSA_mo=r"D:\sgo-kwa\Repos\optisim\submodules\IBPSA\IBPSA\package.mo",
        #Teaser_mo=r"D:\sgo-kwa\Repos\optisim\submodules\teaserweb_AixLib\package.mo",
        MPC_mo=r"D:\sgo-kwa\Repos\optisim\models\MPC\package.mo", 
        PCM_mo=r"D:\sgo-kwa\Repos\ebc0888_mitsubishi_pcmgoeshil_kap\Modelica\PCMgoesHIL\package.mo",
        file_dir= r"D:\sgo-kwa\Repos\ParaStudy",
        log_fmu=False,
        output_interval=100
    )