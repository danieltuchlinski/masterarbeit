within MPC;
model MA_Kevin_Model
  extends BESMod.Systems.BaseClasses.PartialBuildingEnergySystem(
    redeclare BESMod.Systems.Demand.DHW.DHW DHW(
      use_pressure=false,
      redeclare BESMod.Systems.RecordsCollection.Movers.DefaultMover pumpData,
      redeclare BESMod.Systems.Demand.DHW.TappingProfiles.calcmFlowEquStatic
        calcmFlow),
    redeclare BESMod.Systems.Electrical.DirectGridConnectionSystem electrical,
    redeclare BESMod.Systems.Demand.Building.TEASERThermalZone building(
        redeclare BESMod.Examples.BAUSimStudy.Buildings.Case_1_standard
        oneZoneParam(
        VAir=120,
        AZone=40,
        AWin={2,2,2,2},
        ATransparent={2,2,2,2},
        AExt={15,15,15,15},
        AInt=140,
        AFloor=35,
        ARoof=37)),
    redeclare BESMod.Systems.Control.NoControl control,
    redeclare BESMod.Systems.Ventilation.NoVentilation ventilation,
    redeclare BESMod.Systems.Hydraulical.HydraulicSystem hydraulic(
      redeclare BESMod.Systems.Hydraulical.Generation.HeatPumpAndHeatingRod
        generation(
        use_heaRod=true,
        redeclare model PerDataMainHP =
            AixLib.DataBase.HeatPump.PerformanceData.LookUpTable2D (dataTable=
                MPC.DataBase.Optihorst_comp_data()),
        redeclare
          BESMod.Systems.Hydraulical.Generation.RecordsCollection.DefaultHP
          heatPumpParameters(genDesTyp=BESMod.Systems.Hydraulical.Generation.Types.GenerationDesign.Monovalent,
            TBiv=266.15),
        redeclare
          BESMod.Systems.Hydraulical.Generation.RecordsCollection.DefaultHR
          heatingRodParameters(eta_hr=parameterStudy.efficiceny_heating_rod),
        redeclare BESMod.Systems.RecordsCollection.Movers.DefaultMover pumpData,
        redeclare package Medium_eva = AixLib.Media.Air),
      redeclare MPC.Controller_for_MPC control(
        HeatingRod(y=false),
        redeclare
          BESMod.Systems.Hydraulical.Control.Components.ThermostaticValveController.ThermostaticValvePIControlled
          thermostaticValveController,
        redeclare
          BESMod.Systems.Hydraulical.Control.RecordsCollection.ThermostaticValveDataDefinition
          thermostaticValveParameters,
        redeclare
          BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultSafetyControl
          safetyControl,
        redeclare
          BESMod.Systems.Hydraulical.Control.Components.HeatPumpNSetController.PI_InverterHeatPumpController
          HP_nSet_Controller(nMin=0.33),
        redeclare
          BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultBivHPControl
          bivalentControlData,
        booleanExpression1(y=true)),
      redeclare BESMod.Systems.Hydraulical.Distribution.BuildingOnly
        distribution(nParallelDem=1),
      redeclare BESMod.Systems.Hydraulical.Transfer.UFHTransferSystem
        transfer(redeclare
          BESMod.Systems.Hydraulical.Transfer.RecordsCollection.DefaultUFHData
          UFHParameters(nZones=1), redeclare
          BESMod.Systems.RecordsCollection.Movers.DefaultMover pumpData)),
    redeclare BESMod.Systems.UserProfiles.TEASERProfiles userProfiles(use_dhw=
          false,
               redeclare BESMod.Systems.Demand.DHW.RecordsCollection.NoDHW
        DHWProfile),
    redeclare
      BESMod.Examples.MyOwnHeatingRodEfficiencyStudy.SimpleStudyOfHeatingRodEfficiency
      parameterStudy,
    redeclare MPC.DataBase.MAKevinSystemParameters systemParameters);

  Modelica.Blocks.Sources.ContinuousClock Clock(offset=0, startTime=0)
    annotation (Placement(transformation(extent={{172,-60},{192,-40}})));
  Modelica.Blocks.Interfaces.RealOutput SimuTime
    annotation (Placement(transformation(extent={{222,-66},{242,-46}})));
equation
  connect(Clock.y, SimuTime) annotation (Line(points={{193,-50},{216,-50},{
          216,-56},{232,-56}}, color={0,0,127}));
  annotation (experiment(
      StopTime=2678400,
      Interval=600,
      __Dymola_Algorithm="Dassl"));
end MA_Kevin_Model;
