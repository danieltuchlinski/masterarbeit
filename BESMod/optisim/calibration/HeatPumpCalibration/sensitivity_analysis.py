# # Example 3 sensitivity analysis

# Goals of this part of the examples:
# 1. Learn how to execute a sensitivity analysis
# 2. Learn how to automatically select sensitive tuner parameters
#
# Import a valid analyzer, e.g. `SobolAnalyzer`
from aixcalibuha import SobolAnalyzer
from ebcpy import FMU_API, DymolaAPI
import pathlib
import optimization_problem

def run_sensitivity_analysis(
        file_dir="D:\sgo-kwa\Repos\optisim\calibration\heatpumpcalibration",
        n_cpu: int = 1,
        aixlib_mo="D:\sgo-kwa\Repos\optisim\submodules\AixLib\AixLib\package.mo",
        BESMod_mo="D:\sgo-kwa\Repos\optisim\submodules\BESMod\BESMod\package.mo",
        Buildings_mo="D:\sgo-kwa\Repos\optisim\submodules\Buildings\Buildings\package.mo",
        IBPSA_mo="D:\sgo-kwa\Repos\optisim\submodules\IBPSA\IBPSA\package.mo",
        #Teaser_mo="D:\sgo-kwa\Repos\optisim\submodules\teaserweb_AixLib\package.mo",
        MPC_mo="D:\sgo-kwa\Repos\optisim\models\MPC\package.mo", 
        output_interval=1,
):
    
    # ## Setup
    # Setup the class according to the documentation.
    # You just have to pass a valid simulation api and
    # some further settings for the analysis.
    # Let's thus first load the necessary simulation api:
    
    file_dir = pathlib.Path(file_dir)
    

    sim_api = DymolaAPI(
                    cd=file_dir.joinpath("testzone"),
                    model_name="MPC.HeatPump_Calibration",
                    n_cpu=n_cpu,
                    packages=[aixlib_mo, Buildings_mo, IBPSA_mo, BESMod_mo,
                    MPC_mo,],
                    show_window=True,
                    n_restart=-1,
                    equidistant_output=False,
                    get_structural_parameters=True
                   )

    sen_analyzer = SobolAnalyzer(
            sim_api=sim_api,
            num_samples=5   ,
            cd=sim_api.cd,
            analysis_variable='S1'
        )
    # Now perform the analysis for the one of the given calibration classes.
    
    def setup_calibration_classes_test(file_dir, multiple_classes=True):
        return optimization_problem.main(
            file_dir=file_dir, multiple_classes=multiple_classes
        )
    
    calibration_classes =  setup_calibration_classes_test(
         file_dir=file_dir, multiple_classes=True
    )[0]
    
    result, classes = sen_analyzer.run(calibration_classes=calibration_classes)
    print("Result of the sensitivity analysis")
    print(result)
    # For each given class, you should see the given tuner parameters
    # and the sensitivity according to the selected method from the SALib.
    # Let's remove some less sensitive parameters based on some threshold
    # to remove complexity from our calibration problem:
    print("Selecting relevant tuner-parameters using a fixed threshold:")
    sen_analyzer.select_by_threshold(calibration_classes=classes,
                                     result=result,
                                     threshold=0.001)
    for cal_class in classes:
        print(f"Class '{cal_class.name}' with parameters:\n{cal_class.tuner_paras}")
    # Return the classes and the sim_api to later perform an automated process in example 5
    return classes, sim_api

if __name__ == "__main__":
    import pathlib
    import optimization_problem
    # Parameters for sen-analysis:
    N_CPU = 1

    # Sensitivity analysis:
    run_sensitivity_analysis(

        file_dir=pathlib.Path(__file__).parent,
        n_cpu=N_CPU,
        aixlib_mo=r"D:\sgo-kwa\Repos\optisim\submodules\AixLib\AixLib\package.mo",
        BESMod_mo=r"D:\sgo-kwa\Repos\optisim\submodules\BESMod\BESMod\package.mo",
        Buildings_mo=r"D:\sgo-kwa\Repos\optisim\submodules\Buildings\Buildings\package.mo",
        IBPSA_mo=r"D:\sgo-kwa\Repos\optisim\submodules\IBPSA\IBPSA\package.mo",
        #Teaser_mo=r"D:\sgo-kwa\Repos\optisim\submodules\teaserweb_AixLib\package.mo",
        MPC_mo=r"D:\sgo-kwa\Repos\optisim\models\MPC\package.mo", 
        output_interval=1
    )
 