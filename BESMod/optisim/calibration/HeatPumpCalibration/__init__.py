"""
Module containing examples on how to use AixCaliBuHA
"""
import pathlib
import sys
from ebcpy import FMU_API
from numpy import e
import optimization_problem
from examples import e2_A_optimization_problem_definition


def setup_fmu(file_dir, example="A", n_cpu=1):
    """
    Setup the FMU used in all examples and tests.

    :param str examples_dir:
        Path to the examples folder of AixCaliBuHA
    :param str example:
        Which example to run, "A" or "B"
    :param int n_cpu:
        Number of cores to use
    """
    file_dir = pathlib.Path(file_dir)
    if example == "A":
        if "win" not in sys.platform:
            raise OSError("Can only run the example type B on windows. "
                          "Select example type A")
        model_name = file_dir.joinpath("model", "BES_sgo_fwi_kwa_fmu")

    return FMU_API(cd=file_dir.joinpath("testzone"),
                   model_name=model_name,
                   log_fmu=False,
                   n_cpu=n_cpu)


def setup_calibration_classes(file_dir, example="A", multiple_classes=True):
    """Setup the CalibrationClasses used in all examples and tests."""
    if example == "A":
        return e2_A_optimization_problem_definition.main(
            file_dir=file_dir, multiple_classes=multiple_classes
        )
    raise ValueError("Only example 'A' and 'B' are available")
