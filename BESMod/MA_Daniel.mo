﻿within ;
package MA_Daniel "Master thesis of Daniel Tuchlinski"
  extends Modelica.Icons.References;

  model WPmitFBH_ALL "Wärmepumpe mit Fußbodenheizung für FMU"

    parameter Boolean b_FMU_input=true
                                "True for FMU use and false for simulation in Dymola";
    extends BESMod.Systems.BaseClasses.PartialBuildingEnergySystem(
      redeclare BESMod.Systems.Hydraulical.HydraulicSystem hydraulic(
        redeclare MA_Daniel.AdaptedModels.HeatPump_without_HeatingRod
          generation(
          redeclare model PerDataMainHP =
              AixLib.DataBase.HeatPump.PerformanceData.LookUpTableND (
              nConv=90,
              interpMethod=SDF.Types.InterpolationMethod.Linear,
              extrapMethod=SDF.Types.ExtrapolationMethod.Linear,
              filename_Pel=ModelicaServices.ExternalReferences.loadResource(
                  "modelica://MPC/Resources/Optihorst/Optihorst_New.sdf"),
              filename_QCon=ModelicaServices.ExternalReferences.loadResource(
                  "modelica://MPC/Resources/Optihorst/Optihorst_New.sdf")),
          redeclare
            BESMod.Systems.Hydraulical.Interfaces.Generation.RecordsCollection.DefaultHP
            heatPumpParameters(genDesTyp=BESMod.Systems.Hydraulical.Interfaces.Generation.Types.GenerationDesign.Monovalent,
              VCon=0.0047),
          redeclare BESMod.Systems.RecordsCollection.Movers.DefaultMover
            pumpData),
        redeclare MA_Daniel.AdaptedModels.Controls.HeatingCurve control(
          b_FMU_input(y=b_FMU_input),
          b_MPC_input(y=b_MPC_input),
          b_u_comp_ctrl(y=b_u_comp_ctrl),
          u_T_VL(y=u_T_VL),
          u_comp(y=u_comp),
          pI_InverterHeatPumpController(
            P=0.03,                     yMax=8/9, nMin=0.34,
            T_I=180),
          safetyControl(minLocTime=600)),
        redeclare MA_Daniel.AdaptedModels.NoDistributionSystem distribution(
            nParallelDem=1, dpTraSys_nominal=hydraulic.transfer.dp_nominal),
        redeclare MA_Daniel.AdaptedModels.UFHTransferSystem_Ueberstroemventil
          transfer(redeclare MPC.DataBase.UFH_Optihorst_Case UFHParameters,
            redeclare
            BESMod.Systems.Hydraulical.Transfer.RecordsCollection.SteelRadiatorStandardPressureLossData
            transferDataBaseDefinition)),
      redeclare BESMod.Systems.Control.NoControl control,
      redeclare MPC.Submodules.Demand.DHWHil DHW(
        mDHW_flow_nominal=0,
        VDHWDay=0,
        tCrit=0,
        QCrit=0),
      redeclare package MediumDHW = IBPSA.Media.Water,
      redeclare package MediumZone = IBPSA.Media.Air,
      redeclare package MediumHyd = IBPSA.Media.Water,
      redeclare BESMod.Systems.Ventilation.NoVentilation ventilation,
      redeclare BESMod.Systems.Electrical.DirectGridConnectionSystem electrical,
      redeclare BESMod.Systems.Demand.Building.TEASERThermalZone building(
        ABui=79,
        hBui=5,
        ARoo=149.626,
        redeclare MA_Daniel.DataBases.Area158_Tabula_de oneZoneParam),
      redeclare final BESMod.Systems.RecordsCollection.ParameterStudy.NoStudy
        parameterStudy,
      redeclare final MPC.DataBase.SystemParametersMPC systemParameters(
        QBui_flow_nominal={8200},
        filNamWea=Modelica.Utilities.Files.loadResource(
            "modelica://BESMod/Resources/TRY2015_522361130393_Jahr_City_Potsdam_HeatingPeriod.mos"),
        use_hydraulic=true,
        use_ventilation=false,
        use_dhw=false,
        use_elecHeating=false),
      redeclare MA_Daniel.AdaptedModels.Profiles_HC userProfiles);

    Modelica.Blocks.Interfaces.RealInput u_T_VL( start=293.15) annotation (Placement(
          transformation(extent={{-338,-10},{-298,30}}), iconTransformation(
            extent={{-338,-10},{-298,30}})));

          Modelica.Blocks.Interfaces.BooleanInput b_MPC_input( start=true)
      annotation (Placement(transformation(extent={{-338,66},{-298,106}}),
          iconTransformation(extent={{-338,66},{-298,106}})));
    Modelica.Blocks.Interfaces.RealInput u_comp annotation (Placement(
          transformation(extent={{-338,-54},{-298,-14}}),
                                                        iconTransformation(extent={{-320,
              -40},{-280,0}})));
          Modelica.Blocks.Interfaces.BooleanInput b_u_comp_ctrl
      annotation (Placement(transformation(extent={{-338,32},{-298,72}}),
          iconTransformation(extent={{-338,32},{-298,72}})));
    Modelica.Blocks.Sources.RealExpression TErrorCalc(y=hydraulic.control.thermostaticValveController.TZoneMea[
          1])
      annotation (Placement(transformation(extent={{126,178},{176,214}})));
    Modelica.Blocks.Interfaces.RealOutput TError
      annotation (Placement(transformation(extent={{280,160},{300,180}})));
    Modelica.Blocks.Sources.CombiTimeTable variableSet(
      tableOnFile=true,
      tableName="Internals",
      fileName=ModelicaServices.ExternalReferences.loadResource(
          "modelica://BESMod/Resources/VariableSetTemperature_StandardTime.txt"),
      columns={2},
      smoothness=Modelica.Blocks.Types.Smoothness.ConstantSegments,
      extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
      timeScale=1)
      annotation (Placement(transformation(extent={{144,134},{174,164}})));
    Modelica.Blocks.Math.Add add(k2=-1)
      annotation (Placement(transformation(extent={{246,162},{266,182}})));
  equation

    connect(TErrorCalc.y, add.u1) annotation (Line(points={{178.5,196},{238,196},
            {238,178},{244,178}}, color={0,0,127}));
    connect(variableSet.y[1], add.u2) annotation (Line(points={{175.5,149},{226,
            149},{226,166},{244,166}}, color={0,0,127}));
    connect(add.y, TError) annotation (Line(points={{267,172},{274,172},{274,
            170},{290,170}}, color={0,0,127}));
    annotation (experiment(
        StopTime=600,
        Interval=600.001,
        __Dymola_Algorithm="Dassl"),
      Diagram(coordinateSystem(extent={{-280,-140},{280,200}})),
      Icon(coordinateSystem(extent={{-280,-140},{280,200}})));
  end WPmitFBH_ALL;

  model WPmitFBH_ALL_HiL "Wärmepumpe mit Fußbodenheizung für HiL"

    parameter Boolean b_FMU_input=true
                            "True for FMU use and false for simulation in Dymola";
    parameter Real startTimeSQL=9E7
                                "Start time SQL";
    extends BESMod.Systems.BaseClasses.PartialBuildingEnergySystem(
      redeclare BESMod.Systems.Hydraulical.HydraulicSystem hydraulic(
        redeclare BESMod.Systems.Hydraulical.Interfaces.Generation.NoGeneration
          generation,
        redeclare MA_Daniel.AdaptedModels.HiL.Controller_for_HiL_MPC control(
          onOffControl_HiL(
            use_minRunTime=true,
            use_minLocTime=true,
            use_runPerHou=true)),
        redeclare MA_Daniel.AdaptedModels.HiL.HiLMqttConnection_MPC
          distribution(topicsMQTT(
            n_reaVal=4,
            topicsToRea={"detHP/IndoorUnit.T_FlowOutlet_IDU",
                "hil2/HYD.Bank[8].Vdot.REAL_VAR","hil2/Sim/MPC.OptimalControl",
                "detHP/StateMachine.rps_Comp"},
            n_wriVal=3,
            topicsToPub={"hil2/Sim/T_RL","hil2/Sim/nSet",
                "hil2/Sim/outputs.building.TZone[1]"},
            staReaVal={20,1418,293.15,0},
            timConDamReaVal={1,1,1,1},
            timConDamWriVal={1,1,1},
            kReadValues={1,1/3600,1,1},
            addReadValues={273.15,0,0,0},
            kWriteValues={1,1,1},
            addWriteValues={-273.15,0,-273.15}), mQTTAndConst(startTimeSQL=
                startTimeSQL)),
        redeclare MA_Daniel.AdaptedModels.UFHTransferSystem_Ueberstroemventil
          transfer(redeclare MPC.DataBase.UFH_Optihorst_Case UFHParameters,
            redeclare
            BESMod.Systems.Hydraulical.Transfer.RecordsCollection.SteelRadiatorStandardPressureLossData
            transferDataBaseDefinition)),
      redeclare BESMod.Systems.Control.NoControl control,
      redeclare MPC.Submodules.Demand.DHWHil DHW(
        mDHW_flow_nominal=0,
        VDHWDay=0,
        tCrit=0,
        QCrit=0),
      redeclare package MediumDHW = IBPSA.Media.Water,
      redeclare package MediumZone = IBPSA.Media.Air,
      redeclare package MediumHyd = IBPSA.Media.Water,
      redeclare BESMod.Systems.Ventilation.NoVentilation ventilation,
      redeclare BESMod.Systems.Electrical.DirectGridConnectionSystem electrical,
      redeclare BESMod.Systems.Demand.Building.TEASERThermalZone building(
        ABui=79,
        hBui=5,
        ARoo=149.626,
        redeclare MA_Daniel.DataBases.Area158_Tabula_de oneZoneParam),
      redeclare final BESMod.Systems.RecordsCollection.ParameterStudy.NoStudy
        parameterStudy,
      redeclare final MPC.DataBase.SystemParametersMPC systemParameters(
        QBui_flow_nominal={8200},
        filNamWea=Modelica.Utilities.Files.loadResource(
            "modelica://BESMod/Resources/TRY2015_522361130393_Jahr_City_Potsdam_HeatingPeriod.mos"),
        use_hydraulic=true,
        use_ventilation=false,
        use_dhw=false,
        use_elecHeating=false),
      redeclare MA_Daniel.AdaptedModels.Profiles_HC userProfiles);

    Modelica.Blocks.Sources.RealExpression TErrorCalc(y=hydraulic.control.thermostaticValveController.TZoneMea[
          1])
      annotation (Placement(transformation(extent={{126,178},{176,214}})));
    Modelica.Blocks.Interfaces.RealOutput TError
      annotation (Placement(transformation(extent={{280,160},{300,180}})));
    Modelica.Blocks.Sources.CombiTimeTable variableSet(
      tableOnFile=true,
      tableName="Internals",
      fileName=ModelicaServices.ExternalReferences.loadResource(
          "modelica://BESMod/Resources/VariableSetTemperature_StandardTime.txt"),
      columns={2},
      smoothness=Modelica.Blocks.Types.Smoothness.ConstantSegments,
      extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
      timeScale=1)
      annotation (Placement(transformation(extent={{142,134},{172,164}})));
    Modelica.Blocks.Math.Add add(k2=-1)
      annotation (Placement(transformation(extent={{246,162},{266,182}})));
    Modelica.Blocks.Sources.ContinuousClock Clock
      annotation (Placement(transformation(extent={{164,-60},{184,-40}})));
    MQTT_Interface.Models.MQTTAndConst mQTTAndConst(
      nWriteValues=6,
      redeclare MQTT_Interface.MQTTConnectionPara.TickStackOpenstack
        mqttConParam,
      startTimeSQL=startTimeSQL,
      namesToWrite={"hil2/Sim/Sim_Time","hil2/Sim/weaDat.weaBus.TDryBul",
          "hil2/Sim/weaDat.weaBus.HDirNor","hil2/Sim/TZoneSet",
          "hil2/Sim/TFussboden","hil2/Sim/TError"},
      timeConstsDampWriteValues={1,1,1,1,1,1},
      addWriteValues={0,-273.15,0,-273.15,-273.15,0})
      annotation (Placement(transformation(extent={{266,-106},{326,-46}})));

    AixLib.BoundaryConditions.WeatherData.Bus weaBus annotation (Placement(
          transformation(extent={{138,-130},{178,-90}}), iconTransformation(
            extent={{-384,-52},{-364,-32}})));
    Modelica.Blocks.Sources.RealExpression TFussboden(y=hydraulic.transfer.panelHeating[
          1].panelHeatingSegment[3].panel_Segment1.heatCapacitor.T)
      annotation (Placement(transformation(extent={{352,-18},{402,18}})));
  equation

    connect(TErrorCalc.y, add.u1) annotation (Line(points={{178.5,196},{238,196},
            {238,178},{244,178}}, color={0,0,127}));
    connect(variableSet.y[1], add.u2) annotation (Line(points={{173.5,149},{226,
            149},{226,166},{244,166}}, color={0,0,127}));
    connect(add.y, TError) annotation (Line(points={{267,172},{274,172},{274,
            170},{290,170}}, color={0,0,127}));
    connect(electrical.weaBus, weaDat.weaBus) annotation (Line(
        points={{-198,103.429},{-212,103.429},{-214,103.429},{-214,70},{-220,70}},
        color={255,204,51},
        thickness=0.5));

    connect(weaBus, electrical.weaBus) annotation (Line(
        points={{158,-110},{158,-154},{-24,-154},{-24,-118},{-214,-118},{-214,
            103.429},{-198,103.429}},
        color={255,204,51},
        thickness=0.5), Text(
        string="%first",
        index=-1,
        extent={{6,3},{6,3}},
        horizontalAlignment=TextAlignment.Left));
    connect(Clock.y, mQTTAndConst.writeValues[1]) annotation (Line(points={{185,-50},
            {258,-50},{258,-77.25},{263,-77.25}},        color={0,0,127}));
    connect(weaBus.TDryBul, mQTTAndConst.writeValues[2]) annotation (Line(
        points={{158,-110},{263,-110},{263,-76.75}},
        color={255,204,51},
        thickness=0.5), Text(
        string="%first",
        index=-1,
        extent={{-3,-6},{-3,-6}},
        horizontalAlignment=TextAlignment.Right));
    connect(weaBus.HDirNor, mQTTAndConst.writeValues[3]) annotation (Line(
        points={{158,-110},{158,-108},{263,-108},{263,-76.25}},
        color={255,204,51},
        thickness=0.5), Text(
        string="%first",
        index=-1,
        extent={{-6,3},{-6,3}},
        horizontalAlignment=TextAlignment.Right));
    connect(variableSet.y[1], mQTTAndConst.writeValues[4]) annotation (Line(
          points={{173.5,149},{214,149},{214,-75.75},{263,-75.75}},   color={0,
            0,127}));
    connect(TFussboden.y, mQTTAndConst.writeValues[5]) annotation (Line(points=
            {{404.5,0},{422,0},{422,-56},{263,-56},{263,-75.25}}, color={0,0,
            127}));
    connect(add.y, mQTTAndConst.writeValues[6]) annotation (Line(points={{267,172},
            {274,172},{274,82},{326,82},{326,-40},{260,-40},{260,-68},{258,-68},
            {258,-74.75},{263,-74.75}},color={0,0,127}));
    annotation (experiment(
        StartTime=3715200,
        StopTime=4320000,
        Interval=599.999616,
        __Dymola_Algorithm="Dassl"),
      Diagram(coordinateSystem(extent={{-280,-140},{280,200}})),
      Icon(coordinateSystem(extent={{-280,-140},{280,200}})));
  end WPmitFBH_ALL_HiL;

  model Simulate_aMPC
    WPmitFBH_ALL wPmitFBH_ALL
      annotation (Placement(transformation(extent={{-40,-8},{16,26}})));
    Modelica.Blocks.Sources.RealExpression realExpression
      annotation (Placement(transformation(extent={{-96,-12},{-76,8}})));
    Modelica.Blocks.Sources.BooleanExpression booleanExpression(y=true)
      annotation (Placement(transformation(extent={{-110,28},{-90,48}})));
    Modelica.Blocks.Sources.BooleanExpression booleanExpression1
      annotation (Placement(transformation(extent={{-126,20},{-106,40}})));
    Modelica.Blocks.Sources.CombiTimeTable variableSet(
      tableOnFile=true,
      tableName="Internals",
      fileName=ModelicaServices.ExternalReferences.loadResource(
          "modelica://BESMod/Resources/SupplyTemperature_aMPC.txt"),
      columns={2},
      smoothness=Modelica.Blocks.Types.Smoothness.ConstantSegments,
      extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
      timeScale=1)
      annotation (Placement(transformation(extent={{-150,-8},{-120,22}})));
    Modelica.Blocks.Sources.RealExpression Vorlaufsolltemperatur1(y=
          wPmitFBH_ALL.u_T_VL)
      annotation (Placement(transformation(extent={{38,90},{88,110}})));
    Modelica.Blocks.Sources.RealExpression Vorlauftemperatur1(y=wPmitFBH_ALL.hydraulic.generation.reaExpTHeaPumOut.y)
      annotation (Placement(transformation(extent={{36,72},{86,92}})));
    Modelica.Blocks.Sources.RealExpression Ruecklauftemperatur1(y=wPmitFBH_ALL.hydraulic.generation.reaExpTHeaPumIn.y)
      annotation (Placement(transformation(extent={{36,54},{86,74}})));
    Modelica.Blocks.Sources.RealExpression Zonensolltemperatur1(y=wPmitFBH_ALL.variableSet.y[
          1]) annotation (Placement(transformation(extent={{36,36},{86,56}})));
    Modelica.Blocks.Sources.RealExpression Zonentemperatur1(y=wPmitFBH_ALL.hydraulic.control.thermostaticValveController.TZoneMea[
          1]) annotation (Placement(transformation(extent={{36,16},{86,36}})));
    Modelica.Blocks.Sources.RealExpression Fussbodentemperatur1(y=wPmitFBH_ALL.hydraulic.transfer.panelHeating[
          1].panelHeatingSegment[3].panel_Segment1.heatCapacitor.T)
      annotation (Placement(transformation(extent={{36,-2},{86,18}})));
    Modelica.Blocks.Sources.RealExpression TError1(y=wPmitFBH_ALL.TError)
      annotation (Placement(transformation(extent={{36,-16},{86,4}})));
    Modelica.Blocks.Sources.RealExpression Drehzahl1(y=wPmitFBH_ALL.hydraulic.control.securityControl.nOut)
      annotation (Placement(transformation(extent={{36,-34},{86,-14}})));
    Modelica.Blocks.Sources.RealExpression Sonneneinstrahlung1(y=wPmitFBH_ALL.weaDat.souSelRad.HDirNor)
      annotation (Placement(transformation(extent={{36,-66},{86,-46}})));
    Modelica.Blocks.Sources.RealExpression Umgebungstemperatur1(y=wPmitFBH_ALL.weaDat.cheTemDryBul.TDryBul)
      annotation (Placement(transformation(extent={{36,-50},{86,-30}})));
    Modelica.Blocks.Sources.RealExpression ElektrischeLeistung1(y=wPmitFBH_ALL.hydraulic.generation.heatPump.innerCycle.Pel)
      annotation (Placement(transformation(extent={{36,-80},{86,-60}})));
    Modelica.Blocks.Sources.RealExpression ElektrischeEnergie1(y=wPmitFBH_ALL.hydraulic.generation.KPIWel.integrator2.y)
      annotation (Placement(transformation(extent={{36,-92},{86,-72}})));
    Modelica.Blocks.Sources.RealExpression Heizleistung1(y=wPmitFBH_ALL.hydraulic.generation.heatPump.innerCycle.QCon)
      annotation (Placement(transformation(extent={{38,-120},{88,-100}})));
    Modelica.Blocks.Sources.RealExpression Heizenergie1(y=wPmitFBH_ALL.hydraulic.generation.KPIQHP.integrator2.y)
      annotation (Placement(transformation(extent={{38,-106},{88,-86}})));
    Modelica.Blocks.Interfaces.RealOutput Vorlaufsolltemperatur
      annotation (Placement(transformation(extent={{116,90},{136,110}})));
    Modelica.Blocks.Interfaces.RealOutput Vorlauftemperatur
      annotation (Placement(transformation(extent={{116,72},{136,92}})));
    Modelica.Blocks.Interfaces.RealOutput Ruecklauftemperatur
      annotation (Placement(transformation(extent={{116,54},{136,74}})));
    Modelica.Blocks.Interfaces.RealOutput Zonensolltemperatur
      annotation (Placement(transformation(extent={{116,36},{136,56}})));
    Modelica.Blocks.Interfaces.RealOutput Zonentemperatur
      annotation (Placement(transformation(extent={{116,16},{136,36}})));
    Modelica.Blocks.Interfaces.RealOutput Fussbodentemperatur
      annotation (Placement(transformation(extent={{116,-2},{136,18}})));
    Modelica.Blocks.Interfaces.RealOutput TError
      annotation (Placement(transformation(extent={{116,-16},{136,4}})));
    Modelica.Blocks.Interfaces.RealOutput Drehzahl
      annotation (Placement(transformation(extent={{116,-34},{136,-14}})));
    Modelica.Blocks.Interfaces.RealOutput Umgebungstemperatur
      annotation (Placement(transformation(extent={{116,-50},{136,-30}})));
    Modelica.Blocks.Interfaces.RealOutput Sonneneinstrahlung
      annotation (Placement(transformation(extent={{116,-68},{136,-48}})));
    Modelica.Blocks.Interfaces.RealOutput ElektrischeLeistung
      annotation (Placement(transformation(extent={{116,-80},{136,-60}})));
    Modelica.Blocks.Interfaces.RealOutput ElektrischeEnergie
      annotation (Placement(transformation(extent={{116,-94},{136,-74}})));
    Modelica.Blocks.Interfaces.RealOutput Heizenergie
      annotation (Placement(transformation(extent={{116,-106},{136,-86}})));
    Modelica.Blocks.Interfaces.RealOutput Heizleistung
      annotation (Placement(transformation(extent={{116,-120},{136,-100}})));
    Modelica.Blocks.Sources.RealExpression Switches1(y=wPmitFBH_ALL.hydraulic.generation.KPISwitches.triggeredAdd.y)
      annotation (Placement(transformation(extent={{38,110},{88,130}})));
    Modelica.Blocks.Interfaces.RealOutput Switches
      annotation (Placement(transformation(extent={{116,110},{136,130}})));
  equation
    connect(realExpression.y, wPmitFBH_ALL.u_comp) annotation (Line(points={{-75,-2},
            {-52,-2},{-52,4},{-42,4}},         color={0,0,127}));
    connect(booleanExpression.y, wPmitFBH_ALL.b_MPC_input) annotation (Line(
          points={{-89,38},{-52,38},{-52,14.6},{-43.8,14.6}}, color={255,0,255}));
    connect(booleanExpression1.y, wPmitFBH_ALL.b_u_comp_ctrl) annotation (Line(
          points={{-105,30},{-105,14},{-54,14},{-54,11.2},{-43.8,11.2}}, color=
            {255,0,255}));
    connect(variableSet.y[1], wPmitFBH_ALL.u_T_VL) annotation (Line(points={{-118.5,
            7},{-118.5,6},{-100,6},{-100,12},{-58,12},{-58,7},{-43.8,7}},
          color={0,0,127}));
    connect(Ruecklauftemperatur, Ruecklauftemperatur)
      annotation (Line(points={{126,64},{126,64}}, color={0,0,127}));
    connect(Vorlaufsolltemperatur1.y, Vorlaufsolltemperatur)
      annotation (Line(points={{90.5,100},{126,100}}, color={0,0,127}));
    connect(Vorlauftemperatur1.y, Vorlauftemperatur)
      annotation (Line(points={{88.5,82},{126,82}}, color={0,0,127}));
    connect(Ruecklauftemperatur1.y, Ruecklauftemperatur)
      annotation (Line(points={{88.5,64},{126,64}}, color={0,0,127}));
    connect(Zonensolltemperatur1.y, Zonensolltemperatur)
      annotation (Line(points={{88.5,46},{126,46}}, color={0,0,127}));
    connect(Zonentemperatur1.y, Zonentemperatur)
      annotation (Line(points={{88.5,26},{126,26}}, color={0,0,127}));
    connect(Fussbodentemperatur1.y, Fussbodentemperatur)
      annotation (Line(points={{88.5,8},{126,8}}, color={0,0,127}));
    connect(TError, TError1.y)
      annotation (Line(points={{126,-6},{88.5,-6}}, color={0,0,127}));
    connect(Drehzahl1.y, Drehzahl)
      annotation (Line(points={{88.5,-24},{126,-24}}, color={0,0,127}));
    connect(Umgebungstemperatur1.y, Umgebungstemperatur)
      annotation (Line(points={{88.5,-40},{126,-40}}, color={0,0,127}));
    connect(Sonneneinstrahlung1.y, Sonneneinstrahlung) annotation (Line(points=
            {{88.5,-56},{112,-56},{112,-58},{126,-58}}, color={0,0,127}));
    connect(ElektrischeLeistung1.y, ElektrischeLeistung)
      annotation (Line(points={{88.5,-70},{126,-70}}, color={0,0,127}));
    connect(ElektrischeEnergie1.y, ElektrischeEnergie) annotation (Line(points=
            {{88.5,-82},{112,-82},{112,-84},{126,-84}}, color={0,0,127}));
    connect(Heizenergie1.y, Heizenergie)
      annotation (Line(points={{90.5,-96},{126,-96}}, color={0,0,127}));
    connect(Heizleistung1.y, Heizleistung)
      annotation (Line(points={{90.5,-110},{126,-110}}, color={0,0,127}));
    connect(Switches1.y, Switches) annotation (Line(points={{90.5,120},{90.5,
            114},{110,114},{110,120},{126,120}}, color={0,0,127}));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
          coordinateSystem(preserveAspectRatio=false)),
      experiment(
        StopTime=21772800,
        Interval=59.9999616,
        __Dymola_Algorithm="Dassl"));
  end Simulate_aMPC;

  model Simulate_ddMPC
    WPmitFBH_ALL wPmitFBH_ALL
      annotation (Placement(transformation(extent={{-46,-12},{10,22}})));
    Modelica.Blocks.Sources.RealExpression realExpression
      annotation (Placement(transformation(extent={{-102,-16},{-82,4}})));
    Modelica.Blocks.Sources.BooleanExpression booleanExpression(y=true)
      annotation (Placement(transformation(extent={{-116,24},{-96,44}})));
    Modelica.Blocks.Sources.BooleanExpression booleanExpression1
      annotation (Placement(transformation(extent={{-132,16},{-112,36}})));
    Modelica.Blocks.Sources.CombiTimeTable variableSet(
      tableOnFile=true,
      tableName="Internals",
      fileName=ModelicaServices.ExternalReferences.loadResource(
          "modelica://BESMod/Resources/SupplyTemperature_ddMPC.txt"),
      columns={2},
      smoothness=Modelica.Blocks.Types.Smoothness.ConstantSegments,
      extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
      timeScale=1)
      annotation (Placement(transformation(extent={{-156,-12},{-126,18}})));
    Modelica.Blocks.Sources.RealExpression Vorlaufsolltemperatur1(y=
          wPmitFBH_ALL.u_T_VL)
      annotation (Placement(transformation(extent={{22,82},{72,102}})));
    Modelica.Blocks.Sources.RealExpression Vorlauftemperatur1(y=wPmitFBH_ALL.hydraulic.generation.reaExpTHeaPumOut.y)
      annotation (Placement(transformation(extent={{20,62},{70,82}})));
    Modelica.Blocks.Sources.RealExpression Ruecklauftemperatur1(y=wPmitFBH_ALL.hydraulic.generation.reaExpTHeaPumIn.y)
      annotation (Placement(transformation(extent={{20,44},{70,64}})));
    Modelica.Blocks.Sources.RealExpression Zonensolltemperatur1(y=wPmitFBH_ALL.variableSet.y[
          1]) annotation (Placement(transformation(extent={{20,26},{70,46}})));
    Modelica.Blocks.Sources.RealExpression Zonentemperatur1(y=wPmitFBH_ALL.hydraulic.control.thermostaticValveController.TZoneMea[
          1]) annotation (Placement(transformation(extent={{20,6},{70,26}})));
    Modelica.Blocks.Sources.RealExpression Fussbodentemperatur1(y=wPmitFBH_ALL.hydraulic.transfer.panelHeating[
          1].panelHeatingSegment[3].panel_Segment1.heatCapacitor.T)
      annotation (Placement(transformation(extent={{20,-12},{70,8}})));
    Modelica.Blocks.Sources.RealExpression TError1(y=wPmitFBH_ALL.TError)
      annotation (Placement(transformation(extent={{20,-26},{70,-6}})));
    Modelica.Blocks.Sources.RealExpression Drehzahl1(y=wPmitFBH_ALL.hydraulic.control.securityControl.nOut)
      annotation (Placement(transformation(extent={{20,-44},{70,-24}})));
    Modelica.Blocks.Sources.RealExpression Sonneneinstrahlung1(y=wPmitFBH_ALL.weaDat.souSelRad.HDirNor)
      annotation (Placement(transformation(extent={{20,-76},{70,-56}})));
    Modelica.Blocks.Sources.RealExpression Umgebungstemperatur1(y=wPmitFBH_ALL.weaDat.cheTemDryBul.TDryBul)
      annotation (Placement(transformation(extent={{20,-60},{70,-40}})));
    Modelica.Blocks.Sources.RealExpression ElektrischeLeistung1(y=wPmitFBH_ALL.hydraulic.generation.heatPump.innerCycle.Pel)
      annotation (Placement(transformation(extent={{20,-90},{70,-70}})));
    Modelica.Blocks.Sources.RealExpression ElektrischeEnergie1(y=wPmitFBH_ALL.hydraulic.generation.KPIWel.integrator2.y)
      annotation (Placement(transformation(extent={{20,-102},{70,-82}})));
    Modelica.Blocks.Sources.RealExpression Heizleistung1(y=wPmitFBH_ALL.hydraulic.generation.heatPump.innerCycle.QCon)
      annotation (Placement(transformation(extent={{22,-130},{72,-110}})));
    Modelica.Blocks.Sources.RealExpression Heizenergie1(y=wPmitFBH_ALL.hydraulic.generation.KPIQHP.integrator2.y)
      annotation (Placement(transformation(extent={{22,-116},{72,-96}})));
    Modelica.Blocks.Interfaces.RealOutput Vorlaufsolltemperatur
      annotation (Placement(transformation(extent={{100,82},{120,102}})));
    Modelica.Blocks.Interfaces.RealOutput Vorlauftemperatur
      annotation (Placement(transformation(extent={{100,62},{120,82}})));
    Modelica.Blocks.Interfaces.RealOutput Ruecklauftemperatur
      annotation (Placement(transformation(extent={{100,44},{120,64}})));
    Modelica.Blocks.Interfaces.RealOutput Zonensolltemperatur
      annotation (Placement(transformation(extent={{100,26},{120,46}})));
    Modelica.Blocks.Interfaces.RealOutput Zonentemperatur
      annotation (Placement(transformation(extent={{100,6},{120,26}})));
    Modelica.Blocks.Interfaces.RealOutput Fussbodentemperatur
      annotation (Placement(transformation(extent={{100,-12},{120,8}})));
    Modelica.Blocks.Interfaces.RealOutput TError
      annotation (Placement(transformation(extent={{100,-26},{120,-6}})));
    Modelica.Blocks.Interfaces.RealOutput Drehzahl
      annotation (Placement(transformation(extent={{100,-44},{120,-24}})));
    Modelica.Blocks.Interfaces.RealOutput Umgebungstemperatur
      annotation (Placement(transformation(extent={{100,-60},{120,-40}})));
    Modelica.Blocks.Interfaces.RealOutput Sonneneinstrahlung
      annotation (Placement(transformation(extent={{100,-78},{120,-58}})));
    Modelica.Blocks.Interfaces.RealOutput ElektrischeLeistung
      annotation (Placement(transformation(extent={{100,-90},{120,-70}})));
    Modelica.Blocks.Interfaces.RealOutput ElektrischeEnergie
      annotation (Placement(transformation(extent={{100,-104},{120,-84}})));
    Modelica.Blocks.Interfaces.RealOutput Heizenergie
      annotation (Placement(transformation(extent={{100,-116},{120,-96}})));
    Modelica.Blocks.Interfaces.RealOutput Heizleistung
      annotation (Placement(transformation(extent={{100,-130},{120,-110}})));
    Modelica.Blocks.Sources.RealExpression Switches1(y=wPmitFBH_ALL.hydraulic.generation.KPISwitches.triggeredAdd.y)
      annotation (Placement(transformation(extent={{20,100},{70,120}})));
    Modelica.Blocks.Interfaces.RealOutput Switches
      annotation (Placement(transformation(extent={{98,100},{118,120}})));
  equation
    connect(realExpression.y,wPmitFBH_ALL. u_comp) annotation (Line(points={{-81,-6},
            {-58,-6},{-58,0},{-48,0}},         color={0,0,127}));
    connect(booleanExpression.y,wPmitFBH_ALL. b_MPC_input) annotation (Line(
          points={{-95,34},{-58,34},{-58,10.6},{-49.8,10.6}}, color={255,0,255}));
    connect(booleanExpression1.y,wPmitFBH_ALL. b_u_comp_ctrl) annotation (Line(
          points={{-111,26},{-111,10},{-60,10},{-60,7.2},{-49.8,7.2}},   color=
            {255,0,255}));
    connect(variableSet.y[1],wPmitFBH_ALL. u_T_VL) annotation (Line(points={{-124.5,
            3},{-124.5,2},{-106,2},{-106,8},{-64,8},{-64,3},{-49.8,3}},
          color={0,0,127}));
    connect(Ruecklauftemperatur, Ruecklauftemperatur)
      annotation (Line(points={{110,54},{110,54}}, color={0,0,127}));
    connect(Vorlaufsolltemperatur1.y, Vorlaufsolltemperatur) annotation (Line(
          points={{74.5,92},{84,92},{84,94},{92,94},{92,92},{110,92}}, color={0,
            0,127}));
    connect(Vorlauftemperatur1.y, Vorlauftemperatur)
      annotation (Line(points={{72.5,72},{110,72}}, color={0,0,127}));
    connect(Ruecklauftemperatur1.y, Ruecklauftemperatur)
      annotation (Line(points={{72.5,54},{110,54}}, color={0,0,127}));
    connect(Zonensolltemperatur1.y, Zonensolltemperatur)
      annotation (Line(points={{72.5,36},{110,36}}, color={0,0,127}));
    connect(Zonentemperatur1.y, Zonentemperatur)
      annotation (Line(points={{72.5,16},{110,16}}, color={0,0,127}));
    connect(Fussbodentemperatur1.y, Fussbodentemperatur)
      annotation (Line(points={{72.5,-2},{110,-2}}, color={0,0,127}));
    connect(TError, TError1.y)
      annotation (Line(points={{110,-16},{72.5,-16}}, color={0,0,127}));
    connect(Drehzahl1.y, Drehzahl)
      annotation (Line(points={{72.5,-34},{110,-34}}, color={0,0,127}));
    connect(Umgebungstemperatur1.y, Umgebungstemperatur)
      annotation (Line(points={{72.5,-50},{110,-50}}, color={0,0,127}));
    connect(Sonneneinstrahlung1.y, Sonneneinstrahlung) annotation (Line(points=
            {{72.5,-66},{96,-66},{96,-68},{110,-68}}, color={0,0,127}));
    connect(ElektrischeLeistung1.y, ElektrischeLeistung)
      annotation (Line(points={{72.5,-80},{110,-80}}, color={0,0,127}));
    connect(ElektrischeEnergie1.y, ElektrischeEnergie) annotation (Line(points=
            {{72.5,-92},{96,-92},{96,-94},{110,-94}}, color={0,0,127}));
    connect(Heizenergie1.y, Heizenergie)
      annotation (Line(points={{74.5,-106},{110,-106}}, color={0,0,127}));
    connect(Heizleistung1.y, Heizleistung) annotation (Line(points={{74.5,-120},
            {90,-120},{90,-120},{110,-120}}, color={0,0,127}));
    connect(Switches1.y, Switches) annotation (Line(points={{72.5,110},{88,110},
            {88,110},{108,110}}, color={0,0,127}));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
          coordinateSystem(preserveAspectRatio=false)),
      experiment(
        StopTime=21772800,
        Interval=60,
        __Dymola_Algorithm="Dassl"));
  end Simulate_ddMPC;

  model Simulate_HeatingCurve
    WPmitFBH_ALL wPmitFBH_ALL
      annotation (Placement(transformation(extent={{-34,4},{22,38}})));
    Modelica.Blocks.Sources.RealExpression realExpression
      annotation (Placement(transformation(extent={{-90,0},{-70,20}})));
    Modelica.Blocks.Sources.BooleanExpression booleanExpression(y=false)
      annotation (Placement(transformation(extent={{-104,40},{-84,60}})));
    Modelica.Blocks.Sources.BooleanExpression booleanExpression1
      annotation (Placement(transformation(extent={{-120,32},{-100,52}})));
    Modelica.Blocks.Sources.RealExpression Vorlaufsolltemperatur1(y=
          wPmitFBH_ALL.hydraulic.control.heatingCurveDaniel.TSet)
      annotation (Placement(transformation(extent={{36,94},{86,114}})));
    Modelica.Blocks.Sources.RealExpression Vorlauftemperatur1(y=wPmitFBH_ALL.hydraulic.generation.reaExpTHeaPumOut.y)
      annotation (Placement(transformation(extent={{34,76},{84,96}})));
    Modelica.Blocks.Sources.RealExpression Ruecklauftemperatur1(y=wPmitFBH_ALL.hydraulic.generation.reaExpTHeaPumIn.y)
      annotation (Placement(transformation(extent={{34,58},{84,78}})));
    Modelica.Blocks.Sources.RealExpression Zonensolltemperatur1(y=wPmitFBH_ALL.variableSet.y[
          1]) annotation (Placement(transformation(extent={{34,40},{84,60}})));
    Modelica.Blocks.Sources.RealExpression Zonentemperatur1(y=wPmitFBH_ALL.hydraulic.control.thermostaticValveController.TZoneMea[
          1]) annotation (Placement(transformation(extent={{34,20},{84,40}})));
    Modelica.Blocks.Sources.RealExpression Fussbodentemperatur1(y=wPmitFBH_ALL.hydraulic.transfer.panelHeating[
          1].panelHeatingSegment[3].panel_Segment1.heatCapacitor.T)
      annotation (Placement(transformation(extent={{34,2},{84,22}})));
    Modelica.Blocks.Sources.RealExpression TError1(y=wPmitFBH_ALL.TError)
      annotation (Placement(transformation(extent={{34,-12},{84,8}})));
    Modelica.Blocks.Sources.RealExpression Drehzahl1(y=wPmitFBH_ALL.hydraulic.control.securityControl.nOut)
      annotation (Placement(transformation(extent={{34,-30},{84,-10}})));
    Modelica.Blocks.Sources.RealExpression Sonneneinstrahlung1(y=wPmitFBH_ALL.weaDat.souSelRad.HDirNor)
      annotation (Placement(transformation(extent={{34,-62},{84,-42}})));
    Modelica.Blocks.Sources.RealExpression Umgebungstemperatur1(y=wPmitFBH_ALL.weaDat.cheTemDryBul.TDryBul)
      annotation (Placement(transformation(extent={{34,-46},{84,-26}})));
    Modelica.Blocks.Sources.RealExpression ElektrischeLeistung1(y=wPmitFBH_ALL.hydraulic.generation.heatPump.innerCycle.Pel)
      annotation (Placement(transformation(extent={{34,-76},{84,-56}})));
    Modelica.Blocks.Sources.RealExpression ElektrischeEnergie1(y=wPmitFBH_ALL.hydraulic.generation.KPIWel.integrator2.y)
      annotation (Placement(transformation(extent={{34,-88},{84,-68}})));
    Modelica.Blocks.Sources.RealExpression Heizleistung1(y=wPmitFBH_ALL.hydraulic.generation.heatPump.innerCycle.QCon)
      annotation (Placement(transformation(extent={{36,-116},{86,-96}})));
    Modelica.Blocks.Sources.RealExpression Heizenergie1(y=wPmitFBH_ALL.hydraulic.generation.KPIQHP.integrator2.y)
      annotation (Placement(transformation(extent={{36,-102},{86,-82}})));
    Modelica.Blocks.Interfaces.RealOutput Vorlaufsolltemperatur
      annotation (Placement(transformation(extent={{114,94},{134,114}})));
    Modelica.Blocks.Interfaces.RealOutput Vorlauftemperatur
      annotation (Placement(transformation(extent={{114,76},{134,96}})));
    Modelica.Blocks.Interfaces.RealOutput Ruecklauftemperatur
      annotation (Placement(transformation(extent={{114,58},{134,78}})));
    Modelica.Blocks.Interfaces.RealOutput Zonensolltemperatur
      annotation (Placement(transformation(extent={{114,40},{134,60}})));
    Modelica.Blocks.Interfaces.RealOutput Zonentemperatur
      annotation (Placement(transformation(extent={{114,20},{134,40}})));
    Modelica.Blocks.Interfaces.RealOutput Fussbodentemperatur
      annotation (Placement(transformation(extent={{114,2},{134,22}})));
    Modelica.Blocks.Interfaces.RealOutput TError
      annotation (Placement(transformation(extent={{114,-12},{134,8}})));
    Modelica.Blocks.Interfaces.RealOutput Drehzahl
      annotation (Placement(transformation(extent={{114,-30},{134,-10}})));
    Modelica.Blocks.Interfaces.RealOutput Umgebungstemperatur
      annotation (Placement(transformation(extent={{114,-46},{134,-26}})));
    Modelica.Blocks.Interfaces.RealOutput Sonneneinstrahlung
      annotation (Placement(transformation(extent={{114,-64},{134,-44}})));
    Modelica.Blocks.Interfaces.RealOutput ElektrischeLeistung
      annotation (Placement(transformation(extent={{114,-76},{134,-56}})));
    Modelica.Blocks.Interfaces.RealOutput ElektrischeEnergie
      annotation (Placement(transformation(extent={{114,-90},{134,-70}})));
    Modelica.Blocks.Interfaces.RealOutput Heizenergie
      annotation (Placement(transformation(extent={{114,-102},{134,-82}})));
    Modelica.Blocks.Interfaces.RealOutput Heizleistung
      annotation (Placement(transformation(extent={{114,-116},{134,-96}})));
    Modelica.Blocks.Sources.RealExpression Switches1(y=wPmitFBH_ALL.hydraulic.generation.KPISwitches.triggeredAdd.y)
      annotation (Placement(transformation(extent={{36,110},{86,130}})));
    Modelica.Blocks.Interfaces.RealOutput Switches
      annotation (Placement(transformation(extent={{114,110},{134,130}})));
  equation
    connect(realExpression.y,wPmitFBH_ALL. u_comp) annotation (Line(points={{-69,10},
            {-46,10},{-46,16},{-36,16}},       color={0,0,127}));
    connect(booleanExpression.y,wPmitFBH_ALL. b_MPC_input) annotation (Line(
          points={{-83,50},{-46,50},{-46,26.6},{-37.8,26.6}}, color={255,0,255}));
    connect(booleanExpression1.y,wPmitFBH_ALL. b_u_comp_ctrl) annotation (Line(
          points={{-99,42},{-99,26},{-48,26},{-48,23.2},{-37.8,23.2}},   color=
            {255,0,255}));
    connect(realExpression.y, wPmitFBH_ALL.u_T_VL) annotation (Line(points={{
            -69,10},{-46,10},{-46,19},{-37.8,19}}, color={0,0,127}));
    connect(Ruecklauftemperatur, Ruecklauftemperatur)
      annotation (Line(points={{124,68},{124,68}}, color={0,0,127}));
    connect(Vorlaufsolltemperatur1.y, Vorlaufsolltemperatur)
      annotation (Line(points={{88.5,104},{124,104}}, color={0,0,127}));
    connect(Vorlauftemperatur1.y, Vorlauftemperatur)
      annotation (Line(points={{86.5,86},{124,86}}, color={0,0,127}));
    connect(Ruecklauftemperatur1.y, Ruecklauftemperatur)
      annotation (Line(points={{86.5,68},{124,68}}, color={0,0,127}));
    connect(Zonensolltemperatur1.y, Zonensolltemperatur)
      annotation (Line(points={{86.5,50},{124,50}}, color={0,0,127}));
    connect(Zonentemperatur1.y, Zonentemperatur)
      annotation (Line(points={{86.5,30},{124,30}}, color={0,0,127}));
    connect(Fussbodentemperatur1.y, Fussbodentemperatur)
      annotation (Line(points={{86.5,12},{124,12}}, color={0,0,127}));
    connect(TError, TError1.y)
      annotation (Line(points={{124,-2},{86.5,-2}}, color={0,0,127}));
    connect(Drehzahl1.y, Drehzahl)
      annotation (Line(points={{86.5,-20},{124,-20}}, color={0,0,127}));
    connect(Umgebungstemperatur1.y, Umgebungstemperatur)
      annotation (Line(points={{86.5,-36},{124,-36}}, color={0,0,127}));
    connect(Sonneneinstrahlung1.y, Sonneneinstrahlung) annotation (Line(points=
            {{86.5,-52},{110,-52},{110,-54},{124,-54}}, color={0,0,127}));
    connect(ElektrischeLeistung1.y, ElektrischeLeistung)
      annotation (Line(points={{86.5,-66},{124,-66}}, color={0,0,127}));
    connect(ElektrischeEnergie1.y, ElektrischeEnergie) annotation (Line(points=
            {{86.5,-78},{110,-78},{110,-80},{124,-80}}, color={0,0,127}));
    connect(Heizenergie1.y, Heizenergie)
      annotation (Line(points={{88.5,-92},{124,-92}}, color={0,0,127}));
    connect(Heizleistung1.y, Heizleistung)
      annotation (Line(points={{88.5,-106},{124,-106}}, color={0,0,127}));
    connect(Switches1.y, Switches)
      annotation (Line(points={{88.5,120},{124,120}}, color={0,0,127}));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
          coordinateSystem(preserveAspectRatio=false)),
      experiment(
        StopTime=21772800,
        Interval=59.9999616,
        __Dymola_Algorithm="Dassl"));
  end Simulate_HeatingCurve;

  package DataBases
    extends Modelica.Icons.RecordsPackage;
    record Daniel_Zone_SingleDwelling_8200W
      "Daniel_Zone_SingleDwelling_8200W"
      extends AixLib.DataBase.ThermalZones.ZoneBaseRecord(
        T_start = 293.15,
        withAirCap = true,
        VAir = 270.375,
        AZone = 108.15,
        hRad = 5.0,
        lat = 0.88645272708792,
        nOrientations = 4,
        AWin = {7.5, 7.5, 7.5, 7.5},
        ATransparent = {7.5, 7.5, 7.5, 7.5},
        hConWin = 2.7,
        RWin = 0.012,
        gWin = 0.67,
        UWin= 1.89,
        ratioWinConRad = 0.03,
        AExt = {25.5, 25.5, 25.5, 25.5},
        hConExt = 2.7,
        nExt = 1,
        RExt = {0.000267},
        RExtRem = 0.0177,
        CExt = {42877747.67263179},
        AInt = 312.5,
        hConInt = 2.7,
        nInt = 1,
        RInt = {0.000395},
        CInt = {19520893.457521748},
        AFloor = 199.5,
        hConFloor = 1.7,
        nFloor = 1,
        RFloor = {0.00011593297102929812},
        RFloorRem =  0.007927986351085511,
        CFloor = {0.007927986351085511},
        ARoof = 199.5,
        hConRoof = 1.7,
        nRoof = 1,
        RRoof = {0.00011923853034779155},
        RRoofRem = 0.009677440439263506,
        CRoof = {72989685.66456117},
        nOrientationsRoof = 1,
        tiltRoof = {0.0},
        aziRoof = {0.0},
        wfRoof = {1},
        aRoof = 0.5,
        aExt = 0.5,
        TSoil = 286.15,
        hConWallOut = 20,
        hRadWall = 5,
        hConWinOut = 20.0,
        hConRoofOut = 20.0,
        hRadRoof = 5.0,
        tiltExtWalls = {1.5707963267948966, 1.5707963267948966, 1.5707963267948966, 1.5707963267948966},
        aziExtWalls = {0.0, 1.5707963267948966, -1.5707963267948966, 3.141592653589793},
        wfWall = {0.25, 0.25, 0.25, 0.25},
        wfWin = {0.25, 0.25, 0.25, 0.25},
        wfGro = 0.0,
        specificPeople = 0.02,
        fixedHeatFlowRatePersons = 70,
        internalGainsMoistureNoPeople = 0.5,
        activityDegree = 1.2,
        ratioConvectiveHeatPeople = 0.5,
        internalGainsMachinesSpecific = 2.0,
        ratioConvectiveHeatMachines = 0.75,
        lightingPowerSpecific = 7.0,
        ratioConvectiveHeatLighting = 0.5,
        useConstantACHrate = false,
        baseACH = 0.2,
        maxUserACH = 1.0,
        maxOverheatingACH = {3.0, 2.0},
        maxSummerACH = {1.0, 283.15, 290.15},
        winterReduction = {0.2, 273.15, 283.15},
        maxIrr = {100.0, 100.0, 100.0, 100.0},
        shadingFactor = {1.0, 1.0, 1.0, 1.0},
        withAHU = false,
        minAHU = 0.3,
        maxAHU = 0.6,
        hHeat = 8056.513630624813,
        lHeat = 0,
        KRHeat = 10000,
        TNHeat = 1,
        HeaterOn = false,
        hCool = 0,
        lCool = -8056.513630624813,
        KRCool = 10000,
        TNCool = 1,
        CoolerOn = false,
        withIdealThresholds = false,
        TThresholdHeater = 288.15,
        TThresholdCooler = 295.15,
        heaLoadFacGrd=0,
        heaLoadFacOut=0);
    end Daniel_Zone_SingleDwelling_8200W;

    record Area140_SingleDwelling "Area140_SingleDwelling"
      extends AixLib.DataBase.ThermalZones.ZoneBaseRecord(
        T_start = 293.15,
        withAirCap = true,
        VAir = 350.0,
        AZone = 140.0,
        hRad = 4.999999999999999,
        lat = 0.88645272708792,
        nOrientations = 4,
        AWin = {7.0, 7.0, 7.0, 7.0},
        ATransparent = {7.0, 7.0, 7.0, 7.0},
        hConWin = 2.7,
        RWin = 0.01279317697228145,
        gWin = 0.67,
        UWin= 1.8936557576825386,
        ratioWinConRad = 0.03,
        AExt = {24.35, 24.35, 24.35, 24.35},
        hConExt = 2.7,
        nExt = 1,
        RExt = {0.0002802977869292484},
        RExtRem = 0.018542426374603946,
        CExt = {40944045.32661115},
        AInt = 291.6666666666667,
        hConInt = 2.7,
        nInt = 1,
        RInt = {0.0004237254225961449},
        CInt = {18219500.560353633},
        AFloor = 186.20000000000002,
        hConFloor = 1.7000000000000002,
        nFloor = 1,
        RFloor = {0.00012421389753139084},
        RFloorRem =  0.00849427109044876,
        CFloor = {14979653.942962203},
        ARoof = 186.20000000000002,
        hConRoof = 1.7000000000000002,
        nRoof = 1,
        RRoof = {0.00012775556822977664},
        RRoofRem = 0.010368686184925186,
        CRoof = {68123706.62025706},
        nOrientationsRoof = 1,
        tiltRoof = {0.0},
        aziRoof = {0.0},
        wfRoof = {1.0},
        aRoof = 0.5,
        aExt = 0.5,
        TSoil = 286.15,
        hConWallOut = 20.0,
        hRadWall = 5.0,
        hConWinOut = 20.0,
        hConRoofOut = 20.000000000000007,
        hRadRoof = 5.0,
        tiltExtWalls = {1.5707963267948966, 1.5707963267948966, 1.5707963267948966, 1.5707963267948966},
        aziExtWalls = {0.0, 1.5707963267948966, 3.141592653589793, -1.5707963267948966},
        wfWall = {0.25, 0.25, 0.25, 0.25},
        wfWin = {0.25, 0.25, 0.25, 0.25},
        wfGro = 0.0,
        specificPeople = 0.02,
        internalGainsMoistureNoPeople = 0.5,
        fixedHeatFlowRatePersons = 70,
        activityDegree = 1.2,
        ratioConvectiveHeatPeople = 0.5,
        internalGainsMachinesSpecific = 2.0,
        ratioConvectiveHeatMachines = 0.75,
        lightingPowerSpecific = 7.0,
        ratioConvectiveHeatLighting = 0.5,
        useConstantACHrate = false,
        baseACH = 0.2,
        maxUserACH = 1.0,
        maxOverheatingACH = {3.0, 2.0},
        maxSummerACH = {1.0, 283.15, 290.15},
        winterReduction = {0.2, 273.15, 283.15},
        maxIrr = {100.0, 100.0, 100.0, 100.0},
        shadingFactor = {1.0, 1.0, 1.0, 1.0},
        withAHU = false,
        minAHU = 0.3,
        maxAHU = 0.6,
        hHeat = 7554.556431126846,
        lHeat = 0,
        KRHeat = 10000,
        TNHeat = 1,
        HeaterOn = false,
        hCool = 0,
        lCool = -7554.556431126846,
        KRCool = 10000,
        TNCool = 1,
        CoolerOn = false,
        withIdealThresholds = false,
        TThresholdHeater = 288.15,
        TThresholdCooler = 295.15,
        heaLoadFacGrd=0,
        heaLoadFacOut=0);
    end Area140_SingleDwelling;

    record Area140_Tabula_de "Area140_SingleDwelling_Tabula_de"
      extends AixLib.DataBase.ThermalZones.ZoneBaseRecord(
        T_start = 293.15,
        withAirCap = true,
        VAir = 350.0,
        AZone = 140.0,
        hRad = 5.0,
        lat = 0.88645272708792,
        nOrientations = 4,
        AWin = {9.31, 9.31, 9.31, 9.31},
        ATransparent = {9.31, 9.31, 9.31, 9.31},
        hConWin = 2.6999999999999997,
        RWin = 0.009568093165243935,
        gWin = 0.6,
        UWin= 1.9004689468829974,
        ratioWinConRad = 0.02,
        AExt = {36.33, 36.33, 36.33, 36.33},
        hConExt = 2.7,
        nExt = 1,
        RExt = {0.0003826707183443334},
        RExtRem = 0.019975316983707962,
        CExt = {26190121.930124693},
        AInt = 291.6666666666667,
        hConInt = 2.7,
        nInt = 1,
        RInt = {0.0004237254225961449},
        CInt = {18219500.560353633},
        AFloor = 96.74,
        hConFloor = 1.7,
        nFloor = 1,
        RFloor = {0.0012600314372379813},
        RFloorRem =  0.02130127355719339,
        CFloor = {11393234.182266902},
        ARoof = 132.57999999999998,
        hConRoof = 1.7,
        nRoof = 1,
        RRoof = {0.00041986552967622},
        RRoofRem = 0.020920092991068024,
        CRoof = {2549282.139852721},
        nOrientationsRoof = 2,
        tiltRoof = {0.6108652381980153, 0.6108652381980153},
        aziRoof = {3.141592653589793, -1.5707963267948966},
        wfRoof = {0.5, 0.5},
        aRoof = 0.5,
        aExt = 0.5,
        TSoil = 286.15,
        hConWallOut = 20.0,
        hRadWall = 5.0,
        hConWinOut = 20.0,
        hConRoofOut = 19.999999999999996,
        hRadRoof = 4.999999999999999,
        tiltExtWalls = {1.5707963267948966, 1.5707963267948966, 1.5707963267948966, 1.5707963267948966},
        aziExtWalls = {0.0, 1.5707963267948966, 3.141592653589793, -1.5707963267948966},
        wfWall = {0.25, 0.25, 0.25, 0.25},
        wfWin = {0.25, 0.25, 0.25, 0.25},
        wfGro = 0.0,
        specificPeople = 0.02,
        internalGainsMoistureNoPeople = 0.5,
        fixedHeatFlowRatePersons = 70,
        activityDegree = 1.2,
        ratioConvectiveHeatPeople = 0.5,
        internalGainsMachinesSpecific = 2.0,
        ratioConvectiveHeatMachines = 0.75,
        lightingPowerSpecific = 7.0,
        ratioConvectiveHeatLighting = 0.5,
        useConstantACHrate = false,
        baseACH = 0.2,
        maxUserACH = 1.0,
        maxOverheatingACH = {3.0, 2.0},
        maxSummerACH = {1.0, 283.15, 290.15},
        winterReduction = {0.2, 273.15, 283.15},
        maxIrr = {100.0, 100.0, 100.0, 100.0},
        shadingFactor = {1.0, 1.0, 1.0, 1.0},
        withAHU = false,
        minAHU = 0.3,
        maxAHU = 0.6,
        hHeat = 6226.517917429194,
        lHeat = 0,
        KRHeat = 10000,
        TNHeat = 1,
        HeaterOn = false,
        hCool = 0,
        lCool = -6226.517917429194,
        KRCool = 10000,
        TNCool = 1,
        CoolerOn = false,
        withIdealThresholds = false,
        TThresholdHeater = 288.15,
        TThresholdCooler = 295.15,
        heaLoadFacGrd=0,
        heaLoadFacOut=0);
    end Area140_Tabula_de;

    record Area150_Tabula_de "Area150_SingleDwelling_Tabula_de"
      extends AixLib.DataBase.ThermalZones.ZoneBaseRecord(
        T_start = 293.15,
        withAirCap = true,
        VAir = 375.0,
        AZone = 150.0,
        hRad = 5.0,
        lat = 0.88645272708792,
        nOrientations = 4,
        AWin = {9.975000000000001, 9.975000000000001, 9.975000000000001, 9.975000000000001},
        ATransparent = {9.975000000000001, 9.975000000000001, 9.975000000000001, 9.975000000000001},
        hConWin = 2.7,
        RWin = 0.008930220287561005,
        gWin = 0.6,
        UWin= 1.9004689468829976,
        ratioWinConRad = 0.02,
        AExt = {38.925000000000004, 38.925000000000004, 38.925000000000004, 38.925000000000004},
        hConExt = 2.7000000000000006,
        nExt = 1,
        RExt = {0.0003571593371213777},
        RExtRem = 0.018643629184794095,
        CExt = {28060844.925133616},
        AInt = 312.5,
        hConInt = 2.7,
        nInt = 1,
        RInt = {0.0003954770610897353},
        CInt = {19520893.457521748},
        AFloor = 103.64999999999999,
        hConFloor = 1.7000000000000002,
        nFloor = 1,
        RFloor = {0.001176029341422116},
        RFloorRem =  0.019881188653380493,
        CFloor = {12207036.623857392},
        ARoof = 142.04999999999998,
        hConRoof = 1.7,
        nRoof = 1,
        RRoof = {0.00039187449436447206},
        RRoofRem = 0.019525420124996824,
        CRoof = {2731373.721270772},
        nOrientationsRoof = 2,
        tiltRoof = {0.6108652381980153, 0.6108652381980153},
        aziRoof = {3.141592653589793, -1.5707963267948966},
        wfRoof = {0.5, 0.5},
        aRoof = 0.5,
        aExt = 0.5,
        TSoil = 286.15,
        hConWallOut = 20.0,
        hRadWall = 5.0,
        hConWinOut = 20.0,
        hConRoofOut = 20.0,
        hRadRoof = 5.0,
        tiltExtWalls = {1.5707963267948966, 1.5707963267948966, 1.5707963267948966, 1.5707963267948966},
        aziExtWalls = {0.0, 1.5707963267948966, 3.141592653589793, -1.5707963267948966},
        wfWall = {0.25, 0.25, 0.25, 0.25},
        wfWin = {0.25, 0.25, 0.25, 0.25},
        wfGro = 0.0,
        specificPeople = 0.02,
        internalGainsMoistureNoPeople = 0.5,
        fixedHeatFlowRatePersons = 70,
        activityDegree = 1.2,
        ratioConvectiveHeatPeople = 0.5,
        internalGainsMachinesSpecific = 2.0,
        ratioConvectiveHeatMachines = 0.75,
        lightingPowerSpecific = 7.0,
        ratioConvectiveHeatLighting = 0.5,
        useConstantACHrate = false,
        baseACH = 0.2,
        maxUserACH = 1.0,
        maxOverheatingACH = {3.0, 2.0},
        maxSummerACH = {1.0, 283.15, 290.15},
        winterReduction = {0.2, 273.15, 283.15},
        maxIrr = {100.0, 100.0, 100.0, 100.0},
        shadingFactor = {1.0, 1.0, 1.0, 1.0},
        withAHU = false,
        minAHU = 0.3,
        maxAHU = 0.6,
        hHeat = 6671.269197245567,
        lHeat = 0,
        KRHeat = 10000,
        TNHeat = 1,
        HeaterOn = false,
        hCool = 0,
        lCool = -6671.269197245567,
        KRCool = 10000,
        TNCool = 1,
        CoolerOn = false,
        withIdealThresholds = false,
        TThresholdHeater = 288.15,
        TThresholdCooler = 295.15,
        heaLoadFacGrd=0,
        heaLoadFacOut=0);
    end Area150_Tabula_de;

    record Area160_Tabula_de "Area160_SingleDwelling_Tabula_de"
      extends AixLib.DataBase.ThermalZones.ZoneBaseRecord(
        T_start = 293.15,
        withAirCap = true,
        VAir = 400.0,
        AZone = 160.0,
        hRad = 5.000000000000001,
        lat = 0.88645272708792,
        nOrientations = 4,
        AWin = {10.64, 10.64, 10.64, 10.64},
        ATransparent = {10.64, 10.64, 10.64, 10.64},
        hConWin = 2.7,
        RWin = 0.008372081519588443,
        gWin = 0.6,
        UWin= 1.900468946882997,
        ratioWinConRad = 0.02,
        AExt = {41.52, 41.52, 41.52, 41.52},
        hConExt = 2.7,
        nExt = 1,
        RExt = {0.00033483687855129164},
        RExtRem = 0.017478402360744466,
        CExt = {29931567.920142513},
        AInt = 333.33333333333337,
        hConInt = 2.7,
        nInt = 1,
        RInt = {0.00037075974477162677},
        CInt = {20822286.35468987},
        AFloor = 110.55999999999999,
        hConFloor = 1.6999999999999997,
        nFloor = 1,
        RFloor = {0.0011025275075832338},
        RFloorRem =  0.018638614362544215,
        CFloor = {13020839.065447878},
        ARoof = 151.51999999999998,
        hConRoof = 1.7,
        nRoof = 1,
        RRoof = {0.0003673823384666925},
        RRoofRem = 0.018305081367184523,
        CRoof = {2913465.302688748},
        nOrientationsRoof = 2,
        tiltRoof = {0.6108652381980153, 0.6108652381980153},
        aziRoof = {3.141592653589793, -1.5707963267948966},
        wfRoof = {0.5, 0.5},
        aRoof = 0.5,
        aExt = 0.5,
        TSoil = 286.15,
        hConWallOut = 20.0,
        hRadWall = 5.0,
        hConWinOut = 20.0,
        hConRoofOut = 20.0,
        hRadRoof = 5.0,
        tiltExtWalls = {1.5707963267948966, 1.5707963267948966, 1.5707963267948966, 1.5707963267948966},
        aziExtWalls = {0.0, 1.5707963267948966, 3.141592653589793, -1.5707963267948966},
        wfWall = {0.25, 0.25, 0.25, 0.25},
        wfWin = {0.25, 0.25, 0.25, 0.25},
        wfGro = 0.0,
        specificPeople = 0.02,
        internalGainsMoistureNoPeople = 0.5,
        fixedHeatFlowRatePersons = 70,
        activityDegree = 1.2,
        ratioConvectiveHeatPeople = 0.5,
        internalGainsMachinesSpecific = 2.0,
        ratioConvectiveHeatMachines = 0.75,
        lightingPowerSpecific = 7.0,
        ratioConvectiveHeatLighting = 0.5,
        useConstantACHrate = false,
        baseACH = 0.2,
        maxUserACH = 1.0,
        maxOverheatingACH = {3.0, 2.0},
        maxSummerACH = {1.0, 283.15, 290.15},
        winterReduction = {0.2, 273.15, 283.15},
        maxIrr = {100.0, 100.0, 100.0, 100.0},
        shadingFactor = {1.0, 1.0, 1.0, 1.0},
        withAHU = false,
        minAHU = 0.3,
        maxAHU = 0.6,
        hHeat = 7116.020477061937,
        lHeat = 0,
        KRHeat = 10000,
        TNHeat = 1,
        HeaterOn = false,
        hCool = 0,
        lCool = -7116.020477061937,
        KRCool = 10000,
        TNCool = 1,
        CoolerOn = false,
        withIdealThresholds = false,
        TThresholdHeater = 288.15,
        TThresholdCooler = 295.15,
        heaLoadFacGrd=0,
        heaLoadFacOut=0);
    end Area160_Tabula_de;

    record Area155_Tabula_de "Area155_SingleDwelling_Tabula_de"
      extends AixLib.DataBase.ThermalZones.ZoneBaseRecord(
        T_start = 293.15,
        withAirCap = true,
        VAir = 387.5,
        AZone = 155.0,
        hRad = 4.999999999999999,
        lat = 0.88645272708792,
        nOrientations = 4,
        AWin = {10.307500000000001, 10.307500000000001, 10.307500000000001, 10.307500000000001},
        ATransparent = {10.307500000000001, 10.307500000000001, 10.307500000000001, 10.307500000000001},
        hConWin = 2.7,
        RWin = 0.008642148665381618,
        gWin = 0.6,
        UWin= 1.9004689468829972,
        ratioWinConRad = 0.02,
        AExt = {40.222500000000004, 40.222500000000004, 40.222500000000004, 40.222500000000004},
        hConExt = 2.7,
        nExt = 1,
        RExt = {0.0003456380681819785},
        RExtRem = 0.018042221791736222,
        CExt = {28996206.422638074},
        AInt = 322.91666666666663,
        hConInt = 2.7,
        nInt = 1,
        RInt = {0.00038271973653845355},
        CInt = {20171589.9061058},
        AFloor = 107.10499999999999,
        hConFloor = 1.7,
        nFloor = 1,
        RFloor = {0.0011380929110536605},
        RFloorRem =  0.019239859987142414,
        CFloor = {12613937.844652642},
        ARoof = 146.785,
        hConRoof = 1.7,
        nRoof = 1,
        RRoof = {0.0003792333816430375},
        RRoofRem = 0.018895567862900153,
        CRoof = {2822419.5119796507},
        nOrientationsRoof = 2,
        tiltRoof = {0.6108652381980153, 0.6108652381980153},
        aziRoof = {3.141592653589793, -1.5707963267948966},
        wfRoof = {0.5, 0.5},
        aRoof = 0.5,
        aExt = 0.5,
        TSoil = 286.15,
        hConWallOut = 19.999999999999996,
        hRadWall = 4.999999999999999,
        hConWinOut = 19.999999999999996,
        hConRoofOut = 19.999999999999996,
        hRadRoof = 4.999999999999999,
        tiltExtWalls = {1.5707963267948966, 1.5707963267948966, 1.5707963267948966, 1.5707963267948966},
        aziExtWalls = {0.0, 1.5707963267948966, 3.141592653589793, -1.5707963267948966},
        wfWall = {0.25, 0.25, 0.25, 0.25},
        wfWin = {0.25, 0.25, 0.25, 0.25},
        wfGro = 0.0,
        specificPeople = 0.02,
        internalGainsMoistureNoPeople = 0.5,
        fixedHeatFlowRatePersons = 70,
        activityDegree = 1.2,
        ratioConvectiveHeatPeople = 0.5,
        internalGainsMachinesSpecific = 2.0,
        ratioConvectiveHeatMachines = 0.75,
        lightingPowerSpecific = 7.0,
        ratioConvectiveHeatLighting = 0.5,
        useConstantACHrate = false,
        baseACH = 0.2,
        maxUserACH = 1.0,
        maxOverheatingACH = {3.0, 2.0},
        maxSummerACH = {1.0, 283.15, 290.15},
        winterReduction = {0.2, 273.15, 283.15},
        maxIrr = {99.99999999999999, 99.99999999999999, 99.99999999999999, 99.99999999999999},
        shadingFactor = {1.0, 1.0, 1.0, 1.0},
        withAHU = false,
        minAHU = 0.3,
        maxAHU = 0.6,
        hHeat = 6893.644837153751,
        lHeat = 0,
        KRHeat = 10000,
        TNHeat = 1,
        HeaterOn = false,
        hCool = 0,
        lCool = -6893.644837153751,
        KRCool = 10000,
        TNCool = 1,
        CoolerOn = false,
        withIdealThresholds = false,
        TThresholdHeater = 288.15,
        TThresholdCooler = 295.15,
        heaLoadFacGrd=0,
        heaLoadFacOut=0);
    end Area155_Tabula_de;

    record Area157_Tabula_de "Area157_SingleDwelling_Tabula_de"
      extends AixLib.DataBase.ThermalZones.ZoneBaseRecord(
        T_start = 293.15,
        withAirCap = true,
        VAir = 392.5,
        AZone = 157.0,
        hRad = 5.0,
        lat = 0.88645272708792,
        nOrientations = 4,
        AWin = {10.4405, 10.4405, 10.4405, 10.4405},
        ATransparent = {10.4405, 10.4405, 10.4405, 10.4405},
        hConWin = 2.7,
        RWin = 0.008532057599580579,
        gWin = 0.6,
        UWin= 1.9004689468829974,
        ratioWinConRad = 0.02,
        AExt = {40.7415, 40.7415, 40.7415, 40.7415},
        hConExt = 2.7,
        nExt = 1,
        RExt = {0.00034123503546628443},
        RExtRem = 0.01781238457145933,
        CExt = {29370351.021639843},
        AInt = 327.0833333333333,
        hConInt = 2.7,
        nInt = 1,
        RInt = {0.0003778443258819127},
        CInt = {20431868.48553934},
        AFloor = 108.487,
        hConFloor = 1.7,
        nFloor = 1,
        RFloor = {0.001123594912186735},
        RFloorRem =  0.01899476622934442,
        CFloor = {12776698.332970738},
        ARoof = 148.679,
        hConRoof = 1.7,
        nRoof = 1,
        RRoof = {0.00037440238315076944},
        RRoofRem = 0.01865485999203518,
        CRoof = {2858837.828263334},
        nOrientationsRoof = 2,
        tiltRoof = {0.6108652381980153, 0.6108652381980153},
        aziRoof = {3.141592653589793, -1.5707963267948966},
        wfRoof = {0.5, 0.5},
        aRoof = 0.5,
        aExt = 0.5,
        TSoil = 286.15,
        hConWallOut = 19.999999999999996,
        hRadWall = 4.999999999999999,
        hConWinOut = 19.999999999999996,
        hConRoofOut = 19.999999999999996,
        hRadRoof = 4.999999999999999,
        tiltExtWalls = {1.5707963267948966, 1.5707963267948966, 1.5707963267948966, 1.5707963267948966},
        aziExtWalls = {0.0, 1.5707963267948966, 3.141592653589793, -1.5707963267948966},
        wfWall = {0.25, 0.25, 0.25, 0.25},
        wfWin = {0.25, 0.25, 0.25, 0.25},
        wfGro = 0.0,
        specificPeople = 0.02,
        internalGainsMoistureNoPeople = 0.5,
        fixedHeatFlowRatePersons = 70,
        activityDegree = 1.2,
        ratioConvectiveHeatPeople = 0.5,
        internalGainsMachinesSpecific = 2.0,
        ratioConvectiveHeatMachines = 0.75,
        lightingPowerSpecific = 7.0,
        ratioConvectiveHeatLighting = 0.5,
        useConstantACHrate = false,
        baseACH = 0.2,
        maxUserACH = 1.0,
        maxOverheatingACH = {3.0, 2.0},
        maxSummerACH = {1.0, 283.15, 290.15},
        winterReduction = {0.2, 273.15, 283.15},
        maxIrr = {100.0, 100.0, 100.0, 100.0},
        shadingFactor = {1.0, 1.0, 1.0, 1.0},
        withAHU = false,
        minAHU = 0.3,
        maxAHU = 0.6,
        hHeat = 6982.5950931170255,
        lHeat = 0,
        KRHeat = 10000,
        TNHeat = 1,
        HeaterOn = false,
        hCool = 0,
        lCool = -6982.5950931170255,
        KRCool = 10000,
        TNCool = 1,
        CoolerOn = false,
        withIdealThresholds = false,
        TThresholdHeater = 288.15,
        TThresholdCooler = 295.15,
        heaLoadFacGrd=0,
        heaLoadFacOut=0);
    end Area157_Tabula_de;

    record Area158_Tabula_de "Area158_SingleDwelling_Tabula_de"
      extends AixLib.DataBase.ThermalZones.ZoneBaseRecord(
        T_start = 293.15,
        withAirCap = true,
        VAir = 395.0,
        AZone = 158.0,
        hRad = 5.0,
        lat = 0.88645272708792,
        nOrientations = 4,
        AWin = {10.507000000000001, 10.507000000000001, 10.507000000000001, 10.507000000000001},
        ATransparent = {10.507000000000001, 10.507000000000001, 10.507000000000001, 10.507000000000001},
        hConWin = 2.7,
        RWin = 0.00847805723502627,
        gWin = 0.6,
        UWin= 1.9004689468829974,
        ratioWinConRad = 0.02,
        AExt = {41.001000000000005, 41.001000000000005, 41.001000000000005, 41.001000000000005},
        hConExt = 2.7,
        nExt = 1,
        RExt = {0.0003390753200519409},
        RExtRem = 0.01769964796024756,
        CExt = {29557423.321140733},
        AInt = 329.1666666666667,
        hConInt = 2.6999999999999997,
        nInt = 1,
        RInt = {0.00037545290609784993},
        CInt = {20562007.775256246},
        AFloor = 109.178,
        hConFloor = 1.6999999999999997,
        nFloor = 1,
        RFloor = {0.0011164835519830215},
        RFloorRem =  0.01887454618991819,
        CFloor = {12858078.577129789},
        ARoof = 149.626,
        hConRoof = 1.7,
        nRoof = 1,
        RRoof = {0.0003720327478143722},
        RRoofRem = 0.018536791257908376,
        CRoof = {2877046.9864050634},
        nOrientationsRoof = 2,
        tiltRoof = {0.6108652381980153, 0.6108652381980153},
        aziRoof = {3.141592653589793, -1.5707963267948966},
        wfRoof = {0.5, 0.5},
        aRoof = 0.5,
        aExt = 0.5,
        TSoil = 286.15,
        hConWallOut = 20.0,
        hRadWall = 5.0,
        hConWinOut = 20.0,
        hConRoofOut = 19.999999999999996,
        hRadRoof = 4.999999999999999,
        tiltExtWalls = {1.5707963267948966, 1.5707963267948966, 1.5707963267948966, 1.5707963267948966},
        aziExtWalls = {0.0, 1.5707963267948966, 3.141592653589793, -1.5707963267948966},
        wfWall = {0.25, 0.25, 0.25, 0.25},
        wfWin = {0.25, 0.25, 0.25, 0.25},
        wfGro = 0.0,
        specificPeople = 0.02,
        internalGainsMoistureNoPeople = 0.5,
        fixedHeatFlowRatePersons = 70,
        activityDegree = 1.2,
        ratioConvectiveHeatPeople = 0.5,
        internalGainsMachinesSpecific = 2.0,
        ratioConvectiveHeatMachines = 0.75,
        lightingPowerSpecific = 7.0,
        ratioConvectiveHeatLighting = 0.5,
        useConstantACHrate = false,
        baseACH = 0.2,
        maxUserACH = 1.0,
        maxOverheatingACH = {3.0, 2.0},
        maxSummerACH = {1.0, 283.15, 290.15},
        winterReduction = {0.2, 273.15, 283.15},
        maxIrr = {99.99999999999999, 99.99999999999999, 99.99999999999999, 99.99999999999999},
        shadingFactor = {1.0, 1.0, 1.0, 1.0},
        withAHU = false,
        minAHU = 0.3,
        maxAHU = 0.6,
        hHeat = 7027.070221098663,
        lHeat = 0,
        KRHeat = 10000,
        TNHeat = 1,
        HeaterOn = false,
        hCool = 0,
        lCool = -7027.070221098663,
        KRCool = 10000,
        TNCool = 1,
        CoolerOn = false,
        withIdealThresholds = false,
        TThresholdHeater = 288.15,
        TThresholdCooler = 295.15,
        heaLoadFacGrd=0,
        heaLoadFacOut=0);
    end Area158_Tabula_de;
  end DataBases;

  package AdaptedModels "Adapted models for MA Daniel"
    package HiL "Models for HiL"
      model HiLMqttConnection_MPC "Distribution HiL MA Daniel"
        extends
          BESMod.Systems.Hydraulical.Distribution.BaseClasses.PartialDistribution(
          dpDem_nominal={0},
          dpSup_nominal={0},
          VStoDHW=0,
          QDHWStoLoss_flow=0,
          designType=BESMod.Systems.Hydraulical.Distribution.Types.DHWDesignType.NoStorage,
          dTTraDHW_nominal=5,
          m_flow_nominal=mDem_flow_nominal,
          dTTra_nominal={0},
          nParallelSup=1,
          nParallelDem=1);

        MQTT_Interface.Models.MQTTAndConst mQTTAndConst(
          final nWriteValues=topicsMQTT.n_wriVal,
          final nReadValues=topicsMQTT.n_reaVal,
          final mqttConParam=topicsMQTT.mqttConParam,
          startTimeSQL=9e7,
          final namesToRead=topicsMQTT.topicsToRea,
          final standardReadValues=topicsMQTT.staReaVal,
          final namesToWrite=topicsMQTT.topicsToPub,
          final timeConstsDampReadValues=topicsMQTT.timConDamReaVal,
          final timeConstsDampWriteValues=topicsMQTT.timConDamWriVal,
          final kReadValues=topicsMQTT.kReadValues,
          final addReadValues=topicsMQTT.addReadValues,
          final maxReadValues=topicsMQTT.maxReadValues,
          final minReadValues=topicsMQTT.minReadValues,
          final kWriteValues=topicsMQTT.kWriteValues,
          final addWriteValues=topicsMQTT.addWriteValues,
          final maxWriteValues=topicsMQTT.maxWriteValues,
          final minWriteValues=topicsMQTT.minWriteValues,
          final useJSON=topicsMQTT.useJSON,
          final nKeys=topicsMQTT.nKey,
          final keys=topicsMQTT.key)
          annotation (Placement(transformation(extent={{-23,-23},{23,23}},
              rotation=180,
              origin={-57,-23})));

        PCMgoesHIL.Utilities.IdealSourcePumpTemperaturewoPump idealSourcePumpTemperaturewoPump(
          redeclare final package Medium = Medium,
          energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
          energyDynamicsDelVol=Modelica.Fluid.Types.Dynamics.FixedInitial,
          TFlow_nominal=323.15,
          TRetDesign=313.15,
          m_flow_nominal=m_flow_nominal[1],
          dpExtra_nominal=0,
          redeclare AixLib.Fluid.Movers.Data.Pumps.Wilo.Stratos25slash1to4 per,
          initTypeDampTFlow=Modelica.Blocks.Types.Init.InitialOutput,
          y_start_TFlow=T_start,
          tauVolHeatUp=10,
          final p_start=p_start,
          final T_start=T_start)
          annotation (Placement(transformation(extent={{-42,10},{20,72}})));

        replaceable parameter PCMgoesHIL.Systems.Hydraulic.Distribution.HiL1PCMTopics
          topicsMQTT(
          n_reaVal=4,
          topicsToRea={"detHP/IndoorUnit.T_FlowOutlet_IDU",
              "hil2/HYD.Bank[8].Vdot.REAL_VAR","hil2/Sim/MPC.OptimalControl",
              "detHP/StateMachine.rps_Comp"},
          n_wriVal=3,
          topicsToPub={"hil2/Sim/T_RL","hil2/Sim/nSet",
              "hil2/Sim/outputs.building.TZone[1]"},
          staReaVal={20,1418,293.15,0},
          timConDamReaVal={1,1,1,1},
          timConDamWriVal={1,1,1},
          kReadValues={1,1/3600,1,1},
          addReadValues={273.15,0,0,0},
          kWriteValues={1,1,1},
          addWriteValues={-273.15,0,-273.15})
                     constrainedby
          PCMgoesHIL.Systems.Hydraulic.Distribution.BaseHiLPar
                                              annotation (Placement(transformation(
                extent={{-98,-98},{-78,-78}})), choicesAllMatching=true);

        BESMod.Systems.Demand.DHW.RecordsCollection.ProfileL DHWProfile
          annotation (Placement(transformation(extent={{-66,-98},{-46,-78}})));
        Modelica.Blocks.Sources.Constant m_flow_nom(k=0.393889)
          annotation (Placement(transformation(extent={{-44,84},{-28,100}})));
        BESMod.Utilities.Electrical.ZeroLoad zeroLoad
          annotation (Placement(transformation(extent={{28,-124},{48,-104}})));
        Modelica.Fluid.Sources.Boundary_pT BouGenSin(redeclare package Medium =
              Medium, nPorts=1) annotation (Placement(transformation(
              extent={{-10,-10},{10,10}},
              rotation=270,
              origin={-80,102})));
        Modelica.Fluid.Sources.MassFlowSource_T bouSouGen(redeclare package
            Medium =
              Medium, nPorts=nParallelSup)
          annotation (Placement(transformation(extent={{-92,46},{-72,66}})));
        IBPSA.Fluid.Sensors.TemperatureTwoPort senTBuiRet(
          redeclare final package Medium = Medium,
          final allowFlowReversal=allowFlowReversal,
          m_flow_nominal=m_flow_nominal[1],
          tau=temperatureSensorData.tau,
          initType=temperatureSensorData.initType,
          T_start=T_start,
          final transferHeat=temperatureSensorData.transferHeat,
          TAmb=temperatureSensorData.TAmb,
          tauHeaTra=temperatureSensorData.tauHeaTra)
          "Temperature at supply for building" annotation (Placement(transformation(
              extent={{10,10},{-10,-10}},
              rotation=180,
              origin={58,48})));
        IBPSA.Fluid.Sensors.TemperatureTwoPort senTBuiSup(
          redeclare final package Medium = Medium,
          final allowFlowReversal=allowFlowReversal,
          m_flow_nominal=m_flow_nominal[1],
          tau=temperatureSensorData.tau,
          initType=temperatureSensorData.initType,
          T_start=T_start,
          final transferHeat=true,
          TAmb=TAmb,
          tauHeaTra=temperatureSensorData.tauHeaTra)
          "Temperature at supply for building" annotation (Placement(transformation(
              extent={{10,10},{-10,-10}},
              rotation=180,
              origin={58,78})));
        BESMod.Systems.RecordsCollection.TemperatureSensors.DefaultSensor
          temperatureSensorData
          annotation (Placement(transformation(extent={{74,6},{94,26}})));
      equation
        connect(mQTTAndConst.activeSQL, idealSourcePumpTemperaturewoPump.switchToSQL)
          annotation (Line(points={{-82.3,-16.1},{-88,-16.1},{-88,41},{-45.1,41}},
              color={255,0,255}));
        connect(idealSourcePumpTemperaturewoPump.TReturnMix, mQTTAndConst.writeValues[
          1]) annotation (Line(points={{23.1,19.3},{23.1,-14},{-8,-14},{-8,-23},{-31.7,
                -23}}, color={0,0,127}));
        connect(portDHW_in, portDHW_in)
          annotation (Line(points={{100,-82},{100,-82}}, color={0,127,255}));
        connect(idealSourcePumpTemperaturewoPump.TFlowHeaCur, sigBusDistr.TFlowHeaCur)
          annotation (Line(points={{-45.1,19.3},{-66,19.3},{-66,80},{0,80},{0,101}},
              color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(m_flow_nom.y, idealSourcePumpTemperaturewoPump.m_flow_in_withoutSQL)
          annotation (Line(points={{-27.2,92},{-26,92},{-26,76},{-54,76},{-54,62.7},{
                -45.1,62.7}}, color={0,0,127}));
        connect(zeroLoad.internalElectricalPin, internalElectricalPin) annotation (
            Line(
            points={{48,-114},{56,-114},{56,-84},{70,-84},{70,-98}},
            color={0,0,0},
            thickness=1));
        connect(portDHW_out, portDHW_in) annotation (Line(points={{100,-22},{74,-22},
                {74,-82},{100,-82}}, color={0,127,255}));
        connect(bouSouGen.ports, portGen_out) annotation (Line(points={{-72,56},{-64,
                56},{-64,40},{-100,40}}, color={0,127,255}));
        connect(BouGenSin.ports[1], portGen_in[1])
          annotation (Line(points={{-80,92},{-80,80},{-100,80}}, color={0,127,255}));
        connect(idealSourcePumpTemperaturewoPump.port_b, senTBuiSup.port_a)
          annotation (Line(points={{20,65.8},{26,65.8},{26,78},{48,78}}, color={0,127,
                255}));
        connect(senTBuiSup.port_b, portBui_out[1]) annotation (Line(points={{68,78},{
                84,78},{84,80},{100,80}}, color={0,127,255}));
        connect(idealSourcePumpTemperaturewoPump.port_a, senTBuiRet.port_a)
          annotation (Line(points={{20,56.5},{32,56.5},{32,48},{48,48}}, color={0,127,
                255}));
        connect(senTBuiRet.port_b, portBui_in[1]) annotation (Line(points={{68,48},{
                74,48},{74,50},{84,50},{84,40},{100,40}}, color={0,127,255}));
        connect(senTBuiSup.T, sigBusDistr.TBuiSupMea) annotation (Line(points={{58,89},
                {60,89},{60,101},{0,101}}, color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{6,3},{6,3}},
            horizontalAlignment=TextAlignment.Left));
        connect(senTBuiRet.T, sigBusDistr.TBuiRetMea) annotation (Line(points={{58,59},
                {60,59},{60,66},{32,66},{32,101},{0,101}}, color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{6,3},{6,3}},
            horizontalAlignment=TextAlignment.Left));
        connect(mQTTAndConst.activeSQL, sigBusDistr.bSQL) annotation (Line(points={{
                -82.3,-16.1},{-88,-16.1},{-88,40},{-52,40},{-52,74},{-24,74},{-24,76},
                {0,76},{0,101}}, color={255,0,255}), Text(
            string="%second",
            index=1,
            extent={{-3,6},{-3,6}},
            horizontalAlignment=TextAlignment.Right));
        connect(mQTTAndConst.writeValues[2], sigBusDistr.nSetByPI) annotation (
            Line(points={{-31.7,-23},{0,-23},{0,101}}, color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{6,3},{6,3}},
            horizontalAlignment=TextAlignment.Left));
        connect(mQTTAndConst.readValues[1], sigBusDistr.TSupplyReal)
          annotation (Line(points={{-82.3,-23},{-110,-23},{-110,101},{0,101}},
              color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(mQTTAndConst.readValues[3], sigBusDistr.TSupplySet) annotation (
           Line(points={{-82.3,-23},{-118,-23},{-118,101},{0,101}}, color={0,0,
                127}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(mQTTAndConst.readValues[4], sigBusDistr.nReal) annotation (Line(
              points={{-82.3,-23},{-132,-23},{-132,101},{0,101}}, color={0,0,
                127}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(idealSourcePumpTemperaturewoPump.m_flow_in_withSQL,
          mQTTAndConst.readValues[2]) annotation (Line(points={{-45.1,53.4},{
                -66,53.4},{-66,0},{-82.3,0},{-82.3,-23}}, color={0,0,127}));
        connect(idealSourcePumpTemperaturewoPump.TFlow, mQTTAndConst.readValues[
          1]) annotation (Line(points={{-45.1,28.6},{-100,28.6},{-100,-16},{
                -82.3,-16},{-82.3,-23}}, color={0,0,127}));
        connect(sigBusDistr.TZoneMea, mQTTAndConst.writeValues[3]) annotation (
            Line(
            points={{0,101},{0,-23},{-31.7,-23}},
            color={255,204,51},
            thickness=0.5), Text(
            string="%first",
            index=-1,
            extent={{6,3},{6,3}},
            horizontalAlignment=TextAlignment.Left));
        connect(idealSourcePumpTemperaturewoPump.TReturnMix, sigBusDistr.T_Ruecklauf)
          annotation (Line(points={{23.1,19.3},{50,19.3},{50,56},{0,56},{0,101}},
              color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{6,3},{6,3}},
            horizontalAlignment=TextAlignment.Left));
      end HiLMqttConnection_MPC;

      model HiLMqttConnection_HC "Distribution HiL MA Daniel"
        extends
          BESMod.Systems.Hydraulical.Distribution.BaseClasses.PartialDistribution(
          dpDem_nominal={0},
          dpSup_nominal={0},
          VStoDHW=0,
          QDHWStoLoss_flow=0,
          designType=BESMod.Systems.Hydraulical.Distribution.Types.DHWDesignType.NoStorage,
          dTTraDHW_nominal=5,
          m_flow_nominal=mDem_flow_nominal,
          dTTra_nominal={0},
          nParallelSup=1,
          nParallelDem=1);

        MQTT_Interface.Models.MQTTAndConst mQTTAndConst(
          final nWriteValues=topicsMQTT.n_wriVal,
          final nReadValues=topicsMQTT.n_reaVal,
          final mqttConParam=topicsMQTT.mqttConParam,
          startTimeSQL=9e7,
          final namesToRead=topicsMQTT.topicsToRea,
          final standardReadValues=topicsMQTT.staReaVal,
          final namesToWrite=topicsMQTT.topicsToPub,
          final timeConstsDampReadValues=topicsMQTT.timConDamReaVal,
          final timeConstsDampWriteValues=topicsMQTT.timConDamWriVal,
          final kReadValues=topicsMQTT.kReadValues,
          final addReadValues=topicsMQTT.addReadValues,
          final maxReadValues=topicsMQTT.maxReadValues,
          final minReadValues=topicsMQTT.minReadValues,
          final kWriteValues=topicsMQTT.kWriteValues,
          final addWriteValues=topicsMQTT.addWriteValues,
          final maxWriteValues=topicsMQTT.maxWriteValues,
          final minWriteValues=topicsMQTT.minWriteValues,
          final useJSON=topicsMQTT.useJSON,
          final nKeys=topicsMQTT.nKey,
          final keys=topicsMQTT.key)
          annotation (Placement(transformation(extent={{-23,-23},{23,23}},
              rotation=180,
              origin={-57,-23})));

        PCMgoesHIL.Utilities.IdealSourcePumpTemperaturewoPump idealSourcePumpTemperaturewoPump(
          redeclare final package Medium = Medium,
          energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
          energyDynamicsDelVol=Modelica.Fluid.Types.Dynamics.FixedInitial,
          TFlow_nominal=323.15,
          TRetDesign=313.15,
          m_flow_nominal=m_flow_nominal[1],
          dpExtra_nominal=0,
          redeclare AixLib.Fluid.Movers.Data.Pumps.Wilo.Stratos25slash1to4 per,
          initTypeDampTFlow=Modelica.Blocks.Types.Init.InitialOutput,
          y_start_TFlow=T_start,
          tauVolHeatUp=10,
          final p_start=p_start,
          final T_start=T_start)
          annotation (Placement(transformation(extent={{-42,10},{20,72}})));

        replaceable parameter PCMgoesHIL.Systems.Hydraulic.Distribution.HiL1PCMTopics
          topicsMQTT(
          n_reaVal=3,
          topicsToRea={"detHP/IndoorUnit.T_FlowOutlet_IDU",
              "hil2/HYD.Bank[8].Vdot.REAL_VAR","detHP/StateMachine.rps_Comp"},
          n_wriVal=3,
          topicsToPub={"hil2/Sim/T_RL","hil2/Sim/nSet",
              "hil2/Sim/outputs.building.TZone[1]"},
          staReaVal={20,1418,0},
          timConDamReaVal={1,1,1},
          timConDamWriVal={1,1,1},
          kReadValues={1,1/3600,1},
          addReadValues={273.15,0,0},
          kWriteValues={1,1,1},
          addWriteValues={-273.15,0,-273.15})
                     constrainedby
          PCMgoesHIL.Systems.Hydraulic.Distribution.BaseHiLPar
                                              annotation (Placement(transformation(
                extent={{-98,-98},{-78,-78}})), choicesAllMatching=true);

        BESMod.Systems.Demand.DHW.RecordsCollection.ProfileL DHWProfile
          annotation (Placement(transformation(extent={{-66,-98},{-46,-78}})));
        Modelica.Blocks.Sources.Constant m_flow_nom(k=0.393889)
          annotation (Placement(transformation(extent={{-44,84},{-28,100}})));
        BESMod.Utilities.Electrical.ZeroLoad zeroLoad
          annotation (Placement(transformation(extent={{28,-124},{48,-104}})));
        Modelica.Fluid.Sources.Boundary_pT BouGenSin(redeclare package Medium =
              Medium, nPorts=1) annotation (Placement(transformation(
              extent={{-10,-10},{10,10}},
              rotation=270,
              origin={-80,102})));
        Modelica.Fluid.Sources.MassFlowSource_T bouSouGen(redeclare package
            Medium =
              Medium, nPorts=nParallelSup)
          annotation (Placement(transformation(extent={{-92,46},{-72,66}})));
        IBPSA.Fluid.Sensors.TemperatureTwoPort senTBuiRet(
          redeclare final package Medium = Medium,
          final allowFlowReversal=allowFlowReversal,
          m_flow_nominal=m_flow_nominal[1],
          tau=temperatureSensorData.tau,
          initType=temperatureSensorData.initType,
          T_start=T_start,
          final transferHeat=temperatureSensorData.transferHeat,
          TAmb=temperatureSensorData.TAmb,
          tauHeaTra=temperatureSensorData.tauHeaTra)
          "Temperature at supply for building" annotation (Placement(transformation(
              extent={{10,10},{-10,-10}},
              rotation=180,
              origin={58,48})));
        IBPSA.Fluid.Sensors.TemperatureTwoPort senTBuiSup(
          redeclare final package Medium = Medium,
          final allowFlowReversal=allowFlowReversal,
          m_flow_nominal=m_flow_nominal[1],
          tau=temperatureSensorData.tau,
          initType=temperatureSensorData.initType,
          T_start=T_start,
          final transferHeat=true,
          TAmb=TAmb,
          tauHeaTra=temperatureSensorData.tauHeaTra)
          "Temperature at supply for building" annotation (Placement(transformation(
              extent={{10,10},{-10,-10}},
              rotation=180,
              origin={58,78})));
        BESMod.Systems.RecordsCollection.TemperatureSensors.DefaultSensor
          temperatureSensorData
          annotation (Placement(transformation(extent={{74,6},{94,26}})));
      equation
        connect(mQTTAndConst.activeSQL, idealSourcePumpTemperaturewoPump.switchToSQL)
          annotation (Line(points={{-82.3,-16.1},{-88,-16.1},{-88,41},{-45.1,41}},
              color={255,0,255}));
        connect(idealSourcePumpTemperaturewoPump.TReturnMix, mQTTAndConst.writeValues[
          1]) annotation (Line(points={{23.1,19.3},{23.1,-14},{-8,-14},{-8,-23},{-31.7,
                -23}}, color={0,0,127}));
        connect(portDHW_in, portDHW_in)
          annotation (Line(points={{100,-82},{100,-82}}, color={0,127,255}));
        connect(idealSourcePumpTemperaturewoPump.TFlowHeaCur, sigBusDistr.TFlowHeaCur)
          annotation (Line(points={{-45.1,19.3},{-66,19.3},{-66,80},{0,80},{0,101}},
              color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(m_flow_nom.y, idealSourcePumpTemperaturewoPump.m_flow_in_withoutSQL)
          annotation (Line(points={{-27.2,92},{-26,92},{-26,76},{-54,76},{-54,62.7},{
                -45.1,62.7}}, color={0,0,127}));
        connect(zeroLoad.internalElectricalPin, internalElectricalPin) annotation (
            Line(
            points={{48,-114},{56,-114},{56,-84},{70,-84},{70,-98}},
            color={0,0,0},
            thickness=1));
        connect(portDHW_out, portDHW_in) annotation (Line(points={{100,-22},{74,-22},
                {74,-82},{100,-82}}, color={0,127,255}));
        connect(bouSouGen.ports, portGen_out) annotation (Line(points={{-72,56},{-64,
                56},{-64,40},{-100,40}}, color={0,127,255}));
        connect(BouGenSin.ports[1], portGen_in[1])
          annotation (Line(points={{-80,92},{-80,80},{-100,80}}, color={0,127,255}));
        connect(idealSourcePumpTemperaturewoPump.port_b, senTBuiSup.port_a)
          annotation (Line(points={{20,65.8},{26,65.8},{26,78},{48,78}}, color={0,127,
                255}));
        connect(senTBuiSup.port_b, portBui_out[1]) annotation (Line(points={{68,78},{
                84,78},{84,80},{100,80}}, color={0,127,255}));
        connect(idealSourcePumpTemperaturewoPump.port_a, senTBuiRet.port_a)
          annotation (Line(points={{20,56.5},{32,56.5},{32,48},{48,48}}, color={0,127,
                255}));
        connect(senTBuiRet.port_b, portBui_in[1]) annotation (Line(points={{68,48},{
                74,48},{74,50},{84,50},{84,40},{100,40}}, color={0,127,255}));
        connect(senTBuiSup.T, sigBusDistr.TBuiSupMea) annotation (Line(points={{58,89},
                {60,89},{60,101},{0,101}}, color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{6,3},{6,3}},
            horizontalAlignment=TextAlignment.Left));
        connect(senTBuiRet.T, sigBusDistr.TBuiRetMea) annotation (Line(points={{58,59},
                {60,59},{60,66},{32,66},{32,101},{0,101}}, color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{6,3},{6,3}},
            horizontalAlignment=TextAlignment.Left));
        connect(mQTTAndConst.activeSQL, sigBusDistr.bSQL) annotation (Line(points={{
                -82.3,-16.1},{-88,-16.1},{-88,40},{-52,40},{-52,74},{-24,74},{-24,76},
                {0,76},{0,101}}, color={255,0,255}), Text(
            string="%second",
            index=1,
            extent={{-3,6},{-3,6}},
            horizontalAlignment=TextAlignment.Right));
        connect(mQTTAndConst.writeValues[2], sigBusDistr.nSetByPI) annotation (
            Line(points={{-31.7,-23},{0,-23},{0,101}}, color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{6,3},{6,3}},
            horizontalAlignment=TextAlignment.Left));
        connect(mQTTAndConst.readValues[1], sigBusDistr.TSupplyReal)
          annotation (Line(points={{-82.3,-23},{-110,-23},{-110,101},{0,101}},
              color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(idealSourcePumpTemperaturewoPump.m_flow_in_withSQL,
          mQTTAndConst.readValues[2]) annotation (Line(points={{-45.1,53.4},{
                -66,53.4},{-66,0},{-82.3,0},{-82.3,-23}}, color={0,0,127}));
        connect(idealSourcePumpTemperaturewoPump.TFlow, mQTTAndConst.readValues[
          1]) annotation (Line(points={{-45.1,28.6},{-100,28.6},{-100,-16},{
                -82.3,-16},{-82.3,-23}}, color={0,0,127}));
        connect(sigBusDistr.TZoneMea, mQTTAndConst.writeValues[3]) annotation (
            Line(
            points={{0,101},{0,-23},{-31.7,-23}},
            color={255,204,51},
            thickness=0.5), Text(
            string="%first",
            index=-1,
            extent={{6,3},{6,3}},
            horizontalAlignment=TextAlignment.Left));
        connect(idealSourcePumpTemperaturewoPump.TReturnMix, sigBusDistr.T_Ruecklauf)
          annotation (Line(points={{23.1,19.3},{50,19.3},{50,56},{0,56},{0,101}},
              color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{6,3},{6,3}},
            horizontalAlignment=TextAlignment.Left));
        connect(mQTTAndConst.readValues[3], sigBusDistr.nReal) annotation (Line(
              points={{-82.3,-23},{-136,-23},{-136,101},{0,101}}, color={0,0,
                127}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
      end HiLMqttConnection_HC;

      model Controller_for_HiL_MPC "Controller_for_HiL_MPC_MA_Daniel"
        extends BESMod.Systems.Hydraulical.Control.BaseClasses.PartialControl;

        Modelica.Blocks.Routing.RealPassThrough realPassThrough_T_Amb1
          "Only used to make warning disappear, has no effect on model veloccity"
          annotation (Placement(transformation(extent={{-244,-64},{-222,-42}})));

        parameter
          BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultBivHPControl bivalentControlData(
          final TOda_nominal=generationParameters.TOda_nominal,
          TSup_nominal=generationParameters.TSup_nominal[1],
          TSetRoomConst=sum(transferParameters.TDem_nominal)/transferParameters.nParallelDem,
          nMin=0)
          annotation (choicesAllMatching=true, Placement(transformation(extent={{
                  -106,-26},{-84,-4}})));
        replaceable
          BESMod.Systems.Hydraulical.Control.Components.ThermostaticValveController.ThermostaticValvePIControlled
          thermostaticValveController constrainedby
          BESMod.Systems.Hydraulical.Control.Components.ThermostaticValveController.BaseClasses.PartialThermostaticValveController(
            final nZones=transferParameters.nParallelDem, final leakageOpening=
              thermostaticValveParameters.leakageOpening) annotation (
            choicesAllMatching=true, Placement(transformation(extent={{66,-32},{92,-2}})));

        replaceable parameter
          BESMod.Systems.Hydraulical.Control.RecordsCollection.ThermostaticValveDataDefinition
          thermostaticValveParameters annotation (choicesAllMatching=true, Placement(
              transformation(extent={{198,-80},{218,-60}})));
        Modelica.Blocks.Logical.Switch SQLorNot
          annotation (Placement(transformation(extent={{128,-66},{148,-46}})));
        Modelica.Blocks.Sources.Constant Valve_open(k=1)
          annotation (Placement(transformation(extent={{-38,-62},{-12,-36}})));
        MA_Daniel.AdaptedModels.HeatingCurveDaniel heatingCurveDaniel
          annotation (Placement(transformation(extent={{-118,12},{-98,32}})));
        Modelica.Blocks.Logical.OnOffController onOffController(bandwidth=6)
          annotation (Placement(transformation(extent={{172,64},{192,84}})));
        BESMod.Systems.Hydraulical.Control.Components.HeatPumpNSetController.PI_InverterHeatPumpController
          pI_InverterHeatPumpController(
          P=0.03,
          yMax=8/9,
          nMin=0.34,
          T_I=180)
          annotation (Placement(transformation(extent={{216,72},{236,92}})));
        Modelica.Blocks.Logical.GreaterThreshold greaterThreshold(threshold=10)
          annotation (Placement(transformation(extent={{168,18},{188,38}})));
        Modelica.Blocks.Sources.BooleanConstant hp_mode(final k=true) annotation (
            Placement(transformation(
              extent={{-7,-7},{7,7}},
              rotation=0,
              origin={239,57})));
        OnOffControl_HiL onOffControl_HiL(
          use_minRunTime=true,
          minRunTime=600,
          use_minLocTime=true,
          minLocTime=600,
          use_runPerHou=true,
          maxRunPerHou=3)
          annotation (Placement(transformation(extent={{258,70},{282,92}})));
      equation

        connect(realPassThrough_T_Amb1.y, sigBusGen.hp_bus.TOdaMea) annotation (Line(
              points={{-220.9,-53},{-152,-53},{-152,-99}},
              color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{6,3},{6,3}},
            horizontalAlignment=TextAlignment.Left));
        connect(realPassThrough_T_Amb1.u, weaBus.TDryBul) annotation (
            Line(points={{-246.2,-53},{-252,-53},{-252,-26},{-210,-26},{-210,2},
                {-237,2}},                                                      color=
               {0,0,127}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(thermostaticValveController.TZoneMea, buiMeaBus.TZoneMea) annotation (
           Line(points={{63.4,-8},{56,-8},{56,78},{65,78},{65,103}},
                                                        color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{-3,6},{-3,6}},
            horizontalAlignment=TextAlignment.Right));
        connect(SQLorNot.y, sigBusTra.opening[1]) annotation (Line(points={{149,-56},{
                174,-56},{174,-100}}, color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{6,3},{6,3}},
            horizontalAlignment=TextAlignment.Left));
        connect(SQLorNot.u2, sigBusDistr.bSQL) annotation (Line(points={{126,-56},{30,
                -56},{30,-100},{1,-100}}, color={255,0,255}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(SQLorNot.u1, Valve_open.y) annotation (Line(points={{126,-48},{-4,-48},
                {-4,-49},{-10.7,-49}}, color={0,0,127}));
        connect(thermostaticValveController.opening[1], SQLorNot.u3) annotation (Line(
              points={{94.6,-17},{100,-17},{100,-64},{126,-64}}, color={0,0,127}));
        connect(thermostaticValveController.TZoneSet[1], useProBus.TZoneSet[1])
          annotation (Line(points={{63.4,-26},{-6,-26},{-6,70},{-76,70},{-76,
                103},{-119,103}}, color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(heatingCurveDaniel.TSet, sigBusDistr.TFlowHeaCur) annotation (
            Line(points={{-97,22},{-80,22},{-80,18},{-60,18},{-60,-100},{1,-100}},
              color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{6,3},{6,3}},
            horizontalAlignment=TextAlignment.Left));
        connect(heatingCurveDaniel.TRaumnom, useProBus.TZoneSet[1]) annotation (
           Line(points={{-108,34},{-108,74},{-119,74},{-119,103}}, color={0,0,
                127}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(heatingCurveDaniel.TOda, weaBus.TDryBul) annotation (Line(
              points={{-120,22},{-210,22},{-210,2},{-237,2}}, color={0,0,127}),
            Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(onOffController.y, pI_InverterHeatPumpController.HP_On)
          annotation (Line(points={{193,74},{210,74},{210,82},{214,82}},
                                                       color={255,0,255}));
        connect(greaterThreshold.y, pI_InverterHeatPumpController.IsOn)
          annotation (Line(points={{189,28},{200,28},{200,30},{220,30},{220,70}},
              color={255,0,255}));
        connect(greaterThreshold.u, sigBusDistr.nReal) annotation (Line(points=
                {{166,28},{88,28},{88,20},{1,20},{1,-100}}, color={0,0,127}),
            Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(onOffController.u, sigBusDistr.TSupplyReal) annotation (Line(
              points={{170,68},{122,68},{122,72},{74,72},{74,58},{20,58},{20,
                -100},{1,-100}},
              color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(onOffController.u, pI_InverterHeatPumpController.T_Meas)
          annotation (Line(points={{170,68},{124,68},{124,66},{132,66},{132,38},
                {226,38},{226,70}}, color={0,0,127}));
        connect(pI_InverterHeatPumpController.T_Set, sigBusDistr.TSupplySet)
          annotation (Line(points={{214,88},{1,88},{1,-100}}, color={0,0,127}),
            Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(pI_InverterHeatPumpController.T_Set, onOffController.reference)
          annotation (Line(points={{214,88},{144,88},{144,78},{158,78},{158,80},
                {170,80}}, color={0,0,127}));

        connect(buiMeaBus.TZoneMea[1], sigBusDistr.TZoneMea) annotation (
          Line(
            points={{65,103},{65,-100},{1,-100}},
            color={255,204,51},
            thickness=0.5),
          Text(
            string="%first",
            index=1,
            extent={{-3,-6},{-3,-6}},
            horizontalAlignment=TextAlignment.Right),
          Text(
            string="%second",
            index=-1,
            extent={{6,3},{6,3}},
            horizontalAlignment=TextAlignment.Left));
        connect(pI_InverterHeatPumpController.n_Set, onOffControl_HiL.nSet)
          annotation (Line(points={{237,82},{256.4,82}}, color={0,0,127}));
        connect(onOffControl_HiL.nOut, sigBusDistr.nSetByPI) annotation (Line(
              points={{283,82},{314,82},{314,70},{338,70},{338,-50},{86,-50},{
                86,-100},{1,-100}}, color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{6,3},{6,3}},
            horizontalAlignment=TextAlignment.Left));
        connect(greaterThreshold.y, onOffControl_HiL.OnOff) annotation (Line(
              points={{189,28},{226,28},{226,26},{274,26},{274,60},{244,60},{
                244,76.4},{256,76.4}}, color={255,0,255}));
        connect(onOffControl_HiL.nOut, onOffControl_HiL.nOutIn) annotation (
            Line(points={{283,82},{294,82},{294,66},{252,66},{252,72.6},{256.6,
                72.6}}, color={0,0,127}));
        annotation (Diagram(coordinateSystem(extent={{-240,-140},{240,100}})), Icon(
              coordinateSystem(extent={{-240,-140},{240,100}})));
      end Controller_for_HiL_MPC;

      model Controller_for_HiL_HC "Controller_for_HiL_MPC_MA_Daniel"
        extends BESMod.Systems.Hydraulical.Control.BaseClasses.PartialControl;

        Modelica.Blocks.Routing.RealPassThrough realPassThrough_T_Amb1
          "Only used to make warning disappear, has no effect on model veloccity"
          annotation (Placement(transformation(extent={{-244,-64},{-222,-42}})));

        parameter
          BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultBivHPControl bivalentControlData(
          final TOda_nominal=generationParameters.TOda_nominal,
          TSup_nominal=generationParameters.TSup_nominal[1],
          TSetRoomConst=sum(transferParameters.TDem_nominal)/transferParameters.nParallelDem,
          nMin=0)
          annotation (choicesAllMatching=true, Placement(transformation(extent={{
                  -106,-26},{-84,-4}})));
        replaceable
          BESMod.Systems.Hydraulical.Control.Components.ThermostaticValveController.ThermostaticValvePIControlled
          thermostaticValveController constrainedby
          BESMod.Systems.Hydraulical.Control.Components.ThermostaticValveController.BaseClasses.PartialThermostaticValveController(
            final nZones=transferParameters.nParallelDem, final leakageOpening=
              thermostaticValveParameters.leakageOpening) annotation (
            choicesAllMatching=true, Placement(transformation(extent={{66,-32},{92,-2}})));

        replaceable parameter
          BESMod.Systems.Hydraulical.Control.RecordsCollection.ThermostaticValveDataDefinition
          thermostaticValveParameters annotation (choicesAllMatching=true, Placement(
              transformation(extent={{198,-80},{218,-60}})));
        MA_Daniel.AdaptedModels.HeatingCurveDaniel heatingCurveDaniel
          annotation (Placement(transformation(extent={{-118,12},{-98,32}})));
        Modelica.Blocks.Logical.OnOffController onOffController(bandwidth=6)
          annotation (Placement(transformation(extent={{172,64},{192,84}})));
        BESMod.Systems.Hydraulical.Control.Components.HeatPumpNSetController.PI_InverterHeatPumpController
          pI_InverterHeatPumpController(
          P=0.03,
          yMax=8/9,
          nMin=0.34,
          T_I=180)
          annotation (Placement(transformation(extent={{216,72},{236,92}})));
        Modelica.Blocks.Logical.GreaterThreshold greaterThreshold(threshold=10)
          annotation (Placement(transformation(extent={{168,18},{188,38}})));
        Modelica.Blocks.Sources.BooleanConstant hp_mode(final k=true) annotation (
            Placement(transformation(
              extent={{-7,-7},{7,7}},
              rotation=0,
              origin={239,57})));
        OnOffControl_HiL onOffControl_HiL(
          use_minRunTime=true,
          minRunTime=600,
          use_minLocTime=true,
          minLocTime=600,
          use_runPerHou=true,
          maxRunPerHou=3)
          annotation (Placement(transformation(extent={{258,70},{282,92}})));
      equation

        connect(realPassThrough_T_Amb1.y, sigBusGen.hp_bus.TOdaMea) annotation (Line(
              points={{-220.9,-53},{-152,-53},{-152,-99}},
              color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{6,3},{6,3}},
            horizontalAlignment=TextAlignment.Left));
        connect(realPassThrough_T_Amb1.u, weaBus.TDryBul) annotation (
            Line(points={{-246.2,-53},{-252,-53},{-252,-26},{-210,-26},{-210,2},
                {-237,2}},                                                      color=
               {0,0,127}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(thermostaticValveController.TZoneMea, buiMeaBus.TZoneMea) annotation (
           Line(points={{63.4,-8},{56,-8},{56,78},{65,78},{65,103}},
                                                        color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{-3,6},{-3,6}},
            horizontalAlignment=TextAlignment.Right));
        connect(thermostaticValveController.TZoneSet[1], useProBus.TZoneSet[1])
          annotation (Line(points={{63.4,-26},{-6,-26},{-6,70},{-76,70},{-76,
                103},{-119,103}}, color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(heatingCurveDaniel.TSet, sigBusDistr.TFlowHeaCur) annotation (
            Line(points={{-97,22},{-80,22},{-80,18},{-60,18},{-60,-100},{1,-100}},
              color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{6,3},{6,3}},
            horizontalAlignment=TextAlignment.Left));
        connect(heatingCurveDaniel.TRaumnom, useProBus.TZoneSet[1]) annotation (
           Line(points={{-108,34},{-108,74},{-119,74},{-119,103}}, color={0,0,
                127}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(heatingCurveDaniel.TOda, weaBus.TDryBul) annotation (Line(
              points={{-120,22},{-210,22},{-210,2},{-237,2}}, color={0,0,127}),
            Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(onOffController.y, pI_InverterHeatPumpController.HP_On)
          annotation (Line(points={{193,74},{210,74},{210,82},{214,82}},
                                                       color={255,0,255}));
        connect(greaterThreshold.y, pI_InverterHeatPumpController.IsOn)
          annotation (Line(points={{189,28},{200,28},{200,30},{220,30},{220,70}},
              color={255,0,255}));
        connect(greaterThreshold.u, sigBusDistr.nReal) annotation (Line(points=
                {{166,28},{88,28},{88,20},{1,20},{1,-100}}, color={0,0,127}),
            Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(onOffController.u, sigBusDistr.TSupplyReal) annotation (Line(
              points={{170,68},{122,68},{122,72},{74,72},{74,58},{20,58},{20,
                -100},{1,-100}},
              color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(onOffController.u, pI_InverterHeatPumpController.T_Meas)
          annotation (Line(points={{170,68},{124,68},{124,66},{132,66},{132,38},
                {226,38},{226,70}}, color={0,0,127}));

        connect(buiMeaBus.TZoneMea[1], sigBusDistr.TZoneMea) annotation (
          Line(
            points={{65,103},{65,-100},{1,-100}},
            color={255,204,51},
            thickness=0.5),
          Text(
            string="%first",
            index=1,
            extent={{-3,-6},{-3,-6}},
            horizontalAlignment=TextAlignment.Right),
          Text(
            string="%second",
            index=-1,
            extent={{6,3},{6,3}},
            horizontalAlignment=TextAlignment.Left));
        connect(pI_InverterHeatPumpController.n_Set, onOffControl_HiL.nSet)
          annotation (Line(points={{237,82},{256.4,82}}, color={0,0,127}));
        connect(onOffControl_HiL.nOut, sigBusDistr.nSetByPI) annotation (Line(
              points={{283,82},{314,82},{314,70},{338,70},{338,-50},{86,-50},{
                86,-100},{1,-100}}, color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{6,3},{6,3}},
            horizontalAlignment=TextAlignment.Left));
        connect(greaterThreshold.y, onOffControl_HiL.OnOff) annotation (Line(
              points={{189,28},{226,28},{226,26},{274,26},{274,60},{244,60},{
                244,76.4},{256,76.4}}, color={255,0,255}));
        connect(onOffControl_HiL.nOut, onOffControl_HiL.nOutIn) annotation (
            Line(points={{283,82},{294,82},{294,66},{252,66},{252,72.6},{256.6,
                72.6}}, color={0,0,127}));
        connect(thermostaticValveController.opening[1], sigBusTra.opening[1])
          annotation (Line(points={{94.6,-17},{174,-17},{174,-100}}, color={0,0,
                127}), Text(
            string="%second",
            index=1,
            extent={{6,3},{6,3}},
            horizontalAlignment=TextAlignment.Left));
        connect(heatingCurveDaniel.TSet, pI_InverterHeatPumpController.T_Set)
          annotation (Line(points={{-97,22},{-40,22},{-40,46},{-12,46},{-12,88},
                {214,88}}, color={0,0,127}));
        connect(heatingCurveDaniel.TSet, onOffController.reference) annotation (
           Line(points={{-97,22},{92,22},{92,80},{170,80}}, color={0,0,127}));
        annotation (Diagram(coordinateSystem(extent={{-240,-140},{240,100}})), Icon(
              coordinateSystem(extent={{-240,-140},{240,100}})));
      end Controller_for_HiL_HC;

      model Controller_for_HiL "Controller_for_HiL MA Daniel"
        extends BESMod.Systems.Hydraulical.Control.BaseClasses.PartialControl;

        Modelica.Blocks.Routing.RealPassThrough realPassThrough_T_Amb1
          "Only used to make warning disappear, has no effect on model veloccity"
          annotation (Placement(transformation(extent={{-228,-58},{-206,-36}})));

        parameter
          BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultBivHPControl bivalentControlData(
          final TOda_nominal=generationParameters.TOda_nominal,
          TSup_nominal=generationParameters.TSup_nominal[1],
          TSetRoomConst=sum(transferParameters.TDem_nominal)/transferParameters.nParallelDem,
          nMin=0)
          annotation (choicesAllMatching=true, Placement(transformation(extent={{
                  -106,-26},{-84,-4}})));
        replaceable
          BESMod.Systems.Hydraulical.Control.Components.ThermostaticValveController.BaseClasses.PartialThermostaticValveController
          thermostaticValveController constrainedby
          BESMod.Systems.Hydraulical.Control.Components.ThermostaticValveController.BaseClasses.PartialThermostaticValveController(
            final nZones=transferParameters.nParallelDem, final leakageOpening=
              thermostaticValveParameters.leakageOpening) annotation (
            choicesAllMatching=true, Placement(transformation(extent={{66,-32},{92,-2}})));

        replaceable parameter
          BESMod.Systems.Hydraulical.Control.RecordsCollection.ThermostaticValveDataDefinition
          thermostaticValveParameters annotation (choicesAllMatching=true, Placement(
              transformation(extent={{198,-80},{218,-60}})));
        Modelica.Blocks.Logical.Switch SQLorNot
          annotation (Placement(transformation(extent={{128,-66},{148,-46}})));
        Modelica.Blocks.Sources.Constant Valve_open(k=1)
          annotation (Placement(transformation(extent={{-38,-62},{-12,-36}})));
        MPC.Submodules.HeatingCurve heatingCurve
          annotation (Placement(transformation(extent={{-138,2},{-118,22}})));
        Controller_Presim controller_Presim
          annotation (Placement(transformation(extent={{-224,50},{-164,86}})));
      equation

        connect(realPassThrough_T_Amb1.y, sigBusGen.hp_bus.TOdaMea) annotation (Line(
              points={{-204.9,-47},{-200,-47},{-200,-68},{-152,-68},{-152,-99}},
              color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{6,3},{6,3}},
            horizontalAlignment=TextAlignment.Left));
        connect(realPassThrough_T_Amb1.u, weaBus.TDryBul) annotation (
            Line(points={{-230.2,-47},{-236,-47},{-236,-26},{-210,-26},{-210,2},{
                -237,2}},                                                       color=
               {0,0,127}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(thermostaticValveController.TZoneMea, buiMeaBus.TZoneMea) annotation (
           Line(points={{63.4,-8},{56,-8},{56,78},{65,78},{65,103}},
                                                        color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{-3,6},{-3,6}},
            horizontalAlignment=TextAlignment.Right));
        connect(SQLorNot.y, sigBusTra.opening[1]) annotation (Line(points={{149,-56},{
                174,-56},{174,-100}}, color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{6,3},{6,3}},
            horizontalAlignment=TextAlignment.Left));
        connect(SQLorNot.u2, sigBusDistr.bSQL) annotation (Line(points={{126,-56},{30,
                -56},{30,-100},{1,-100}}, color={255,0,255}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(SQLorNot.u1, Valve_open.y) annotation (Line(points={{126,-48},{-4,-48},
                {-4,-49},{-10.7,-49}}, color={0,0,127}));
        connect(thermostaticValveController.opening[1], SQLorNot.u3) annotation (Line(
              points={{94.6,-17},{100,-17},{100,-64},{126,-64}}, color={0,0,127}));
        connect(heatingCurve.TOda, weaBus.TDryBul) annotation (Line(points={{-140,12},
                {-210,12},{-210,2},{-237,2}}, color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(heatingCurve.TSet, sigBusDistr.TFlowHeaCur) annotation (Line(points={
                {-117,12},{-44,12},{-44,-100},{1,-100}}, color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{6,3},{6,3}},
            horizontalAlignment=TextAlignment.Left));
        connect(controller_Presim.T_Zone_Set, heatingCurve.TSetRoom)
          annotation (Line(points={{-156.909,56.48},{-128,56.48},{-128,24}},
              color={0,0,127}));
        connect(controller_Presim.T_Zone_Set, thermostaticValveController.TZoneSet[
          1]) annotation (Line(points={{-156.909,56.48},{-18,56.48},{-18,-26},{
                63.4,-26}}, color={0,0,127}));
        annotation (Diagram(coordinateSystem(extent={{-240,-140},{240,100}})), Icon(
              coordinateSystem(extent={{-240,-140},{240,100}})));
      end Controller_for_HiL;

      model Controller_Presim "Controller for Presim MA Daniel"
        Modelica.Blocks.Interfaces.RealOutput T_Zone_Set
          annotation (Placement(transformation(extent={{136,-74},{156,-54}})));
        Buildings.Utilities.Time.CalendarTime calTim(zerTim=Buildings.Utilities.Time.Types.ZeroTime.NY2014,
            yearRef=2015)
          annotation (Placement(transformation(extent={{-98,42},{-44,96}})));
        Modelica.Blocks.Sources.Constant T_Zone_Set_day(k=17 + 273.15)
          annotation (Placement(transformation(extent={{-94,-30},{-68,-4}})));
        Modelica.Blocks.Sources.Constant T_Zone_Set_night(k=20 + 273.15)
          annotation (Placement(transformation(extent={{-94,-72},{-68,-46}})));
        Modelica.Blocks.Logical.GreaterThreshold greaterThreshold(threshold=7)
          annotation (Placement(transformation(extent={{18,14},{38,34}})));
        Modelica.Blocks.Math.IntegerToReal integerToReal
          annotation (Placement(transformation(extent={{-20,14},{0,34}})));
        Modelica.Blocks.Logical.Switch switch1
          annotation (Placement(transformation(extent={{98,-74},{118,-54}})));
        Modelica.Blocks.Logical.LessThreshold lessThreshold(threshold=14)
          annotation (Placement(transformation(extent={{18,-26},{38,-6}})));
        Modelica.Blocks.Logical.And and1
          annotation (Placement(transformation(extent={{56,-4},{76,16}})));
        Modelica.Blocks.Math.IntegerToReal integerToReal1
          annotation (Placement(transformation(extent={{2,48},{22,68}})));
        Modelica.Blocks.Logical.LessThreshold lessThreshold1(threshold=6)
          annotation (Placement(transformation(extent={{34,48},{54,68}})));
        Modelica.Blocks.Logical.And and2
          annotation (Placement(transformation(extent={{90,6},{110,26}})));
      equation
        connect(integerToReal.y, greaterThreshold.u)
          annotation (Line(points={{1,24},{16,24}}, color={0,0,127}));
        connect(switch1.u1, T_Zone_Set_day.y) annotation (Line(points={{96,-56},
                {-58,-56},{-58,-17},{-66.7,-17}},
                                             color={0,0,127}));
        connect(switch1.u3, T_Zone_Set_night.y) annotation (Line(points={{96,-72},
                {-58,-72},{-58,-59},{-66.7,-59}},color={0,0,127}));
        connect(switch1.y, T_Zone_Set) annotation (Line(points={{119,-64},{146,
                -64},{146,-64}},     color={0,0,127}));
        connect(T_Zone_Set, T_Zone_Set)
          annotation (Line(points={{146,-64},{146,-64}}, color={0,0,127}));
        connect(lessThreshold.u, integerToReal.y) annotation (Line(points={{16,-16},{
                6,-16},{6,24},{1,24}}, color={0,0,127}));
        connect(greaterThreshold.y, and1.u1) annotation (Line(points={{39,24},{52,24},
                {52,6},{54,6}}, color={255,0,255}));
        connect(lessThreshold.y, and1.u2) annotation (Line(points={{39,-16},{52,-16},
                {52,-2},{54,-2}}, color={255,0,255}));
        connect(calTim.hour, integerToReal.u) annotation (Line(points={{-41.3,86.28},
                {-28,86.28},{-28,24},{-22,24}}, color={255,127,0}));
        connect(calTim.weekDay, integerToReal1.u) annotation (Line(points={{
                -41.3,58.2},{-20.65,58.2},{-20.65,58},{0,58}}, color={255,127,0}));
        connect(lessThreshold1.u, integerToReal1.y)
          annotation (Line(points={{32,58},{23,58}}, color={0,0,127}));
        connect(and1.y, and2.u2)
          annotation (Line(points={{77,6},{77,8},{88,8}}, color={255,0,255}));
        connect(lessThreshold1.y, and2.u1) annotation (Line(points={{55,58},{72,
                58},{72,46},{80,46},{80,16},{88,16}}, color={255,0,255}));
        connect(and2.y, switch1.u2) annotation (Line(points={{111,16},{116,16},
                {116,-36},{86,-36},{86,-64},{96,-64}}, color={255,0,255}));
        annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
                  -100},{120,100}})), Diagram(coordinateSystem(preserveAspectRatio=
                  false, extent={{-100,-100},{120,100}})),
          experiment(
            StopTime=1209600,
            Interval=3600.00288,
            __Dymola_Algorithm="Dassl"));
      end Controller_Presim;

      model HeatPumpBusPassThroughHiL
        BESMod.Systems.Hydraulical.Interfaces.GenerationControlBus sigBusGen
          annotation (Placement(transformation(extent={{-110,-10},{-90,10}})));
        AixLib.Controls.Interfaces.VapourCompressionMachineControlBus
          vapourCompressionMachineControlBus
          annotation (Placement(transformation(extent={{82,-22},{122,18}})));
        Modelica.Blocks.Routing.RealPassThrough realPassThrough
          annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
        Modelica.Blocks.Routing.BooleanPassThrough booleanPassThrough
          annotation (Placement(transformation(extent={{-10,-40},{10,-20}})));
      equation
        connect(booleanPassThrough.u, sigBusGen.heaPumIsOn) annotation (Line(points={{
                -12,-30},{-86,-30},{-86,0},{-100,0}}, color={255,0,255}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(booleanPassThrough.y, vapourCompressionMachineControlBus.onOffMea)
          annotation (Line(points={{11,-30},{46,-30},{46,-26},{102.1,-26},{102.1,-1.9}},
              color={255,0,255}), Text(
            string="%second",
            index=1,
            extent={{6,3},{6,3}},
            horizontalAlignment=TextAlignment.Left));
        connect(realPassThrough.y, vapourCompressionMachineControlBus.nSet)
          annotation (Line(points={{11,0},{78,0},{78,-1.9},{102.1,-1.9}},
                                                                    color={0,0,127}),
            Text(
            string="%second",
            index=1,
            extent={{6,3},{6,3}},
            horizontalAlignment=TextAlignment.Left));
        connect(realPassThrough.u, sigBusGen.yHeaPumSet) annotation (Line(points={{
                -12,0},{-100,0}}, color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
              coordinateSystem(preserveAspectRatio=false)));
      end HeatPumpBusPassThroughHiL;

      model OnOffControl_HiL
        "Controlls if the minimal runtime, stoptime and max. runs per hour are inside given boundaries"
        parameter Boolean use_minRunTime
          "False if minimal runtime of HP is not considered" annotation(choices(checkBox=true));
        parameter Modelica.Units.SI.Time minRunTime(displayUnit="min")
          "Mimimum runtime of heat pump" annotation (Dialog(enable=use_minRunTime));
        parameter Boolean use_minLocTime
          "False if minimal locktime of HP is not considered" annotation(choices(checkBox=true));
        parameter Modelica.Units.SI.Time minLocTime(displayUnit="min")
          "Minimum lock time of heat pump" annotation (Dialog(enable=use_minLocTime));
        parameter Boolean use_runPerHou
          "False if maximal runs per hour of HP are not considered" annotation(choices(checkBox=true));
        parameter Integer maxRunPerHou "Maximal number of on/off cycles in one hour"
          annotation (Dialog(enable=use_runPerHou));
        parameter Boolean pre_n_start=true "Start value of pre(n) at initial time";
        Modelica.Blocks.Logical.GreaterThreshold
                                        nSetGreaterZero(final threshold=Modelica.Constants.eps)
                                                        "True if device is set on"
          annotation (Placement(transformation(extent={{-110,56},{-94,72}})));
        Modelica.Blocks.Logical.And andRun
          annotation (Placement(transformation(extent={{18,72},{30,84}})));
        Modelica.Blocks.Logical.Pre pre1(final pre_u_start=pre_n_start)
          annotation (Placement(transformation(extent={{-84,-48},{-72,-36}})));
        AixLib.Controls.HeatPump.SafetyControls.BaseClasses.RunPerHouBoundary runPerHouBoundary(final
            maxRunPer_h=maxRunPerHou, final delayTime=3600) if use_runPerHou
          annotation (Placement(transformation(extent={{-40,-80},{-20,-60}})));
        AixLib.Controls.HeatPump.SafetyControls.BaseClasses.TimeControl locTimControl(final
            minRunTime=minLocTime) if use_minLocTime
          annotation (Placement(transformation(extent={{-40,-24},{-20,-4}})));
        Modelica.Blocks.Logical.Not notIsOn
          annotation (Placement(transformation(extent={{-66,-22},{-58,-14}})));
        AixLib.Controls.HeatPump.SafetyControls.BaseClasses.TimeControl runTimControl(final
            minRunTime=minRunTime) if use_minRunTime
          annotation (Placement(transformation(extent={{-40,52},{-20,72}})));
        Modelica.Blocks.Logical.And andLoc
          annotation (Placement(transformation(extent={{28,-66},{40,-54}})));

        Modelica.Blocks.Sources.BooleanConstant booleanConstantRunPerHou(final k=true) if not
          use_runPerHou
          annotation (Placement(transformation(extent={{0,-90},{14,-76}})));
        Modelica.Blocks.Sources.BooleanConstant booleanConstantLocTim(final k=true) if not
          use_minLocTime
          annotation (Placement(transformation(extent={{-34,-44},{-20,-30}})));
        Modelica.Blocks.Sources.BooleanConstant booleanConstantRunTim(final k=true) if not
          use_minRunTime
          annotation (Placement(transformation(extent={{-4,52},{10,66}})));
        Modelica.Blocks.Logical.Not notSetOn
          annotation (Placement(transformation(extent={{-66,72},{-56,82}})));
        Modelica.Blocks.Logical.And andTurnOff
          "Check if HP is on and is set to be turned off"
          annotation (Placement(transformation(extent={{-12,80},{0,92}})));
        Modelica.Blocks.Logical.And andTurnOn
          "Check if HP is Off and is set to be turned on"
          annotation (Placement(transformation(extent={{28,-90},{40,-78}})));
        Modelica.Blocks.Logical.And andIsOn
          "Check if both set and actual value are greater zero"
          annotation (Placement(transformation(extent={{16,12},{28,24}})));
        Modelica.Blocks.Interfaces.RealInput nSet
          "Set value relative speed of compressor. Analog from 0 to 1"
          annotation (Placement(transformation(extent={{-152,-16},{-120,16}})));
        Modelica.Blocks.Interfaces.RealOutput nOut
          "Relative speed of compressor. From 0 to 1"
          annotation (Placement(transformation(extent={{120,-10},{140,10}})));
        AixLib.Utilities.Logical.SmoothSwitch swinOutnSet
          "If any of the ornSet conditions is true, nSet will be passed. Else nOut will stay the same"
          annotation (Placement(transformation(extent={{90,-10},{110,10}})));
        Modelica.Blocks.MathBoolean.Or orSetN(nu=4)
          "Output is true if nSet value is correct"
          annotation (Placement(transformation(extent={{52,-10},{72,10}})));
        Modelica.Blocks.Logical.And andIsOff
          "Check if both set and actual value are equal to zero"
          annotation (Placement(transformation(extent={{16,32},{28,44}})));
        Modelica.Blocks.Logical.And andLocOff
          annotation (Placement(transformation(extent={{52,-78},{64,-66}})));
        Modelica.Blocks.Interfaces.RealInput nOutIn
          "Set value relative speed of compressor. Analog from 0 to 1"
          annotation (Placement(transformation(extent={{-150,-110},{-118,-78}})));
        Modelica.Blocks.Interfaces.BooleanInput OnOff annotation (Placement(
              transformation(extent={{-160,-76},{-120,-36}})));
      equation
        connect(pre1.y, runPerHouBoundary.u) annotation (Line(points={{-71.4,-42},{-71.4,
                -70},{-42,-70}},         color={255,0,255}));
        connect(pre1.y, notIsOn.u) annotation (Line(points={{-71.4,-42},{-71.4,-28},{-72,
                -28},{-72,-18},{-66.8,-18}}, color={255,0,255}));
        connect(notIsOn.y, locTimControl.u) annotation (Line(points={{-57.6,-18},{-50,
                -18},{-50,-14},{-42,-14}}, color={255,0,255}));
        connect(runTimControl.y, andRun.u2) annotation (Line(points={{-19,62},{-8,62},
                {-8,73.2},{16.8,73.2}},color={255,0,255},
            pattern=LinePattern.Dash));
        connect(runTimControl.u, pre1.y) annotation (Line(points={{-42,62},{-71.4,62},
                {-71.4,-42}},                color={255,0,255}));
        connect(locTimControl.y, andLoc.u1) annotation (Line(points={{-19,-14},{6,-14},
                {6,-60},{26.8,-60}},         color={255,0,255},
            pattern=LinePattern.Dash));
        connect(runPerHouBoundary.y, andLoc.u2) annotation (Line(points={{-19,-70},{6,
                -70},{6,-64.8},{26.8,-64.8}}, color={255,0,255},
            pattern=LinePattern.Dash));
        connect(booleanConstantRunPerHou.y, andLoc.u2) annotation (Line(
            points={{14.7,-83},{16,-83},{16,-64.8},{26.8,-64.8}},
            color={255,0,255},
            pattern=LinePattern.Dash));
        connect(booleanConstantRunTim.y, andRun.u2) annotation (Line(
            points={{10.7,59},{10.7,73.2},{16.8,73.2}},
            color={255,0,255},
            pattern=LinePattern.Dash));

        connect(nSet,nSetGreaterZero. u) annotation (Line(points={{-136,0},{-120,0},{-120,
                64},{-111.6,64}}, color={0,0,127}));
        connect(nSetGreaterZero.y, notSetOn.u) annotation (Line(points={{-93.2,64},{-78,
                64},{-78,77},{-67,77}}, color={255,0,255}));
        connect(pre1.y, andIsOn.u2) annotation (Line(points={{-71.4,-42},{-71.4,12},{-72,
                12},{-72,14},{-42,14},{-42,13.2},{14.8,13.2}},
                                            color={255,0,255}));
        connect(nSetGreaterZero.y, andIsOn.u1) annotation (Line(points={{-93.2,64},{-86,
                64},{-86,18},{14.8,18}},                             color={255,0,255}));
        connect(nOut, nOut)
          annotation (Line(points={{130,0},{130,0}}, color={0,0,127}));
        connect(swinOutnSet.y, nOut)
          annotation (Line(points={{111,0},{130,0}}, color={0,0,127}));
        connect(nSet, swinOutnSet.u1) annotation (Line(points={{-136,0},{-120,0},{
                -120,98},{78,98},{78,8},{88,8}},
                                              color={0,0,127}));
        connect(andTurnOff.y, andRun.u1) annotation (Line(points={{0.6,86},{8,86},{8,78},
                {16.8,78}}, color={255,0,255}));
        connect(orSetN.y, swinOutnSet.u2)
          annotation (Line(points={{73.5,0},{88,0}}, color={255,0,255}));
        connect(notSetOn.y, andIsOff.u1) annotation (Line(points={{-55.5,77},{-50,77},
                {-50,42},{16,42},{16,38},{14.8,38}}, color={255,0,255}));
        connect(andIsOff.y, orSetN.u[1]) annotation (Line(points={{28.6,38},{40,
                38},{40,-2.625},{52,-2.625}},
                                  color={255,0,255}));
        connect(andIsOn.y, orSetN.u[2]) annotation (Line(points={{28.6,18},{38,
                18},{38,-0.875},{52,-0.875}},
                                  color={255,0,255}));
        connect(andRun.y, orSetN.u[3]) annotation (Line(points={{30.6,78},{46,
                78},{46,0.875},{52,0.875}},
                                    color={255,0,255}));
        connect(andLoc.y, andLocOff.u1) annotation (Line(points={{40.6,-60},{46,-60},{
                46,-72},{50.8,-72}}, color={255,0,255}));
        connect(andTurnOn.y, andLocOff.u2) annotation (Line(points={{40.6,-84},{46,-84},
                {46,-76.8},{50.8,-76.8}}, color={255,0,255}));
        connect(andLocOff.y, orSetN.u[4]) annotation (Line(points={{64.6,-72},{
                64.6,-32},{40,-32},{40,2.625},{52,2.625}},
                                                 color={255,0,255}));
        connect(notSetOn.y, andTurnOff.u2) annotation (Line(points={{-55.5,77},{-50,77},
                {-50,81.2},{-13.2,81.2}}, color={255,0,255}));
        connect(pre1.y, andTurnOff.u1) annotation (Line(points={{-71.4,-42},{-72,-42},
                {-72,86},{-13.2,86}}, color={255,0,255}));
        connect(nSetGreaterZero.y, andTurnOn.u2) annotation (Line(points={{-93.2,64},{
                -86,64},{-86,-98},{24,-98},{24,-88.8},{26.8,-88.8}}, color={255,0,255}));
        connect(notIsOn.y, andTurnOn.u1) annotation (Line(points={{-57.6,-18},{-56,-18},
                {-56,-96},{22,-96},{22,-84},{26.8,-84}}, color={255,0,255}));
        connect(notIsOn.y, andIsOff.u2) annotation (Line(points={{-57.6,-18},{-56,-18},
                {-56,33.2},{14.8,33.2}}, color={255,0,255}));
        connect(booleanConstantLocTim.y, andLoc.u1) annotation (Line(
            points={{-19.3,-37},{6,-37},{6,-60},{26.8,-60}},
            color={255,0,255},
            pattern=LinePattern.Dash));
        connect(swinOutnSet.u3, nOutIn) annotation (Line(points={{88,-8},{88,
                -112},{-110,-112},{-110,-94},{-134,-94}}, color={0,0,127}));
        connect(pre1.u, OnOff) annotation (Line(points={{-85.2,-42},{-104,-42},
                {-104,-56},{-140,-56}}, color={255,0,255}));
        annotation (Documentation(info="<html><p>
  Checks if the nSet value is legal by checking if the device can
  either be turned on or off, depending on which state it was in.
</p>
<p>
  E.g. If it is turned on, and the new nSet value is 0, it will only
  turn off if current runtime is longer than the minimal runtime. Else
  it will keep the current rotating speed.
</p>
<ul>
  <li>
    <i>November 26, 2018&#160;</i> by Fabian Wüllhorst:<br/>
    First implementation (see issue <a href=
    \"https://github.com/RWTH-EBC/AixLib/issues/577\">#577</a>)
  </li>
</ul>
</html>"),Diagram(coordinateSystem(extent={{-120,-120},{120,100}})),
          Icon(coordinateSystem(extent={{-120,-120},{120,100}}), graphics={
              Polygon(
                points={{-42,20},{0,62},{-42,20}},
                lineColor={28,108,200},
                fillColor={0,0,0},
                fillPattern=FillPattern.Solid),
              Ellipse(
                extent={{-48,-26},{48,66}},
                lineColor={0,0,0},
                fillColor={91,91,91},
                fillPattern=FillPattern.Solid),
              Ellipse(
                extent={{-36,-14},{36,54}},
                lineColor={0,0,0},
                fillColor={255,255,255},
                fillPattern=FillPattern.Solid),
              Rectangle(
                extent={{-60,20},{60,-80}},
                lineColor={0,0,0},
                fillColor={91,91,91},
                fillPattern=FillPattern.Solid),
              Rectangle(
                extent={{-10,-30},{10,-70}},
                lineColor={0,0,0},
                fillColor={255,255,255},
                fillPattern=FillPattern.Solid),
              Ellipse(
                extent={{-14,-40},{16,-12}},
                lineColor={0,0,0},
                fillColor={255,255,255},
                fillPattern=FillPattern.Solid),
              Text(
                extent={{-104,100},{106,76}},
                lineColor={28,108,200},
                lineThickness=0.5,
                fillColor={255,255,255},
                fillPattern=FillPattern.None,
                textString="%name"),
              Rectangle(
                extent={{-120,100},{120,-100}},
                lineColor={28,108,200},
                lineThickness=0.5,
                fillColor={255,255,255},
                fillPattern=FillPattern.None)}));
      end OnOffControl_HiL;
    end HiL;

    package Controls "Different Controls for MA Daniel"
      extends BESMod.Utilities.Icons.ControlIcon;

      model FMU_SupTem1 "MA Daniel - FMU SupTem1"
        extends PartialHydraulicControl_FMU(redeclare
            BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultBivHPControl
            bivalentControlData, securityControl(use_antFre=true));
        HeatPumpHysteresis heatPumpHysteresis(bandwidth=0.1)
          annotation (Placement(transformation(extent={{-70,50},{-50,70}})));

        Modelica.Blocks.Sources.RealExpression control_variable
          annotation (Placement(transformation(extent={{-262,56},{-242,76}})));
        BESMod.Systems.Hydraulical.Control.Components.HeatingCurve
          heatingCurve(
          GraHeaCurve=bivalentControlData.gradientHeatCurve,
          THeaThres=bivalentControlData.TSetRoomConst,
          dTOffSet_HC=bivalentControlData.dTOffSetHeatCurve)
          annotation (Placement(transformation(extent={{-184,16},{-164,36}})));
        Modelica.Blocks.Sources.BooleanExpression externalInput
          annotation (Placement(transformation(extent={{-240,38},{-220,58}})));

        Modelica.Blocks.Logical.Switch switch1
          annotation (Placement(transformation(extent={{-148,50},{-128,70}})));
        parameter Modelica.Blocks.Interfaces.BooleanOutput extInput=false
          "Value of Boolean output";
      equation
        connect(heatPumpHysteresis.T_sup, sigBusGen.THeaPumOut) annotation (
            Line(points={{-72,52},{-78,52},{-78,50},{-102,50},{-102,-99},{-152,
                -99}}, color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(heatPumpHysteresis.y, HP_nSet_Controller.HP_On) annotation (
            Line(points={{-49,60},{72,60},{72,78},{79,78}}, color={255,0,255}));
        connect(heatingCurve.TOda, weaBus.TDryBul) annotation (Line(points={{-186,26},
                {-222,26},{-222,2},{-237,2}}, color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(heatingCurve.TSetRoom, useProBus.TZoneSet[1]) annotation (Line(points=
               {{-174,38},{-174,103},{-119,103}}, color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{-3,6},{-3,6}},
            horizontalAlignment=TextAlignment.Right));
        connect(control_variable.y, switch1.u1) annotation (Line(points={{-241,66},{-238,
                66},{-238,68},{-150,68}}, color={0,0,127}));
        connect(heatingCurve.TSet, switch1.u3) annotation (Line(points={{-163,26},{-156,
                26},{-156,52},{-150,52}}, color={0,0,127}));
        connect(externalInput.y, switch1.u2) annotation (Line(points={{-219,48},{-192,
                48},{-192,56},{-150,56},{-150,60}}, color={255,0,255}));
        connect(switch1.y, heatPumpHysteresis.T_set) annotation (Line(points={{-127,60},
                {-100,60},{-100,74},{-76,74},{-76,76},{-72,76},{-72,68}}, color={0,0,127}));
        connect(HP_nSet_Controller.T_Set, heatPumpHysteresis.T_set) annotation (Line(
              points={{79,86.4},{-94,86.4},{-94,74},{-76,74},{-76,76},{-72,76},{-72,68}},
              color={0,0,127}));
        connect(switch2.u1, switch1.u1) annotation (Line(points={{120,40},{-16,
                40},{-16,42},{-164,42},{-164,68},{-150,68}}, color={0,0,127}));
      end FMU_SupTem1;

      model MultipleInputs "MA Daniel - Multiple Inputs"
        extends PartialHydraulicControl_FMU(
          redeclare
            BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultBivHPControl
            bivalentControlData,
          securityControl(use_antFre=false),
          pI_InverterHeatPumpController(P=1,
            nMin=0.3,                        T_I=100),
          thermostaticValveController(k={thermostaticValveParameters.k}, Ti=
                {thermostaticValveParameters.Ti}));

        BESMod.Systems.Hydraulical.Control.Components.HeatingCurve
          heatingCurve(
          GraHeaCurve=bivalentControlData.gradientHeatCurve,
          THeaThres=bivalentControlData.TSetRoomConst,
          dTOffSet_HC=bivalentControlData.dTOffSetHeatCurve)
          annotation (Placement(transformation(extent={{-216,24},{-196,44}})));

        Modelica.Blocks.Sources.RealExpression u_T_VL
          annotation (Placement(transformation(extent={{-320,16},{-288,34}})));
        Modelica.Blocks.Sources.RealExpression u_comp
          annotation (Placement(transformation(extent={{-320,-10},{-288,10}})));
        Modelica.Blocks.Sources.BooleanExpression b_MPC_input
          annotation (Placement(transformation(extent={{-318,62},{-286,82}})));
        Modelica.Blocks.Sources.BooleanExpression b_FMU_input
          annotation (Placement(transformation(extent={{-318,86},{-286,106}})));
        Modelica.Blocks.Sources.BooleanExpression b_u_comp_ctrl
          annotation (Placement(transformation(extent={{-318,34},{-286,54}})));
        Model_inputs_adapted model_inputs_adapted
          annotation (Placement(transformation(extent={{-250,94},{-222,130}})));
        Modelica.Blocks.Sources.BooleanExpression pumpIsOn1(y=true)
          annotation (Placement(transformation(extent={{-10,36},{10,56}})));
        NotUsedAnymore.DeadZoneCustom deadZoneCustom(uMax=1/3)
          annotation (Placement(transformation(extent={{296,26},{316,46}})));
      equation
        connect(heatingCurve.TOda, weaBus.TDryBul) annotation (Line(points={{-218,34},
                {-254,34},{-254,2},{-237,2}}, color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(heatingCurve.TSetRoom, useProBus.TZoneSet[1]) annotation (Line(points={{-206,46},
                {-206,78},{-119,78},{-119,103}},  color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{-3,6},{-3,6}},
            horizontalAlignment=TextAlignment.Right));
        connect(b_FMU_input.y, model_inputs_adapted.b_FMU_input) annotation (Line(
              points={{-284.4,96},{-284.4,128.094},{-253,128.094}},
                                                                color={255,0,255}));
        connect(b_MPC_input.y, model_inputs_adapted.b_MPC_input) annotation (Line(
              points={{-284.4,72},{-276,72},{-276,118.988},{-252.6,118.988}},
                                                                          color={255,0,
                255}));
        connect(b_u_comp_ctrl.y, model_inputs_adapted.b_u_comp_ctrl) annotation (Line(
              points={{-284.4,44},{-270,44},{-270,115.388},{-252.6,115.388}},
                                                                          color={255,0,
                255}));
        connect(u_T_VL.y, model_inputs_adapted.u_T_VL_outer) annotation (Line(points={{-286.4,
                25},{-264,25},{-264,112},{-252.6,112}},         color={0,0,127}));
        connect(u_comp.y, model_inputs_adapted.u_comp_outer) annotation (Line(points={{-286.4,
                0},{-258,0},{-258,108.4},{-252.6,108.4}},         color={0,0,127}));
        connect(heatingCurve.TSet, model_inputs_adapted.T_set_HC) annotation (Line(
              points={{-195,34},{-178,34},{-178,24},{-168,24},{-168,60},{-246,
                60},{-246,88},{-244,88},{-244,95.6941},{-252.6,95.6941}},
                                                          color={0,0,127}));
        connect(model_inputs_adapted.n_Set, securityControl.nSet) annotation (
            Line(points={{-217.8,111.576},{178,111.576},{178,84.4},{191.867,
                84.4}}, color={0,0,127}));
        connect(model_inputs_adapted.T_VL_set, pI_InverterHeatPumpController.T_Set)
          annotation (Line(points={{-218,102.894},{-146,102.894},{-146,74},{34,
                74},{34,86},{58,86}}, color={0,0,127}));
        connect(pI_InverterHeatPumpController.n_Set, model_inputs_adapted.u_comp_PID)
          annotation (Line(points={{81,80},{90,80},{90,78},{98,78},{98,126},{
                -260,126},{-260,101.624},{-252.6,101.624}}, color={0,0,127}));
        connect(pumpIsOn1.y, pI_InverterHeatPumpController.HP_On) annotation (
            Line(points={{11,46},{58,46},{58,80}},         color={255,0,255}));
        connect(securityControl.nOut, deadZoneCustom.u) annotation (Line(points={{227.333,
                84.4},{294,84.4},{294,36}},          color={0,0,127}));
        connect(deadZoneCustom.y, sigBusGen.yHeaPumSet) annotation (Line(points=
               {{317,36},{324,36},{324,-126},{-88,-126},{-88,-114},{-152,-114},
                {-152,-99}}, color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{6,3},{6,3}},
            horizontalAlignment=TextAlignment.Left));
        connect(thermostaticValveController.TZoneSet[1], useProBus.TZoneSet[1])
          annotation (Line(points={{87.4,-46},{30,-46},{30,-24},{-134,-24},{
                -134,30},{-148,30},{-148,78},{-119,78},{-119,103}}, color={0,0,
                127}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(switch1.u2, b_MPC_input.y) annotation (Line(points={{80,-76},{
                22,-76},{22,-78},{-38,-78},{-38,72},{-284.4,72}}, color={255,0,
                255}));
        connect(pI_InverterHeatPumpController.IsOn, sigBusGen.heaPumIsOn)
          annotation (Line(points={{64,68},{64,14},{-86,14},{-86,-99},{-152,-99}},
              color={255,0,255}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
      end MultipleInputs;

      model Model_inputs_adapted "Select inputs from correct source"
        Modelica.Blocks.Interfaces.RealInput u_T_VL_outer
          annotation (Placement(transformation(extent={{-146,-10},{-106,30}})));
        Modelica.Blocks.Interfaces.BooleanInput b_u_comp_ctrl
          annotation (Placement(transformation(extent={{-146,22},{-106,62}})));
        Modelica.Blocks.Interfaces.BooleanInput b_MPC_input
          annotation (Placement(transformation(extent={{-146,56},{-106,96}})));
        Modelica.Blocks.Interfaces.RealInput u_comp_outer
          annotation (Placement(transformation(extent={{-146,-44},{-106,-4}})));
        Modelica.Blocks.Logical.And and_u_comp
          annotation (Placement(transformation(extent={{-68,54},{-48,74}})));
        Modelica.Blocks.Logical.Switch MPC_Switch
          annotation (Placement(transformation(extent={{32,24},{52,44}})));
        Modelica.Blocks.Interfaces.RealInput u_comp_PID
          annotation (Placement(transformation(extent={{-146,-108},{-106,-68}})));
        Modelica.Blocks.Logical.Switch T_set_Switch
          annotation (Placement(transformation(extent={{50,-84},{70,-64}})));
        Modelica.Blocks.Logical.Or or_b_MPC__b_u_comp
          annotation (Placement(transformation(extent={{2,-60},{22,-40}})));
        Modelica.Blocks.Logical.Not not_b_MPC
          annotation (Placement(transformation(extent={{-38,-4},{-18,16}})));
        Modelica.Blocks.Interfaces.RealInput T_set_HC
          annotation (Placement(transformation(extent={{-146,-164},{-106,-124}})));
        Modelica.Blocks.Interfaces.RealOutput n_Set
          annotation (Placement(transformation(extent={{212,-4},{232,16}})));
        Modelica.Blocks.Interfaces.RealOutput T_VL_set
          annotation (Placement(transformation(extent={{210,-86},{230,-66}})));
        Modelica.Blocks.Logical.Switch n_Set_switch_fmu
          annotation (Placement(transformation(extent={{152,-4},{172,16}})));
        Modelica.Blocks.Logical.Switch T_set_switch_fmu
          annotation (Placement(transformation(extent={{142,-86},{162,-66}})));
              Modelica.Blocks.Interfaces.BooleanInput b_FMU_input
          annotation (Placement(transformation(extent={{-150,142},{-110,182}})));
      equation
        connect(and_u_comp.u1, b_MPC_input) annotation (Line(points={{-70,64},{-80,
                64},{-80,76},{-126,76}}, color={255,0,255}));
        connect(and_u_comp.u2, b_u_comp_ctrl) annotation (Line(points={{-70,56},{
                -88,56},{-88,42},{-126,42}}, color={255,0,255}));
        connect(and_u_comp.y, MPC_Switch.u2) annotation (Line(points={{-47,64},{16,64},
                {16,34},{30,34}},     color={255,0,255}));
        connect(u_comp_outer, MPC_Switch.u1) annotation (Line(points={{-126,-24},
                {-52,-24},{-52,42},{30,42}}, color={0,0,127}));
        connect(MPC_Switch.u3, u_comp_PID) annotation (Line(points={{30,26},{-42,26},
                {-42,-88},{-126,-88}}, color={0,0,127}));
        connect(or_b_MPC__b_u_comp.y, T_set_Switch.u2) annotation (Line(points={{23,
                -50},{30,-50},{30,-74},{48,-74}}, color={255,0,255}));
        connect(not_b_MPC.u, b_MPC_input) annotation (Line(points={{-40,6},{-92,6},
                {-92,76},{-126,76}}, color={255,0,255}));
        connect(not_b_MPC.y, or_b_MPC__b_u_comp.u1) annotation (Line(points={{-17,6},
                {-12,6},{-12,-50},{0,-50}}, color={255,0,255}));
        connect(or_b_MPC__b_u_comp.u2, b_u_comp_ctrl) annotation (Line(points={{0,
                -58},{-44,-58},{-44,42},{-126,42}}, color={255,0,255}));
        connect(T_set_Switch.u3, u_T_VL_outer) annotation (Line(points={{48,-82},
                {28,-82},{28,-32},{-26,-32},{-26,-30},{-96,-30},{-96,10},{-126,
                10}}, color={0,0,127}));
        connect(T_set_Switch.u1, T_set_HC) annotation (Line(points={{48,-66},{-30,
                -66},{-30,-144},{-126,-144}}, color={0,0,127}));
        connect(T_VL_set, T_VL_set)
          annotation (Line(points={{220,-76},{220,-76}}, color={0,0,127}));
        connect(MPC_Switch.y, n_Set_switch_fmu.u1) annotation (Line(points={{53,34},{
                142,34},{142,14},{150,14}},  color={0,0,127}));
        connect(n_Set_switch_fmu.y, n_Set)
          annotation (Line(points={{173,6},{222,6}}, color={0,0,127}));
        connect(n_Set_switch_fmu.u3, u_comp_PID) annotation (Line(points={{150,-2},
                {54,-2},{54,-44},{-42,-44},{-42,-88},{-126,-88}}, color={0,0,127}));
        connect(T_set_switch_fmu.u2, n_Set_switch_fmu.u2) annotation (Line(points={
                {140,-76},{134,-76},{134,6},{150,6}}, color={255,0,255}));
        connect(T_set_switch_fmu.y, T_VL_set)
          annotation (Line(points={{163,-76},{220,-76}}, color={0,0,127}));
        connect(T_set_Switch.y, T_set_switch_fmu.u1) annotation (Line(points={{71,
                -74},{136,-74},{136,-68},{140,-68}}, color={0,0,127}));
        connect(T_set_switch_fmu.u3, T_set_HC) annotation (Line(points={{140,-84},{
                110,-84},{110,-128},{-30,-128},{-30,-144},{-126,-144}}, color={0,0,
                127}));
        connect(n_Set_switch_fmu.u2, b_FMU_input) annotation (Line(points={{150,6},
                {134,6},{134,162},{-130,162}}, color={255,0,255}));
        annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
                  -160},{180,180}})), Diagram(coordinateSystem(preserveAspectRatio=
                  false, extent={{-100,-160},{180,180}})));
      end Model_inputs_adapted;

      partial model SystemWithThermostaticValveControl_MA_Daniel
        "SystemWithThermostaticValveControl_MA_Daniel"
        extends BESMod.Systems.Hydraulical.Control.BaseClasses.PartialControl;
        replaceable
          BESMod.Systems.Hydraulical.Control.Components.ThermostaticValveController.BaseClasses.PartialThermostaticValveController
          thermostaticValveController constrainedby
          BESMod.Systems.Hydraulical.Control.Components.ThermostaticValveController.BaseClasses.PartialThermostaticValveController(
            final nZones=transferParameters.nParallelDem, final leakageOpening=
              thermostaticValveParameters.leakageOpening) annotation (
            choicesAllMatching=true, Placement(transformation(extent={{102,-90},
                  {128,-60}})));
        replaceable parameter
          BESMod.Systems.Hydraulical.Control.RecordsCollection.ThermostaticValveDataDefinition
          thermostaticValveParameters annotation (choicesAllMatching=true, Placement(
              transformation(extent={{178,-80},{198,-60}})));
        Modelica.Blocks.Sources.RealExpression realExpression(y=1)
          annotation (Placement(transformation(extent={{-116,-34},{-96,-14}})));
        Modelica.Blocks.Logical.Switch switch1
          annotation (Placement(transformation(extent={{-84,-68},{-64,-48}})));
      equation
        connect(thermostaticValveController.opening, sigBusTra.opening) annotation (
            Line(points={{130.6,-75},{146,-75},{146,-70},{174,-70},{174,-100}},
                                                            color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{6,3},{6,3}},
            horizontalAlignment=TextAlignment.Left));
        connect(thermostaticValveController.TZoneMea, buiMeaBus.TZoneMea) annotation (
           Line(points={{99.4,-66},{65,-66},{65,103}},
              color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(switch1.y, sigBusTra.opening[1]) annotation (Line(points={{-63,
                -58},{174,-58},{174,-100}}, color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{6,3},{6,3}},
            horizontalAlignment=TextAlignment.Left));
        connect(realExpression.y, switch1.u1) annotation (Line(points={{-95,-24},
                {-86,-24},{-86,-50}}, color={0,0,127}));
        annotation (Diagram(graphics={
              Rectangle(
                extent={{74,-58},{206,-100}},
                lineColor={162,29,33},
                lineThickness=1),
              Text(
                extent={{76,-100},{180,-120}},
                lineColor={162,29,33},
                lineThickness=1,
                textString="Thermostatic Valve")}));
      end SystemWithThermostaticValveControl_MA_Daniel;

      partial model SystemWithoutThermostaticValveControl_MA_Daniel
        "SystemWithoutThermostaticValveControl_MA_Daniel"
        extends BESMod.Systems.Hydraulical.Control.BaseClasses.PartialControl;
        replaceable
          BESMod.Systems.Hydraulical.Control.Components.ThermostaticValveController.BaseClasses.PartialThermostaticValveController
          thermostaticValveController constrainedby
          BESMod.Systems.Hydraulical.Control.Components.ThermostaticValveController.BaseClasses.PartialThermostaticValveController(
            final nZones=transferParameters.nParallelDem, final leakageOpening=
              thermostaticValveParameters.leakageOpening) annotation (
            choicesAllMatching=true, Placement(transformation(extent={{90,-52},
                  {116,-22}})));
        replaceable parameter
          BESMod.Systems.Hydraulical.Control.RecordsCollection.ThermostaticValveDataDefinition
          thermostaticValveParameters annotation (choicesAllMatching=true, Placement(
              transformation(extent={{178,-80},{198,-60}})));
        Modelica.Blocks.Sources.RealExpression realExpression(y=1)
          annotation (Placement(transformation(extent={{46,-80},{66,-60}})));
        Modelica.Blocks.Logical.Switch switch1
          annotation (Placement(transformation(extent={{82,-86},{102,-66}})));
      equation
        connect(thermostaticValveController.TZoneMea, buiMeaBus.TZoneMea) annotation (
           Line(points={{87.4,-28},{65,-28},{65,103}},
              color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(switch1.y, sigBusTra.opening[1]) annotation (Line(points={{103,
                -76},{146,-76},{146,-70},{174,-70},{174,-100}}, color={0,0,127}),
            Text(
            string="%second",
            index=1,
            extent={{6,3},{6,3}},
            horizontalAlignment=TextAlignment.Left));
        connect(realExpression.y, switch1.u1) annotation (Line(points={{67,-70},
                {67,-68},{80,-68}}, color={0,0,127}));
        connect(thermostaticValveController.opening[1], switch1.u3) annotation (
           Line(points={{118.6,-37},{128,-37},{128,-64},{42,-64},{42,-82},{80,
                -82},{80,-84}}, color={0,0,127}));
        annotation (Diagram(graphics={
              Rectangle(
                extent={{74,-58},{206,-100}},
                lineColor={162,29,33},
                lineThickness=1),
              Text(
                extent={{76,-100},{180,-120}},
                lineColor={162,29,33},
                lineThickness=1,
                textString="Thermostatic Valve")}));
      end SystemWithoutThermostaticValveControl_MA_Daniel;

      model MultipleInputs_Hysteresis
        "MA Daniel - Multiple Inputs TVL Hysteresis"
        extends PartialHydraulicControl_FMU(
          redeclare
            BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultBivHPControl
            bivalentControlData,
          securityControl(use_antFre=false),
          pI_InverterHeatPumpController(P=1,
            nMin=1/3,                        T_I=100),
          thermostaticValveController(k={thermostaticValveParameters.k}, Ti=
                {thermostaticValveParameters.Ti}));

        BESMod.Systems.Hydraulical.Control.Components.HeatingCurve
          heatingCurve(
          GraHeaCurve=bivalentControlData.gradientHeatCurve,
          THeaThres=bivalentControlData.TSetRoomConst,
          dTOffSet_HC=bivalentControlData.dTOffSetHeatCurve)
          annotation (Placement(transformation(extent={{-216,24},{-196,44}})));

        Modelica.Blocks.Sources.RealExpression u_T_VL
          annotation (Placement(transformation(extent={{-320,16},{-288,34}})));
        Modelica.Blocks.Sources.RealExpression u_comp
          annotation (Placement(transformation(extent={{-320,-10},{-288,10}})));
        Modelica.Blocks.Sources.BooleanExpression b_MPC_input
          annotation (Placement(transformation(extent={{-318,62},{-286,82}})));
        Modelica.Blocks.Sources.BooleanExpression b_FMU_input
          annotation (Placement(transformation(extent={{-318,86},{-286,106}})));
        Modelica.Blocks.Sources.BooleanExpression b_u_comp_ctrl
          annotation (Placement(transformation(extent={{-318,34},{-286,54}})));
        Model_inputs_adapted model_inputs_adapted
          annotation (Placement(transformation(extent={{-250,96},{-222,132}})));
        Modelica.Blocks.Logical.OnOffController onOffController(bandwidth=6)
          annotation (Placement(transformation(extent={{-26,134},{-6,154}})));
      equation
        connect(heatingCurve.TOda, weaBus.TDryBul) annotation (Line(points={{-218,34},
                {-254,34},{-254,2},{-237,2}}, color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(heatingCurve.TSetRoom, useProBus.TZoneSet[1]) annotation (Line(points={{-206,46},
                {-206,78},{-119,78},{-119,103}},  color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{-3,6},{-3,6}},
            horizontalAlignment=TextAlignment.Right));
        connect(b_FMU_input.y, model_inputs_adapted.b_FMU_input) annotation (Line(
              points={{-284.4,96},{-284.4,130.094},{-253,130.094}},
                                                                color={255,0,255}));
        connect(b_MPC_input.y, model_inputs_adapted.b_MPC_input) annotation (Line(
              points={{-284.4,72},{-276,72},{-276,120.988},{-252.6,120.988}},
                                                                          color={255,0,
                255}));
        connect(b_u_comp_ctrl.y, model_inputs_adapted.b_u_comp_ctrl) annotation (Line(
              points={{-284.4,44},{-270,44},{-270,117.388},{-252.6,117.388}},
                                                                          color={255,0,
                255}));
        connect(u_T_VL.y, model_inputs_adapted.u_T_VL_outer) annotation (Line(points={{-286.4,
                25},{-264,25},{-264,114},{-252.6,114}},         color={0,0,127}));
        connect(u_comp.y, model_inputs_adapted.u_comp_outer) annotation (Line(points={{-286.4,
                0},{-258,0},{-258,110.4},{-252.6,110.4}},         color={0,0,127}));
        connect(heatingCurve.TSet, model_inputs_adapted.T_set_HC) annotation (Line(
              points={{-195,34},{-178,34},{-178,4},{-166,4},{-166,60},{-246,60},
                {-246,88},{-244,88},{-244,97.6941},{-252.6,97.6941}},
                                                          color={0,0,127}));
        connect(model_inputs_adapted.n_Set, securityControl.nSet) annotation (
            Line(points={{-217.8,113.576},{178,113.576},{178,84.4},{191.867,
                84.4}}, color={0,0,127}));
        connect(pI_InverterHeatPumpController.n_Set, model_inputs_adapted.u_comp_PID)
          annotation (Line(points={{81,80},{90,80},{90,78},{98,78},{98,126},{
                -260,126},{-260,103.624},{-252.6,103.624}}, color={0,0,127}));
        connect(thermostaticValveController.TZoneSet[1], useProBus.TZoneSet[1])
          annotation (Line(points={{87.4,-46},{30,-46},{30,-24},{-134,-24},{
                -134,30},{-148,30},{-148,78},{-119,78},{-119,103}}, color={0,0,
                127}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(switch1.u2, b_MPC_input.y) annotation (Line(points={{80,-76},{
                22,-76},{22,-78},{-38,-78},{-38,72},{-284.4,72}}, color={255,0,
                255}));
        connect(pI_InverterHeatPumpController.IsOn, sigBusGen.heaPumIsOn)
          annotation (Line(points={{64,68},{-18,68},{-18,24},{-132,24},{-132,
                -99},{-152,-99}}, color={255,0,255}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(onOffController.y, pI_InverterHeatPumpController.HP_On)
          annotation (Line(points={{-5,144},{-5,62},{58,62},{58,80}}, color={
                255,0,255}));
        connect(securityControl.nOut, sigBusGen.yHeaPumSet) annotation (Line(
              points={{227.333,84.4},{302,84.4},{302,-116},{-152,-116},{-152,
                -99}}, color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{6,3},{6,3}},
            horizontalAlignment=TextAlignment.Left));
        connect(onOffController.u, sigBusGen.THeaPumOut) annotation (Line(
              points={{-28,138},{-44,138},{-44,120},{-152,120},{-152,-99}},
              color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(model_inputs_adapted.T_VL_set, onOffController.reference)
          annotation (Line(points={{-218,104.894},{-190,104.894},{-190,102},{
                -150,102},{-150,150},{-28,150}}, color={0,0,127}));
        connect(pI_InverterHeatPumpController.T_Set, onOffController.reference)
          annotation (Line(points={{58,86},{-60,86},{-60,102},{-150,102},{-150,
                150},{-28,150}}, color={0,0,127}));
      end MultipleInputs_Hysteresis;

      model HeatingCurve "MA Daniel - HeatingCurve"
        extends PartialHydraulicControl_FMU(
          redeclare
            BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultBivHPControl
            bivalentControlData,
          securityControl(use_antFre=false),
          pI_InverterHeatPumpController(P=1,
            nMin=1/3,                        T_I=100),
          thermostaticValveController(k={thermostaticValveParameters.k}, Ti=
                {thermostaticValveParameters.Ti}));

        Modelica.Blocks.Sources.RealExpression u_T_VL
          annotation (Placement(transformation(extent={{-320,16},{-288,34}})));
        Modelica.Blocks.Sources.RealExpression u_comp
          annotation (Placement(transformation(extent={{-320,-10},{-288,10}})));
        Modelica.Blocks.Sources.BooleanExpression b_MPC_input
          annotation (Placement(transformation(extent={{-318,62},{-286,82}})));
        Modelica.Blocks.Sources.BooleanExpression b_FMU_input
          annotation (Placement(transformation(extent={{-318,86},{-286,106}})));
        Modelica.Blocks.Sources.BooleanExpression b_u_comp_ctrl
          annotation (Placement(transformation(extent={{-318,34},{-286,54}})));
        Model_inputs_adapted model_inputs_adapted
          annotation (Placement(transformation(extent={{-250,96},{-222,132}})));
        Modelica.Blocks.Logical.OnOffController onOffController(bandwidth=6)
          annotation (Placement(transformation(extent={{-26,134},{-6,154}})));
        HeatingCurveDaniel heatingCurveDaniel
          annotation (Placement(transformation(extent={{-206,34},{-186,54}})));
      equation
        connect(b_FMU_input.y, model_inputs_adapted.b_FMU_input) annotation (Line(
              points={{-284.4,96},{-284.4,130.094},{-253,130.094}},
                                                                color={255,0,255}));
        connect(b_MPC_input.y, model_inputs_adapted.b_MPC_input) annotation (Line(
              points={{-284.4,72},{-276,72},{-276,120.988},{-252.6,120.988}},
                                                                          color={255,0,
                255}));
        connect(b_u_comp_ctrl.y, model_inputs_adapted.b_u_comp_ctrl) annotation (Line(
              points={{-284.4,44},{-270,44},{-270,117.388},{-252.6,117.388}},
                                                                          color={255,0,
                255}));
        connect(u_T_VL.y, model_inputs_adapted.u_T_VL_outer) annotation (Line(points={{-286.4,
                25},{-264,25},{-264,114},{-252.6,114}},         color={0,0,127}));
        connect(u_comp.y, model_inputs_adapted.u_comp_outer) annotation (Line(points={{-286.4,
                0},{-258,0},{-258,110.4},{-252.6,110.4}},         color={0,0,127}));
        connect(thermostaticValveController.TZoneSet[1], useProBus.TZoneSet[1])
          annotation (Line(points={{87.4,-46},{30,-46},{30,-24},{-134,-24},{
                -134,30},{-148,30},{-148,78},{-119,78},{-119,103}}, color={0,0,
                127}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(switch1.u2, b_MPC_input.y) annotation (Line(points={{80,-76},{
                22,-76},{22,-78},{-38,-78},{-38,72},{-284.4,72}}, color={255,0,
                255}));
        connect(onOffController.y, pI_InverterHeatPumpController.HP_On)
          annotation (Line(points={{-5,144},{-5,62},{58,62},{58,80}}, color={
                255,0,255}));
        connect(securityControl.nOut, sigBusGen.yHeaPumSet) annotation (Line(
              points={{227.333,84.4},{302,84.4},{302,-116},{-152,-116},{-152,
                -99}}, color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{6,3},{6,3}},
            horizontalAlignment=TextAlignment.Left));
        connect(onOffController.u, sigBusGen.THeaPumOut) annotation (Line(
              points={{-28,138},{-44,138},{-44,120},{-152,120},{-152,-99}},
              color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(model_inputs_adapted.T_VL_set, onOffController.reference)
          annotation (Line(points={{-218,104.894},{-190,104.894},{-190,102},{
                -150,102},{-150,150},{-28,150}}, color={0,0,127}));
        connect(pI_InverterHeatPumpController.T_Set, onOffController.reference)
          annotation (Line(points={{58,86},{-60,86},{-60,102},{-150,102},{-150,
                150},{-28,150}}, color={0,0,127}));
        connect(heatingCurveDaniel.TOda, weaBus.TDryBul) annotation (Line(points={{-208,
                44},{-220,44},{-220,46},{-237,46},{-237,2}}, color={0,0,127}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(heatingCurveDaniel.TSet, model_inputs_adapted.T_set_HC)
          annotation (Line(points={{-185,44},{-180,44},{-180,42},{-174,42},{
                -174,84},{-252.6,84},{-252.6,97.6941}}, color={0,0,127}));
        connect(heatingCurveDaniel.TRaumnom, useProBus.TZoneSet[1]) annotation (
           Line(points={{-196,56},{-204,56},{-204,103},{-119,103}}, color={0,0,
                127}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(pI_InverterHeatPumpController.IsOn, sigBusGen.heaPumIsOn)
          annotation (Line(points={{64,68},{50,68},{50,24},{-90,24},{-90,-99},{
                -152,-99}}, color={255,0,255}), Text(
            string="%second",
            index=1,
            extent={{-6,3},{-6,3}},
            horizontalAlignment=TextAlignment.Right));
        connect(model_inputs_adapted.n_Set, securityControl.nSet) annotation (
            Line(points={{-217.8,113.576},{176,113.576},{176,88},{191.867,88},{
                191.867,84.4}}, color={0,0,127}));
        connect(model_inputs_adapted.u_comp_PID, pI_InverterHeatPumpController.n_Set)
          annotation (Line(points={{-252.6,103.624},{-286,103.624},{-286,140},{
                102,140},{102,80},{81,80}}, color={0,0,127}));
      end HeatingCurve;
    end Controls;
    extends BESMod.Utilities.Icons.SystemIcon;
    model NoDistributionSystem "Distribution MA Daniel"
      extends
        BESMod.Systems.Hydraulical.Distribution.BaseClasses.PartialDistribution(
        use_dhw=false,
        final fFullSto=0,
        final QDHWBefSto_flow_nominal=Modelica.Constants.eps,
        final VStoDHW=0,
        QCrit=0,
        tCrit=0,
        final QDHWStoLoss_flow=0,
        final designType=BESMod.Systems.Hydraulical.Distribution.Types.DHWDesignType.NoStorage,
        QDHW_flow_nominal=Modelica.Constants.eps,
        final dpDem_nominal=fill(0, nParallelDem),
        final dpSup_nominal=dpTraSys_nominal,
        final nParallelSup=1,
        final dTTraDHW_nominal=0,
        final QLoss_flow_nominal=f_design .* Q_flow_nominal .- Q_flow_nominal,
        final f_design=fill(1, nParallelDem),
        final dTLoss_nominal=fill(0, nParallelDem),
        final m_flow_nominal=mSup_flow_nominal,
        final TSup_nominal=TDem_nominal .+ dTLoss_nominal .+ dTTra_nominal,
        redeclare package MediumGen = Medium,
        redeclare package MediumDHW = Medium,
        final dTTra_nominal=fill(0, nParallelDem));
      parameter Modelica.Units.SI.PressureDifference dpTraSys_nominal[nParallelDem]
        "Nominal pressure loss of transfer system";

      BESMod.Utilities.Electrical.ZeroLoad zeroLoad
        annotation (Placement(transformation(extent={{16,-74},{36,-54}})));
    equation
      connect(zeroLoad.internalElectricalPin, internalElectricalPin)
        annotation (Line(
          points={{36,-64},{70,-64},{70,-98}},
          color={0,0,0},
          thickness=1));
      connect(portGen_in[1], portBui_out[1])
        annotation (Line(points={{-100,80},{100,80}}, color={0,127,255}));
      connect(portGen_out[1], portBui_in[1])
        annotation (Line(points={{-100,40},{100,40}}, color={0,127,255}));
    end NoDistributionSystem;

    model HeatPump_without_HeatingRod "Monovalent heat pump MA Daniel"
      extends
        BESMod.Systems.Hydraulical.Interfaces.Generation.BaseClasses.PartialGeneration(
        redeclare package Medium = IBPSA.Media.Air,
        final QLoss_flow_nominal=f_design .* Q_flow_nominal .- Q_flow_nominal,
        final dTLoss_nominal=fill(0, nParallelDem),
        dTTra_nominal={if TDem_nominal[i] > 273.15 + 55 then 10 elseif TDem_nominal[
            i] > 44.9 + 273.15 then 8 else 5 for i in 1:nParallelDem},
        dp_nominal={heatPump.dpCon_nominal},
          nParallelDem=1);

      parameter Boolean use_airSource=true
        "Turn false to use water as temperature source."
         annotation(Dialog(group="Component choices"));

       parameter Boolean use_heaRod=false
                                         "=false to disable the heating rod"
         annotation(Dialog(group="Component choices"));
        replaceable model PerDataMainHP =
          AixLib.DataBase.HeatPump.PerformanceData.LookUpTable2D
        constrainedby
        AixLib.DataBase.HeatPump.PerformanceData.BaseClasses.PartialPerformanceData
        annotation (Dialog(group="Component data"), __Dymola_choicesAllMatching=true);
      parameter Modelica.Media.Interfaces.Types.Temperature TSoilConst=273.15 +
          10
        "Constant soil temperature for ground source heat pumps"
        annotation(Dialog(group="Component choices", enable=use_airSource));

      replaceable package Medium_eva = IBPSA.Media.Air                         constrainedby
        Modelica.Media.Interfaces.PartialMedium annotation (Dialog(group="Component choices"),
          choices(
            choice(redeclare package Medium = IBPSA.Media.Air "Moist air"),
            choice(redeclare package Medium = IBPSA.Media.Water "Water"),
            choice(redeclare package Medium =
                IBPSA.Media.Antifreeze.PropyleneGlycolWater (
                  property_T=293.15,
                  X_a=0.40)
                  "Propylene glycol water, 40% mass fraction")));
      replaceable parameter
        BESMod.Systems.Hydraulical.Interfaces.Generation.RecordsCollection.DefaultHP
        heatPumpParameters(genDesTyp=BESMod.Systems.Hydraulical.Interfaces.Generation.Types.GenerationDesign.Monovalent)
        constrainedby
        BESMod.Systems.Hydraulical.Interfaces.Generation.RecordsCollection.HeatPumpBaseDataDefinition(
          final QGen_flow_nominal=Q_flow_nominal[1], final TOda_nominal=
            TOda_nominal) annotation (
        Dialog(group="Component data"),
        choicesAllMatching=true,
        Placement(transformation(extent={{-98,14},{-82,28}})));

      replaceable parameter
        BESMod.Systems.RecordsCollection.Movers.DefaultMover
        pumpData annotation (Dialog(group="Component data"),
        choicesAllMatching=true, Placement(transformation(extent={{42,-56},
                {56,-44}})));
      replaceable parameter
        BESMod.Systems.RecordsCollection.TemperatureSensors.DefaultSensor
        temperatureSensorData
        annotation (Dialog(group="Component data"), choicesAllMatching=true,
        Placement(transformation(extent={{60,10},{80,30}})));

      AixLib.Fluid.HeatPumps.HeatPump heatPump(
        redeclare package Medium_con = Medium,
        redeclare package Medium_eva = Medium_eva,
        final use_rev=true,
        final use_autoCalc=false,
        final Q_useNominal=0,
        final scalingFactor=heatPumpParameters.scalingFactor,
        final use_refIne=heatPumpParameters.use_refIne,
        final refIneFre_constant=heatPumpParameters.refIneFre_constant,
        final nthOrder=1,
        final useBusConnectorOnly=false,
        final mFlow_conNominal=m_flow_nominal[1],
        final VCon=heatPumpParameters.VCon,
        final dpCon_nominal=heatPumpParameters.dpCon_nominal,
        final use_conCap=false,
        final CCon=0,
        final GConOut=0,
        final GConIns=0,
        final mFlow_evaNominal=heatPumpParameters.mEva_flow_nominal,
        final VEva=heatPumpParameters.VEva,
        final dpEva_nominal=heatPumpParameters.dpEva_nominal,
        final use_evaCap=false,
        final CEva=0,
        final GEvaOut=0,
        final GEvaIns=0,
        final tauSenT=temperatureSensorData.tau,
        final transferHeat=temperatureSensorData.transferHeat,
        final allowFlowReversalEva=allowFlowReversal,
        final allowFlowReversalCon=allowFlowReversal,
        final tauHeaTraEva=temperatureSensorData.tauHeaTra,
        final TAmbEva_nominal=temperatureSensorData.TAmb,
        final tauHeaTraCon=temperatureSensorData.tauHeaTra,
        final TAmbCon_nominal=temperatureSensorData.TAmb,
        final pCon_start=p_start,
        final TCon_start=T_start,
        final pEva_start=Medium_eva.p_default,
        final TEva_start=Medium_eva.T_default,
        final energyDynamics=energyDynamics,
        final show_TPort=show_T,
        redeclare model PerDataMainHP = PerDataMainHP,
        redeclare model PerDataRevHP =
            AixLib.DataBase.Chiller.PerformanceData.LookUpTable2D (dataTable=
                AixLib.DataBase.Chiller.EN14511.Vitocal200AWO201()))
                                                     annotation (Placement(
            transformation(
            extent={{22,-27},{-22,27}},
            rotation=270,
            origin={-44,15})));

      IBPSA.Fluid.Sources.Boundary_ph bou_sinkAir(final nPorts=1, redeclare
          package Medium = Medium_eva)                       annotation (Placement(
            transformation(
            extent={{10,-10},{-10,10}},
            rotation=180,
            origin={-90,-10})));
      IBPSA.Fluid.Sources.MassFlowSource_T bou_air(
        final m_flow=heatPumpParameters.mEva_flow_nominal,
        final use_T_in=true,
        redeclare package Medium = Medium_eva,
        final use_m_flow_in=false,
        final nPorts=1)
        annotation (Placement(transformation(extent={{-100,40},{-80,60}})));

      Modelica.Blocks.Logical.Switch switch2 annotation (Placement(transformation(
            extent={{-10,-10},{10,10}},
            rotation=0,
            origin={-130,50})));
      Modelica.Blocks.Sources.BooleanConstant AirOrSoil(final k=use_airSource)
        annotation (Placement(transformation(
            extent={{-10,-10},{10,10}},
            rotation=0,
            origin={-170,90})));

      BESMod.Utilities.KPIs.EnergyKPICalculator KPIWel(use_inpCon=true)
        annotation (Placement(transformation(extent={{-140,-80},{-120,-60}})));

      IBPSA.Fluid.Movers.SpeedControlled_y pump(
        redeclare final package Medium = Medium,
        final energyDynamics=energyDynamics,
        final p_start=p_start,
        final T_start=T_start,
        final allowFlowReversal=allowFlowReversal,
        final show_T=show_T,
        redeclare
          BESMod.Systems.RecordsCollection.Movers.AutomaticConfigurationData
          per(
          final speed_rpm_nominal=pumpData.speed_rpm_nominal,
          final m_flow_nominal=m_flow_nominal[1],
          final dp_nominal=dpDem_nominal[1] + dp_nominal[1],
          final rho=rho,
          final V_flowCurve=pumpData.V_flowCurve,
          final dpCurve=pumpData.dpCurve),
        final inputType=IBPSA.Fluid.Types.InputType.Continuous,
        final addPowerToMedium=pumpData.addPowerToMedium,
        final tau=pumpData.tau,
        final use_inputFilter=pumpData.use_inputFilter,
        final riseTime=pumpData.riseTimeInpFilter,
        final init=Modelica.Blocks.Types.Init.InitialOutput,
        final y_start=1)                 annotation (Placement(transformation(
            extent={{-10,10},{10,-10}},
            rotation=180,
            origin={10,-70})));

      Modelica.Blocks.Sources.Constant TSoil(k=TSoilConst)
        annotation (Placement(transformation(
            extent={{-10,-10},{10,10}},
            rotation=0,
            origin={-170,50})));

      BESMod.Utilities.KPIs.EnergyKPICalculator KPIQHP(use_inpCon=false, y=heatPump.con.QFlow_in)
        annotation (Placement(transformation(extent={{-140,-112},{-120,-92}})));

      IBPSA.Fluid.Sources.Boundary_pT bouPum(
        redeclare package Medium = Medium,
        final p=p_start,
        final T=T_start,
        final nPorts=1) "Pressure boundary for pump" annotation (Placement(
            transformation(
            extent={{-10,-10},{10,10}},
            rotation=90,
            origin={50,-86})));

      BESMod.Utilities.Electrical.RealToElecCon realToElecCon(use_souGen=false)
        annotation (Placement(transformation(
            extent={{-10,-10},{10,10}},
            rotation=180,
            origin={100,-78})));
      Modelica.Blocks.Math.MultiSum multiSum(nu=2)                           annotation (Placement(
            transformation(
            extent={{-6,-6},{6,6}},
            rotation=180,
            origin={130,-82})));
      BESMod.Utilities.KPIs.DeviceKPICalculator KPISwitches(
        use_reaInp=false,
        calc_singleOnTime=true,
        calc_totalOnTime=true,
        calc_numSwi=true)
        annotation (Placement(transformation(extent={{-138,-26},{-118,-6}})));
      Modelica.Blocks.Sources.BooleanExpression booExpHeaPumIsOn(y=heatPump.greaterThreshold.y)
        annotation (Placement(transformation(extent={{-180,-20},{-160,0}})));
      Modelica.Blocks.Sources.RealExpression reaExpPEleHeaPum(y=heatPump.innerCycle.Pel)
        annotation (Placement(transformation(extent={{-180,-80},{-160,-60}})));
      Modelica.Blocks.Sources.Constant conIceFac(final k=1) annotation (Placement(
            transformation(
            extent={{-11,-11},{11,11}},
            rotation=0,
            origin={-169,15})));
      Modelica.Blocks.Sources.RealExpression reaExpTHeaPumOut(y=heatPump.senT_b1.T)
        annotation (Placement(transformation(extent={{-62,78},{-42,98}})));
      Modelica.Blocks.Sources.RealExpression reaExpTHeaPumIn(y=heatPump.senT_a1.T)
        annotation (Placement(transformation(extent={{-60,60},{-40,80}})));
      Modelica.Blocks.Sources.BooleanConstant conNotRev(final k=true) annotation (
          Placement(transformation(
            extent={{-10,-10},{10,10}},
            rotation=0,
            origin={-170,-38})));
      Modelica.Blocks.Sources.RealExpression reaExpTEvaIn(y=heatPump.senT_a2.T)
        annotation (Placement(transformation(extent={{-60,40},{-40,60}})));

    equation

      connect(bou_air.ports[1], heatPump.port_a2) annotation (Line(
          points={{-80,50},{-74,50},{-74,42},{-57.5,42},{-57.5,37}},
          color={0,127,255}));
      connect(heatPump.port_b2, bou_sinkAir.ports[1]) annotation (Line(
          points={{-57.5,-7},{-56,-7},{-56,-10},{-80,-10}},
          color={0,127,255}));
      connect(bou_air.T_in, switch2.y)
        annotation (Line(points={{-102,54},{-108,54},{-108,50},{-119,50}},
                                                       color={0,0,127}));
      connect(switch2.u2, AirOrSoil.y)
        annotation (Line(points={{-142,50},{-152,50},{-152,90},{-159,90}},
                                                         color={255,0,255}));
      connect(pump.port_a, portGen_in[1]) annotation (Line(
          points={{20,-70},{100,-70},{100,-2}},
          color={0,127,255}));

      connect(pump.port_b, heatPump.port_a1) annotation (Line(
          points={{1.77636e-15,-70},{-30.5,-70},{-30.5,-7}},
          color={0,127,255}));
      connect(TSoil.y, switch2.u3) annotation (Line(points={{-159,50},{-156,50},{-156,
              42},{-142,42}},       color={0,0,127}));
      connect(bouPum.ports[1], pump.port_a)
        annotation (Line(points={{50,-76},{50,-70},{20,-70}}, color={0,127,255}));

      connect(realToElecCon.internalElectricalPin, internalElectricalPin)
        annotation (Line(
          points={{89.8,-78.2},{72,-78.2},{72,-100}},
          color={0,0,0},
          thickness=1));
      connect(multiSum.y, realToElecCon.PEleLoa)
        annotation (Line(points={{122.98,-82},{112,-82}}, color={0,0,127}));

      connect(multiSum.u[2], pump.P) annotation (Line(points={{136,-83.05},{144,
              -83.05},{144,-114},{-14,-114},{-14,-44},{0,-44},{0,-61},{-1,-61}},
                                                 color={0,0,127}));
      connect(multiSum.u[1], reaExpPEleHeaPum.y) annotation (Line(points={{136,
              -80.95},{144,-80.95},{144,-126},{-154,-126},{-154,-70},{-159,-70}},
                                                               color={0,0,127}),
          Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));

      connect(switch2.u1, weaBus.TDryBul) annotation (Line(points={{-142,58},{-144,58},
              {-144,80},{-101,80}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(KPIQHP.KPI, outBusGen.QHP_flow) annotation (Line(points={{-117.8,-102},
              {0,-102},{0,-100}},              color={135,135,135}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
      connect(KPIWel.KPI, outBusGen.PEleHP) annotation (Line(points={{-117.8,-70},{-104,
              -70},{-104,-102},{0,-102},{0,-100}},                   color={135,135,
              135}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
      connect(KPISwitches.KPI, outBusGen.heaPum) annotation (Line(points={{-115.8,-16},
              {-110,-16},{-110,-100},{0,-100}}, color={135,135,135}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
      connect(booExpHeaPumIsOn.y, KPISwitches.u) annotation (Line(points={{-159,-10},
              {-152,-10},{-152,-16},{-140.2,-16}}, color={255,0,255}));
      connect(reaExpPEleHeaPum.y, KPIWel.u)
        annotation (Line(points={{-159,-70},{-141.8,-70}}, color={0,0,127}));
      connect(conIceFac.y, heatPump.iceFac_in) annotation (Line(points={{-156.9,15},
              {-106,15},{-106,-1.72},{-74.6,-1.72}}, color={0,0,127}));
      connect(booExpHeaPumIsOn.y, sigBusGen.heaPumIsOn) annotation (Line(points={{-159,
              -10},{-146,-10},{-146,116},{2,116},{2,98}},
                                                 color={255,0,255}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
      connect(reaExpTHeaPumIn.y, sigBusGen.THeaPumIn) annotation (Line(points={{-39,70},
              {-36,70},{-36,102},{2,102},{2,98}},
                                  color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
      connect(reaExpTHeaPumOut.y, sigBusGen.THeaPumOut) annotation (Line(points={{-41,88},
              {-38,88},{-38,98},{2,98}},            color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
      connect(heatPump.modeSet, conNotRev.y) annotation (Line(points={{-48.5,-10.52},
              {-48.5,-38},{-159,-38}}, color={255,0,255}));
      connect(reaExpTEvaIn.y, sigBusGen.THeaPumEvaIn) annotation (Line(points={{-39,50},
              {2,50},{2,98}},     color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
      connect(portGen_out[1], heatPump.port_b1) annotation (Line(points={{100,80},{36,
              80},{36,37},{-30.5,37}}, color={0,127,255}));
      connect(heatPump.nSet, sigBusGen.yHeaPumSet) annotation (Line(points={{
              -39.5,-10.52},{-39.5,-28},{24,-28},{24,98},{2,98}}, color={0,0,
              127}), Text(
          string="%second",
          index=1,
          extent={{-3,-6},{-3,-6}},
          horizontalAlignment=TextAlignment.Right));
      connect(pump.y, sigBusGen.uPump) annotation (Line(points={{10,-58},{10,74},
              {2,74},{2,98}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
      annotation (Line(
          points={{-52.775,-6.78},{-52.775,33.61},{-56,33.61},{-56,74}},
          color={255,204,51},
          thickness=0.5),
                  Diagram(coordinateSystem(extent={{-180,-140},{100,100}})));
    end HeatPump_without_HeatingRod;

    block HeatPumpHysteresis
      "On-off controller for a heat pump control MA Daniel"
      extends Modelica.Blocks.Icons.PartialBooleanBlock;
      Modelica.Blocks.Interfaces.RealInput T_set "Set temperature"
        annotation (Placement(transformation(extent={{-140,100},{-100,60}})));
      Modelica.Blocks.Interfaces.RealInput T_sup
        "Connector of Real input signal used as measurement signal of supply temperature after heat pump"
        annotation (Placement(transformation(extent={{-140,-60},{-100,-100}})));
      Modelica.Blocks.Interfaces.BooleanOutput y
        "Connector of Real output signal used as actuator signal"
        annotation (Placement(transformation(extent={{100,-10},{120,10}})));

      parameter Real bandwidth(start=0.1) "Bandwidth around reference signal";
      parameter Boolean pre_y_start=false "Value of pre(y) at initial time";

    initial equation
      pre(y) = pre_y_start;
    equation
      y = pre(y) and (T_sup < T_set + bandwidth/2) or (T_sup < T_set - bandwidth/2);
      annotation (Icon(coordinateSystem(preserveAspectRatio=true, extent={{-100,
                -100},{100,100}}), graphics={
            Text(
              extent={{-92,74},{44,44}},
              textString="reference"),
            Text(
              extent={{-94,-52},{-34,-74}},
              textString="u"),
            Line(points={{-76,-32},{-68,-6},{-50,26},{-24,40},{-2,42},{16,36},{32,28},{48,12},{58,-6},{68,-28}},
              color={0,0,127}),
            Line(points={{-78,-2},{-6,18},{82,-12}},
              color={255,0,0}),
            Line(points={{-78,12},{-6,30},{82,0}}),
            Line(points={{-78,-16},{-6,4},{82,-26}}),
            Line(points={{-82,-18},{-56,-18},{-56,-40},{64,-40},{64,-20},{90,-20}},
              color={255,0,255})}), Documentation(info="<html>
<p>The block StorageHysteresis sets the output signal <b>y</b> to <b>true</b> when the input signal <b>T_top</b> falls below the <b>T_set</b> signal minus half of the bandwidth and sets the output signal <b>y</b> to <b>false</b> when the input signal <b>T_bot</b> exceeds the <b>T_set</b> signal plus half of the bandwidth.</p>
<p>This control ensure that the whole storage has the required temperature. If you just want to control one layer, apply the same Temperature to both <b>T_top</b> and <b>T_bot</b>.</p>
</html>"));
    end HeatPumpHysteresis;

    partial model PartialHydraulicControl
      "PartialModelHydraulicControl MA Daniel"
      extends
        BESMod.Systems.Hydraulical.Control.BaseClasses.SystemWithThermostaticValveControl(
          redeclare
          BESMod.Systems.Hydraulical.Control.RecordsCollection.ThermostaticValveDataDefinition
          thermostaticValveParameters,
         redeclare
          BESMod.Systems.Hydraulical.Control.Components.ThermostaticValveController.ThermostaticValvePIControlled
          thermostaticValveController);
      replaceable BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultSafetyControl
        safetyControl
        annotation (choicesAllMatching=true,Placement(transformation(extent={{200,30},{220,50}})));
      replaceable parameter
        BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultBivHPControl
        bivalentControlData constrainedby
        BESMod.Systems.Hydraulical.Control.RecordsCollection.BivalentHeatPumpControlDataDefinition(
        final TOda_nominal=generationParameters.TOda_nominal,
        TSup_nominal=generationParameters.TSup_nominal[1],
        TSetRoomConst=sum(transferParameters.TDem_nominal)/transferParameters.nParallelDem)
        annotation (choicesAllMatching=true, Placement(transformation(extent={{-80,-68},
                {-58,-46}})));
      replaceable
        BESMod.Systems.Hydraulical.Control.Components.HeatPumpNSetController.PI_InverterHeatPumpController
        HP_nSet_Controller(
        P=bivalentControlData.k,
        nMin=bivalentControlData.nMin,
        T_I=bivalentControlData.T_I)
                           annotation (choicesAllMatching=true, Placement(
            transformation(extent={{82,64},{112,92}})));

      AixLib.Controls.HeatPump.SafetyControls.SafetyControl securityControl(
        final minRunTime=safetyControl.minRunTime,
        final minLocTime=safetyControl.minLocTime,
        final maxRunPerHou=safetyControl.maxRunPerHou,
        final use_opeEnv=safetyControl.use_opeEnv,
        final use_opeEnvFroRec=false,
        final dataTable=AixLib.DataBase.HeatPump.EN14511.Vitocal200AWO201(
            tableUppBou=[-20,50; -10,60; 30,60; 35,55]),
        final tableUpp=safetyControl.tableUpp,
        final use_minRunTime=safetyControl.use_minRunTime,
        final use_minLocTime=safetyControl.use_minLocTime,
        final use_runPerHou=safetyControl.use_runPerHou,
        final dTHystOperEnv=safetyControl.dT_opeEnv,
        final use_deFro=false,
        final minIceFac=0,
        final use_chiller=false,
        final calcPel_deFro=0,
        final pre_n_start=safetyControl.pre_n_start_hp,
        use_antFre=false) annotation (Placement(transformation(
            extent={{-16,-17},{16,17}},
            rotation=0,
            origin={210,81})));
      Modelica.Blocks.Sources.BooleanConstant hp_mode(final k=true) annotation (
          Placement(transformation(
            extent={{-7,-7},{7,7}},
            rotation=0,
            origin={155,69})));

      Modelica.Blocks.Math.BooleanToReal booleanToReal1 "Turn Pump in heat pump on"
                                                       annotation (Placement(
            transformation(
            extent={{-10,-10},{10,10}},
            rotation=270,
            origin={-170,-54})));
      BESMod.Systems.Hydraulical.Control.Components.HeatPumpBusPassThrough heatPumpBusPassThrough
        annotation (Placement(transformation(
            extent={{-10,-10},{10,10}},
            rotation=180,
            origin={30,-4})));

      Modelica.Blocks.Sources.BooleanExpression pumpIsOn(y=true)
        annotation (Placement(transformation(extent={{-230,-44},{-210,-24}})));
    equation

      connect(securityControl.modeSet, hp_mode.y) annotation (Line(points={{191.867,
              77.6},{168,77.6},{168,69},{162.7,69}}, color={255,0,255}));
      connect(HP_nSet_Controller.n_Set, securityControl.nSet) annotation (Line(
            points={{113.5,78},{144,78},{144,84.4},{191.867,84.4}}, color={0,0,127}));

      connect(HP_nSet_Controller.IsOn, sigBusGen.heaPumIsOn) annotation (Line(
            points={{88,61.2},{88,-110},{-88,-110},{-88,-104},{-152,-104},{-152,-99}},
                                                               color={255,0,255}),
          Text(
          string="%second",
          index=1,
          extent={{-3,-6},{-3,-6}},
          horizontalAlignment=TextAlignment.Right));
      connect(heatPumpBusPassThrough.sigBusGen, sigBusGen) annotation (Line(
          points={{40,-4},{52,-4},{52,12},{-152,12},{-152,-99}},
          color={255,204,51},
          thickness=0.5), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
      connect(heatPumpBusPassThrough.vapourCompressionMachineControlBus,
        securityControl.sigBusHP) annotation (Line(
          points={{19.8,-3.8},{18,-3.8},{18,-16},{184,-16},{184,69.27},{192,69.27}},
          color={255,204,51},
          thickness=0.5));
      connect(securityControl.nOut, sigBusGen.yHeaPumSet) annotation (Line(points={{227.333,
              84.4},{282,84.4},{282,-130},{-152,-130},{-152,-99}},         color={0,
              0,127}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
      connect(booleanToReal1.y, sigBusGen.uPump) annotation (Line(points={{-170,-65},
              {-170,-76},{-106,-76},{-106,-99},{-152,-99}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{-3,-6},{-3,-6}},
          horizontalAlignment=TextAlignment.Right));
      connect(HP_nSet_Controller.T_Meas, sigBusGen.THeaPumOut) annotation (Line(
            points={{97,61.2},{97,-32},{-152,-32},{-152,-99}}, color={0,0,127}),
          Text(
          string="%second",
          index=1,
          extent={{-3,-6},{-3,-6}},
          horizontalAlignment=TextAlignment.Right));
      connect(pumpIsOn.y, booleanToReal1.u) annotation (Line(points={{-209,-34},
              {-192,-34},{-192,-32},{-170,-32},{-170,-42}}, color={255,0,255}));
      annotation (Diagram(graphics={
            Rectangle(
              extent={{0,100},{132,52}},
              lineColor={28,108,200},
              lineThickness=1),
            Text(
              extent={{4,122},{108,102}},
              lineColor={28,108,200},
              lineThickness=1,
              textString="Heat Pump Control"),
            Rectangle(
              extent={{138,100},{240,52}},
              lineColor={28,108,200},
              lineThickness=1),
            Text(
              extent={{138,122},{242,102}},
              lineColor={28,108,200},
              lineThickness=1,
              textString="Heat Pump Safety")}));
    end PartialHydraulicControl;

    model UFHTransferSystem_Ueberstroemventil "MA Daniel"
      extends BESMod.Systems.Hydraulical.Transfer.BaseClasses.PartialTransfer(
        final mSup_flow_nominal,
        final QSup_flow_nominal,
        final QLoss_flow_nominal,
        final f_design,
        final dTLoss_nominal,
        final m_flow_nominal,
        final dTTra_nominal,
        final TSup_nominal,
          final nParallelSup=1,
        final dp_nominal=transferDataBaseDefinition.dp_nominal);

      IBPSA.Fluid.FixedResistances.PressureDrop res1[nParallelDem](
        redeclare package Medium = Medium,
        each final dp_nominal=transferDataBaseDefinition.dpHeaDistr_nominal,
        final m_flow_nominal=m_flow_nominal)     "Hydraulic resistance of supply"
        annotation (Placement(transformation(
            extent={{-10.5,-12},{10.5,12}},
            rotation=0,
            origin={-24.5,88})));

      BESMod.Systems.Hydraulical.Components.UFH.PanelHeating panelHeating[
        nParallelDem](
        redeclare package Medium = Medium,
        final floorHeatingType=floorHeatingType,
        each final dis=5,
        final A=UFHParameters.area,
        final T0=TDem_nominal,
        each calcMethod=1) annotation (Placement(transformation(
            extent={{-23,-10},{23,10}},
            rotation=270,
            origin={5,-4})));

      Modelica.Thermal.HeatTransfer.Sources.FixedTemperature
                                                          fixedTemperature
                                                                       [nParallelDem](each final
                T=UFHParameters.T_floor)
                   annotation (Placement(transformation(
            extent={{-10,-10},{10,10}},
            rotation=0,
            origin={-90,10})));
      Modelica.Thermal.HeatTransfer.Sensors.HeatFlowSensor heatFlowSensor
                                                                       [nParallelDem]
                   annotation (Placement(transformation(
            extent={{-10,-10},{10,10}},
            rotation=0,
            origin={-46,-6})));
      Modelica.Thermal.HeatTransfer.Sources.FixedHeatFlow fixedHeatFlow[nParallelDem](each final
                Q_flow=0)
                   annotation (Placement(transformation(
            extent={{-10,-10},{10,10}},
            rotation=0,
            origin={-92,-20})));
      Modelica.Thermal.HeatTransfer.Components.HeatCapacitor
                                                          heatCapacitor[nParallelDem](each final
                C=100)
                   annotation (Placement(transformation(
            extent={{-10,-10},{10,10}},
            rotation=0,
            origin={-114,2})));

      replaceable parameter
        BESMod.Systems.Hydraulical.Transfer.RecordsCollection.UFHData
        UFHParameters constrainedby
        BESMod.Systems.Hydraulical.Transfer.RecordsCollection.UFHData(nZones=
            nParallelDem, area=AZone) annotation (choicesAllMatching=true,
          Placement(transformation(extent={{26,40},{46,60}})));

      BESMod.Utilities.Electrical.ZeroLoad zeroLoad
        annotation (Placement(transformation(extent={{32,-108},{52,-88}})));
      BESMod.Systems.Hydraulical.Distribution.Components.Valves.PressureReliefValve
        pressureReliefValveOnOff(
        redeclare final package Medium = Medium,
        m_flow_nominal=mSup_flow_nominal[1],
        dpFullOpen_nominal=dpFullOpen_nominal,
        dpThreshold_nominal=perPreRelValOpens*dpFullOpen_nominal,
        l=transferDataBaseDefinition.leakageOpening,
        use_inputFilter=false,
        dpValve_nominal=transferDataBaseDefinition.dpPumpHeaCir_nominal,
        val(R=50))
        annotation (Placement(transformation(
            extent={{-10,-10},{10,10}},
            rotation=270,
            origin={-142,6})));
      AixLib.Fluid.Actuators.Valves.TwoWayEqualPercentage val[nParallelDem](
        redeclare each final package Medium = Medium,
        final m_flow_nominal=m_flow_nominal,
        each final CvData=AixLib.Fluid.Types.CvTypes.OpPoint,
        each final deltaM=deltaMValve,
        each final from_dp=from_dpBranches,
        final dpValve_nominal=transferDataBaseDefinition.dpHeaSysValve_nominal,
        each final allowFlowReversal=allowFlowReversal,
        each final show_T=show_T,
        each final linearized=linearized,
        final dpFixed_nominal=transferDataBaseDefinition.dpHeaSysPreValve_nominal,
        each final l=transferDataBaseDefinition.leakageOpening,
        each final R=R,
        each final delta0=delta0,
        each final rhoStd=rho,
        each final use_inputFilter=use_inputFilter,
        each final riseTime=riseTime,
        each final init=init,
        each final y_start=y_start)
                             annotation (Placement(transformation(
            extent={{-10,-10},{10,10}},
            rotation=270,
            origin={0,42})));
      parameter Modelica.Units.SI.PressureDifference dpFullOpen_nominal=
          transferDataBaseDefinition.dp_nominal[1]
        "Pressure difference at which valve is fully open";
      parameter Real perPreRelValOpens=0.99 "Percentage of nominal pressure difference at which the pressure relief valve starts to open" annotation(Dialog(enable=use_preRelVal));
      parameter Boolean from_dpBranches=false
        "= true, use m_flow = f(dp) else dp = f(m_flow)" annotation (Dialog(tab="Advanced"));
      parameter Boolean linearized=false
      "= true, use linear relation between m_flow and dp for any flow rate"  annotation (Dialog(tab="Advanced"));
      parameter Real deltaMValve=0.02
      "Fraction of nominal flow rate where linearization starts, if y=1" annotation (Dialog(tab="Advanced", group="Transition to laminar"));
      parameter Modelica.Units.SI.Time riseTime=120
        "Rise time of the filter (time to reach 99.6 % of an opening step)";
      parameter Modelica.Blocks.Types.Init init=Modelica.Blocks.Types.Init.InitialOutput
        "Type of initialization (no init/steady state/initial state/initial output)";
      parameter Real y_start=1 "Initial value of output";
      parameter Real R=50 "Rangeability, R=50...100 typically";
      parameter Real delta0=0.01
        "Range of significant deviation from equal percentage law";
      parameter Boolean use_inputFilter=true
        "= true, if opening is filtered with a 2nd order CriticalDamping filter";
      parameter Modelica.Units.SI.MassFlowRate m_flow_small(min=0) = 1E-4*abs(
        sum(m_flow_nominal))
         "Small mass flow rate for regularization of zero flow" annotation(Dialog(tab="Advanced"));

      replaceable parameter BESMod.Systems.Hydraulical.Transfer.RecordsCollection.SteelRadiatorStandardPressureLossData
        transferDataBaseDefinition constrainedby
        BESMod.Systems.Hydraulical.Transfer.RecordsCollection.TransferDataBaseDefinition(
        final Q_flow_nominal=Q_flow_nominal .* f_design,
        final nZones=nParallelDem,
        final AFloor=ABui,
        final heiBui=hBui,
        final mRad_flow_nominal=m_flow_nominal,
        final mHeaCir_flow_nominal=mSup_flow_nominal[1])
        annotation (choicesAllMatching=true, Placement(transformation(extent={{58,72},
                {78,92}})));
      BESMod.Utilities.KPIs.EnergyKPICalculator
                                         integralKPICalculator[nParallelDem]
        annotation (Placement(transformation(extent={{-46,-112},{-26,-92}})));
      Modelica.Blocks.Routing.RealPassThrough reaPasThrOpe[nParallelDem]
        "Opening KPI"
        annotation (Placement(transformation(extent={{-10,-10},{10,10}},
            rotation=180,
            origin={-138,82})));
    protected
      parameter
        BESMod.Systems.Hydraulical.Components.UFH.ActiveWallBaseDataDefinition
        floorHeatingType[nParallelDem]={
          BESMod.Systems.Hydraulical.Components.UFH.ActiveWallBaseDataDefinition(
          Temp_nom=Modelica.Units.Conversions.from_degC({TTra_nominal[i],
            TTra_nominal[i] - dTTra_nominal[i],TDem_nominal[i]}),
          q_dot_nom=Q_flow_nominal[i]/UFHParameters.area[i],
          k_isolation=UFHParameters.k_top[i] + UFHParameters.k_down[i],
          k_top=UFHParameters.k_top[i],
          k_down=UFHParameters.k_down[i],
          VolumeWaterPerMeter=0,
          eps=0.9,
          C_ActivatedElement=UFHParameters.C_ActivatedElement[i],
          c_top_ratio=UFHParameters.c_top_ratio[i],
          PressureDropExponent=0,
          PressureDropCoefficient=0,
          diameter=UFHParameters.diameter) for i in 1:nParallelDem};

    equation

      for i in 1:nParallelDem loop
      if UFHParameters.is_groundFloor[i] then
       connect(fixedHeatFlow[i].port, heatCapacitor[i].port) annotation (Line(points=
             {{-82,-20},{-76,-20},{-76,-8},{-114,-8}}, color={191,0,0}));
       connect(fixedTemperature[i].port, heatFlowSensor[i].port_a) annotation (Line(
          points={{-80,10},{-64,10},{-64,-6},{-56,-6}},
          color={191,0,0},
          pattern=LinePattern.Dash));
      else
       connect(fixedHeatFlow[i].port, heatFlowSensor[i].port_a) annotation (Line(
          points={{-82,-20},{-66,-20},{-66,-6},{-56,-6}},
          color={191,0,0},
          pattern=LinePattern.Dash));
       connect(fixedTemperature[i].port, heatCapacitor[i].port) annotation (Line(
          points={{-80,10},{-80,2},{-70,2},{-70,-8},{-114,-8}},
          color={191,0,0},
          pattern=LinePattern.Dash));
      end if;
      end for;

      connect(panelHeating.thermConv, heatPortCon) annotation (Line(points={{16.6667,
              -7.22},{52,-7.22},{52,42},{100,42},{100,40}}, color={191,0,0}));
      connect(panelHeating.starRad, heatPortRad) annotation (Line(points={{16,-1.24},
              {40,-1.24},{40,-40},{100,-40}}, color={0,0,0}));

      connect(heatFlowSensor.port_b, panelHeating.ThermDown) annotation (Line(
            points={{-36,-6},{-22,-6},{-22,-5.84},{-6,-5.84}}, color={191,0,0}));

      connect(zeroLoad.internalElectricalPin, internalElectricalPin) annotation (
          Line(
          points={{52,-98},{72,-98}},
          color={0,0,0},
          thickness=1));
      connect(portTra_in[1], pressureReliefValveOnOff.port_a) annotation (Line(
            points={{-100,38},{-86,38},{-86,30},{-120,30},{-120,16},{-142,16}},
            color={0,127,255}));
      connect(pressureReliefValveOnOff.port_b, portTra_out[1]) annotation (Line(
            points={{-142,-4},{-124,-4},{-124,-42},{-100,-42}}, color={0,127,255}));
      connect(panelHeating.port_a, val.port_b) annotation (Line(points={{3.33333,19},
              {3.33333,24},{-1.83187e-15,24},{-1.83187e-15,32}}, color={0,127,255}));
      connect(val.port_a, res1.port_b) annotation (Line(points={{1.77636e-15,52},{1.77636e-15,
              70},{-14,70},{-14,88}},                            color={0,127,255}));
      connect(portTra_out, panelHeating.port_b) annotation (Line(points={{-100,
              -42},{3.33333,-42},{3.33333,-27}}, color={0,127,255}));
      connect(portTra_in, res1.port_a) annotation (Line(points={{-100,38},{-64,38},
              {-64,82},{-58,82},{-58,88},{-35,88}}, color={0,127,255}));
      connect(val.y, traControlBus.opening) annotation (Line(points={{12,42},{18,42},
              {18,84},{0,84},{0,100}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(heatFlowSensor.Q_flow, integralKPICalculator.u) annotation (Line(
            points={{-46,-17},{-58,-17},{-58,-102},{-47.8,-102}}, color={0,0,
              127}));
      connect(integralKPICalculator.KPI, outBusTra.QUFH_flow) annotation (Line(
            points={{-23.8,-102},{-14,-102},{-14,-90},{0,-90},{0,-104}}, color=
              {135,135,135}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
      connect(reaPasThrOpe.u, traControlBus.opening) annotation (Line(points={{-126,82},
              {-80,82},{-80,100},{0,100}},          color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
      connect(reaPasThrOpe.y, outBusTra.opening) annotation (Line(points={{-149,
              82},{-170,82},{-170,80},{-184,80},{-184,-80},{-2,-80},{-2,-104},{
              0,-104}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
    end UFHTransferSystem_Ueberstroemventil;

    partial model PartialHydraulicControl_FMU
      "MA Daniel PartialModelHydraulicControl"
      extends
        MA_Daniel.AdaptedModels.Controls.SystemWithoutThermostaticValveControl_MA_Daniel(
          redeclare
          BESMod.Systems.Hydraulical.Control.RecordsCollection.ThermostaticValveDataDefinition
          thermostaticValveParameters,
         redeclare
          BESMod.Systems.Hydraulical.Control.Components.ThermostaticValveController.ThermostaticValvePIControlled
          thermostaticValveController);
      replaceable BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultSafetyControl
        safetyControl
        annotation (choicesAllMatching=true,Placement(transformation(extent={{200,30},{220,50}})));
      replaceable parameter
        BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultBivHPControl
        bivalentControlData constrainedby
        BESMod.Systems.Hydraulical.Control.RecordsCollection.BivalentHeatPumpControlDataDefinition(
        final TOda_nominal=generationParameters.TOda_nominal,
        TSup_nominal=generationParameters.TSup_nominal[1],
        TSetRoomConst=sum(transferParameters.TDem_nominal)/transferParameters.nParallelDem)
        annotation (choicesAllMatching=true, Placement(transformation(extent={{-80,-68},
                {-58,-46}})));

      AixLib.Controls.HeatPump.SafetyControls.SafetyControl securityControl(
        final minRunTime=safetyControl.minRunTime,
        final minLocTime=safetyControl.minLocTime,
        final maxRunPerHou=safetyControl.maxRunPerHou,
        final use_opeEnv=safetyControl.use_opeEnv,
        final use_opeEnvFroRec=false,
        final dataTable=AixLib.DataBase.HeatPump.EN14511.Vitocal200AWO201(
            tableUppBou=[-20,50; -10,60; 30,60; 35,55]),
        final tableUpp=safetyControl.tableUpp,
        final use_minRunTime=safetyControl.use_minRunTime,
        final use_minLocTime=safetyControl.use_minLocTime,
        final use_runPerHou=safetyControl.use_runPerHou,
        final dTHystOperEnv=safetyControl.dT_opeEnv,
        final use_deFro=false,
        final minIceFac=0,
        final use_chiller=false,
        final calcPel_deFro=0,
        final pre_n_start=safetyControl.pre_n_start_hp,
        use_antFre=false) annotation (Placement(transformation(
            extent={{-16,-17},{16,17}},
            rotation=0,
            origin={210,81})));
      Modelica.Blocks.Sources.BooleanConstant hp_mode(final k=true) annotation (
          Placement(transformation(
            extent={{-7,-7},{7,7}},
            rotation=0,
            origin={155,69})));

      Modelica.Blocks.Math.BooleanToReal booleanToReal1 "Turn Pump in heat pump on"
                                                       annotation (Placement(
            transformation(
            extent={{-10,-10},{10,10}},
            rotation=270,
            origin={-170,-54})));
      BESMod.Systems.Hydraulical.Control.Components.HeatPumpBusPassThrough heatPumpBusPassThrough
        annotation (Placement(transformation(
            extent={{-10,-10},{10,10}},
            rotation=180,
            origin={30,-4})));

      Modelica.Blocks.Sources.BooleanExpression pumpIsOn(y=true)
        annotation (Placement(transformation(extent={{-230,-44},{-210,-24}})));
      BESMod.Systems.Hydraulical.Control.Components.HeatPumpNSetController.PI_InverterHeatPumpController
        pI_InverterHeatPumpController(nMin=0)
        annotation (Placement(transformation(extent={{60,70},{80,90}})));
    equation

      connect(securityControl.modeSet, hp_mode.y) annotation (Line(points={{191.867,
              77.6},{168,77.6},{168,69},{162.7,69}}, color={255,0,255}));

      connect(heatPumpBusPassThrough.sigBusGen, sigBusGen) annotation (Line(
          points={{40,-4},{52,-4},{52,12},{-152,12},{-152,-99}},
          color={255,204,51},
          thickness=0.5), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
      connect(booleanToReal1.y, sigBusGen.uPump) annotation (Line(points={{-170,-65},
              {-170,-76},{-106,-76},{-106,-99},{-152,-99}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{-3,-6},{-3,-6}},
          horizontalAlignment=TextAlignment.Right));
      connect(heatPumpBusPassThrough.vapourCompressionMachineControlBus,
        securityControl.sigBusHP) annotation (Line(
          points={{19.8,-3.8},{8,-3.8},{8,-22},{144,-22},{144,4},{192,4},{192,
              69.27}},
          color={255,204,51},
          thickness=0.5));
      connect(pI_InverterHeatPumpController.T_Meas, sigBusGen.THeaPumOut)
        annotation (Line(points={{70,68},{70,-34},{-152,-34},{-152,-99}}, color={0,0,
              127}), Text(
          string="%second",
          index=1,
          extent={{-3,-6},{-3,-6}},
          horizontalAlignment=TextAlignment.Right));
      connect(pumpIsOn.y, booleanToReal1.u) annotation (Line(points={{-209,-34},
              {-186,-34},{-186,-32},{-170,-32},{-170,-42}}, color={255,0,255}));
      annotation (Diagram(graphics={
            Rectangle(
              extent={{0,100},{132,52}},
              lineColor={28,108,200},
              lineThickness=1),
            Text(
              extent={{4,122},{108,102}},
              lineColor={28,108,200},
              lineThickness=1,
              textString="Heat Pump Control"),
            Rectangle(
              extent={{138,100},{240,52}},
              lineColor={28,108,200},
              lineThickness=1),
            Text(
              extent={{138,122},{242,102}},
              lineColor={28,108,200},
              lineThickness=1,
              textString="Heat Pump Safety")}));
    end PartialHydraulicControl_FMU;

    model Profiles_MPC "Profiles MPC"
      extends BESMod.Systems.UserProfiles.BaseClasses.PartialUserProfiles(
          TSetZone_nominal(displayUnit="K") = fill(293.15, nZones));
      parameter String fileNameIntGains=Modelica.Utilities.Files.loadResource(
          "modelica://BESMod/Resources/UserProfilesHeatingPeriod.txt")
        "File where matrix is stored";
      parameter Real gain[3]=fill(1, 3) "Gain value multiplied with internal gains. Used to e.g. disable single gains.";

      Modelica.Blocks.Sources.CombiTimeTable tabIntGai(
        final tableOnFile=true,
        smoothness=Modelica.Blocks.Types.Smoothness.ConstantSegments,
        final extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
        final tableName="Internals",
        final fileName=fileNameIntGains,
        columns=2:4) "Profiles for internal gains" annotation (Placement(
            transformation(
            extent={{10,10},{-10,-10}},
            rotation=180,
            origin={-10,30})));

      Modelica.Blocks.Math.Gain gainIntGai[3](k=gain) "Gain for internal gains"
        annotation (Placement(transformation(
            extent={{10,10},{-10,-10}},
            rotation=180,
            origin={30,30})));

      Modelica.Blocks.Sources.Constant conTSetZone[nZones](k=TSetZone_nominal)
        "Constant room set temperature" annotation (Placement(transformation(
            extent={{10,10},{-10,-10}},
            rotation=180,
            origin={-26,-30})));
      Modelica.Blocks.Logical.Switch constOrNot
        annotation (Placement(transformation(extent={{30,-80},{50,-60}})));
      Modelica.Blocks.Sources.BooleanExpression constSet(y=false)
        annotation (Placement(transformation(extent={{-52,-74},{-32,-54}})));
      Modelica.Blocks.Sources.CombiTimeTable variableSet(
        tableOnFile=true,
        tableName="Internals",
        fileName=ModelicaServices.ExternalReferences.loadResource(
            "modelica://BESMod/Resources/VariableSetTemperature.txt"),
        columns={2},
        smoothness=Modelica.Blocks.Types.Smoothness.ConstantSegments,
        extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
        timeScale=1)
        annotation (Placement(transformation(extent={{-56,-106},{-36,-86}})));
    equation
      connect(tabIntGai.y, gainIntGai.u)
        annotation (Line(points={{1,30},{18,30}}, color={0,0,127}));
      connect(gainIntGai.y, useProBus.intGains) annotation (Line(points={{41,30},{
              74,30},{74,-1},{115,-1}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
      connect(conTSetZone[1].y, constOrNot.u1) annotation (Line(points={{-15,
              -30},{20,-30},{20,-62},{28,-62}}, color={0,0,127}));
      connect(constOrNot.y, useProBus.TZoneSet[1]) annotation (Line(points={{51,
              -70},{115,-70},{115,-1}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
      connect(constSet.y, constOrNot.u2) annotation (Line(points={{-31,-64},{-6,
              -64},{-6,-70},{28,-70}}, color={255,0,255}));
      connect(variableSet.y[1], constOrNot.u3) annotation (Line(points={{-35,
              -96},{20,-96},{20,-78},{28,-78}}, color={0,0,127}));
    end Profiles_MPC;

    model Profiles_HC "Profiles HC"
      extends BESMod.Systems.UserProfiles.BaseClasses.PartialUserProfiles(
          TSetZone_nominal(displayUnit="K") = fill(293.15, nZones));
      parameter String fileNameIntGains=Modelica.Utilities.Files.loadResource(
          "modelica://BESMod/Resources/UserProfilesHeatingPeriod.txt")
        "File where matrix is stored";
      parameter Real gain[3]=fill(1, 3) "Gain value multiplied with internal gains. Used to e.g. disable single gains.";

      Modelica.Blocks.Sources.CombiTimeTable tabIntGai(
        final tableOnFile=true,
        smoothness=Modelica.Blocks.Types.Smoothness.ConstantSegments,
        final extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
        final tableName="Internals",
        final fileName=fileNameIntGains,
        columns=2:4) "Profiles for internal gains" annotation (Placement(
            transformation(
            extent={{10,10},{-10,-10}},
            rotation=180,
            origin={-8,34})));

      Modelica.Blocks.Math.Gain gainIntGai[3](k=gain) "Gain for internal gains"
        annotation (Placement(transformation(
            extent={{10,10},{-10,-10}},
            rotation=180,
            origin={30,30})));

      Modelica.Blocks.Sources.Constant conTSetZone[nZones](k=TSetZone_nominal)
        "Constant room set temperature" annotation (Placement(transformation(
            extent={{10,10},{-10,-10}},
            rotation=180,
            origin={-26,-30})));
      Modelica.Blocks.Logical.Switch constOrNot
        annotation (Placement(transformation(extent={{30,-80},{50,-60}})));
      Modelica.Blocks.Sources.BooleanExpression constSet(y=false)
        annotation (Placement(transformation(extent={{-52,-74},{-32,-54}})));
      Modelica.Blocks.Sources.CombiTimeTable variableSet(
        tableOnFile=true,
        tableName="Internals",
        fileName=ModelicaServices.ExternalReferences.loadResource(
            "modelica://BESMod/Resources/VariableSetTemperature_StandardTime_HC.txt"),
        columns={2},
        smoothness=Modelica.Blocks.Types.Smoothness.ConstantSegments,
        extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
        timeScale=1)
        annotation (Placement(transformation(extent={{-56,-106},{-36,-86}})));
    equation
      connect(tabIntGai.y, gainIntGai.u)
        annotation (Line(points={{3,34},{10,34},{10,30},{18,30}},
                                                  color={0,0,127}));
      connect(gainIntGai.y, useProBus.intGains) annotation (Line(points={{41,30},{
              74,30},{74,-1},{115,-1}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
      connect(conTSetZone[1].y, constOrNot.u1) annotation (Line(points={{-15,
              -30},{20,-30},{20,-62},{28,-62}}, color={0,0,127}));
      connect(constOrNot.y, useProBus.TZoneSet[1]) annotation (Line(points={{51,
              -70},{115,-70},{115,-1}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
      connect(constSet.y, constOrNot.u2) annotation (Line(points={{-31,-64},{-6,
              -64},{-6,-70},{28,-70}}, color={255,0,255}));
      connect(variableSet.y[1], constOrNot.u3) annotation (Line(points={{-35,
              -96},{20,-96},{20,-78},{28,-78}}, color={0,0,127}));
    end Profiles_HC;

    model TEASERProfiles "Standard TEASER Profiles Flo"
      extends BESMod.Systems.UserProfiles.BaseClasses.PartialUserProfiles;
      parameter String fileNameIntGains=Modelica.Utilities.Files.loadResource("modelica://BESMod/Resources/InternalGains.txt")
        "File where matrix is stored";
      parameter Real gain[3]=fill(1, 3) "Gain value multiplied with internal gains. Used to e.g. disable single gains.";

      Modelica.Blocks.Sources.CombiTimeTable tabIntGai(
        final tableOnFile=true,
        smoothness=Modelica.Blocks.Types.Smoothness.ConstantSegments,
        final extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
        final tableName="Internals",
        final fileName=fileNameIntGains,
        columns=2:4) "Profiles for internal gains" annotation (Placement(
            transformation(
            extent={{10,10},{-10,-10}},
            rotation=180,
            origin={-12,30})));

      Modelica.Blocks.Math.Gain gainIntGai[3](k=gain) "Gain for internal gains"
        annotation (Placement(transformation(
            extent={{10,10},{-10,-10}},
            rotation=180,
            origin={30,30})));

      Modelica.Blocks.Sources.Constant conTSetZone[nZones](k=TSetZone_nominal)
        "Constant room set temperature" annotation (Placement(transformation(
            extent={{10,10},{-10,-10}},
            rotation=180,
            origin={-10,-50})));
    equation
      connect(tabIntGai.y, gainIntGai.u)
        annotation (Line(points={{-1,30},{18,30}},color={0,0,127}));
      connect(gainIntGai.y, useProBus.intGains) annotation (Line(points={{41,30},{
              74,30},{74,-1},{115,-1}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
      connect(conTSetZone.y, useProBus.TZoneSet) annotation (Line(points={{1,-50},{
              115,-50},{115,-1}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
    end TEASERProfiles;

    model HeatingCurveDaniel "Heating Cruve in MA Daniel"

      parameter Real n=1.8 "Exponent heat curve";

      parameter Modelica.Units.SI.Temperature THeaThres=273.15 + 15
        "Constant heating threshold temperature";

      parameter Modelica.Units.SI.Temperature TVLnom=273.15 + 40
        "Nominelle Vorlauftemperatur";

      Modelica.Blocks.Interfaces.RealInput TOda
        annotation (Placement(transformation(extent={{-140,-20},{-100,20}})));
      Modelica.Blocks.Interfaces.RealOutput TSet
        annotation (Placement(transformation(extent={{100,-10},{120,10}})));

      Modelica.Blocks.Interfaces.RealInput TRaumnom annotation (Placement(
            transformation(
            extent={{-20,-20},{20,20}},
            rotation=270,
            origin={0,120})));

      parameter Modelica.Units.SI.Temperature TOdanom=273.15 - 12
        "Nominelle Außentemperatur";
    equation
      if TOda < THeaThres then
        TSet = (TVLnom-TRaumnom)*((TRaumnom-TOda)/(TRaumnom-TOdanom))^(1/n)+TRaumnom;
      else
        // No heating required.
        TSet = TRaumnom;
      end if;
      annotation (Icon(graphics={Rectangle(
              extent={{-100,100},{100,-100}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillColor={255,255,255},
              fillPattern=FillPattern.Solid),
                                 Text(
              extent={{-100,230},{100,30}},
              lineColor={0,0,0},
              textString="%name")}));
    end HeatingCurveDaniel;

  end AdaptedModels;

  package NotUsedAnymore
    extends Modelica.Icons.InternalPackage;
    model ConstHys_PI_ConOut_HPSController_wo_DHW
      "Hys + PI with condenser outlet control without DHW"
      extends MA_Daniel.NotUsedAnymore.PartialTwoPoint_HPS_Controller_wo_DHW(
                                                              redeclare
          BESMod.Systems.Hydraulical.Control.Components.HeatPumpNSetController.PI_InverterHeatPumpController
          HP_nSet_Controller(
          P=bivalentControlData.k,
          nMin=bivalentControlData.nMin,
          T_I=bivalentControlData.T_I), redeclare
          BESMod.Systems.Hydraulical.Control.Components.OnOffController.ConstantHysteresisTimeBasedHR
          BufferOnOffController(
          Hysteresis=bivalentControlData.dTHysBui,
          dt_hr=bivalentControlData.dtHeaRodBui,
          addSet_dt_hr=bivalentControlData.addSet_dtHeaRodBui));

    equation
      connect(HP_nSet_Controller.T_Meas, sigBusGen.THeaPumOut) annotation (
          Line(points={{97,61.2},{97,-64},{-152,-64},{-152,-99}},  color={0,0,127}),
          Text(
          string="%second",
          index=1,
          extent={{-3,-6},{-3,-6}},
          horizontalAlignment=TextAlignment.Right));
    end ConstHys_PI_ConOut_HPSController_wo_DHW;

    partial model PartialTwoPoint_HPS_Controller_wo_DHW
      "Partial model with replaceable blocks for rule based control of HPS using on off heating rods"
      extends
        BESMod.Systems.Hydraulical.Control.BaseClasses.SystemWithThermostaticValveControl;
      replaceable
        BESMod.Systems.Hydraulical.Control.Components.OnOffController.BaseClasses.PartialOnOffController
        BufferOnOffController annotation (choicesAllMatching=true, Placement(
            transformation(extent={{-126,36},{-110,50}})));
      replaceable BESMod.Systems.Hydraulical.Control.RecordsCollection.HeatPumpSafetyControl
        safetyControl
        annotation (choicesAllMatching=true,Placement(transformation(extent={{200,30},{220,50}})));
      replaceable parameter
        BESMod.Systems.Hydraulical.Control.RecordsCollection.BivalentHeatPumpControlDataDefinition
        bivalentControlData constrainedby
        BESMod.Systems.Hydraulical.Control.RecordsCollection.BivalentHeatPumpControlDataDefinition(
        final TOda_nominal=generationParameters.TOda_nominal,
        TSup_nominal=generationParameters.TSup_nominal[1],
        TSetRoomConst=sum(transferParameters.TDem_nominal)/transferParameters.nParallelDem)
        annotation (choicesAllMatching=true, Placement(transformation(extent={{-92,
                -32},{-70,-10}})));
      BESMod.Systems.Hydraulical.Control.Components.HeatingCurve
        heatingCurve(
        GraHeaCurve=bivalentControlData.gradientHeatCurve,
        THeaThres=bivalentControlData.TSetRoomConst,
        dTOffSet_HC=bivalentControlData.dTOffSetHeatCurve)
        annotation (Placement(transformation(extent={{-160,20},{-140,40}})));
      replaceable
        BESMod.Systems.Hydraulical.Control.Components.HeatPumpNSetController.BaseClasses.PartialHPNSetController
        HP_nSet_Controller annotation (choicesAllMatching=true, Placement(
            transformation(extent={{82,64},{112,92}})));
      Modelica.Blocks.Sources.Constant const_dT_loading1(k=distributionParameters.dTTra_nominal[1])
                                                              annotation (Placement(
            transformation(
            extent={{4,-4},{-4,4}},
            rotation=180,
            origin={14,58})));

      Modelica.Blocks.Logical.Switch switchHR annotation (Placement(transformation(
            extent={{-5,-5},{5,5}},
            rotation=0,
            origin={43,25})));
      Modelica.Blocks.Sources.Constant constZero(final k=0) annotation (Placement(
            transformation(
            extent={{2,-2},{-2,2}},
            rotation=180,
            origin={24,10})));

      Modelica.Blocks.Math.Add add_dT_LoadingBuf
        annotation (Placement(transformation(extent={{38,54},{48,64}})));

      parameter BESMod.Utilities.SupervisoryControl.Types.SupervisoryControlType
        supCtrlTypeDHWSet=BESMod.Utilities.SupervisoryControl.Types.SupervisoryControlType.Local
        "Type of supervisory control for DHW Setpoint";
      Modelica.Blocks.Math.MinMax minMax(nu=transferParameters.nParallelDem)
        annotation (Placement(transformation(extent={{-202,32},{-182,52}})));
      Modelica.Blocks.Math.BooleanToReal booleanToReal1 "Turn Pump in heat pump on"
                                                       annotation (Placement(
            transformation(
            extent={{-10,-10},{10,10}},
            rotation=270,
            origin={-170,-54})));
      Modelica.Blocks.Logical.Or HP_or_HR_active annotation (Placement(
            transformation(
            extent={{-10,-10},{10,10}},
            rotation=270,
            origin={-170,-24})));
    equation
      connect(BufferOnOffController.T_Top, sigBusDistr.TStoBufTopMea) annotation (
          Line(points={{-126.8,47.9},{-130,47.9},{-130,48},{-132,48},{-132,-86},
              {2,-86},{2,-100},{1,-100}},
            color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(heatingCurve.TSet, BufferOnOffController.T_Set) annotation (Line(
            points={{-139,30},{-139,28},{-118,28},{-118,35.3}},
                                                        color={0,0,127}));

      connect(BufferOnOffController.T_bot, sigBusDistr.TStoBufTopMea) annotation (
          Line(points={{-126.8,39.5},{-138,39.5},{-138,-72},{-8,-72},{-8,-100},{1,
              -100}},              color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(constZero.y, switchHR.u3) annotation (Line(points={{26.2,10},{28,10},{
              28,21},{37,21}}, color={0,0,127}));
      connect(switchHR.y, sigBusGen.uHeaRod) annotation (Line(points={{48.5,25},{62,25},
              {62,-48},{-152,-48},{-152,-99}},  color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
      connect(HP_nSet_Controller.IsOn, sigBusGen.heaPumIsOn) annotation (Line(
            points={{88,61.2},{88,-58},{-152,-58},{-152,-99}}, color={255,0,255}),
          Text(
          string="%second",
          index=1,
          extent={{-3,-6},{-3,-6}},
          horizontalAlignment=TextAlignment.Right));
      connect(heatingCurve.TSet, add_dT_LoadingBuf.u1) annotation (Line(points={{-139,30},
              {-139,28},{4,28},{4,66},{32,66},{32,62},{37,62}},
                                                 color={0,0,127}));
      connect(const_dT_loading1.y, add_dT_LoadingBuf.u2) annotation (Line(points={{
              18.4,58},{26,58},{26,56},{37,56}}, color={0,0,127}));
      connect(heatingCurve.TOda, weaBus.TDryBul) annotation (Line(points={{-162,30},
              {-236,30},{-236,2},{-237,2}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(BufferOnOffController.T_oda, weaBus.TDryBul) annotation (Line(points={{-118,
              50.84},{-118,56},{-236,56},{-236,30},{-237,30},{-237,2}},       color=
             {0,0,127}), Text(
          string="%second",
          index=1,
          extent={{-3,-6},{-3,-6}},
          horizontalAlignment=TextAlignment.Right));
      connect(minMax.u, useProBus.TZoneSet) annotation (Line(points={{-202,42},{
              -208,42},{-208,62},{-119,62},{-119,103}},                color={0,0,127}),
          Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(minMax.yMax, heatingCurve.TSetRoom)
        annotation (Line(points={{-181,48},{-150,48},{-150,42}}, color={0,0,127}));
      connect(booleanToReal1.u, HP_or_HR_active.y)
        annotation (Line(points={{-170,-42},{-170,-35}}, color={255,0,255}));
      connect(booleanToReal1.y, sigBusGen.uPump) annotation (Line(points={{-170,-65},
              {-172,-65},{-172,-72},{-152,-72},{-152,-99}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
      connect(HP_or_HR_active.u2, sigBusGen.heaPumIsOn) annotation (Line(
            points={{-178,-12},{-194,-12},{-194,-99},{-152,-99}}, color={255,0,255}),
          Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(BufferOnOffController.HP_On, HP_nSet_Controller.HP_On)
        annotation (Line(points={{-108.88,47.9},{-34,47.9},{-34,92},{74,92},{74,
              78},{79,78}}, color={255,0,255}));
      connect(add_dT_LoadingBuf.y, HP_nSet_Controller.T_Set) annotation (Line(
            points={{48.5,59},{62,59},{62,86.4},{79,86.4}}, color={0,0,127}));
      connect(switchHR.u1, BufferOnOffController.Auxilliar_Heater_set)
        annotation (Line(points={{37,29},{24,29},{24,36.98},{-108.88,36.98}},
            color={0,0,127}));
      connect(BufferOnOffController.Auxilliar_Heater_On, switchHR.u2)
        annotation (Line(points={{-108.88,39.5},{-108.88,26},{-38,26},{-38,25},
              {37,25}}, color={255,0,255}));
      connect(HP_or_HR_active.u1, switchHR.u2) annotation (Line(points={{-170,
              -12},{-18,-12},{-18,25},{37,25}}, color={255,0,255}));
      connect(HP_nSet_Controller.n_Set, sigBusGen.yHeaPumSet) annotation (Line(
            points={{113.5,78},{278,78},{278,-132},{-124,-132},{-124,-99},{-152,
              -99}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
      annotation (Diagram(graphics={
            Rectangle(
              extent={{-240,58},{-50,14}},
              lineColor={0,140,72},
              lineThickness=1),
            Text(
              extent={{-216,-14},{-122,20}},
              lineColor={0,140,72},
              lineThickness=1,
              textString="Buffer Control"),
            Rectangle(
              extent={{0,100},{132,52}},
              lineColor={28,108,200},
              lineThickness=1),
            Text(
              extent={{4,122},{108,102}},
              lineColor={28,108,200},
              lineThickness=1,
              textString="Heat Pump Control"),
            Rectangle(
              extent={{0,46},{132,4}},
              lineColor={162,29,33},
              lineThickness=1),
            Text(
              extent={{2,4},{106,-16}},
              lineColor={162,29,33},
              lineThickness=1,
              textString="Heating Rod Control")}));
    end PartialTwoPoint_HPS_Controller_wo_DHW;

    model WPmitFBH2
      extends WPmitFBH_adapted(redeclare
          BESMod.Systems.Hydraulical.HydraulicSystem hydraulic(
          redeclare
            BESMod.Systems.Hydraulical.Interfaces.Generation.HeatPumpAndHeatingRod
            generation(
            use_heaRod=false,
            redeclare model PerDataMainHP =
                AixLib.DataBase.HeatPump.PerformanceData.LookUpTable2D,
            redeclare package Medium_eva = IBPSA.Media.Air,
            redeclare
              BESMod.Systems.Hydraulical.Interfaces.Generation.RecordsCollection.DefaultHP
              heatPumpParameters(genDesTyp=BESMod.Systems.Hydraulical.Interfaces.Generation.Types.GenerationDesign.Monovalent),
            redeclare
              BESMod.Systems.Hydraulical.Interfaces.Generation.RecordsCollection.DefaultHR
              heatingRodParameters,
            redeclare BESMod.Systems.RecordsCollection.Movers.DefaultMover
              pumpData,
            redeclare
              BESMod.Systems.RecordsCollection.TemperatureSensors.DefaultSensor
              temperatureSensorData),
          redeclare
            BESMod.Systems.Hydraulical.Control.ConstHys_PI_ConOut_HPSController
            control(
            redeclare
              BESMod.Systems.Hydraulical.Control.Components.ThermostaticValveController.ThermostaticValvePIControlled
              thermostaticValveController,
            redeclare
              BESMod.Systems.Hydraulical.Control.RecordsCollection.ThermostaticValveDataDefinition
              thermostaticValveParameters,
            redeclare
              BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultSafetyControl
              safetyControl,
            redeclare
              BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultBivHPControl
              bivalentControlData,
            redeclare
              BESMod.Systems.Hydraulical.Control.Components.DHWSetControl.ConstTSet_DHW
              TSet_DHW,
            supCtrlTypeDHWSet=BESMod.Utilities.SupervisoryControl.Types.SupervisoryControlType.Local),
          redeclare BESMod.Systems.Hydraulical.Distribution.BuildingOnly
            distribution(nParallelDem=1),
          redeclare BESMod.Systems.Hydraulical.Transfer.UFHTransferSystem
            transfer(redeclare
              BESMod.Systems.Hydraulical.Transfer.RecordsCollection.DefaultUFHData
              UFHParameters, redeclare
              BESMod.Systems.RecordsCollection.Movers.DefaultMover pumpData)));

    end WPmitFBH2;

    model Local_Hydraulic "Local control of hydraulic system"
      extends BESMod.Systems.Hydraulical.Control.BaseClasses.PartialControl;
    end Local_Hydraulic;

    partial model PartialHydraulicControl
      "PartialModelHydraulicControl MA Daniel"
      extends
        BESMod.Systems.Hydraulical.Control.BaseClasses.SystemWithThermostaticValveControl;
      replaceable BESMod.Systems.Hydraulical.Control.RecordsCollection.HeatPumpSafetyControl
        safetyControl
        annotation (choicesAllMatching=true,Placement(transformation(extent={{200,30},{220,50}})));
      replaceable parameter
        BESMod.Systems.Hydraulical.Control.RecordsCollection.BivalentHeatPumpControlDataDefinition
        bivalentControlData constrainedby
        BESMod.Systems.Hydraulical.Control.RecordsCollection.BivalentHeatPumpControlDataDefinition(
        final TOda_nominal=generationParameters.TOda_nominal,
        TSup_nominal=generationParameters.TSup_nominal[1],
        TSetRoomConst=sum(transferParameters.TDem_nominal)/transferParameters.nParallelDem)
        annotation (choicesAllMatching=true, Placement(transformation(extent={{-82,-48},
                {-60,-26}})));
      BESMod.Systems.Hydraulical.Control.Components.HeatingCurve
        heatingCurve(
        GraHeaCurve=bivalentControlData.gradientHeatCurve,
        THeaThres=bivalentControlData.TSetRoomConst,
        dTOffSet_HC=bivalentControlData.dTOffSetHeatCurve)
        annotation (Placement(transformation(extent={{-160,20},{-140,40}})));
      replaceable
        BESMod.Systems.Hydraulical.Control.Components.HeatPumpNSetController.BaseClasses.PartialHPNSetController
        HP_nSet_Controller annotation (choicesAllMatching=true, Placement(
            transformation(extent={{82,64},{112,92}})));

      AixLib.Controls.HeatPump.SafetyControls.SafetyControl securityControl(
        final minRunTime=safetyControl.minRunTime,
        final minLocTime=safetyControl.minLocTime,
        final maxRunPerHou=safetyControl.maxRunPerHou,
        final use_opeEnv=safetyControl.use_opeEnv,
        final use_opeEnvFroRec=false,
        final dataTable=AixLib.DataBase.HeatPump.EN14511.Vitocal200AWO201(
            tableUppBou=[-20,50; -10,60; 30,60; 35,55]),
        final tableUpp=safetyControl.tableUpp,
        final use_minRunTime=safetyControl.use_minRunTime,
        final use_minLocTime=safetyControl.use_minLocTime,
        final use_runPerHou=safetyControl.use_runPerHou,
        final dTHystOperEnv=safetyControl.dT_opeEnv,
        final use_deFro=false,
        final minIceFac=0,
        final use_chiller=false,
        final calcPel_deFro=0,
        final pre_n_start=safetyControl.pre_n_start_hp,
        use_antFre=false) annotation (Placement(transformation(
            extent={{-16,-17},{16,17}},
            rotation=0,
            origin={210,81})));
      Modelica.Blocks.Sources.BooleanConstant hp_mode(final k=true) annotation (
          Placement(transformation(
            extent={{-7,-7},{7,7}},
            rotation=0,
            origin={155,69})));

      Modelica.Blocks.Math.BooleanToReal booleanToReal1 "Turn Pump in heat pump on"
                                                       annotation (Placement(
            transformation(
            extent={{-10,-10},{10,10}},
            rotation=270,
            origin={-170,-54})));
      BESMod.Systems.Hydraulical.Control.Components.HeatPumpBusPassThrough heatPumpBusPassThrough
        annotation (Placement(transformation(
            extent={{-10,-10},{10,10}},
            rotation=180,
            origin={30,-4})));
      AdaptedModels.HeatPumpHysteresis heatPumpHysteresis(bandwidth=bandwidth)
        annotation (Placement(transformation(extent={{-104,46},{-84,66}})));
      Modelica.Blocks.Math.MinMax minMax(nu=transferParameters.nParallelDem)
        annotation (Placement(transformation(extent={{-196,34},{-176,54}})));
      parameter Real bandwidth=0.1 "Bandwidth around reference signal";
    equation

      connect(securityControl.modeSet, hp_mode.y) annotation (Line(points={{191.867,
              77.6},{168,77.6},{168,69},{162.7,69}}, color={255,0,255}));
      connect(HP_nSet_Controller.n_Set, securityControl.nSet) annotation (Line(
            points={{113.5,78},{144,78},{144,84.4},{191.867,84.4}}, color={0,0,127}));

      connect(HP_nSet_Controller.IsOn, sigBusGen.heaPumIsOn) annotation (Line(
            points={{88,61.2},{88,-110},{-88,-110},{-88,-104},{-152,-104},{-152,-99}},
                                                               color={255,0,255}),
          Text(
          string="%second",
          index=1,
          extent={{-3,-6},{-3,-6}},
          horizontalAlignment=TextAlignment.Right));
      connect(heatingCurve.TOda, weaBus.TDryBul) annotation (Line(points={{-162,30},
              {-236,30},{-236,2},{-237,2}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(minMax.u, useProBus.TZoneSet) annotation (Line(points={{-196,44},
              {-204,44},{-204,74},{-119,74},{-119,103}},               color={0,0,127}),
          Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(minMax.yMax, heatingCurve.TSetRoom)
        annotation (Line(points={{-175,50},{-150,50},{-150,42}}, color={0,0,127}));
      connect(heatPumpBusPassThrough.sigBusGen, sigBusGen) annotation (Line(
          points={{40,-4},{52,-4},{52,12},{-152,12},{-152,-99}},
          color={255,204,51},
          thickness=0.5), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
      connect(heatPumpBusPassThrough.vapourCompressionMachineControlBus,
        securityControl.sigBusHP) annotation (Line(
          points={{19.8,-3.8},{18,-3.8},{18,-16},{184,-16},{184,69.27},{192,69.27}},
          color={255,204,51},
          thickness=0.5));
      connect(booleanToReal1.u, sigBusGen.heaPumIsOn) annotation (Line(points={{-170,
              -42},{-170,-18},{-270,-18},{-270,-138},{-152,-138},{-152,-99}}, color=
             {255,0,255}), Text(
          string="%second",
          index=1,
          extent={{-3,6},{-3,6}},
          horizontalAlignment=TextAlignment.Right));
      connect(heatPumpHysteresis.y, HP_nSet_Controller.HP_On) annotation (Line(
            points={{-83,56},{70,56},{70,78},{79,78}},   color={255,0,255}));
      connect(heatPumpHysteresis.T_sup, sigBusGen.THeaPumOut) annotation (Line(
            points={{-106,48},{-114,48},{-114,14},{-152,14},{-152,-99}},
                                                                color={0,0,127}),
          Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(securityControl.nOut, sigBusGen.yHeaPumSet) annotation (Line(points={{227.333,
              84.4},{282,84.4},{282,-130},{-152,-130},{-152,-99}},         color={0,
              0,127}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
      connect(booleanToReal1.y, sigBusGen.uPump) annotation (Line(points={{-170,-65},
              {-170,-76},{-106,-76},{-106,-99},{-152,-99}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{-3,-6},{-3,-6}},
          horizontalAlignment=TextAlignment.Right));
      connect(heatingCurve.TSet, HP_nSet_Controller.T_Set) annotation (Line(
            points={{-139,30},{36,30},{36,86.4},{79,86.4}}, color={0,0,127}));
      connect(heatPumpHysteresis.T_set, heatingCurve.TSet) annotation (Line(
            points={{-106,64},{-112,64},{-112,66},{-128,66},{-128,30},{-139,30}},
            color={0,0,127}));
      annotation (Diagram(graphics={
            Rectangle(
              extent={{-240,58},{-50,14}},
              lineColor={0,140,72},
              lineThickness=1),
            Rectangle(
              extent={{0,100},{132,52}},
              lineColor={28,108,200},
              lineThickness=1),
            Text(
              extent={{4,122},{108,102}},
              lineColor={28,108,200},
              lineThickness=1,
              textString="Heat Pump Control"),
            Rectangle(
              extent={{138,100},{240,52}},
              lineColor={28,108,200},
              lineThickness=1),
            Text(
              extent={{138,122},{242,102}},
              lineColor={28,108,200},
              lineThickness=1,
              textString="Heat Pump Safety")}));
    end PartialHydraulicControl;

    model HydraulicControl "HydraulicControl MA Daniel"
      extends MA_Daniel.NotUsedAnymore.PartialHydraulicControl(
        redeclare
          BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultBivHPControl
          bivalentControlData,
        redeclare
          BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultSafetyControl
          safetyControl,
        redeclare
          BESMod.Systems.Hydraulical.Control.RecordsCollection.ThermostaticValveDataDefinition
          thermostaticValveParameters,
        redeclare
          BESMod.Systems.Hydraulical.Control.Components.ThermostaticValveController.ThermostaticValvePIControlled
          thermostaticValveController,
        redeclare
          BESMod.Systems.Hydraulical.Control.Components.HeatPumpNSetController.PI_InverterHeatPumpController
          HP_nSet_Controller(
          P=bivalentControlData.k,
          nMin=bivalentControlData.nMin,
          T_I=bivalentControlData.T_I));

    equation
      connect(HP_nSet_Controller.T_Meas, sigBusGen.THeaPumOut) annotation (
          Line(points={{97,61.2},{97,-64},{-152,-64},{-152,-99}},  color={0,0,127}),
          Text(
          string="%second",
          index=1,
          extent={{-3,-6},{-3,-6}},
          horizontalAlignment=TextAlignment.Right));
      annotation (Diagram(graphics={
            Text(
              extent={{-356,36},{-252,16}},
              lineColor={0,140,72},
              lineThickness=1,
              textString="Heat Pump Hysteresis Control")}));
    end HydraulicControl;

    model HeatCurve2 "MA Daniel - HeatCurve2"
      extends AdaptedModels.PartialHydraulicControl(redeclare
          BESMod.Systems.Hydraulical.Control.Components.HeatPumpNSetController.PI_InverterHeatPumpController
          HP_nSet_Controller, redeclare
          BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultBivHPControl
          bivalentControlData);
      BESMod.Systems.Hydraulical.Control.Components.HeatingCurve
        heatingCurve(
        GraHeaCurve=bivalentControlData.gradientHeatCurve,
        THeaThres=bivalentControlData.TSetRoomConst,
        dTOffSet_HC=bivalentControlData.dTOffSetHeatCurve)
        annotation (Placement(transformation(extent={{-128,28},{-108,48}})));
      AdaptedModels.HeatPumpHysteresis heatPumpHysteresis(bandwidth=0.1)
        annotation (Placement(transformation(extent={{-70,50},{-50,70}})));
      Modelica.Blocks.Logical.And and1
        annotation (Placement(transformation(extent={{-30,42},{-10,62}})));
      AdaptedModels.HeatPumpHysteresis heatPumpHysteresis1(bandwidth=1)
        annotation (Placement(transformation(extent={{-68,6},{-48,26}})));
    equation
      connect(heatingCurve.TSet, heatPumpHysteresis.T_set) annotation (Line(
            points={{-107,38},{-100,38},{-100,36},{-86,36},{-86,68},{-72,68}},
            color={0,0,127}));
      connect(heatingCurve.TOda, weaBus.TDryBul) annotation (Line(points={{
              -130,38},{-237,38},{-237,2}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(heatingCurve.TSetRoom, useProBus.TZoneSet[1]) annotation (Line(
            points={{-118,50},{-118,76.5},{-119,76.5},{-119,103}}, color={0,0,
              127}), Text(
          string="%second",
          index=1,
          extent={{-3,6},{-3,6}},
          horizontalAlignment=TextAlignment.Right));
      connect(heatPumpHysteresis.T_sup, sigBusGen.THeaPumOut) annotation (
          Line(points={{-72,52},{-76,52},{-76,54},{-100,54},{-100,-99},{-152,
              -99}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(heatingCurve.TSet, HP_nSet_Controller.T_Set) annotation (Line(
            points={{-107,38},{20,38},{20,84},{58,84},{58,86.4},{79,86.4}},
            color={0,0,127}));
      connect(and1.y, HP_nSet_Controller.HP_On) annotation (Line(points={{-9,
              52},{12,52},{12,48},{36,48},{36,78},{79,78}}, color={255,0,255}));
      connect(heatPumpHysteresis.y, and1.u1) annotation (Line(points={{-49,60},
              {-42,60},{-42,52},{-32,52}}, color={255,0,255}));
      connect(heatPumpHysteresis1.y, and1.u2) annotation (Line(points={{-47,
              16},{-36,16},{-36,44},{-32,44}}, color={255,0,255}));
      connect(heatPumpHysteresis1.T_set, useProBus.TZoneSet[1]) annotation (
          Line(points={{-70,24},{-94,24},{-94,103},{-119,103}}, color={0,0,
              127}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(heatPumpHysteresis1.T_sup, buiMeaBus.TZoneMea[1]) annotation (
          Line(points={{-70,8},{-78,8},{-78,100},{-76,100},{-76,120},{65,120},
              {65,103}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
    end HeatCurve2;

    model Controller_for_MPC
      extends MPC.Submodules.SystemWithControllableThermostaticValveControl;

      replaceable BESMod.Systems.Hydraulical.Control.RecordsCollection.HeatPumpSafetyControl
        safetyControl
        annotation (choicesAllMatching=true,Placement(transformation(extent={{206,32},
                {226,52}})));
      BESMod.Systems.Hydraulical.Control.Components.HeatingCurve
        heatingCurve(
        GraHeaCurve=bivalentControlData.gradientHeatCurve,
        THeaThres=bivalentControlData.TSetRoomConst,
        dTOffSet_HC=bivalentControlData.dTOffSetHeatCurve)
        annotation (Placement(transformation(extent={{-150,20},{-128,42}})));
      Modelica.Blocks.Sources.Constant const_dT_loading1(k=distributionParameters.dTTra_nominal[1])
                                                              annotation (Placement(
            transformation(
            extent={{4,-4},{-4,4}},
            rotation=180,
            origin={14,58})));

      Modelica.Blocks.Sources.BooleanConstant hp_mode(final k=true) annotation (
          Placement(transformation(
            extent={{-7,-7},{7,7}},
            rotation=0,
            origin={177,83})));
      Modelica.Blocks.Sources.Constant hp_iceFac(final k=1) annotation (Placement(
            transformation(
            extent={{-7,-7},{7,7}},
            rotation=0,
            origin={-199,-137})));

      Modelica.Blocks.Routing.RealPassThrough realPassThrough_T_Amb1
        "Only used to make warning disappear, has no effect on model veloccity"
        annotation (Placement(transformation(extent={{-230,-56},{-208,-34}})));

      Modelica.Blocks.Math.Add add_dT_LoadingBuf
        annotation (Placement(transformation(extent={{38,54},{48,64}})));

      parameter
        BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultBivHPControl bivalentControlData(
        final TOda_nominal=generationParameters.TOda_nominal,
        TSup_nominal=generationParameters.TSup_nominal[1],
        TSetRoomConst=sum(transferParameters.TDem_nominal)/transferParameters.nParallelDem,
        nMin=0)
        annotation (choicesAllMatching=true, Placement(transformation(extent={{
                -106,-26},{-84,-4}})));
      Modelica.Blocks.Sources.BooleanExpression HeatingRod(y=true)
        annotation (Placement(transformation(extent={{-46,-16},{-26,4}})));
      Modelica.Blocks.Math.BooleanToReal booleanToReal_HR annotation (Placement(
            transformation(
            extent={{-6,-6},{6,6}},
            rotation=270,
            origin={-22,-26})));
      MPC.Submodules.Model_inputs model_inputs
        annotation (Placement(transformation(extent={{-40,26},{-16,54}})));
      MPC.Submodules.PI_Control_for_MPC pI_Control_for_MPC
        annotation (Placement(transformation(extent={{80,64},{112,92}})));
      Modelica.Blocks.Sources.Constant const_dT_loading2(k=273.15 + 25)
                                                              annotation (Placement(
            transformation(
            extent={{4,-4},{-4,4}},
            rotation=180,
            origin={-164,74})));
      Modelica.Blocks.Math.BooleanToReal booleanToReal_u_Pump annotation (
          Placement(transformation(
            extent={{-6,-6},{6,6}},
            rotation=270,
            origin={66,-40})));
      Modelica.Blocks.Sources.BooleanExpression FMU_Input(y=true)
        annotation (Placement(transformation(extent={{-84,-48},{-64,-28}})));
      Modelica.Blocks.Sources.RealExpression valve_set(y=1)
        annotation (Placement(transformation(extent={{52,-186},{72,-166}})));
      Modelica.Blocks.Sources.BooleanExpression Pump(y=true)
        annotation (Placement(transformation(extent={{22,-30},{42,-10}})));
      MPC.Submodules.comp_input_transformer comp_input_transformer1
        annotation (Placement(transformation(extent={{92,-30},{146,2}})));
      Modelica.Blocks.Logical.Switch Valve_Switch_fmu2
        annotation (Placement(transformation(extent={{38,-98},{58,-78}})));
      Modelica.Blocks.Sources.RealExpression HP_off(y=0)
        annotation (Placement(transformation(extent={{8,-4},{28,16}})));
      Modelica.Blocks.Logical.Hysteresis hysteresis(uLow=59.9 + 273.15, uHigh=60.1
             + 273.15)
        annotation (Placement(transformation(extent={{-88,-96},{-76,-84}})));
    equation

      connect(hp_iceFac.y, sigBusGen.hp_bus.iceFacMea) annotation (Line(
            points={{-191.3,-137},{-191.3,-108},{-196,-108},{-196,-72},{-152,-72},{-152,
              -99}},      color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));

      connect(realPassThrough_T_Amb1.y, sigBusGen.hp_bus.TOdaMea) annotation (Line(
            points={{-206.9,-45},{-176,-45},{-176,-56},{-152,-56},{-152,-99}},
            color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
      connect(realPassThrough_T_Amb1.u, weaBus.TDryBul) annotation (
          Line(points={{-232.2,-45},{-238,-45},{-238,-26},{-210,-26},{-210,2},
              {-237,2}},                                                      color=
             {0,0,127}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(realPassThrough_T_Amb1.y, heatingCurve.TOda) annotation (Line(
            points={{-206.9,-45},{-160,-45},{-160,31},{-152.2,31}},
                           color={0,0,127}));
      connect(const_dT_loading1.y, add_dT_LoadingBuf.u2) annotation (Line(points={{
              18.4,58},{26,58},{26,56},{37,56}}, color={0,0,127}));
      connect(HeatingRod.y, booleanToReal_HR.u) annotation (Line(points={{-25,-6},
              {-25,-14},{-22,-14},{-22,-18.8}}, color={255,0,255}));
      connect(booleanToReal_HR.y, sigBusGen.hr_on) annotation (Line(points={{-22,
              -32.6},{-22,-52},{-152,-52},{-152,-99}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(hp_mode.y, sigBusGen.hp_bus.modeSet) annotation (Line(points={{184.7,
              83},{202,83},{202,-44},{110,-44},{110,-66},{42,-66},{42,-64},{-152,
              -64},{-152,-99}},
                     color={255,0,255}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(heatingCurve.TSet, model_inputs.T_set_HC) annotation (Line(points={{-126.9,
              31},{-52,31},{-52,27.3176},{-42.2286,27.3176}},       color={0,0,
              127}));
      connect(model_inputs.T_VL_set, add_dT_LoadingBuf.u1) annotation (Line(
            points={{-12.5714,32.9176},{-4,32.9176},{-4,66},{28,66},{28,62},{
              37,62}},
            color={0,0,127}));
      connect(add_dT_LoadingBuf.y, pI_Control_for_MPC.T_Set) annotation (Line(
            points={{48.5,59},{54,59},{54,60},{76.5333,60},{76.5333,85.28}},
            color={0,0,127}));
      connect(pI_Control_for_MPC.n_Set, model_inputs.u_comp_PID) annotation (Line(
            points={{117.6,82.48},{116,82.48},{116,72},{114,72},{114,66},{116,
              66},{116,52},{74,52},{74,38},{0,38},{0,20},{-34,20},{-34,18},{
              -46,18},{-46,30},{-42.2286,30},{-42.2286,31.9294}},
                                                          color={0,0,127}));
      connect(pI_Control_for_MPC.T_Meas, sigBusGen.hp_bus.TConOutMea) annotation (
         Line(points={{92.5333,60.08},{92.5333,20},{2,20},{2,-62},{-152,-62},
              {-152,-99}},color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(model_inputs.b_valve_pi_out, Valve_Switch.u2) annotation (Line(
            points={{-12.7429,48.7294},{-12.7429,-50},{42,-50},{42,-128},{80,
              -128},{80,-160},{174,-160}},
                                     color={255,0,255}));
      connect(heatingCurve.TSet, sigBusDistr.TFlowHeaCur) annotation (Line(points={{-126.9,
              31},{-126.9,14},{-8,14},{-8,-60},{1,-60},{1,-100}},
                         color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
      connect(const_dT_loading2.y, heatingCurve.TSetRoom) annotation (Line(points={{-159.6,
              74},{-139,74},{-139,44.2}},         color={0,0,127}));
      connect(booleanToReal_u_Pump.y, sigBusGen.uPump) annotation (Line(points={{66,
              -46.6},{66,-52},{-20,-52},{-20,-60},{-152,-60},{-152,-99}},
            color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(FMU_Input.y, model_inputs.b_FMU_input) annotation (Line(points={{-63,-38},
              {-48,-38},{-48,52.5176},{-42.0571,52.5176}},          color={255,0,
              255}));
      connect(Valve_Switch_fmu.u2, model_inputs.b_FMU_input) annotation (Line(
            points={{106,-148},{30,-148},{30,-134},{-48,-134},{-48,52.5176},{
              -42.0571,52.5176}}, color={255,0,255}));
      connect(valve_set.y, Valve_Switch_fmu.u3) annotation (Line(points={{73,-176},
              {84,-176},{84,-156},{106,-156}}, color={0,0,127}));
      connect(booleanToReal_u_Pump.u, Pump.y) annotation (Line(points={{66,
              -32.8},{54,-32.8},{54,-20},{43,-20}},
                                            color={255,0,255}));
      connect(Valve_Switch_fmu1.u2, FMU_Input.y) annotation (Line(points={{32,-120},
              {-8,-120},{-8,-38},{-63,-38},{-63,-38}}, color={255,0,255}));
      connect(comp_input_transformer1.HP_on, sigBusTra.HP_on) annotation (Line(
            points={{147.322,-7.04348},{147.322,-58},{174,-58},{174,-100},{
              174,-100}},
            color={255,0,255}), Text(
          string="%second",
          index=1,
          extent={{-3,-6},{-3,-6}},
          horizontalAlignment=TextAlignment.Right));
      connect(Valve_Switch_fmu2.u3, comp_input_transformer1.y) annotation (Line(
            points={{36,-96},{30,-96},{30,-62},{148,-62},{148,-36},{152,-36},
              {152,-11.7739},{147.873,-11.7739}},
                                             color={0,0,127}));
      connect(Valve_Switch_fmu2.u1, HP_off.y) annotation (Line(points={{36,-80},{36,
              -36},{48,-36},{48,-2},{42,-2},{42,6},{29,6}}, color={0,0,127}));
      connect(hysteresis.y, Valve_Switch_fmu2.u2) annotation (Line(points={{-75.4,
              -90},{-42,-90},{-42,-76},{30,-76},{30,-88},{36,-88}}, color={255,0,
              255}));
      connect(hysteresis.u, sigBusGen.hp_bus.TConOutMea) annotation (Line(points={{
              -89.2,-90},{-120,-90},{-120,-68},{-152,-68},{-152,-99}}, color={0,0,
              127}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(comp_input_transformer1.u, model_inputs.n_Set) annotation (Line(
            points={{88.6939,-8.43478},{82,-8.43478},{82,22},{-6,22},{-6,
              39.6706},{-12.4,39.6706}},
                               color={0,0,127}));
      connect(pI_Control_for_MPC.HP_on, comp_input_transformer1.HP_on) annotation (
          Line(points={{76.2667,67.64},{76.2667,-36},{147.322,-36},{147.322,
              -7.04348}}, color={255,0,255}));
      connect(Valve_Switch_fmu2.y, sigBusGen.hp_bus.nSet) annotation (Line(points={
              {59,-88},{64,-88},{64,-72},{26,-72},{26,-70},{-28,-70},{-28,-68},{
              -118,-68},{-118,-66},{-152,-66},{-152,-99}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      annotation (Diagram(graphics={
            Text(
              extent={{-234,94},{-140,128}},
              lineColor={238,46,47},
              lineThickness=1,
              textString="DHW Control"),
            Text(
              extent={{4,122},{108,102}},
              lineColor={28,108,200},
              lineThickness=1,
              textString="Heat Pump Control"),
            Text(
              extent={{138,122},{242,102}},
              lineColor={28,108,200},
              lineThickness=1,
              textString="Heat Pump Safety")}));
    end Controller_for_MPC;

    model HeatCurve3 "MA Daniel - HeatCurve3"
      extends AdaptedModels.PartialHydraulicControl(redeclare
          BESMod.Systems.Hydraulical.Control.Components.HeatPumpNSetController.PI_InverterHeatPumpController
          HP_nSet_Controller, redeclare
          BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultBivHPControl
          bivalentControlData);
      BESMod.Systems.Hydraulical.Control.Components.HeatingCurve
        heatingCurve(
        GraHeaCurve=bivalentControlData.gradientHeatCurve,
        THeaThres=bivalentControlData.TSetRoomConst,
        dTOffSet_HC=bivalentControlData.dTOffSetHeatCurve)
        annotation (Placement(transformation(extent={{-128,28},{-108,48}})));
      AdaptedModels.HeatPumpHysteresis heatPumpHysteresis(bandwidth=0.1)
        annotation (Placement(transformation(extent={{-70,50},{-50,70}})));
      Modelica.Blocks.Logical.And and1
        annotation (Placement(transformation(extent={{-30,42},{-10,62}})));
      AdaptedModels.HeatPumpHysteresis heatPumpHysteresis1(bandwidth=1)
        annotation (Placement(transformation(extent={{-68,6},{-48,26}})));
      Modelica.Blocks.Logical.LessEqual lessEqual
        annotation (Placement(transformation(extent={{-66,-22},{-46,-2}})));
      Modelica.Blocks.Logical.And and2
        annotation (Placement(transformation(extent={{-32,-4},{-12,16}})));
    equation
      connect(heatingCurve.TSet, heatPumpHysteresis.T_set) annotation (Line(
            points={{-107,38},{-100,38},{-100,36},{-86,36},{-86,68},{-72,68}},
            color={0,0,127}));
      connect(heatingCurve.TOda, weaBus.TDryBul) annotation (Line(points={{
              -130,38},{-237,38},{-237,2}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(heatingCurve.TSetRoom, useProBus.TZoneSet[1]) annotation (Line(
            points={{-118,50},{-118,76.5},{-119,76.5},{-119,103}}, color={0,0,
              127}), Text(
          string="%second",
          index=1,
          extent={{-3,6},{-3,6}},
          horizontalAlignment=TextAlignment.Right));
      connect(heatPumpHysteresis.T_sup, sigBusGen.THeaPumOut) annotation (
          Line(points={{-72,52},{-76,52},{-76,54},{-100,54},{-100,-99},{-152,
              -99}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(heatingCurve.TSet, HP_nSet_Controller.T_Set) annotation (Line(
            points={{-107,38},{20,38},{20,84},{58,84},{58,86.4},{79,86.4}},
            color={0,0,127}));
      connect(and1.y, HP_nSet_Controller.HP_On) annotation (Line(points={{-9,
              52},{12,52},{12,48},{36,48},{36,78},{79,78}}, color={255,0,255}));
      connect(heatPumpHysteresis.y, and1.u1) annotation (Line(points={{-49,60},
              {-42,60},{-42,52},{-32,52}}, color={255,0,255}));
      connect(heatPumpHysteresis1.T_set, useProBus.TZoneSet[1]) annotation (
          Line(points={{-70,24},{-94,24},{-94,103},{-119,103}}, color={0,0,
              127}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(heatPumpHysteresis1.T_sup, buiMeaBus.TZoneMea[1]) annotation (
          Line(points={{-70,8},{-78,8},{-78,100},{-76,100},{-76,120},{65,120},
              {65,103}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(heatPumpHysteresis1.y, and2.u1) annotation (Line(points={{-47,
              16},{-44,16},{-44,10},{-42,10},{-42,6},{-34,6}}, color={255,0,
              255}));
      connect(lessEqual.y, and2.u2) annotation (Line(points={{-45,-12},{-40,
              -12},{-40,-2},{-34,-2}}, color={255,0,255}));
      connect(and2.y, and1.u2) annotation (Line(points={{-11,6},{-8,6},{-8,32},
              {-44,32},{-44,44},{-32,44}}, color={255,0,255}));
      connect(heatPumpHysteresis1.T_sup, lessEqual.u1) annotation (Line(
            points={{-70,8},{-78,8},{-78,-8},{-76,-8},{-76,-12},{-68,-12}},
            color={0,0,127}));
      connect(heatPumpHysteresis1.T_set, lessEqual.u2) annotation (Line(
            points={{-70,24},{-94,24},{-94,-16},{-78,-16},{-78,-20},{-68,-20}},
            color={0,0,127}));
    end HeatCurve3;

    model FMU_SupTem3 "MA Daniel - FMU SupTem3"
      extends AdaptedModels.PartialHydraulicControl(redeclare
          BESMod.Systems.Hydraulical.Control.Components.HeatPumpNSetController.PI_InverterHeatPumpController
          HP_nSet_Controller, redeclare
          BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultBivHPControl
          bivalentControlData);
      AdaptedModels.HeatPumpHysteresis heatPumpHysteresis(bandwidth=0.1)
        annotation (Placement(transformation(extent={{-70,48},{-50,68}})));
      Modelica.Blocks.Logical.And and1
        annotation (Placement(transformation(extent={{-12,36},{8,56}})));
      AdaptedModels.HeatPumpHysteresis heatPumpHysteresis1(bandwidth=0.1)
        annotation (Placement(transformation(extent={{-68,6},{-48,26}})));

      Modelica.Blocks.Interfaces.RealInput control_variable
        annotation (Placement(transformation(extent={{-280,50},{-240,90}})));
      Modelica.Blocks.Logical.LessEqual lessEqual
        annotation (Placement(transformation(extent={{-70,-22},{-50,-2}})));
      Modelica.Blocks.Logical.And and2
        annotation (Placement(transformation(extent={{-22,-8},{-2,12}})));
    equation
      connect(heatPumpHysteresis.T_sup, sigBusGen.THeaPumOut) annotation (
          Line(points={{-72,50},{-76,50},{-76,54},{-100,54},{-100,-99},{-152,-99}},
                     color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(and1.y, HP_nSet_Controller.HP_On) annotation (Line(points={{9,46},{
              14,46},{14,78},{79,78}},              color={255,0,255}));
      connect(heatPumpHysteresis.y, and1.u1) annotation (Line(points={{-49,58},
              {-22,58},{-22,46},{-14,46}},
                                      color={255,0,255}));
      connect(heatPumpHysteresis1.T_set, useProBus.TZoneSet[1]) annotation (Line(
            points={{-70,24},{-94,24},{-94,103},{-119,103}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(heatPumpHysteresis1.T_sup, buiMeaBus.TZoneMea[1]) annotation (Line(
            points={{-70,8},{-78,8},{-78,100},{-76,100},{-76,120},{65,120},{65,103}},
            color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(heatPumpHysteresis.T_set, control_variable) annotation (Line(
            points={{-72,66},{-234,66},{-234,70},{-260,70}}, color={0,0,127}));
      connect(HP_nSet_Controller.T_Set, control_variable) annotation (Line(
            points={{79,86.4},{78,86.4},{78,70},{-44,70},{-44,74},{-92,74},{
              -92,66},{-234,66},{-234,70},{-260,70}}, color={0,0,127}));
      connect(and2.y, and1.u2) annotation (Line(points={{-1,2},{12,2},{12,30},
              {-38,30},{-38,38},{-14,38}}, color={255,0,255}));
      connect(heatPumpHysteresis1.y, and2.u1) annotation (Line(points={{-47,
              16},{-24,16},{-24,2}}, color={255,0,255}));
      connect(lessEqual.y, and2.u2) annotation (Line(points={{-49,-12},{-38,
              -12},{-38,-14},{-24,-14},{-24,-6}}, color={255,0,255}));
      connect(heatPumpHysteresis1.T_sup, lessEqual.u1) annotation (Line(
            points={{-70,8},{-78,8},{-78,-6},{-76,-6},{-76,-12},{-72,-12}},
            color={0,0,127}));
      connect(heatPumpHysteresis1.T_set, lessEqual.u2) annotation (Line(
            points={{-70,24},{-86,24},{-86,-16},{-80,-16},{-80,-20},{-72,-20}},
            color={0,0,127}));
    end FMU_SupTem3;

    model FMU_SupTem2 "MA Daniel - FMU SupTem2"
      extends AdaptedModels.PartialHydraulicControl(redeclare
          BESMod.Systems.Hydraulical.Control.Components.HeatPumpNSetController.PI_InverterHeatPumpController
          HP_nSet_Controller, redeclare
          BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultBivHPControl
          bivalentControlData);
      AdaptedModels.HeatPumpHysteresis heatPumpHysteresis(bandwidth=0.1)
        annotation (Placement(transformation(extent={{-70,50},{-50,70}})));
      Modelica.Blocks.Logical.And and1
        annotation (Placement(transformation(extent={{-30,42},{-10,62}})));
      AdaptedModels.HeatPumpHysteresis heatPumpHysteresis1(bandwidth=1)
        annotation (Placement(transformation(extent={{-68,6},{-48,26}})));
      Modelica.Blocks.Interfaces.RealInput control_variable
        annotation (Placement(transformation(extent={{-282,40},{-242,80}})));
    equation
      connect(heatPumpHysteresis.T_sup, sigBusGen.THeaPumOut) annotation (
          Line(points={{-72,52},{-76,52},{-76,54},{-100,54},{-100,-99},{-152,
              -99}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(and1.y, HP_nSet_Controller.HP_On) annotation (Line(points={{-9,
              52},{12,52},{12,48},{36,48},{36,78},{79,78}}, color={255,0,255}));
      connect(heatPumpHysteresis.y, and1.u1) annotation (Line(points={{-49,60},
              {-42,60},{-42,52},{-32,52}}, color={255,0,255}));
      connect(heatPumpHysteresis1.y, and1.u2) annotation (Line(points={{-47,
              16},{-36,16},{-36,44},{-32,44}}, color={255,0,255}));
      connect(heatPumpHysteresis1.T_set, useProBus.TZoneSet[1]) annotation (
          Line(points={{-70,24},{-94,24},{-94,103},{-119,103}}, color={0,0,
              127}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(heatPumpHysteresis1.T_sup, buiMeaBus.TZoneMea[1]) annotation (
          Line(points={{-70,8},{-78,8},{-78,100},{-76,100},{-76,120},{65,120},
              {65,103}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(heatPumpHysteresis.T_set, control_variable) annotation (Line(
            points={{-72,68},{-132,68},{-132,70},{-194,70},{-194,60},{-262,60}},
            color={0,0,127}));
      connect(HP_nSet_Controller.T_Set, control_variable) annotation (Line(
            points={{79,86.4},{-212,86.4},{-212,60},{-262,60}}, color={0,0,
              127}));
    end FMU_SupTem2;

    model SimFMU
      Modelica.Blocks.Sources.Constant const(k=40 + 273.15)
        annotation (Placement(transformation(extent={{-82,10},{-62,30}})));
    equation
      connect(const.y, wPmitFBH_fmu.control_variable)
        annotation (Line(points={{-61,20},{-6.4,20}}, color={0,0,127}));
      annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
            coordinateSystem(preserveAspectRatio=false)),
        experiment(
          StopTime=7244400,
          Interval=600,
          __Dymola_Algorithm="Dassl"));
    end SimFMU;

    model WPmitFBH_adapted_FMU
      "Wärmepumpe mit Fußbodenheizung als FMU Export möglich"
      extends BESMod.Systems.BaseClasses.PartialBuildingEnergySystem(
        redeclare BESMod.Systems.Hydraulical.HydraulicSystem hydraulic(
          redeclare MA_Daniel.AdaptedModels.HeatPump_without_HeatingRod
            generation(redeclare model PerDataMainHP =
                AixLib.DataBase.HeatPump.PerformanceData.LookUpTableND (
                nConv=90,
                interpMethod=SDF.Types.InterpolationMethod.Linear,
                extrapMethod=SDF.Types.ExtrapolationMethod.Linear,
                filename_Pel=ModelicaServices.ExternalReferences.loadResource(
                    "modelica://MPC/Resources/Optihorst/Optihorst_New.sdf"),
                filename_QCon=ModelicaServices.ExternalReferences.loadResource(
                    "modelica://MPC/Resources/Optihorst/Optihorst_New.sdf")),
              redeclare
              BESMod.Systems.Hydraulical.Interfaces.Generation.RecordsCollection.DefaultHP
              heatPumpParameters(genDesTyp=BESMod.Systems.Hydraulical.Interfaces.Generation.Types.GenerationDesign.Monovalent,
                VCon=0.0047)),
          redeclare MA_Daniel.AdaptedModels.Controls.FMU_SupTem1 control(
            control_variable(y=control_variable),
            extInput=true,
            externalInput(y=extInput)),
          redeclare MA_Daniel.AdaptedModels.NoDistributionSystem distribution(
              nParallelDem=1),
          redeclare MA_Daniel.AdaptedModels.UFHTransferSystem_Ueberstroemventil
            transfer(redeclare MPC.DataBase.UFH_Optihorst_Case UFHParameters,
              redeclare
              BESMod.Systems.Hydraulical.Transfer.RecordsCollection.SteelRadiatorStandardPressureLossData
              transferDataBaseDefinition)),
        redeclare BESMod.Systems.Control.NoControl control,
        redeclare MPC.Submodules.Demand.DHWHil DHW(
          mDHW_flow_nominal=0,
          VDHWDay=0,
          tCrit=0,
          QCrit=0),
        redeclare package MediumDHW = IBPSA.Media.Water,
        redeclare package MediumZone = IBPSA.Media.Air,
        redeclare package MediumHyd = IBPSA.Media.Water,
        redeclare BESMod.Systems.Ventilation.NoVentilation ventilation,
        redeclare BESMod.Systems.Electrical.DirectGridConnectionSystem
          electrical,
        redeclare BESMod.Systems.Demand.Building.TEASERThermalZone building(
          ABui=13.751,
          hBui=6.875,
          ARoo=172.9,
          redeclare MPC.DataBase.MPC_Zone_SingleDwelling_130 oneZoneParam,
          TOda_nominal=systemParameters.nZones),
        redeclare final BESMod.Systems.RecordsCollection.ParameterStudy.NoStudy
          parameterStudy,
        redeclare final MPC.DataBase.SystemParametersMPC systemParameters(
          QBui_flow_nominal={7641},
          use_hydraulic=true,
          use_ventilation=false,
          use_dhw=false,
          use_elecHeating=false),
        redeclare BESMod.Systems.UserProfiles.TEASERProfiles userProfiles);

      Modelica.Blocks.Interfaces.RealInput control_variable annotation (Placement(
            transformation(extent={{-400,-22},{-282,96}}), iconTransformation(
              extent={{-400,-22},{-282,96}})));

      parameter Boolean extInput=true;

      parameter Boolean uAsControlVariable=true "True for compressor and false for supply temperature";

    equation

      annotation (experiment(
          StopTime=2419200,
          Interval=599.999616,
          __Dymola_Algorithm="Dassl"));
    end WPmitFBH_adapted_FMU;

    model WPmitFBH_adapted "Wärmepumpe mit Fußbodenheizung"
      extends BESMod.Systems.BaseClasses.PartialBuildingEnergySystem(
        redeclare BESMod.Systems.Hydraulical.HydraulicSystem hydraulic(
          redeclare MA_Daniel.AdaptedModels.HeatPump_without_HeatingRod
            generation(redeclare model PerDataMainHP =
                AixLib.DataBase.HeatPump.PerformanceData.LookUpTableND (
                nConv=90,
                interpMethod=SDF.Types.InterpolationMethod.Linear,
                extrapMethod=SDF.Types.ExtrapolationMethod.Linear,
                filename_Pel=ModelicaServices.ExternalReferences.loadResource(
                    "modelica://MPC/Resources/Optihorst/Optihorst_New.sdf"),
                filename_QCon=ModelicaServices.ExternalReferences.loadResource(
                    "modelica://MPC/Resources/Optihorst/Optihorst_New.sdf")),
              redeclare
              BESMod.Systems.Hydraulical.Interfaces.Generation.RecordsCollection.DefaultHP
              heatPumpParameters(genDesTyp=BESMod.Systems.Hydraulical.Interfaces.Generation.Types.GenerationDesign.Monovalent,
                VCon=0.0047)),
          redeclare MA_Daniel.NotUsedAnymore.HeatCurve1 control,
          redeclare MA_Daniel.AdaptedModels.NoDistributionSystem distribution(
              nParallelDem=1),
          redeclare MA_Daniel.AdaptedModels.UFHTransferSystem_Ueberstroemventil
            transfer(
            redeclare MPC.DataBase.UFH_Optihorst_Case UFHParameters,
            dpFullOpen_nominal(displayUnit="bar"),
            redeclare
              BESMod.Systems.Hydraulical.Transfer.RecordsCollection.SteelRadiatorStandardPressureLossData
              transferDataBaseDefinition,
            pressureReliefValveOnOff(val(R=50)))),
        redeclare BESMod.Systems.Control.NoControl control,
        redeclare MPC.Submodules.Demand.DHWHil DHW(
          mDHW_flow_nominal=0,
          VDHWDay=0,
          tCrit=0,
          QCrit=0),
        redeclare package MediumDHW = IBPSA.Media.Water,
        redeclare package MediumZone = IBPSA.Media.Air,
        redeclare package MediumHyd = IBPSA.Media.Water,
        redeclare BESMod.Systems.Ventilation.NoVentilation ventilation,
        redeclare BESMod.Systems.Electrical.DirectGridConnectionSystem
          electrical,
        redeclare BESMod.Systems.Demand.Building.TEASERThermalZone building(
          ABui=13.751,
          hBui=6.875,
          ARoo=172.9,
          redeclare MPC.DataBase.MPC_Zone_SingleDwelling_130 oneZoneParam,
          TOda_nominal=systemParameters.nZones),
        redeclare final BESMod.Systems.RecordsCollection.ParameterStudy.NoStudy
          parameterStudy,
        redeclare final MPC.DataBase.SystemParametersMPC systemParameters(
          QBui_flow_nominal={7641},
          use_hydraulic=true,
          use_ventilation=false,
          use_dhw=false,
          use_elecHeating=false),
        redeclare BESMod.Systems.UserProfiles.TEASERProfiles userProfiles);

    equation

      annotation (experiment(
          StopTime=2419200,
          Interval=599.999616,
          __Dymola_Algorithm="Dassl"));
    end WPmitFBH_adapted;

    model TEASERProfiles "MA Daniel - Standard TEASER Profiles"
      extends BESMod.Systems.UserProfiles.BaseClasses.PartialUserProfiles;
      parameter String fileNameIntGains=Modelica.Utilities.Files.loadResource("modelica://BESMod/Resources/InternalGains.txt")
        "File where matrix is stored";
      parameter Real gain[3]=fill(1, 3) "Gain value multiplied with internal gains. Used to e.g. disable single gains.";

      Modelica.Blocks.Sources.CombiTimeTable tabIntGai(
        final tableOnFile=true,
        final extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
        final tableName="Internals",
        final fileName=fileNameIntGains,
        columns=2:4) "Profiles for internal gains" annotation (Placement(
            transformation(
            extent={{10,10},{-10,-10}},
            rotation=180,
            origin={-10,30})));

      Modelica.Blocks.Math.Gain gainIntGai[3](k=gain) "Gain for internal gains"
        annotation (Placement(transformation(
            extent={{10,10},{-10,-10}},
            rotation=180,
            origin={30,30})));

      Modelica.Blocks.Sources.Constant conTSetZone[nZones](k=TSetZone_nominal)
        "Constant room set temperature" annotation (Placement(transformation(
            extent={{10,10},{-10,-10}},
            rotation=180,
            origin={-32,-82})));
      Modelica.Blocks.Sources.BooleanExpression FMU_input
        annotation (Placement(transformation(extent={{-74,-56},{-54,-36}})));
      Modelica.Blocks.Sources.RealExpression TZoneSet_outer
        annotation (Placement(transformation(extent={{-72,-32},{-52,-12}})));
      Modelica.Blocks.Logical.Switch switch1
        annotation (Placement(transformation(extent={{26,-60},{46,-40}})));
    equation
      connect(tabIntGai.y, gainIntGai.u)
        annotation (Line(points={{1,30},{18,30}}, color={0,0,127}));
      connect(gainIntGai.y, useProBus.intGains) annotation (Line(points={{41,30},{
              74,30},{74,-1},{115,-1}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
      connect(FMU_input.y, switch1.u2) annotation (Line(points={{-53,-46},{16,
              -46},{16,-50},{24,-50}}, color={255,0,255}));
      connect(TZoneSet_outer.y, switch1.u1) annotation (Line(points={{-51,-22},
              {-30,-22},{-30,-20},{-4,-20},{-4,-42},{24,-42}}, color={0,0,127}));
      connect(conTSetZone[1].y, switch1.u3) annotation (Line(points={{-21,-82},
              {-10,-82},{-10,-84},{4,-84},{4,-58},{24,-58}}, color={0,0,127}));
      connect(switch1.y, useProBus.TZoneSet[1]) annotation (Line(points={{47,
              -50},{70,-50},{70,-52},{115,-52},{115,-1}}, color={0,0,127}),
          Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
    end TEASERProfiles;

    block DeadZoneCustom "Provide a region of zero output"
      parameter Real uMax(start=1) "Upper limits of dead zones";
      parameter Real uMin=-uMax "Lower limits of dead zones";
      extends Modelica.Blocks.Interfaces.SISO;

    equation
      assert(uMax >= uMin, "DeadZone: Limits must be consistent. However, uMax (=" + String(uMax) +
                           ") < uMin (=" + String(uMin) + ")");

      y = homotopy(actual=smooth(0,if u > uMax then u else if u < uMin then u else 0), simplified=u);

      annotation (
        Documentation(info="<html>
<p>
The DeadZone block defines a region of zero output.
</p>
<p>
If the input is within uMin ... uMax, the output
is zero. Outside of this zone, the output is a linear
function of the input with a slope of 1.
</p>
</html>"),
         Icon(coordinateSystem(
        preserveAspectRatio=true,
        extent={{-100,-100},{100,100}}), graphics={
        Line(points={{0,-90},{0,68}}, color={192,192,192}),
        Polygon(
          points={{0,90},{-8,68},{8,68},{0,90}},
          lineColor={192,192,192},
          fillColor={192,192,192},
          fillPattern=FillPattern.Solid),
        Line(points={{-90,0},{68,0}}, color={192,192,192}),
        Polygon(
          points={{90,0},{68,-8},{68,8},{90,0}},
          lineColor={192,192,192},
          fillColor={192,192,192},
          fillPattern=FillPattern.Solid),
        Line(points={{-80,-60},{-20,0},{20,0},{80,60}}),
        Text(
          extent={{-150,-150},{150,-110}},
          textColor={160,160,164},
          textString="uMax=%uMax")}));
    end DeadZoneCustom;

    model BuildingOnly_MA_Daniel "Only loads building + DHW connection"
      extends
        BESMod.Systems.Hydraulical.Distribution.BaseClasses.PartialDistribution(
        use_dhw=false,
        final fFullSto=0,
        final QDHWBefSto_flow_nominal=Modelica.Constants.eps,
        final VStoDHW=0,
        QCrit=0,
        tCrit=0,
        final QDHWStoLoss_flow=0,
        final designType=BESMod.Systems.Hydraulical.Distribution.Types.DHWDesignType.NoStorage,
        QDHW_flow_nominal=Modelica.Constants.eps,
        final dpDem_nominal=fill(0, nParallelDem),
        final dpSup_nominal=fill(0, nParallelSup),
        final nParallelSup=1,
        final dTTraDHW_nominal=0,
        final QLoss_flow_nominal=f_design .* Q_flow_nominal .- Q_flow_nominal,
        final f_design=fill(1, nParallelDem),
        final dTLoss_nominal=fill(0, nParallelDem),
        final m_flow_nominal=mSup_flow_nominal,
        final TSup_nominal=TDem_nominal .+ dTLoss_nominal .+ dTTra_nominal,
        redeclare package MediumGen = Medium,
        redeclare package MediumDHW = Medium,
        final dTTra_nominal=fill(0, nParallelDem));

      BESMod.Utilities.Electrical.ZeroLoad zeroLoad
        annotation (Placement(transformation(extent={{30,-108},{50,-88}})));
      Modelica.Blocks.Sources.RealExpression reaExpTStoBufTopMea(y=
            Medium.temperature(Medium.setState_phX(
            portGen_in[1].p,
            inStream(portGen_in[1].h_outflow),
            inStream(portGen_in[1].Xi_outflow))))
        "Use storage name as all controls use this variable"
        annotation (Placement(transformation(extent={{-82,0},{-62,20}})));
      Modelica.Blocks.Sources.RealExpression reaExpTStoDHWTopMea(y=
            Medium.temperature(Medium.setState_phX(
                portGen_in[1].p,
                inStream(portGen_in[1].h_outflow),
                inStream(portGen_in[1].Xi_outflow))))
        "Use storage name as all controls use this variable"
        annotation (Placement(transformation(extent={{-18,-22},{2,-2}})));
    equation
      connect(zeroLoad.internalElectricalPin, internalElectricalPin) annotation (
          Line(
          points={{50,-98},{70,-98}},
          color={0,0,0},
          thickness=1));
      connect(reaExpTStoBufTopMea.y, sigBusDistr.TStoBufTopMea) annotation (Line(
            points={{-61,10},{0,10},{0,101}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
      connect(portGen_out[1], portBui_in[1])
        annotation (Line(points={{-100,40},{100,40}}, color={0,127,255}));
      connect(portGen_in[1], portBui_out[1])
        annotation (Line(points={{-100,80},{100,80}}, color={0,127,255}));
      connect(reaExpTStoDHWTopMea.y, sigBusDistr.TStoDHWTopMea) annotation (
          Line(points={{3,-12},{18,-12},{18,101},{0,101}}, color={0,0,127}),
          Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
    end BuildingOnly_MA_Daniel;

    model HeatCurve1 "MA Daniel - HeatCurve1"
      extends AdaptedModels.PartialHydraulicControl(redeclare
          BESMod.Systems.Hydraulical.Control.Components.HeatPumpNSetController.PI_InverterHeatPumpController
          HP_nSet_Controller, redeclare
          BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultBivHPControl
          bivalentControlData);
      BESMod.Systems.Hydraulical.Control.Components.HeatingCurve
        heatingCurve(
        GraHeaCurve=bivalentControlData.gradientHeatCurve,
        THeaThres=bivalentControlData.TSetRoomConst,
        dTOffSet_HC=bivalentControlData.dTOffSetHeatCurve)
        annotation (Placement(transformation(extent={{-196,42},{-176,62}})));
      AdaptedModels.HeatPumpHysteresis heatPumpHysteresis(bandwidth=0.2)
        annotation (Placement(transformation(extent={{-70,50},{-50,70}})));
    equation
      connect(heatingCurve.TOda, weaBus.TDryBul) annotation (Line(points={{-198,52},
              {-210,52},{-210,2},{-237,2}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(heatingCurve.TSetRoom, useProBus.TZoneSet[1]) annotation (Line(
            points={{-186,64},{-186,74},{-119,74},{-119,103}},     color={0,0,
              127}), Text(
          string="%second",
          index=1,
          extent={{-3,6},{-3,6}},
          horizontalAlignment=TextAlignment.Right));
      connect(heatPumpHysteresis.T_sup, sigBusGen.THeaPumOut) annotation (
          Line(points={{-72,52},{-76,52},{-76,54},{-100,54},{-100,-99},{-152,
              -99}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(heatPumpHysteresis.y, HP_nSet_Controller.HP_On) annotation (
          Line(points={{-49,60},{72,60},{72,78},{79,78}}, color={255,0,255}));
      connect(heatingCurve.TSet, heatPumpHysteresis.T_set) annotation (Line(
            points={{-175,52},{-156,52},{-156,48},{-134,48},{-134,58},{-130,
              58},{-130,68},{-72,68}}, color={0,0,127}));
      connect(HP_nSet_Controller.T_Set, heatPumpHysteresis.T_set) annotation (
         Line(points={{79,86.4},{-94,86.4},{-94,68},{-72,68}}, color={0,0,127}));
    end HeatCurve1;

    model Constant_SetSupTem "MA Daniel - Constant 40°C Suuply Temperature"
      extends AdaptedModels.PartialHydraulicControl(redeclare
          BESMod.Systems.Hydraulical.Control.Components.HeatPumpNSetController.PI_InverterHeatPumpController
          HP_nSet_Controller, redeclare
          BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultBivHPControl
          bivalentControlData);
      AdaptedModels.HeatPumpHysteresis heatPumpHysteresis(bandwidth=0.1)
        annotation (Placement(transformation(extent={{-70,50},{-50,70}})));
      Modelica.Blocks.Sources.RealExpression SupTem(y=40 + 273.15)
        annotation (Placement(transformation(extent={{-190,54},{-170,74}})));
    equation
      connect(heatPumpHysteresis.T_sup, sigBusGen.THeaPumOut) annotation (
          Line(points={{-72,52},{-76,52},{-76,54},{-100,54},{-100,-99},{-152,
              -99}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(heatPumpHysteresis.y, HP_nSet_Controller.HP_On) annotation (
          Line(points={{-49,60},{72,60},{72,78},{79,78}}, color={255,0,255}));
      connect(SupTem.y, heatPumpHysteresis.T_set) annotation (Line(points={{
              -169,64},{-80,64},{-80,68},{-72,68}}, color={0,0,127}));
      connect(HP_nSet_Controller.T_Set, heatPumpHysteresis.T_set) annotation (
         Line(points={{79,86.4},{-110,86.4},{-110,64},{-80,64},{-80,68},{-72,
              68}}, color={0,0,127}));
    end Constant_SetSupTem;

    model MultipleInputs_Hysteresis2
      "MA Daniel - Multiple Inputs TVL Hysteresis 2"
      extends AdaptedModels.PartialHydraulicControl_FMU(
        redeclare
          BESMod.Systems.Hydraulical.Control.RecordsCollection.DefaultBivHPControl
          bivalentControlData,
        securityControl(use_antFre=false),
        pI_InverterHeatPumpController(
          P=1,
          nMin=1/3,
          T_I=100),
        thermostaticValveController(k={thermostaticValveParameters.k}, Ti={
              thermostaticValveParameters.Ti}));

      BESMod.Systems.Hydraulical.Control.Components.HeatingCurve
        heatingCurve(
        GraHeaCurve=bivalentControlData.gradientHeatCurve,
        THeaThres=bivalentControlData.TSetRoomConst,
        dTOffSet_HC=bivalentControlData.dTOffSetHeatCurve)
        annotation (Placement(transformation(extent={{-216,24},{-196,44}})));

      Modelica.Blocks.Sources.RealExpression u_T_VL
        annotation (Placement(transformation(extent={{-320,16},{-288,34}})));
      Modelica.Blocks.Sources.RealExpression u_comp
        annotation (Placement(transformation(extent={{-320,-10},{-288,10}})));
      Modelica.Blocks.Sources.BooleanExpression b_MPC_input
        annotation (Placement(transformation(extent={{-318,62},{-286,82}})));
      Modelica.Blocks.Sources.BooleanExpression b_FMU_input
        annotation (Placement(transformation(extent={{-318,86},{-286,106}})));
      Modelica.Blocks.Sources.BooleanExpression b_u_comp_ctrl
        annotation (Placement(transformation(extent={{-318,34},{-286,54}})));
      AdaptedModels.Controls.Model_inputs_adapted model_inputs_adapted
        annotation (Placement(transformation(extent={{-250,96},{-222,132}})));
      Modelica.Blocks.Logical.OnOffController onOffController(bandwidth=6)
        annotation (Placement(transformation(extent={{-26,134},{-6,154}})));
      Modelica.Blocks.Logical.Switch switch2
        annotation (Placement(transformation(extent={{-72,78},{-52,98}})));
    equation
      connect(heatingCurve.TOda, weaBus.TDryBul) annotation (Line(points={{-218,34},
              {-254,34},{-254,2},{-237,2}}, color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(heatingCurve.TSetRoom, useProBus.TZoneSet[1]) annotation (Line(points={{-206,46},
              {-206,78},{-119,78},{-119,103}},  color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{-3,6},{-3,6}},
          horizontalAlignment=TextAlignment.Right));
      connect(b_FMU_input.y, model_inputs_adapted.b_FMU_input) annotation (Line(
            points={{-284.4,96},{-284.4,130.094},{-253,130.094}},
                                                              color={255,0,255}));
      connect(b_MPC_input.y, model_inputs_adapted.b_MPC_input) annotation (Line(
            points={{-284.4,72},{-276,72},{-276,120.988},{-252.6,120.988}},
                                                                        color={255,0,
              255}));
      connect(b_u_comp_ctrl.y, model_inputs_adapted.b_u_comp_ctrl) annotation (Line(
            points={{-284.4,44},{-270,44},{-270,117.388},{-252.6,117.388}},
                                                                        color={255,0,
              255}));
      connect(u_T_VL.y, model_inputs_adapted.u_T_VL_outer) annotation (Line(points={{-286.4,
              25},{-264,25},{-264,114},{-252.6,114}},         color={0,0,127}));
      connect(u_comp.y, model_inputs_adapted.u_comp_outer) annotation (Line(points={{-286.4,
              0},{-258,0},{-258,110.4},{-252.6,110.4}},         color={0,0,127}));
      connect(heatingCurve.TSet, model_inputs_adapted.T_set_HC) annotation (Line(
            points={{-195,34},{-178,34},{-178,4},{-166,4},{-166,60},{-246,60},{
              -246,88},{-244,88},{-244,97.6941},{-252.6,97.6941}},
                                                        color={0,0,127}));
      connect(model_inputs_adapted.n_Set, securityControl.nSet) annotation (
          Line(points={{-217.8,113.576},{178,113.576},{178,84.4},{191.867,84.4}},
                      color={0,0,127}));
      connect(pI_InverterHeatPumpController.n_Set, model_inputs_adapted.u_comp_PID)
        annotation (Line(points={{81,80},{90,80},{90,78},{98,78},{98,126},{-260,
              126},{-260,103.624},{-252.6,103.624}},      color={0,0,127}));
      connect(thermostaticValveController.TZoneSet[1], useProBus.TZoneSet[1])
        annotation (Line(points={{87.4,-46},{30,-46},{30,-24},{-134,-24},{
              -134,30},{-148,30},{-148,78},{-119,78},{-119,103}}, color={0,0,
              127}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(switch1.u2, b_MPC_input.y) annotation (Line(points={{80,-76},{
              22,-76},{22,-78},{-38,-78},{-38,72},{-284.4,72}}, color={255,0,
              255}));
      connect(pI_InverterHeatPumpController.IsOn, sigBusGen.heaPumIsOn)
        annotation (Line(points={{64,68},{-18,68},{-18,24},{-132,24},{-132,
              -99},{-152,-99}}, color={255,0,255}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(onOffController.y, pI_InverterHeatPumpController.HP_On)
        annotation (Line(points={{-5,144},{-5,62},{58,62},{58,80}}, color={
              255,0,255}));
      connect(securityControl.nOut, sigBusGen.yHeaPumSet) annotation (Line(
            points={{227.333,84.4},{302,84.4},{302,-116},{-152,-116},{-152,-99}},
                     color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}},
          horizontalAlignment=TextAlignment.Left));
      connect(onOffController.u, sigBusGen.THeaPumOut) annotation (Line(
            points={{-28,138},{-44,138},{-44,120},{-152,120},{-152,-99}},
            color={0,0,127}), Text(
          string="%second",
          index=1,
          extent={{-6,3},{-6,3}},
          horizontalAlignment=TextAlignment.Right));
      connect(model_inputs_adapted.T_VL_set, onOffController.reference)
        annotation (Line(points={{-218,104.894},{-190,104.894},{-190,102},{-150,
              102},{-150,150},{-28,150}},      color={0,0,127}));
      connect(switch2.y, pI_InverterHeatPumpController.T_Set) annotation (
          Line(points={{-51,88},{-51,82},{36,82},{36,86},{58,86}}, color={0,0,
              127}));
      connect(switch2.u1, onOffController.reference) annotation (Line(points=
              {{-74,96},{-112,96},{-112,106},{-150,106},{-150,150},{-28,150}},
            color={0,0,127}));
      connect(pI_InverterHeatPumpController.T_Meas, switch2.u3) annotation (
          Line(points={{70,68},{70,-34},{-80,-34},{-80,80},{-74,80}}, color={
              0,0,127}));
      connect(pI_InverterHeatPumpController.IsOn, switch2.u2) annotation (
          Line(points={{64,68},{-18,68},{-18,24},{-96,24},{-96,90},{-88,90},{
              -88,88},{-74,88}}, color={255,0,255}));
    end MultipleInputs_Hysteresis2;

    model WPmitFBH_FloImitation
      "Wärmepumpe mit Fußbodenheizung - Multiple Inputs"

      parameter Boolean b_FMU_input=true
                                  "True for FMU use and false for simulation in Dymola";
      extends BESMod.Systems.BaseClasses.PartialBuildingEnergySystem(
        redeclare BESMod.Systems.Hydraulical.HydraulicSystem hydraulic(
          redeclare MA_Daniel.AdaptedModels.HeatPump_without_HeatingRod
            generation(
            redeclare model PerDataMainHP =
                AixLib.DataBase.HeatPump.PerformanceData.LookUpTableND (
                nConv=90,
                interpMethod=SDF.Types.InterpolationMethod.Linear,
                extrapMethod=SDF.Types.ExtrapolationMethod.Linear,
                filename_Pel=ModelicaServices.ExternalReferences.loadResource(
                    "modelica://MPC/Resources/Optihorst/Optihorst_New.sdf"),
                filename_QCon=ModelicaServices.ExternalReferences.loadResource(
                    "modelica://MPC/Resources/Optihorst/Optihorst_New.sdf")),
            redeclare
              BESMod.Systems.Hydraulical.Interfaces.Generation.RecordsCollection.DefaultHP
              heatPumpParameters(genDesTyp=BESMod.Systems.Hydraulical.Interfaces.Generation.Types.GenerationDesign.Monovalent,
                VCon=0.0047),
            redeclare BESMod.Systems.RecordsCollection.Movers.DefaultMover
              pumpData),
          redeclare MA_Daniel.AdaptedModels.Controls.MultipleInputs_Hysteresis
            control(
            b_FMU_input(y=b_FMU_input),
            b_MPC_input(y=b_MPC_input),
            b_u_comp_ctrl(y=b_u_comp_ctrl),
            u_T_VL(y=u_T_VL),
            u_comp(y=u_comp),
            onOffController(bandwidth=6)),
          redeclare MA_Daniel.AdaptedModels.NoDistributionSystem distribution(
              nParallelDem=1, dpTraSys_nominal=hydraulic.transfer.dp_nominal),
          redeclare MA_Daniel.AdaptedModels.UFHTransferSystem_Ueberstroemventil
            transfer(redeclare MPC.DataBase.UFH_Optihorst_Case UFHParameters,
              redeclare
              BESMod.Systems.Hydraulical.Transfer.RecordsCollection.SteelRadiatorStandardPressureLossData
              transferDataBaseDefinition)),
        redeclare BESMod.Systems.Control.NoControl control,
        redeclare MPC.Submodules.Demand.DHWHil DHW(
          mDHW_flow_nominal=0,
          VDHWDay=0,
          tCrit=0,
          QCrit=0),
        redeclare package MediumDHW = IBPSA.Media.Water,
        redeclare package MediumZone = IBPSA.Media.Air,
        redeclare package MediumHyd = IBPSA.Media.Water,
        redeclare BESMod.Systems.Ventilation.NoVentilation ventilation,
        redeclare BESMod.Systems.Electrical.DirectGridConnectionSystem electrical,
        redeclare BESMod.Systems.Demand.Building.TEASERThermalZone building(
          ABui=65,
          hBui=5,
          ARoo=172.9,
          redeclare MPC.DataBase.MPC_Zone_SingleDwelling_130 oneZoneParam),
        redeclare final BESMod.Systems.RecordsCollection.ParameterStudy.NoStudy
          parameterStudy,
        redeclare final MPC.DataBase.SystemParametersMPC systemParameters(
          QBui_flow_nominal={6596.211},
          filNamWea=Modelica.Utilities.Files.loadResource(
              "modelica://BESMod/Resources/TRY2015_522361130393_Jahr_City_Potsdam_HeatingPeriod.mos"),
          use_hydraulic=true,
          use_ventilation=false,
          use_dhw=false,
          use_elecHeating=false),
        redeclare BESMod.Systems.UserProfiles.TEASERProfiles userProfiles);

      Modelica.Blocks.Interfaces.RealInput u_T_VL annotation (Placement(
            transformation(extent={{-338,-10},{-298,30}}), iconTransformation(
              extent={{-338,-10},{-298,30}})));

            Modelica.Blocks.Interfaces.BooleanInput b_MPC_input
        annotation (Placement(transformation(extent={{-338,66},{-298,106}}),
            iconTransformation(extent={{-338,66},{-298,106}})));
      Modelica.Blocks.Interfaces.RealInput u_comp annotation (Placement(
            transformation(extent={{-338,-54},{-298,-14}}),
                                                          iconTransformation(extent={{-320,
                -40},{-280,0}})));
            Modelica.Blocks.Interfaces.BooleanInput b_u_comp_ctrl
        annotation (Placement(transformation(extent={{-338,32},{-298,72}}),
            iconTransformation(extent={{-338,32},{-298,72}})));
    equation

      annotation (experiment(
          StopTime=1209600,
          Interval=600.001344,
          __Dymola_Algorithm="Dassl"),
        Diagram(coordinateSystem(extent={{-280,-140},{280,200}})),
        Icon(coordinateSystem(extent={{-280,-140},{280,200}})));
    end WPmitFBH_FloImitation;

    model WPmitFBH "Wärmepumpe mit Fußbodenheizung für FMU"

      parameter Boolean b_FMU_input=true
                                  "True for FMU use and false for simulation in Dymola";
      extends BESMod.Systems.BaseClasses.PartialBuildingEnergySystem(
        redeclare BESMod.Systems.Hydraulical.HydraulicSystem hydraulic(
          redeclare MA_Daniel.AdaptedModels.HeatPump_without_HeatingRod
            generation(
            redeclare model PerDataMainHP =
                AixLib.DataBase.HeatPump.PerformanceData.LookUpTableND (
                nConv=90,
                interpMethod=SDF.Types.InterpolationMethod.Linear,
                extrapMethod=SDF.Types.ExtrapolationMethod.Linear,
                filename_Pel=ModelicaServices.ExternalReferences.loadResource(
                    "modelica://MPC/Resources/Optihorst/Optihorst_New.sdf"),
                filename_QCon=ModelicaServices.ExternalReferences.loadResource(
                    "modelica://MPC/Resources/Optihorst/Optihorst_New.sdf")),
            redeclare
              BESMod.Systems.Hydraulical.Interfaces.Generation.RecordsCollection.DefaultHP
              heatPumpParameters(genDesTyp=BESMod.Systems.Hydraulical.Interfaces.Generation.Types.GenerationDesign.Monovalent,
                VCon=0.0047),
            redeclare BESMod.Systems.RecordsCollection.Movers.DefaultMover
              pumpData),
          redeclare MA_Daniel.AdaptedModels.Controls.MultipleInputs_Hysteresis
            control(
            b_FMU_input(y=b_FMU_input),
            b_MPC_input(y=b_MPC_input),
            b_u_comp_ctrl(y=b_u_comp_ctrl),
            u_T_VL(y=u_T_VL),
            u_comp(y=u_comp)),
          redeclare MA_Daniel.AdaptedModels.NoDistributionSystem distribution(
              nParallelDem=1, dpTraSys_nominal=hydraulic.transfer.dp_nominal),
          redeclare MA_Daniel.AdaptedModels.UFHTransferSystem_Ueberstroemventil
            transfer(redeclare MPC.DataBase.UFH_Optihorst_Case UFHParameters,
              redeclare
              BESMod.Systems.Hydraulical.Transfer.RecordsCollection.SteelRadiatorStandardPressureLossData
              transferDataBaseDefinition)),
        redeclare BESMod.Systems.Control.NoControl control,
        redeclare MPC.Submodules.Demand.DHWHil DHW(
          mDHW_flow_nominal=0,
          VDHWDay=0,
          tCrit=0,
          QCrit=0),
        redeclare package MediumDHW = IBPSA.Media.Water,
        redeclare package MediumZone = IBPSA.Media.Air,
        redeclare package MediumHyd = IBPSA.Media.Water,
        redeclare BESMod.Systems.Ventilation.NoVentilation ventilation,
        redeclare BESMod.Systems.Electrical.DirectGridConnectionSystem electrical,
        redeclare BESMod.Systems.Demand.Building.TEASERThermalZone building(
          ABui=79,
          hBui=5,
          ARoo=149.626,
          redeclare MA_Daniel.DataBases.Area158_Tabula_de oneZoneParam),
        redeclare final BESMod.Systems.RecordsCollection.ParameterStudy.NoStudy
          parameterStudy,
        redeclare final MPC.DataBase.SystemParametersMPC systemParameters(
          QBui_flow_nominal={8200},
          filNamWea=Modelica.Utilities.Files.loadResource(
              "modelica://BESMod/Resources/TRY2015_522361130393_Jahr_City_Potsdam_HeatingPeriod.mos"),
          use_hydraulic=true,
          use_ventilation=false,
          use_dhw=false,
          use_elecHeating=false),
        redeclare MA_Daniel.AdaptedModels.Profiles_MPC userProfiles(
            fileNameIntGains=Modelica.Utilities.Files.loadResource(
              "modelica://BESMod/Resources/UserProfilesHeatingPeriod.txt")));

      Modelica.Blocks.Interfaces.RealInput u_T_VL( start=293.15) annotation (Placement(
            transformation(extent={{-338,-10},{-298,30}}), iconTransformation(
              extent={{-338,-10},{-298,30}})));

            Modelica.Blocks.Interfaces.BooleanInput b_MPC_input( start=true)
        annotation (Placement(transformation(extent={{-338,66},{-298,106}}),
            iconTransformation(extent={{-338,66},{-298,106}})));
      Modelica.Blocks.Interfaces.RealInput u_comp annotation (Placement(
            transformation(extent={{-338,-54},{-298,-14}}),
                                                          iconTransformation(extent={{-320,
                -40},{-280,0}})));
            Modelica.Blocks.Interfaces.BooleanInput b_u_comp_ctrl
        annotation (Placement(transformation(extent={{-338,32},{-298,72}}),
            iconTransformation(extent={{-338,32},{-298,72}})));
      Modelica.Blocks.Sources.RealExpression TErrorCalc(y=hydraulic.control.thermostaticValveController.TZoneMea[
            1] - userProfiles.variableSet.y[1])
        annotation (Placement(transformation(extent={{210,154},{260,190}})));
      Modelica.Blocks.Interfaces.RealOutput TError
        annotation (Placement(transformation(extent={{280,160},{300,180}})));
    equation

      connect(TErrorCalc.y, TError) annotation (Line(points={{262.5,172},{274,172},{
              274,170},{290,170}}, color={0,0,127}));
      annotation (experiment(
          StartTime=3715200,
          StopTime=4320000,
          Interval=599.999616,
          __Dymola_Algorithm="Dassl"),
        Diagram(coordinateSystem(extent={{-280,-140},{280,200}})),
        Icon(coordinateSystem(extent={{-280,-140},{280,200}})));
    end WPmitFBH;

    model WPmitFBH_HeatingCurve
      "Wärmepumpe mit Fußbodenheizung - Heating Curve"

      parameter Boolean b_FMU_input=true
                                  "True for FMU use and false for simulation in Dymola";
      extends BESMod.Systems.BaseClasses.PartialBuildingEnergySystem(
        redeclare BESMod.Systems.Hydraulical.HydraulicSystem hydraulic(
          redeclare MA_Daniel.AdaptedModels.HeatPump_without_HeatingRod
            generation(
            redeclare model PerDataMainHP =
                AixLib.DataBase.HeatPump.PerformanceData.LookUpTableND (
                nConv=90,
                interpMethod=SDF.Types.InterpolationMethod.Linear,
                extrapMethod=SDF.Types.ExtrapolationMethod.Linear,
                filename_Pel=ModelicaServices.ExternalReferences.loadResource(
                    "modelica://MPC/Resources/Optihorst/Optihorst_New.sdf"),
                filename_QCon=ModelicaServices.ExternalReferences.loadResource(
                    "modelica://MPC/Resources/Optihorst/Optihorst_New.sdf")),
            redeclare
              BESMod.Systems.Hydraulical.Interfaces.Generation.RecordsCollection.DefaultHP
              heatPumpParameters(genDesTyp=BESMod.Systems.Hydraulical.Interfaces.Generation.Types.GenerationDesign.Monovalent,
                VCon=0.0047),
            redeclare BESMod.Systems.RecordsCollection.Movers.DefaultMover
              pumpData),
          redeclare MA_Daniel.AdaptedModels.Controls.HeatingCurve control(
            b_FMU_input(y=b_FMU_input),
            b_MPC_input(y=b_MPC_input),
            b_u_comp_ctrl(y=b_u_comp_ctrl),
            u_T_VL(y=u_T_VL),
            u_comp(y=u_comp),
            bivalentControlData(dTOffSetHeatCurve=0)),
          redeclare MA_Daniel.AdaptedModels.NoDistributionSystem distribution(
              nParallelDem=1, dpTraSys_nominal=hydraulic.transfer.dp_nominal),
          redeclare MA_Daniel.AdaptedModels.UFHTransferSystem_Ueberstroemventil
            transfer(redeclare MPC.DataBase.UFH_Optihorst_Case UFHParameters,
              redeclare
              BESMod.Systems.Hydraulical.Transfer.RecordsCollection.SteelRadiatorStandardPressureLossData
              transferDataBaseDefinition)),
        redeclare BESMod.Systems.Control.NoControl control,
        redeclare MPC.Submodules.Demand.DHWHil DHW(
          mDHW_flow_nominal=0,
          VDHWDay=0,
          tCrit=0,
          QCrit=0),
        redeclare package MediumDHW = IBPSA.Media.Water,
        redeclare package MediumZone = IBPSA.Media.Air,
        redeclare package MediumHyd = IBPSA.Media.Water,
        redeclare BESMod.Systems.Ventilation.NoVentilation ventilation,
        redeclare BESMod.Systems.Electrical.DirectGridConnectionSystem electrical,
        redeclare BESMod.Systems.Demand.Building.TEASERThermalZone building(
          ABui=79,
          hBui=5,
          ARoo=149.626,
          redeclare MA_Daniel.DataBases.Area158_Tabula_de oneZoneParam),
        redeclare final BESMod.Systems.RecordsCollection.ParameterStudy.NoStudy
          parameterStudy,
        redeclare final MPC.DataBase.SystemParametersMPC systemParameters(
          QBui_flow_nominal={8200},
          filNamWea=Modelica.Utilities.Files.loadResource(
              "modelica://BESMod/Resources/TRY2015_522361130393_Jahr_City_Potsdam_HeatingPeriod.mos"),
          use_hydraulic=true,
          use_ventilation=false,
          use_dhw=false,
          use_elecHeating=false),
        redeclare MA_Daniel.AdaptedModels.Profiles_HC userProfiles);

      Modelica.Blocks.Interfaces.RealInput u_T_VL( start=293.15) annotation (Placement(
            transformation(extent={{-338,-10},{-298,30}}), iconTransformation(
              extent={{-338,-10},{-298,30}})));

            Modelica.Blocks.Interfaces.BooleanInput b_MPC_input( start=false)
        annotation (Placement(transformation(extent={{-338,66},{-298,106}}),
            iconTransformation(extent={{-338,66},{-298,106}})));
      Modelica.Blocks.Interfaces.RealInput u_comp annotation (Placement(
            transformation(extent={{-338,-54},{-298,-14}}),
                                                          iconTransformation(extent={{-320,
                -40},{-280,0}})));
            Modelica.Blocks.Interfaces.BooleanInput b_u_comp_ctrl
        annotation (Placement(transformation(extent={{-338,32},{-298,72}}),
            iconTransformation(extent={{-338,32},{-298,72}})));
      Modelica.Blocks.Sources.RealExpression TErrorCalc(y=hydraulic.control.thermostaticValveController.TZoneMea[
            1] - userProfiles.variableSet.y[1])
        annotation (Placement(transformation(extent={{210,154},{260,190}})));
      Modelica.Blocks.Interfaces.RealOutput TError
        annotation (Placement(transformation(extent={{280,160},{300,180}})));
    equation

      connect(TErrorCalc.y, TError) annotation (Line(points={{262.5,172},{274,172},{
              274,170},{290,170}}, color={0,0,127}));
      annotation (experiment(
          StopTime=31536000,
          Interval=599.999616,
          __Dymola_Algorithm="Dassl"),
        Diagram(coordinateSystem(extent={{-280,-140},{280,200}})),
        Icon(coordinateSystem(extent={{-280,-140},{280,200}})));
    end WPmitFBH_HeatingCurve;
  end NotUsedAnymore;

  annotation (uses(Modelica(version="4.0.0"),
      IBPSA(version="4.0.0"),
      AixLib(version="1.3.1"),
      ModelicaServices(version="4.0.0"),
      SDF(version="0.4.2"),
      MQTT_Interface(version="0.0.2"),
      BESMod(version="0.3.2"),
      MPC(version="5"),
      PCMgoesHIL(version="2"),
      Buildings(version="9.0.0")),
    version="1",
    conversion(noneFromVersion=""));
end MA_Daniel;
