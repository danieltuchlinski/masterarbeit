# import pandas as pd
# from os.path import isfile
# import os
# from pathlib import Path
# from ddmpc.modeling.modeling import Model
# from ddmpc.systems.base_class import System
# from ddmpc.utils.pickle_handler import write_pkl, read_pkl
# from ddmpc.utils.communication import mqttClient, httpsClient
# import ddmpc.utils.fwi_Utils as fwi_Utils
#
#
# class HiL(System):
#     """
#     This HiL class implements functionalities to interact with HIL
#     """
#
#     def __init__(
#             self,
#             model: Model,
#             step_size:  int,
#             name: str,
#             https_client: httpsClient,
#             mqtt_client: mqttClient,
#             directory_dist: Path = Path('stored_data', 'Disturbances'),
#
#     ):
#         """
#         initialize HIL System class
#         :param step_size:       time step size of the fmu
#         """
#
#         # initialize parent class
#         super().__init__(
#             model=model,
#             step_size=step_size,
#             name=name,
#         )
#
#         self.directory_dist = directory_dist
#         self.variable_dict_send, self.variable_dict_read = self.get_variable_dict()
#
#         self.mqttClient = mqtt_client
#         self.httpsClient = https_client
#
#         # disturbances
#         self.disturbances = None
#
#         self.load_disturbances()
#
#     def setup(
#             self,
#             start_time:     int,
#             instance_name:  str = 'hil',
#     ):
#
#         start_time = self._calc_start_time()
#         self._wait_start_time(start_time=start_time)
#         self.time = start_time
#
#         # write first value to optimal control
#         # self._write_value(var_name='optimal_control', value=0)
#
#     def close(self):
#         """ Closes the simulation and clears the fmu object """
#
#         del self.last_df
#         self.last_df = pd.DataFrame()
#
#     def do_step(self):
#         """ Simulates one step """
#         # read Model_Time
#         with self.mqttClient:
#             model_time = self.mqttClient.subscribe(topic='hil2/Sim', var_name='Sim_Time')
#             continue_time = self.system_time + self.step_size
#
#             print(f'waiting for next step:')
#             while model_time < continue_time:
#                 model_time = self.mqttClient.subscribe(topic='hil2/Sim', var_name='Sim_Time')
#                 print(f'\r{model_time}/{continue_time}', end="")
#
#             print()  # new line
#         self.system_time += self.step_size
#
#     def _get_forecast(self, length: int) -> pd.DataFrame:
#         """ Returns forecast for the current prediction horizon """
#
#         return self.disturbances[
#             (self.system_time <= self.disturbances['SimTime']) &
#             (self.disturbances['SimTime'] <= (self.system_time + length))
#             ]
#
#     @property
#     def disturbances_filepath(self) -> str:
#         """ The filepath where the disturbances are stored """
#
#         return f'{self.directory_dist}//{self.name}_disturbances_{self.step_size}.pkl'
#
#     def load_disturbances(self):
#         """ Loads the disturbances DataFrame from the disc """
#
#         # check if the disturbances already exist.
#         if isfile(self.disturbances_filepath):
#             self.disturbances = read_pkl(filename=self.disturbances_filepath)
#         else:
#             raise FileNotFoundError(f'Disturbances missing at "{self.disturbances_filepath}".')
#
#     def get_variable_dict(self):
#         """
#         Returns a read and a send-dict with all needed variables.
#         """
#         ROOT_DIR = os.path.abspath(os.curdir)
#         path_send = f'{ROOT_DIR}\\stored_data\\variable_lists\\config_send.json'
#         path_read = f'{ROOT_DIR}\\stored_data\\variable_lists\\config_read.json'
#         return fwi_Utils.get_variable_dict(path_send), fwi_Utils.get_variable_dict(path_read)
#
#     # ------------ hil interaction ------------
#
#     def read_values(self) -> dict:
#         """ Reads current variable values and returns them as a dict """
#
#         res = dict()
#         for var_name in self._readable_columns:
#             res[var_name] = self._read_value(var_name)
#
#         # add current time to results
#         res['SimTime'] = self.system_time
#         return res
#
#     def _read_value(self, var_name: str):
#         """
#         Read a single variable.
#         """
#
#         # get topic and bucket from variable dict
#         topic = self.variable_dict_read[var_name]['topic']
#         bucket = self.variable_dict_read[var_name]['bucket']
#         shift = 0
#         if 'shift' in self.variable_dict_read[var_name]:
#             shift = self.variable_dict_read[var_name]['shift']
#
#         return self.httpsClient.read_data(topic=topic, var_name=var_name, bucket=bucket) + shift
#
#     def write_values(self, control_dict: dict):
#         """
#         Writes Values of control dict to mqtt
#         :param control_dict:
#         :return:
#         """
#
#         if control_dict:
#             for var_name, value in control_dict.items():
#                 self._write_value(var_name, value)
#             else:
#                 self._write_default_controls()
#
#     def _write_value(self, var_name: str, value):
#         """
#         Write a single control value
#         :param var_name: Name of the variable to write
#         :param value:  value to be written
#         :return:
#         """
#
#         # get topic from variable dict
#         topic = self.variable_dict_send[var_name]['topic']
#         with self.mqttClient:
#             self.mqttClient.publish_data(topic=topic, var_name=var_name, value=value)
#
#     def _calc_start_time(self):
#         """
#         calc SimTime where time is dividable by stepsize
#         """
#         with self.mqttClient:
#             model_time = self.mqttClient.subscribe(topic='hil2/Sim', var_name='Sim_Time')
#             start_time = model_time + (self.step_size - model_time % self.step_size)
#         return start_time
#
#     def _wait_start_time(self, start_time):
#         """
#         wait until SimTime is dividable by step size
#         """
#
#         with self.mqttClient:
#             model_time = self.mqttClient.subscribe(topic='hil2/Sim', var_name='Sim_Time')
#             print(f'wait until SimTime is dividable by step size ({start_time})')
#
#             while model_time < start_time:
#                 model_time = self.mqttClient.subscribe(topic='hil2/Sim', var_name='Sim_Time')
#                 print(f'\r{model_time}/{start_time}', end="")
#
#     def summary(self):
#         print(f'Experiment finished at {self.system_time}')

import pandas as pd
from os.path import isfile
import os
from pathlib import Path
from ddmpc.modeling.modeling import Model
from ddmpc.systems.base_class import System
from ddmpc.utils.pickle_handler import write_pkl, read_pkl
from ddmpc.utils.communication import mqttClient, httpsClient
import ddmpc.utils.fwi_Utils as fwi_Utils


class HiL(System):
    """
    This HiL class implements functionalities to interact with HIL
    """

    def __init__(
            self,
            model: Model,
            step_size:  int,
            name: str,
            https_client: httpsClient,
            mqtt_client: mqttClient,
            directory_dist: Path = Path('stored_data', 'Disturbances'),

    ):
        """
        initialize HIL System class
        :param step_size:       time step size of the fmu
        """

        # initialize parent class
        super().__init__(
            model=model,
            step_size=step_size,
            name=name,
        )

        self.directory_dist = directory_dist
        self.variable_dict_send, self.variable_dict_read = self.get_variable_dict()

        self.mqttClient = mqtt_client
        self.httpsClient = https_client

        # disturbances
        self.disturbances = None

        self.load_disturbances()

    def setup(
            self,
            start_time:     int,
            instance_name:  str = 'hil',
    ):

        start_time = self._calc_start_time()
        self._wait_start_time(start_time=start_time)
        self.time = start_time

        # write first value to optimal control
        # self._write_value(var_name='optimal_control', value=0)

    def close(self):
        """ Closes the simulation and clears the fmu object """

        del self.last_df
        self.last_df = pd.DataFrame()

    def do_step(self):
        """ Simulates one step """
        # read Model_Time
        with self.mqttClient:
            model_time = self.mqttClient.subscribe(topic='hil2/Sim', var_name='Sim_Time')
            continue_time = self.time + self.step_size

            print(f'waiting for next step:')
            while model_time < continue_time:
                model_time = self.mqttClient.subscribe(topic='hil2/Sim', var_name='Sim_Time')
                print(f'\r{model_time}/{continue_time}', end="")

            print()  # new line
        self.time += self.step_size

    def _get_forecast(self, length: int) -> pd.DataFrame:
        """ Returns forecast for the current prediction horizon """

        return self.disturbances[
            (self.time <= self.disturbances['SimTime']) &
            (self.disturbances['SimTime'] <= (self.time + length))
            ]

    @property
    def disturbances_filepath(self) -> str:
        """ The filepath where the disturbances are stored """

        return f'{self.directory_dist}//{self.name}_disturbances_{self.step_size}.pkl'

    def load_disturbances(self):
        """ Loads the disturbances DataFrame from the disc """

        # check if the disturbances already exist.
        if isfile(self.disturbances_filepath):
            self.disturbances = read_pkl(filename=self.disturbances_filepath)
        else:
            raise FileNotFoundError(f'Disturbances missing at "{self.disturbances_filepath}".')

    def get_variable_dict(self):
        """
        Returns a read and a send-dict with all needed variables.
        """
        ROOT_DIR = os.path.abspath(os.curdir)
        path_send = f'{ROOT_DIR}\\stored_data\\variable_lists\\config_send.json'
        path_read = f'{ROOT_DIR}\\stored_data\\variable_lists\\config_read.json'
        return fwi_Utils.get_variable_dict(path_send), fwi_Utils.get_variable_dict(path_read)

    # ------------ hil interaction ------------

    def read_values(self) -> dict:
        """ Reads current variable values and returns them as a dict """

        res = dict()
        for var_name in self._readable_columns:
            res[var_name] = self._read_value(var_name)

        # add current time to results
        res['SimTime'] = self.time
        return res

    def _read_value(self, var_name: str):
        """
        Read a single variable.
        """

        # get topic and bucket from variable dict
        topic = self.variable_dict_read[var_name]['topic']
        bucket = self.variable_dict_read[var_name]['bucket']
        shift = 0
        if 'shift' in self.variable_dict_read[var_name]:
            shift = self.variable_dict_read[var_name]['shift']

        return self.httpsClient.read_data(topic=topic, var_name=var_name, bucket=bucket) + shift

    def write_values(self, control_dict: dict):
        """
        Writes Values of control dict to mqtt
        :param control_dict:
        :return:
        """

        if control_dict:
            for var_name, value in control_dict.items():
                self._write_value(var_name, value)
        else:
            self._write_default_controls()

    def _write_value(self, var_name: str, value):
        """
        Write a single control value
        :param var_name: Name of the variable to write
        :param value:  value to be written
        :return:
        """

        # get topic from variable dict
        topic = self.variable_dict_send[var_name]['topic']
        with self.mqttClient:
            self.mqttClient.publish_data(topic=topic, var_name=var_name, value=value)

    def _calc_start_time(self):
        """
        calc SimTime where time is dividable by stepsize
        """
        with self.mqttClient:
            model_time = self.mqttClient.subscribe(topic='hil2/Sim', var_name='Sim_Time')
            start_time = model_time + (self.step_size - model_time % self.step_size)
        return start_time

    def _wait_start_time(self, start_time):
        """
        wait until SimTime is dividable by step size
        """

        with self.mqttClient:
            model_time = self.mqttClient.subscribe(topic='hil2/Sim', var_name='Sim_Time')
            print(f'wait until SimTime is dividable by step size ({start_time})')

            while model_time < start_time:
                model_time = self.mqttClient.subscribe(topic='hil2/Sim', var_name='Sim_Time')
                print(f'\r{model_time}/{start_time}', end="")

    def summary(self):
        print(f'Experiment finished at {self.time}')
