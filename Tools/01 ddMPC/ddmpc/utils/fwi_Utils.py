from ddmpc import *
import json
import pandas as pd


def load_elec_prices(load_new):

    disturbances = read_pkl(filename='Acoustic_FMU_nFan.fmu_disturbances_600.pkl', directory=r'D:\cve-fwi\git\data-driven-model-predictive-control\Examples\Acoustic_System\n_Fan_control\stored_data\FMUs')

    if load_new:

        df = pd.read_csv('D:\cve-fwi\data-driven-model-predictive-control\Examples\HiL_System_FMU\stored_data\Data\elec_prices.csv', delimiter=';')
        df = df[0:len(disturbances)]
        df = df['elec_prices']
        disturbances['elec_prices'] = df

        write_pkl(data=disturbances, filename='MPC_MPC_0Model.fmu_disturbances_600.pkl', directory='D:\cve-fwi\data-driven-model-predictive-control\Examples\HiL_System_FMU\stored_data\FMUs', override=True)


def load_timebased_costs(name, t_start, t_end, w_low, w_high=1):

    disturbances = read_pkl(filename='Acoustic_FMU_nFan.fmu_disturbances_600.pkl',
                            directory=r'D:\cve-fwi\git\data-driven-model-predictive-control\Examples\Acoustic_System\n_Fan_control\stored_data\FMUs')

    Time_Offset_2015 = 1420066800
    Time = [datetime.datetime.fromtimestamp(ts + Time_Offset_2015) for ts in disturbances['SimTime']]

    weights = list()

    for idx, time in enumerate(Time):
        if t_start <= time.hour <= t_end:
            weights.append(w_low)

        else:
            weights.append(w_high)

    disturbances[f'weights_{name}'] = weights

    write_pkl(data=disturbances, filename='Acoustic_FMU_nFan.fmu_disturbances_600.pkl',
              directory=r'D:\cve-fwi\git\data-driven-model-predictive-control\Examples\Acoustic_System\n_Fan_control\stored_data\FMUs',
              override=True)


def get_variable_dict(path):
    """
    Returns a df with all needed variables from TwinCat.
    """
    with open(
            path, 'r') as f:
        var_dict = json.load(f)
    return var_dict


def add_logistic_var_to_train_data(train_data, var_name, var_name_new):

    def logistic(x):
        return 1 / (1 + np.exp((-x+0.33) * 10))

    train_data.df[var_name_new] = logistic(train_data.df[var_name])

    return train_data


def add_changed_var_to_train_data(train_data, var_name, var_name_new):

    change = [train_data.df[var_name][idx+1] - train_data.df[var_name][idx] for idx, val in enumerate(train_data.df[var_name][0:-1])]
    change.insert(0, float("nan"))
    train_data.df[var_name_new] = change

    return train_data


def change_names(train_data):

    mapping = {'u_hp_logistic': 'Function(RelPower(u_hp),_f=logistic)',
               }


def comp_rps_to_comp_ao(train_data):

    def transform(x):
        return 1/90 * x

    train_data.df['Comp_AO_created'] = transform(train_data.df['StateMachine.rps_Comp'])

    return train_data


def create_artificial_points(train_data):

    art_points = pd.DataFrame(columns=['KK.Temp_KK', 'Comp_AO_created', 'IndoorUnit.T_CondIn_IDU', 'Leistung.Messung_1.Gesamtleistung'])
    values_T_KK = range(14, 18)
    values_Comp_AO = [element/100 for element in range(0, 33)]
    values_T_Cond = range(25, 30)
    values_P_el = [0]

    for i in range(100):
        art_points = art_points.append({'KK.Temp_KK': random.choice(values_T_KK),
                                        'Comp_AO_created': random.choice(values_Comp_AO),
                                        'IndoorUnit.T_CondIn_IDU': random.choice(values_T_Cond),
                                        'Leistung.Messung_1.Gesamtleistung': random.choice(values_P_el)},
                                       ignore_index=True)

    train_data = train_data.append(art_points, ignore_index=True)

    return train_data

