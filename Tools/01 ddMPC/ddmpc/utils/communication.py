import influxdb_client
import pandas as pd
from paho.mqtt.client import Client as PahoMQTTClient
from datetime import datetime, timedelta
import pytz
from ddmpc.data_handling.storing_data import DataContainer, DataHandler


def determine_timeshift():
    localtime = pytz.timezone('Europe/Berlin')
    a = localtime.localize(datetime.now())
    if a.dst:
        delta_t = -timedelta(hours=2)
    else:
        delta_t = -timedelta(hours=1)
    return delta_t


def shift_time_for_influx(time):
    if type(time) is str:
        time = datetime.strptime(time, '%d.%m.%Y %H:%M:%S')

    time = time + determine_timeshift()

    time = time.replace(microsecond=0).isoformat()
    time = f'{time}Z'
    return time


def get_time():
    time = datetime.now()
    return time


class httpsClient():

    def __init__(self,
                 org: str = 'EBC',
                 token: str = 'tMg2XUqTI8CSPKgQ5y9K5WVlvUyTF62n7XY4UwPgLtx1pj9UzNv_GxskQvdpFmWn9OzZmY09RjBOY-c1uQ31Lg==',
                 url: str = 'https://ebc-tick-stack.westeurope.cloudapp.azure.com:8086'
                 ):

        client = influxdb_client.InfluxDBClient(
            url=url,
            token=token,
            org=org
        )
        self.query_api = client.query_api()
        self.org = org

    def read_data(self, topic: str, var_name: str, bucket: str):

        query = f' from(bucket: "{bucket}")\
                  |> range(start: -15m)\
                  |> filter(fn: (r) => r["_measurement"] == "mqtt_consumer")\
                  |> filter(fn: (r) => r["_field"] == "value")\
                  |> filter(fn: (r) => r["topic"] == "{topic}/{var_name}")\
                  |> aggregateWindow(every: 1s, fn: last, createEmpty: false)'

        result = self.query_api.query(org=self.org, query=query)

        if len(result)==0 and var_name == "MPC.OptimalControl":
            result_value = 303.15
        else:
            result_value = result[0].records[-1].get_value()

        return result_value

    def get_timeseries(self, vars, start_time, end_time, interval):

        start_time = shift_time_for_influx(start_time)
        end_time = shift_time_for_influx(end_time)

        timeseries = pd.DataFrame()
        for var in vars:
            series = []
            topic = vars[var]['topic']
            bucket = vars[var]['bucket']
            query = f' from(bucket: "{bucket}")\
                      |> range(start: {start_time}, stop: {end_time})\
                      |> filter(fn: (r) => r["_measurement"] == "mqtt_consumer")\
                      |> filter(fn: (r) => r["topic"] == "{topic}/{var}")\
                      |> aggregateWindow(every: {interval}m, fn: last) '

            # |> filter(fn: (r) => r["_field"] == "value")\
            # |> filter(fn: (r) => r["host"] == "9f4a55938f26")\

            result = self.query_api.query(org=self.org, query=query)

            # get timestamp
            if 'timestamp' not in timeseries:
                for table in result:
                    for record in table.records:
                        series.append(record.get_time())
                timeseries['timestamp'] = series
                # account for timeshift in influx db
                timeseries['timestamp'] = timeseries['timestamp'] - determine_timeshift()

            series = []
            # get var data
            for table in result:
                for record in table.records:
                    series.append(record.get_value())

            if series:
                timeseries[var] = series

        # drop last row to keep 10 min interval
        timeseries = timeseries[:-1]
        dc = DataContainer(df=timeseries)
        return dc


class mqttClient:
    """
    Initializes Mqtt Client and provides functions to publish data
    """

    def __init__(self,
                 url: str = 'hil.ebc-team-kap.osc.eonerc.rwth-aachen.de'):
        """
        """

        self.ip = url
        self.mqtt = PahoMQTTClient(clean_session=True)
        self.payload = 1  # dummy
        self.received = False

    def __enter__(self):
        """
        Enter Connection and start mqtt loop
        :return:
        """
        self.mqtt.connect(self.ip, 1883, 60)
        self.mqtt.loop_start()

    def __exit__(self, exception_type, exception_value, traceback):
        """
        Exit communication and stop loop
        :return:
        """
        self.mqtt.loop_stop()
        # print('closed mqtt connection')

    def publish_data(self,
                     topic: str,
                     var_name: str,
                     value: str):

        topic = f'{topic}/{var_name}'
        self.mqtt.publish(topic, value)

    def on_message(self, client, userdata, msg):
        msg.payload = msg.payload.decode("utf-8")
        self.payload = float(msg.payload)
        self.received = True

    def subscribe(self,
                  topic: str,
                  var_name: str):

        topic = f'{topic}/{var_name}'
        self.mqtt.subscribe(topic=topic, qos=0)

        while not self.received:
            self.mqtt.on_message = self.on_message

        self.received = False
        return self.payload
