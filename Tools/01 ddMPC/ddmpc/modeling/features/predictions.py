""" Features of the type Prediction and Error are  used to store the predictions of the MPC. """

from ddmpc.modeling.features.sources import PlotOptions, Source, Constructed
import ddmpc.utils.formatting as fmt
import pandas as pd


class Prediction(Source):
    """ used to track the predictions made by the MPC """

    def __init__(
            self,
            feature: Source,
            k: int = 1,
    ):

        self.read_name = None
        assert k > 0, 'Please make sure k ist always greater or equal to 1!'

        super(Prediction, self).__init__(
            name=f'{self.__class__.__name__}({feature.source.name}, k={k})',
            plt_opts=PlotOptions(color=fmt.black, line=fmt.line_dotted),
        )

        # feature to predict
        self.feature = feature

        # future steps
        self.k = k

    def __getitem__(self, i: int):
        """ instead of creating 'own' variables they must be accessed through the base feature """

        return self.feature[i]

    @property
    def col_name(self) -> str:

        return f'Prediction({self.feature.name}, k={self.k})'

    @property
    def sub_features(self) -> list:
        return [self.feature]

    def update(self, df: pd.DataFrame, idx: int) -> pd.DataFrame:
        """Calculates the Constructed Source for only one row"""

        # Readable sources are red and therefore must not be calculated.

        return df

    def process(self, df: pd.DataFrame) -> pd.DataFrame:
        """Calculates the Constructed Source for all rows"""

        # Readable sources are red and therefore must not be calculated.

        return df

    @property
    def subs(self) -> list[Source]:
        """Returns a list with all Source's that are required to calculate this Source"""

        # Readable Sources are red from the system and therefore an empty list is returned.

        return list()

class Predictions(Source):
    """ used to track multiple predictions made by the mpc """

    def __init__(
            self,
            feature:    Source,
            N:          int,
    ):
        # feature to save the predictions from
        self.feature = feature

        self.predictions = [Prediction(feature=feature, k=k) for k in range(1, N+1)]

        # default plot options
        plt_opts = PlotOptions(
            color=fmt.black,
            line=fmt.line_solid,
        )

        super().__init__(
            name=f'{self.__class__.__name__}({self.feature}, N={N})',
            plt_opts=plt_opts,
        )

    def __getitem__(self, i: int) -> Prediction:
        return self.predictions[i]

    def col_name(self) -> str:
        return self.name


    @property
    def sub_features(self):
        return self.predictions

    def statistics(self, df: pd.DataFrame) -> dict:

        stats = dict()

        for prediction in self.predictions:
            stats[prediction] = prediction.statistics(df)

        return stats

    def update(self, df: pd.DataFrame, idx: int) -> pd.DataFrame:
        """Calculates the Constructed Source for only one row"""

        # Readable sources are red and therefore must not be calculated.

        return df

    def process(self, df: pd.DataFrame) -> pd.DataFrame:
        """Calculates the Constructed Source for all rows"""

        # Readable sources are red and therefore must not be calculated.

        return df

    @property
    def subs(self) -> list[Source]:
        """Returns a list with all Source's that are required to calculate this Source"""

        # Readable Sources are red from the system and therefore an empty list is returned.

        return list()
class Error(Constructed):
    """ used to track the error of the predictions """

    def __init__(
            self,
            prediction: Prediction,
            plt_opts: PlotOptions = None,
    ):
        if plt_opts is None:
            plt_opts = PlotOptions(color=fmt.black, line=fmt.line_solid)

        super().__init__(
            name=f'{self.__class__.__name__}({prediction.name})',
            plt_opts=plt_opts,
        )

        self.prediction = prediction

    @property
    def col_name(self) -> str:
        return self.name

    def process(self, df: pd.DataFrame) -> pd.DataFrame:
        # define the new feature
        col_loc = df.columns.get_loc(self.prediction.col_name) + 1  # next column
        col_name = self.col_name
        col_value = df[self.prediction.col_name] - df[self.prediction.feature.col_name].shift(-self.prediction.k)

        # insert the feature
        df.insert(loc=col_loc, column=col_name, value=col_value)

        return df

    def get_constraint(self, k: int):
        return None

    @property
    def sub_features(self) -> list:
        return [self.prediction]


class Errors(Source):
    """ used to track errors for multiple predictions """

    def __init__(
            self,
            predictions: Predictions,
            plt_opts: PlotOptions = None,
    ):

        if plt_opts is None:
            plt_opts = PlotOptions(color=fmt.black, line=fmt.line_solid)

        super().__init__(
            name=f'{self.__class__.__name__}({predictions.name})',
            plt_opts=plt_opts,
        )

        self.prediction_errors = [Error(prediction=prediction) for prediction in predictions.predictions]

    def __getitem__(self, i: int) -> Error:
        return self.prediction_errors[i]

    @property
    def col_name(self) -> str:
        return self.name

    @property
    def sub_features(self) -> list:
        return self.prediction_errors

    def statistics(self, df: pd.DataFrame) -> dict:

        stats = dict()

        for prediction in self.prediction_errors:
            stats[prediction.name] = prediction.statistics(df)

        return stats

    def plot(self, df: pd.DataFrame):

        import matplotlib.pyplot as plt
        from matplotlib.ticker import MultipleLocator

        stats = dict()

        for prediction_error in self.prediction_errors:

            stats[prediction_error] = prediction_error.statistics(df)

        abs_mean = [dct['abs_mean'] for dct in stats.values()]
        std = [dct['std'] for dct in stats.values()]

        plt.title(
            f'Prediction Error of\n{self.prediction_errors[0].prediction.feature.name}'
        )

        x = range(1, len(self.prediction_errors)+1)

        plt.plot(x, abs_mean, c=fmt.blue, label='Absolute Mean')
        plt.plot(x, std, c=fmt.red, label='Standard Deviation')
        plt.legend()
        plt.xlabel('k')
        plt.ylim(0, 0.2)

        ax = plt.gca()
        ax.xaxis.set_major_locator(MultipleLocator(1))

        plt.show()
