# from ddmpc.utils import evaluate_results
from ddmpc.utils.communication import httpsClient, mqttClient
from ddmpc.utils.fwi_Utils import *
from ddmpc.data_handling.storing_data import DataContainer
import pandas as pd

Client = httpsClient()

variables = get_variable_dict(path=r"D:\lma-dtu TEMP\masterarbeit\Tools\01 ddMPC\Examples\FMUs\StandardCase\stored_data\variable_lists\read_data.json")

data = Client.get_timeseries(start_time='29.10.2023 13:36:00', end_time='30.10.2023 13:38:00', vars=variables, interval=1)
data = data.df
data = data.set_index(pd.date_range("2015-04-07 00:00:00", periods=len(data), freq="1min").to_series())
# data = data.fillna(method='ffill')
# data = data.fillna(method='bfill')
# data = pd.concat([data1[pd.to_datetime('2015-04-19 00:00:00'):pd.to_datetime('2015-04-19 22:54:00')], data[pd.to_datetime('2015-04-19 22:55:00'):pd.to_datetime('2015-04-20 00:00:00')]], axis=0)
rename={
    "timestamp":"Realzeit",
    "KK.Temp_KK":"Umgebungstemperatur",
    "StateMachine.rps_Comp": "Drehzahl",
    "IndoorUnit.T_FlowOutlet_IDU": "Vorlauftemperatur",
    "MPC.OptimalControl":"Vorlaufsolltemperatur",
    "IndoorUnit.T_CondIn_IDU": "Ruecklauftemperatur",
    "outputs.building.TZone[1]": "Zonentemperatur",
    "Sim_Time": "Simulationszeit",
    "weaDat.weaBus.HDirNor": "Sonneneinstrahlung",
    "weaDat.weaBus.TDryBul": "Sollumgebungstemperatur",
    "TZoneSet": "Zonensolltemperatur",
    "TError": "TError",
    "TFussboden": "Fussbodentemperatur",
    "StateMachine.Pel_Inv": "ElektrischeLeistung",
    "StateMachine.delta_H_Cond": "Heizleistung"
}
data = data.rename(columns=rename)
data['ElektrischeLeistung'] =data['ElektrischeLeistung']*1000
data['ElektrischeEnergie'] = data['ElektrischeLeistung'].cumsum()*60
data['Heizenergie'] = data['Heizleistung'].cumsum()*60

data.to_csv("ampc_exp_7Apr.csv")

# ddmpc_new = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\01 ddMPC\Examples\FMUs\StandardCase\ddmpc_exp_18Nov_new.csv", index_col=0)
# ddmpc_old = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\01 ddMPC\Examples\FMUs\StandardCase\ddmpc_exp_18Nov_old.csv", index_col=0)
#
# ddmpc= pd.concat([ddmpc_new["2014-11-18 00:00:00":"2014-11-18 06:30:00"], ddmpc_old["2014-11-18 06:30:00":"2014-11-19 00:00:00"]], axis=0)
# ddmpc['ElektrischeEnergie'] = ddmpc['ElektrischeLeistung'].cumsum()*60
# ddmpc['Heizenergie'] = ddmpc['Heizleistung'].cumsum()*60
# ddmpc.to_csv("ddmpc_exp_18Nov.csv")

