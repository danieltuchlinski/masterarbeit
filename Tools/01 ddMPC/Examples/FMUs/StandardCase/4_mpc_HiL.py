from Examples.FMUs.StandardCase.config_HiL import *

""" Choose the process models """
TAirZone_predictor = load_NeuralNetwork("TAirZone_ann_PI_Controller_updated")

""" Set the comfort boundaries """
TAirZone.mode = Economic(
    day_start=8,  # Time to activate the daytime boundaries
    day_end=17,  # Time to activate the night time boundaries
    day_lb=273.15 + 17,
    day_ub=273.15 + 25,
    night_lb=273.15 + 20,
    night_ub=273.15 + 22,
    weekend=True,  # Considers the night time constraints for weekends if true
    time_offset=1410303600,  # UNIX Time Offset (relevant for real-life systems)
)

""" Initialize Model Predictive Controller """
HiL_MPC = ModelPredictive(
    step_size=one_minute * 10,
    nlp=NLP(
        model=HiL_Model,
        N=36,
        objectives=[
            Objective(feature=T_VL_Set, cost=Linear(weight=0.1*1/300)),
            Objective(feature=T_VL_Set_change, cost=Quadratic(weight=0.005*1/30*50)), # 0.005*1/30*50
            Objective(feature=TAirZone, cost=Quadratic(weight=10)),
        ],
        constraints=[
            Constraint(feature=T_VL_Set, lb=273.15+20, ub=273.15+40),
        ],
    ),
    forecast_callback=HiL_System.get_forecast,
)

HiL_System.setup(start_time=one_day*69)

# Load initial data to consider for retraining
dh = DataHandler()

HiL_MPC.nlp.build(
        solver_options={"verbose": True, "ipopt.print_level": 1, "ipopt.max_iter": 200, "expand": True},
        predictors=[TAirZone_predictor, ],
    )

online_data = HiL_System.run(  # run system with MPC for desired time
    controllers=[HiL_MPC],
    duration=one_hour * 24,
)

dh.add(online_data)

dh.save("hil_18thNovember", override=True) # save results

