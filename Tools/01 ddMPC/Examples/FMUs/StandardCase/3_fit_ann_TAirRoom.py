from Examples.FMUs.StandardCase.config import *
from keras.callbacks import EarlyStopping

"""
Train an ANN to learn the Temperature change using the generated training data
"""

# load data
pid_winter = load_DataHandler("pid_winter_PI_Controller_updated")
pid_spring = load_DataHandler("pid_spring_PI_Controller_updated")
pid_fall = load_DataHandler("pid_fall_PI_Controller_updated")

# Add data to training data class, shuffle and split
TAirZone_TrainingData.add(pid_winter)
TAirZone_TrainingData.add(pid_spring)
TAirZone_TrainingData.add(pid_fall)
TAirZone_TrainingData.shuffle()
TAirZone_TrainingData.split(trainShare=0.7, validShare=0.15, testShare=0.15)

# define tuner model for hyperparameter optimization
TAirZone_TunerModel = TunerModel(
    TunerBatchNormalizing(),
    TunerDense(units=(16,), activations=('softsign',)),  # Train networks with 4 or 8 neurons
    # TunerDense(units=(8, 10, 12, 14, 16,)),
    name="TAirZone_TunerModel",
)

trainer = NetworkTrainer()
trainer.build(
    n=5, keras_tuner=TAirZone_TunerModel
)  # Train n different networks with the defined configuration

trainer.fit( # pass training data and training parameters
    training_data=TAirZone_TrainingData,
    epochs=5, #1000
    batch_size=10, #100
    verbose=1,
    # callbacks=[EarlyStopping(patience=50, verbose=1)],
)
trainer.eval(training_data=TAirZone_TrainingData, show_plot=True, save_result=True, print_result=True)

trainer.best.save("TAirZone_ann_PI_Controller_updated_working", override=True) # save best network
