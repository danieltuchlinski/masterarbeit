from Examples.FMUs.StandardCase.config import *

""" 
This script is used to generate the necessary training data
"""

# TAirZone.mode = Random(# Set the Air temperature mode to random for identification
#     day_start=8,
#     day_end=17,
#     day_lb=273.15 + 17,
#     night_lb=273.15 + 20,
#     day_ub=273.15 + 25,
#     night_ub=273.15 + 22,
#     interval=60 * 60 * 4,   # change set point after interval
#     time_offset=1410303600,
# )
TAirZone.mode = Identification(# Set the Air temperature mode to random for identification
    day_start=8,
    day_end=17,
    day_lb=273.15 + 17,
    night_lb=273.15 + 20,
    day_ub=273.15 + 25,
    night_ub=273.15 + 22,
    time_offset=1410303600,
)

Data_PID = PID(
    y=TAirZone,
    u=T_VL_Set,
    step_size=60 * 10,
    Kp=1,
    Ti=600,
    Td=0,
)

# Simulate system for given time using the defined base controllers
# -----------------------------------------------------------------------------------------
system.setup(start_time=one_day * 31)
system.define_control_input({'b_MPC_input': True, 'b_u_comp_ctrl': False, })

pid_fall = DataHandler(
    [
        system.run(controllers=(Data_PID, ), duration=one_week*3),
    ]
)

system.close()
# ----------------------------------------------------------------------------------------
system.setup(start_time=one_day * 100)
system.define_control_input({'b_MPC_input': True, 'b_u_comp_ctrl': False, })

pid_winter = DataHandler(
    [
        system.run(controllers=(Data_PID, ), duration=one_week*3),
    ]
)

system.close()
# ----------------------------------------------------------------------------------------
system.setup(start_time=one_day*185)
system.define_control_input({'b_MPC_input': True, 'b_u_comp_ctrl': False, })

pid_spring = DataHandler(
    [
        system.run(controllers=(Data_PID, ), duration=one_week*3),
    ]
)

system.close()
# ----------------------------------------------------------------------------------------
# Plot Training data
pid_plotter.plot(df=pid_winter.containers[0].df, show_plot=True, save_plot=True, filepath=r'stored_data\Plots\pid_winter_PI_Controller_updated_id')
pid_plotter.plot(df=pid_spring.containers[0].df, show_plot=True, save_plot=True, filepath=r'stored_data\Plots\pid_spring_PI_Controller_updated_id')
pid_plotter.plot(df=pid_fall.containers[0].df, show_plot=True, save_plot=True, filepath=r'stored_data\Plots\pid_fall_PI_Controller_updated_id')

# Save training data
pid_winter.save("pid_winter_PI_Controller_updated_id", override=True)
pid_spring.save("pid_spring_PI_Controller_updated_id", override=True)
pid_fall.save("pid_fall_PI_Controller_updated_id", override=True)
