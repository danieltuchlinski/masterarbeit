from Examples.FMUs.StandardCase.config import *

""" Choose the process models """
TAirZone_predictor = load_NeuralNetwork("TAirZone_ann_PI_Controller_updated")

features = []
for i in model.readable:
    features.append(i.read_name)

""" Set the comfort boundaries """
TAirZone.mode = Economic(
    day_start=8,  # Time to activate the daytime boundaries
    day_end=17,  # Time to activate the night time boundaries
    day_lb=273.15 + 17,
    day_ub=273.15 + 25,
    night_lb=273.15 + 20,
    night_ub=273.15 + 22,
    weekend=True,  # Considers the night time constraints for weekends if true
    time_offset=1410303600,  # UNIX Time Offset (relevant for real-life systems)
)

""" Initialize Model Predictive Controller """
ThermalZone_MPC = ModelPredictive(
    step_size=one_minute * 10,
    nlp=NLP(
        model=model,
        N=36,
        objectives=[
            Objective(feature=T_VL_Set, cost=Linear(weight=0.1*1/300)),
            Objective(feature=T_VL_Set_change, cost=Quadratic(weight=0.005*1/30*50)), # 0.005*1/30*50
            Objective(feature=TAirZone, cost=Quadratic(weight=10)),
        ],
        constraints=[
            Constraint(feature=T_VL_Set, lb=273.15+20, ub=273.15+40),
        ],
    ),
    forecast_callback=system.get_forecast,
)

system.setup(start_time=(one_day*0))

# Load initial data to consider for retraining
dh = load_DataHandler("pid_winter_PI_Controller_updated")

ThermalZone_MPC.nlp.build(  # build nlp with (re-) trained models
    solver_options={"verbose": False, "ipopt.print_level": 0,'expand':True},
    predictors=[TAirZone_predictor],
)

system.define_control_input({'b_MPC_input': True, 'b_u_comp_ctrl': False, })
online_data = system.run(  # run system with MPC for desired time
    controllers=[ThermalZone_MPC],
    duration=one_day * 252, # 252
)

dh.add(DataContainer(features))
dh.add(DataContainer([system.step_size]))
dh.add(DataContainer([datetime.datetime.fromtimestamp(System.display_time_offset).strftime(System.display_time_format)]))
dh.add(online_data)

mpc_plotter.plot(dh.containers[-1].df, show_plot=True)

dh.save("PI_Controller_updated_testing", override=True) # save results
system.close()

########################################################################################################################
# AddMo Readifier
########################################################################################################################

# Import pickles-files
data = open(r"D:\lma-dtu TEMP\masterarbeit\Tools\01 ddMPC\Examples\FMUs\StandardCase\stored_data\data\PI_Controller_updated_testing.pkl", "rb")
mpc_data = pickle.load(data)
# Extract timeseries data
df = pd.DataFrame(mpc_data.containers[-1].df)

########################################################
###                 User Inputs                      ###
########################################################
# Add control variable
control_variable = 'u_T_VL'

# Disturbances with prediction horizon
predictions = {
    'weaDat.weaBus.TDryBul': [6, 12, 18, 24, 30, 36],
    'weaDat.weaBus.HDirNor': [6, 12, 18, 24, 30, 36],
    'LowerBound(Zone Temperature)': [6, 12, 18, 24, 30, 36],
}
# Insert start dats for periods
start_date = '09.10.2014 - 00:00'
# Delete specific columns
df = df.drop(columns=['SimTime', 'runtime'])

##########################################################
##########################################################
##########################################################

# Delete unnecessary columns (with strings and Nans)
predictions_mpc = df[["predictions_TZone", "predictions_TVL"]].fillna(0)
df = df.dropna(axis='columns')
df = df._get_numeric_data()
df = pd.concat([df, predictions_mpc], axis=1)
# Get column names/potential features
features1 = list(df.columns)

# Get step length
frequency = str(mpc_data.containers[2].df[0])+'s'

# Set index for dataframes with dates and calculate gap times
df = df.set_index(pd.date_range(start_date, periods=len(df), freq=frequency))
predictions_mpc = predictions_mpc.set_index(pd.date_range(start_date, periods=len(df), freq=frequency))
# Initialize feature lists
feature = list()

# Append '[]' to each feature
for i in range(len(features1)):
    feature.append(features1[i]+' []')
    df = df.rename(columns={features1[i]: feature[i]})

# Move column with control variable one row below
df[control_variable+' []'] = df[control_variable+' []'].shift(1)

# Delete first row
df[control_variable+' []'][0] = df[control_variable+' []'][1]

# Insert columns for lags
for prediction in predictions:
    for lag in predictions[prediction]:
        df[prediction+'_fc'+str(lag)+' []'] = df[prediction+' []'].shift(-lag)

# Delete all rows with nans
df = df.dropna()

# Write data to excel for each period
df.to_csv(r'D:\lma-dtu TEMP\masterarbeit\Tools\Results\ddmpc.csv')
df = df.drop(columns=["predictions_TZone []", "predictions_TVL []"])
df.to_excel(r'D:\lma-dtu TEMP\masterarbeit\Tools\02 AddMo\02_Tool\Data\InputData.xlsx')







