from Examples.FMUs.StandardCase.config import *
import time
""" Choose the process models """
TAirZone_predictor = load_NeuralNetwork("TAirZone_ann")
# Power_predictor = load_GaussianProcess("Power_gpr")

features = []
for i in model.readable:
    features.append(i.read_name)

""" Set the comfort boundaries """
TAirZone.mode = Economic(
    day_start=8,  # Time to activate the daytime boundaries
    day_end=17,  # Time to activate the night time boundaries
    day_lb=273.15 + 17,
    day_ub=273.15 + 25,
    night_lb=273.15 + 20,
    night_ub=273.15 + 22,
    weekend=True,  # Considers the night time constraints for weekends if true
    time_offset=1410300000,  # UNIX Time Offset (relevant for real-life systems)
)

weights1 = [1,]
weights2 = [0.001, 0.01, 0.1, 1, 10, 100, 1000,]
weights3 = [0.001, 0.01, 0.1, 1, 10, 100, 1000,]

for weight1 in weights1:
    for weight2 in weights2:
        for weight3 in weights3:
            """ Initialize Model Predictive Controller """
            ThermalZone_MPC = ModelPredictive(
                step_size=one_minute * 10,
                nlp=NLP(
                    model=model,
                    N=36,
                    objectives=[
                        Objective(feature=T_VL_Set, cost=Linear(weight=weight1*1/300)),
                        Objective(feature=T_VL_Set_change, cost=Quadratic(weight=weight2*1/30)),
                        Objective(feature=TAirZone, cost=Quadratic(weight=10*weight3)),

                    ],
                    constraints=[
                        Constraint(feature=T_VL_Set, lb=273.15+20, ub=273.15+40),
                    ],
                ),
                forecast_callback=system.get_forecast,
            )

            #Fall
            system.setup(start_time=one_day*51)
            system.define_control_input({'b_MPC_input': False, 'b_u_comp_ctrl': False, })
            system.run(
                duration=one_day*1
            )  # run the Simulation for one day without controller to settle the system

            # Load initial data to consider for retraining
            dh = load_DataHandler("pid_fall")

            ThermalZone_MPC.nlp.build(  # build nlp with (re-) trained models
                solver_options={"verbose": False, "ipopt.print_level": 0,'expand':True},
                predictors=[TAirZone_predictor],
                # predictors=[TAirZone_predictor, Power_predictor],
            )

            system.define_control_input({'b_MPC_input': True, 'b_u_comp_ctrl': False, })
            online_data = system.run(  # run system with MPC for desired time
                controllers=[ThermalZone_MPC],
                duration=one_day * 14,
            )

            dh.add(DataContainer(features))
            dh.add(DataContainer([system.step_size]))
            dh.add(DataContainer([datetime.datetime.fromtimestamp(System.display_time_offset).strftime(System.display_time_format)]))
            dh.add(online_data)

            mpc_plotter.plot(dh.containers[-1].df, show_plot=True)

            name = "fall_"+str(weight1)+"_"+str(weight2)+"_"+str(weight3)
            dh.save(name, override=True) # save results
            system.close()

            #Winter
            system.setup(start_time=one_day * 157)
            system.define_control_input({'b_MPC_input': False, 'b_u_comp_ctrl': False, })
            system.run(
                duration=one_day * 1
            )  # run the Simulation for one day without controller to settle the system

            # Load initial data to consider for retraining
            dh = load_DataHandler("pid_winter")

            ThermalZone_MPC.nlp.build(  # build nlp with (re-) trained models
                solver_options={"verbose": False, "ipopt.print_level": 0, 'expand': True},
                predictors=[TAirZone_predictor],
                # predictors=[TAirZone_predictor, Power_predictor],
            )

            system.define_control_input({'b_MPC_input': True, 'b_u_comp_ctrl': False, })
            online_data = system.run(  # run system with MPC for desired time
                controllers=[ThermalZone_MPC],
                duration=one_day * 14,
            )

            dh.add(DataContainer(features))
            dh.add(DataContainer([system.step_size]))
            dh.add(DataContainer(
                [datetime.datetime.fromtimestamp(System.display_time_offset).strftime(System.display_time_format)]))
            dh.add(online_data)

            mpc_plotter.plot(dh.containers[-1].df, show_plot=True)

            name = "winter_" + str(weight1) + "_" + str(weight2) + "_" + str(weight3)
            dh.save(name, override=True)  # save results
            system.close()

            # Spring
            system.setup(start_time=one_day * 193)
            system.define_control_input({'b_MPC_input': False, 'b_u_comp_ctrl': False, })
            system.run(
                duration=one_day * 1
            )  # run the Simulation for one day without controller to settle the system

            # Load initial data to consider for retraining
            dh = load_DataHandler("pid_spring")

            ThermalZone_MPC.nlp.build(  # build nlp with (re-) trained models
                solver_options={"verbose": False, "ipopt.print_level": 0, 'expand': True},
                predictors=[TAirZone_predictor],
                # predictors=[TAirZone_predictor, Power_predictor],
            )

            system.define_control_input({'b_MPC_input': True, 'b_u_comp_ctrl': False, })
            online_data = system.run(  # run system with MPC for desired time
                controllers=[ThermalZone_MPC],
                duration=one_day * 14,
            )

            dh.add(DataContainer(features))
            dh.add(DataContainer([system.step_size]))
            dh.add(DataContainer(
                [datetime.datetime.fromtimestamp(System.display_time_offset).strftime(System.display_time_format)]))
            dh.add(online_data)

            mpc_plotter.plot(dh.containers[-1].df, show_plot=True)

            name = "spring_" + str(weight1) + "_" + str(weight2) + "_" + str(weight3)
            dh.save(name, override=True)  # save results
            system.close()
