import pandas as pd
import os
import matplotlib.pyplot as plt

# Colors
hellhellrotebc = (248/255,217/255,213/255)
hellrotebc = (241/255,179/255,171/255)
hellerrotebc = (235/255,140/255,129/255)
rotebc = (221/255,64/255,45/255)
dunkelrotebc = (172/255,43/255,28/255)
dunkeldunkelrotebc = (114/255,29/255,19/255)
dunkeldunkeldunkelrotebc = (76/255,17/255,12/255)
hellhellhellgrauebc = (242/255,242/255,242/255)
hellhellgrauebc = (235/255,236/255,236/255)
hellgrauebc = (216/255,216/255,217/255)
hellergrauebc = (196/255,197/255,198/255)
grauebc = (157/255,158/255,160/255)
dunkelgrauebc = (117/255,118/255,121/255)
dunkeldunkelgrauebc = (81/255,84/255,83/255)

folder = "PI_Controller_updated"

ampc = pd.read_csv(os.path.join("D:\lma-dtu TEMP\masterarbeit\Tools\Results", folder, "Simulation_files/HeatingPeriod_1min_relevant/ampc_relevant.csv"), index_col=0, sep=",")
ddmpc = pd.read_csv(os.path.join("D:\lma-dtu TEMP\masterarbeit\Tools\Results", folder, "Simulation_files/HeatingPeriod_1min_relevant/ddmpc_relevant.csv"), index_col=0, sep=",")
hc = pd.read_csv(os.path.join("D:\lma-dtu TEMP\masterarbeit\Tools\Results", folder, "Simulation_files/HeatingPeriod_1min_relevant/hc_relevant.csv"), index_col=0, sep=",")

startdate = "2014-10-01 00:00:00"
enddate = "2015-05-01 00:00:00"
A_building = 158

KPIs = pd.DataFrame(index=["Switches pro Tag", "El. Energie [kWh]", "Temp.-abweichung [Kh]", "Th. Energie [kWh]", "SCOP", "Spez. Wärme [kWh/m²]"], columns=["aMPC", "ddMPC", "Heizkurve"])

# # AMPC
# KPIs["aMPC"]["Switches pro Tag"] = (ampc.loc[enddate]["hydraulic.generation.outBusGen.heaPum.numSwi"]-ampc.loc[startdate]["hydraulic.generation.outBusGen.heaPum.numSwi"])/212
# KPIs["aMPC"]["El. Energie [kWh]"] = (ampc.loc[enddate]["outputs.hydraulic.gen.PEleHP.integral"]-ampc.loc[startdate]["outputs.hydraulic.gen.PEleHP.integral"])/1000/3600
# KPIs["aMPC"]["Temp.-abweichung [Kh]"] = ampc["TError"][startdate:enddate].mask(ampc["TError"][startdate:enddate] > 0, 0).abs().sum()/6
# KPIs["aMPC"]["Th. Energie [kWh]"] = (ampc.loc[enddate]["outputs.hydraulic.gen.QHP_flow.integral"]-ampc.loc[startdate]["outputs.hydraulic.gen.QHP_flow.integral"])/1000/3600
# KPIs["aMPC"]["SCOP"] = KPIs["aMPC"]["Th. Energie [kWh]"]/KPIs["aMPC"]["El. Energie [kWh]"]
# KPIs["aMPC"]["Spez. Wärme [kWh/m²]"] = KPIs["aMPC"]["Th. Energie [kWh]"]/A_building
#
# # DDMPC
# KPIs["ddMPC"]["Switches pro Tag"] = (ddmpc.loc[enddate]["hydraulic.generation.outBusGen.heaPum.numSwi []"]-ddmpc.loc[startdate]["hydraulic.generation.outBusGen.heaPum.numSwi []"])/212
# KPIs["ddMPC"]["El. Energie [kWh]"] = (ddmpc.loc[enddate]["outputs.hydraulic.gen.PEleHP.integral []"]-ddmpc.loc[startdate]["outputs.hydraulic.gen.PEleHP.integral []"])/1000/3600
# KPIs["ddMPC"]["Temp.-abweichung [Kh]"] = ddmpc["TError []"][startdate:enddate].mask(ddmpc["TError []"][startdate:enddate] > 0, 0).abs().sum()/6
# KPIs["ddMPC"]["Th. Energie [kWh]"] = (ddmpc.loc[enddate]["outputs.hydraulic.gen.QHP_flow.integral []"]-ddmpc.loc[startdate]["outputs.hydraulic.gen.QHP_flow.integral []"])/1000/3600
# KPIs["ddMPC"]["SCOP"] = KPIs["ddMPC"]["Th. Energie [kWh]"]/KPIs["ddMPC"]["El. Energie [kWh]"]
# KPIs["ddMPC"]["Spez. Wärme [kWh/m²]"] = KPIs["ddMPC"]["Th. Energie [kWh]"]/A_building
#
# # Heating Curve
# KPIs["Heizkurve"]["Switches pro Tag"] = (hc.loc[enddate]["hydraulic.generation.outBusGen.heaPum.numSwi"]-hc.loc[startdate]["hydraulic.generation.outBusGen.heaPum.numSwi"])/212
# KPIs["Heizkurve"]["El. Energie [kWh]"] = (hc.loc[enddate]["outputs.hydraulic.gen.PEleHP.integral"]-hc.loc[startdate]["outputs.hydraulic.gen.PEleHP.integral"])/1000/3600
# KPIs["Heizkurve"]["Temp.-abweichung [Kh]"] = hc["TError"][startdate:enddate].mask(hc["TError"][startdate:enddate] > 0, 0).abs().sum()/6
# KPIs["Heizkurve"]["Th. Energie [kWh]"] = (hc.loc[enddate]["outputs.hydraulic.gen.QHP_flow.integral"]-hc.loc[startdate]["outputs.hydraulic.gen.QHP_flow.integral"])/1000/3600
# KPIs["Heizkurve"]["SCOP"] = KPIs["Heizkurve"]["Th. Energie [kWh]"]/KPIs["Heizkurve"]["El. Energie [kWh]"]
# KPIs["Heizkurve"]["Spez. Wärme [kWh/m²]"] = KPIs["Heizkurve"]["Th. Energie [kWh]"]/A_building

# AMPC
KPIs["aMPC"]["Switches pro Tag"] = (ampc.loc[enddate]["Switches"]-ampc.loc[startdate]["Switches"])/212
KPIs["aMPC"]["El. Energie [kWh]"] = (ampc.loc[enddate]["ElektrischeEnergie"]-ampc.loc[startdate]["ElektrischeEnergie"])/1000/3600
KPIs["aMPC"]["Temp.-abweichung [Kh]"] = ampc["TError"][startdate:enddate].mask(ampc["TError"][startdate:enddate] > 0, 0).abs().sum()/60
KPIs["aMPC"]["Th. Energie [kWh]"] = (ampc.loc[enddate]["Heizenergie"]-ampc.loc[startdate]["Heizenergie"])/1000/3600
KPIs["aMPC"]["SCOP"] = KPIs["aMPC"]["Th. Energie [kWh]"]/KPIs["aMPC"]["El. Energie [kWh]"]
KPIs["aMPC"]["Spez. Wärme [kWh/m²]"] = KPIs["aMPC"]["Th. Energie [kWh]"]/A_building

# DDMPC
KPIs["ddMPC"]["Switches pro Tag"] = (ddmpc.loc[enddate]["Switches"]-ddmpc.loc[startdate]["Switches"])/212
KPIs["ddMPC"]["El. Energie [kWh]"] = (ddmpc.loc[enddate]["ElektrischeEnergie"]-ddmpc.loc[startdate]["ElektrischeEnergie"])/1000/3600
KPIs["ddMPC"]["Temp.-abweichung [Kh]"] = ddmpc["TError"][startdate:enddate].mask(ddmpc["TError"][startdate:enddate] > 0, 0).abs().sum()/60
KPIs["ddMPC"]["Th. Energie [kWh]"] = (ddmpc.loc[enddate]["Heizenergie"]-ddmpc.loc[startdate]["Heizenergie"])/1000/3600
KPIs["ddMPC"]["SCOP"] = KPIs["ddMPC"]["Th. Energie [kWh]"]/KPIs["ddMPC"]["El. Energie [kWh]"]
KPIs["ddMPC"]["Spez. Wärme [kWh/m²]"] = KPIs["ddMPC"]["Th. Energie [kWh]"]/A_building

# Heating Curve
KPIs["Heizkurve"]["Switches pro Tag"] = (hc.loc[enddate]["Switches"]-hc.loc[startdate]["Switches"])/212
KPIs["Heizkurve"]["El. Energie [kWh]"] = (hc.loc[enddate]["ElektrischeEnergie"]-hc.loc[startdate]["ElektrischeEnergie"])/1000/3600
KPIs["Heizkurve"]["Temp.-abweichung [Kh]"] = hc["TError"][startdate:enddate].mask(hc["TError"][startdate:enddate] > 0, 0).abs().sum()/60
KPIs["Heizkurve"]["Th. Energie [kWh]"] = (hc.loc[enddate]["Heizenergie"]-hc.loc[startdate]["Heizenergie"])/1000/3600
KPIs["Heizkurve"]["SCOP"] = KPIs["Heizkurve"]["Th. Energie [kWh]"]/KPIs["Heizkurve"]["El. Energie [kWh]"]
KPIs["Heizkurve"]["Spez. Wärme [kWh/m²]"] = KPIs["Heizkurve"]["Th. Energie [kWh]"]/A_building

types = ['Heizkurve', 'ddMPC', 'aMPC']
labels = ['Heizkurve', 'MPC', 'aMPC']
colors = [hellgrauebc, dunkelgrauebc, rotebc]

fig, axs = plt.subplots(1,2, figsize=(6,3))

for i in range(len(types)):
    axs[0].scatter(KPIs[types[i]]['Temp.-abweichung [Kh]'], KPIs[types[i]]['El. Energie [kWh]'], color=colors[i],marker = 'v',label=labels[i], zorder=3)
    axs[1].scatter(KPIs[types[i]]['SCOP'], KPIs[types[i]]['Switches pro Tag'], color=colors[i],marker = 'v',label=labels[i], zorder=3)

axs[0].set_ylim(6700, 7100)
axs[0].set_xlim(0, 600)
axs[1].set_xlim(3, 3.3)
axs[0].set_ylabel("El. Energie in kWh")
axs[0].set_xlabel("Unt. Solltemp.-abw. in Kh/a")
axs[1].set_ylabel("Switches in 1/Tag")
axs[1].set_ylim(7.5,14.5)
axs[1].set_xticks([3.0,3.1,3.2,3.3])
axs[1].set_xlabel("SCOP in -")
axs[0].set_yticks([6700, 6800, 6900, 7000, 7100])
axs[1].set_yticks([8,9,10,11,12,13,14])
axs[0].set_xticks([0,200,400,600])
fig.legend(loc='upper center', ncol=3)
axs[0].grid(zorder=0)
axs[1].grid(zorder=0)
fig.tight_layout()

plt.savefig("KPIs_Sim_HeatingPeriod.pdf")

plt.show()

# KPIs.to_excel(os.path.join("D:\lma-dtu TEMP\masterarbeit\Tools\Results", folder, "Plots", "KPIs.xlsx"))
