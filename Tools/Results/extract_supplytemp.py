import csv

# Funktion zum Extrahieren und Speichern einer Spalte in einer neuen CSV-Datei
def extract_and_save_column(input_file, output_file, column_index):
    try:
        with open(input_file, 'r', newline='') as csvfile:
            reader = csv.reader(csvfile)
            data = [row[column_index] for row in reader]

        with open(output_file, 'w', newline='') as csvfile:
            writer = csv.writer(csvfile)
            for item in data:
                writer.writerow([item])

        print(f"Spalte {column_index} wurde in '{output_file}' gespeichert.")
    except Exception as e:
        print(f"Fehler beim Extrahieren und Speichern der Spalte: {e}")


if __name__ == '__main__':
    input_file = 'PI_Controller_updated/aMPC.csv'  # Eingabe-CSV-Datei
    output_file = 'PI_Controller_updated/SupplyTemperature_aMPC.csv'  # Ausgabe-CSV-Datei
    column_index = 3  # Index der zu extrahierenden Spalte (0-basiert)

    extract_and_save_column(input_file, output_file, column_index)