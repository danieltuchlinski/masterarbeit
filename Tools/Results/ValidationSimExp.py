import pandas as pd
import matplotlib.pyplot as plt
import os
import matplotlib
import matplotlib.gridspec as gridspec

matplotlib.rcParams.update({'font.size': 9})
hellhellrotebc = (248/255,217/255,213/255)
hellrotebc = (241/255,179/255,171/255)
hellerrotebc = (235/255,140/255,129/255)
rotebc = (221/255,64/255,45/255)
dunkelrotebc = (172/255,43/255,28/255)
dunkeldunkelrotebc = (114/255,29/255,19/255)
dunkeldunkeldunkelrotebc = (76/255,17/255,12/255)
hellhellhellgrauebc = (242/255,242/255,242/255)
hellhellgrauebc = (235/255,236/255,236/255)
hellgrauebc = (216/255,216/255,217/255)
hellergrauebc = (196/255,197/255,198/255)
grauebc = (157/255,158/255,160/255)
dunkelgrauebc = (117/255,118/255,121/255)
dunkeldunkelgrauebc = (81/255,84/255,83/255)

ampc_exp_18Nov = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\HiL_files\ampc_exp_18Nov.csv", index_col=0)
ampc_exp_18Nov = ampc_exp_18Nov.set_index(pd.date_range("2014-11-18 00:00:00", periods=len(ampc_exp_18Nov), freq="1min").to_series())
ampc_exp_28Dec = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\HiL_files\ampc_exp_28Dec.csv", index_col=0)
ampc_exp_28Dec = ampc_exp_28Dec.set_index(pd.date_range("2014-12-28 00:00:00", periods=len(ampc_exp_28Dec), freq="1min").to_series())
ampc_exp_7Apr = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\HiL_files\ampc_exp_7Apr.csv", index_col=0)
ampc_exp_7Apr = ampc_exp_7Apr.set_index(pd.date_range("2015-04-07 00:00:00", periods=len(ampc_exp_7Apr), freq="1min").to_series())
ampc_exp_19Apr = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\HiL_files\ampc_exp_19Apr.csv", index_col=0)
ampc_exp_19Apr = ampc_exp_19Apr.set_index(pd.date_range("2015-04-19 00:00:00", periods=len(ampc_exp_19Apr), freq="1min").to_series())
ampc_exp = [ampc_exp_18Nov, ampc_exp_28Dec, ampc_exp_7Apr, ampc_exp_19Apr]

ampc_sim_18Nov = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files\ampc_sim_18Nov.csv", index_col=0)
ampc_sim_18Nov = ampc_sim_18Nov.set_index(pd.date_range("2014-11-18 00:00:00", periods=len(ampc_sim_18Nov), freq="1min").to_series())
ampc_sim_28Dec = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files\ampc_sim_28Dec.csv", index_col=0)
ampc_sim_28Dec = ampc_sim_28Dec.set_index(pd.date_range("2014-12-28 00:00:00", periods=len(ampc_sim_28Dec), freq="1min").to_series())
ampc_sim_7Apr = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files\ampc_sim_7Apr.csv", index_col=0)
ampc_sim_7Apr = ampc_sim_7Apr.set_index(pd.date_range("2015-04-07 00:00:00", periods=len(ampc_sim_7Apr), freq="1min").to_series())
ampc_sim_19Apr = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files\ampc_sim_19Apr.csv", index_col=0)
ampc_sim_19Apr = ampc_sim_19Apr.set_index(pd.date_range("2015-04-19 00:00:00", periods=len(ampc_sim_19Apr), freq="1min").to_series())
ampc_sim = [ampc_sim_18Nov, ampc_sim_28Dec, ampc_sim_7Apr, ampc_sim_19Apr]

ddmpc_exp_18Nov = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\HiL_files\ddmpc_exp_18Nov.csv", index_col=0)
ddmpc_exp_18Nov = ddmpc_exp_18Nov.set_index(pd.date_range("2014-11-18 00:00:00", periods=len(ddmpc_exp_18Nov), freq="1min").to_series())
ddmpc_exp_28Dec = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\HiL_files\ddmpc_exp_28Dec.csv", index_col=0)
ddmpc_exp_28Dec = ddmpc_exp_28Dec.set_index(pd.date_range("2014-12-28 00:00:00", periods=len(ddmpc_exp_28Dec), freq="1min").to_series())
ddmpc_exp_7Apr = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\HiL_files\ddmpc_exp_7Apr.csv", index_col=0)
ddmpc_exp_7Apr = ddmpc_exp_7Apr.set_index(pd.date_range("2015-04-07 00:00:00", periods=len(ddmpc_exp_7Apr), freq="1min").to_series())
ddmpc_exp_19Apr = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\HiL_files\ddmpc_exp_19Apr.csv", index_col=0)
ddmpc_exp_19Apr = ddmpc_exp_19Apr.set_index(pd.date_range("2015-04-19 00:00:00", periods=len(ddmpc_exp_19Apr), freq="1min").to_series())
ddmpc_exp = [ddmpc_exp_18Nov, ddmpc_exp_28Dec, ddmpc_exp_7Apr, ddmpc_exp_19Apr]

ddmpc_sim_18Nov = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files\ddmpc_sim_18Nov.csv", index_col=0)
ddmpc_sim_18Nov = ddmpc_sim_18Nov.set_index(pd.date_range("2014-11-18 00:00:00", periods=len(ddmpc_sim_18Nov), freq="1min").to_series())
ddmpc_sim_28Dec = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files\ddmpc_sim_28Dec.csv", index_col=0)
ddmpc_sim_28Dec = ddmpc_sim_28Dec.set_index(pd.date_range("2014-12-28 00:00:00", periods=len(ddmpc_sim_28Dec), freq="1min").to_series())
ddmpc_sim_7Apr = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files\ddmpc_sim_7Apr.csv", index_col=0)
ddmpc_sim_7Apr = ddmpc_sim_7Apr.set_index(pd.date_range("2015-04-07 00:00:00", periods=len(ddmpc_sim_7Apr), freq="1min").to_series())
ddmpc_sim_19Apr = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files\ddmpc_sim_19Apr.csv", index_col=0)
ddmpc_sim_19Apr = ddmpc_sim_19Apr.set_index(pd.date_range("2015-04-19 00:00:00", periods=len(ddmpc_sim_19Apr), freq="1min").to_series())
ddmpc_sim = [ddmpc_sim_18Nov, ddmpc_sim_28Dec, ddmpc_sim_7Apr, ddmpc_sim_19Apr]

hc_exp_18Nov = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\HiL_files\hc_exp_18Nov.csv", index_col=0)
hc_exp_18Nov = hc_exp_18Nov.set_index(pd.date_range("2014-11-18 00:00:00", periods=len(hc_exp_18Nov), freq="1min").to_series())
hc_exp_28Dec = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\HiL_files\hc_exp_28Dec.csv", index_col=0)
hc_exp_28Dec = hc_exp_28Dec.set_index(pd.date_range("2014-12-28 00:00:00", periods=len(hc_exp_28Dec), freq="1min").to_series())
hc_exp_7Apr = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\HiL_files\hc_exp_7Apr.csv", index_col=0)
hc_exp_7Apr = hc_exp_7Apr.set_index(pd.date_range("2015-04-07 00:00:00", periods=len(hc_exp_7Apr), freq="1min").to_series())
hc_exp_19Apr = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\HiL_files\hc_exp_19Apr.csv", index_col=0)
hc_exp_19Apr = hc_exp_19Apr.set_index(pd.date_range("2015-04-19 00:00:00", periods=len(hc_exp_19Apr), freq="1min").to_series())
hc_exp = [hc_exp_18Nov, hc_exp_28Dec, hc_exp_7Apr, hc_exp_19Apr]

hc_sim_18Nov = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files\hc_sim_18Nov.csv", index_col=0)
hc_sim_18Nov = hc_sim_18Nov.set_index(pd.date_range("2014-11-18 00:00:00", periods=len(hc_sim_18Nov), freq="1min").to_series())
hc_sim_28Dec = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files\hc_sim_28Dec.csv", index_col=0)
hc_sim_28Dec = hc_sim_28Dec.set_index(pd.date_range("2014-12-28 00:00:00", periods=len(hc_sim_28Dec), freq="1min").to_series())
hc_sim_7Apr = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files\hc_sim_7Apr.csv", index_col=0)
hc_sim_7Apr = hc_sim_7Apr.set_index(pd.date_range("2015-04-07 00:00:00", periods=len(hc_sim_7Apr), freq="1min").to_series())
hc_sim_19Apr = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files\hc_sim_19Apr.csv", index_col=0)
hc_sim_19Apr = hc_sim_19Apr.set_index(pd.date_range("2015-04-19 00:00:00", periods=len(hc_sim_19Apr), freq="1min").to_series())
hc_sim = [hc_sim_18Nov, hc_sim_28Dec, hc_sim_7Apr, hc_sim_19Apr]

hc = [hc_exp, hc_sim]
ddmpc = [ddmpc_exp, ddmpc_sim]
ampc = [ampc_exp, ampc_sim]

strategies = [hc, ddmpc, ampc]
types = ['Heizkurve', 'MPC', 'aMPC']
savenames = ['hc','ddmpc','ampc']
days = ['18Nov','28Dec', '7Apr', '19Apr']
titles = ['18. November', '28. Dezember', '7. April', '19. April']

colors = [grauebc, dunkelgrauebc, rotebc]

for type in range(len(types)):
    amount = 5
    fig = plt.figure(figsize=(15.5/2.54,15.5/2.54))  # figsize=(15.5/2.54,15.5/2.54)
    outer = gridspec.GridSpec(2, 2, wspace=0.05, hspace=0.25)

    for i in range(len(ampc_exp)):
        inner = gridspec.GridSpecFromSubplotSpec(5, 1, subplot_spec=outer[i], wspace=0.1, hspace=0.1)

        for j in range(5):
            ax = plt.Subplot(fig, inner[j])
            fig.add_subplot(ax)

            if j==0:
                ax.plot(strategies[type][1][i]['Zonensolltemperatur'] - 273.15, color="black",linewidth=0.75, label="Soll")
                ax.plot(strategies[type][1][i]['Zonentemperatur']-273.15, color=colors[type],linestyle='--',linewidth=0.75, label="Simulation")
                ax.plot(strategies[type][0][i]['Zonentemperatur'], color=colors[type],linewidth=0.75, label="Experiment")
                ax.grid(True, linewidth=0.25)
                ax.set_title(titles[i], fontsize=9, fontweight="bold")
                ax.set_xticks(ticks=pd.date_range(strategies[type][1][i].index[0], periods=7, freq="4h"),labels=['0', '4', '8', '12', '16', '20', '0'])
                ax.set_xticklabels([])
                ax.set_ylabel('$T_{Zone}$ in °C')
                ax.set_yticks([17,18,19,20,21])
                ax.set_ylim([16.5,21.5])
                ax.tick_params(axis='x', which='both', length=0)
                if i==3:#bei 4 typtagen auf 3 ändern
                    handles, labels = ax.get_legend_handles_labels()
                    fig.legend(handles, labels, loc='lower center', bbox_to_anchor=(0.5, 0), ncol=4)
            if j==1:
                if type==0:
                    ax.plot(strategies[type][1][i]['Opening'], color=colors[type],linewidth=0.75, linestyle='--', label="Simulation")
                    ax.plot(strategies[type][0][i]['Opening'], color=colors[type],linewidth=0.75, label="Experiment")
                    ax.grid(True, linewidth=0.25)
                    ax.set_xticks(ticks=pd.date_range(strategies[type][1][i].index[0], periods=7, freq="4h"),labels=['0', '4', '8', '12', '16', '20', '0'])
                    ax.set_xticklabels([])
                    ax.set_ylim([-0.1, 1.1])
                    ax.set_yticks([0,0.25,0.5,0.75,1])
                    ax.set_ylabel('Öffnungsgrad in -')
                    ax.tick_params(axis='x', which='both', length=0)
                else:
                    ax.plot(strategies[type][1][i]['Vorlaufsolltemperatur'] - 273.15, color=colors[type],linewidth=0.75,linestyle='--', label="Simulation")
                    ax.plot(strategies[type][0][i]['Vorlaufsolltemperatur'] - 273.15, color=colors[type],linewidth=0.75,label="Experiment")
                    ax.grid(True, linewidth=0.25)
                    ax.set_xticks(ticks=pd.date_range(strategies[type][1][i].index[0], periods=7, freq="4h"), labels=['0', '4', '8', '12', '16', '20', '0'])
                    ax.set_xticklabels([])
                    ax.set_ylim([18, 42])
                    ax.set_yticks([20, 25, 30, 35, 40])
                    ax.set_ylabel('$T_{VL}^{Soll}$ in °C')
                    ax.tick_params(axis='x', which='both', length=0)
            if j==2:
                ax.plot(strategies[type][1][i]['Vorlauftemperatur'] - 273.15, color=colors[type],linewidth=0.75, linestyle='--',label="Simulation")
                ax.plot(strategies[type][0][i]['Vorlauftemperatur'], color=colors[type],linewidth=0.75,label="Experiment")
                ax.grid(True, linewidth=0.25)
                ax.set_xticks(ticks=pd.date_range(strategies[type][1][i].index[0], periods=7, freq="4h"),labels=['0', '4', '8', '12', '16', '20', '0'])
                ax.set_ylabel('$T_{VL}$ in °C')
                if type==0:
                    ax.set_ylim([18, 72])
                    ax.set_yticks([20, 30, 40, 50, 60, 70])
                else:
                    ax.set_ylim([18, 42])
                    ax.set_yticks([20, 25, 30, 35, 40])
                ax.set_xlabel('Uhrzeit')
            if j==3:
                ax.plot(strategies[type][1][i]['ElektrischeLeistung'], color=colors[type], linewidth=0.75,
                        linestyle='--', label="Simulation")
                ax.plot(strategies[type][0][i]['ElektrischeLeistung'], color=colors[type], linewidth=0.75,
                        label="Experiment")
                ax.grid(True, linewidth=0.25)
                ax.set_xticks(ticks=pd.date_range(strategies[type][1][i].index[0], periods=7, freq="4h"),
                              labels=['0', '4', '8', '12', '16', '20', '0'])
                ax.set_ylabel('El- Leistung in W')
            if j==4:
                ax.plot(strategies[type][1][i]['Drehzahl'], color=colors[type], linewidth=0.75,
                        linestyle='--', label="Simulation")
                ax.plot(strategies[type][0][i]['Drehzahl']/90, color=colors[type], linewidth=0.75,
                        label="Experiment")
                ax.grid(True, linewidth=0.25)
                ax.set_xticks(ticks=pd.date_range(strategies[type][1][i].index[0], periods=7, freq="4h"),
                              labels=['0', '4', '8', '12', '16', '20', '0'])
                ax.set_ylabel('n in -')
            if i==1 or i==3:
                ax.set_yticklabels([])
                ax.set_ylabel('')
                ax.tick_params(axis='y', which='both', length=0)

            # Graue Markierungen
            # if type==0:
            #     if j==2 and i==0:
            #         ax.axvspan(pd.to_datetime("2014-11-18 08:00:00"), pd.to_datetime("2014-11-18 14:00:00"), color=hellgrauebc,alpha=0.5, edgecolor='none')
            #     if j == 2 and i == 1:
            #         ax.axvspan(pd.to_datetime("2014-12-28 12:00:00"), pd.to_datetime("2014-12-28 14:15:00"),color=hellgrauebc, alpha=0.5, edgecolor='none')
            #     if j == 2 and i == 3:
            #         ax.axvspan(pd.to_datetime("2015-04-19 09:40:00"), pd.to_datetime("2015-04-19 20:45:00"),color=hellgrauebc, alpha=0.5, edgecolor='none')
            #     if j == 2 and i == 2:
            #         ax.axvspan(pd.to_datetime("2015-04-07 00:00:00"), pd.to_datetime("2015-04-07 00:50:00"),color=hellgrauebc, alpha=0.5, edgecolor='none')
            #
            # if type==1:
            #     if j == 2 and i == 0:
            #         ax.axvspan(pd.to_datetime("2014-11-18 00:00:00"), pd.to_datetime("2014-11-18 04:00:00"), color=hellgrauebc, alpha=0.5, edgecolor='none')
            #     if j == 1 and i == 0:
            #         ax.axvspan(pd.to_datetime("2014-11-18 00:00:00"), pd.to_datetime("2014-11-18 04:00:00"),color=hellgrauebc, alpha=0.5, edgecolor='none')
            #     if j == 0 and i == 0:
            #         ax.axvspan(pd.to_datetime("2014-11-18 00:00:00"), pd.to_datetime("2014-11-18 04:00:00"),color=hellgrauebc, alpha=0.5, edgecolor='none')
            #     if j == 2 and i == 1:
            #         ax.axvspan(pd.to_datetime("2014-12-28 00:00:00"), pd.to_datetime("2014-12-28 04:00:00"), color=hellgrauebc, alpha=0.5, edgecolor='none')
            #     if j == 1 and i == 1:
            #         ax.axvspan(pd.to_datetime("2014-12-28 00:00:00"), pd.to_datetime("2014-12-28 04:00:00"),color=hellgrauebc, alpha=0.5, edgecolor='none')
            #     if j == 0 and i == 1:
            #         ax.axvspan(pd.to_datetime("2014-12-28 00:00:00"), pd.to_datetime("2014-12-28 04:00:00"),color=hellgrauebc, alpha=0.5, edgecolor='none')
            #     if j == 2 and i == 2:
            #         ax.axvspan(pd.to_datetime("2015-04-07 00:00:00"), pd.to_datetime("2015-04-07 04:00:00"), color=hellgrauebc, alpha=0.5, edgecolor='none')
            #     if j == 1 and i == 2:
            #         ax.axvspan(pd.to_datetime("2015-04-07 00:00:00"), pd.to_datetime("2015-04-07 04:00:00"),color=hellgrauebc, alpha=0.5, edgecolor='none')
            #     if j == 0 and i == 2:
            #         ax.axvspan(pd.to_datetime("2015-04-07 00:00:00"), pd.to_datetime("2015-04-07 04:00:00"),color=hellgrauebc, alpha=0.5, edgecolor='none')
            #     if j == 2 and i == 3:
            #         ax.axvspan(pd.to_datetime("2015-04-19 00:00:00"), pd.to_datetime("2015-04-19 04:00:00"), color=hellgrauebc, alpha=0.5, edgecolor='none')
            #     if j == 1 and i == 3:
            #         ax.axvspan(pd.to_datetime("2015-04-19 00:00:00"), pd.to_datetime("2015-04-19 04:00:00"),color=hellgrauebc, alpha=0.5, edgecolor='none')
            #     if j == 0 and i == 3:
            #         ax.axvspan(pd.to_datetime("2015-04-19 00:00:00"), pd.to_datetime("2015-04-19 04:00:00"),color=hellgrauebc, alpha=0.5, edgecolor='none')
            #
            # if type==2:
            #     if j == 2 and i == 0:
            #         ax.axvspan(pd.to_datetime("2014-11-18 08:45:00"), pd.to_datetime("2014-11-18 13:30:00"),color=hellgrauebc, alpha=0.5, edgecolor='none')
            #     if j == 1 and i == 0:
            #         ax.axvspan(pd.to_datetime("2014-11-18 00:00:00"), pd.to_datetime("2014-11-18 04:00:00"),color=hellgrauebc, alpha=0.5, edgecolor='none')
            #     if j == 2 and i == 1:
            #         ax.axvspan(pd.to_datetime("2014-12-28 19:10:00"), pd.to_datetime("2014-12-28 19:50:00"),color=hellgrauebc, alpha=0.5, edgecolor='none')
            #         ax.axvspan(pd.to_datetime("2014-12-28 11:00:00"), pd.to_datetime("2014-12-28 16:00:00"),color=hellgrauebc, alpha=0.5, edgecolor='none')
            #     if j == 1 and i == 3:
            #         ax.axvspan(pd.to_datetime("2015-04-19 09:00:00"), pd.to_datetime("2015-04-19 20:00:00"),color=hellgrauebc, alpha=0.5, edgecolor='none')

            ax.set_xlim([strategies[type][1][i].index[0], strategies[type][1][i].index[-1]])
    plt.savefig(types[type] +'Validation.pdf')
    fig.show()
