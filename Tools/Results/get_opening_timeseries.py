from ebcpy import TimeSeriesData
import pandas as pd
import matplotlib.pyplot as plt
import os
import numpy as np

timeseries = True
KeyPerformance = True

path_hc = r"D:\MA_Daniel\25_10_2023_April19th_hc\222\dsres.mat"
hc = TimeSeriesData(path_hc)
hc = pd.DataFrame(hc)
hc.columns = hc.columns.droplevel(1)
opening1 = hc['hydraulic.control.sigBusTra.opening[1]']
opening1 = opening1.set_axis(pd.date_range("2015-04-19 00:00:00", periods=len(opening1), freq="1s").to_series())
opening1 = opening1[:][pd.date_range("2015-04-19 00:00:00", periods=len(opening1)/60+1, freq="60s").to_series()]

hc = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\HiL_files\hc_exp_19Apr_without_opening.csv", index_col=0)
hc = hc.set_index(pd.date_range("2015-04-19 00:00:00", periods=len(hc), freq="60s").to_series())
hc['Opening']=opening1

hc.to_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\HiL_files\hc_exp_19Apr.csv")

print('Debugger')
# path_hc = r"D:\MA_Daniel\06_10_2023_December28th_hc\110\dsres.mat"
# hc = TimeSeriesData(path_hc)
# hc = pd.DataFrame(hc)
# hc.columns = hc.columns.droplevel(1)
# opening2 = hc['hydraulic.control.sigBusTra.opening[1]']
# opening2 = opening2.set_axis(pd.date_range("2014-12-28 00:00:00", periods=len(opening2), freq="1s").to_series())
#
# path_hc = r"D:\MA_Daniel\08_10_2023_April7th_hc\210\dsres.mat"
# hc = TimeSeriesData(path_hc)
# hc = pd.DataFrame(hc)
# hc.columns = hc.columns.droplevel(1)
# opening3 = hc['hydraulic.control.sigBusTra.opening[1]']
# opening3 = opening3.set_axis(pd.date_range("2015-04-07 00:00:00", periods=len(opening3), freq="1s").to_series())
#
# path_hc = r"D:\MA_Daniel\25_10_2023_April19th_hc\222\dsres.mat"
# hc = TimeSeriesData(path_hc)
# hc = pd.DataFrame(hc)
# hc.columns = hc.columns.droplevel(1)
# opening4 = hc['hydraulic.control.sigBusTra.opening[1]']
# opening4 = opening4.set_axis(pd.date_range("2015-04-19 00:00:00", periods=len(opening4), freq="1s").to_series())




