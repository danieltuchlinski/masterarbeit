import matplotlib
import matplotlib.pyplot as plt
import numpy as np

def HeatingCurve(T_U,n):
    TVL_nom = 40
    TZone_nom = 20
    TU_nom = -12


    TVL_Soll = (TVL_nom-TZone_nom)*((TZone_nom-T_U)/(TZone_nom-TU_nom))**(1/n)+TZone_nom

    return TVL_Soll

cm = 1 / 2.54
x = np.linspace(-12,20,100)
plt.figure()
plt.plot(x, HeatingCurve(x, n=1.8), color='black')
plt.grid(True)
plt.xlabel('Umgebungstemperatur in °C')
plt.ylabel('Sollvorlauftemperatur in °C')
plt.xlim([-12, 20])
plt.ylim([20, 40])
plt.yticks([20,25,30,35,40])
plt.xticks([-10,-5,0,5,10,15,20])
plt.savefig(r'D:\lma-dtu TEMP\masterarbeit\Schriftliche Ausarbeitung\Graphiken\HeatingCurve.pdf')
plt.show()

