import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import os
import numpy as np
import pandas as pd
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches

matplotlib.rcParams.update({'font.size': 9})
hellhellrotebc = (248/255,217/255,213/255)
hellrotebc = (241/255,179/255,171/255)
hellerrotebc = (235/255,140/255,129/255)
rotebc = (221/255,64/255,45/255)
dunkelrotebc = (172/255,43/255,28/255)
dunkeldunkelrotebc = (114/255,29/255,19/255)
dunkeldunkeldunkelrotebc = (76/255,17/255,12/255)
hellhellhellgrauebc = (242/255,242/255,242/255)
hellhellgrauebc = (235/255,236/255,236/255)
hellgrauebc = (216/255,216/255,217/255)
hellergrauebc = (196/255,197/255,198/255)
grauebc = (157/255,158/255,160/255)
dunkelgrauebc = (117/255,118/255,121/255)
dunkeldunkelgrauebc = (81/255,84/255,83/255)

ampc_18Nov = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\HiL_files\ampc_exp_18Nov.csv", index_col=0)
ampc_18Nov = ampc_18Nov.set_index(pd.date_range("2014-11-18 00:00:00", periods=len(ampc_18Nov), freq="1min").to_series())
ampc_28Dec = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\HiL_files\ampc_exp_28Dec.csv", index_col=0)
ampc_28Dec = ampc_28Dec.set_index(pd.date_range("2014-12-28 00:00:00", periods=len(ampc_28Dec), freq="1min").to_series())
ampc_7Apr = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\HiL_files\ampc_exp_7Apr.csv", index_col=0)
ampc_7Apr = ampc_7Apr.set_index(pd.date_range("2015-04-07 00:00:00", periods=len(ampc_7Apr), freq="1min").to_series())
ampc_19Apr = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\HiL_files\ampc_exp_19Apr.csv", index_col=0)
ampc_19Apr = ampc_19Apr.set_index(pd.date_range("2015-04-19 00:00:00", periods=len(ampc_19Apr), freq="1min").to_series())
ampc = [ampc_18Nov, ampc_28Dec, ampc_7Apr, ampc_19Apr]

colors=[grauebc, dunkelgrauebc, hellrotebc, rotebc]
labels = ['18. November','28. Dezember','4. April','19. April']

for i in range(len(ampc)):
    plt.plot(np.arange(1441), ampc[i]['Sollumgebungstemperatur'], color=colors[i], label=labels[i])
plt.xlim([0,1440])
plt.ylim([-5,15])
plt.yticks([-5,0,5,10,15])
plt.ylabel('Umgebungstemperatur in °C')
plt.xlabel('Uhrzeit')
plt.grid()
plt.xticks(ticks=[0,240,480,720,960,1200,1440], labels=['00:00','04:00','08:00','12:00','16:00','20:00','00:00'])
plt.legend()
plt.savefig('Umgebungstemperaturen.pdf')
plt.show()