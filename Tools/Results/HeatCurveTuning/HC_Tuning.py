import pandas as pd
import os
import matplotlib.pyplot as plt

hc02 = pd.read_csv(os.path.join("D:\lma-dtu TEMP\masterarbeit\Tools\Results", "HeatCurveTuning", "HC02.csv"), index_col=0, sep=",")
hc04 = pd.read_csv(os.path.join("D:\lma-dtu TEMP\masterarbeit\Tools\Results", "HeatCurveTuning", "HC04.csv"), index_col=0, sep=",")
hc06 = pd.read_csv(os.path.join("D:\lma-dtu TEMP\masterarbeit\Tools\Results", "HeatCurveTuning", "HC06.csv"), index_col=0, sep=",")
hc08 = pd.read_csv(os.path.join("D:\lma-dtu TEMP\masterarbeit\Tools\Results", "HeatCurveTuning", "HC08.csv"), index_col=0, sep=",")
hc10 = pd.read_csv(os.path.join("D:\lma-dtu TEMP\masterarbeit\Tools\Results", "HeatCurveTuning", "HC10.csv"), index_col=0, sep=",")
hc12 = pd.read_csv(os.path.join("D:\lma-dtu TEMP\masterarbeit\Tools\Results", "HeatCurveTuning", "HC12.csv"), index_col=0, sep=",")
hc14 = pd.read_csv(os.path.join("D:\lma-dtu TEMP\masterarbeit\Tools\Results", "HeatCurveTuning", "HC14.csv"), index_col=0, sep=",")
hc16 = pd.read_csv(os.path.join("D:\lma-dtu TEMP\masterarbeit\Tools\Results", "HeatCurveTuning", "HC16.csv"), index_col=0, sep=",")
hc18 = pd.read_csv(os.path.join("D:\lma-dtu TEMP\masterarbeit\Tools\Results", "HeatCurveTuning", "HC18.csv"), index_col=0, sep=",")
hc20 = pd.read_csv(os.path.join("D:\lma-dtu TEMP\masterarbeit\Tools\Results", "HeatCurveTuning", "HC20.csv"), index_col=0, sep=",")
hc22 = pd.read_csv(os.path.join("D:\lma-dtu TEMP\masterarbeit\Tools\Results", "HeatCurveTuning", "HC22.csv"), index_col=0, sep=",")
hc24 = pd.read_csv(os.path.join("D:\lma-dtu TEMP\masterarbeit\Tools\Results", "HeatCurveTuning", "HC24.csv"), index_col=0, sep=",")
hc26 = pd.read_csv(os.path.join("D:\lma-dtu TEMP\masterarbeit\Tools\Results", "HeatCurveTuning", "HC26.csv"), index_col=0, sep=",")


startdate = "2014-10-01 00:00:00"
enddate = "2015-04-30 23:50:00"

hc = [hc02, hc04, hc06, hc08, hc10, hc12, hc14, hc16, hc18, hc20, hc22, hc24, hc26]
n = [0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 2.2, 2.4, 2.6]

Switches = []
Energy = []
Diskomfort = []

for curve in hc:
    Switches.append((curve.loc[enddate]["hydraulic.generation.outBusGen.heaPum.numSwi"]-curve.loc[startdate]["hydraulic.generation.outBusGen.heaPum.numSwi"])/212)
    Energy.append((curve.loc[enddate]["outputs.hydraulic.gen.PEleHP.integral"]-curve.loc[startdate]["outputs.hydraulic.gen.PEleHP.integral"])/1000/3600)
    Diskomfort.append(curve["TError"][startdate:enddate].mask(curve["TError"][startdate:enddate] > 0, 0).abs().sum()/6)

ProSwitches = (Switches-min(Switches))/(max(Switches)-min(Switches))
ProEnergy = (Energy-min(Energy))/(max(Energy)-min(Energy))
ProDiskomfort = (Diskomfort-min(Diskomfort))/(max(Diskomfort)-min(Diskomfort))

ProTotal = ProSwitches + ProEnergy + ProDiskomfort

plt.figure(figsize=(4,3))
plt.grid(True, linewidth = 0.25)
plt.plot(n, ProEnergy, label='El. Energie', color='lightgrey')
plt.plot(n, ProSwitches, '--',label='Anzahl Switches', color='darkgrey')
plt.plot(n, ProDiskomfort, '-.',label='U. Solltemp.-abw.', color='black')
plt.xlim([0.2, 2.6])
plt.ylim([0, 1])
plt.xlabel('n')
plt.ylabel('Normierter KPI')
plt.legend()
plt.tight_layout()
plt.savefig(r'D:\lma-dtu TEMP\masterarbeit\Tools\Results\nStudy.pdf')
plt.show()