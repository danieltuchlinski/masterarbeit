import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import os
import numpy as np
import pandas as pd
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches

matplotlib.rcParams.update({'font.size': 9})
hellhellrotebc = (248/255,217/255,213/255)
hellrotebc = (241/255,179/255,171/255)
hellerrotebc = (235/255,140/255,129/255)
rotebc = (221/255,64/255,45/255)
dunkelrotebc = (172/255,43/255,28/255)
dunkeldunkelrotebc = (114/255,29/255,19/255)
dunkeldunkeldunkelrotebc = (76/255,17/255,12/255)
hellhellhellgrauebc = (242/255,242/255,242/255)
hellhellgrauebc = (235/255,236/255,236/255)
hellgrauebc = (216/255,216/255,217/255)
hellergrauebc = (196/255,197/255,198/255)
grauebc = (157/255,158/255,160/255)
dunkelgrauebc = (117/255,118/255,121/255)
dunkeldunkelgrauebc = (81/255,84/255,83/255)

def insert_sim(df1, df2, start_index, end_index):
    # Überprüfe, ob die Indizes in beiden DataFrames vorhanden sind
    df1['ElektrischeEnergie'] = df1['ElektrischeLeistung'].cumsum()*60
    df1['Heizenergie'] = df1['Heizleistung'].cumsum() * 60
    df1 = count_zero_transitions(df1, 'Drehzahl', 'Switches')
    if not all(df1.index.isin(df2.index)) or not all(df2.index.isin(df1.index)):
        raise ValueError("Die Indizes in den DataFrames stimmen nicht überein.")

    # Kopiere df2, um df3 zu erstellen, der die Änderungen enthält
    df3 = df2.copy()

    # Überschreibe den Indexbereich von df3 mit df1
    df3.loc[start_index:end_index] = df1.loc[start_index:end_index]
    # Füge fehlende Spalten von df1 in df3 ein
    for col in df1.columns:
        if col not in df3.columns:
            df3[col] = df1[col]

    df3 = count_zero_transitions(df3,'Drehzahl','Switches')
    df3['ElektrischeEnergie'] = df3['ElektrischeLeistung'].cumsum() * 60
    df3['Heizenergie'] = df3['Heizleistung'].cumsum() * 60
    return df3


def count_zero_transitions(df, value_column, count_column):
    # Erstelle eine neue Spalte mit dem Namen count_column und initialisiere sie mit 0
    df[count_column] = 0

    prev_value = 0  # Initialisierung für den Übergang von 0
    count = 0  # Zähler für die Übergänge

    for index, row in df.iterrows():
        current_value = row[value_column]

        if prev_value > 0 and current_value == 0:
            count += 1

        prev_value = current_value
        df.at[index, count_column] = count

    return df

ampc_18Nov = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\HiL_files\ampc_exp_18Nov.csv", index_col=0)
ampc_18Nov = ampc_18Nov.set_index(pd.date_range("2014-11-18 00:00:00", periods=len(ampc_18Nov), freq="1min").to_series())
ampc_28Dec = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\HiL_files\ampc_exp_28Dec.csv", index_col=0)
ampc_28Dec = ampc_28Dec.set_index(pd.date_range("2014-12-28 00:00:00", periods=len(ampc_28Dec), freq="1min").to_series())
ampc_7Apr = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\HiL_files\ampc_exp_7Apr.csv", index_col=0)
ampc_7Apr = ampc_7Apr.set_index(pd.date_range("2015-04-07 00:00:00", periods=len(ampc_7Apr), freq="1min").to_series())
ampc_19Apr = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\HiL_files\ampc_exp_19Apr.csv", index_col=0)
ampc_19Apr = ampc_19Apr.set_index(pd.date_range("2015-04-19 00:00:00", periods=len(ampc_19Apr), freq="1min").to_series())
ampc = [ampc_18Nov, ampc_28Dec, ampc_7Apr, ampc_19Apr]


ddmpc_18Nov = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\HiL_files\ddmpc_exp_18Nov.csv", index_col=0)
ddmpc_18Nov = ddmpc_18Nov.set_index(pd.date_range("2014-11-18 00:00:00", periods=len(ddmpc_18Nov), freq="1min").to_series())
ddmpc_28Dec = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\HiL_files\ddmpc_exp_28Dec.csv", index_col=0)
ddmpc_28Dec = ddmpc_28Dec.set_index(pd.date_range("2014-12-28 00:00:00", periods=len(ddmpc_28Dec), freq="1min").to_series())
ddmpc_7Apr = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\HiL_files\ddmpc_exp_7Apr.csv", index_col=0)
ddmpc_7Apr = ddmpc_7Apr.set_index(pd.date_range("2015-04-07 00:00:00", periods=len(ddmpc_7Apr), freq="1min").to_series())
ddmpc_19Apr = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\HiL_files\ddmpc_exp_19Apr.csv", index_col=0)
ddmpc_19Apr = ddmpc_19Apr.set_index(pd.date_range("2015-04-19 00:00:00", periods=len(ddmpc_19Apr), freq="1min").to_series())
ddmpc = [ddmpc_18Nov, ddmpc_28Dec, ddmpc_7Apr, ddmpc_19Apr]

hc_18Nov = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\HiL_files\hc_exp_18Nov.csv", index_col=0)
hc_18Nov = hc_18Nov.set_index(pd.date_range("2014-11-18 00:00:00", periods=len(hc_18Nov), freq="1min").to_series())
hc_28Dec = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\HiL_files\hc_exp_28Dec.csv", index_col=0)
hc_28Dec = hc_28Dec.set_index(pd.date_range("2014-12-28 00:00:00", periods=len(hc_28Dec), freq="1min").to_series())
hc_7Apr = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\HiL_files\hc_exp_7Apr.csv", index_col=0)
hc_7Apr = hc_7Apr.set_index(pd.date_range("2015-04-07 00:00:00", periods=len(hc_7Apr), freq="1min").to_series())
hc_19Apr = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\HiL_files\hc_exp_19Apr.csv", index_col=0)
hc_19Apr = hc_19Apr.set_index(pd.date_range("2015-04-19 00:00:00", periods=len(hc_19Apr), freq="1min").to_series())
hc = [hc_18Nov, hc_28Dec, hc_7Apr, hc_19Apr]

timeseries = False
KeyPerformance = True

days = ['18Nov', '28Dec', '7Apr', '19Apr']
titles = ['18. November', '28. Dezember', '7. April', '19. April']
data= [hc,ddmpc,ampc]
types = ['Heizkurve', 'MPC', 'aMPC']
colors = [grauebc, dunkelgrauebc, rotebc]


# Add Switches column
for i in range(len(data)):
    for j in range(len(ampc)):
        data[i][j] = count_zero_transitions(data[i][j],'Drehzahl','Switches')

if timeseries:
    linewidths = [1, 1, 1]
    linestyles = ['dashed', 'solid', 'solid']

    amount=4
    fig = plt.figure() #figsize=(15.5/2.54,12.5/2.54)
    outer = gridspec.GridSpec(2, 2, wspace=0.05, hspace=0.35)

    for i in range(amount): # eigentlich 4
        inner = gridspec.GridSpecFromSubplotSpec(2, 1,subplot_spec=outer[i], wspace=0.1, hspace=0.1)

        for j in range(2):
            ax = plt.Subplot(fig, inner[j])
            fig.add_subplot(ax)
            if j==0:
                ax.set_xticks(ticks=pd.date_range(data[0][i].index[0], periods=7, freq="4h"),labels=['0', '4', '8', '12', '16', '20', '0'])
                ax.grid(True, linewidth=0.25)
                ax.set_xticklabels([])
                ax.tick_params(axis='x', which='both', length=0)
                ax.set_ylim([16.5,21.5])
                ax.set_yticks([17,18,19,20,21])
                ax.set_title(titles[i], fontsize=9, fontweight="bold")
                ax.set_ylabel('$T_{Zone}$ in °C')
                ax.plot(data[0][i]["Zonensolltemperatur"], color="black", label="Soll", linewidth=1)
                for type in range(len(data)):
                    ax.plot(data[type][i]["Zonentemperatur"], color=colors[type],label=types[type], linestyle=linestyles[type], linewidth=linewidths[type])
                if i==amount-1:#bei 4 typtagen amount auf 4 ändern
                    handles, labels = ax.get_legend_handles_labels()
                    fig.legend(handles, labels, loc='lower center', bbox_to_anchor=(0.5, 0), ncol=4)
            if j==1:
                ax.set_xticks(ticks=pd.date_range(data[0][i].index[0], periods=7, freq="4h"),labels=['0','4', '8','12', '16','20', '0'])
                ax.grid(True, linewidth=0.25)
                ax.set_xlabel('Uhrzeit')
                ax.set_ylim([18, 42])
                ax.set_yticks([20,25,30,35,40])
                ax.set_ylabel('$T_{VL}^{Soll}$ in °C')
                for type in range(len(data)):
                    ax.plot(data[type][i]["Vorlaufsolltemperatur"]-273.15, color=colors[type],label=types[type], linestyle=linestyles[type], linewidth=linewidths[type])
            if i==1 or i==3:
                ax.set_ylabel('')
                ax.set_yticklabels([])
                ax.tick_params(axis='y', which='both', length=0)

            ax.set_xlim([data[0][i].index[0],data[0][i].index[-1]])

            ######Rectangles#################
            if j==1 and i == 2:
                ax.axvspan(pd.to_datetime("2015-04-07 02:28:00"), pd.to_datetime("2015-04-07 02:40:00"),color=hellgrauebc, alpha=0.5, edgecolor='none')
                ax.axvspan(pd.to_datetime("2015-04-07 08:00:00"), pd.to_datetime("2015-04-07 12:00:00"), color=hellgrauebc,alpha=0.5, edgecolor='none')
            if j==0 and i == 2:
                ax.axvspan(pd.to_datetime("2015-04-07 08:00:00"), pd.to_datetime("2015-04-07 12:00:00"),color=hellgrauebc, alpha=0.5, edgecolor='none')
                ax.axvspan(pd.to_datetime("2015-04-07 20:00:00"), pd.to_datetime("2015-04-08 00:00:00"),color=hellgrauebc, alpha=0.5, edgecolor='none')
            if j==1 and i==1:
                ax.axvspan(pd.to_datetime("2014-12-28 00:00:00"), pd.to_datetime("2014-12-28 01:00:00"),color=hellgrauebc, alpha=0.5, edgecolor='none')
            if j==0 and i==0:
                ax.axvspan(pd.to_datetime("2014-11-18 00:00:00"), pd.to_datetime("2014-11-18 01:30:00"),color=hellgrauebc, alpha=0.5, edgecolor='none')
                ax.axvspan(pd.to_datetime("2014-11-18 17:00:00"), pd.to_datetime("2014-11-18 20:30:00"),color=hellgrauebc, alpha=0.5, edgecolor='none')
            if j==1 and i==3:
                ax.axvspan(pd.to_datetime("2015-04-19 13:53:00"), pd.to_datetime("2015-04-19 14:02:00"),color=hellgrauebc, alpha=0.5, edgecolor='none')
                ax.axvspan(pd.to_datetime("2015-04-19 22:55:00"), pd.to_datetime("2015-04-20 00:00:00"), color=hellgrauebc,alpha=0.5, edgecolor='none')

    plt.savefig('TyptageExp.pdf')
    fig.show()

labels = ['Elektrische Energie in kWh', 'Anzahl der Switches', 'Untere Solltemperaturabweichung in Kh', 'SCOP']
savenames = ['Energie', 'Switches', 'TD', 'SCOP']
if KeyPerformance:

    # Uncorrected and corrected

    ampc_7Apr_sim = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files\ampc_sim_7Apr.csv", index_col=0)
    ampc_7Apr_sim = ampc_7Apr_sim.set_index(pd.date_range("2015-04-07 00:00:00", periods=len(ampc_7Apr_sim), freq="1min").to_series())
    data[2][2] = insert_sim(df1=ampc_7Apr_sim, df2=data[2][2], start_index=pd.to_datetime("2015-04-07 02:29:00"), end_index=pd.to_datetime("2015-04-07 02:39:00"))

    ampc_19Apr_sim = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files\ampc_sim_19Apr.csv",index_col=0)
    ampc_19Apr_sim = ampc_19Apr_sim.set_index(pd.date_range("2015-04-19 00:00:00", periods=len(ampc_19Apr_sim), freq="1min").to_series())
    data[2][3] = insert_sim(df1=ampc_19Apr_sim, df2=data[2][3], start_index=pd.to_datetime("2015-04-19 13:53:00"),end_index=pd.to_datetime("2015-04-19 14:18:00"))

    ddmpc_19Apr_sim = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files\ddmpc_sim_19Apr.csv",index_col=0)
    ddmpc_19Apr_sim = ddmpc_19Apr_sim.set_index(pd.date_range("2015-04-19 00:00:00", periods=len(ddmpc_19Apr_sim), freq="1min").to_series())
    data[1][3] = insert_sim(df1=ddmpc_19Apr_sim, df2=data[1][3], start_index=pd.to_datetime("2015-04-19 22:55:00"),end_index=pd.to_datetime("2015-04-20 00:00:00"))

    # Corrected

    ampc_28Dec_sim = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files\ampc_sim_28Dec.csv", index_col=0)
    ampc_28Dec_sim = ampc_28Dec_sim.set_index(pd.date_range("2014-12-28 00:00:00", periods=len(ampc_28Dec_sim), freq="1min").to_series())
    data[2][1] = insert_sim(df1=ampc_28Dec_sim, df2=data[2][1], start_index=pd.to_datetime("2014-12-28 00:00:00"),end_index=pd.to_datetime("2014-12-28 04:00:00"))

    ampc_18Nov_sim = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files\ampc_sim_18Nov.csv", index_col=0)
    ampc_18Nov_sim = ampc_18Nov_sim.set_index(pd.date_range("2014-11-18 00:00:00", periods=len(ampc_18Nov_sim), freq="1min").to_series())
    data[2][0] = insert_sim(df1=ampc_18Nov_sim, df2=data[2][0], start_index=pd.to_datetime("2014-11-18 00:00:00"),end_index=pd.to_datetime("2014-11-18 04:00:00"))

    ampc_7Apr_sim = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files\ampc_sim_7Apr.csv",index_col=0)
    ampc_7Apr_sim = ampc_7Apr_sim.set_index(pd.date_range("2015-04-07 00:00:00", periods=len(ampc_7Apr_sim), freq="1min").to_series())
    data[2][2] = insert_sim(df1=ampc_7Apr_sim, df2=data[2][2], start_index=pd.to_datetime("2015-04-07 00:00:00"),end_index=pd.to_datetime("2015-04-07 04:00:00"))

    ampc_19Apr_sim = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files\ampc_sim_19Apr.csv",index_col=0)
    ampc_19Apr_sim = ampc_19Apr_sim.set_index(pd.date_range("2015-04-19 00:00:00", periods=len(ampc_19Apr_sim), freq="1min").to_series())
    data[2][3] = insert_sim(df1=ampc_19Apr_sim, df2=data[2][3], start_index=pd.to_datetime("2015-04-19 00:00:00"),end_index=pd.to_datetime("2015-04-19 04:00:00"))

    ddmpc_28Dec_sim = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files\ddmpc_sim_28Dec.csv",index_col=0)
    ddmpc_28Dec_sim = ddmpc_28Dec_sim.set_index(pd.date_range("2014-12-28 00:00:00", periods=len(ddmpc_28Dec_sim), freq="1min").to_series())
    data[1][1] = insert_sim(df1=ddmpc_28Dec_sim, df2=data[1][1], start_index=pd.to_datetime("2014-12-28 00:00:00"),end_index=pd.to_datetime("2014-12-28 04:00:00"))

    ddmpc_18Nov_sim = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files\ddmpc_sim_18Nov.csv",index_col=0)
    ddmpc_18Nov_sim = ddmpc_18Nov_sim.set_index(pd.date_range("2014-11-18 00:00:00", periods=len(ddmpc_18Nov_sim), freq="1min").to_series())
    data[1][0] = insert_sim(df1=ddmpc_18Nov_sim, df2=data[1][0], start_index=pd.to_datetime("2014-11-18 00:00:00"),end_index=pd.to_datetime("2014-11-18 04:00:00"))

    ddmpc_7Apr_sim = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files\ddmpc_sim_7Apr.csv",index_col=0)
    ddmpc_7Apr_sim = ddmpc_7Apr_sim.set_index(pd.date_range("2015-04-07 00:00:00", periods=len(ddmpc_7Apr_sim), freq="1min").to_series())
    data[1][2] = insert_sim(df1=ddmpc_7Apr_sim, df2=data[1][2], start_index=pd.to_datetime("2015-04-07 00:00:00"),end_index=pd.to_datetime("2015-04-07 04:00:00"))

    ddmpc_19Apr_sim = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files\ddmpc_sim_19Apr.csv",index_col=0)
    ddmpc_19Apr_sim = ddmpc_19Apr_sim.set_index(pd.date_range("2015-04-19 00:00:00", periods=len(ddmpc_19Apr_sim), freq="1min").to_series())
    data[1][3] = insert_sim(df1=ddmpc_19Apr_sim, df2=data[1][3], start_index=pd.to_datetime("2015-04-19 00:00:00"),end_index=pd.to_datetime("2015-04-19 04:00:00"))

    hc_28Dec_sim = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files\hc_sim_28Dec.csv",index_col=0)
    hc_28Dec_sim = hc_28Dec_sim.set_index(pd.date_range("2014-12-28 00:00:00", periods=len(hc_28Dec_sim), freq="1min").to_series())
    data[0][1] = insert_sim(df1=hc_28Dec_sim, df2=data[0][1], start_index=pd.to_datetime("2014-12-28 00:00:00"),end_index=pd.to_datetime("2014-12-28 04:00:00"))

    hc_18Nov_sim = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files\hc_sim_18Nov.csv",index_col=0)
    hc_18Nov_sim = hc_18Nov_sim.set_index(pd.date_range("2014-11-18 00:00:00", periods=len(hc_18Nov_sim), freq="1min").to_series())
    data[0][0] = insert_sim(df1=hc_18Nov_sim, df2=data[0][0], start_index=pd.to_datetime("2014-11-18 00:00:00"),end_index=pd.to_datetime("2014-11-18 04:00:00"))

    hc_7Apr_sim = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files\hc_sim_7Apr.csv",index_col=0)
    hc_7Apr_sim = hc_7Apr_sim.set_index(pd.date_range("2015-04-07 00:00:00", periods=len(hc_7Apr_sim), freq="1min").to_series())
    data[0][2] = insert_sim(df1=hc_7Apr_sim, df2=data[0][2], start_index=pd.to_datetime("2015-04-07 00:00:00"),end_index=pd.to_datetime("2015-04-07 04:00:00"))

    hc_19Apr_sim = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files\hc_sim_19Apr.csv",index_col=0)
    hc_19Apr_sim = hc_19Apr_sim.set_index(pd.date_range("2015-04-19 00:00:00", periods=len(hc_19Apr_sim), freq="1min").to_series())
    data[0][3] = insert_sim(df1=hc_19Apr_sim, df2=data[0][3], start_index=pd.to_datetime("2015-04-19 00:00:00"),end_index=pd.to_datetime("2015-04-19 04:00:00"))

    Energie_hc = []
    Energie_mpc = []
    Energie_ampc = []

    TD_hc = []
    TD_mpc = []
    TD_ampc = []

    Swi_hc = []
    Swi_mpc = []
    Swi_ampc = []

    SCOP_hc = []
    SCOP_mpc = []
    SCOP_ampc = []

    for i in range(len(ampc)):
        Energie_hc.append((hc[i]['ElektrischeEnergie'][-1] - hc[i]['ElektrischeEnergie'][0]) / 3600 / 1000)
        Energie_mpc.append((ddmpc[i]['ElektrischeEnergie'][-1] - ddmpc[i]['ElektrischeEnergie'][0]) / 3600 / 1000)
        Energie_ampc.append((ampc[i]['ElektrischeEnergie'][-1] - ampc[i]['ElektrischeEnergie'][0]) / 3600 / 1000)
        TD_hc.append(hc[i]["TError"].mask(hc[i]["TError"] > 0,0).abs().sum() / 60)
        TD_mpc.append(ddmpc[i]["TError"].mask(ddmpc[i]["TError"] > 0,0).abs().sum() / 60)
        TD_ampc.append(ampc[i]["TError"].mask(ampc[i]["TError"] > 0,0).abs().sum() / 60)
        Swi_hc.append(hc[i]['Switches'][-1] - hc[i]['Switches'][0])
        Swi_mpc.append(ddmpc[i]['Switches'][-1] - ddmpc[i]['Switches'][0])
        Swi_ampc.append(ampc[i]['Switches'][-1] - ampc[i]['Switches'][0])
        SCOP_hc.append(((hc[i]['Heizenergie'][-1] - hc[i]['Heizenergie'][0]) / 3600 / 1000) / Energie_hc[i])
        SCOP_mpc.append(((ddmpc[i]['Heizenergie'][-1] - ddmpc[i]['Heizenergie'][0]) / 3600 / 1000) / Energie_mpc[i])
        SCOP_ampc.append(((ampc[i]['Heizenergie'][-1] - ampc[i]['Heizenergie'][0]) / 3600 / 1000) / Energie_ampc[i])

    Energie = [Energie_hc, Energie_mpc, Energie_ampc]
    TD = [TD_hc, TD_mpc, TD_ampc]
    Swi = [Swi_hc, Swi_mpc, Swi_ampc]
    SCOP = [SCOP_hc, SCOP_mpc, SCOP_ampc]

    KPI_names = ["Energie", "Switches", "TD_0", "SCOP"]
    KPIs = [Energie, Swi, TD, SCOP]
    KPI_df = pd.DataFrame(KPIs, index=KPI_names, columns=["HK","MPC","aMPC"])
    Gewichte = [83,24,69,36]
    KPI_df["HK_year"] = np.zeros(len(KPI_df))
    KPI_df["MPC_year"] = np.zeros(len(KPI_df))
    KPI_df["aMPC_year"] = np.zeros(len(KPI_df))
    for i in range(4):
        divide = 1
        if KPI_names[i]=="Switches" or KPI_names[i]=="SCOP":
            divide=212
        KPI_df["HK_year"][KPI_names[i]] = sum(np.array(KPIs[i][0])*np.array(Gewichte))/divide
        KPI_df["MPC_year"][KPI_names[i]] = sum(np.array(KPIs[i][1]) * np.array(Gewichte))/divide
        KPI_df["aMPC_year"][KPI_names[i]] = sum(np.array(KPIs[i][2]) * np.array(Gewichte))/divide
    KPI_df.to_excel('D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\HiL_files\KPIs_corrected.xlsx')
    # KPIs = [Energie,TD,SCOP]
    # x = np.arange(len(days))
    # x = np.arange(4)
    # for i in range(len(labels)):
    #     plt.figure()
    #     width = 0.2
    #     plt.bar(x - 0.2, KPIs[i][0], width, color=colors[0])
    #     plt.bar(x, KPIs[i][1], width, color=colors[1])
    #     plt.bar(x + 0.2, KPIs[i][2], width, color=colors[2])
    #     plt.xticks(x, ['18.11.', '28.12', '07.04.', '19.04'])
    #     # plt.xticks(x, ['18.11.', '28.12', '07.04.'])
    #     plt.xlabel("Typtage")
    #     plt.ylabel(labels[i])
    #     plt.legend(["Heizkurve", "MPC", "aMPC"])
    #     plt.tight_layout()
    #     name = savenames[i] + '_' + 'Exp.pdf'
    #     plt.savefig(os.path.join("D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\HiL_files", name))
    #     plt.show()



