import pandas as pd
import matplotlib.pyplot as plt
import os

hellhellrotebc = (248/255,217/255,213/255)
hellrotebc = (241/255,179/255,171/255)
hellerrotebc = (235/255,140/255,129/255)
rotebc = (221/255,64/255,45/255)
dunkelrotebc = (172/255,43/255,28/255)
dunkeldunkelrotebc = (114/255,29/255,19/255)
dunkeldunkeldunkelrotebc = (76/255,17/255,12/255)
hellhellhellgrauebc = (242/255,242/255,242/255)
hellhellgrauebc = (235/255,236/255,236/255)
hellgrauebc = (216/255,216/255,217/255)
hellergrauebc = (196/255,197/255,198/255)
grauebc = (157/255,158/255,160/255)
dunkelgrauebc = (117/255,118/255,121/255)
dunkeldunkelgrauebc = (81/255,84/255,83/255)

path = r'D:\lma-dtu TEMP\masterarbeit\BESMod\BESMod\Resources'

up = pd.read_excel(os.path.join(path, 'UserProfilesHeatingPeriod.xlsx'), header=None)
temp = pd.read_excel(os.path.join(path,'VariableSetTemperature_StandardTime.xlsx'))


fig, axs = plt.subplots(2, 2)
axs[0,0].step(range(0, 25), temp['Temperature (K)'][264:289]-273.15, where='post', color = 'black')
axs[0,0].set_ylabel('Solltemperatur in °C')
axs[1,0].step(range(0, 25), up[1][263:288], where='post', color='black', linewidth = 2, label='Personen')
axs[1,0].step(range(0, 25), up[3][263:288], '--', where='post', color=rotebc, linewidth = 1, label='Licht')
axs[1,0].plot(range(0, 25), up[2][263:288], color=dunkelgrauebc, linewidth = 1, label='Maschinen')
axs[1,0].legend( loc='lower center', bbox_to_anchor=(0.5, 0), ncol=3)
axs[1,0].set_ylabel('Rel. Leistung in -')
axs[0,0].set_xlim([0, 24])
axs[1,0].set_xlim([0, 24])
axs[0,0].set_xlabel('Uhrzeit')
axs[1,0].set_xlabel('Uhrzeit')
axs[0,0].set_ylim([16.5,20.5])
axs[0,0].set_xticks(ticks=[0,8,16,24], labels=['00:00', '08:00', '16:00', '00:00'])
axs[1,0].set_xticks(ticks=[0,8,16,24], labels=['00:00', '08:00', '16:00', '00:00'])


axs[0,1].step(range(0, 25), temp['Temperature (K)'][120:145]-273.15, where='post', color = 'black')
axs[0,1].set_ylabel('Solltemperatur in °C')
axs[1,1].step(range(0, 25), up[1][119:144], where='post', color='black', linewidth = 2, label='Personen')
axs[1,1].step(range(0, 25), up[3][119:144], '--', where='post', color=rotebc, linewidth = 1, label='Licht')
axs[1,1].plot(range(0, 25), up[2][119:144], color=dunkelgrauebc, linewidth = 1, label='Maschinen')
# axs[1,1].legend(loc='upper center', framealpha=1, edgecolor='black')
axs[1,1].set_ylabel('Rel. Leistung in -')
axs[0,1].set_xlim([0, 24])
axs[1,1].set_xlim([0, 24])
axs[0,1].set_xlabel('Uhrzeit')
axs[1,1].set_xlabel('Uhrzeit')
axs[0,1].set_ylim([16.5,20.5])

axs[0,1].set_xticks(ticks=[0,8,16,24], labels=['00:00', '08:00', '16:00', '00:00'])
axs[1,1].set_xticks(ticks=[0,8,16,24], labels=['00:00', '08:00', '16:00', '00:00'])
axs[0,0].grid(linewidth=0.25)
axs[0,1].grid(linewidth=0.25)
axs[1,0].grid(linewidth=0.25)
axs[1,1].grid(linewidth=0.25)

plt.tight_layout()
plt.savefig(r'D:\lma-dtu TEMP\masterarbeit\Schriftliche Ausarbeitung\Graphiken\UserProfile&TargetTemp.pdf')
plt.show()
