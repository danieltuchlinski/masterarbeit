import numpy as np
import matplotlib.pyplot as plt

# Annahme: Sie haben bereits Daten für Stellgröße, Sollgröße und Istgröße vorliegen
# Erstellen Sie Beispieldaten (Sie sollten Ihre eigenen Daten verwenden)
time_steps = np.arange(0, 10, 1)  # Zeitpunkte
setpoint = np.array([20, 22, 24, 25, 26, 26, 26, 26, 26, 26])  # Sollgröße
output = np.array([18, 21, 22, 24, 25, 26, 26, 26, 26, 26])  # Istgröße
control_input = np.array([1, 1, 1, 1, 0.8, 0.6, 0.6, 0.6, 0.6, 0.6])  # Stellgröße

# Aktueller Zeitschritt (k)
current_step = 5

# Zeitschritte für die Vergangenheit und Zukunft
past_steps = 2
future_steps = 6

# Ausschneiden der relevanten Daten
past_setpoint = setpoint[current_step - past_steps:current_step + 1]
past_output = output[current_step - past_steps:current_step + 1]
future_setpoint = setpoint[current_step + 1:current_step + 1 + future_steps]
future_output = output[current_step + 1:current_step + 1 + future_steps]
future_control_input = control_input[current_step + 1:current_step + 1 + future_steps]

# Erstellen des Plots
plt.figure(figsize=(10, 6))

# Plot der Sollgröße und Istgröße
plt.plot(time_steps[current_step - past_steps:current_step + 1], past_setpoint, marker='o', label='Sollgröße (Vergangenheit)')
plt.plot(time_steps[current_step - past_steps:current_step + 1], past_output, marker='x', label='Istgröße (Vergangenheit)')
plt.plot(time_steps[current_step:current_step + future_steps + 1], future_setpoint, marker='o', label='Sollgröße (Zukunft)')
plt.plot(time_steps[current_step:current_step + future_steps + 1], future_output, marker='x', label='Istgröße (Zukunft)')

# Plot der Stellgröße
plt.step(time_steps[current_step:current_step + future_steps + 1], future_control_input, label='Stellgröße (Zukunft)', where='post')

# Beschriftungen und Legende
plt.xlabel('Zeitschritte')
plt.ylabel('Werte')
plt.title('Modellprädiktive Regelung')
plt.grid(True)
plt.legend()

# Anzeige des Plots
plt.show()