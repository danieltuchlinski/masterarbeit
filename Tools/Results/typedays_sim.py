from ebcpy import TimeSeriesData
import pandas as pd
import matplotlib.pyplot as plt
import os
import numpy as np

timeseries = True
KeyPerformance = False

path_hc = r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files\HeatingPeriod_1min_relevant\hc_relevant.csv"
path_ampc = r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files\HeatingPeriod_1min_relevant\ampc_relevant.csv"
path_ddmpc = r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files\HeatingPeriod_1min_relevant\ddmpc_relevant.csv"
hc = pd.read_csv(path_hc, index_col=0)
ampc = pd.read_csv(path_ampc, index_col=0)
ddmpc = pd.read_csv(path_ddmpc, index_col=0)

ampc = ampc.set_index(pd.date_range("2014-09-10 00:00:00", periods=len(ampc), freq="1min").to_series())
ddmpc = ddmpc.set_index(pd.date_range("2014-09-10 00:00:00", periods=len(ddmpc), freq="1min").to_series())
hc = hc.set_index(pd.date_range("2014-09-10 00:00:00", periods=len(hc), freq="1min").to_series())

startdate1 = "2014-11-18 00:00:00"
enddate1 = "2014-11-19 00:00:00"
startdate2 = "2014-12-28 00:00:00"
enddate2 = "2014-12-29 00:00:00"
startdate3 = "2015-04-07 00:00:00"
enddate3 = "2015-04-08 00:00:00"
startdate4 = "2015-04-19 00:00:00"
enddate4 = "2015-04-20 00:00:00"
startdates = [startdate1, startdate2, startdate3, startdate4]
enddates = [enddate1, enddate2, enddate3, enddate4]
days = ['18Nov','28Dec', '7Apr', '19Apr']
titles = ['18. November', '28. Dezember', '7. April', '19. April']
data= [hc,ddmpc,ampc]
types = ['Heizkurve', 'MPC', 'aMPC']
colors = ['lightgrey', 'lightblue', 'blue']

# savenames = ['hc','ddmpc','ampc']
# for i in range(len(data)):
#     for j in range(len(days)):
#         df = data[i][startdates[j]:enddates[j]]
#         name = savenames[i] + '_sim_' + days[j] + '.csv'
#         df.to_csv(name)


if timeseries:

    linewidths = [1,1,1]
    linestyles = ['dotted', 'solid', 'solid']

    for i in range(len(startdates)):
        fig, axs = plt.subplots(2)
        axs[0].plot(data[0]["Zonensolltemperatur"][startdates[i]:enddates[i]] - 273.15, color="black", label="Soll")
        axs[0].set_ylabel("Zonentemp. in °C")
        axs[0].set_xlabel("Uhrzeit")
        axs[0].set_xticks(ticks=pd.date_range(startdates[i], periods=7, freq="4h"), labels=['00:00','04:00','08:00','12:00','16:00','20:00','00:00'])
        axs[0].set_ylim(16, 21)
        axs[0].set_title(titles[i])
        axs[1].set_ylabel("Vorlauftemp. in °C")
        axs[1].set_ylim(15, 45)
        axs[1].set_xticks(ticks=pd.date_range(startdates[i], periods=7, freq="4h"),labels=['00:00', '04:00', '08:00', '12:00', '16:00', '20:00', '00:00'])
        axs[1].set_xlabel("Uhrzeit")
        for type in range(len(data)):
            axs[0].plot(data[type]["Zonentemperatur"][startdates[i]:enddates[i]] - 273.15, color=colors[type], label=types[type], linestyle=linestyles[type])
            axs[1].plot(data[type]["Vorlaufsolltemperatur"][startdates[i]:enddates[i]] - 273.15, color=colors[type], label=types[type], linestyle=linestyles[type])

        fig.tight_layout()
        axs[0].legend(loc="lower right")
        axs[1].legend(loc="lower right")
        name = days[i] + '.pdf'
        plt.savefig(os.path.join("D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files", name))
        plt.show()

labels = ['Elektrische Energie in kWh', 'Anzahl der Switches', 'Untere Solltemperaturabweichung in Kh', 'SCOP']
savenames = ['Energie', 'Switches', 'TD', 'SCOP']
if KeyPerformance:

    Energie_hc = []
    Energie_mpc = []
    Energie_ampc = []

    TD_hc = []
    TD_mpc = []
    TD_ampc = []

    Swi_hc = []
    Swi_mpc = []
    Swi_ampc = []

    SCOP_hc = []
    SCOP_mpc = []
    SCOP_ampc = []

    for i in range(len(startdates)):
        Energie_hc.append((hc['ElektrischeEnergie'][enddates[i]]-hc['ElektrischeEnergie'][startdates[i]]) / 3600 / 1000)
        Energie_mpc.append((ddmpc['ElektrischeEnergie'][enddates[i]] - ddmpc['ElektrischeEnergie'][startdates[i]]) / 3600 / 1000)
        Energie_ampc.append((ampc['ElektrischeEnergie'][enddates[i]] - ampc['ElektrischeEnergie'][startdates[i]]) / 3600 / 1000)
        TD_hc.append(hc["TError"][startdates[i]:enddates[i]].mask(hc["TError"][startdates[i]:enddates[i]] > 0, 0).abs().sum()/60)
        TD_mpc.append(ddmpc["TError"][startdates[i]:enddates[i]].mask(ddmpc["TError"][startdates[i]:enddates[i]] > 0,0).abs().sum() / 60)
        TD_ampc.append(ampc["TError"][startdates[i]:enddates[i]].mask(ampc["TError"][startdates[i]:enddates[i]] > 0,0).abs().sum() / 60)
        Swi_hc.append(hc['Switches'][enddates[i]] - hc['Switches'][startdates[i]])
        Swi_mpc.append(ddmpc['Switches'][enddates[i]] - ddmpc['Switches'][startdates[i]])
        Swi_ampc.append(ampc['Switches'][enddates[i]] - ampc['Switches'][startdates[i]])
        SCOP_hc.append(((hc['Heizenergie'][enddates[i]]-hc['Heizenergie'][startdates[i]]) / 3600 / 1000)/Energie_hc[i])
        SCOP_mpc.append(((ddmpc['Heizenergie'][enddates[i]] - ddmpc['Heizenergie'][startdates[i]]) / 3600 / 1000) / Energie_mpc[i])
        SCOP_ampc.append(((ampc['Heizenergie'][enddates[i]] - ampc['Heizenergie'][startdates[i]]) / 3600 / 1000) / Energie_ampc[i])

    Energie = [Energie_hc, Energie_mpc, Energie_ampc]
    TD = [TD_hc, TD_mpc, TD_ampc]
    Swi = [Swi_hc, Swi_mpc, Swi_ampc]
    SCOP = [SCOP_hc, SCOP_mpc, SCOP_ampc]

    KPIs= [Energie, Swi, TD, SCOP]
    x = np.arange(len(days))
    for i in range(len(labels)):

        plt.figure()
        width = 0.2
        plt.bar(x - 0.2, KPIs[i][0], width, color=colors[0])
        plt.bar(x, KPIs[i][1], width, color=colors[1])
        plt.bar(x + 0.2, KPIs[i][2], width, color=colors[2])
        plt.xticks(x, ['18.11.', '28.12', '07.04.', '19.04'])
        plt.xlabel("Typtage")
        plt.ylabel(labels[i])
        plt.legend(["Heizkurve", "MPC", "aMPC"])
        plt.tight_layout()
        name = savenames[i]+'_'+'Sim.pdf'
        plt.savefig(os.path.join("D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files", name))
        plt.show()

    n = [83, 24, 69, 36]

    startdate = "2014-10-01 00:00:00"
    enddate = "2015-05-01 00:00:00"
    Energie_tot_hc = (hc['ElektrischeEnergie'][enddate]-hc['ElektrischeEnergie'][startdate]) / 3600 / 1000
    Energie_tot_ddmpc = (ddmpc['ElektrischeEnergie'][enddate]-ddmpc['ElektrischeEnergie'][startdate]) / 3600 / 1000
    Energie_tot_ampc = (ampc['ElektrischeEnergie'][enddate]-ampc['ElektrischeEnergie'][startdate]) / 3600 / 1000
    TD_tot_hc = hc["TError"][startdate:enddate].mask(hc["TError"][startdate:enddate] > 0, 0).abs().sum()/60
    TD_tot_ddmpc = ddmpc["TError"][startdate:enddate].mask(ddmpc["TError"][startdate:enddate] > 0, 0).abs().sum()/60
    TD_tot_ampc = ampc["TError"][startdate:enddate].mask(ampc["TError"][startdate:enddate] > 0, 0).abs().sum()/60
    Swi_tot_hc = hc['Switches'][enddate] - hc['Switches'][startdate]
    Swi_tot_ddmpc = ddmpc['Switches'][enddate] - ddmpc['Switches'][startdate]
    Swi_tot_ampc = ampc['Switches'][enddate] - ampc['Switches'][startdate]
    SCOP_tot_hc = ((hc['Heizenergie'][enddate]-hc['Heizenergie'][startdate]) / 3600 / 1000)/Energie_tot_hc
    SCOP_tot_ddmpc = ((ddmpc['Heizenergie'][enddate]-ddmpc['Heizenergie'][startdate]) / 3600 / 1000)/Energie_tot_ddmpc
    SCOP_tot_ampc = ((ampc['Heizenergie'][enddate]-ampc['Heizenergie'][startdate]) / 3600 / 1000)/Energie_tot_ampc

    Energie_typ_tot_hc = sum(np.array(Energie_hc)*np.array(n))
    Energie_typ_tot_ddmpc = sum(np.array(Energie_mpc)*np.array(n))
    Energie_typ_tot_ampc = sum(np.array(Energie_ampc)*np.array(n))
    Swi_typ_tot_hc = sum(np.array(Swi_hc)*np.array(n))
    Swi_typ_tot_ddmpc = sum(np.array(Swi_mpc) * np.array(n))
    Swi_typ_tot_ampc = sum(np.array(Swi_ampc) * np.array(n))
    TD_typ_tot_hc = sum(np.array(TD_hc)*np.array(n))
    TD_typ_tot_ddmpc = sum(np.array(TD_mpc) * np.array(n))
    TD_typ_tot_ampc = sum(np.array(TD_ampc) * np.array(n))
    SCOP_typ_tot_hc = sum(np.array(SCOP_hc)*np.array(n))/212
    SCOP_typ_tot_ddmpc = sum(np.array(SCOP_mpc) * np.array(n))/212
    SCOP_typ_tot_ampc = sum(np.array(SCOP_ampc) * np.array(n))/212

    Energie_typ_tot = [Energie_typ_tot_hc,Energie_typ_tot_ddmpc,Energie_typ_tot_ampc]
    Swi_typ_tot = [Swi_typ_tot_hc/212, Swi_typ_tot_ddmpc/212, Swi_typ_tot_ampc/212]
    TD_typ_tot = [TD_typ_tot_hc, TD_typ_tot_ddmpc, TD_typ_tot_ampc]
    SCOP_typ_tot = [SCOP_typ_tot_hc, SCOP_typ_tot_ddmpc, SCOP_typ_tot_ampc]

    KPIs_typ = [Energie_typ_tot,Swi_typ_tot,TD_typ_tot,SCOP_typ_tot]

    Energie_err_hc = (Energie_typ_tot_hc-Energie_tot_hc)/Energie_tot_hc*100
    Energie_err_ddmpc = (Energie_typ_tot_ddmpc-Energie_tot_ddmpc)/Energie_tot_ddmpc*100
    Energie_err_ampc = (Energie_typ_tot_ampc-Energie_tot_ampc)/Energie_tot_ampc*100
    TD_err_hc = (TD_typ_tot_hc-TD_tot_hc)/TD_tot_hc*100
    TD_err_ddmpc = (TD_typ_tot_ddmpc-TD_tot_ddmpc)/TD_tot_ddmpc*100
    TD_err_ampc = (TD_typ_tot_ampc-TD_tot_ampc)/TD_tot_ampc*100
    Swi_err_hc = (Swi_typ_tot_hc-Swi_tot_hc)/Swi_tot_hc*100
    Swi_err_ddmpc = (Swi_typ_tot_ddmpc-Swi_tot_ddmpc)/Swi_tot_ddmpc*100
    Swi_err_ampc = (Swi_typ_tot_ampc-Swi_tot_ampc)/Swi_tot_ampc*100
    SCOP_err_hc = (SCOP_typ_tot_hc-SCOP_tot_hc)/SCOP_tot_hc*100
    SCOP_err_ddmpc = (SCOP_typ_tot_ddmpc-SCOP_tot_ddmpc)/SCOP_tot_ddmpc*100
    SCOP_err_ampc = (SCOP_typ_tot_ampc-SCOP_tot_ampc)/SCOP_tot_ampc*100

    Errors =[[Energie_err_hc,Energie_err_ddmpc,Energie_err_ampc],[TD_err_hc,TD_err_ddmpc,TD_err_ampc],[Swi_err_hc,Swi_err_ddmpc,Swi_err_ampc],[SCOP_err_hc,SCOP_err_ddmpc,SCOP_err_ampc]]

    KPI_names = ["Energie", "Switches", "TD_0", "SCOP"]
    KPIs = [Energie, Swi, TD, SCOP]
    KPI_df = pd.DataFrame(KPIs, index=KPI_names, columns=["HK","MPC","aMPC"])
    KPI_df["HK_year"] = np.zeros(len(KPI_df))
    KPI_df["MPC_year"] = np.zeros(len(KPI_df))
    KPI_df["aMPC_year"] = np.zeros(len(KPI_df))
    for i in range(4):
        KPI_df["HK_year"][KPI_names[i]] = KPIs_typ[i][0]
        KPI_df["MPC_year"][KPI_names[i]] = KPIs_typ[i][1]
        KPI_df["aMPC_year"][KPI_names[i]] = KPIs_typ[i][2]
    KPI_df.to_excel('D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\HiL_files\KPIs_Sim_Typ.xlsx')