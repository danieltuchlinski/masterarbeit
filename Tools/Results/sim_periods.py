from ebcpy import TimeSeriesData
import pandas as pd
import matplotlib.pyplot as plt
import os
import numpy as np
from sklearn.metrics import mean_absolute_error, r2_score

# Colors
hellhellrotebc = (248/255,217/255,213/255)
hellrotebc = (241/255,179/255,171/255)
hellerrotebc = (235/255,140/255,129/255)
rotebc = (221/255,64/255,45/255)
dunkelrotebc = (172/255,43/255,28/255)
dunkeldunkelrotebc = (114/255,29/255,19/255)
dunkeldunkeldunkelrotebc = (76/255,17/255,12/255)
hellhellhellgrauebc = (242/255,242/255,242/255)
hellhellgrauebc = (235/255,236/255,236/255)
hellgrauebc = (216/255,216/255,217/255)
hellergrauebc = (196/255,197/255,198/255)
grauebc = (157/255,158/255,160/255)
dunkelgrauebc = (117/255,118/255,121/255)
dunkeldunkelgrauebc = (81/255,84/255,83/255)

timeseries = False
KeyPerformance = True

path_hc = r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files\HeatingPeriod_1min_relevant\hc_relevant.csv"
path_ampc = r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files\HeatingPeriod_1min_relevant\ampc_relevant.csv"
path_ddmpc = r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Simulation_files\HeatingPeriod_1min_relevant\ddmpc_relevant.csv"

hc = pd.read_csv(path_hc, index_col=0)
ddmpc = pd.read_csv(path_ddmpc, index_col=0)
ampc = pd.read_csv(path_ampc, index_col=0)

ampc = ampc['2014-10-01 00:00:00':'2015-05-01 00:00:00']
ddmpc = ddmpc['2014-10-01 00:00:00':'2015-05-01 00:00:00']
hc = hc['2014-10-01 00:00:00':'2015-05-01 00:00:00']

ampc = ampc.set_index(pd.date_range("2014-10-01 00:00:00", periods=len(ampc), freq="1min").to_series())
ddmpc = ddmpc.set_index(pd.date_range("2014-10-01 00:00:00", periods=len(ddmpc), freq="1min").to_series())
hc = hc.set_index(pd.date_range("2014-10-01 00:00:00", periods=len(hc), freq="1min").to_series())

plt.show()
start_date = pd.to_datetime('2015-01-01 00:00:00')
end_date = pd.to_datetime('2015-01-08 00:00:00')
linewidths = [1.25, 2, 1.25]
linestyles = ['dashed', 'solid', 'solid']

types = [hc, ddmpc, ampc]
labels = ['Heizkurve', 'MPC', 'aMPC']
colors = [grauebc, dunkelgrauebc, rotebc]
fig, axs = plt.subplots(2, figsize=(8,4))
axs[0].plot(ampc["Zonensolltemperatur"][start_date:end_date] - 273.15, color='black', label="Soll")
for i in range(len(types)):
    axs[0].plot(types[i]["Zonentemperatur"][start_date:end_date] - 273.15, label=labels[i], color = colors[i], linestyle = linestyles[i], linewidth=linewidths[i])
    axs[1].plot(types[i]["Vorlaufsolltemperatur"][start_date:end_date] - 273.15, color = colors[i], linestyle = linestyles[i], linewidth=linewidths[i])

axs[0].set_ylim(16.5, 21)
axs[0].grid(True, linewidth=0.25)
axs[1].grid(True, linewidth=0.25)
axs[0].set_xlim(start_date, end_date)
axs[1].set_xlim(start_date, end_date)
axs[0].set_ylabel("Zonentemp. in °C")
axs[0].set_xlabel("Datum")
axs[1].set_ylabel("Sollvorlauftemp. in °C")
axs[1].set_ylim(18, 40)
axs[1].set_xticks(ticks=pd.date_range(start_date, periods=8, freq="24h"),labels=['01.01.', '02.01.', '03.01.', '04.01.', '05.01.', '06.01.', '07.01.', '08.01.'])
axs[1].set_xlabel("Datum")
axs[0].set_yticks([17,18,19,20,21])
axs[1].set_yticks([20,25,30,35,40])
axs[0].set_xticks(ticks=pd.date_range(start_date, periods=8, freq="24h"),labels=['01.01.', '02.01.', '03.01.', '04.01.', '05.01.', '06.01.', '07.01.', '08.01.'])
axs[0].legend(loc='upper center', bbox_to_anchor=(0.5, 1.4), ncol=4)
fig.tight_layout()


plt.savefig('ClosedLoop_Sim_January.pdf')
plt.show()

start_date = pd.to_datetime('2015-02-01 00:00:00')
end_date = pd.to_datetime('2015-03-01 00:00:00')
mae_february = mean_absolute_error(ampc['Vorlaufsolltemperatur'][start_date:end_date],ddmpc['Vorlaufsolltemperatur'][start_date:end_date])
mae_total = mean_absolute_error(ampc['Vorlaufsolltemperatur'],ddmpc['Vorlaufsolltemperatur'])
r2_february = r2_score(ampc['Vorlaufsolltemperatur'][start_date:end_date],ddmpc['Vorlaufsolltemperatur'][start_date:end_date])
r2_total = r2_score(ampc['Vorlaufsolltemperatur'],ddmpc['Vorlaufsolltemperatur'])


print("MAE Februar: " + str(mae_february) + " K")
print("MAE Total: " + str(mae_total) + " K")

print("R2 Februar: " + str(r2_february) + " K")
print("R2 Total: " + str(r2_total) + " K")
