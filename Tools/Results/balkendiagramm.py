import pandas as pd
import matplotlib.pyplot as plt
import os
import numpy as np

hellhellrotebc = (248/255,217/255,213/255)
hellrotebc = (241/255,179/255,171/255)
hellerrotebc = (235/255,140/255,129/255)
rotebc = (221/255,64/255,45/255)
dunkelrotebc = (172/255,43/255,28/255)
dunkeldunkelrotebc = (114/255,29/255,19/255)
dunkeldunkeldunkelrotebc = (76/255,17/255,12/255)
hellhellhellgrauebc = (242/255,242/255,242/255)
hellhellgrauebc = (235/255,236/255,236/255)
hellgrauebc = (216/255,216/255,217/255)
hellergrauebc = (196/255,197/255,198/255)
grauebc = (157/255,158/255,160/255)
dunkelgrauebc = (117/255,118/255,121/255)
dunkeldunkelgrauebc = (81/255,84/255,83/255)

KPIs_hc = [3,-1.5,26.8,2.3]
KPIs_mpc = [-0.2,-14.2,10.3,3.7]
KPIs_ampc = [-0.5,0.8,27.4,3.7]

KPIs = [KPIs_hc, KPIs_mpc, KPIs_ampc]
colors = [grauebc, dunkelgrauebc, rotebc]
plt.figure(figsize=(4,3.2))
width = 0.2
x = np.arange(len(KPIs_hc))
plt.grid(zorder=0)
for i in range(3):
    plt.bar(x - 0.2, KPIs[0], width, color=colors[0], zorder=3)
    plt.bar(x, KPIs[1], width, color=colors[1], zorder=3)
    plt.bar(x + 0.2, KPIs[2], width, color=colors[2], zorder=3)

plt.xticks(x, ['El. Energie', 'Unt. Solltemp.-abw.', 'Switches', 'SCOP'])
plt.xlabel("KPIs")
plt.ylim([-15,30])
plt.yticks([-15,-10,-5,0,5,10,15,20,25,30])
plt.ylabel('Abweichung in %')
plt.legend(["Heizkurve", "MPC", "aMPC"])
plt.tight_layout()

plt.savefig('ErrorKPIsAll.pdf')
plt.show()