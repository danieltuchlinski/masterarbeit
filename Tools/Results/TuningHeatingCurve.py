import pandas as pd
import matplotlib.pyplot as plt
import os

hellhellrotebc = (248/255,217/255,213/255)
hellrotebc = (241/255,179/255,171/255)
hellerrotebc = (235/255,140/255,129/255)
rotebc = (221/255,64/255,45/255)
dunkelrotebc = (172/255,43/255,28/255)
dunkeldunkelrotebc = (114/255,29/255,19/255)
dunkeldunkeldunkelrotebc = (76/255,17/255,12/255)
hellhellhellgrauebc = (242/255,242/255,242/255)
hellhellgrauebc = (235/255,236/255,236/255)
hellgrauebc = (216/255,216/255,217/255)
hellergrauebc = (196/255,197/255,198/255)
grauebc = (157/255,158/255,160/255)
dunkelgrauebc = (117/255,118/255,121/255)
dunkeldunkelgrauebc = (81/255,84/255,83/255)

folder = "PI_Controller_updated"

startdate = "2014-12-01 00:00:00"
enddate = "2014-12-01 23:50:00"

tuned = pd.read_csv(os.path.join("D:\lma-dtu TEMP\masterarbeit\Tools\Results", folder, "HeatingCurve.csv"), index_col=0, sep=",")
nottuned = pd.read_csv(os.path.join("D:\lma-dtu TEMP\masterarbeit\Tools\Results", folder, "HeatingCurve_NoTimeShift.csv"), index_col=0, sep=",")

fig, axs = plt.subplots(1, 2, figsize=(15.5/2.54,7/2.54))
axs[0].plot(nottuned["LowerBound(Zone Temperature)"][startdate:enddate]-273.15, color=dunkelgrauebc, label="Solltemperatur")
axs[0].plot(nottuned["outputs.building.TZone[1]"][startdate:enddate]-273.15, color="black", label="Zonentemperatur")
axs[0].set_xlim([0,144])
axs[0].set_ylim([16.5,21])
axs[0].set_xticks(ticks=[0, 48, 96, 144], labels=['00:00','08:00','16:00','00:00'])
axs[0].set_xlabel('Uhrzeit')
axs[0].set_ylabel('Zonentemperatur in °C')
# axs[0].legend(loc='lower left')


axs[1].plot(tuned["LowerBound(Zone Temperature)"][startdate:enddate]-273.15, color=dunkelgrauebc, label="Solltemperatur")
axs[1].plot(tuned["outputs.building.TZone[1]"][startdate:enddate]-273.15, color="black", label="Zonentemperatur")
axs[1].set_xlim([0,144])
axs[1].set_ylim([16.5,21])
axs[1].set_xticks(ticks=[0, 48, 96, 144], labels=['00:00','08:00','16:00','00:00'])
axs[1].set_xlabel('Uhrzeit')
axs[1].set_ylabel('Zonentemperatur in °C')
# axs[1].legend(loc='lower left')
axs[0].grid(True, linewidth=0.25)
axs[1].grid(True, linewidth=0.25)
plt.tight_layout()
plt.savefig(r'D:\lma-dtu TEMP\masterarbeit\Tools\Results\HC_Untuned.pdf')
plt.show()
