import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import os

start_date = "2015-02-01 00:00:00"
end_date = "2015-02-28 23:50:00"

folder = "PI_Controller_updated"
save_name = "NoFBH_OpenLoop.pdf"

prediction = pd.read_excel(r"D:\lma-dtu TEMP\masterarbeit\Tools\02 AddMo\02_Tool\Results\MA Daniel\KeineFußbodentemperatur\Predictions\ANN\Prediction_ann_bayesian_predictor_ANN.xlsx", index_col=0)
prediction.index = pd.date_range(start="2015-02-01 00:00:00", end="2015-02-28 23:50:00", freq="10min").strftime('%Y-%m-%d %H:%M:%s').to_series()
train1 = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\ddmpc.csv", sep=",", index_col=0)
train = pd.DataFrame(train1["u_T_VL []"][prediction.index[0]:prediction.index[-1]])

plt.figure(figsize=(15.5/2.54, 3))
plt.plot(train[start_date:end_date]-273.15, color="black", label="Messung")
plt.plot(prediction[start_date:end_date]-273.15, color="red", label="Vorhersage")
plt.ylabel("Sollvorlauftemp. in °C")
plt.xlabel("Datum")
plt.xlim([0,1008])
plt.legend()
plt.grid()
plt.xticks(ticks=[0,1008, 2016, 3024, 4032], labels=['01.02.','08.02.','15.02.','22.02.','01.03.',])
plt.tight_layout()
plt.savefig(os.path.join("D:\lma-dtu TEMP\masterarbeit\Tools\Results", folder, "Plots", save_name))
plt.show()
