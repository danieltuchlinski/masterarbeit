import pandas as pd
import os
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np

# Colors
hellhellrotebc = (248/255,217/255,213/255)
hellrotebc = (241/255,179/255,171/255)
hellerrotebc = (235/255,140/255,129/255)
rotebc = (221/255,64/255,45/255)
dunkelrotebc = (172/255,43/255,28/255)
dunkeldunkelrotebc = (114/255,29/255,19/255)
dunkeldunkeldunkelrotebc = (76/255,17/255,12/255)
hellhellhellgrauebc = (242/255,242/255,242/255)
hellhellgrauebc = (235/255,236/255,236/255)
hellgrauebc = (216/255,216/255,217/255)
hellergrauebc = (196/255,197/255,198/255)
grauebc = (157/255,158/255,160/255)
dunkelgrauebc = (117/255,118/255,121/255)
dunkeldunkelgrauebc = (81/255,84/255,83/255)

sim_energy = [[41.4,46.5,22.2,34.5],[39.1,44.2,20.2,31.0],[38.7,44.2,21.0,30.9]]
exp_energy = [[44.0,50.2,26.6,42.6],[48.0,49.8,24.9,41.9],[45.6,48.6,28.1,37.4]]
cor_energy = [[44.0,50.2,25.9,41.7],[45.1,47.8,22.9,39.2],[42.9,47.9,26.2,36.2]]

sim_td = [[1.746,1.916,3.386,3.775],[0.154,0.237,0.317,0.089],[0.063,0.219,0.749,0.581]]
exp_td = [[1.856,2.010,3.250,3.521],[0.825,0.641,0.696,0.977],[0.538,0.883,1.168,1.037]]
cor_td = [[1.776,1.985,3.355,3.560],[0.144,0.247,0.281,0.155],[0.106,0.749,0.886,0.825]]

sim_swi = [[2,7,20,32],[5,1,16,27],[9,0,17,31]]
exp_swi = [[7,3,17,10],[4,1,11,5],[6,0,6,18]]
cor_swi = [[7,3,17,10],[4,0,11,5],[6,0,6,18]]

sim_scop = [[3.104,2.993,3.197,3.080],[3.227,3.183,3.595,3.490],[3.232,3.183,3.606,3.497]]
exp_scop = [[3.176,2.968,3.006,2.687],[2.994,3.009,3.371,2.859],[3.156,3.094,2.969,3.235]]
cor_scop = [[3.147,2.936,3.060,2.714],[3.064,3.097,3.418,2.958],[3.195,3.071,3.054,3.208]]

sim = [sim_energy, sim_td, sim_swi, sim_scop]
exp = [exp_energy, exp_td, exp_swi, exp_scop]
cor = [cor_energy, cor_td, cor_swi, cor_scop]

methods = [sim, exp, cor]
method_names = ['Simulation', 'Original', 'Korrigiert']
labels = ['Heizkurve', 'MPC', 'aMPC']
typedays = ['18.11.','28.12.','07.04','19.04.']
KPIs = ['$E_{el}$ in kWh', '$TD_0$ in Kh', r'$Swi$ in $\frac{1}{d}$', '$SCOP$ in -']
row = [0,0,1,1]
col = [0,1,0,1]
colors = [grauebc, dunkelgrauebc, rotebc]
width=0.2
for method in range(len(methods)):
    fig, axs = plt.subplots(2,2)
    for j in range(len(sim)):
        x = np.arange(len(typedays))
        axs[row[j],col[j]].grid(zorder=0)
        for i in range(len(typedays)):
            axs[row[j],col[j]].bar(x - 0.2, methods[method][j][0], width, color=colors[0], zorder=3, label=labels[0])
            axs[row[j],col[j]].bar(x, methods[method][j][1], width, color=colors[1], zorder=3, label=labels[1])
            axs[row[j],col[j]].bar(x + 0.2, methods[method][j][2], width, color=colors[2], zorder=3, label=labels[2])
        axs[row[j],col[j]].set_xticks(x, typedays)
        axs[row[j],col[j]].set_xlabel("Typtage")
        axs[row[j], col[j]].set_ylabel(KPIs[j])
    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = dict(zip(labels, handles))
    plt.legend(by_label.values(), by_label.keys(), ncol=3, loc='lower center')
    fig.tight_layout()
    plt.savefig('KPIs_Typtage_'+method_names[method]+'.pdf')
    plt.show()