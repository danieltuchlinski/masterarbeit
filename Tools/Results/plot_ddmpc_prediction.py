import pandas as pd
import matplotlib.pyplot as plt
import os
import json

hellhellrotebc = (248/255,217/255,213/255)
hellrotebc = (241/255,179/255,171/255)
hellerrotebc = (235/255,140/255,129/255)
rotebc = (221/255,64/255,45/255)
dunkelrotebc = (172/255,43/255,28/255)
dunkeldunkelrotebc = (114/255,29/255,19/255)
dunkeldunkeldunkelrotebc = (76/255,17/255,12/255)
hellhellhellgrauebc = (242/255,242/255,242/255)
hellhellgrauebc = (235/255,236/255,236/255)
hellgrauebc = (216/255,216/255,217/255)
hellergrauebc = (196/255,197/255,198/255)
grauebc = (157/255,158/255,160/255)
dunkelgrauebc = (117/255,118/255,121/255)
dunkeldunkelgrauebc = (81/255,84/255,83/255)

ddmpc = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\ddmpc.csv", index_col=0)

startdate = "2014-12-28 00:00:00"
enddate = "2014-12-29 00:00:00"
ddmpc = ddmpc[:][startdate:enddate]

fig, axs = plt.subplots(2)

axs[0].plot(ddmpc['LowerBound(Zone Temperature) []']-273.15, color='black', label='Soll')
axs[0].plot(ddmpc['outputs.building.TZone[1] []']-273.15, color=dunkelgrauebc, linewidth=1.5, label='Ist')

axs[1].plot(ddmpc['u_T_VL []']-273.15, color=dunkelgrauebc, linewidth=1.5, label='Real')


axs[0].set_ylabel("Zonentemp. in °C")
axs[0].set_xlabel("Uhrzeit")
axs[1].set_ylabel("Sollvorlauftemp. in °C")
axs[1].set_xlabel("Uhrzeit")
axs[0].set_xticks(ticks=pd.date_range(ddmpc.index[0], periods=7, freq="4h").strftime('%Y-%m-%d %H:%M:%s').to_series(),labels=['00:00', '04:00', '08:00', '12:00', '16:00', '20:00', '00:00'])
axs[1].set_xticks(ticks=pd.date_range(ddmpc.index[0], periods=7, freq="4h").strftime('%Y-%m-%d %H:%M:%s').to_series(),labels=['00:00', '04:00', '08:00', '12:00', '16:00', '20:00', '00:00'])
axs[0].set_ylim(19, 21)
axs[1].set_ylim(15, 45)
axs[0].set_xlim(startdate, enddate)
axs[1].set_xlim(startdate, enddate)

dates = ['2014-12-28 10:10:00', ]
prediction_TZone = ddmpc["predictions_TZone []"][dates]
prediction_TVL = ddmpc["predictions_TVL []"][dates]
for timestamp in prediction_TZone.index:
    past_dates = list(pd.date_range(end=timestamp, periods=7, freq="10min").strftime('%Y-%m-%d %H:%M:%s').to_series())
    future_dates = list(pd.date_range(start=timestamp, periods=36, freq="10min").strftime('%Y-%m-%d %H:%M:%s').to_series())[1:]
    dates = past_dates + future_dates
    pred_TZone_df = pd.DataFrame(json.loads(prediction_TZone[timestamp]), index=dates)
    pred_TVL_df = pd.DataFrame(json.loads(prediction_TVL[timestamp]), index=dates)
    axs[0].plot(pred_TZone_df[timestamp:future_dates[-1]] - 273.15, "--", color=dunkeldunkelgrauebc, linewidth=1, label='Prädiziert')
    axs[1].plot(pred_TVL_df[timestamp:future_dates[-1]] - 273.15, "--", color=dunkeldunkelgrauebc, linewidth=1, label='Prädiziert')

axs[0].legend(loc="lower right")
axs[1].legend(loc="lower right")
axs[0].grid(True)
axs[1].grid(True)
plt.tight_layout()
plt.savefig(r'D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\Plots\ddmpc_bad_processmodel.pdf')
plt.show()
print('Debugger')