import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import os
matplotlib.rcParams.update({'font.size': 10})
import json

########################################################################################################################
folder = "PI_Controller_updated"
save_name = ""
save = False

startdate = "2014-12-28 00:00:00"
enddate = "2014-12-28 23:50:00"
show_ddmpc_prediction = False
########################################################################################################################
ampc = pd.read_csv(os.path.join("D:\lma-dtu TEMP\masterarbeit\Tools\Results", folder, "ampc.csv"), index_col=0, sep=",")
ddmpc = pd.read_csv(os.path.join("D:\lma-dtu TEMP\masterarbeit\Tools\Results", folder, "ddmpc.csv"), index_col=0, sep=",")
hc = pd.read_csv(os.path.join("D:\lma-dtu TEMP\masterarbeit\Tools\Results", folder, "HeatingCurve.csv"), index_col=0, sep=",")

fig, axs = plt.subplots(4, figsize=(8, 10))

# axs[0].set_title("1.-7. April", size=)
axs[0].plot(ddmpc["LowerBound(Zone Temperature) []"][startdate:enddate]-273.15, color="lightgrey", label="Solltemperatur")
# axs[0].plot(hc["outputs.building.TZone[1]"][startdate:enddate]-273.15, "-", color="blue", label="Heizkurve")
# axs[0].plot(ddmpc["outputs.building.TZone[1] []"][startdate:enddate]-273.15, color="black", label="ddMPC")
axs[0].plot(ampc["outputs.building.TZone[1]"][startdate:enddate]-273.15, color="red", label="aMPC")
axs[0].set_ylabel("Zonentemp. in °C", size=14)
axs[0].set_xticks([])
axs[0].set_ylim(16,21)
axs[0].legend(loc="lower right")

# axs[1].plot(hc["hydraulic.control.heatingCurveDaniel.TSet"][startdate:enddate]-273.15, "--", color="blue", label="Heizkurve", linewidth=0.5)
# axs[1].plot(hc["hydraulic.generation.sigBusGen.THeaPumOut"][startdate:enddate]-273.15, color="green", label="Heizkurve Real")
# axs[1].plot(ddmpc["hydraulic.generation.sigBusGen.THeaPumOut []"][startdate:enddate]-273.15, color="green", label="ddMPC Real")
# axs[1].plot(ddmpc["u_T_VL []"][startdate:enddate]-273.15, color="black", label="ddMPC Set")
axs[1].plot(ampc["u_T_VL"][startdate:enddate]-273.15, color="red", label="aMPC")
axs[1].plot(ampc["hydraulic.generation.sigBusGen.THeaPumOut"][startdate:enddate]-273.15, color="green", label="aMPC Real")
axs[1].set_ylabel("Vorlauftemp. in °C", size=14)
axs[1].set_xticks([])
# axs[1].set_xlabel("Datum", size=20)

# axs[2].plot(ddmpc["hydraulic.control.sigBusGen.yHeaPumSet []"][startdate:enddate], color="black", label="ddMPC")
# axs[2].plot(ddmpc["hydraulic.control.pI_InverterHeatPumpController.n_Set []"][startdate:enddate], color="red", label="ddMPC")
axs[2].plot(ampc["hydraulic.control.sigBusGen.yHeaPumSet"][startdate:enddate], color="red", label="aMPC")
# axs[2].plot(hc["hydraulic.control.sigBusGen.yHeaPumSet"][startdate:enddate], color="blue", label="HC")
axs[2].set_ylabel("Rel. Leistung WP", size=14)
axs[2].set_xticks([])

axs[3].plot(ddmpc["weaDat.weaBus.TDryBul []"][startdate:enddate]-273.15)
axs[3].set_ylabel("Außentemp. in °C", size=14)
axs[3].set_xlabel("Datum", size=16)
axs[3].set_xticks([])

# axs[4].plot(ddmpc["weaDat.weaBus.HDirNor []"][startdate:enddate])
# axs[4].set_ylabel("Strahlung in W/m²", size=14)
# axs[4].set_xlabel("Datum", size=16)
# axs[4].set_xticks([])

if show_ddmpc_prediction:
    # dates = list(pd.date_range(start=startdate, end=enddate, freq="6h").strftime('%Y-%m-%d %H:%M:%s').to_series())
    dates = ['2014-12-28 09:20:00',]
    prediction_TZone = ddmpc["predictions_TZone []"][dates]
    prediction_TVL = ddmpc["predictions_TVL []"][dates]
    for timestamp in prediction_TZone.index:
        past_dates = list(pd.date_range(end=timestamp, periods=7, freq="10min").strftime('%Y-%m-%d %H:%M:%s').to_series())
        future_dates = list(pd.date_range(start=timestamp, periods=36, freq="10min").strftime('%Y-%m-%d %H:%M:%s').to_series())[1:]
        dates = past_dates + future_dates
        pred_TZone_df = pd.DataFrame(json.loads(prediction_TZone[timestamp]), index=dates)
        pred_TVL_df = pd.DataFrame(json.loads(prediction_TVL[timestamp]), index=dates)
        axs[0].plot(pred_TZone_df[timestamp:future_dates[-1]] - 273.15, "--", color="red", linewidth=1)
        axs[1].plot(pred_TVL_df[timestamp:future_dates[-1]] - 273.15, "--", color="black", linewidth=0.5)
# axs[0].legend(loc="lower left")
if save:
    plt.savefig(os.path.join("D:\lma-dtu TEMP\masterarbeit\Tools\Results", folder, "Plots", save_name))
plt.show()

