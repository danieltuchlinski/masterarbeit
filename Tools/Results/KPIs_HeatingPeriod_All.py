import matplotlib.pyplot as plt

hellhellrotebc = (248/255,217/255,213/255)
hellrotebc = (241/255,179/255,171/255)
hellerrotebc = (235/255,140/255,129/255)
rotebc = (221/255,64/255,45/255)
dunkelrotebc = (172/255,43/255,28/255)
dunkeldunkelrotebc = (114/255,29/255,19/255)
dunkeldunkeldunkelrotebc = (76/255,17/255,12/255)
hellhellhellgrauebc = (242/255,242/255,242/255)
hellhellgrauebc = (235/255,236/255,236/255)
hellgrauebc = (216/255,216/255,217/255)
hellergrauebc = (196/255,197/255,198/255)
grauebc = (157/255,158/255,160/255)
dunkelgrauebc = (117/255,118/255,121/255)
dunkeldunkelgrauebc = (81/255,84/255,83/255)

sim_energy = [7042,6757,6794]
sim_type_energy = [7326,6811,6830]
uncor_energy = [8228,8407,8239]
cor_energy = [8139,7880,7818]
energy= [sim_energy,sim_type_energy, uncor_energy,cor_energy]

sim_swi = [10.8,10.7,11.3]
sim_type_swi = [13.5,11.9,14.3]
uncor_swi = [10.3,6.1,7.4]
cor_swi = [10.3,6.0,7.4]
swi= [sim_swi, sim_type_swi, uncor_swi,cor_swi]

sim_td = [574,55,83]
sim_type_td = [560,44,83]
uncor_td = [553,167,184]
cor_td = [555,43,118]
td= [sim_td,sim_type_td, uncor_td,cor_td]

sim_scop = [3.03,3.22,3.23]
sim_type_scop = [3.12,3.39,3.39]
uncor_scop = [3.01,3.10,3.10]
cor_scop = [3.02,3.17,3.14]
scop= [sim_scop, sim_type_scop, uncor_scop,cor_scop]

KPIs= [energy, swi, td, scop]

colors = [grauebc, dunkelgrauebc,rotebc]
markers = ['x','o' ,'s', 'v']

labels1 = ['Heizkurve', 'MPC', 'aMPC']
labels2 = ['Sim','SimTypedays', 'Ori', 'Kor']
fig, axs = plt.subplots(1,2, figsize=(6,3))

for method in range(4):
    for type in range(3):
        axs[0].scatter(KPIs[2][method][type], KPIs[0][method][type], color=colors[type],marker =markers[method],label=labels1[type]+labels2[method],zorder=3)
        axs[1].scatter(KPIs[3][method][type], KPIs[1][method][type], color=colors[type],marker = markers[method],zorder=3)

axs[0].set_ylim(6500, 8500)
axs[0].set_xlim(0, 600)
axs[1].set_xlim(3, 3.4)
axs[0].set_ylabel("El. Energie in kWh")
axs[0].set_xlabel("Unt. Solltemp.-abw. in Kh")
axs[1].set_ylabel("Switches in 1/Tag")
axs[1].set_ylim(5,15)
axs[1].set_xticks([3.0,3.1,3.2,3.3,3.4])
axs[1].set_xlabel("SCOP in -")
axs[0].set_yticks([6500, 7000, 7500, 8000, 8500])
axs[1].set_yticks([5, 7, 9, 11, 13, 15])
axs[0].set_xticks([0, 200, 400, 600])
fig.legend(loc='upper center', ncol=3)
axs[0].grid(zorder=0)
axs[1].grid(zorder=0)
fig.tight_layout()

plt.savefig("KPIs_HeatingPeriod_All.pdf")

plt.show()