import datetime

display_time_offset = 1410300000
display_time_format = '%m.%d.%Y - %H:%M'

time = datetime.datetime.fromtimestamp(display_time_offset)

print(time.strftime(display_time_format))