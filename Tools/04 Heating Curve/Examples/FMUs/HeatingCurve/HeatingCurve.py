from Examples.FMUs.HeatingCurve.config import *

""" 
This script is used to generate the necessary training data
"""

TAirZone.mode = Economic( # Set the Air temperature mode to random for identification
    day_start=8,
    day_end=17,
    day_lb=273.15 + 17,
    night_lb=273.15 + 20,
    day_ub=273.15 + 25,
    night_ub=273.15 + 22,
    time_offset=1410303600,
)

# Simulate system for given time using the defined base controllers
# -----------------------------------------------------------------------------------------
system.setup(start_time=one_day * 0)
system.define_control_input({'b_MPC_input': False, 'b_u_comp_ctrl': False, })

HC_data = DataHandler(
    [
        system.run(duration=one_day*252),
    ]
)

system.close()

HC_data.save("2minstepsize", override=True)
# --------------------------------------------------------------------

df = HC_data.containers[0].df

df.index = pd.date_range("2014-09-10 00:00", periods=181440, freq="2min").to_series()
df.to_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\2minstepsize.csv")





