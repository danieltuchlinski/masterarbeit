from ddmpc import *
import sys

sys.setrecursionlimit(10000)
"""
This script is used to define the system, 
the relevant variables and what to plot during simulation
"""

""" Define the features (Variables) of your system """
T_VL_Set = Control(  # control variables are manipulated by the controller
    source=Readable(
        name="T VL Set",
        read_name="u_T_VL",
        plt_opts=red_line,
    ),
    lb=273.15+20,
    ub=273.15+40,
    default=273.15+20,  # without controller the default value is active
)

T_VL_Set_change = Connection(
    Change(base=T_VL_Set)
)  # later we want to penalize the change to prevent oscillations

TAirZone = Controlled(  # The room temperature should be controlled
    source=Readable(
        name="Zone Temperature",  # internal name
        read_name="outputs.building.TZone[1]",  # name of datapoint or FMU variable
        plt_opts=red_line,  # here some customization for plotting
    ),
    mode=Random(),  # Control mode (defines boundaries and control goal etc
)

TAirZone_change = Connection(
    Change(base=TAirZone)
)  # we want to predict the temperature change

dry_bul = Disturbance(  # for disturbances a forecast is needed
    Readable(
        name="Outside Temperature", read_name="weaDat.weaBus.TDryBul", plt_opts=light_red_line
    )
)

rad_dir = Disturbance(
    Readable(name="Dir. Rad.", read_name="weaDat.weaBus.HDirNor", plt_opts=red_line)
)

# Define additional constructed features
def sin_d(t):
    return np.sin(2 * np.pi * t / 86400)

def cos_d(t):
    return np.cos(2 * np.pi * t / 86400)

def sin_w(t):
    return np.sin(2 * np.pi * t / 604800)

def cos_w(t):
    return np.cos(2 * np.pi * t / 604800)

def logistic(x):
    return 1 / (1 + np.exp((-x + 0.33) * 10))

u_real = Tracking(
    Readable(name="u real", read_name="hydraulic.control.sigBusGen.yHeaPumSet", plt_opts=blue_line)
)

Power = Tracking(
    Readable(name="Power", read_name="hydraulic.generation.heatPump.innerCycle.Pel", plt_opts=blue_line)
)

T_VL = Tracking(
    Readable(name="T VL", read_name="hydraulic.generation.sigBusGen.THeaPumOut", plt_opts=light_grey_line)
)

NumSwi = Tracking(
    Readable(name="Switches", read_name="hydraulic.generation.outBusGen.heaPum.numSwi", plt_opts=light_grey_line)
)

W_el = Tracking(
    Readable(name="Energy consumed", read_name="outputs.hydraulic.gen.PEleHP.integral", plt_opts=red_line)
)

Q_zu = Tracking(
    Readable(name="Energy produced", read_name="outputs.hydraulic.gen.QHP_flow.integral", plt_opts=blue_line)
)

T_Fussboden = Tracking(
    Readable(name="T Fussboden", read_name="hydraulic.transfer.panelHeating[1].panelHeatingSegment[3].panel_Segment1.heatCapacitor.T", plt_opts=blue_line)
)
T_Ruecklauf = Tracking(
    Readable(name="T Rücklauf", read_name="hydraulic.generation.sigBusGen.THeaPumIn", plt_opts=blue_line)
)
T_him = Tracking(
    Readable(name="Sky temp.", read_name="weaDat.weaBus.TBlaSky", plt_opts=blue_line)
)

T_tau = Tracking(
    Readable(name="Dew temp.", read_name="weaDat.weaBus.TDewPoi", plt_opts=blue_line)
)

T_Error = Tracking(
    Readable(name="T Error", read_name="TError", plt_opts=blue_line)
)

T_HeatingCurveSet = Tracking(
    Readable(name="TsetHC", read_name="hydraulic.control.heatingCurveDaniel.TSet", plt_opts=blue_line)
)

Opening = Tracking(
    Readable(name="Opening", read_name="hydraulic.control.sigBusTra.opening[1]", plt_opts=blue_line)
)

# here the daytime and day of the week encoded as sin/cos are used to learn user behavior
daily_sin = Disturbance(TimeFunc(name="daily_sin", func=sin_d))
daily_cos = Disturbance(TimeFunc(name="daily_cos", func=cos_d))
weekly_sin = Disturbance(TimeFunc(name="weekly_sin", func=sin_w))
weekly_cos = Disturbance(TimeFunc(name="weekly_cos", func=cos_w))

""" Define the controlled system """
model = Model(*Feature.all)  # pass all features to the model

system = FMU(
    model=model, step_size=60 * 2, name="PI_Controller_updated.fmu"
)  # initialize system

""" Define the Inputs and Outputs of the
 process models using the Training data class"""
TAirZone_TrainingData = TrainingData(
    inputs=Inputs(
        Input(TAirZone, lag=6),  # the lags define the considered time steps
        Input(T_VL_Set, lag=6),
        Input(dry_bul, lag=2),
        Input(rad_dir, lag=2),
        Input(daily_sin, lag=2),
        Input(daily_cos, lag=2),
        Input(weekly_sin, lag=2),
        Input(weekly_cos, lag=2),
    ),
    output=Output(source=TAirZone_change),
    step_size=60 * 10,
)

Power_TrainingData = TrainingData(
    inputs=Inputs(
        Input(TAirZone_change, lag=1),  # the lags define the considered time steps
        Input(T_VL_Set, lag=1),
        Input(dry_bul, lag=1),
    ),
    output=Output(source=Power),
    step_size=60 * 10,
)

""" Define which quantities should be plotted """
pid_plotter = Plotter(
    SubPlot(features=[TAirZone], y_label="Air Temp. [°C]", shift=273.15, legend=False),
    SubPlot(
        features=[T_VL],
        y_label="T VL Set [°C]",
        shift=273.15,
        lb=15,
        ub=80,
        step=True,
        legend=False,
    ),
    SubPlot(features=[u_real], y_label="u real [-]", shift=0, legend=False),
    SubPlot(features=[dry_bul], y_label='Ambient temp [°C]', shift=273.15, legend=False),
)
mpc_plotter = Plotter(
    SubPlot(features=[TAirZone], y_label="Air Temp. [°C]", shift=273.15, legend=False),
    SubPlot(
        features=[T_VL_Set, T_VL],
        # features=[uCompSet],
        y_label="T VL Set [°C]",
        # y_label="u set [-]",
        shift=273.15,
        lb=15,
        ub=50,
        # shift=0,
        # lb=0,
        # ub=8 / 9,
        step=True,
        legend=False,
    ),
    SubPlot(features=[u_real], y_label="u real [-]", normalize=True),
    SubPlot(features=[dry_bul], y_label='Ambient temp [°C]', shift=273.15, legend=False),
)
