import pandas as pd
import os

folder = "Standard_PI_changed"

# Heating Period
start_date = "2014-10-01 00:00:00"
end_date = "2015-04-30 23:50:00"

# Read relevant data
ampc = pd.read_csv(os.path.join(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results", folder, "ampc.csv"), sep=",", index_col=0)[start_date:end_date]
ddmpc = pd.read_csv(os.path.join(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results", folder, "ddmpc.csv"), sep=",", index_col=0)[start_date:end_date]

diff_TVL = abs(ampc["u_T_VL"]-ddmpc["u_T_VL []"])
diff_TZ = abs(ampc["outputs.building.TZone[1]"]-ddmpc["outputs.building.TZone[1] []"])

TVL_list = []
TZ_list = []
for i in range(212):
    TVL_list.append(sum(diff_TVL.iloc[i*144:(i+1)*144]))
    TZ_list.append(sum(diff_TZ.iloc[i * 144:(i + 1) * 144]))

TVL_list = pd.Series(TVL_list, name="TVL_diff", index=list(pd.date_range(start=start_date, end=end_date, freq="24h").strftime('%Y-%m-%d').to_series()))
TZ_list = pd.Series(TZ_list, name="TZ_diff", index=list(pd.date_range(start=start_date, end=end_date, freq="24h").strftime('%Y-%m-%d').to_series()))

results = pd.concat([TVL_list, TZ_list], axis=1)
print("dd")


