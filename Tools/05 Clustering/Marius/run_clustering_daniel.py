from __future__ import division
import numpy as np
import pandas as pd
import clustering_medoid as clustering
import datetime
import matplotlib.pyplot as plt
import os
from sklearn.metrics import r2_score, mean_squared_error, mean_absolute_error
from itertools import combinations, product
import time

def eval_clustering(datas, weights, ampc, ddmpc, hc, sizes):
    weight_string = [str(round(x, 2)) for x in weights]
    save_name = "".join(sizes)+ "_" + "".join(weight_string)

    # KPIs ddmpc
    Energy_ddmpc = (ddmpc['outputs.hydraulic.gen.PEleHP.integral []'][-1] - ddmpc['outputs.hydraulic.gen.PEleHP.integral []'][0])/1000/3600
    Diskomfort_ddmpc = ddmpc["TError []"].mask(ddmpc["TError []"] > 0, 0).abs().sum()/6
    Switches_ddmpc = ddmpc['hydraulic.generation.outBusGen.heaPum.numSwi []'][-1] - ddmpc['hydraulic.generation.outBusGen.heaPum.numSwi []'][0]
    # KPIs ampc
    Energy_ampc = (ampc['outputs.hydraulic.gen.PEleHP.integral'][-1] - ampc['outputs.hydraulic.gen.PEleHP.integral'][0])/1000/3600
    Diskomfort_ampc = ampc["TError"].mask(ampc["TError"] > 0, 0).abs().sum()/6
    Switches_ampc = ampc['hydraulic.generation.outBusGen.heaPum.numSwi'][-1] - ampc['hydraulic.generation.outBusGen.heaPum.numSwi'][0]
    # KPIs Heating Curve
    Energy_hc = (hc['outputs.hydraulic.gen.PEleHP.integral'][-1] - hc['outputs.hydraulic.gen.PEleHP.integral'][0])/1000/3600
    Diskomfort_hc = hc["TError"].mask(hc["TError"] > 0, 0).abs().sum()/6
    Switches_hc = hc['hydraulic.generation.outBusGen.heaPum.numSwi'][-1] - hc['hydraulic.generation.outBusGen.heaPum.numSwi'][0]

    # Initialize lists
    raw_inputs = {}

    # Unscaled inputs
    for data in datas.keys():
        raw_inputs[data] = datas[data]

    # Tolerance for clustering
    Mip_Gap = 0.01

    # Clustering Inputdata
    #set the range of typedays you want to analyse here!
    first = 1
    last = 6
    distance = 1
    typedays = range(first, last+1, distance)

    results = pd.DataFrame(columns=["Anzahl", "Datum", "Gewichtung Typtage", "KPIs Typtage aMPC", "KPIs TRY aMPC", "Fehler aMPC", "KPIs Typtage ddMPC", "KPIs TRY ddMPC", "Fehler ddMPC", "KPIs Typtage HC", "KPIs TRY HC", "Fehler HC"], index=range(len(typedays)))
    row = 0
    # clustering various inputs
    for i in typedays:

        number_clusters = i
        inputs_cluster = []
        for data in datas.keys():
            inputs_cluster.append(raw_inputs[data])

        (inputs, nc, z, inputsTransformed) = clustering.cluster(np.array(inputs_cluster), number_clusters, norm=2, mip_gap=Mip_Gap, weights=weights)

        # Set Names for Data nameing
        dataname = "Cluster_Alle_"+str(first)+'_'+str(number_clusters)+'_'+str(distance)+str('_'+str(weights)+'_')+str(Mip_Gap).replace('.', '')

        # Determine time steps per day
        len_day = int(np.array(inputs_cluster).shape[1] / 212)

        print("Clustering with "+str(i)+" clusters successful!")

        clustered = {}
        numb = 0
        for data in datas.keys():
            clustered[data+"_"+str(i)] = inputs[numb]
            numb += 1

        clustered["weights"+str(i)] = nc
        clustered["mapping_"+str(i)] = z

        # only 0 and 1
        for m in range(len(z)):
            for n in range(len(z)):
                if z[n, m] < 1:
                    z[n, m] = 0

        # build matrix map_days indicating which day of the year is assigned to which typeday
        map_days = np.zeros([len(z), len(z)])
        times_cluster = np.sum(z, axis=1)
        j = 1
        for t in range(len(times_cluster)):
            if times_cluster[t] > 0:
                map_days[t, :] = z[t, :]*j
                j = j+1

        clustered_timeseries = {}
        for data in datas.keys():
            clustered_timeseries[data] = []

        for m in range(len(z)):
            for n in range(len(z)):
                if map_days[n, m] > 0:
                    for data in datas.keys():
                        clustered_timeseries[data] = np.append(clustered_timeseries[data], clustered[data+"_"+str(i)][int(map_days[n, m] - 1)])

        # Calculate KPIs for typedays
        Energy_Typ_ampc = 0
        Diskomfort_Typ_ampc = 0
        Switches_Typ_ampc = 0
        Energy_Typ_ddmpc = 0
        Diskomfort_Typ_ddmpc = 0
        Switches_Typ_ddmpc = 0
        Energy_Typ_hc = 0
        Diskomfort_Typ_hc = 0
        Switches_Typ_hc = 0
        days = list(np.nonzero(times_cluster))
        for day in days[0]:
            Energy_Typ_ampc = Energy_Typ_ampc + ((ampc['outputs.hydraulic.gen.PEleHP.integral'][(day+1)*144-1]-ampc['outputs.hydraulic.gen.PEleHP.integral'][day*144])/1000/3600) * times_cluster[day]
            Diskomfort_Typ_ampc = Diskomfort_Typ_ampc + (ampc["TError"][day*144:(day+1)*144].mask(ampc["TError"][day*144:(day+1)*144] > 0, 0).abs().sum()/6)*times_cluster[day]
            Switches_Typ_ampc = Switches_Typ_ampc + (ampc['hydraulic.generation.outBusGen.heaPum.numSwi'][(day+1)*144-1]-ampc['hydraulic.generation.outBusGen.heaPum.numSwi'][day*144])*times_cluster[day]
            Energy_Typ_ddmpc = Energy_Typ_ddmpc + ((ddmpc['outputs.hydraulic.gen.PEleHP.integral []'][(day + 1) * 144 - 1] - ddmpc['outputs.hydraulic.gen.PEleHP.integral []'][day * 144]) / 1000 / 3600) * times_cluster[day]
            Diskomfort_Typ_ddmpc = Diskomfort_Typ_ddmpc + (ddmpc["TError []"][day * 144:(day + 1) * 144].mask(ddmpc["TError []"][day * 144:(day + 1) * 144] > 0, 0).abs().sum() / 6) * times_cluster[day]
            Switches_Typ_ddmpc = Switches_Typ_ddmpc + (ddmpc['hydraulic.generation.outBusGen.heaPum.numSwi []'][(day + 1) * 144 - 1] - ddmpc['hydraulic.generation.outBusGen.heaPum.numSwi []'][day * 144]) * times_cluster[day]
            Energy_Typ_hc = Energy_Typ_hc + ((hc['outputs.hydraulic.gen.PEleHP.integral'][(day + 1) * 144 - 1] - hc['outputs.hydraulic.gen.PEleHP.integral'][day * 144]) / 1000 / 3600) * times_cluster[day]
            Diskomfort_Typ_hc = Diskomfort_Typ_hc + (hc["TError"][day * 144:(day + 1) * 144].mask(hc["TError"][day * 144:(day + 1) * 144] > 0, 0).abs().sum() / 6) * times_cluster[day]
            Switches_Typ_hc = Switches_Typ_hc + (hc['hydraulic.generation.outBusGen.heaPum.numSwi'][(day + 1) * 144 - 1] - hc['hydraulic.generation.outBusGen.heaPum.numSwi'][day * 144]) * times_cluster[day]

        # Calculate r2 for typedays
        # RMSE = pd.DataFrame(index=range(len(z)), columns=list(datas.keys()))
        # RMSE_sum = pd.DataFrame(float(0), index=range(len(z)), columns=list(datas.keys()))
        # R2 = pd.DataFrame(index=range(len(z)), columns=list(datas.keys()))
        # R2_sum = pd.DataFrame(float(0), index=range(len(z)), columns=list(datas.keys()))
        # MAE = pd.DataFrame(index=range(len(z)), columns=list(datas.keys()))
        # MAE_sum = pd.DataFrame(float(0), index=range(len(z)), columns=list(datas.keys()))
        # for day in range(len(z)):
        #     for data in datas.keys():
        #         RMSE[data][day] = mean_squared_error(datas[data][day*144:day*144+144], datas[data][np.nonzero(z[:,day])[0][0]*144:np.nonzero(z[:,day])[0][0]*144+144], squared=False)
        #         RMSE_sum[data][np.nonzero(z[:, day])[0][0]] = RMSE_sum[data][np.nonzero(z[:, day])[0][0]] + mean_squared_error(datas[data][day*144:day*144+144], datas[data][np.nonzero(z[:,day])[0][0]*144:np.nonzero(z[:,day])[0][0]*144+144], squared=False)
        #         R2[data][day] = r2_score(datas[data][day * 144:day * 144 + 144], datas[data][np.nonzero(z[:, day])[0][0] * 144: np.nonzero(z[:, day])[0][0] * 144 + 144])
        #         R2_sum[data][np.nonzero(z[:, day])[0][0]] = R2_sum[data][np.nonzero(z[:, day])[0][0]] + r2_score(datas[data][day * 144:day * 144 + 144],datas[data][np.nonzero(z[:, day])[0][0] * 144:np.nonzero(z[:, day])[0][0] * 144 + 144])
        #         MAE[data][day] = mean_absolute_error(datas[data][day * 144:day * 144 + 144], datas[data][np.nonzero(z[:, day])[0][0] * 144: np.nonzero(z[:, day])[0][0] * 144 + 144])
        #         MAE_sum[data][np.nonzero(z[:, day])[0][0]] = MAE_sum[data][np.nonzero(z[:, day])[0][0]] + mean_absolute_error(datas[data][day * 144:day * 144 + 144],datas[data][np.nonzero(z[:, day])[0][0] * 144:np.nonzero(z[:, day])[0][0] * 144 + 144])

        # RMSE_sum = RMSE_sum.loc[~(RMSE_sum==0).all(axis=1)]
        # RMSE_sum = (RMSE_sum.transpose()/nc).transpose().reset_index(drop=True)
        # R2_sum = R2_sum.loc[~(R2_sum == 0).all(axis=1)]
        # R2_sum = (R2_sum.transpose() / nc).transpose().reset_index(drop=True)
        # MAE_sum = MAE_sum.loc[~(MAE_sum == 0).all(axis=1)]
        # MAE_sum = (MAE_sum.transpose() / nc).transpose().reset_index(drop=True)

        # Calculate date
        first = datetime.date(2014, 10, 1)
        results["Anzahl"][row] = i
        dates = []
        for date in days[0]:
            dates.append(str(first+datetime.timedelta(int(date))))

        # Add results to results-dataframe
        results["Datum"][row] = dates
        results["Gewichtung Typtage"][row] = list(nc)

        results["KPIs Typtage aMPC"][row] = list(np.around(np.array([Energy_Typ_ampc, Diskomfort_Typ_ampc, Switches_Typ_ampc])))
        results["KPIs TRY aMPC"][row] = list(np.around(np.array([Energy_ampc, Diskomfort_ampc, Switches_ampc]), 0))
        results["Fehler aMPC"][row] = list(np.around(np.array(list((np.array([Energy_Typ_ampc, Diskomfort_Typ_ampc, Switches_Typ_ampc])-np.array([Energy_ampc, Diskomfort_ampc, Switches_ampc]))/np.array([Energy_ampc, Diskomfort_ampc, Switches_ampc])*100)), 2))

        results["KPIs Typtage ddMPC"][row] = list(np.around(np.array([Energy_Typ_ddmpc, Diskomfort_Typ_ddmpc, Switches_Typ_ddmpc])))
        results["KPIs TRY ddMPC"][row] = list(np.around(np.array([Energy_ddmpc, Diskomfort_ddmpc, Switches_ddmpc]), 0))
        results["Fehler ddMPC"][row] = list(np.around(np.array(list((np.array([Energy_Typ_ddmpc, Diskomfort_Typ_ddmpc, Switches_Typ_ddmpc])-np.array([Energy_ddmpc, Diskomfort_ddmpc, Switches_ddmpc]))/np.array([Energy_ddmpc, Diskomfort_ddmpc, Switches_ddmpc])*100)), 2))

        results["KPIs Typtage HC"][row] = list(np.around(np.array([Energy_Typ_hc, Diskomfort_Typ_hc, Switches_Typ_hc])))
        results["KPIs TRY HC"][row] = list(np.around(np.array([Energy_hc, Diskomfort_hc, Switches_hc]), 0))
        results["Fehler HC"][row] = list(np.around(np.array(list((np.array([Energy_Typ_hc, Diskomfort_Typ_hc, Switches_Typ_hc])-np.array([Energy_hc, Diskomfort_hc, Switches_hc]))/np.array([Energy_hc, Diskomfort_hc, Switches_hc])*100)), 2))

        # RMSE_list = []
        # for data in datas.keys():
        #     RMSE_list.append({data : list(np.around(np.array(list(RMSE_sum[data].values)), 2))})
        # results["RMSE"][row] = RMSE_list
        row += 1

        # create plot if only one amount of typedays is evaluated
        # if len(typedays) == 1:
        #     fig, axs = plt.subplots(typedays[0], len(datas), figsize=(int(len(datas)*5), int(i*3)))
        #     for day in range(len(map_days)):
        #         for data in range(len(datas)):
        #             if not day == int(np.nonzero(map_days[:, day])[0][0]):
        #                 axs[int(map_days[:, day][np.nonzero(map_days[:, day])[0][0]]) - 1, data].plot(range(0, 144, 1), datas[list(datas)[data]][day * 144:(day + 1) * 144], color="lightgrey")
        #     for day in range(len(map_days)):
        #         for data in range(len(datas)):
        #             if day == int(np.nonzero(map_days[:, day])[0][0]):
        #                 axs[int(map_days[:, day][np.nonzero(map_days[:, day])[0][0]]) - 1, data].plot(range(0, 144, 1), datas[list(datas)[data]][day * 144:(day + 1) * 144],color="black")
        #
        #     axs[3, 0].set_xlabel("Zeitschritt", size=16)
        #     axs[3, 1].set_xlabel("Zeitschritt", size=16)
        #     axs[0, 0].set_xticks([])
        #     axs[0, 1].set_xticks([])
        #     axs[1, 0].set_xticks([])
        #     axs[1, 1].set_xticks([])
        #     axs[2, 0].set_xticks([])
        #     axs[2, 1].set_xticks([])
        #
        #     col = 0
        #     for data in datas.keys():
        #         axs[0, col].set_title(data, size=20)
        #         for row in range(typedays[0]):
        #             axs[row, col].text(0.5, 0.9, 'RMSE = ' + str(round(RMSE_sum[data][row], 2)), horizontalalignment='center', verticalalignment='center', transform=axs[row, col].transAxes)
        #             axs[row, col].text(0.5, 0.8, 'R2 = ' + str(round(R2_sum[data][row], 2)),horizontalalignment='center', verticalalignment='center', transform=axs[row, col].transAxes)
        #             axs[row, col].text(0.5, 0.7, 'MAE = ' + str(round(MAE_sum[data][row], 2)), horizontalalignment='center', verticalalignment='center', transform=axs[row, col].transAxes)
        #         col += 1
        #     plt.show()

    save_name_csv = save_name + ".csv"
    results.to_csv(os.path.join(r"D:\lma-dtu TEMP\masterarbeit\Tools\05 Clustering\Marius\Parameterstudie\csv-files", save_name_csv))

    # error_ampc = []
    # error_ddmpc = []
    # error_hc = []
    # for j in range(3):
    #     temp_ampc = []
    #     temp_ddmpc = []
    #     temp_hc = []
    #     for i in range(len(typedays)):
    #         temp_ampc.append(abs(results["Fehler aMPC"][i][j]))
    #         temp_ddmpc.append(abs(results["Fehler ddMPC"][i][j]))
    #         temp_hc.append(abs(results["Fehler HC"][i][j]))
    #     error_ampc.append(temp_ampc)
    #     error_ddmpc.append(temp_ddmpc)
    #     error_hc.append(temp_hc)

    # fig, axs = plt.subplots(1, 3, figsize=(20, 5))
    # axs[0].plot(typedays, error_ampc[0], label="aMPC", color="red")
    # axs[0].plot(typedays, error_ddmpc[0], label="ddMPC", color="black")
    # axs[0].plot(typedays, error_hc[0], label="HC", color="blue")
    # axs[1].plot(typedays, error_ampc[1], label="aMPC", color="red")
    # axs[1].plot(typedays, error_ddmpc[1], label="ddMPC", color="black")
    # axs[1].plot(typedays, error_hc[1], label="HC", color="blue")
    # axs[2].plot(typedays, error_ampc[2], label="aMPC", color="red")
    # axs[2].plot(typedays, error_ddmpc[2], label="ddMPC", color="black")
    # axs[2].plot(typedays, error_hc[2], label="HC", color="blue")
    #
    # axs[0].set_title("Fehler: El. Energie")
    # axs[1].set_title("Fehler: Temperaturabweichung")
    # axs[2].set_title("Fehler: Anzahl Switches")
    # axs[0].set_ylabel("Abweichung in %")
    # axs[1].set_ylabel("Abweichung in %")
    # axs[2].set_ylabel("Abweichung in %")
    # axs[0].set_xlabel("Anzahl Typtage")
    # axs[1].set_xlabel("Anzahl Typtage")
    # axs[2].set_xlabel("Anzahl Typtage")
    # axs[0].set_xticks(typedays)
    # axs[1].set_xticks(typedays)
    # axs[2].set_xticks(typedays)
    #
    # axs[0].legend()
    # axs[1].legend()
    # axs[2].legend()
    #
    # # plt.show()
    #
    # plt.savefig(os.path.join(r"D:\lma-dtu TEMP\masterarbeit\Tools\05 Clustering\Marius\Parameterstudie\png-files", save_name))
#######################################################################################################################
### Load data
folder = "PI_Controller_updated"

# Heating Period
start_date = "2014-10-01 00:00:00"
end_date = "2015-04-30 23:50:00"

# Read relevant data
ampc = pd.read_csv(os.path.join(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results", folder, "ampc.csv"), sep=",", index_col=0)[start_date:end_date]
ddmpc = pd.read_csv(os.path.join(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results", folder, "ddmpc.csv"), sep=",", index_col=0)[start_date:end_date]
hc = pd.read_csv(os.path.join(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results", folder, "HeatingCurve.csv"), sep=",", index_col=0)[start_date:end_date]
hc["LowerBound(Zone Temperature) []"] = ampc["LowerBound(Zone Temperature) []"]

########################################################################################################################
#Do weightstudy

# Timeseries to cluster
variable_list = [
    "TVL",
    "TZ",
    "n",
    "TSoll",
    "TAmb",
    "HSon"
]

combination_df = pd.DataFrame(index=range(1000), columns=["Clustergrößen", "Gewichte"])

weights_list = [1, 2]  # Gewichtswerte: 1, 2

i = 0
for num_arguments in range(1, len(variable_list) + 1):
    argument_combinations = combinations(variable_list, num_arguments)
    for args in argument_combinations:
        weight_combinations = product(weights_list, repeat=len(args))
        for weights in weight_combinations:
            combination_df["Clustergrößen"][i] = list(args)
            combination_df["Gewichte"][i] = list(weights)
            i += 1

combination_df = combination_df.dropna()

for i in range(len(combination_df)):
    datas = {}
    weights = []
    cluster_size = combination_df["Clustergrößen"][i]
    pre_weights = combination_df["Gewichte"][i]
    if "TVL" in cluster_size:
        datas["Vorlauftemperatur_ampc"] = ampc["u_T_VL"]
        datas["Vorlauftemperatur_hc"] = hc["hydraulic.control.heatingCurveDaniel.TSet"]
        datas["Vorlauftemperatur_ddmpc"] = ddmpc["u_T_VL []"]
        weights.append(pre_weights[cluster_size.index("TVL")]/3)
        weights.append(pre_weights[cluster_size.index("TVL")]/3)
        weights.append(pre_weights[cluster_size.index("TVL")]/3)
    if "TZ" in cluster_size:
        datas["Zonentemperatur_ampc"] = ampc["outputs.building.TZone[1]"]
        datas["Zonentemperatur_hc"] = hc["outputs.building.TZone[1]"]
        datas["Zonentemperatur_ddmpc"] = ddmpc["outputs.building.TZone[1] []"]
        weights.append(pre_weights[cluster_size.index("TZ")]/3)
        weights.append(pre_weights[cluster_size.index("TZ")]/3)
        weights.append(pre_weights[cluster_size.index("TZ")]/3)
    if "n" in cluster_size:
        datas["Drehzahl_ampc"] = ampc["hydraulic.control.sigBusGen.yHeaPumSet"]
        datas["Drehzahl_hc"] = hc["hydraulic.control.sigBusGen.yHeaPumSet"]
        datas["Drehzahl_ddmpc"] = ddmpc["hydraulic.control.sigBusGen.yHeaPumSet []"]
        weights.append(pre_weights[cluster_size.index("n")]/3)
        weights.append(pre_weights[cluster_size.index("n")]/3)
        weights.append(pre_weights[cluster_size.index("n")]/3)
    if "TSoll" in cluster_size:
        datas["Solltemperatur"] = hc["LowerBound(Zone Temperature) []"]
        weights.append(pre_weights[cluster_size.index("TSoll")])
    if "TAmb" in cluster_size:
        datas["Außentemperatur"] = ampc["weaDat.weaBus.TDryBul []"]
        weights.append(pre_weights[cluster_size.index("TAmb")])
    if "HSon" in cluster_size:
        datas["Sonneneinstrahlung"] = ampc["weaDat.weaBus.HDirNor []"]
        weights.append(pre_weights[cluster_size.index("HSon")])

    eval_clustering(datas, weights, ampc, ddmpc, hc, cluster_size)
