import pandas as pd
import os
import numpy as np
import ast


folders = os.listdir(r"D:\lma-dtu TEMP\masterarbeit\Tools\05 Clustering\Marius\Parameterstudie\Sorted_csv-files\Weights_0.33_0.67_1_2")

for folder in folders:

    files = os.listdir(os.path.join(r"D:\lma-dtu TEMP\masterarbeit\Tools\05 Clustering\Marius\Parameterstudie\Sorted_csv-files\Weights_0.33_0.67_1_2", folder))
    file_names = list(map(lambda x: x.replace('.csv', ''), files))

    iterables = [file_names, ["aMPC", "ddMPC", "HeatingCurve"]]
    index = pd.MultiIndex.from_product(iterables, names=["Clustergrößen", "Regelungsstrategie"])

    results = pd.DataFrame(index=index, columns=[1, 2, 3, 4, 5, 6])
    sums = pd.DataFrame(index=index, columns=[1, 2, 3, 4, 5, 6])
    for file in files:
        cur = pd.read_csv(os.path.join(r"D:\lma-dtu TEMP\masterarbeit\Tools\05 Clustering\Marius\Parameterstudie\Sorted_csv-files\Weights_0.33_0.67_1_2", folder, file), index_col=0)
        for day in np.linspace(1, 6, 6):
            results[day][file.replace('.csv', ''), 'aMPC'] = list(map(abs, ast.literal_eval(cur['Fehler aMPC'][day - 1])))
            sums[day][file.replace('.csv', ''), 'aMPC'] = sum(results[day][file.replace('.csv', ''), 'aMPC'])/3
            results[day][file.replace('.csv', ''), 'ddMPC'] = list(map(abs, ast.literal_eval(cur['Fehler ddMPC'][day - 1])))
            sums[day][file.replace('.csv', ''), 'ddMPC'] = sum(results[day][file.replace('.csv', ''), 'ddMPC'])/3
            results[day][file.replace('.csv', ''), 'HeatingCurve'] = list(map(abs, ast.literal_eval(cur['Fehler HC'][day - 1])))
            sums[day][file.replace('.csv', ''), 'HeatingCurve'] = sum(results[day][file.replace('.csv', ''), 'HeatingCurve'])/3

    total_sums = pd.DataFrame(index=file_names, columns=[1, 2, 3, 4, 5, 6])
    for file in file_names:
        for day in np.linspace(1, 6, 6):
            total_sums[day][file] = (sums[day][file, 'aMPC'] + sums[day][file, 'ddMPC'] + sums[day][file, 'HeatingCurve'])/3

    total_sums[1] = pd.to_numeric(total_sums[1])
    total_sums[2] = pd.to_numeric(total_sums[2])
    total_sums[3] = pd.to_numeric(total_sums[3])
    total_sums[4] = pd.to_numeric(total_sums[4])
    total_sums[5] = pd.to_numeric(total_sums[5])
    total_sums[6] = pd.to_numeric(total_sums[6])

    minimum_df = total_sums.min(axis=0)
    minimum_df_idx = total_sums.idxmin(axis=0)

    final_result = pd.DataFrame(index = np.linspace(1, 6, 6, dtype=int), columns=['AverageDev','Clustersizes','aMPC','ddMPC','HC'])

    final_result["AverageDev"] = minimum_df
    final_result["Clustersizes"] = minimum_df_idx

    for i in np.linspace(1, 6, 6, dtype=int):
        final_result["aMPC"][i] = results[i][final_result["Clustersizes"][i], "aMPC"]
        final_result["ddMPC"][i] = results[i][final_result["Clustersizes"][i], "ddMPC"]
        final_result["HC"][i] = results[i][final_result["Clustersizes"][i], "HeatingCurve"]

    save_name = folder + ".xlsx"
    final_result.to_excel(os.path.join(r"D:\lma-dtu TEMP\masterarbeit\Tools\05 Clustering\Marius\Parameterstudie\Auswertung\Weights_0.33_0.67_1_2\EnergyDiskomfortSwitches", save_name))











