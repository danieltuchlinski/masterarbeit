from __future__ import division
import numpy as np
import pandas as pd
import clustering_medoid as clustering
import datetime
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import os
from sklearn.metrics import r2_score, mean_squared_error, mean_absolute_error
from itertools import combinations, product

hellhellrotebc = (248/255,217/255,213/255)
hellrotebc = (241/255,179/255,171/255)
hellerrotebc = (235/255,140/255,129/255)
rotebc = (221/255,64/255,45/255)
dunkelrotebc = (172/255,43/255,28/255)
dunkeldunkelrotebc = (114/255,29/255,19/255)
dunkeldunkeldunkelrotebc = (76/255,17/255,12/255)
hellhellhellgrauebc = (242/255,242/255,242/255)
hellhellgrauebc = (235/255,236/255,236/255)
hellgrauebc = (216/255,216/255,217/255)
hellergrauebc = (196/255,197/255,198/255)
grauebc = (157/255,158/255,160/255)
dunkelgrauebc = (117/255,118/255,121/255)
dunkeldunkelgrauebc = (81/255,84/255,83/255)

def eval_clustering(datas, weights, ampc, ddmpc, hc, sizes):
    weight_string = [str(round(x,2)) for x in weights]
    save_name = "".join(sizes)+ "_" + "".join(weight_string)

    # KPIs ddmpc
    Energy_ddmpc = (ddmpc['outputs.hydraulic.gen.PEleHP.integral []'][-1] - ddmpc['outputs.hydraulic.gen.PEleHP.integral []'][0])/1000/3600
    Diskomfort_ddmpc = ddmpc["TError []"].mask(ddmpc["TError []"] > 0, 0).abs().sum()/6
    Switches_ddmpc = ddmpc['hydraulic.generation.outBusGen.heaPum.numSwi []'][-1] - ddmpc['hydraulic.generation.outBusGen.heaPum.numSwi []'][0]
    Q_ddmpc = (ddmpc['outputs.hydraulic.gen.QHP_flow.integral []'][-1] - ddmpc['outputs.hydraulic.gen.QHP_flow.integral []'][0]) / 1000 / 3600
    SCOP_ddmpc = Q_ddmpc/Energy_ddmpc
    # KPIs ampc
    Energy_ampc = (ampc['outputs.hydraulic.gen.PEleHP.integral'][-1] - ampc['outputs.hydraulic.gen.PEleHP.integral'][0])/1000/3600
    Diskomfort_ampc = ampc["TError"].mask(ampc["TError"] > 0, 0).abs().sum()/6
    Switches_ampc = ampc['hydraulic.generation.outBusGen.heaPum.numSwi'][-1] - ampc['hydraulic.generation.outBusGen.heaPum.numSwi'][0]
    Q_ampc = (ampc['outputs.hydraulic.gen.QHP_flow.integral'][-1] - ampc['outputs.hydraulic.gen.QHP_flow.integral'][0]) / 1000 / 3600
    SCOP_ampc = Q_ampc / Energy_ampc
    # KPIs Heating Curve
    Energy_hc = (hc['outputs.hydraulic.gen.PEleHP.integral'][-1] - hc['outputs.hydraulic.gen.PEleHP.integral'][0])/1000/3600
    Diskomfort_hc = hc["TError"].mask(hc["TError"] > 0, 0).abs().sum()/6
    Switches_hc = hc['hydraulic.generation.outBusGen.heaPum.numSwi'][-1] - hc['hydraulic.generation.outBusGen.heaPum.numSwi'][0]
    Q_hc = (hc['outputs.hydraulic.gen.QHP_flow.integral'][-1] - hc['outputs.hydraulic.gen.QHP_flow.integral'][0]) / 1000 / 3600
    SCOP_hc = Q_hc / Energy_hc

    # Initialize lists
    raw_inputs = {}

    # Unscaled inputs
    for data in datas.keys():
        raw_inputs[data] = datas[data]

    # Tolerance for clustering
    Mip_Gap = 0.01

    # Clustering Inputdata
    #set the range of typedays you want to analyse here!
    first = 4
    last = 4
    distance = 1
    typedays = range(first, last+1, distance)

    results = pd.DataFrame(columns=["Anzahl", "Datum", "Gewichtung Typtage", "KPIs Typtage aMPC", "KPIs TRY aMPC", "Fehler aMPC", "KPIs Typtage ddMPC", "KPIs TRY ddMPC", "Fehler ddMPC", "KPIs Typtage HC", "KPIs TRY HC", "Fehler HC", "RMSE"], index=range(len(typedays)))
    row = 0
    # clustering various inputs
    for i in typedays:

        number_clusters = i
        inputs_cluster = []
        for data in datas.keys():
            inputs_cluster.append(raw_inputs[data])

        (inputs, nc, z, inputsTransformed) = clustering.cluster(np.array(inputs_cluster), number_clusters, norm=2, mip_gap=Mip_Gap, weights=weights)

        # Set Names for Data nameing
        dataname = "Cluster_Alle_"+str(first)+'_'+str(number_clusters)+'_'+str(distance)+str('_'+str(weights)+'_')+str(Mip_Gap).replace('.', '')

        # Determine time steps per day
        len_day = int(np.array(inputs_cluster).shape[1] / 212)

        print("Clustering with "+str(i)+" clusters successful!")

        clustered = {}
        numb = 0
        for data in datas.keys():
            clustered[data+"_"+str(i)] = inputs[numb]
            numb += 1

        clustered["weights"+str(i)] = nc
        clustered["mapping_"+str(i)] = z

        # only 0 and 1
        for m in range(len(z)):
            for n in range(len(z)):
                if z[n, m] < 1:
                    z[n, m] = 0

        # build matrix map_days indicating which day of the year is assigned to which typeday
        map_days = np.zeros([len(z), len(z)])
        times_cluster = np.sum(z, axis=1)
        j = 1
        for t in range(len(times_cluster)):
            if times_cluster[t] > 0:
                map_days[t, :] = z[t, :]*j
                j = j+1

        clustered_timeseries = {}
        for data in datas.keys():
            clustered_timeseries[data] = []

        for m in range(len(z)):
            for n in range(len(z)):
                if map_days[n, m] > 0:
                    for data in datas.keys():
                        clustered_timeseries[data] = np.append(clustered_timeseries[data], clustered[data+"_"+str(i)][int(map_days[n, m] - 1)])

        # Calculate KPIs for typedays
        Energy_Typ_ampc = 0
        Diskomfort_Typ_ampc = 0
        Switches_Typ_ampc = 0
        SCOP_Typ_ampc = 0
        Q_Typ_ampc = 0
        Energy_Typ_ddmpc = 0
        Diskomfort_Typ_ddmpc = 0
        Switches_Typ_ddmpc = 0
        SCOP_Typ_ddmpc = 0
        Q_Typ_ddmpc = 0
        Energy_Typ_hc = 0
        Diskomfort_Typ_hc = 0
        Switches_Typ_hc = 0
        SCOP_Typ_hc = 0
        Q_Typ_hc = 0
        days = list(np.nonzero(times_cluster))
        for day in days[0]:
            Energy_Typ_ampc = Energy_Typ_ampc + ((ampc['outputs.hydraulic.gen.PEleHP.integral'][(day+1)*144-1]-ampc['outputs.hydraulic.gen.PEleHP.integral'][day*144])/1000/3600) * times_cluster[day]
            Diskomfort_Typ_ampc = Diskomfort_Typ_ampc + (ampc["TError"][day*144:(day+1)*144].mask(ampc["TError"][day*144:(day+1)*144] > 0, 0).abs().sum()/6)*times_cluster[day]
            Switches_Typ_ampc = Switches_Typ_ampc + (ampc['hydraulic.generation.outBusGen.heaPum.numSwi'][(day+1)*144-1]-ampc['hydraulic.generation.outBusGen.heaPum.numSwi'][day*144])*times_cluster[day]
            Q_Typ_ampc = Q_Typ_ampc + ((ampc['outputs.hydraulic.gen.QHP_flow.integral'][(day+1)*144-1]-ampc['outputs.hydraulic.gen.QHP_flow.integral'][day*144])/1000/3600) * times_cluster[day]
            Energy_Typ_ddmpc = Energy_Typ_ddmpc + ((ddmpc['outputs.hydraulic.gen.PEleHP.integral []'][(day + 1) * 144 - 1] - ddmpc['outputs.hydraulic.gen.PEleHP.integral []'][day * 144]) / 1000 / 3600) * times_cluster[day]
            Diskomfort_Typ_ddmpc = Diskomfort_Typ_ddmpc + (ddmpc["TError []"][day * 144:(day + 1) * 144].mask(ddmpc["TError []"][day * 144:(day + 1) * 144] > 0, 0).abs().sum() / 6) * times_cluster[day]
            Switches_Typ_ddmpc = Switches_Typ_ddmpc + (ddmpc['hydraulic.generation.outBusGen.heaPum.numSwi []'][(day + 1) * 144 - 1] - ddmpc['hydraulic.generation.outBusGen.heaPum.numSwi []'][day * 144]) * times_cluster[day]
            Q_Typ_ddmpc = Q_Typ_ddmpc + ((ddmpc['outputs.hydraulic.gen.QHP_flow.integral []'][(day + 1) * 144 - 1] -ddmpc['outputs.hydraulic.gen.QHP_flow.integral []'][day * 144]) / 1000 / 3600) * times_cluster[day]
            Energy_Typ_hc = Energy_Typ_hc + ((hc['outputs.hydraulic.gen.PEleHP.integral'][(day + 1) * 144 - 1] - hc['outputs.hydraulic.gen.PEleHP.integral'][day * 144]) / 1000 / 3600) * times_cluster[day]
            Diskomfort_Typ_hc = Diskomfort_Typ_hc + (hc["TError"][day * 144:(day + 1) * 144].mask(hc["TError"][day * 144:(day + 1) * 144] > 0, 0).abs().sum() / 6) * times_cluster[day]
            Switches_Typ_hc = Switches_Typ_hc + (hc['hydraulic.generation.outBusGen.heaPum.numSwi'][(day + 1) * 144 - 1] - hc['hydraulic.generation.outBusGen.heaPum.numSwi'][day * 144]) * times_cluster[day]
            Q_Typ_hc = Q_Typ_hc + ((hc['outputs.hydraulic.gen.QHP_flow.integral'][(day + 1) * 144 - 1] -hc['outputs.hydraulic.gen.QHP_flow.integral'][day * 144]) / 1000 / 3600) * times_cluster[day]

        SCOP_Typ_ampc = Q_Typ_ampc/Energy_Typ_ampc
        SCOP_Typ_ddmpc = Q_Typ_ddmpc/Energy_Typ_ddmpc
        SCOP_Typ_hc=Q_Typ_hc/Energy_Typ_hc

        # Calculate r2 for typedays
        RMSE = pd.DataFrame(index=range(len(z)), columns=list(datas.keys()))
        RMSE_sum = pd.DataFrame(float(0), index=range(len(z)), columns=list(datas.keys()))
        R2 = pd.DataFrame(index=range(len(z)), columns=list(datas.keys()))
        R2_sum = pd.DataFrame(float(0), index=range(len(z)), columns=list(datas.keys()))
        MAE = pd.DataFrame(index=range(len(z)), columns=list(datas.keys()))
        MAE_sum = pd.DataFrame(float(0), index=range(len(z)), columns=list(datas.keys()))
        for day in range(len(z)):
            for data in datas.keys():
                RMSE[data][day] = mean_squared_error(datas[data][day*144:day*144+144], datas[data][np.nonzero(z[:,day])[0][0]*144:np.nonzero(z[:,day])[0][0]*144+144], squared=False)
                RMSE_sum[data][np.nonzero(z[:, day])[0][0]] = RMSE_sum[data][np.nonzero(z[:, day])[0][0]] + mean_squared_error(datas[data][day*144:day*144+144], datas[data][np.nonzero(z[:,day])[0][0]*144:np.nonzero(z[:,day])[0][0]*144+144], squared=False)
                R2[data][day] = r2_score(datas[data][day * 144:day * 144 + 144], datas[data][np.nonzero(z[:, day])[0][0] * 144: np.nonzero(z[:, day])[0][0] * 144 + 144])
                R2_sum[data][np.nonzero(z[:, day])[0][0]] = R2_sum[data][np.nonzero(z[:, day])[0][0]] + r2_score(datas[data][day * 144:day * 144 + 144],datas[data][np.nonzero(z[:, day])[0][0] * 144:np.nonzero(z[:, day])[0][0] * 144 + 144])
                MAE[data][day] = mean_absolute_error(datas[data][day * 144:day * 144 + 144], datas[data][np.nonzero(z[:, day])[0][0] * 144: np.nonzero(z[:, day])[0][0] * 144 + 144])
                MAE_sum[data][np.nonzero(z[:, day])[0][0]] = MAE_sum[data][np.nonzero(z[:, day])[0][0]] + mean_absolute_error(datas[data][day * 144:day * 144 + 144],datas[data][np.nonzero(z[:, day])[0][0] * 144:np.nonzero(z[:, day])[0][0] * 144 + 144])

        RMSE_sum = RMSE_sum.loc[~(RMSE_sum==0).all(axis=1)]
        RMSE_sum = (RMSE_sum.transpose()/nc).transpose().reset_index(drop=True)
        R2_sum = R2_sum.loc[~(R2_sum == 0).all(axis=1)]
        R2_sum = (R2_sum.transpose() / nc).transpose().reset_index(drop=True)
        MAE_sum = MAE_sum.loc[~(MAE_sum == 0).all(axis=1)]
        MAE_sum = (MAE_sum.transpose() / nc).transpose().reset_index(drop=True)

        # Calculate date
        first = datetime.date(2014, 10, 1)
        results["Anzahl"][row] = i
        dates = []
        for date in days[0]:
            dates.append(str(first+datetime.timedelta(int(date))))

        # Add results to results-dataframe
        results["Datum"][row] = dates
        results["Gewichtung Typtage"][row] = list(nc)

        results["KPIs Typtage aMPC"][row] = list(np.around(np.array([Energy_Typ_ampc, Diskomfort_Typ_ampc, Switches_Typ_ampc,SCOP_Typ_ampc])))
        results["KPIs TRY aMPC"][row] = list(np.around(np.array([Energy_ampc, Diskomfort_ampc, Switches_ampc,SCOP_ampc]), 0))
        results["Fehler aMPC"][row] = list(np.around(np.array(list((np.array([Energy_Typ_ampc, Diskomfort_Typ_ampc, Switches_Typ_ampc,SCOP_Typ_ampc])-np.array([Energy_ampc, Diskomfort_ampc, Switches_ampc, SCOP_ampc]))/np.array([Energy_ampc, Diskomfort_ampc, Switches_ampc, SCOP_ampc])*100)), 2))

        results["KPIs Typtage ddMPC"][row] = list(np.around(np.array([Energy_Typ_ddmpc, Diskomfort_Typ_ddmpc, Switches_Typ_ddmpc,SCOP_Typ_ddmpc])))
        results["KPIs TRY ddMPC"][row] = list(np.around(np.array([Energy_ddmpc, Diskomfort_ddmpc, Switches_ddmpc,SCOP_ddmpc]), 0))
        results["Fehler ddMPC"][row] = list(np.around(np.array(list((np.array([Energy_Typ_ddmpc, Diskomfort_Typ_ddmpc, Switches_Typ_ddmpc,SCOP_Typ_ddmpc])-np.array([Energy_ddmpc, Diskomfort_ddmpc, Switches_ddmpc,SCOP_ddmpc]))/np.array([Energy_ddmpc, Diskomfort_ddmpc, Switches_ddmpc,SCOP_ddmpc])*100)), 2))

        results["KPIs Typtage HC"][row] = list(np.around(np.array([Energy_Typ_hc, Diskomfort_Typ_hc, Switches_Typ_hc,SCOP_Typ_hc])))
        results["KPIs TRY HC"][row] = list(np.around(np.array([Energy_hc, Diskomfort_hc, Switches_hc,SCOP_hc]), 0))
        results["Fehler HC"][row] = list(np.around(np.array(list((np.array([Energy_Typ_hc, Diskomfort_Typ_hc, Switches_Typ_hc,SCOP_Typ_hc])-np.array([Energy_hc, Diskomfort_hc, Switches_hc,SCOP_hc]))/np.array([Energy_hc, Diskomfort_hc, Switches_hc, SCOP_hc])*100)), 2))

        RMSE_list = []
        for data in datas.keys():
            RMSE_list.append({data : list(np.around(np.array(list(RMSE_sum[data].values)), 2))})
        results["RMSE"][row] = RMSE_list
        row += 1

        # create plot if only one amount of typedays is evaluated
        if len(typedays) == 1:
            fig, axs = plt.subplots(typedays[0], len(datas)-2, figsize=(15.5/2.54, 7), gridspec_kw = {'wspace':0.1, 'hspace':0.1})
            if len(datas) == 1:
                for day in range(len(map_days)):
                    for data in range(len(datas)):
                        if not day == int(np.nonzero(map_days[:, day])[0][0]):
                            axs[int(map_days[:, day][np.nonzero(map_days[:, day])[0][0]]) - 1].plot(range(0, 144, 1), datas[list(datas)[data]][day * 144:(day + 1) * 144]-273.15, color=grauebc)
                for day in range(len(map_days)):
                    for data in range(len(datas)):
                        if day == int(np.nonzero(map_days[:, day])[0][0]):
                            axs[int(map_days[:, day][np.nonzero(map_days[:, day])[0][0]]) - 1].plot(range(0, 144, 1), datas[list(datas)[data]][day * 144:(day + 1) * 144]-273.15,color="black")
            else:
                for day in range(len(map_days)):
                    for data in range(len(datas)-2):
                        if not day == int(np.nonzero(map_days[:, day])[0][0]):
                            axs[int(map_days[:, day][np.nonzero(map_days[:, day])[0][0]]) - 1, data].plot(range(0, 144, 1), datas[list(datas)[data]][day * 144:(day + 1) * 144]-273.15, color=grauebc, linewidth=0.75)
                for day in range(len(map_days)):
                    for data in range(len(datas)-2):
                        if day == int(np.nonzero(map_days[:, day])[0][0]):
                            axs[int(map_days[:, day][np.nonzero(map_days[:, day])[0][0]]) - 1, data].plot(range(0, 144, 1), datas[list(datas)[data]][day * 144:(day + 1) * 144]-273.15,color="black", linewidth=1.25)

            col = 0
            titles = ['aMPC', 'Heizkurve', 'MPC']
            daynames = ['18. Nov.', '28. Dez', '4. Apr.', '19. Apr.']
            if len(datas) == 1:
                for data in datas.keys():
                    axs[0].set_title(titles[col])
                    for row in range(typedays[0]):
                        axs[row].text(0.5, 0.9, 'RMSE = ' + str(round(RMSE_sum[data][row], 2)) + ' K', horizontalalignment='center', verticalalignment='center', transform=axs[row].transAxes)
                        # axs[row].text(0.5, 0.8, 'R2 = ' + str(round(R2_sum[data][row], 2)),horizontalalignment='center', verticalalignment='center', transform=axs[row].transAxes)
                        axs[row].text(0.5, 0.8, 'MAE = ' + str(round(MAE_sum[data][row], 2)) + ' K', horizontalalignment='center', verticalalignment='center', transform=axs[row].transAxes)
            else:
                for data in datas.keys():
                    if col < 3:
                        axs[0, col].set_title(titles[col])
                        for row in range(typedays[0]):
                            axs[row, col].text(0.5, 0.9, 'RMSE = ' + str(round(RMSE_sum[data][row], 2)) + ' K', horizontalalignment='center', verticalalignment='center', transform=axs[row, col].transAxes)
                            # axs[row, col].text(0.5, 0.8, 'R2 = ' + str(round(R2_sum[data][row], 2)),horizontalalignment='center', verticalalignment='center', transform=axs[row, col].transAxes)
                            axs[row, col].text(0.5, 0.8, 'MAE = ' + str(round(MAE_sum[data][row], 2)) + ' K', horizontalalignment='center', verticalalignment='center', transform=axs[row, col].transAxes)
                            if col==2:
                                axs[row, col].text(1.1, 0.5, daynames[row],horizontalalignment='left', verticalalignment='center',transform=axs[row, col].transAxes)
                        col += 1


            for col in range(3):
                for row in range(4):
                    if row<=2:
                        axs[row, col].set_xticks(ticks=[0, 48, 96, 143], labels=['0', '8', '16', '0'])
                        axs[row, col].grid(True, linewidth=0.25)
                        axs[row, col].set_xticklabels([])
                        axs[row, col].tick_params(axis='x', which='both', length=0)
                    axs[row, col].set_xlim([0,143])
                    axs[row, col].set_ylim([15, 45])
                    if col>0:
                        axs[row, col].set_yticklabels([])
                        axs[row, col].tick_params(axis='y', which='both', length=0)
                    if row ==3:
                        axs[row,col].set_xlabel('Uhrzeit')
                        axs[row, col].set_xticks(ticks=[0, 48, 96, 143], labels=['0', '8', '16', '0'])
                    if col==0:
                        axs[row, col].set_ylabel('Sollvorlauftemp. in °C')
                        axs[row, col].set_yticks([15, 25, 35, 45])
                    axs[row, col].grid(True, linewidth=0.25)
            plt.subplots_adjust(wspace=0, hspace=0)
            plt.tight_layout()
            plt.savefig('ClusteredVL.svg')
            plt.show()

        if len(typedays) == 1:
            fig, axs = plt.subplots(typedays[0], len(datas)-3, figsize=(13.5/2.54, 7))
            if len(datas) == 1:
                for day in range(len(map_days)):
                    for data in range(len(datas)-3):
                        if not day == int(np.nonzero(map_days[:, day])[0][0]):
                            axs[int(map_days[:, day][np.nonzero(map_days[:, day])[0][0]]) - 1].plot(range(0, 144, 1), datas[list(datas)[data+3]][day * 144:(day + 1) * 144]-273.15, color=grauebc)
                for day in range(len(map_days)):
                    for data in range(len(datas)-3):
                        if day == int(np.nonzero(map_days[:, day])[0][0]):
                            axs[int(map_days[:, day][np.nonzero(map_days[:, day])[0][0]]) - 1].plot(range(0, 144, 1), datas[list(datas)[data+3]][day * 144:(day + 1) * 144]-273.15,color="black")
            else:
                for day in range(len(map_days)):
                    for data in range(len(datas)-3):
                        if not day == int(np.nonzero(map_days[:, day])[0][0]):
                            axs[int(map_days[:, day][np.nonzero(map_days[:, day])[0][0]]) - 1, data].plot(range(0, 144, 1), datas[list(datas)[data+3]][day * 144:(day + 1) * 144]-273.15, color=grauebc)
                for day in range(len(map_days)):
                    for data in range(len(datas)-3):
                        if day == int(np.nonzero(map_days[:, day])[0][0]):
                            axs[int(map_days[:, day][np.nonzero(map_days[:, day])[0][0]]) - 1, data].plot(range(0, 144, 1), datas[list(datas)[data+3]][day * 144:(day + 1) * 144]-273.15,color="black")

            col = 0
            titles = ['aMPC', 'Heizkurve', 'MPC']
            if len(datas) == 1:
                for data in datas.keys():
                    axs[0].set_title(titles[col])
                    for row in range(typedays[0]):
                        axs[row].text(0.5, 0.9, 'RMSE = ' + str(round(RMSE_sum[data][row], 2)) + ' K', horizontalalignment='center', verticalalignment='center', transform=axs[row].transAxes)
                        # # axs[row].text(0.5, 0.8, 'R2 = ' + str(round(R2_sum[data][row], 2)),horizontalalignment='center', verticalalignment='center', transform=axs[row].transAxes)
                        axs[row].text(0.5, 0.8, 'MAE = ' + str(round(MAE_sum[data][row], 2)) + ' K', horizontalalignment='center', verticalalignment='center', transform=axs[row].transAxes)
            else:
                for data in datas.keys():
                    if col > 2:
                        for row in range(typedays[0]):
                            axs[row, col-3].text(0.5, 0.9, 'RMSE = ' + str(round(RMSE_sum[data][row], 2)) + ' K', horizontalalignment='center', verticalalignment='center', transform=axs[row, col-3].transAxes)
                            # axs[row, col].text(0.5, 0.8, 'R2 = ' + str(round(R2_sum[data][row], 2)),horizontalalignment='center', verticalalignment='center', transform=axs[row, col].transAxes)
                            axs[row, col-3].text(0.5, 0.8, 'MAE = ' + str(round(MAE_sum[data][row], 2)) + ' K', horizontalalignment='center', verticalalignment='center', transform=axs[row, col-3].transAxes)
                    col += 1
                for row in range(4):
                    axs[row, 1].text(1.1, 0.5, daynames[row], horizontalalignment='left',verticalalignment='center', transform=axs[row, 1].transAxes)



            for col in range(2):
                for row in range(4):
                    axs[row, col].set_xlim([0,143])
                    if row<=2:
                        axs[row, col].set_xticks(ticks=[0, 48, 96, 143], labels=['0', '8', '16', '0'])
                        axs[row, col].grid(True, linewidth=0.25)
                        axs[row, col].set_xticklabels([])
                        axs[row, col].tick_params(axis='x', which='both', length=0)
                    if col==0:
                        axs[row, col].set_ylim([16.5, 21])
                        axs[row, col].set_yticks([17, 18, 19, 20])
                        axs[row, col].set_ylabel('Zonensolltemp. in °C')
                    else:
                        axs[row, col].set_ylim([-12, 30])
                        axs[row, col].set_yticks([-10, 0, 10, 20,30])
                        axs[row, col].set_ylabel('Umgebungstemp. in °C')
                    if row==3:
                        axs[row, col].set_xticks(ticks=[0,48,96,143], labels=['0', '8', '16', '0'])
                        axs[row,col].set_xlabel('Uhrzeit')
                        axs[row, col].grid(True, linewidth=0.25)
            # axs[0,0].set_title('Zonensolltemp. in °C')
            # axs[0,1].set_title('Umgebungstemp. in °C')
            plt.tight_layout()
            plt.savefig('ClusteredSetAmb.svg')
            plt.show()

####################################################################################################################################################################################




    save_name_xlsx = save_name + ".xlsx"
    results.to_excel(os.path.join(r"D:\lma-dtu TEMP\masterarbeit\Tools\05 Clustering\Marius\Parameterstudie\Testing", save_name_xlsx))

    error_ampc = []
    error_ddmpc = []
    error_hc = []
    for j in range(3):
        temp_ampc = []
        temp_ddmpc = []
        temp_hc = []
        for i in range(len(typedays)):
            temp_ampc.append(abs(results["Fehler aMPC"][i][j]))
            temp_ddmpc.append(abs(results["Fehler ddMPC"][i][j]))
            temp_hc.append(abs(results["Fehler HC"][i][j]))
        error_ampc.append(temp_ampc)
        error_ddmpc.append(temp_ddmpc)
        error_hc.append(temp_hc)

    # fig, axs = plt.subplots(1, 3, figsize=(20, 5))
    # axs[0].plot(typedays, error_ampc[0], label="aMPC", color="red")
    # axs[0].plot(typedays, error_ddmpc[0], label="ddMPC", color="black")
    # axs[0].plot(typedays, error_hc[0], label="HC", color="blue")
    # axs[1].plot(typedays, error_ampc[1], label="aMPC", color="red")
    # axs[1].plot(typedays, error_ddmpc[1], label="ddMPC", color="black")
    # axs[1].plot(typedays, error_hc[1], label="HC", color="blue")
    # axs[2].plot(typedays, error_ampc[2], label="aMPC", color="red")
    # axs[2].plot(typedays, error_ddmpc[2], label="ddMPC", color="black")
    # axs[2].plot(typedays, error_hc[2], label="HC", color="blue")
    #
    # axs[0].set_title("Fehler: El. Energie")
    # axs[1].set_title("Fehler: Temperaturabweichung")
    # axs[2].set_title("Fehler: Anzahl Switches")
    # axs[0].set_ylabel("Abweichung in %")
    # axs[1].set_ylabel("Abweichung in %")
    # axs[2].set_ylabel("Abweichung in %")
    # axs[0].set_xlabel("Anzahl Typtage")
    # axs[1].set_xlabel("Anzahl Typtage")
    # axs[2].set_xlabel("Anzahl Typtage")
    # axs[0].set_xticks(typedays)
    # axs[1].set_xticks(typedays)
    # axs[2].set_xticks(typedays)
    #
    # axs[0].legend()
    # axs[1].legend()
    # axs[2].legend()
    #
    # plt.show()

    # plt.savefig(os.path.join(r"D:\lma-dtu TEMP\masterarbeit\Tools\05 Clustering\Marius\Parameterstudie\png-files", save_name))

### Load data
folder = "PI_Controller_updated"

# Heating Period
start_date = "2014-10-01 00:00:00"
end_date = "2015-04-30 23:50:00"

# Read relevant data
ampc = pd.read_csv(os.path.join(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results", folder, "ampc.csv"), sep=",", index_col=0)[start_date:end_date]
ddmpc = pd.read_csv(os.path.join(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results", folder, "ddmpc.csv"), sep=",", index_col=0)[start_date:end_date]
hc = pd.read_csv(os.path.join(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results", folder, "HeatingCurve.csv"), sep=",", index_col=0)[start_date:end_date]
hc["LowerBound(Zone Temperature) []"] = ampc["LowerBound(Zone Temperature) []"]
########################################################################################################################
## Single data
datas = {
    "Vorlauftemperatur_ampc": ampc["u_T_VL"],
    "Vorlauftemperatur_hc": hc["hydraulic.control.heatingCurveDaniel.TSet"],
    "Vorlauftemperatur_ddmpc": ddmpc["u_T_VL []"],
    # "Zonentemperatur_ampc": ampc["outputs.building.TZone[1]"],
    # "Zonentemperatur_hc": hc["outputs.building.TZone[1]"],
    # "Zonentemperatur_ddmpc": ddmpc["outputs.building.TZone[1] []"],
    # "Drehzahl_ampc": ampc["hydraulic.control.sigBusGen.yHeaPumSet"],
    # "Drehzahl_hc": hc["hydraulic.control.sigBusGen.yHeaPumSet"],
    # "Drehzahl_ddmpc": ddmpc["hydraulic.control.sigBusGen.yHeaPumSet []"],
    "Solltemperatur": hc["LowerBound(Zone Temperature) []"],
    "Außentemperatur": ampc["weaDat.weaBus.TDryBul []"],
    # "Sonneneinstrahlung": ampc["weaDat.weaBus.HDirNor []"]
    # "diff_VL": ampc["u_T_VL"]-ddmpc["u_T_VL []"],
    # "diff_TZone": ampc["outputs.building.TZone[1]"]-ddmpc["outputs.building.TZone[1] []"]
}

weights = [1/3,1/3,1/3,1,2]

# cluster_size = ["TVL", "TZ", "n", "TSoll", "TAmb", "HSon"]
cluster_size = ["TVL", "Tsoll", "TAuß"]

eval_clustering(datas, weights, ampc, ddmpc, hc, cluster_size)
