from pathlib import Path
from agentlib.utils.multi_agent_system import LocalMASAgency


def run_ampc(
        pubtopic: str,
        output_alias: str,
        t_sample: float = 10 * 60,
        run_duration: float = 86400 * 1.2
):
    logging.warning("AMPC output will be published to %s/%s",
                    pubtopic, output_alias)
    # Path definition
    base_path = Path(r'D:\lma-dtu TEMP\masterarbeit\Tools\03 aMPC')  # path at which ampc Framework is located
    results_folder = base_path.joinpath("data", "AddMo_Trainings_results", "PI_Controller_updated")
    results_folder_sub_test = results_folder.joinpath("Predictions", "ANN")

    MODEL_CONFIG = dict(
        type={
            "file": "addmo_ampc.py",
            "class_name": "AddmoAMPCModel"
        },
        results_folder=results_folder,
        results_folder_sub_test=results_folder_sub_test,
        forecasts_pickle=base_path.joinpath("data", "forecasts_daniel.pickle"),
        disturbances_pickle=base_path.joinpath("data", "disturbances_daniel.pickle"),
    )
    SIMULATOR_CONFIG = {
        "type": "simulator",
        "model": MODEL_CONFIG,
        "t_sample": t_sample,
        "save_results": False,
        "outputs": [{"name": "T_VL_ampc", "alias": output_alias}],
        "inputs": [
            {"name": "SimTime", "alias": "Sim_Time"},
            {"name": "TZone", "alias": "outputs.building.TZone[1]"},
            {"name": "TAmb", "alias": "weaDat.weaBus.TDryBul"},
            {"name": "HDirNor", "alias": "weaDat.weaBus.HDirNor"},
            {"name": "TFußboden", "alias": "TFussboden"},
            {"name": "TError", "alias": "TError"},
            {"name": "TZoneSet", "alias": "TZoneSet"}
        ]
    }
    MQTT_CONFIG = {
        "type": {
            "file": "hil_mqtt.py",
            "class_name": "HiLMqttClient"
        },
        "url": "mqtt://hil.ebc-team-kap.osc.eonerc.rwth-aachen.de",
        "subscriptions": [
            "hil2/Sim/Sim_Time",
            "hil2/Sim/outputs.building.TZone[1]",
            "hil2/Sim/weaDat.weaBus.TDryBul",
            "hil2/Sim/weaDat.weaBus.HDirNor",
            "hil2/Sim/TFussboden",
            "hil2/Sim/TError",
            "hil2/Sim/TZoneSet",
        ],
        "pubtopic": pubtopic,
        "log_level": "INFO"
    }
    AGENT_CONFIG = {
        "id": "AMPC_Agent",
        "modules": [
            MQTT_CONFIG,
            SIMULATOR_CONFIG
        ]
    }

    env_config = {"rt": True, "factor": 1, "t_sample": t_sample}
    # create multiple agents with different configuration
    mas = LocalMASAgency(
        env=env_config,
        agent_configs=[AGENT_CONFIG],
        variable_logging=False,
    )
    # Simulate
    mas.run(until=run_duration)


if __name__ == '__main__':
    import logging
    logging.basicConfig(level="DEBUG")
    # @Daniel: Wenn du z.B. auf das topic "hil2/Sim/ShadowAMPC_T_VL" publishen willst,
    # muss:
    # - pubtopic="hil2/Sim"
    # - output_alias="ShadowAMPC"
    run_ampc(
        t_sample=10,
        run_duration=86400 * 1.2, #None
        pubtopic="hil2/Sim",
        output_alias="MPC.OptimalControl"
    )
