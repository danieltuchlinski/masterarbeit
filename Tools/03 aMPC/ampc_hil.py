import pandas as pd
from ampc.ampc_basics import main_OnlyPredict
import pickle
import datetime

base_path = r'D:\lma-dtu TEMP\masterarbeit\Tools\03 aMPC' # path at which ampc Framework is located

# Load files with disturbances and forecasts
disturbances = pickle.load(open(base_path + r'\data\disturbances_daniel.pickle', 'rb'))
forecasts = pickle.load(open(base_path + r'\data\forecasts_daniel.pickle', 'rb'))

# Get data from MQTT
SimTime = 0 # hil2/Sim/Sim_Time
TZone = 293.15 # hil2/Sim/outputs.building.TZone[1]
TAmb = 293.15 # "hil2/Sim/weaDat.weaBus.TDryBul
HDirNor = 0 # hil2/Sim/weaDat.weaBus.HDirNor
TFußboden = 293.15 # hil2/Sim/TFussboden
TError = 0 # hil2/Sim/TError
TZoneSet = 293.15 # hil2/Sim/TZoneSet

# Seconds since 01/01/1970
AbsTime = 1410300000 + SimTime
# Transform AbsTime into dateformat
date = datetime.datetime.fromtimestamp(AbsTime).strftime('%Y-%m-%d %H:%M:%S')
# Load relevant data from disturbances and forecasts file
daily_sin = disturbances['TimeFunc(daily_sin) []'][date]
daily_cos = disturbances['TimeFunc(daily_cos) []'][date]
weekly_sin = disturbances['TimeFunc(weekly_sin) []'][date]
weekly_cos = disturbances['TimeFunc(weekly_cos) []'][date]
TAmb_fc6 = forecasts['weaDat.weaBus.TDryBul_fc6 []'][date]
TAmb_fc12 = forecasts['weaDat.weaBus.TDryBul_fc12 []'][date]
TAmb_fc18 = forecasts['weaDat.weaBus.TDryBul_fc18 []'][date]
TAmb_fc24 = forecasts['weaDat.weaBus.TDryBul_fc24 []'][date]
TAmb_fc30 = forecasts['weaDat.weaBus.TDryBul_fc30 []'][date]
TAmb_fc36 = forecasts['weaDat.weaBus.TDryBul_fc36 []'][date]
HDirNor_fc6 = forecasts['weaDat.weaBus.HDirNor_fc6 []'][date]
HDirNor_fc12 = forecasts['weaDat.weaBus.HDirNor_fc12 []'][date]
HDirNor_fc18 = forecasts['weaDat.weaBus.HDirNor_fc18 []'][date]
HDirNor_fc24 = forecasts['weaDat.weaBus.HDirNor_fc24 []'][date]
HDirNor_fc30 = forecasts['weaDat.weaBus.HDirNor_fc30 []'][date]
HDirNor_fc36 = forecasts['weaDat.weaBus.HDirNor_fc36 []'][date]
TZoneSet_fc6 = forecasts['LowerBound(Zone Temperature)_fc6 []'][date]
TZoneSet_fc12 = forecasts['LowerBound(Zone Temperature)_fc12 []'][date]
TZoneSet_fc18 = forecasts['LowerBound(Zone Temperature)_fc18 []'][date]
TZoneSet_fc24 = forecasts['LowerBound(Zone Temperature)_fc24 []'][date]
TZoneSet_fc30 = forecasts['LowerBound(Zone Temperature)_fc30 []'][date]
TZoneSet_fc36 = forecasts['LowerBound(Zone Temperature)_fc36 []'][date]

X_unscaled = [TZone, TAmb, HDirNor, TFußboden, TError, TZoneSet, daily_sin, daily_cos, weekly_sin, weekly_cos, TAmb_fc6,
              TAmb_fc12, TAmb_fc18, TAmb_fc24, TAmb_fc30, TAmb_fc36, HDirNor_fc6, HDirNor_fc12, HDirNor_fc18,
              HDirNor_fc24,HDirNor_fc30, HDirNor_fc36, TZoneSet_fc6, TZoneSet_fc12, TZoneSet_fc18, TZoneSet_fc24,
              TZoneSet_fc30, TZoneSet_fc36]

df_X_unscaled = pd.DataFrame(data=X_unscaled)

# Path definition
ResultsFolder = base_path + r'\data\AddMo_Trainings_results\PI_Controller_updated'
ResultsFolderSubTest = ResultsFolder + r'\Predictions\ANN'

# Output to send to MQTT
y_Predicted = main_OnlyPredict(ResultsFolder, ResultsFolderSubTest, df_X_unscaled, OnlyPredictRecursive=False,
                               signal="TVL")

