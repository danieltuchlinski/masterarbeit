
import pandas as pd
path = r'D:\lma-dtu TEMP\masterarbeit\Tools\03 aMPC\data\AddMo_Trainings_results\Standard\Pickles\ThePickle_from_ImportData.pickle'

data = pd.read_pickle(path)

forecasts = data.filter(regex='_fc')
disturbances = data.iloc[:, [2, 3, 7, 8, 12, 14, 16, 18, 19, 20, 21]]

forecasts.to_pickle(r'D:\lma-dtu TEMP\masterarbeit\Tools\03 aMPC\data\forecasts_daniel.pickle')
disturbances.to_pickle(r'D:\lma-dtu TEMP\masterarbeit\Tools\03 aMPC\data\disturbances_daniel.pickle')

print('Debug help!')
