from agentlib import (
    Model, ModelConfig,
    ModelInput, ModelOutput,
    ModelInputs, ModelOutputs
)
from pathlib import Path

import pandas as pd
import pickle
import datetime

from ampc.ampc_basics import main_OnlyPredict


class AMPCModelConfig(ModelConfig):
    results_folder: Path
    results_folder_sub_test: Path
    disturbances_pickle: Path
    forecasts_pickle: Path
    inputs: ModelInputs = [
        ModelInput(name="SimTime", value=0),
        ModelInput(name="TZone", value=293.15),
        ModelInput(name="TAmb", value=293.15),
        ModelInput(name="HDirNor", value=0),
        ModelInput(name="TFußboden", value=293.15),
        ModelInput(name="TError", value=0),
        ModelInput(name="TZoneSet", value=293.15)
    ]
    outputs: ModelOutputs = [
        ModelOutput(name="T_VL_ampc", value=308.15)
    ]


class AddmoAMPCModel(Model):
    config_type = AMPCModelConfig

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        # Load files with disturbances and forecasts
        # @Daniel: Ohne das with wird das file nach dem open nicht
        # wieder geschlossen, das kann zu ekeligen Fehlern führen
        with open(self.config.disturbances_pickle, 'rb') as file:
            self.disturbances = pickle.load(file)
        with open(self.config.forecasts_pickle, "rb") as file:
            self.forecasts = pickle.load(file)

    def initialize(self, **kwargs):
        pass  # Not init needed

    def do_step(self, *, t_start, t_sample=None):
        # Seconds since 01/01/1970
        SimTime = self.get("SimTime").value
        print(SimTime)
        if SimTime%600==0:
            TZone = self.get("TZone").value + 273.15
            TAmb = self.get("TAmb").value + 273.15
            HDirNor = self.get("HDirNor").value
            TFußboden = self.get("TFußboden").value + 273.15
            TError = self.get("TError").value
            TZoneSet = self.get("TZoneSet").value + 273.15

            AbsTime = 1410303600 + SimTime
            time = datetime.datetime.fromtimestamp(AbsTime)
            if (time-datetime.datetime(1970, 1, 1)).total_seconds()<1414292400 or (time-datetime.datetime(1970, 1, 1)).total_seconds()>1427594400:
                time = time + datetime.timedelta(hours=-1)
            # Transform AbsTime into dateformat
            date = time.strftime('%Y-%m-%d %H:%M:%S')
            print(date)
            # Load relevant data from disturbances and forecasts file
            daily_sin = self.disturbances['TimeFunc(daily_sin) []'][date]
            daily_cos = self.disturbances['TimeFunc(daily_cos) []'][date]
            weekly_sin = self.disturbances['TimeFunc(weekly_sin) []'][date]
            weekly_cos = self.disturbances['TimeFunc(weekly_cos) []'][date]
            TAmb_fc6 = self.forecasts['weaDat.weaBus.TDryBul_fc6 []'][date]
            TAmb_fc12 = self.forecasts['weaDat.weaBus.TDryBul_fc12 []'][date]
            TAmb_fc18 = self.forecasts['weaDat.weaBus.TDryBul_fc18 []'][date]
            TAmb_fc24 = self.forecasts['weaDat.weaBus.TDryBul_fc24 []'][date]
            TAmb_fc30 = self.forecasts['weaDat.weaBus.TDryBul_fc30 []'][date]
            TAmb_fc36 = self.forecasts['weaDat.weaBus.TDryBul_fc36 []'][date]
            HDirNor_fc6 = self.forecasts['weaDat.weaBus.HDirNor_fc6 []'][date]
            HDirNor_fc12 = self.forecasts['weaDat.weaBus.HDirNor_fc12 []'][date]
            HDirNor_fc18 = self.forecasts['weaDat.weaBus.HDirNor_fc18 []'][date]
            HDirNor_fc24 = self.forecasts['weaDat.weaBus.HDirNor_fc24 []'][date]
            HDirNor_fc30 = self.forecasts['weaDat.weaBus.HDirNor_fc30 []'][date]
            HDirNor_fc36 = self.forecasts['weaDat.weaBus.HDirNor_fc36 []'][date]
            TZoneSet_fc6 = self.forecasts['LowerBound(Zone Temperature)_fc6 []'][date]
            TZoneSet_fc12 = self.forecasts['LowerBound(Zone Temperature)_fc12 []'][date]
            TZoneSet_fc18 = self.forecasts['LowerBound(Zone Temperature)_fc18 []'][date]
            TZoneSet_fc24 = self.forecasts['LowerBound(Zone Temperature)_fc24 []'][date]
            TZoneSet_fc30 = self.forecasts['LowerBound(Zone Temperature)_fc30 []'][date]
            TZoneSet_fc36 = self.forecasts['LowerBound(Zone Temperature)_fc36 []'][date]

            X_unscaled = [TZone, TAmb, HDirNor, TFußboden, TError, TZoneSet, daily_sin, daily_cos, weekly_sin, weekly_cos,
                          TAmb_fc6, TAmb_fc12, TAmb_fc18, TAmb_fc24, TAmb_fc30, TAmb_fc36, HDirNor_fc6, HDirNor_fc12,
                          HDirNor_fc18, HDirNor_fc24, HDirNor_fc30, HDirNor_fc36, TZoneSet_fc6, TZoneSet_fc12,
                          TZoneSet_fc18, TZoneSet_fc24, TZoneSet_fc30, TZoneSet_fc36]

            df_X_unscaled = pd.DataFrame(data=X_unscaled)

            # Output to send to MQTT
            # TODO-Tipp: Du kannst den Predictor einmal laden und sparst so Zeit. Also ist hier egal, aber generell gut :)
            # Predictor = jl.load(os.path.join(ResultsFolderSubTest, "BestModels", "%s.save" % (
            #    NameOfPredictor)))
            y_predicted = main_OnlyPredict(
                self.config.results_folder,
                self.config.results_folder_sub_test,
                df_X_unscaled,
                OnlyPredictRecursive=False,
                signal="TVL"
            )
            if y_predicted[0] < 293.15:
                y_predicted[0] = 293.15
            elif y_predicted[0] > 313.15:
                y_predicted[0] = 313.15

            self._set_output_value(name="T_VL_ampc", value=y_predicted[0])
        else:
            return