from agentlib.modules.communicator.mqtt import MQTTClientConfig, MqttClient
from agentlib.core.datamodels import AgentVariable


class HiLMQTTClientConfig(MQTTClientConfig):
    pubtopic: str


class HiLMqttClient(MqttClient):

    config_type = HiLMQTTClientConfig

    @property
    def pubtopic(self):
        return self.config.pubtopic

    def _callback(self, variable: AgentVariable):
        """Publish the given output"""
        topic = '/'.join([self.pubtopic, variable.alias])
        self._mqttc.publish(topic=topic,
                            payload=variable.value,
                            qos=self.config.qos,
                            retain=False,
                            properties=None)

    def _message_callback(self, client, userdata, msg):
        """
        The default callback for when a PUBLISH message is
        received from the server.
        """
        variable_name = msg.topic.split("/")[-1]
        agent_inp = AgentVariable(
            name=variable_name,
            value=float(msg.payload)
        )
        self.logger.info("Received variable %s = %s",
                         agent_inp.alias, agent_inp.value)
        self.agent.data_broker.send_variable(agent_inp)

    def _get_all_topics(self):
        """
        Helper function to return all topics the client
        should listen to.
        """
        topics = set(self.config.subscriptions)
        topics.update(set(self.config.subtopics))
        return topics
