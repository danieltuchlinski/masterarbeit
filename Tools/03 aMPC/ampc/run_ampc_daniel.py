import json
import pandas as pd
import utilities.fmu_handler as fmu_handler
from utilities.pickle_handler import *
import time
import os
import physical_ampc_controller as ampc_controller
import shutil


class SimulateAMPC:
    """
    Simulates FMU with Addmo-based AMPC output
    """

    def __init__(self, path_simulation_setup):
        """
        Initializes Subcomponents
        load simulation options as json
        """

        """ FMU Settings and Initialization """
        with open(path_simulation_setup, 'r') as f:
            self.setup = json.load(f)
        print('Initialize FMU')
        self.start_time = self.setup['start_time']
        self.stop_time = self.setup['stop_time']
        self.fmu_step_size = self.setup['fmu_step_size']
        self.fmu_tolerance = self.setup['fmu_tolerance']
        self.print_interval = self.setup['print_interval']
        self.path_to_fmu = self.setup['path_to_fmu']
        self.instance_name = self.setup['instance_name']
        self.fmu_bus = self.setup['fmu_bus']
        self.save = self.setup['save']
        self.savename = self.setup['savename']
        self.__initialize_fmu()
        self.path_to_ampc_results = self.setup['path_to_ampc_results']

        """ AMPC & RB Controller Settings and Initialization """
        print('Initialize Controller')
        self.mpc_step_size = self.setup['mpc_step_size']
        self.path_to_mos = self.setup['path_to_mos']
        self.path_to_ampc_mapping = self.setup['path_to_ampc_mapping']

        # Mapping
        with open(self.path_to_ampc_mapping, 'r') as f:
            self.mapping_ampc = json.load(f)
        self.mapping_TVL = self.mapping_ampc['TVL']
        self.mapping_ampc_addmo_bestdata_to_processedinput = self.mapping_ampc['mapping_addmo_bestdata_to_dataframes']

        """ Initialize Dataframe for Simulation"""
        self.__initialize_sim_data()

        """ Initialize Dict to Store Optimization Results """
        self.ampc_results = pd.DataFrame({'u_T_VL': [293.15]})
        self.results = pd.DataFrame(columns=['u_T_VL'])
        # TODO: Adapt so that automatic mapping
        self.mapped_controls = pd.DataFrame({'u_T_VL': [293.15]})

        # Import necessary BestData and ProcessedInputData files
        self.signal_list = ['TVL']
        self.paths_processed_input_data_pickles = {}
        self.paths_bestdata = {}
        self.paths_scalertrackerfeatures = {}
        self.processed_input_data = {}
        self.bestdata = {}
        self.features_list = {}

        # Create paths to files
        for signal in self.signal_list:
            # paths to ScalerTrackerFeatures.save files
            self.paths_scalertrackerfeatures[signal] = os.path.join("/".join(
                list(self.setup["ResultsFolderSubTest_" + signal].split('/')[0:-2])),
                'ScalerTrackerFeatures.save')

            # paths to BestData
            self.paths_bestdata[signal] = os.path.join(
                self.setup["ResultsFolderSubTest_" + signal],
                str.join('', ['BestData_', self.setup["ResultsFolderSubTest_" + signal].split('/')[-1], '.xlsx']))

            # read BestData if it exists
            if os.path.isfile(self.paths_bestdata[signal]):
                self.bestdata[signal] = pd.read_excel(self.paths_bestdata[signal], nrows=1)
            else:
                print('There is no BestData.xlsx for ' + signal +
                      '! Used features will be read from ProcessedInputData.')

            # read ProcessedInputData if needed
            # Create paths
            self.paths_processed_input_data_pickles[signal] = {}
            self.paths_processed_input_data_pickles[signal]['ImportData'] = os.path.join("/".join(
                list(self.setup["ResultsFolderSubTest_" + signal].split('/')[0:-2])),
                'Pickles', 'ThePickle_from_ImportData.pickle')
            self.paths_processed_input_data_pickles[signal]['FeatureSelection'] = os.path.join("/".join(
                list(self.setup["ResultsFolderSubTest_" + signal].split('/')[0:-2])),
                'Pickles', 'ThePickle_from_FeatureSelection.pickle')

            # read ProcessedInputData from pickles
            # ImportData is needed, if FeatureScaler has to be build
            # FeatureSelectin is needed for list of used features, if BestData does not exist
            self.processed_input_data[signal] = {}
            self.processed_input_data[signal]['ImportData'] = pd.read_pickle(
                self.paths_processed_input_data_pickles[signal]['ImportData'])
            self.processed_input_data[signal]['FeatureSelection'] = pd.read_pickle(
                self.paths_processed_input_data_pickles[signal]['FeatureSelection'])

            # Create features_list for all relevant signals
            # check if BestData.xlsx exists
            if signal in self.bestdata:
                # Read all columns to get used features
                used_features = self.bestdata[signal].columns
                # Create lists for features and feature lags
                self.features_list[signal] = used_features[1:].to_list()
            # get used features from ProcessedInputData.xlsx if Bestdata.xlsx does not exist
            else:
                used_features = self.processed_input_data[signal]['FeatureSelection'].columns
                # Create lists for features and feature lags
                self.features_list[signal] = used_features[1:].to_list()

        # Initialize AMPC controller
        self.__init_ampc(path_simulation_setup, self.features_list, self.processed_input_data)

        """ Initialize Disturbances """
        print('Initialize Disturbances')
        self.__initialize_disturbances()
        # Inital time step
        self.current_AMPC_step = 0

        # Time for control processing
        self.pred_times = pd.DataFrame({'ampc_step': [0]})
        self.pred_times_ann = pd.DataFrame({'ampc_step': [0]})

    def __init_ampc(self, path_simulation_setup, dict_feature_lists, processed_input_data_dict):
        """
        Initializes instance of class AMPC controller
        :param path_simulation_setup: path to set up json
        :return:
        """
        self.ampc = ampc_controller.AMPC_Controller(path_simulation_setup, dict_feature_lists,
                                                    processed_input_data_dict)

    def __initialize_disturbances(self):
        """
        Initialize the disturbance dataframe which is used for forecasting and resample it for usage in AMPC
        :return:
        """
        # Import disturbances from pickle file
        self.path_disturbances = self.setup['path_to_disturbances']
        self.disturbances = pd.read_pickle(self.path_disturbances)
        # self.disturbances.set_index(pd.date_range("2014-09-10", periods=36253, freq="10min").to_series())

    def __initialize_fmu(self):
        """
        Initialize fmu to be simulated
        :return:
        """
        self.fmu = fmu_handler.fmu_handler(start_time=self.start_time,
                                           stop_time=self.stop_time,
                                           step_size=self.fmu_step_size,
                                           sim_tolerance=self.fmu_tolerance,
                                           fmu_file=self.path_to_fmu,
                                           instanceName=self.instance_name)

    def __initialize_sim_data(self):
        """
        Initializes Data Frame with relevant datapoints
        :return:
        """
        self.fmu_vars = []  # self.fmu.find_vars('thermalZone1.weaBus')# self.fmu.find_vars(self.fmu_bus)
        # Measurements from fmu
        for key in self.mapping_TVL.keys():
            self.fmu_vars.append(self.mapping_TVL[key])

        # remove duplicates
        self.fmu_vars = list(set(self.fmu_vars))
        self.simulation_data = pd.DataFrame(columns=self.fmu_vars + ['SimTime'])

    def get_ampc_control(self):

        """
        Perform AMPC step
        1. Check current time step
        2. get states
        3. get forecasts
        4. perform MPC step
        :return:
        """
        perform_mpc_step = True
        if perform_mpc_step:
            # Get disturbances
            dist = self.disturbances

            if self.current_AMPC_step < 36:
                u_TVL = 273.15+20

                # safe results
                new_result = {'u_T_VL': u_TVL}
                self.results = self.results.append(new_result, ignore_index=True)

            else:
            # todo: geändert
            # print("Input in set_ampc_input: results:")
            # print(self.results)

                X_mea_unscaled_TVL = self.ampc.set_ampc_input(
                    simulation_mea=self.simulation_data,
                    ampc_timestep=self.current_AMPC_step,
                    dist=dist,
                    signal='TVL',
                    features_list=self.features_list['TVL'],
                    mapping=self.mapping_ampc_addmo_bestdata_to_processedinput,
                    results_ampc=self.results)

                u_TVL, time_ann = self.ampc.get_control(
                    signal='TVL',
                    X_mea_unscaled_signal=X_mea_unscaled_TVL,
                    ampc_outputs=self.results,
                    ampc_timestep=self.current_AMPC_step)

                # update self.results with temporary 0-entry for TAhuSet
                new_result = {'u_T_VL': u_TVL}
                self.results = self.results.append(new_result, ignore_index=True)

                self.pred_times_ann = self.pred_times_ann.append({'ampc_step': time_ann}, ignore_index=True)

            # TODO: Here: fmu variable naming is used
            self.mapped_controls.update({'u_T_VL': [u_TVL]})
            # TODO: adapt so that automatic mapping is performed
            self.ampc_results.update({'u_T_VL': [u_TVL]})

            self.current_AMPC_step += 1

    def update_measurements(self):
        """
        load new simulation data from fmu
        :return:
        """
        res = self.fmu.read_variables(self.fmu_vars)
        self.simulation_data = self.simulation_data.append(pd.DataFrame(res, index=[res['SimTime']]))

    def write_inputs(self):
        """
        Write inputs to fmu
        :return:
        """

        self.fmu.set_variables(self.mapped_controls)

    def simulation_step(self):
        """
        Perform one simulation step with actual
        :return:
        """
        finished = self.fmu.do_step()
        return finished

    def close_simulation(self):
        """
        Close FMU and save Results
        :return:
        """
        self.fmu.close()
        if self.save:
            write_pickle(self.savename, {'Simulation_res': self.simulation_data, 'setup': self.setup,
                                         'Disturbances': self.disturbances})

        # Save as csv data
        path = self.path_to_ampc_results
        self.simulation_data.to_csv(os.path.join(path, "sim.csv"))
        self.disturbances.to_csv(os.path.join(path, "dist.csv"))
        self.results.to_csv(os.path.join(path, "ampc_output.csv"))
        self.pred_times.to_csv(os.path.join(path, "pred_times.csv"))
        self.pred_times_ann.to_csv(os.path.join(path, "pred_times_ann.csv"))

        # Save .csv in format for evaluation.py
        sim = self.simulation_data.reset_index(drop=True)
        res = self.results.reset_index(drop=True)
        # Drop time delay data
        dist = self.disturbances.iloc[self.ampc.time_delay:, :].reset_index(drop=True)

        # Concat and save csv
        results_for_evaluation = pd.concat([sim, res, dist], axis=1, sort=False)
        results_for_evaluation.to_csv(os.path.join(path, "results_ampc.csv"))

    def run(self):
        """
        Perform Simulation and AMPC
        :return:
        """
        self.fmu.setup()
        self.fmu.initialize()

        finished = False
        start_time = time.time()
        print('Start Simulation')

        # Initial value for output
        u_TVL = 293.15

        while not finished:
            self.update_measurements()  # get Values from fmu

            # Measure duration of control feedback (relevant for real time applicability)
            time_control_start = time.time()
            # Get ampc control output
            self.get_ampc_control()
            # Measure end time of control step
            time_control_end = time.time()
            # Append prediction time vector by current prediction time
            self.pred_times = self.pred_times.append({'ampc_step': time_control_end - time_control_start},
                                                     ignore_index=True)
            # Write inputs to fmu
            self.write_inputs()
            finished = self.simulation_step()

            if self.fmu.current_time % self.print_interval == 0:
                print("-------------------------------------------------------------------------------------")
                print(
                    f'Simulated {(self.fmu.current_time - self.start_time) / (self.stop_time - self.start_time) * 100} % | Current Time: {self.fmu.current_time / 3600} hours')
                print(f'Elapsed Time {(time.time() - start_time) / 60} min')
        print(f"finished simulation in {(time.time() - start_time) / 60} min")
        self.close_simulation()


if __name__ == '__main__':

    # Create forecasts and disturbances file
    path_ddmpc = r'D:\lma-dtu TEMP\masterarbeit\Tools\Results\PI_Controller_updated\ddmpc.csv'
    data = pd.read_csv(path_ddmpc, index_col=0, sep=",")
    forecasts = data.filter(regex='_fc')
    disturbances = data.iloc[:, [2, 3, 7, 8, 12, 14, 16, 18, 19, 20, 21]]
    forecasts.to_pickle(r'D:\lma-dtu TEMP\masterarbeit\Tools\03 aMPC\data\forecasts_daniel.pickle')
    disturbances.to_pickle(r'D:\lma-dtu TEMP\masterarbeit\Tools\03 aMPC\data\disturbances_daniel.pickle')

    path_setup = r"D:\lma-dtu TEMP\masterarbeit\Tools\03 aMPC\setup\setup_standard.json"
    Sim = SimulateAMPC(path_setup)

    Sim.run()

    results_ampc = pd.read_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\03 aMPC\results\results_ampc.csv", sep=",")
    results_ampc = results_ampc.set_index(pd.date_range("2014-09-10 00:00:00", periods=len(results_ampc), freq="10min").to_series())
    results_ampc.to_csv(r"D:\lma-dtu TEMP\masterarbeit\Tools\Results\ampc.csv")








