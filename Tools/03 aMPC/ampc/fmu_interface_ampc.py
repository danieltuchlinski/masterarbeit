import utilities.fmu_handler as fmu_handler
import pandas as pd
import matplotlib.pyplot as plt

# Initialize
path_fmu = r"D:\01_Modellierung\evaluation-of-ai-based-control-applications\fmu\ashrae140_900_set_point_fmu.fmu"

sim_tolerance = 0.00001  # tolerance
start_time = 0  # start time
stop_time = 3600 * 1  # stop time
step_size = 60

fmu = fmu_handler.fmu_handler(start_time=start_time,
                              stop_time=stop_time,
                              step_size=step_size,
                              sim_tolerance=sim_tolerance,
                              fmu_file=path_fmu,
                              instanceName='fmu2')
# Targets
T_set_ahu = 297
Q_set_cca = 3

variables = fmu.find_vars('Bus')
variables.append('QFlowTabsSet')
variables.append('TAhuSet')
# initialize fmu
fmu.setup()
fmu.initialize()

# init for while loop
finished = False

# initialization for data frame
init_df = False
df_interval = 1 * fmu.step_size  # data storage interval

while not finished:
    # read variables
    res = fmu.read_variables(variables)

    # store data in dataframe
    if not init_df:
        df = pd.DataFrame(res, index=[0])
        init_df = True
    else:
        if fmu.current_time % df_interval == 0:
            df = df.append(pd.DataFrame(res, index=[0]), ignore_index=True)


    fmu.set_variables({'QFlowTabsSet':Q_set_cca,'TAhuSet':T_set_ahu})

    print(str(fmu.current_time / 3600) + ' hours')

    finished = fmu.do_step()

print('Stepsize ' + str(step_size))
print('Tolerance ' + str(sim_tolerance))

# close fmu
fmu.close()




df[['Bus.TZoneMea', 'Bus.ahuBus.heaterBus.TAirOutMea']].plot()
plt.show()