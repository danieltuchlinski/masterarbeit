import ampc.ampc_basics
import ampc_basics
import pandas as pd
import os
import json
import numpy as np
import re


class AMPC_Controller:
    """
    AMPC controller class based on ML tool AddMo
    """

    def __init__(self, path_simulation_setup, dict_relevant_features, processed_input_data):
        """
        load simulation options as json
        """
        """ Folder settings """
        with open(path_simulation_setup, 'r') as f:
            self.setup = json.load(f)
        print('Initialize AMPC Controller')

        # Folder to AddMo results
        self.ResultsFolder_TVL = self.setup['ResultsFolder_TVL']  # ToDo: Check if i acutally use these

        self.ResultsFolderSubTest_TVL = self.setup['ResultsFolderSubTest_TVL']

        # Options for predict only function in AddMo
        self.OnlyPredictRecursive = self.setup['OnlyPredictRecursive']

        # Initialize ampc controller by loading used feature scaler in Addmo
        # Scaler needs to be applied on measurement data inverse and forward
        # for QCCA
        self.signal = 'TVL'
        self.scaler_TVL = ampc_basics.apply_feature_scalers(
            signal=self.signal,
            used_features_list=dict_relevant_features[self.signal],
            processed_input_data=processed_input_data[self.signal],
            results_folder=self.ResultsFolder_TVL)


        # Create data frames for day of hour and time of day
        # Starts with a Monday == day = 1
        # Length of data frame: 1 year + 2 weeks
        self.df_time_related_features = pd.DataFrame(
            index=pd.date_range("2014-09-10 00:00", periods=36252, freq="10min").to_series())
        self.df_time_related_features['weekday [d]'] = self.df_time_related_features.index.dayofweek + 1
        self.df_time_related_features['Time of day [h]'] = self.df_time_related_features.index.hour + \
            self.df_time_related_features.index.minute / 60

        # Create data frame with forecasts
        self.forecasts = pd.read_pickle(r'..\data\forecasts_daniel.pickle')
        # self.forecasts.set_index(pd.date_range("2014-09-10 00:00", periods=36253, freq="10min").to_series())

        # Control outputs/Sim inputs
        self.inputs = ['u_T_VL']

        # Two week delay
        self.time_delay = 0

        # AMPC step in seconds
        self.ampc_step_size = 600

        # # Upper and lower change thresholds for delta control signals
        # # Todo: check boundaries
        self.TVL_max = 273.15+40
        self.TVL_min = 273.15+20

    def get_control(self, signal, X_mea_unscaled_signal, ampc_timestep, ampc_outputs):
        """
        Solves approximate MPC Problem based on given inputs, disturbances and references

        :param signal: signal (str) for that method was called (dTAHU or QCCA)
        :param X_mea_unscaled_signal: dict with features for prediction step
        :param ampc_timestep: current_AMPC_step
        :param ampc_outputs dictionary with Optimization Results
        :return: control value (and delta) of signal
        """
        if signal == 'TVL':
            u_signal, time_ann = ampc_basics.ampc_predict(
                scaler_features_signal=self.scaler_TVL,
                _X_test_unscaled_signal=X_mea_unscaled_signal,
                OnlyPredictRecursive=self.OnlyPredictRecursive,
                ResultsFolder_signal=self.ResultsFolder_TVL,
                ResultsFolderSubTest_signal=self.ResultsFolderSubTest_TVL)
        else:
            raise Exception("Signal must be 'TVL'! You gave " + signal + " !")

        if signal == 'TVL':
            u_TVL = self.transform_control_vars(signal=signal,
                                                          u_signal=u_signal,
                                                          ampc_outputs=ampc_outputs,
                                                          ampc_timestep=ampc_timestep)
            # print("udQcca nach Adaption")
            # print(u_dQCCA_adapted)
            # print(u_QCCA)

            return u_TVL, time_ann
        else:
            raise Exception("Signal must be 'TVL'! You gave " + signal + " !")

    def transform_control_vars(self, signal, u_signal, ampc_outputs, ampc_timestep):
        """
        Transforms delta signals into absolute values or vice versa
        :param self:
        :param signal: signal for that the method was called (dTAHU or QCCA)
        :param u_signal: Current signal/control value
        :param ampc_timestep: current_AMPC_step
        :param ampc_outputs dictionary with Optimization Results
        :return: absolute control variables (with or without delta)
        """

        if signal == 'TVL':

            u_TVL = u_signal

            # Check if control variables are in bound
            if u_TVL > self.TVL_max:
                u_TVL = self.TVL_max
            elif u_TVL < self.TVL_min:
                u_TVL = self.TVL_min

            return u_TVL
        else:
            raise Exception("Signal must be 'TVL'! You gave " + signal + " !")

    def set_ampc_input(self, simulation_mea, ampc_timestep, dist, signal, features_list, mapping, results_ampc):
        """
        Prepare input for one ampc step
        :param self:
        :param simulation_mea: Simulation results data
        :param ampc_timestep: current_AMPC_step
        :param dist: disturbances
        :param signal: signal, for that feature input shall be prepared (dTAHU or QCCA)
        :param features_list: list of used features for prediction
        :param mapping: mapping of features to their column names in DataFrames
        :param results_ampc: ampc results for u_QCCA and u_TAHU and u_dQCCA
        :return: unscaled feature input for signal
        """

        timestep = ampc_timestep + self.time_delay
        print("AMPC Timestep:")
        print(ampc_timestep)
        sim_time = ampc_timestep * self.ampc_step_size + self.setup['start_time']
        print("Time Step of disturbances")
        print(timestep)

        # Inputs for signal
        feature_input_signal = pd.DataFrame(index=range(len(features_list)), columns=range(1))
        feature_input_signal.set_axis(features_list, inplace=True)

        # Create dictionary for looping through different input dataframes
        dict_input_sources = {"simulation_mea": simulation_mea,
                              "dist": dist,
                              "df_time": self.df_time_related_features,
                              "forecasts": self.forecasts,
                              "ampc_results": results_ampc
                              }
        # Create row to count upwards in index of feature_input_signal
        row = 0
        # Fill entries for feature input
        for feature in features_list:
            # Introduce boolean to check later if column was found in any of the sources or a typo might have occured
            column_used = False
            # Find feature in one of the input source dataframes
            for input_source_key in dict_input_sources.keys():
                # set timestep according to input source dataframe
                if input_source_key == "simulation_mea":
                    input_timestep = sim_time
                elif input_source_key == 'ampc_results':
                    input_timestep = ampc_timestep
                else:
                    input_timestep = timestep

                # If feature (without lags) is in the current input source dataframe:
                #  add entry in feature_input_signal
                if mapping[feature.split('_lag')[0]] in \
                        dict_input_sources[input_source_key] and "lag" not in feature:
                    feature_input_signal[0][row] = dict_input_sources[input_source_key][
                        mapping[feature]][input_timestep]
                    row += 1
                    column_used = True
                    break

                # If feature (with lags) is in the current input source dataframe: add entry in feature_input_signal
                elif mapping[feature.split('_lag')[0]] in \
                        dict_input_sources[input_source_key] and "lag" in feature:
                    # set lag
                    lag = int(feature.split('_lag')[1])
                    # set format for timestep with lag in current input source dataframe
                    if input_source_key == "simulation_mea":
                        input_timestep_lag = input_timestep - lag * self.ampc_step_size
                    else:
                        input_timestep_lag = input_timestep - lag
                    # add feature lag entry
                    feature_input_signal[0][row] = dict_input_sources[input_source_key][
                        mapping[feature.split('_lag')[0]]][input_timestep_lag]
                    row += 1
                    column_used = True
                    break
            # check if column was found in any of the sources. Otherwise there might be a typo in the mapping
            if not column_used:
                raise Exception(mapping[feature.split('_lag')[0]] +
                                ' not found in any of the sources. Please check for typos!')


        print("Unscaled feature input for ML algo")
        print("for " + signal)
        print(feature_input_signal)
        return feature_input_signal


if __name__ == '__main__':
    df_time_of_day_hour = pd.date_range("2015-24-09", periods=36144, freq="10min").to_series()

    df_time_of_day_hour['weekday'] = df_time_of_day_hour[df_time_of_day_hour.index].dt.dayofweek + 1

    df_time_of_day_hour = pd.read_excel(os.path.join(
        r'D:\01_Modellierung\evaluation-of-ai-based-control-applications\ampc\Training_results\PhysicalMPC\Results\ph_wcFL_wOL_ANN210N_12m_RScal_wfc_0_89_dTAHU',
        'ProcessedInputData_small.xlsx'),
        sheet_name='ImportData', usecols='X', header=0)
    # df2 = df_time_of_day_hour['time_day']
    # print(df_time_of_day_hour['time_day'])
    # klappt: print(df_time_of_day_hour['weekday'].loc[df_time_of_day_hour.index[95]])
    # print(df2['Time of day [h]'].loc[df_time_of_day_hour.index[5]])
    print(df_time_of_day_hour['Time of day [h]'].loc[df_time_of_day_hour.index[1]])
    # dataframe[‘column_name’].loc[dataframe.index[row_number]]

    # print(df_time_of_day_hour.loc[1344]['weekday'])
