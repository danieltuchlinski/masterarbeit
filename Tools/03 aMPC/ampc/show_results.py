import pandas as pd
import matplotlib.pyplot as plt
import json
import matplotlib
import numpy as np
import datetime
import pickle
matplotlib.rcParams.update({'font.size': 10})

path_to_results_ampc = r"..\results"
path_to_results_ddmpc = r"D:\lma-dtu TEMP\masterarbeit\Tools\03 aMPC\data\AddMo_Trainings_results\Standard\Pickles\ThePickle_from_ImportData.pickle"
path_to_results_hc = r"D:\lma-dtu TEMP\masterarbeit\Tools\01 ddMPC\Examples\FMUs\HeatingCurve\stored_data\data\HeatingCurve_WithoutOffset.xlsx"

startdate = "2015-02-14 00:00:00"
enddate = "2015-02-20 23:50:00"

A_building = 158
########################################################################################################################
# aMPC
with open(path_to_results_ampc+"\setup_standard.json", 'r') as f:
    setup = json.load(f)

starttime = setup["start_time"]
endtime = setup["stop_time"]

dist = pd.read_csv(path_to_results_ampc+"\dist.csv", index_col=0)
sim = pd.read_csv(path_to_results_ampc+"\sim.csv")

startsteps=starttime/600
endsteps = endtime/600
sim.index = dist.index[int(startsteps):int(endsteps+1)]
sim.index = pd.to_datetime(sim.index)
dist.index = pd.to_datetime(dist.index)

# Switches_ampc = sim.loc[enddate]["hydraulic.generation.outBusGen.heaPum.numSwi"]-sim.loc[startdate]["hydraulic.generation.outBusGen.heaPum.numSwi"]
# Energy_ampc = (sim.loc[enddate]["outputs.hydraulic.gen.PEleHP.integral"]-sim.loc[startdate]["outputs.hydraulic.gen.PEleHP.integral"])/1000/3600
# Diskomfort_ampc = sim["TError"].mask(sim["TError"] > 0, 0).abs().sum()/6
# Heat_ampc = (sim.loc[enddate]["outputs.hydraulic.gen.QHP_flow.integral"]-sim.loc[startdate]["outputs.hydraulic.gen.QHP_flow.integral"])/1000/3600
# SCOP_ampc = Heat_ampc/Energy_ampc
# Specific_heat_ampc = Heat_ampc/A_building

########################################################################################################################
# ddMPC
fig, axs = plt.subplots(2, figsize=(8, 5))
ddmpc = pd.read_pickle(path_to_results_ddmpc)
hc = pd.read_excel(path_to_results_hc, index_col=0)

# axs[0].set_title("1.-7. April", size=)
axs[0].plot(dist.index[int(sim.loc[startdate]["SimTime"]/600):int(sim.loc[enddate]["SimTime"]/600+1)], ddmpc["LowerBound(Zone Temperature) []"][startdate:enddate]-273.15, color="lightgrey", label="Solltemperatur")
axs[0].plot(dist.index[int(sim.loc[startdate]["SimTime"]/600):int(sim.loc[enddate]["SimTime"]/600+1)], hc["outputs.building.TZone[1]"][startdate:enddate]-273.15, "--", color="blue", label="Heizkurve", linewidth=0.5)
axs[0].plot(dist.index[int(sim.loc[startdate]["SimTime"]/600):int(sim.loc[enddate]["SimTime"]/600+1)], ddmpc["outputs.building.TZone[1] []"][startdate:enddate]-273.15, color="black", label="ddMPC")
axs[0].plot(dist.index[int(sim.loc[startdate]["SimTime"]/600):int(sim.loc[enddate]["SimTime"]/600+1)], sim["outputs.building.TZone[1]"][startdate:enddate]-273.15, color="red", label="aMPC")
# axs[0].set_ylabel("Zonentemp. in °C", size=14)
axs[0].set_xticks([])
axs[0].legend(loc="lower left")

axs[1].plot(dist.index[int(sim.loc[startdate]["SimTime"]/600):int(sim.loc[enddate]["SimTime"]/600+1)], hc["hydraulic.control.heatingCurveDaniel.TSet"][startdate:enddate]-273.15, "--", color="blue", label="Heizkurve", linewidth=0.5)
axs[1].plot(dist.index[int(sim.loc[startdate]["SimTime"]/600):int(sim.loc[enddate]["SimTime"]/600+1)], ddmpc["u_T_VL []"][startdate:enddate]-273.15, color="black", label="ddMPC")
axs[1].plot(dist.index[int(sim.loc[startdate]["SimTime"]/600):int(sim.loc[enddate]["SimTime"]/600+1)], sim["u_T_VL"][startdate:enddate]-273.15, color="red", label="aMPC")
# axs[1].set_ylabel("Vorlauftemp. in °C", size=14)
# axs[1].set_xticks([])
axs[1].set_xlabel("Datum", size=20)

# axs[2].plot(dist.index[int(sim.loc[startdate]["SimTime"]/600):int(sim.loc[enddate]["SimTime"]/600+1)], ddmpc["hydraulic.control.sigBusGen.yHeaPumSet []"][startdate:enddate], color="black", label="ddMPC")
# axs[2].plot(dist.index[int(sim.loc[startdate]["SimTime"]/600):int(sim.loc[enddate]["SimTime"]/600+1)], sim["hydraulic.control.sigBusGen.yHeaPumSet"][startdate:enddate], color="red", label="aMPC")
# axs[2].set_ylabel("Rel. Leistung WP", size=14)
# axs[2].set_xticks([])
#
#
# axs[3].plot(dist.index[int(sim.loc[startdate]["SimTime"]/600):int(sim.loc[enddate]["SimTime"]/600+1)], ddmpc["weaDat.weaBus.TDryBul []"][startdate:enddate]-273.15)
# axs[3].set_ylabel("Außentemp. in °C", size=14)
# axs[3].set_xlabel("Datum", size=16)
# axs[3].set_xticks([])

plt.show()

# Switches_ddmpc = ddmpc.loc[enddate]["hydraulic.generation.outBusGen.heaPum.numSwi []"]-ddmpc.loc[startdate]["hydraulic.generation.outBusGen.heaPum.numSwi []"]
# Energy_ddmpc = (ddmpc.loc[enddate]["outputs.hydraulic.gen.PEleHP.integral []"]-ddmpc.loc[startdate]["outputs.hydraulic.gen.PEleHP.integral []"])/1000/3600
# Diskomfort_ddmpc = ddmpc["TError []"][startdate:enddate].mask(ddmpc["TError []"][startdate:enddate] > 0, 0).abs().sum()/6
# Heat_ddmpc = (ddmpc.loc[enddate]["outputs.hydraulic.gen.QHP_flow.integral []"]-ddmpc.loc[startdate]["outputs.hydraulic.gen.QHP_flow.integral []"])/1000/3600
# SCOP_ddmpc = Heat_ddmpc/Energy_ddmpc
# Specific_heat_ddmpc = Heat_ddmpc/A_building
#
# #######################################################################################################################
# print("Switches pro Tag (aMPC): "+str(Switches_ampc/212))
# print("Switches pro Tag (ddMPC): "+str(Switches_ddmpc/212))
# print("Energie (aMPC): "+str(Energy_ampc)+" kWh")
# print("Energie (ddMPC): "+str(Energy_ddmpc)+" kWh")
# print("Diskomfort (aMPC): "+str(Diskomfort_ampc)+" Kh")
# print("Diskomfort (ddMPC): "+str(Diskomfort_ddmpc)+" Kh")
# print("SCOP (aMPC): "+str(SCOP_ampc))
# print("SCOP (ddMPC): "+str(SCOP_ddmpc))
# print("Specific Heat (aMPC): "+str(Specific_heat_ampc)+" kWh/m²")
# print("Specific Heat (ddMPC): "+str(Specific_heat_ddmpc)+" kWh/m²")

#######################################################################################################################
