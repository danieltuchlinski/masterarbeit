"""
Adapted script from AddMo Model Tuning for MPC loop
"""
# Package imports
import os
import sklearn
from sklearn.externals import joblib
import joblib as jl
import pandas as pd
import numpy as np
import time
from sklearn.preprocessing import RobustScaler
from sklearn_pandas import DataFrameMapper


def reshape(series):
    """
    Can reshape pandas series and numpy.array

    :param series:
    :type series: pandas.series or mumpy.ndarray
    :return: two dimensional array with one column (like a series)
    :rtype: ndarray
    """

    if isinstance(series, pd.Series):
        array = series.values.reshape(-1, 1)
    elif isinstance(series, pd.DataFrame):
        array = series.values.reshape(-1, 1)
    elif isinstance(series, np.ndarray):
        array = series.reshape(-1, 1)
    elif isinstance(series, list):
        array = np.array(series).reshape(-1, 1)
    else:
        print("reshape could not been done, unsupported data type{}".format(type(series)))

    return array


def predict(NameOfPredictor, _X_test, ResultsFolderSubTest, OnlyPredictRecursive):
    'Loads trained models from previous trainings and does a prediction for the respective period of _X_test. Individual models are regarded.'
    if os.path.isfile(os.path.join(ResultsFolderSubTest, "BestModels",
                                   "%s.save" % (NameOfPredictor))):  # to find out which indivmodel was used

        Predictor = jl.load(os.path.join(ResultsFolderSubTest, "BestModels", "%s.save" % (
            NameOfPredictor)))  # load the best and trained model from previous tuning and training
        if OnlyPredictRecursive == False:
            Predicted = Predictor.predict(_X_test)
        elif OnlyPredictRecursive == True:
            Features_test_i = recursive(_X_test, Predictor)
            Predicted = Predictor.predict(Features_test_i)
        IndividualModel = "None"
    else:
        return False, False
    return Predicted, IndividualModel


def only_predict(NameOfPredictor, _X_test_scaled, ResultsFolderSubTest, OnlyPredictRecursive):
    """
    Main predict function
    :param NameOfPredictor:
    :param _X_test: input data for FMU
    :return: ComputationTime for prediction and y_predicted
    """
    timestart = time.time()
    Predicted, IndividualModel = predict(NameOfPredictor, _X_test_scaled, ResultsFolderSubTest, OnlyPredictRecursive)
    if type(Predicted) == bool:
        print(
            "There is no trained model of %s to do OnlyPredict, if needed set OnlyPredict=False and train a model first." % NameOfPredictor)  # stop function if specific BestModel is not present
        return
    timeend = time.time()
    ComputationTime = (timeend - timestart)
    # Todo: geändert
    return ComputationTime, Predicted[0]
    # visualization_documentation(NameOfPredictor, Predicted, _Y_test, Indexer, None, ComputationTime, SV.OnlyPredictFolder, None, None, None,
    #                           None, SV.OnlyPredictRecursive, IndividualModel, None, None)


def apply_feature_scalers(signal, used_features_list, processed_input_data, results_folder):
    """
    Function reconstructs scaler used within AddMo for features and saves it in Result Folder as .save file

    :param signal: signal, for that scaler shall be prepared
    :param used_features_list: List of used features for the chosen signal
    :param processed_input_data: Dataframe with processed input data from Addmo
    :param results_folder: folder where the processedInputData.xlsx is stored (from AddMo)
    :return: fitted scaler
    """

    if signal == 'TVL':
        # load ScalerTrackerFeatures.save if it exists
        try:
            scaler_features = jl.load(os.path.join(results_folder, "ScalerTrackerFeatures.save"))
        # create ScalerTrackerFeatures if it does not exist
        except FileNotFoundError:
            print("\nThere is no 'ScalerTrackerFeatures.save'-file "
                  "for signal " + signal + "' yet.\nThe file will be created now!")
            list_for_df_features = []
            needed_column_names = [column.split('_lag')[0] for column in used_features_list]
            for column in needed_column_names:
                # find column in process input data and append it to list
                list_for_df_features.append(processed_input_data['ImportData'][column])
                # for lags use is in or something similar. The same column as for the original data is needed

            # Create df_features from list (this is faster than letting the dataframe grow)
            df_features = pd.DataFrame(list_for_df_features).T

            # Apply scaler
            scaler_features = RobustScaler()
            scaler_features.fit(df_features)
            joblib.dump(scaler_features, os.path.join(results_folder, "ScalerTrackerFeatures.save"))

    return scaler_features


def main_OnlyPredict(ResultsFolder, ResultsFolderSubTest, _X_test_unscaled, OnlyPredictRecursive, signal):
    # Reconstruct originally used scaler for features
    scaler_features = apply_feature_scalers(signal=signal, used_features_list=0, processed_input_data=0, results_folder=ResultsFolder)
    # Transpose data frame
    _X_test_unscaled = _X_test_unscaled.transpose()
    # Transform data based on scaler
    x_transformed = scaler_features.transform(_X_test_unscaled.copy())
    print("X Scaled:")
    print(x_transformed)
    # Call only predict main function
    AvailablePredictors = ["ann_bayesian_predictor"]
    for NameOfPredictor in AvailablePredictors:
        comp_time, y_predicted_scaled = only_predict(NameOfPredictor, x_transformed, ResultsFolderSubTest,
                                                     OnlyPredictRecursive)
        print("Y Scaled:")
        print(y_predicted_scaled)
    # Scale y values back to unscaled y
    if os.path.isfile(os.path.join(ResultsFolder, "ScalerTracker.save")):  # if scaler was used
        ScaleTracker_Signal = jl.load(os.path.join(ResultsFolder, "ScalerTracker.save"))  # load used scaler
        # Scale Results back to normal
        y_Predicted = ScaleTracker_Signal.inverse_transform(y_predicted_scaled.reshape(-1, 1))
        print("Y Prediction:")
        print(y_Predicted[0])
    return y_Predicted[0]


def ampc_predict(scaler_features_signal, _X_test_unscaled_signal, OnlyPredictRecursive,
                 ResultsFolder_signal, ResultsFolderSubTest_signal):
    # Transpose data frames
    _X_test_unscaled_signal = _X_test_unscaled_signal.transpose()

    # Transform data based on scaler
    x_transformed_signal = scaler_features_signal.transform(_X_test_unscaled_signal.copy())

    # Adapt x_transformed due to unscaled ownlag of dQset
    # Todo: find a nicer version for ownlag without scaling; now, ownlag is not scaled and therefore time series of
    # TODO: Zeros is used as scaling base
    # print(x_transformed_signal)

    # Call only predict main function
    AvailablePredictors = ["ann_bayesian_predictor"]

    for NameOfPredictor in AvailablePredictors:
        time_control_start = time.perf_counter()
        comp_time, y_predicted_scaled_signal = only_predict(NameOfPredictor, x_transformed_signal,
                                                            ResultsFolderSubTest_signal, OnlyPredictRecursive)

        time_control_end = time.perf_counter()
        time_ann = time_control_end - time_control_start

    # Scale y values back to unscaled y
    if os.path.isfile(os.path.join(ResultsFolder_signal, "ScalerTracker.save")):  # if scaler was used
        ScaleTracker_Signal = jl.load(
            os.path.join(ResultsFolder_signal, "ScalerTracker.save"))  # load used scaler
        # Scale Results back to normal
        y_Predicted_signal = ScaleTracker_Signal.inverse_transform(y_predicted_scaled_signal.reshape(-1, 1))

        y_Predicted_signal1 = y_Predicted_signal[0]

        y_Predicted_signal2 = y_Predicted_signal1[0]

        # print(y_Predicted_dQCC2)

    return y_Predicted_signal2, time_ann


if __name__ == '__main__':
    OnlyPredictRecursive = False
    signal = "TVL"
    # Set folders where results are stored
    ResultsFolder = r'D:\lma-dtu TEMP\masterarbeit\Tools\03 aMPC\data\AddMo_Trainings_results\TVL'
    ResultsFolderSubTest = r'D:\lma-dtu TEMP\masterarbeit\Tools\03 aMPC\data\AddMo_Trainings_results\TVL\Predictions\ANN'
    # Test data for dTAHU
    _X_test_unscaled = [286.3235490663736,271.35,0.0,-6.826450933626404,293.15,271.35,270.85,270.35,269.55,268.85,268.65,0.0,0.0,0.0,0.0,0.0,0.0,293.15,293.15,293.15,293.15,293.15, 293.15]
    print("X Unscaled:")
    print([_X_test_unscaled])
    df_X_test_unscaled = pd.DataFrame(data=_X_test_unscaled)

    y_Predicted = main_OnlyPredict(ResultsFolder, ResultsFolderSubTest, df_X_test_unscaled, OnlyPredictRecursive,
                                   signal)

