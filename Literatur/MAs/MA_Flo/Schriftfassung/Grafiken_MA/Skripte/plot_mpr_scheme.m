istx = [0;10; 20; 30; 40; 50; 60; 70; 80; 90];
isty = [10;10; 10; 13; 16; 15; 14.5; 15; 15; 15];

for i=1:90
    X(i)=i;
    if i < 10
        Stell(i)= 1;
        Soll(i)=10;
    end
    if i>= 10 && i < 20
        Stell(i)= 1;
        Soll(i)=10;
    end
    if i>=20 &&i< 30
        Stell(i)= 2;
        Soll(i)=10;
    end
    if i>=30 &&i< 40
        Stell(i)= 4;
        Soll(i)=15;
    end
    if i>=40 &&i< 50
        Stell(i)= 3;
        Soll(i)=15;
    end
    if i>=50 &&i< 60
        Stell(i)= 6;
        Soll(i)=15;
    end
    if i>=60 &&i< 70
        Stell(i)= 7;
        Soll(i)=15;
    end
    if i>=70 &&i< 80
        Stell(i)= 6;
        Soll(i)=15;
    end
    if i>=80 &&i< 90
        Stell(i)= 8;
        Soll(i)=15;
    end
end

%%
p = fit(istx,isty,'cubicinterp');

fig = figure;
plot(Stell);
hold on
plot(Soll);
plot(p, 'k');
xline(30,'LineWidth',2)
legend('Stellgröße','Sollgröße','Istgröße')
hold off

%set(groot,'defaultAxesTickLabelInterpreter','latex');  
grid on
set(gca,'xticklabel',{'k-2','k-1','k','k+1','k+2','k+3','...','k+N-1','k+N'})
set(gca,'yticklabel',{[]})
xlabel('Zeitschritt')
ylabel('')
xlim([1 90])
ylim([0 20])

xt = [10 32];
yt = [19 19];
str1 = sprintf('Zukunft');
str2 = sprintf('Vergangenheit');
text(xt,yt,{str2,str1})


%%
%matlab2tikz(); 
%cleanfigure; 
%matlab2tikz('mpr_scheme.tex', 'dataPath','C:\git\test_repo_2\Grafiken_MA')

%exportgraphics(ax,'mpr_scheme.pdf')
%Plot2LaTeX(fig,'mpr_scheme')
plotpdftex(fig,'mpr_scheme')


