n = 3;
T_r_nom = 20 + 273.15;
d_T_out = @(T_out) T_r_nom - T_out;
T_su_nom = 40 + 273.15;
T_re_nom = 33 + 273.15;
T_out_nom = -12 + 273.15;
d_T = (T_su_nom - T_re_nom)/(log((T_su_nom - T_r_nom)/(T_re_nom - T_r_nom)));
c1 = (T_su_nom - T_re_nom)/(T_r_nom - T_out_nom);
c2 = (c1 * (T_r_nom - T_out_nom)^(-1/n))/(d_T); 

Hea_cur = @(d_T_out) (T_r_nom - (T_r_nom + c1 * d_T_out) * exp(c2 * d_T_out^(1-1/n)))/(1-exp(c2 * d_T_out^(1-1/n))) - 273.15;

fplot(Hea_cur)

xlim([0 40])

%%
T_r_nom = 20;
T_out_nom = -12;
T_out = -12:0.1:20;

f=figure;
n = 1.1;
T_su_nom = 35;
Hea_cur = (T_su_nom - T_r_nom) * ((T_r_nom - T_out)/(T_r_nom - T_out_nom)).^(1/n) + T_r_nom;
plot(T_out, Hea_cur, 'black')
hold on

n = 1.1;
T_su_nom = 40;
Hea_cur = (T_su_nom - T_r_nom) * ((T_r_nom - T_out)/(T_r_nom - T_out_nom)).^(1/n) + T_r_nom;
plot(T_out, Hea_cur, 'black')
hold on

n= 1.1;
T_su_nom = 50;
Hea_cur = (T_su_nom - T_r_nom) * ((T_r_nom - T_out)/(T_r_nom - T_out_nom)).^(1/n) + T_r_nom;
plot(T_out, Hea_cur, 'black')

n = 1.1;
T_su_nom = 45;
Hea_cur = (T_su_nom - T_r_nom) * ((T_r_nom - T_out)/(T_r_nom - T_out_nom)).^(1/n) + T_r_nom;
plot(T_out, Hea_cur, 'black')
hold on

n= 1.2 ;
T_su_nom = 55;
Hea_cur = (T_su_nom - T_r_nom) * ((T_r_nom - T_out)/(T_r_nom - T_out_nom)).^(1/n) + T_r_nom;
plot(T_out, Hea_cur, 'black')

n= 1.1;
T_su_nom = 60;
Hea_cur = (T_su_nom - T_r_nom) * ((T_r_nom - T_out)/(T_r_nom - T_out_nom)).^(1/n) + T_r_nom;
plot(T_out, Hea_cur, 'black')


xlim([-12 20])

grid minor

xlabel('Außentemperatur [°C]','Interpreter','latex')
ylabel('Vorlauftemperatur [°C]','Interpreter','latex')

matlab2tikz(); 
cleanfigure; 
matlab2tikz('Hea_cur.tex', 'dataPath','C:\git\test_repo_2\Grafiken_MA')