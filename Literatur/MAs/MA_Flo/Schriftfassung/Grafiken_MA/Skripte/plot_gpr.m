rng('default') % For reproducibility
x_observed = [0;1;3;4;5;8;9];
y_observed1 = x_observed.*sin(x_observed);
y_observed2 = y_observed1 + 0*randn(size(x_observed));

gprMdl1 = fitrgp(x_observed,y_observed1);
gprMdl2 = fitrgp(x_observed,y_observed2);

x = linspace(0,10)';
[ypred1,~,yint1] = predict(gprMdl1,x,'Alpha',0.05);
[ypred2,~,yint2] = predict(gprMdl2,x,'Alpha',0.05);

hold on
scatter(x_observed,y_observed2,'xr', 'DisplayName', 'Observationen') % Observed data points
plot(x,ypred2,'b', 'DisplayName','GPR Pr\"adiktionen')                   % GPR predictions
patch([x;flipud(x)],[yint2(:,1);flipud(yint2(:,2))],'k','FaceAlpha',0.1, 'DisplayName', '95\% Konfidenzintervall'); % Prediction intervals
hold off


legend('Interpreter','latex', 'Location', 'best','FontSize', 11);
%set(hl, 'Interpreter','latex', 'Location', 'best','FontSize', 11)
set(gca,'TickLabelInterpreter','latex','FontSize', 11, 'XTickLabel', [], 'YTickLabel', [])
fig = get(groot,'CurrentFigure');
fig.Position = [100 100 450 300];

exportgraphics(fig,'gpr_scheme.eps')