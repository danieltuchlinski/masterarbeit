%% Sigmoid
x = -10:0.01:10;
T=[0.1 1 4 100];
Linestiles = ["-" "--" ":" "-."];

f=figure;
for i=1:4
y = 1./(1+exp((-x-0)./T(i)));
plot(x,y,'color','black','LineStyle',Linestiles(i),'DisplayName',['T=' num2str(T(i))])
hold on
end
grid minor
ylabel('f(x)') 
% set(gca, 'XTickLabel', [])
legend

%% Softsign
x = -10:0.01:10;
Linestiles = ["-" "--" ":" "-."];

f=figure;

y = x./(1+abs(x));
plot(x,y,'color','black','LineStyle',Linestiles(1),'DisplayName','softsign')
hold on
grid minor
ylabel('$f(x)$', 'Interpreter','latex') 
set(gca,'TickLabelInterpreter','latex','FontSize', 11, 'XTickLabel', [], 'YTickLabel', [])
legend('Interpreter','latex')
%%
exportgraphics(f,'softsign.eps')