\chapter{Reglerentwicklung}
\label{cha:Reglerentwicklung}
In diesem Kapitel wird die Entwicklung der zu vergleichenden Regler beschrieben. Dazu wird zunächst die Entwicklung des Referenzreglers beschrieben. Anschließend wird die Parametrisierung der MPR beschrieben, indem auf das zugrundeliegende Optimierungsproblem eingegangen wird und die datengetriebenen Prozessmodelle eingeführt werden.

\section{Referenzregler}
\label{sec:Referenzregler}

Neben den beiden MPR Variationen wird in dieser Arbeit ein Referenzregler als Vergleich genutzt. Der Referenzregler wird wie die MPR im Framework integriert. Dieser wird im Folgenden beschrieben. \\
Der Referenzregler nutzt eine Heizkurve, um eine Soll-Vorlauftemperatur zu setzen. Die Komfortzone der MPR in der Nacht lässt kältere Raumtemperaturen als tagsüber zu. Daher wird auch die Sollraumtemperatur, auf deren Basis die Sollvorlauftemperatur mit der Heizkurve berechnet wird, über Nacht heruntergesetzt. Wie in Abbildung \ref{fig:bounds} zu sehen ist, wird die Sollraumtemperatur um 3:00 Uhr hochgesetzt. Das ist notwendig, um die untere Komfortgrenze, die um 8:00 Uhr erhöht wird, zu erreichen. Außerdem wird die Sollraumtemperatur um 16 Uhr abgesenkt, damit die thermische Trägheit des Raumes genutzt werden kann. Die Uhrzeiten, zu der die Sollraumtemperatur hochgesetzt/abgesenkt wird, wurden dabei iterativ ermittelt.\\

\begin{figure}[h]
\centering
\def\svgwidth{300pt}    
\input{All_in_One/Figures/bounds.pdf_tex}  
\caption{Komfortzone für die MPR und Sollraumtemperatur für die Heizkurve, die als Referenzregler genutzt wird.}
\label{fig:bounds}
\end{figure}

Über eine Heizkurve kann nicht alleine die Raumtemperatur geregelt werden. Daher wird beim Referenzregler der Volumenstrom des Heizkreises mit einem PI-Regler geregelt, der Soll- und Istraumtemperatur vergleicht und darüber den Öffnungsgrad eines Ventils einstellt. Die Sollraumtemperatur wird dabei analog zur Sollraumtemperatur der Heizkurve über Nacht abgesenkt. Der beschriebene Regelkreis ist auch für die Versuche mit realer Wärmepumpe in Modelica implementiert.\\
Durch den im vorhinein beschriebenen Referenzregler tritt regelmäßiges Takten der Wärmepumpe auf. Das liegt am Verhalten der beiden Regelkreise, die Vorlauftemperatur und Raumtemperatur regeln: Wenn die Sollraumtemperatur erreicht wird, schließt der Regler das Ventil. Sobald das Ventil vollständig geschlossen ist, findet keine Wärmeabnahme über das Gebäude mehr statt, da kein Wasser mehr durch die Fußbodenheizung strömt. Die Wärmepumpe läuft aber weiter. Dadurch nähern sich Vor- und Rücklauftemperatur der Wärmepumpe an und steigen. Der PI-Regler, der die Vorlauftemperatur regelt, gibt immer geringere Verdichterdehzahlen vor, bis dieser aus geht. Da aber auch bei geschlossenem Ventil Wärme über den Heizkreis abgegeben wird, sinkt die Vorlauftemperatur. Unterschreitet sie die von der Heizkurve vorgegebene Sollvorlauftemperatur, wird der Verdichter wieder eingeschaltet. Bei geschlossenem Ventil steigt dann die Vorlauftemperatur schlagartig an und der Verdichter wird kurz nach Anschalten wieder ausgeschaltet.\\ Dieses Verhalten wird verhindert, indem die Sollraumtemperatur der Heizkurve abgesenkt wird, wenn das Ventil geschlossen ist. Dadurch wird bei geschlossenem Ventil die Wärmepumpe nicht angeschaltet. 

\section{MPR Konfigurationen}
\label{sec:MPR Konfigs}

Im Folgenden werden die MPRs mit Stellgrößen Kompressordrehzahl und Vorlauftemperatur näher beschrieben. Dafür wird auf die jeweiligen Kostenfunktionen und Nebenbedingungen des Optimierungsproblems eingegangen.

Zunächst wird das zu lösende Optimierungsproblem aufgezeigt. Dieses basiert auf der Formulierung in \cite{stoffel_online_2022}.

\pagebreak

\begin{align}
\label{eq:Kost}
    \min_{u} \sum_{k=0}^{N-1} w_{\mathrm{\epsilon},m}\cdot\epsilon_{k}^{2} + w_{\mathrm{u},m}\cdot u_{k} + w_{\mathrm{P},m}\cdot P_{\mathrm{el},k} + w_{\mathrm{\Delta u},m}\cdot\Delta u_{k}^{2} + w_{\Delta u,{\mathrm{log},m}} \cdot \Delta u_{\mathrm{log}}^{2}\\
\intertext {unter den Nebenbedingungen} 
\label{eq:T_KNN}
    \Delta T_{\mathrm{Raum},k} = f_\mathrm{KNN}(...)\\
\label{eq:P_GPR}
    P_{\mathrm{el},k} = f_\mathrm{GPR}(...)\\
\label{eq:Grenzen}
    u_{\mathrm{min},k} \leq u_{k} \leq u_{\mathrm{max},k}\\
    T_{\mathrm{Raum,min},k} - \epsilon_{k} \leq T_{\mathrm{Raum},k} \leq T_{\mathrm{Raum,max},k} + \epsilon_{k} \\
    0 \leq \epsilon_{k}\\
\label{eq:Prä}
    \forall k \in [0,...,N-1]
\end{align}

Da im Rahmen dieser Arbeit vier verschiedene MPR Konfigurationen betrachtet werden, gibt es auch vier verschiedene Formulierungen des Optimierungsproblems. In Gleichung \ref{eq:Kost} bis Gleichung \ref{eq:Prä} ist das Optimierungsproblem allgemein für alle vier Fälle beschrieben. Jedes $m \in [1,4]$ steht dabei für eine andere Reglerkonfiguration. Die entsprechenden Gewichte $w$ für die Reglerkonfigurationen sind in Tabelle \ref{tab:Gewichte} dargestellt. Die Gewichtungen wurden dabei iterativ ermittelt.\\

Tabelle \ref{tab:Kostenfunktion} gibt zusätzlich für die vier Reglerkonfigurationen eine Übersicht über die Prozessmodelle sowie die Terme der Kostenfunktionen. \\

\begin{table}[bp]
\centering
\caption{Stellgröße $u$, Gewichte $w$ und obere und untere Grenzen $u_\mathrm{max}$, $u_\mathrm{min}$ des im vorhinein beschriebenen Optimierungsproblems.}
\begin{tabular}{@{}lllllllll@{}}
\toprule
m & $u$              & $w_\mathrm{\epsilon}$ & $w_\mathrm{u}$             & $w_\mathrm{P}$             & $w_\mathrm{\Delta u}$      & $w_{\Delta u,\mathrm{log}}$ & $u_\mathrm{min}$ & $u_\mathrm{max}$ \\ \midrule
1 & $u_\mathrm{Komp}$ & 100                   & 0.1                        & 0                          & 0.1                        & 10                          & 0                & $\frac{8}{9}$    \\
2 & $u_\mathrm{Komp}$ & 100                   & 0                          & $0.1 \cdot \frac{1}{4000}$ & 0.1                        & 10                          & 0                & $\frac{8}{9}$    \\
3 & $u_\mathrm{T}$    & 10                    & $0.1 \cdot \frac{1}{300} $ & 0                          & $0.005 \cdot \frac{1}{30}$ & 0                           & 287.15           & 323.15           \\
4 & $u_\mathrm{T}$    & 10                    & 0                          & $0.1 \cdot \frac{1}{4000}$ & $0.005 \cdot \frac{1}{30}$ & 0                           & 287.15           & 323.15           \\ \bottomrule
\end{tabular}
\label{tab:Gewichte}
\end{table}

Im Folgenden werden die Terme des Optimierungsproblems kurz erklärt. Der Wert $\epsilon$ steht für die Verletzung der Temperaturgrenzen. Dieser wird quadriert, um auch negative Verletzungen der Temperaturgrenzen mit einzubeziehen. Da das übergeordnete Ziel der MPRs ist, hohen thermischen Komfort bereitzustellen, ist die Gewichtung dieser Terme vergleichweise hoch. \\
$u_{k}$ bestraft die Stellgrößen $u_{\mathrm{Komp},k}$ und $u_{\mathrm{T},k}$. Die sollen möglichst gering sein, um den Energieverbrauch gering zu halten. $u_{\mathrm{T},k}$ wird mit $\frac{1}{300}$ skaliert, da die absoluten Werte von $u_{\mathrm{T},k}$ höher sind. $u_{\mathrm{T},k}$ wird genau wie $u_{\mathrm{Komp},k}$ über Gleichung \ref{eq:Grenzen} begrenzt. Für die MPR mit Verdichterdrehzahl als Stellgröße hat $u_{\mathrm{Komp},k}$ dabei die Grenzen 0 und $\frac{8}{9}$. Das entspricht einer maximalen Verdichterdrehzahl von 80\,Hz. Die maximal mögliche Drehzahl der Wärmepumpe wären 90\,Hz, diese können allerdings aufgrund des internen Überhitzungsreglers nicht ausgenutzt werden. Die untere und obere Grenze von $u_{\mathrm{T},k}$ liegen bei 287.15\,K und 323.15\,K. Für $m=2$ und $m=4$ ist $w_\mathrm{u}=0$, da hier ein Prozessmodell zur Vorhersage von $P_\mathrm{el}$ genutzt wird. Daher wird anstelle von $u_{k}$, $P_{k}$ bestraft.\\

\begin{table}[h]
\centering
\caption{Konfiguration der Kostenfunktion für die zu untersuchenden Fälle. $u_\mathrm{Komp}$ bezeichnet die Stellgröße Kompressordrehzahl und $u_\mathrm{T}$ die Stellgröße Vorlauftemperatur. $T_\mathrm{KNN}$ ist ein KNN welches die Änderung der Raumtemperatur prädiziert, $P_\mathrm{GPR}$ ein GPR das die elektrische Leistung der Wärmepumpe prädiziert. $\epsilon$ ist der Term, der thermischen Diskomfort bestraft, $u$ bestraft die Stellgröße, $P$ bestraft die elektrische Leistung der Wärmepumpe, $\Delta u$ und $\Delta u_\mathrm{log}$ bestrafen Änderungen der Stellgröße. Mit x markierte Zellen sind in der Kostenfunktion der jeweiligen Reglerkonfiguration beinhaltet.}
\begin{tabular}{@{}llcccccc@{}}
\toprule
Stellgröße               & Prozessmodelle   & \multicolumn{1}{l}{$m$}  & \multicolumn{1}{l}{$\epsilon$} & \multicolumn{1}{l}{$u$} & \multicolumn{1}{l}{$P$} & \multicolumn{1}{l}{$\Delta u$} & \multicolumn{1}{l}{$\Delta u_\mathrm{log}$} \\ \midrule
\multirow{2}{*}{$u_\mathrm{Komp}$} & $T_\mathrm{KNN}$    & 1      & x                               & x                                   &                                   & x                                   & x                                              \\
                         & $T_\mathrm{KNN}$, $P_\mathrm{GPR}$ & 2 & x                               &                                     & x                                 & x                                   & x                                             
\\ \midrule
\multirow{2}{*}{$u_\mathrm{T}$}   & $T_\mathrm{KNN}$    & 3      & x                               & x                                   &                                   & x                                   &                                                \\
                         & $T_\mathrm{KNN}$, $P_\mathrm{GPR}$ & 4 & x                               &                                     & x                                 & x                                   &                                                \\ \bottomrule

\end{tabular}
\label{tab:Kostenfunktion}
\end{table}

Der Term $P_{k}$ wird dabei mit 0.1 gewichtet und mit $\frac{1}{4000}$ skaliert, da die absoluten Werte von $P_\mathrm{el}$ höher als die von $u$ sind. \\
$\Delta u_k$ bestraft Änderungen der Stellgröße. Das verhindert ein Schwingen des Systems. Der Term wird quadriert, damit Änderungen sowohl in negative als auch in positive Richtung bestraft werden. \\
Für $m=1$ und $m=2$ wird zudem $\Delta f_\mathrm{log}(u_{\mathrm{Komp},k})$ bestraft, mit 

\begin{align}
    f_\mathrm{log}(x)=\frac{1}{1+ e^{\frac{0.33-x}{0.1}}}
    \label{eq:log}
\end{align}

$f_\mathrm{log}(x)$ hat bei 0.33, also beim Einschaltschwellwert des Verdichters, die größte Steigung. Dadurch wird um die Einschaltschwelle eine Änderung von $u_{\mathrm{Komp},k}$ stärker bestraft, als bei weiter von der Einschaltschwelle entfernten Werten. Dies reduziert die Einschaltvorgänge der Wärmepumpe.

Ein weiterer wichtiger Parameter der MPR ist der Prädiktionshorizont $N$. Dieser ist in dieser Arbeit 36. Das entspricht bei der hier gewählten Schrittweite von 10 Minuten einem Prädiktionshorizont von 6 Stunden. Dieser Wert wurde iterativ unter Einbeziehung der thermischen Trägheit des Systems ermittelt.

Zur Vorhersage der Zustandsgrößen $\Delta T_{\mathrm{Raum},k}$ und $P_{\mathrm{el},k}$ werden datengetriebene Prozessmodelle genutzt. Diese Zusammenhänge sind über die Gleichungen \ref{eq:T_KNN} und \ref{eq:P_GPR} dargestellt. Die Prozessmodelle werden im nächsten Unterkapitel näher beschrieben. 

\section{Datengetriebene Prozessmodelle mit Modelldaten}
\label{sec:Datengetriebene Prozessmodelle}

Die in dieser Arbeit genutzten datengetriebenen Prozessmodelle werden in Tabelle \ref{tab:Features} näher beschrieben. Die Verzögerung beschreibt dabei die Anzahl der vergangenen Zeitschritte einer Eingangsvariablen, die in das Prozessmodell eingehen. Je größer die Verzögerung ist, desto weiter sieht das Prozessmodell in die Vergangenheit.\\


\begin{table}[h]
\centering
\caption{Features der datengetriebenen Prozessmodelle. $T_\mathrm{U}$ ist die Umgebungstemperatur, $\dot{q}_\mathrm{dir}$ ist die direkte Sonneneinstrahlung, $t_\mathrm{Tag}$ ist die Tageszeit und $t_\mathrm{Woche}$ ist die Wochenzeit.}
\begin{tabular}{|c|c|c|c|c|}
\hline
                                        & \begin{tabular}[c]{@{}c@{}}$T_\mathrm{KNN}$\\ (für $u_\mathrm{Komp}$)\end{tabular} & \begin{tabular}[c]{@{}c@{}}$T_\mathrm{KNN}$\\ (für $u_\mathrm{T}$)\end{tabular} & \begin{tabular}[c]{@{}c@{}}$P_\mathrm{GPR}$\\ (für $u_\mathrm{Komp}$)\end{tabular} & \begin{tabular}[c]{@{}c@{}}$P_\mathrm{GPR}$\\ (für $u_\mathrm{T}$)\end{tabular} \\ \hline
\multicolumn{1}{|c|}{Eingang}           & \multicolumn{1}{c|}{Verzögerung}                                                 & \multicolumn{1}{c|}{Verzögerung}                                              & \multicolumn{1}{c|}{Verzögerung}                                        & \multicolumn{1}{c|}{Verzögerung}                                     \\ \hline
\multicolumn{1}{|c|}{$T_\mathrm{U}$}           & \multicolumn{1}{c|}{2}                                                           & \multicolumn{1}{c|}{2}                                                        & \multicolumn{1}{c|}{1}                                                  & \multicolumn{1}{c|}{1}                                               \\ \hline
\multicolumn{1}{|c|}{$u_\mathrm{Komp}$}        & \multicolumn{1}{c|}{6}                                                           & \multicolumn{1}{c|}{-}                                                        & \multicolumn{1}{c|}{1}                                                  & \multicolumn{1}{c|}{-}                                               \\ \hline
\multicolumn{1}{|c|}{$u_\mathrm{T}$}           & \multicolumn{1}{c|}{-}                                                           & \multicolumn{1}{c|}{6}                                                        & \multicolumn{1}{c|}{-}                                                  & \multicolumn{1}{c|}{1}                                               \\ \hline
\multicolumn{1}{|c|}{$\dot{q}_\mathrm{dir}$}               & \multicolumn{1}{c|}{2}                                                           & \multicolumn{1}{c|}{2}                                                        & \multicolumn{1}{c|}{-}                                                  & \multicolumn{1}{c|}{-}                                               \\ \hline
\multicolumn{1}{|c|}{$t_\mathrm{Tag}$}         & \multicolumn{1}{c|}{2}                                                           & \multicolumn{1}{c|}{2}                                                        & \multicolumn{1}{c|}{-}                                                  & \multicolumn{1}{c|}{-}                                               \\ \hline
\multicolumn{1}{|c|}{$t_\mathrm{Woche}$}       & \multicolumn{1}{c|}{2}                                                           & \multicolumn{1}{c|}{2}                                                        & \multicolumn{1}{c|}{-}                                                  & \multicolumn{1}{c|}{-}                                               \\ \hline
\multicolumn{1}{|c|}{$T_\mathrm{Raum}$}        & \multicolumn{1}{c|}{6}                                                           & \multicolumn{1}{c|}{6}                                                        & \multicolumn{1}{c|}{-}                                                  & \multicolumn{1}{c|}{-}                                               \\ \hline
\multicolumn{1}{|c|}{$\Delta T_\mathrm{Raum}$} & \multicolumn{1}{c|}{-}                                                           & \multicolumn{1}{c|}{-}                                                        & \multicolumn{1}{c|}{1}                                                  & \multicolumn{1}{c|}{1}                                               \\ \hline
\multicolumn{1}{|c|}{$u_\mathrm{Komp,log}$} &  \multicolumn{1}{c|}{-}                                                         &  \multicolumn{1}{c|}{-}                                                        &  \multicolumn{1}{c|}{1}                                                           &  \multicolumn{1}{c|}{-}                                                                     \\ \hline
\end{tabular}
\label{tab:Features}
\end{table}

Für die KNNs, die $\Delta T_\mathrm{Raum}$ lernen, haben die Stellgröße und die Raumtemperatur mit einer Verzögerung von einer Stunde die größte Verzögerung. Dadurch wird die thermische Trägheit des Raumes mit einbezogen. Über $T_\mathrm{U}$ und $\dot{q}_\mathrm{dir}$ wird Wärmeübertragung zwischen Umgebung und Raum betrachtet. $t_\mathrm{Tag}$ und $t_\mathrm{Woche}$ werden als Eingänge gewählt, um die Wärmeübertragung durch interne Gewinne mit einzubeziehen. Die Verzögerungen der einzelnen Größen wurden über manuelles Tuning ermittelt.\\
Für die Prozessmodelle, die $P_\mathrm{el}$ lernen, werden Eingangsgrößen gewählt, die Einfluss auf den Betrieb der Wärmepumpe haben. Dabei wird für beide Reglervariationen die Stellgröße als Eingang gewählt, welche den Haupteinfluss auf $P_\mathrm{el}$ haben. Für die MPR mit $u_\mathrm{Komp}$ als Stellgröße wird außerdem $u_\mathrm{Komp,log}$ als Eingang gewählt. Dadurch kann das Prozessmodell die Unstetigkeit aufgrund der Einschaltschwelle besser lernen. Weiterhin haben $T_\mathrm{U}$ und $\Delta T_\mathrm{Raum}$ Einfluss auf $P_\mathrm{el}$. 

\subsection{Trainingsdaten}
\label{subsec:Trainingsdaten}

Die Trainingsdaten für die datengetriebenen Modelle werden mit einem PI-Regler generiert. Dieser regelt die jeweilige Stellgröße, also $u_\mathrm{Komp}$ oder $u_\mathrm{T}$ auf $T_\mathrm{Raum}$. Dabei wird der Sollwert von $T_\mathrm{Raum}$ rechtecksförmig verändert. Abbildung \ref{fig:Trainingsdaten} zeigt beispielhaft einen Ausschnitt der Trainingsdaten von fünf Tagen.

\begin{figure}[]
\centering
\def\svgwidth{450pt}    
\input{All_in_One/Figures/Trainingsdaten.pdf_tex}  
\caption{Ausschnitt aus den Trainingsdaten im Frühling. Die Sollraumtemperatur (oberer Plot, schwarz), wird rechtecksförmig verändert. Der PI-Regler stellt hier $u_\mathrm{Komp}$, um die Sollraumtemperatur zu erreichen.}
\label{fig:Trainingsdaten}
\end{figure}

Damit die Prozessmodelle an verschiedene Wetterbedingungen angepasst sind, werden die Trainingsdaten entsprechend ausgewählt. Es haben sich jeweils zwei Wochen im Winter, Frühling und Herbst als ausreichende Trainingszeiträume herausgestellt.  

\subsection{Hyperparameter der datengetriebenen Prozessmodelle}
\label{subsec:Architektur Datengetriebene Modelle}

Im Folgenden werden die Hyperparameter der datengetriebenen Modelle beschrieben. Dabei wird zunächst auf die KNNs eingegangen, die $\Delta T_\mathrm{Raum}$ lernen und anschließend auf die GPRs, die $P_\mathrm{el}$ lernen. Die KNNs und GPRs werden über den Root Mean Square Error (RMSE) und den Normalized Root Mean Square Error (NRMSE) bewertet. Die Definitionen dieser Größen sind im Anhang in den Gleichungen \ref{eq:RMSE} und \ref{eq:NRMSE} gegeben.

\textbf{KNNs}

Der Zusammenhang zwischen Ein- und Ausgangsdaten eines KNNs wird neben den Trainingsdaten auch über die Hyperparamter definiert. Die in dieser Arbeit angepassten Hyperparameter sind dabei die Anzahl der verdeckten Schichten, die Anzahl der Neuronen pro Schicht und die Aktivierungsfunktion der Neuronen der verdeckten Schichten. \\
Angelehnt an eine vorangegangene Arbeit \cite{berktold_modellpradiktive_nodate} wird ein KNN mit einer verdeckten Schicht und 16 Neuronen ausgewählt.\\
Zur Auswahl der Aktivierungsfunktion wird ein Brute-Force Hyperparameter Tuning durchgeführt. Da der Trainingsvorgang eines KNNs probabilistisch ist, werden fünf Trainingsvorgänge für jede zu vergleichende Aktivierungsfunktion durchgeführt. Als Trainingsdaten werden dabei die in \ref{subsec:Trainingsdaten} beschriebenen, am Modell generierten Daten genutzt. Die Testdaten, mit denen die KNNs bewertet werden, sind dabei Daten aus Winter, Frühling und Herbst über einen Zeitraum von je drei Tagen. Diese Zeiträume sind außerhalb der Zeiträume der Trainingsdaten.\\
Abbildung \ref{fig:Aktivierungsfunktionen T_VL} zeigt die Auswertung von fünf verschiedenen KNNs pro Aktivierungsfunktion. Dabei wird der RMSE verglichen. Es zeigt sich, dass für beide Stellgrößen $u_{Komp}$ und $T_{VL}$ die softsign Aktivierungsfunktion mit 0,0287\,K bzw. 0,0261\,K den besten durchschnittlichen RMSE vorweist. Aus diesem Grund wird jeweils ein KNN mit softsign Aktivierungsfunktion gewählt. Abbildung \ref{fig:Softsign} im Anhang zeigt die softsign Aktivierungsfunktion. 

\begin{figure}[]
\centering
\def\svgwidth{400pt}    
\input{All_in_One/Figures/Aktivierungsfunktionen_u_comp.pdf_tex}  
\def\svgwidth{400pt}    
\input{All_in_One/Figures/Aktivierungsfunktionen_T_VL.pdf_tex}  
\caption{RMSE von je 5 KNNs (KNN $\Delta T_\mathrm{Raum}$) für die Aktivierungsfunktionen relu, sofstign, softplus, sigmoid und tanh. Oben: Stellgröße  $u_\mathrm{Komp}$, unten: Stellgröße $T_\mathrm{VL}$.}
\label{fig:Aktivierungsfunktionen T_VL}
\end{figure}

\textbf{GPRs}

Bei GPRs kann vor allem über die Kernelfunktion Einfluss auf den Zusammenhang zwischen Ein- und Ausgangsdaten genommen werden. Für diese Arbeit wird das in Abschnitt \ref{subsec:Gaußprozesse} beschriebene RBF-Kernel genutzt. Das wird gemacht, da für sich ähnelnden Eingangswerten davon auszugehen ist, dass auch die Ausgangswerte, also $P_\mathrm{el}$ ähnlich sind. Die Ausnahme bildet der Sprung von $P_\mathrm{el}$ bei der Einschaltschwelle des Verdichters. Abbildung \ref{fig:Einschaltschwelle} zeigt $P_\mathrm{el}$ über $u_\mathrm{Komp}$. Es ist zu erkennen, dass bei $u_\mathrm{Komp}=\frac{1}{3}$, $P_\mathrm{el}$ einen Sprung aufweist. Diese Unstetigkeit liegt an der minimalen Verdichterfrequenz von 30\,Hz. Daraus resultiert eine minimale Leistung der Wärmepumpe bei eingeschaltetem Verdichter von etwas über 1000\,W. 

Ein wichtiger Hyperparameter, der beim RBF-Kernel angepasst werden kann, ist die Längen-Skala ($l$) der Kernelfunktion. Diese definiert, wie weit Punkte voneinander entfernt sind, um keine Kovarianz zueinander mehr aufzuweisen. Je kleiner $l$ ist, desto schneller geht der Ausgangswert des GPRs gegen den Mittelwert, wenn man sich von einem Trainingsdatenpunkt entfernt. Ein kleines $l$ sorgt also für \glqq wellige\grqq{} Funktionen. Wenn sich der Testdatenpunkt nur wenig außerhalb der Trainingsdatenpunkte befindet, erhält man den Mittelwert des GPRs, der meistens null ist. Dieses Verhalten ist hier nicht erwünscht. \\
Die Hyperparameter werden während des Trainings des GPRs automatisch optimiert. Es hat sich allerdings herausgestellt, dass immer gegen ein sehr kleines $l$ konvergiert wird, sodass dass im Vorhinein erklärte Verhalten auftritt. Daher wird die untere Grenze für $l$ auf 40 gesetzt. Dieser Wert wurde iterativ ermittelt.




\begin{figure}[]
\centering
\def\svgwidth{300pt}    
\input{All_in_One/Figures/Einschaltschwelle.pdf_tex}  
\caption{Darstellung der Einschaltschwelle des Verdichters anhand von $u_\mathrm{Komp}$ und $P_\mathrm{el}$. Dazu werden hier Daten der realen Wärmepumpe genutzt.}
\label{fig:Einschaltschwelle}
\end{figure}

Diese Unstetigkeit widerspricht den A-Priori über das RBF-Kernel eingebrachten Informationen über die Daten. Um trotzdem ein genaues GPR zu erhalten, welches die Einschaltschwelle darstellen kann, wird als Eingangswert $u_\mathrm{Komp,log}$ genutzt. Durch die Eigenschaften der logistischen Funktion nähert sich $u_\mathrm{Komp,log}$ für $u_\mathrm{Komp}$ unter der Einschaltschwelle schnell null, umgekehrt nähert sich $u_\mathrm{Komp,log}$ für $u_\mathrm{Komp}$ über der Einschaltschwelle schnell eins. Je steiler dabei die Steigung der logistischen Funktion gewählt wird, desto schneller nähert sich $u_\mathrm{Komp,log}$ eins bzw. null, desto genauer kann der GPR die Werte von $P_\mathrm{el}$ um die Einschaltschwelle lernen. Es hat sich allerdings in der Anwendung herausgestellt, dass zu große Steigungen bei der Optimierung das Finden der optimalen Stellgröße erschweren. Aus diesem Grund wird die logistische Funktion wie in Gleichung \ref{eq:log} gezeigt mit einer Steigung von 10 gewählt.


\subsection{Evaluierung der datengetriebenen Prozessmodelle}
\label{subsec:Evaluierung Datengetriebene Modelle}

Im Folgenden werden die datengetriebenen Prozessmodelle anhand ihrer Genauigkeit bewertet. Dabei werden hier datengetriebene Modelle aus simulativ generierten Trainingsdaten betrachtet.

Zur Bewertung der datengetriebenen Prozessmodelle werden die Testdaten genutzt, die in \ref{subsec:Architektur Datengetriebene Modelle} zur Auswahl der Aktivierungsfunktion genutzt werden. 

Zunächst werden die KNNs mit Ausgangsgröße $\Delta T_\mathrm{Raum}$ betrachtet. Abbildung \ref{fig:dTANN} zeigt die Genauigkeit der KNNs für die beiden Stellgrößen. Es werden RMSEs von 0,02723\,K und 0,02567\,K erreicht. Das entspricht NRMSEs von 4,08\,\% und 5,5\,\%. Der NRMSE ist bei $u_\mathrm{Komp}$ als Stellgröße besser, obwohl der RMSE höher ist. Das liegt an der höheren Spreizung von $\Delta T_\mathrm{Raum}$ in den Testdaten. Diese höhere Genauigkeit kann  damit erklärt werden, dass der Zusammenhang zwischen $u_\mathrm{Komp}$ und $T_\mathrm{Raum}$  direkter ist als der zwischen $T_\mathrm{VL}$ und $T_\mathrm{Raum}$. So liegt ein weiterer Regelkreis bei der MPR mit Stellgröße $T_\mathrm{VL}$ vor. Dadurch können bei gleicher $T_\mathrm{VL}$ unterschiedliche $\Delta T_\mathrm{Raum}$ erreicht werden. \\

\begin{figure}[!h]
\centering
\def\svgwidth{210pt}    
\input{All_in_One/Figures/Prae_vs_real_DT_u_k.pdf_tex}  
\def\svgwidth{210pt}
\input{All_in_One/Figures/Prae_vs_real_DT_T_VL.pdf_tex}  
\caption{Darstellung der Genauigkeit der KNNs mit Ausgang $\Delta T_\mathrm{Raum}$. Links ist das KNN für $u_\mathrm{Komp}$, rechts das KNN für $u_\mathrm{T}$ dargestellt. Die Datenpunkte der Testdaten sind aufsteigend sortiert. In rot sind die realen Werte von $\Delta T_\mathrm{Raum}$ dargestellt, in blau die vom datengetriebenen Modell generierten Datenpunkte.}
\label{fig:dTANN}
\end{figure}

Um $P_{el}$ zu lernen, werden GPRs genutzt. Abbildung \ref{fig:PelGPR} zeigt die Genauigkeit der GPRs für die beiden Stellgrößen.


\begin{figure}[!h]
\centering
\def\svgwidth{210pt}    
\input{All_in_One/Figures/Prae_vs_real_GPR_P_u_k_hypers_l.pdf_tex}  
\def\svgwidth{210pt}
\input{All_in_One/Figures/Prae_vs_real_GPR_P_T_VL_hypers_l.pdf_tex}  
\caption{Darstellung der Genauigkeit der GPRs mit Ausgang $P_\mathrm{el}$. Links ist der GPR für $u_\mathrm{Komp}$, rechts der GPR für $u_\mathrm{T}$ dargestellt. In rot sind die realen Werte von $P_\mathrm{el}$ dargestellt, in blau die vom datengetriebenen Prozessmodell generierten Datenpunkte.}
\label{fig:PelGPR}
\end{figure}

 Dabei wird für die Stellgröße $u_\mathrm{Komp}$ ein RMSE von 375,81\,W erreicht. Das entspricht einem NRMSE von 6,53\,\%. Der Sprung von $P_\mathrm{el}$ zwischen 0 und 1000\,W entsteht aufgrund der Einschaltschwelle des Verdichters. Es ist zu erkennen, dass in diesem Bereich $P_\mathrm{el}$ am ungenauesten vom Prozessmodell dargestellt wird. Für $P_\mathrm{el}$ ab etwa 2000\,W wird eine hohe Genauigkeit erreicht.\\
Für die Stellgröße $T_\mathrm{VL,soll}$ wird ein RMSE von 525,78\,W erreicht, also ein NRMSE von 9,66\,\% Auch anhand der Abbildung ist zu erkennen, dass der Zusammenhang zwischen Eingangsgrößen und $P_\mathrm{el}$ schlechter als bei $u_\mathrm{Komp}$ als Stellgröße gelernt werden kann. Das liegt an dem indirekten Zusammenhang zwischen $T_\mathrm{VL,soll}$ und $P_\mathrm{el}$. So kann der Verdichter für gleiche $T_\mathrm{VL,soll}$ sowohl an als auch aus sein, und damit auch unterschiedliche $P_\mathrm{el}$ vorweisen. Zudem wird das Lernen durch die minimale Lauf- und Standzeit der Wärmepumpe erschwert. Aus diesen Gründen wird bei ausgeschalteter Wärmepumpe $P_\mathrm{el}$ zum Teil überschätzt und bei geringen $P_\mathrm{el}$ zum Teil unterschätzt.\\

Beide GPRs werden als ausreichend genau bewertet, um als Prozessmodelle genutzt werden zu können.


\section{Einfluss der Gewichte der Kostenfunktion auf die KPIs}
\label{sec:Einfluss der Gewichte auf die KPIs}

Im Folgenden wird der Einfluss der Gewichte der Kostenfunktion auf die KPIs untersucht. Damit wird die Sensivität der KPIs bei Änderung der Gewichte gezeigt. Dafür wird beispielhaft der Typtag 11.01. mit der Reglervariation $u_\mathrm{Komp}$ betrachtet. Für diesen Tag wird jeweils ein Gewicht der Kostenfunktion schrittweise erhöht, während alle anderen Gewichte auf ihren Standardwerten festgehalten werden. Bei Veränderung eines Gewichtes ist der Standardwert des jeweiligen Gewichtes der Mittelwert. 

Anhand von Abbildung \ref{fig:wT_wu_E_Dk} ist zu erkennen, dass bei der verbrauchten Energie bei Gewichten über 50 bzw. 0,05 kein klarer Zusammenhang zwischen dieser und den Gewichten $w_\mathrm{K}$ und $w_\mathrm{u}$ vorhanden ist. Bei Gewichten kleiner als 50 bzw. 0,05 nimmt die verbrauchte Energie ab, wenn sich $w_\mathrm{K}$ verringert. Dagegen nimmt die verbrauchte Energie zu wenn sich $w_\mathrm{u}$ verringert.\\
Bei Betrachtung von Abbildung \ref{fig:wT_wu_E_Dk} rechts ist zu erkennen, dass sich der Verlauf des Diskomforts gegenläufig zu dem der verbrauchten Energie verhält. Hier ist der Zusammenhang zwischen den Gewichten und dem Diskomfort über die gesamte Reichweite der Gewichte zu erkennen. Für hohe $w_\mathrm{K}$ wird ein geringen Diskomfort erzielt, wird $w_\mathrm{K}$ verringert, erhöht sich der Diskomfort. Für $w_\mathrm{u}$ ist der entgegengesetzte Zusammenhang zu erkennen: Bei Verringerung von $w_\mathrm{u}$ verringert sich auch der Diskomfort.\\

\begin{figure}[h]
\centering
\def\svgwidth{210pt}    
\input{All_in_One/Figures/w_T_w_u_E.pdf_tex}  
\def\svgwidth{210pt}
\input{All_in_One/Figures/w_T_w_u_Dk.pdf_tex}  
\caption{Energie und Diskomfort für den Typtag 11.01. bei Änderung der Gewichte für den Komfort ($w_\mathrm{K}$, schwarz) und für die Stellgröße ($w_\mathrm{u}$, rot).}
\label{fig:wT_wu_E_Dk}
\end{figure}

Diese Beobachtungen zeigen den Zielkonflikt zwischen Energie und Diskomfort auf. Bei Verringerung des Diskomforts durch Anpassung der Gewichte erhöht sich der Energieverbrauch. Das gilt auch andersherum, wenn die Energie verringert wird, erhöht sich der Diskomfort.\\
Zum Anderen zeigen die Beobachtungen, dass selbst bei größeren Veränderungen der Gewichte der Kostenfunktion, trotzdem ähnliche Ergebnisse bei verbrauchter Energie und Diskomfort erreicht werden können. Dies unterstreicht die Robustheit der Regelung gegenüber Änderung der Gewichte. 

Im Anschluss wird der Einfluss von $w_\mathrm{\Delta u}$ und $w_\mathrm{\Delta u,log}$ auf die Startvorgänge untersucht. Abbildung \ref{fig:wdu_st} zeigt, dass eine Änderung von $w_\mathrm{\Delta u}$ keinen starken Einfluss auf die Startvorgänge hat. Dagegen hat eine Änderung von $w_\mathrm{\Delta u,log}$ einen großen Einfluss auf die Anzahl der Startvorgänge. Für höhere $w_\mathrm{\Delta u,log}$ werden die geringsten Startvorgänge erreicht. Für $w_\mathrm{\Delta u,log}=0$ werden die meisten Startvorgänge erreicht.

\begin{figure}[h]
\centering
\def\svgwidth{210pt}    
\input{All_in_One/Figures/w_u_w_du_St.pdf_tex}  
\caption{Starts für den Typtag 11.01. bei Änderung der Gewichte für die Änderung der Stellgröße ($w_\mathrm{\Delta u}$, schwarz) und für die logistische Änderung der Stellgröße ($w_\mathrm{\Delta u,log}$, rot).}
\label{fig:wdu_st}
\end{figure}

Im Anhang in den Abbildungen  \ref{fig:wTwU_SCOP_St} bis \ref{fig:w_du_SCOP} sind weitere Zusammenhänge zwischen Gewichten der Kostenfunktion und den KPIs dargestellt. 