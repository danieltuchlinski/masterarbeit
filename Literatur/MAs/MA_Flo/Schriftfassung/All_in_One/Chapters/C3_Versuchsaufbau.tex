\chapter{Methodik}
\label{cha:Methodik}
In diesem Kapitel werden die Simulationsumgebung und der Versuchsaufbau dieser Arbeit beschrieben. Zunächst wird aber das allgemeine Vorgehen der Reglerentwicklung und Bewertung in dieser Arbeit dargestellt.

Es wird zuerst der Anwendungsfall dieser Arbeit definiert. Dieses ist ein Gebäudeenergiesystem mit Fußbodenheizung als Transfersystem und Luft-Wasser-Wärmepumpe als Wärmeerzeuger. Es wird keine Trinkwarmwassernutzung betrachtet und das System hat keinen Warmwasserspeicher. Dazu wird ein entsprechendes Simulationsmodell benötigt. Zum Aufbau des Simulationsmodells gehören unter anderem die Auswahl eines Gebäudemodells und eines Verteilsystems. Näheres dazu wird in Abschnitt \ref{sec:Simulationsmodell} beschrieben. \\
Nach Definierung des Systems werden der Referenzregler und die MPRs konfiguriert. \\
Für die MPRs werden die Prozessmodelle parametrisiert und trainiert. Außerdem wird die Kostenfunktion parametrisiert. Näheres dazu wird in Kapitel \ref{cha:Reglerentwicklung} beschrieben.

Zusammengefasst werden zunächst rein simulative Versuche gemacht, um den Anwendungsfall auszuwählen, die Regler zu parametrisieren und Daten zum Anlernen der datengetriebenen Prozessmodelle zu generieren. Dann werden die Versuche an einem realen Prüfstand durchgeführt, indem die modellierte Wärmepumpe durch eine reale ausgetauscht wird. Näheres dazu wird in Abschnitt \ref{sec:Versuchsaufbau} beschrieben. In den Versuchen mit realer Wärmepumpe können Effekte betrachtet werden, die im Modell der Wärmepumpe nicht einbezogen sind. 

\section{Simulationsmodell}
\label{sec:Simulationsmodell}
Für das Simulationsmodell wird die Modelica Bibliothek \textit{Building Energy System Modules} (BESMod) genutzt. Diese nutzt existierende Modelle aus anderen Bibliotheken, der \textit{IBPSA} \cite{doecode_25252}, \textit{AixLib} \cite{muller_aixlib_2016}, \textit{BuildingSystems} \cite{nytsch-geusen_modelica_2013}, \textit{IDEAS} \cite{Jorissen-IDEAS-2018} und \textit{Buildings} \cite{wetter_modelica_2014}. In der BESMod können diese existierenden Modelle modular zu einem Gesamtsystem zusammengestellt werden. \cite{fabian_wullhorst_modular_2021}

\subsection{Aufbau}
\label{subsec:Aufbau}
Abbildung \ref{fig:BesMod} zeigt die oberste Ebene des genutzten BESMod Modells. Dieses Gesamtmodell ist dabei aufgeteilt in die Systeme \glqq ventilation\grqq{}, \glqq electrical\grqq{}, \glqq control\grqq{}, \glqq building\grqq{}, \glqq hydraulic\grqq{} und \glqq DHW\grqq{}. Für diese Arbeit sind nur die Systeme \glqq building\grqq{} und \glqq hydraulic\grqq{} relevant. Es wird keine Trinkwarmwassernutzung betrachtet und keine aktive Ventilation.\\

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.6]{All_in_One/Figures/Bes_MOD.pdf}
\caption{Abbildung der obersten Ebene des genutzten BESMod Modells aus Dymola. Die einzelnen Teilsysteme sind über Busverbindungen verknüpft und wiederum in Teilsysteme unterteilt.}
\label{fig:BesMod}
\end{center}
\end{figure}

\subsection{Komponenten}
\label{subsec:Komponenten}
Im Folgenden wird das \glqq hydraulic\grqq{} System anhand seiner Komponenten beschrieben. Dieses ist in Abbildung \ref{fig:BesMod_Hyd} dargesteltt. Es beinhaltet den Wärmeerzeuger (generation), die Regelung (control) und das Verteilsystem (transfer und distribution). 

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.7]{All_in_One/Figures/BesMOD_Hydraulic.pdf}
\caption{Abbildung des \glqq hydraulic\grqq{} Teilsystems der BESMod aus Dymola. Dieses ist wiederum in einzelne Teilsysteme unterteilt die über Busverbindungen verknüpft sind.}
\label{fig:BesMod_Hyd}
\end{center}
\end{figure}

\textbf{Wärmeerzeuger}
\\
Als Wärmeerzeuger wird in dieser Arbeit eine Luft-Wasser-Wärmepumpe genutzt. Das genutzte Wärmepumpenmodell ist in \cite{fabian_wullhorst_modular_2021} beschrieben. Es basiert auf einem Black-Box Ansatz, indem die Kondensatorleistung und die elektrische Leistung über ein Kennfeld bestimmt werden. Dieses gibt die beiden genannten Größen in Abhängigkeit von Umgebungstemperatur, Vorlauftemperatur und Verdichterdrehzahl wieder.\\
Dieses Kennfeld wurde dabei im Rahmen der Arbeit von \citeauthor{waiz_simulative_2023} ermittelt und für die reale Wärmepumpe, die in dieser Arbeit genutzt wird, kalibriert. Zur Kalibrierung wurde dabei das Tool AixCaliBuHA \cite{Wuellhorst2022} genutzt. 

\textbf{Verteilsystem}
\\
Die Wärmepumpe erhitzt im Betrieb das Wasser des Heizkreises. Dieses wird über eine Pumpe bewegt, die durchgehend einen konstanten Massenstrom erzeugt.\\
Die von der Wärmepumpe in den Heizkreis übergebene Wärme wird wiederum an das Gebäude übergeben. Das passiert über eine Fußbodenheizung. Das Modell dafür hat ein Rohr durch welches das Wasser strömt. Dieses wird diskretisiert, die einzelnen Elemente werden in Serie verbunden. Die Rücklauftemperatur des vorherigen Elements ist die Vorlauftemperatur des nächsten Elements \cite{openmodelica_panelheating_nodate}.
Über Konvektion, Wärmeleitung und Strahlung wird Wärme zwischen den einzelnen Elementen der Fußbodenheizung und dem Raum übertragen. Außerdem wird über Wärmeleitung Wärme zwischen Fußbodenheizung und Erdreich übertragen \cite{openmodelica_panelheatingsegment_nodate}. Für das Erdreich wird eine konstante Temperatur angenommen. Außerdem haben die einzelnen Elemente jeweils Wärmekapazitäten zwischen Rohr und Raum und Rohr und Erdreich. 

\textbf{Gebäudemodell}
\\
Das verwendete Gebäudemodell ist ein über TEASER \cite{remmen_teaser_2018} (Tool for Energy Analysis and Simulation for Efficient Retrofit) generiertes Wohngebäude. \\
Tabelle \ref{tab:Gebäudemodell} zeigt die Eckdaten des Gebäudemodells. 

\begin{table}[h]
\caption{Eckdaten des verwendeten Gebäudemodells.}
\begin{tabular}{clclc}
\hline
Konstruktionsjahr & \multicolumn{1}{c}{Grundfläche} & Stockwerkhöhe             & Stockwerkanzahl       & nom. Heizbedarf \\ \hline
1997              & 130\,m$^2$                     & 2.5\,cm$^2$ & \multicolumn{1}{c}{1} & 6.6\,kW    \\ \hline
\end{tabular}
\label{tab:Gebäudemodell}
\end{table}

Der nominelle Heizbedarf des Gebäudes wird über das Modell \glqq CalcHeaDemROM\grqq{} der BESMod berechnet. Diese Berechnung wird nach DIN EN 12831 \cite{din_en_12831_energetische_2017} durchgeführt. Dafür wird eine nominelle Außentemperatur vorgegeben, die dem Gebäude konstant aufgeprägt wird. Diese beträgt hier -12\,°C. Die Raumsolltemperatur beträgt 20\,°C. Daraus ergibt sich dann ein Wärmestrom, der dem Gebäude zugeführt wird. Ist der Zustand des Systems stationär, also Raumtemperatur und zugeführter Wärmestrom konstant, kann der zugeführte Wärmestrom als nomineller Heizbedarf entnommen werden. 

\textbf{Randbedingungen}
\\
Während der Simulation werden bestimmte Randbedingungen und Parameter vorgegeben. \\ 
Eine Randbedingung ist das Wetter. Dieses gibt dem System dynamische Randbedingungen vor, die Einfluss auf das Gebäude und die Wärmepumpe haben. Als Wetterdaten wird ein Testreferenzjahr (TRJ) verwendet. TRJs repräsentieren einen mittleren, für das Jahr typischen Witterungsverlauf. Dafür werden Wetterdaten von 1995 bis 2012 für den jeweiligen Standort als Referenz gewählt. Die Wetterdaten des TRJs beinhalten unter anderem Werte für Lufttempertur, Sonneneinstrahlung und Luftfeuchtigkeit in Intervallen von einer Stunde. \cite{deutscher_wetterdienst_handbuch_2017}\\
Für diese Arbeit werden Wetterdaten vom Standort Potsdam genutzt.

Eine weitere Randbedingung sind interne Gewinne. Das ist Wärme, die über Personen, Maschinen und Licht in das Gebäude eingebracht wird. Diese werden dynamisch mit Intervallen von einer Stunde in das System einbezogen.

Wichtig ist zudem die Dimensionierung des Transfersystems, also der Fußbodenheizung. Das passiert im Modell über die Vorgabe von Fläche der thermischen Zone, nominellem Heizbedarf, der nominellen Außentemperatur und der nominellen Vorlauftemperatur. Die ersten drei Werte wurden im Abschnitt zur Berechnung des Heizbedarfs schon beschrieben, die nominelle Vorlauftemperatur wird hier auf 40\,°C gesetzt. 


\textbf{Regelung}
\\
Das System bestehend aus Wärmeerzeuger, Transfersystem und Gebäudemodell wird über einen Systemregler geregelt. Die Entwicklung dieses Reglers ist ein zentraler Arbeitsbereich dieser Arbeit. Darauf wird näher in Kapitel \ref{cha:Reglerentwicklung} eingegangen.

\section{Framework der MPR}
\label{sec:MPR Programm}

Die Regelung des Systems wird sowohl in den rein simulativen Versuchen als auch in den Versuchen mit realer Wärmepumpe über ein in \cite{stoffel_online_2022} und \cite{berktold_modellpradiktive_nodate} entwickeltes Framework realisiert.\\
Dieses Framework ist in Python geschrieben und beinhaltet unter anderem Funktionen zum Training datengetriebener Modelle, zum Aufstellen des Optimierungsproblems und zur eigentlichen Regelung des Systems. \\ 
Zunächst wird der Zustandsraum des Systems, also zum Beispiel die Stellgrößen oder die Störgrößen, definiert. Aus diesem Zustandsraum wird das Optimierungsproblem in CasADi \cite{andersson_casadi_2019} Syntax definiert. Dabei werden auch die datengetriebenen Modelle integriert. Dieses Optimierungsproblem wird in jedem Schritt der MPR über den Solver Ipopt \cite{andreas_wachter_implementation_2006} gelöst. Die Stellgröße wird an das zu regelnde System weitergegeben. \\
Dabei können verschiedene Systeme genutzt werden. Im Rahmen dieser Arbeit wird dafür aus einem Dymola Modell eine Functional Mock-Up Unit (FMU) erstellt. Die Kommunikation zu diesem System ist im Framework schon integriert.

\section{Versuchsaufbau}
\label{sec:Versuchsaufbau}

Im Folgenden wird der Versuchsaufbau des realen Systems beschrieben. Ein Ziel der Arbeit ist es, die Effekte einer realen Wärmepumpe mit MPR zu untersuchen. Der Betrieb einer realen Wärmepumpe unterscheidet sich vom Betrieb einer modellierten Wärmepumpe. Gründe dafür sind unter anderem nicht statisches Verhalten der realen Wärmepumpe und Dynamiken von Wärmequellen und Wärmesenken. \cite{Mehrfeld-KI-2020} \\
Es wird also eine Versuchsumgebung benötigt, die eine reale Wärmepumpe mit einbezieht. Diese Versuchsumgebung ist der Hardware-in-the-Loop (HiL) Prüfstand des Lehrstuhls für Gebäude- und Raumklimatechnik der RWTH Aachen. Außerdem wird eine Wärmepumpe benötigt, welche die in dieser Arbeit zu untersuchenden Schnittstellen zur Verfügung stellt. Es wird zunächst der HiL-Prüfstand beschrieben und anschließend die in dieser Arbeit verwendete Wärmepumpe.    


\subsection{Hardware-in-the-Loop Prüfstand}
\label{subsec:HiL}
Der HiL-Prüfstand besteht hardwareseitig aus Klimakammer und hydraulischem Prüfstand.
Abbildung \ref{fig:HiL Prüf} zeigt das Schema des HiL-Prüfstands. Die Wärmepumpe steht in der Klimakammer, welche die Umgebungsbedingungen emuliert. Der hydraulische Prüfstand emuliert die Wärmesenke der Wärmepumpe. Im Folgenden werden die drei Hauptkomponenten des HiL-Prüfstands genauer beschrieben.

\pagebreak

\begin{figure}[h]
\centering
\def\svgwidth{410pt}    
\input{All_in_One/Figures/HiL_Schema.pdf_tex}  
\caption{Schematische Darstellung des HiL-Prüfstands. Die Klimakammer erhält Umgebungstemperatur ($T_\mathrm{U}$) und Luftfeucthigkeit ($\varphi$) vom Simulationsmodell. Das Simulationsmodell erhält zudem die Vorlauftemperatur ($T_\mathrm{VL}$) des Heizkreises vom hydraulischen Prüfstand und übergibt an diesen die Rücklauftemperatur ($T_\mathrm{RL}$).}
\label{fig:HiL Prüf}
\end{figure}


\textbf{Klimakammer}

Die Klimakammer hat die Aufgabe, realistische Umgebungsbedingungen für die Wärmepumpe vorzugeben. Dafür nutzt sie Wetterdaten, die aus der Simulation vorgegeben werden. Diese werden in Echtzeit in regelmäßigen Zeitintervallen an die Regelung der Klimakammer weitergegeben. Konkret bekommt diese Regelung eine Sollumgebungstemperatur und eine Sollluftfeuchtigkeit aus den Wetterdaten.\\
Die Regelung der Klimakammer funktoniert wie folgt: Die Luft der Klimakammer wird von einem Ventilator angesaugt. Der Nennvolumenstrom beträgt dabei 3000\,$\mathrm{\tfrac{m^{3}}{h}}$. Die angesaugte Luft wird runtergekühlt und dabei entfeuchtet. Anschließend wird sie von einem elektrischen Heizer auf die Solltemperatur gebracht und über einen Befeuchter auf die Sollluftfeuchtigkeit. \cite{nurenberg_hardware---loop_2017}

\textbf{Hydraulischer Prüfstand}

Der hydraulische Prüfstand besteht aus acht verschiedenen hydraulischen Kreisen. Diese können jeweils als Wärmesenke oder Wärmequelle genutzt werden. Um zwischen den Modi zu wechseln werden entsprechende Ventile umgeschaltet. Dadurch wird zwischen Fernkälte und Fernwärme Anbindung gewechselt. Im Rahmen dieser Arbeit wird ein hydraulischer Kreis als Wärmesenke der Wärmepumpe genutzt. Über ein Mischventil kann die Temperatur des Wassers, welches als Rücklauf zur Wärmepumpe fließt, geregelt werden. Diese Sollrücklauftemperatur wird dabei über den Heizbedarf des simulierten Gebäudes definiert. \cite{nurenberg_hardware---loop_2017}

\textbf{Simulationsmodell}

Das Gebäudemodell läuft in Echtzeit in der Software Dymola. Um in ein HiL-Experiment mit einbezogen zu werden, kommuniziert es mit dem HiL-Prüfstand. Dabei erhält es in dieser Arbeit die Vorlauftemperatur der Wärmepumpe und den Volumenstrom des Heizkreises. An den Prüfstand zurück werden Sollumgebungstemperatur für die Klimakammer und Sollrücklauftemperatur für den hydraulischen Prüfstand gegeben. Diese Daten werden sekündlich aktualisiert, sodass eine ständige Kommunikation mit dem Prüfstand besteht. Je nach Heizbedarf des Gebäudes verändert sich die Rücklauftemperatur zum hydraulischen Prüfstand.

\subsection{Wärmepumpenprüfstand}
\label{subsec:Wärmepumpe}

In dieser Arbeit wird ein im Rahmen einer Abschlussarbeit \cite{horst_aufbau_2021} gebauter Wärmepumpenprüfstand genutzt. Die folgende Beschreibung der Wärmepumpe hält sich an diese Arbeit. \\


Die Wärmepumpe wurde mit dem Ziel entwickelt, frei ansteuerbare Aktoren zu haben, um eigene Regelkonzepte bewerten zu können. Dabei wurde eine kommerziell erhältliche Wärmepumpe um Sensorik und Aktorik ergänzt. Die ergänzten Aktoren waren dabei ein elektronisches Expansionsventil mit Schrittmotor und ein Verdichter, bei dem über einen Frequenzumrichter variabel die Drehzahl angesteuert werden kann. Durch Nutzung der frei ansteuerbaren Verdichterdrehzahl wird im Rahmen dieser Arbeit untersucht, inwieweit die Ansteuerung dieser Schnittstelle einen Vorteil gegenüber Ansteuerung der Vorlauftemperatur bieten kann.

Abbildung \ref{fig:WP_RI_Opti} zeigt ein Fließbild der Wärmepumpe. Im Vergleich zum in Abschnitt \ref{sec:Kältekreis} dargestellten Kältekreis werden die vier Hauptkomponenten Verdichter, Kondensator, Expansionsventil und Verdampfer um ein 4-Wege-Ventil erweitert. Über das 4-Wege-Ventil kann der Kältekreis umgekehrt werden. Dadurch wird Wärme über den ursprünglichen Verdampfer abgegeben und über den ursprünglichen Kondensator aufgenommen. So kann die Wärmepumpe zum Kühlen genutzt werden. Außerdem kann bei Reifbildung an den Lamellen des Verdampfers dieser abgetaut werden.

\begin{figure}[h]
\centering
\def\svgwidth{280pt}    
\input{All_in_One/Figures/WP_RI_Opti.pdf_tex}  
\caption{Schematische Darstellung der Wärmepumpe. Die Hauptkomponenten sind Verdampfer, Verdichter mit Frequenzumrichter (FU), Kondensator, elektronisches Expansionsventil und 4-Wege-Ventil. Über die Stellgröße $n_\mathrm{{Komp}}$ wird die Verdichterdrehzahl gesteuert und über die Stellgröße $u_\mathrm{{EV}}$ der Öffnungsgrad des Expansionsventils.}
\label{fig:WP_RI_Opti}
\end{figure}

\subsection{Kältekreisregelung des Wärmepumpenprüfstands}
\label{subsec:Regelung Wärmepumpe}

Wie in Abschnitt \ref{subsec:Regelung einer Wärmepumpe} beschrieben, wird bei einer Wärmepumpe die Überhitzung geregelt. Bei der hier vorliegenden Wärmepumpe wird dafür das Regelverfahren der Führungsgrößenvorsteuerung verwendet. Dieses Verfahren wurde für diese Wärmepumpe in \cite{fiedler_entwicklung_nodate} entwickelt und in \cite{knieps_entwicklung_nodate} verbessert.\\
Bei der Führungsgrößenvorsteuerung werden zwei GPRs genutzt, die auf Basis des Systemzustandes eine minimale, stabile Sollüberhitzung und eine Sollexpansionsventilstellung vorgeben. Die Modellabweichungen bei Vorhersage der Sollexpansionsventilstellung werden durch einen PI-Regler kompensiert. Die Regelung wurde in \cite{knieps_entwicklung_nodate} validiert. 

Für diese Arbeit ist neben der Überhitzungsregelung zudem der Startvorgang der Wärmepumpe relevant. In diesem wird die Wärmepumpe so lange in einem sicheren Betriebsbereich gehalten bis bestimmte Bedingungen erreicht werden. Dabei wird zwischen Kalt- und Warmstart der Wärmepumpe unterschieden. Beim Kaltstart ist der Startvorgang beendet, sobald Kompressorein -und ausgangstemperatur keine starke Änderung mehr aufweisen und ein Kondensationsdruck von über 15\,bar erreicht wird. Beim Warmstart muss nur ein Kondensationsdruck von über 15\,bar erreicht werden. Beim Erreichen dieser Bedingungen wechselt die Wärmepumpe vom Startvorgang in die Führungsgrößenvorsteuerung.\\

Außerdem wird beim Ausschalten der Wärmepumpe ein Pumpdown durchgeführt. Dieser verhindert, dass beim anschließenden Start der Wärmepumpe flüssiges Kältemittel vom Verdichter angesaugt wird. Bei einem Pumpdown wird das Expansionsventil auf einen niedrigen Öffnungsgrad gesetzt, der Kompressor läuft auf einer niedrigen Drehzahl. Durch diesen Vorgang wird Kältemittel aus dem Verdampfer gesaugt und der Druck am Verdichtereingang nimmt ab. 


\subsection{Kommunikation am Prüfstand}
\label{subsec:Kommunikation am Prüfstand}

 Es müssen Kommunikationsschnittstellen zwischen Wärmepumpensteuerung, HiL Prüfstandssteuerung und Simulationsmodell bestehen. In diese Kommunikationsstruktur wird sich auch der in dieser Arbeit entwickelte Regler eingliedern. Außerdem müssen zur Auswertung von Versuchen, die generierten Daten gespeichert werden. Um die Kommunikationsstruktur umzusetzen werden drei Kommunikationsprotokolle genutzt. Diese sind Hypertext Transfer Protocol Secure (HTTPS), Message Queuing Telemetry Transport (MQTT) und Automation Device Specification (ADS). Die drei Kommunikationsprotokolle werden im Folgenden kurz erklärt. 

 \textbf{HTTPS}
 
Die folgenden Beschreibungen gelten sowohl für HTTP als auch für HTTPS. HTTPS ist das in dieser Arbeit genutzte Kommunikationsprotokoll. Es ist die verschlüsselte Variante zu HTTP. \cite{dasari_https_nodate} HTTP bildet die Basis für Datenaustausch im Internet. Es wird dabei immer zwischen einem Client und einem Server kommuniziert. Die Kommunikation wird durch den Client initialisiert. Dieser sendet eine Nachricht an den Server. Die Nachricht beinhaltet die Operation, die vom Server gewollt ist. HTTP wird daher als Frage-Antwort-Schema bezeichnet. \\
Für diese Arbeit ist die Query Methode relevant. Diese wird genutzt, um größere Datenmengen von einem Server zu erhalten. \cite{reschke_http_nodate, mdn_contributors_overview_nodate}

\textbf{MQTT}

MQTT ist nicht wie HTTPS ein Frage-Antwort-Protokoll, sondern ein Publish-Subscribe-Protokoll. Die Kommunikation besteht dabei zwischen einem oder mehreren Clients und einem Server. Der Server wird in diesem Kontext als Broker bezeichnet. \\
Die Clients werden in Subscriber und Publisher unterteilt. Die Publisher schicken Nachrichten an den Broker. Diese Nachrichten beinhalten ein Topic und die Nachricht an sich. Die Subscriber registrieren sich für Topics. Sobald ein Publisher eine Nachricht unter einem Topic veröffentlicht, leitet der Broker diese Nachricht an alle Clients, die dieses Topic subscribed haben, weiter. \cite{al-masri_investigating_2020}

\pagebreak

\textbf{ADS}

ADS ist das Kommunikationsprotokoll der Prüfstandssoftware TwinCAT. Es ist ein Frage-Antwort-Kommunikationsprotokoll. \cite{beckhoff_automation_gmbh__co_kg_twincat_nodate} Über ADS werden Messwerte von der Prüfstandssoftware ausgelesen und Steuergrößen an die Prüfstandssoftware gesendet.

\textbf{Kommunikatiosstruktur am HiL-Prüfstand}

Abbildung \ref{fig:Komm Prüf} zeigt die Kommunikationsstruktur am HiL-Prüfstand. Kommuniziert wird dabei zwischen der Steuerung des Prüfstands (beinhaltet Steuerung von Wärmepumpe, Klimakammer und hydraulischem Prüfstand), dem simulierten Gebäude, einer Datenbank und dem Systemregler, der in dieser Arbeit in die HiL-Prüfstandsumgebung integriert wird. Die Prüfstands-PCs fungieren dabei gleichzeitig als Server und Client, der die Daten weiterverarbeitet.\\

\begin{figure}[h]
\centering
\def\svgwidth{400pt}    
\input{All_in_One/Figures/Kommunikation_Pruefstand_m_Regler.pdf_tex}  
\caption{Kommunikationstruktur am HiL-Prüfstand. Über die Kommunikationsprotokolle HTTPS, MQTT und ADS werden Daten zwischen den verschiedenen Komponenten des Prüfstands ausgetauscht. Die Messdaten werden in einer Datenbank gespeichert.}
\label{fig:Komm Prüf}
\end{figure}

Als Datenbank wird die InfluxDB \cite{influxdata_influx_nodate} genutzt. Diese speichert Zeitreihen ab, also zum Beispiel Messwerte mit ihrem jeweiligen Zeitstempel. \\
Die Steuerung des Prüfstands sendet über ADS Messwerte an die Prüfstands-PCs. Diese werden von da veröffentlicht. Die Datenbank und das simulierte Gebäude erhalten die Nachrichten zu den Topics, die sie registriert haben. Für das simulierte Gebäude ist das zum Beispiel die Vorlauftemperatur der Wärmepumpe. Aus der Simulation werden wiederum Nachrichten veröffentlicht, die über die Prüfstands-PCs an die Steuerung gehen. Das ist zum Beispiel die Soll-Temperatur für die Klimakammer. \\
Zur Auswertung der Messwerte werden diese als Zeitreihen über HTTPS aus der Datenbank ausgelesen. 

\subsection{Integration des Reglers in die Wärmepumpensteuerung}
\label{subsec:Integration Regler}

Als erstes wird die Kommunikation vom Framework mit dem Gebäudemodell beschrieben, danach die Integration des Frameworks in die HiL-Prüfstandsumgebung. 

\subsubsection{Modell}
\label{subsubsec:Modell_Int}

In das Framework ist die Kommunikation mit FMUs bereits integriert. Von daher wird nur das Modell angepasst. Um mit demselben Modell verschiedene Reglervariationen zu ermöglichen, wird eine logische Verschaltung integriert, die abhängig von Reglereinstellungen im Framework die Stellgrößen für Kompressordrehzahl und Vorlauftemperatur der Wärmepumpe und Ventilöffnungsgrad des Heizkreises setzt. 

\subsubsection{Prüfstand}
\label{subsubsec:Int Prüfstand}

Zur Integration des Reglers in die Prüfstandsumgebung werden sowohl Schnittstellen am Framework als auch in der Steuerung der Wärmepumpe benötigt. Diese werden im Folgenden beschrieben.

\textbf{Framework}

Um das Framework in die HiL-Prüfstandsumgebung einzubinden, wird die HiL-Klasse entwickelt. Diese hat die Aufgabe, die zur Regelung relevanten Daten zu lesen, die Steuergrößen zu senden und Vorhersagen einzubinden. 

Abbildung \ref{fig:Schrittkontrolle} zeigt das Fließbild zur Kontrolle der Eingriffszeitpunkte des Reglers. $t_\mathrm{sim}$ wird aus der Simulation ausgelesen. Sobald die ausgelesene Zeit größer als $t_\mathrm{w}$ ist, greift der Regler in das System ein. $t_\mathrm{w}$ ergibt sich dabei aus $t_\mathrm{sys}$ und $t_\mathrm{s}$. $t_\mathrm{sys}$ wird eingeführt, um die Funktion robust zu halten.  

\begin{figure}[h]
\centering
\def\svgwidth{200pt}    
\input{All_in_One/Figures/Zeitkontrolle.pdf_tex}  
\caption{Fließbild zur Kontrolle der Eingriffszeitpunkte des Reglers. $t_\mathrm{sim}$ ist die ausgelesene Simulationszeit, $t_\mathrm{w}$ ist die Zeit, bei der der Regler das nächste Mal eingreift, $t_\mathrm{s}$ ist die Schrittweite und $t_\mathrm{sys}$ ist die Systemzeit.}
\label{fig:Schrittkontrolle}
\end{figure}

Es ist eine passende Kommunikationsstruktur notwendig, um den Systemregler in die HiL-Prüfstandsumgebung zu integrieren. Dazu werden die beiden in \ref{subsec:Kommunikation am Prüfstand} beschrieben Schnittstellen HTTPS und MQTT genutzt. Abbildung \ref{fig:Komm Prüf} zeigt, wie der Regler in die Kommunikationsstruktur eingebunden wird. 

Um die Optimierung durchführen zu können, muss der Regler den Zustand des Systems kennen. Diese Größen werden vom Prüfstands-PC in die Datenbank geladen. Von dort liest der Regler die benötigten Daten über HTTPS aus. Diese Schnittstelle muss hier genutzt werden, da über MQTT nur auf aktuelle und nicht auf historische Daten zugegriffen werden kann. \\
Das Ergebnis der Optimierung, die Stellgröße, wird über MQTT gesendet. Der Prüfstands-PC hat die Stellgröße abonniert und schreibt diese über ADS auf die Steuerung.\\
Mit dieser Kommunikationsstruktur kann der Regler auf einem beliebigem Rechner oder Server mit Internetzugang laufen. Damit kann die Software als Service angeboten und verkauft werden.

\textbf{Wärmepumpensteuerung}

Zusätzlich wird eine Schnittstelle für die MPR in der Wärmepumpensteuerung integriert. Dabei wird zwischen den Modi direkte Kompressoransteuerung und Vorgabe der Vorlauftemperatur unterschieden. Zunächst wird der Modus direkte Kompressoransteuerung beschrieben.\\

In Abbildung \ref{fig:TC uKomp} ist das entsprechende Fließbild dargestellt. In jedem Zyklus des Steuerprogramms wird $u_\mathrm{Komp}$ ausgelesen. Das ist die von der MPR ausgerechnete optimale Wärmepumpenmodulation. Abhängig von der Zeitschrittweite der MPR wird $u_\mathrm{Komp}$ in regelmäßigen Abständen neu geschrieben. Wenn ein neuer Wert geschrieben wurde, wird abhängig von diesem überprüft, ob sich der Betriebszustand des Kompressors ändern soll. Wenn der Kompressor aus ist, kann er entweder angeschaltet werden oder aus bleiben. Dafür wird $u_\mathrm{Komp}$ mit $u_\mathrm{s}$ verglichen. Wenn der Schwellwert überschritten wird, wechselt die Wärmepumpe in den in \ref{subsec:Regelung Wärmepumpe} beschriebenen Startmodus. Ansonsten bleibt die Wärmepumpe aus. \\
Wenn der Kompressor an ist, kann er entweder ausgeschaltet werden oder anbleiben. Dafür wird wiederum überprüft, ob der Schwellwert unterschritten wird. Ist dies der Fall, wird ein Pumpdown durchgeführt und der Verdichter anschließend ausgeschaltet. Im anderen Fall bleibt der Verdichter an.\\
Die Verdichterdrehzahl wird nur geschrieben, wenn sich die Wärmepumpe im Standardbetrieb befindet, also der Startmodus beendet ist. Das ist wichtig, da sich die Wärmepumpe in einem sicheren Betriebszustand befinden muss bevor beliebige Drehzahlen angefahren werden können. Dies wird zum Schluss überprüft und abhängig davon die Kompressordrehzahl geschrieben oder nicht. Da die von der MPR vorgegebene Größe $u_\mathrm{Komp}$ relativ ist, wird diese in der Wärmepumpensteuerung in die absolute Verdichterdrehzahl umgerechnet.

\begin{figure}[]
\centering
\def\svgwidth{310pt}    
\input{All_in_One/Figures/TwinCAT_Ucomp.pdf_tex}  
\caption{Fließbild der Wärmepumpensteuerung bei direkter Kompressoransteuerung. $u_\mathrm{Komp}$ ist die Wärmepumpen Modulation, $u_\mathrm{s}$ ist der Schwellwert bei der der Kompressor an/aus geht und $RPS$ ist die Kompressordrehzahl.}
\label{fig:TC uKomp}
\end{figure}

Abbildung \ref{fig:TC TVL} zeigt das Fließbild bei Vorgabe von $T_\mathrm{VL,soll}$ von der MPR. Genau wie in Abbildung \ref{fig:TC uKomp} wird zunächst überprüft, ob der Verdichter an ist. Abhängig davon wird der Betriebszustand des Verdichters gesetzt. Dabei wird im Unterschied zur direkten Kompressoransteuerung eine absolute Verdichterdrehzahl als Schwellwert genutzt. Das liegt daran, dass $T_\mathrm{VL}$ über einen PI-Regler geregelt wird, dessen Ausgang die Verdichterdrehzahl ($RPS$) ist. Die Wärmepumpe geht also entweder in den Startmodus, den Pumpdown oder bleibt im vorherigen Zustand. \\
Da der PI-Regler $RPS$ in kurzen Intervallen schreibt, und nicht wie bei direkter Verdichterdrehzahlansteuerung in der Zeitschrittweite der MPR, wird überprüft ob ein Starten/Stoppen des Verdichters möglich ist. Das ist der Fall, wenn die Wärmepumpe mindestens 10 Minuten an oder aus war. Damit wird ein hochfrequentes Takten der Wärmepumpe verhindert. 

\begin{figure}[]
\centering
\def\svgwidth{310pt}    
\input{All_in_One/Figures/TwinCAT_TVL.pdf_tex}  
\caption{Fließbild der Wärmepumpensteuerung bei Vorgabe der Vorlauftemperatur. $T_\mathrm{VL,soll}$ ist die Soll-Vorlauftemperatur, $RPS_\mathrm{s}$ ist der Schwellwert bei der der Kompressor an/aus geht und $RPS$ ist die Kompressordrehzahl.}
\label{fig:TC TVL}
\end{figure}


\section{Versuchsdurchführung}
\label{sec:Versuchsablauf}

Ein Ziel der Versuche ist es, verschiedene Reglervariationen zu bewerten und zu vergleichen. Im Folgenden werden diese Reglervariationen beschrieben und ein Verfahren vorgestellt, mit dem die Versuchszeiträume für die HiL-Versuche ausgewählt werden. 
 
\subsection{Reglervariationen}
\label{sec:Reglervariationen}
 
Die Reglervariationen sind in Tabelle \ref{tab:Reglervariationen} dargestellt.\\
Für die MPR werden Versuche für zwei verschiedene Stellgrößen gefahren. Eine Konfiguration hat die Vorlauftemperatur ($u_\mathrm{{T}}$), die andere die Kompressormodulation ($u_\mathrm{{Komp}}$) als Stellgröße. Dabei wird zum einen ein Prozessmodell gewählt, welches die Raumtemperatur vorhersagt, zum anderen wird dieses um ein Prozessmodell, welches die elektrische Leistung der Wärmepumpe vorhersagt, ergänzt. Als Referenz wird ein Heizkurvenregler genutzt.\\



\begin{table}[h]
\centering
\caption{Die drei verschiedenen Reglervariationen und die jeweiligen Prozessmodelle.}
\begin{tabular}{|l|l|}
\hline
\multicolumn{1}{|c|}{Regler}                     & \multicolumn{1}{c|}{Prozessmodelle} \\ \hline
\multicolumn{1}{|l|}{\multirow{2}{*}{MPR, $u_\mathrm{T}$}} & $T_\mathrm{Raum}$   \\ \cline{2-2} 
\multicolumn{1}{|l|}{}                           & $T_\mathrm{Raum}$, $P_\mathrm{el}$  \\ \hline
\multirow{2}{*}{MPR, $u_\mathrm{Komp}$}                     & $T_\mathrm{Raum}$   \\ \cline{2-2} 
                                                 & $T_\mathrm{Raum}$, $P_\mathrm{el}$  \\ \hline
Heizkurve                                        & keins        \\ \hline
\end{tabular}
\label{tab:Reglervariationen}
\end{table}

Insgesamt werden also Versuche für fünf verschiedene Regler gefahren. Diese werden sowohl rein simulativ als auch am HiL-Prüfstand eingesetzt. 
In den rein simulativen Versuchen wird dabei der Zeitraum einer gesamten Heizperiode betrachtet. Jahressimulationen sind im Kontext dieser Arbeit nicht sinnvoll, da die Wärmepumpe nur zum Heizen des Gebäudes genutzt wird, was außerhalb der Heizperiode nicht notwendig ist. Die Heizperiode ist der Zeitraum vom 01.10. bis zum 30.04.. \\
Bei den Zeiträumen für die Versuche gibt es Unterschiede zwischen den rein simulativen und den Versuchen am HiL-Prüfstand. Während in der Simulation eine Jahressimulation betrachtet werden kann, ist ein so langer Zeitraum für einen Versuch in Echtzeit nicht praktikabel. Aus diesem Grund wurde für HiL-Versuche ein Verfahren entwickelt, welches ein ganzes Jahr über sogenannte Typtage repräsentieren kann. Dieses Verfahren wird im Folgenden beschrieben. 

\subsection{Typtage}
\label{subsec:Typtage}

Im Rahmen dieser Arbeit werden Typtage genutzt, die über das Verfahren des k-medoids-Clustering ermittelt werden. Bei diesem Verfahren werden die Tage so ausgesucht, dass vorher ausgewählte KPIs dieser Tage möglichst genau denen des ganzen Jahres entsprechen. Dafür wird das gesamte Jahr über einen mathematischen Optimierer in Cluster mit unterschiedlichen Anzahlen von Tagen unterteilt, deren Tage jeweils ähnliche Wettercharakteristiken aufweisen. Dabei werden unter anderem Umgebungstemperatur und Strahlung betrachtet. Der Clusterschwerpunkt (Medoid) wird für jedes Cluster bestimmt und ist der für das jeweilige Cluster repräsentative Tag. Da die Cluster unterschiedliche Anzahlen von Tagen enthalten, werden die Typtage bei Auswertung der KPIs unterschiedlich stark gewichtet. \cite{Mueller-IEBWUMS-Abschlussbericht-2017} \\
Es wurde  \citeauthor{mehrfeld_dynamic_2020} in und \citeauthor{Mueller-IEBWUMS-Abschlussbericht-2017} dargestellt, dass über dieses Verfahren mit vier Typtagen relative Abweichungen einer Jahressimulation zu einer Simulation über die Typtage von weniger als 4\,\% erreicht werden. \\

\subsection{Ausgewählte Zeiträume}
\label{subsec:Zeiträume}

Die für diese Arbeit verwendeten Typtage mit der jeweiligen Gewichtung sind in Tabelle \ref{tab:Typtage} dargestellt. 

\begin{table}[h]
\centering
\caption{Typtage mit der jeweiligen Gewichtung für das TRJ am Standort Potsdam.}
\begin{tabular}{clclc}
\hline
Typtag     & \multicolumn{1}{c}{11.01.} & 19.08. & 05.09.                  & 18.10. \\ \hline
Gewichtung & 114                        & 62     & \multicolumn{1}{c}{102} & 87    \\ \hline
\end{tabular}
\label{tab:Typtage}
\end{table} 

Der 19.08. wird dabei für diese Arbeit nicht verwendet, da an diesem Tag nicht geheizt werden muss. \\ 
Für die rein simulativen Versuche wird also der Zeitraum der Heizperiode und die drei Typtage ausgewählt. Mit den drei Typtagen können die Typtage für die verschiedenen Reglervariationen validiert werden. Für jede der fünf Reglervariationen werden vier verschiedene Zeiträume ausgewertet. Es ergeben sich also 20 Variationen.\\
Für die Versuche mit realer Wärmepumpe werden die drei Typtage als Zeitraum ausgewählt, sodass sich 15 verschiedene Variationen ergeben. Das entspricht 15 Tage Versuche in Echtzeit.

\subsection{Vorsimulation}
\label{subsec:Vorsimulation}

Der Verlauf eines Versuchs hängt vom Ausgangszustand des Gebäudes ab. Um die Versuche vergleichbar zu machen und das Gebäude auf einen realistischen Ausgangszustand zu bringen wird eine Vorsimulation vor jedem Versuch durchgeführt. Dies ist notwendig, da Gebäude hohe thermische Kapazitäten und damit hohe Trägheiten aufweisen.\\
In der Vorsimulation wird dabei über den Zeitraum eines Tages das Gebäude über den in Kapitel \ref{sec:Referenzregler} entwickelten Referenzregler geregelt. Der ausgewählte Tag ist dabei der Vortag zum Startzeitpunkt des eigentlichen Versuchs. Durch dieses Verfahren passen sich Größen des Gebäudes wie zum Beispiel Temperaturgradienten oder thermische Kapazitäten an die Umgebungsbedingungen an. 
