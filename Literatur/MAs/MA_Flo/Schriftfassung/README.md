# Wichtige Hinweise zu Formularen / Important Information on forms
**English version below** 

Allgemeiner Link:
http://www.rwth-aachen.de/cms/root/Studium/Im-Studium/Pruefungen-Abschlussarbeiten/~hjxv/Hinweise-zu-schriftlichen-Arbeiten/

Direktlinks:
Eidesstattliche Versicherung Deutsch: 
http://www.rwth-aachen.de/global/show_document.asp?id=aaaaaaaaaaoasiu

Verwendung RWTH-Logo Deutsch: 
http://www.rwth-aachen.de/global/show_document.asp?id=aaaaaaaaaambazz




**English version**

General Link:
http://www.rwth-aachen.de/cms/root/Studium/Im-Studium/Pruefungen-Abschlussarbeiten/~hjxv/Hinweise-zu-schriftlichen-Arbeiten/?lidx=1

Direct Links:
Affidavit form (only german version exists):
http://www.rwth-aachen.de/global/show_document.asp?id=aaaaaaaaaaoasiu

declaration of use RWTH logo English (but german version has to be signed): 
http://www.rwth-aachen.de/global/show_document.asp?id=aaaaaaaaaambbaa

# Font (Schriftart) installation

To not receive pixelated headers you need to install the font `cm-super`!  
The easiest way to do this, use the MiKTeX Package manager (described in [this post](https://tex.stackexchange.com/a/181194)).  
The minimal example in the comment above or below at tex.stackexchange.com varifies the font installation.

# Q and A

## Questions and Answers 


This is an example chapter with the purpose of providing answers and
examples to everyday challenges in LaTeX. The format represents a
question and answer structure separated by the category. It is
recommended that you read through the questions before trying to find a
solution to your problem, in most cases, someone has already found the
solution and has written the answer here. Feel free to use the examples
provided here in your own document. Keep in mind that some methods have
conflicts with other methods and therefore the least used case is
commented here.

Although we tried to include as many answers and examples as possible,
you may find better options to achieve certain tasks. Feel free to
extend this document or write issues if this template needs correction
in its GIT repository.

What should I consider when using this template?

The template uses the so called toggles to switch between different
modes. So you have to consider what you are writing, e.g. a master
thesis or a dissertation and correct the toggles by changing them to
false or true (e.g. `\toggletrue{thesis}`). This automatically reformats
your document to include the correct cover page, margins, chapters, etc.

After that you just update the pages you need and add your chapters to
the document. If you do not have very special wishes, you do not need to
change anything else in the document as all the packages and options
that you may require are predefined.

How do I choose the language between German and English?

Change `\togglefalse{ingerman}` to `\toggletrue{ingerman}` for a
document in German or vice versa for a document in English.

## Page Layout 

This document uses the “Koma-Script” class. This class provides great
flexibility in comparison to predefined classes. The following Q&A’s
describe some of the commands necessary to achieve different tasks in
the Koma-Script class. But, there are many more options available which
you can find
[here](http://ftp.uni-erlangen.de/ctan/macros/latex/contrib/koma-script/doc/scrguien.pdf).

How do I write a part of my document in German/English?

`\selectlanguage{language}` is used to change the default language at
any part of your document. Don’t forget to change it back after you
finished your text in the second language. For example,

    \selectlanguage{ngerman}
        Heizölrückstoßabdämpfung        
      \selectlanguage{english}
        Some text in English ...
                

will result in: Heizölrückstoßabdämpfung and Some text in English ...

This is specially useful, for example, when you are writing an abstract
in two languages.

How do I change the font size of my whole document?

11 points is chosen as the default font size in most of the templates at
the institute. If you want to change it, just change the value in the
options of the `\documentclass`.

For example, `\documentclass[12pt]{scrreprt}` changes the font size of
the whole document to 12 points.

How do I change the font size of a part of the document (locally)?

Depending on the situation, there are a few ways to to this. LaTeXhas
many predefined formats such as `\huge`, `\large`, `\small`, etc. You
can use them either inline or in an environment.

-   Inline: Just write the command in front of your text in a curly
    bracket.

    For example, `{\Huge Text}` results in a <span>Huge Text</span>,

    and `{\tiny Text}` results in a <span>tiny Text</span>.

-   Environment: You can use an environment not only to change the font
    size of a block of text but rather the font size in figures and
    tables. You can create the environment as follow:

        \begin{Font size}
            Your text here ...      
          \end{Font size}
                    

Keep in mind that there is usually no need to change your font size. A
uniform font size makes the document look much better. Change the font
size locally only if no other method of separating context works.

What should I consider when I want to have a double sided document?

Use the option `twoside` for the document class.

`\documentclass[twoside]{scrreprt}`.

This automatically moves the page number of odd pages to the right and
even pages to the left. It also recalculates the binding space for a two
sided document depending on odd and even pages.

The figure/table captions are too long resulting in a very cluttered
list of figures and tables. How can I prevent that?

You can actually define two captions, one for the table of contents
(TOC) by placing it in a bracket and one for the actual figure or table
by placing it inside a curly bracket.

`\caption[Caption for TOC]{Actual caption for the figure, table, chapter, section, etc.}`.

## Figures 

There are many ways to import different figures in LaTeX. It is always
recommended to keep the fonts similar to the rest of the document. Below
are a few examples of what can be done.

How can I input a figure in LaTeX?

You can use the figure environment to include figures with a caption and
a label (see Figure [fig:PDFExample]). The code for including the Figure
[fig:PDFExample] is given below.

`\begin{figure}[h]   `\
` \centering`\
` \includegraphics[scale=0.5]{Resources/rwth_eerc_rgb_ohne_Schutzraum}`\
` \caption{A PDF example figure.}`\
` \label{fig:PDFExample}`\
` \end{figure}`\

![A PDF example figure.](Resources/rwth_eerc_rgb_ohne_Schutzraum "fig:")
[fig:PDFExample]

How can I input a figure side by side?

To be answered ...

How can I input a figure but with LaTeX fonts?

One way to do that is to create/import your figure in Inkscape. Then
save it as a PDF and check the PDFLaTeX box while saving. This generates
two separate files. One includes all the graphics as a PDF and one is a
TeX formatted file including all the text and their positions in the
figure with the extension pdf\_tex. Note that this file loads the PDF
graphics automatically. If these files are in a folder not directly
accessible from your main document, you will get an error. To fix that
you have to add the path to your graphics path by writing:

`\graphicspath{{<path to file>/}}`

in your main document. Figure [fig:InkScapeExample] shows an example.

[fig:InkScapeExample]

Note that changing the scale of the figure does not affect the font
size. You have to adjust the position of the text boxes slightly as the
font size and types vary in the Inkscape file and LaTeX.

It is also possible to write equations or accented characters by just
writing them in LaTeX mathematics format in your Inkscape file.

How can I implement a diagram while having the LaTeX font?

The easiest way is to save your diagram in Python or Matlab with their
LaTeX font in PDF format and include those in your document. The problem
with that method is that you cannot scale your diagram without scaling
the font as well. And, changing the final figure is impossible.

Another method is to use TikZ plots. Note that TikZ plots are not only
for diagramms, and there are many examples what you can do
[here](http://www.texample.net/tikz/). There are functions in Matlab and
Python (for example [here](https://github.com/matlab2tikz/matlab2tikz))
that generate TiKZ plots from your figures. They basically generate a
TikZ code which includes the definition of axis and labels following by
the actual data points of your diagram. Several types diagrams are
possible such as bars, line, etc with or without error bars. Sometimes
you have to slightly change the generated TikZ code for your need. You
can also directly change the labels or even the range of your diagram
within the TikZ code. Figure [fig:TikzDiagramExample] shows an example
of a TikZ diagram.

[fig:TikzDiagramExample]

Depending on the method you generate the TikZ code, it may contain fixed
dimensions. You can change it by replacing the fixed with and height
with variables `\figureheight` and `\figurewidth` in the TikZ code.

The disadvantage of using TikZ plots is that it makes compilation of
your LaTeXcode slower and when there are many data points in the
diagram, it may lead to a memory error. It is possible to compile TikZ
plots externally and only once when there is no change by using the
`\usetikzlibrary{external}`.

You can also prevent the memory error by using the LuaLaTeX compiler
instead of the PDFLaTeX.

In any case, it is recommended that you compile only the chapters you
are currently writing in while commenting the rest and compile the whole
document at the end.

## Tables


Creating tables in LaTeX is really time consuming. Making it look good
is sometimes even harder. But there are ways to make it easier. In any
case try to prevent using vertical lines in your table to make it look
more professional. In the following are some examples:

How can I create tables without using pure codes?

Usually different programs have some tools to make it easier. For
example TexMaker has a Table-Assistant. If you are using that, try to
make your table as close as to the one you actually want before exiting,
otherwise you cannot go back.

There is also an online tool [Tables
Generator](http://www.tablesgenerator.com/) which makes coding a table
much easier.

I am having a hard time writing the values in my table, is there a way
to do it more efficiently?

There are some tools to generate table data in LaTeX format. For example
the [latexTable function](https://github.com/eliduenisch/latexTable) in
Matlab can put your data in correct LaTeX table style.

What is the best way to draw horizontal lines in the table?

Use `\toprule`, `\midrule` or `\bottomrule` depending on the position
instead of `\hline`. These command add extra space in the correct
direction to prevent text being too close to the lines.
Tables [tab:hline] and [tab:rule] illustrate the difference.

How did we just put two tables side by side?

Use:
`\parbox{0.45\linewidth}{...}\hfill\parbox\parbox{0.45\linewidth}{...}`

See the code of the previous tables as an example.

Center aligned columns with predefined width are too time consuming.
What can I do?

Keep in mind that if you are using a certain type of column too many
times, it is always easier to define it once in your main document and
use your defined configuration instead. For the specific case of
centered alignment with predefined width, this template defines the
column type `C{<width>}}`. See Table [tab:PredefinedCenterAlignment] as
an example.

[ht]

<span>m<span>3cm</span>C<span>3cm</span></span> Non centered & Centered\
abc & efg\
123 & 456\

[tab:PredefinedCenterAlignment]

How can I align my data based on the decimal point?

It is more professional to align numbers on their decimal point rather
than format the columns centered. To make it easier, this template has a
predefined `d{x.y}` type which does the aligning. The `x` is the number
of characters at the left side and `y` is the number of characters at
the right side of the decimal point. The only problem is that if you
want to add label text in the same column, it enters the math-mode. To
prevent this, you have to write the label in a multicolumn format which
is only one column as follow:

`\multicolumn{1}{c}{Label}`

To align numbers with decimal comma instead of a decimal point, change
the following line in the main.tex file:

`  \newcolumntype{d}[1]{D{.}{.}{#1}}`

to:

`  \newcolumntype{d}[1]{D{,}{,}{#1}}`

Table [tab:DecimalAllignment] shows an example.

[ht]

<span>lcd<span>5.5</span></span> Name & Centered &\
A & 1000 & 1000\
B & 100 & 100\
C & 10 & 10\
D & 1 & 1\
E & .1 & .1\
F & .01 & .01\
G & .001 & .001\
H & .0001 & .0001\

[tab:DecimalAllignment]

How can I wrap the text in more lines in a single table cell?

To be answered ...

How can I draw horizontal lines under only certain cells?

To be answered ...

## Literature and citations


How can I manage the literature?

You should use a reference manager to organize your literature. A common
reference management software is Citavi which can be installed from the
software center.

How can I import references to latex?

In latex you can cite papers, books, etc. by importing your refenerces
from a bibtex file (’*Literature.bib*’). The bibtex file can be created
with Citavi by exporting the references. If you have created or modified
the ’*Literature.bib*’ file, you have to compile your latex document
with BibTex once.\
To refer to a reference in the text, use the command `citep{}` or
`cite{}` to write the reference with or without square brackets:
[@Lamberg2003] or @Lamberg2003\
In order to understand all options for and types of the `cite` command,
e.g. to reference a particular page, have a look into online
documentations.

## Miscellaneous 


How do I create a nomenclature?

There are two possible way for creating the nomenclature:

1.  You can add all abbreviations manually in a table. Therefor, include
    the file NomenclatureTable in the main document:\
    `\include{Chapters/NomenclatureTable} `

2.  You can use the package `nomencl` to generate the nomenclature
    automatically. The advantage of the automatic generation is that you
    can add and define abbreviations everywhere in your document. For
    further information have a look
    [here](http://texdoc.net/texmf-dist/doc/latex/nomencl/nomencl.pdf).

How do I write Modelica Code in LaTeX??

You can use the lstlisting environment. There is a Modelica Style file
that defines the colors and format. It is also possible to add a
complete source code file after the lstlisting command.

`\begin{lstlisting}[language=modelica] <Code> \end{lstlisting}`

For example:

``` {language="modelica"}
    replaceable partial function f
        extends Modelica.Icons.Function;
        input ThermodynamicState state "thermodynamic state record";
        output Real f;
    end f;
```

How do I write footnotes?

To be answered ...

How do I restart footnote numbering at the beginning of each chapter?

To be answered ...
